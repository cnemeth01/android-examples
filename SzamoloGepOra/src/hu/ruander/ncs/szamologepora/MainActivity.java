package hu.ruander.ncs.szamologepora;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends Activity {

	private String[] muveletek={"+","-","/","*"};
	Spinner muveletSp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        muveletSp=(Spinner)findViewById(R.id.muveletSpiner);
        muveletSp.setAdapter(new ArrayAdapter<String>(this, R.layout.muveletitem, muveletek));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
        return true;
    }
    public void szamolj(View view){
    	EditText egyikEdit = (EditText) findViewById(R.id.egyikText);
    	EditText masikEdit= (EditText)findViewById(R.id.masikText);
    	EditText ereEdit=(EditText) findViewById(R.id.erdedmenyText);
    	String g;
    	
    	
    	try {
    		
			int egyik =Integer.parseInt((egyikEdit.getText().toString()));
			int masik =Integer.parseInt((masikEdit.getText().toString()));
			int eredmeny =0;
			
			String muvelet=(String)muveletSp.getSelectedItem();
			
			if (muvelet.equals("+")) {
				eredmeny=egyik+masik;
			}
			else if (muvelet.equals("-")) {
				eredmeny=egyik-masik;
			}
			else if (muvelet.equals("/")) {
				eredmeny=egyik/masik;
			}
			else if (muvelet.equals("*")) {
				eredmeny=egyik*masik;
			}
			
			
			
			ereEdit.setText(eredmeny+"");
			Log.d("szamolj()", egyik+"+"+masik+"="+eredmeny);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			ereEdit.setText("Hib�s sz�m");
			Log.e("szamolj()", e.getMessage());
			
			e.printStackTrace();
		}    	
    }
    
}
