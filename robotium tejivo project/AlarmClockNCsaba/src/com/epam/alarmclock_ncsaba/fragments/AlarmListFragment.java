package com.epam.alarmclock_ncsaba.fragments;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.epam.alarmclock_ncsaba.MainActivity;
import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.adapters.MainAdapter;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;

public class AlarmListFragment extends Fragment implements OnItemClickListener {

    public static final String TAG = "AlarmlistFragment";

    private MainAdapter adapter;
    private TextView txtTime;
    private String currentTime;
    private IAlarmListFragmentCallBack listener;
    private ListView mainList;
  

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_alarms_list, container, false);
        txtTime =(TextView) rootView.findViewById(R.id.tv_main_clock);
        Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		String strDate = sdf.format(c.getTime());
		txtTime.setText(strDate);
       
        mainList = (ListView) rootView.findViewById(R.id.lv_main);

        adapter = new MainAdapter(container.getContext(), R.id.lv_main );

        mainList.setAdapter(adapter);
        mainList.setOnItemClickListener(this);

        
        // TODO Auto-generated method stub
        return rootView;
    }
    
    public void setCurrentTime(String currentTime){
    	 txtTime.setText(currentTime);
    	
    }

    @Override
    public void onAttach(Activity activity) {

        try {
            listener = (IAlarmListFragmentCallBack) activity;
        } catch (ClassCastException ce) {
            Log.e(TAG, "Parent Activity does not implement listener interface!");
        } catch (Exception e) {
            Log.e(TAG, "Unexpected exception!");
            e.printStackTrace();
        }
        super.onAttach(activity);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        registerForContextMenu(mainList);
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

    }

   
    public void newAlarmOnClick(View v) {
       

    }

    public interface IAlarmListFragmentCallBack {
        public void newAlarm();
        public void alarmSettings(int position);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      AlarmModel alarm= MainActivity.alarms.get(position);
      view.setSelected(true);
        listener.alarmSettings(position);

    }
   
    
}
