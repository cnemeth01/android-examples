package com.epam.alarmclock_ncsaba.fragments;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TimePicker;

import com.epam.alarmclock_ncsaba.MainActivity;
import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.adapters.SettingsAdapter;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;
import com.epam.alarmclock_ncsaba.pojos.Days;

public class AlarmSettingsFragment extends Fragment {

    private static final int SET_ALARM_REPEAT_DAYS = 1;
    private static final int SET_ALARM_TIME = 0;
    private SettingsAdapter adapter;
    public static final String TAG = "AlarmSetFragment";
    private int alarmPositioninList = 0;
    private CheckBox chMon;
    private CheckBox chTue;
    private CheckBox chWed;
    private CheckBox chThu;
    private CheckBox chFri;
    private CheckBox chSat;
    private CheckBox chSun;

    public static final String ARG_SETTINGS = "settings";

    private AlarmModel alarm;
    private ListView settingList;
    private ArrayList<String[]> settings = new ArrayList<String[]>();

    public AlarmSettingsFragment() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.alarm_settings, container, false);

        setRetainInstance(true);
        //		if (savedInstanceState != null) {
        //			alarmPositioninList = (Integer) savedInstanceState.get(ARG_SETTINGS);
        //		}

        settingList = (ListView) rootView.findViewById(R.id.lv_settings);
        settingList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                switch (position) {
                case SET_ALARM_TIME:
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;

                    mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            MainActivity.alarms.get(alarmPositioninList).setAlarmTimeHour(selectedHour);
                            MainActivity.alarms.get(alarmPositioninList).setAlarmTimeMinutes(selectedMinute);
                            initList(alarmPositioninList);
                        }
                    }, hour, minute, true);// Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                    break;

                case SET_ALARM_REPEAT_DAYS:
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View daysChooser = inflater.inflate(R.layout.days_chooser, null);

                    final AlertDialog alert = builder.setView(daysChooser).create();

                    chMon = (CheckBox) daysChooser.findViewById(R.id.checkBoxMon);
                    chTue = (CheckBox) daysChooser.findViewById(R.id.checkBoxTue);
                    chWed = (CheckBox) daysChooser.findViewById(R.id.checkBoxWed);
                    chThu = (CheckBox) daysChooser.findViewById(R.id.checkBoxThu);
                    chFri = (CheckBox) daysChooser.findViewById(R.id.checkBoxFri);
                    chSat = (CheckBox) daysChooser.findViewById(R.id.checkBoxSat);
                    chSun = (CheckBox) daysChooser.findViewById(R.id.checkBoxSun);

                    Button btnDaysDone = (Button) daysChooser.findViewById(R.id.btnDaysDone);

                    alert.show();
                    alert.setOnKeyListener(new Dialog.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                            // TODO Auto-generated method stub
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                setDays(alarmPositioninList);
                                alert.dismiss();
                            }
                            return true;
                        }
                    });
                    btnDaysDone.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            setDays(alarmPositioninList);
                            alert.dismiss();

                            System.out.println(AlarmSettingsFragment.class.toString()
                                    + MainActivity.alarms.get(alarmPositioninList).getAlarmRepeatString());
                            initList(alarmPositioninList);

                        }
                    });

                    break;

                default:
                    break;
                }

            }

        });

        return rootView;
    }

    public void setDays(int positionInList) {
        MainActivity.alarms.get(alarmPositioninList).cleareRepeatDays();

        if (chMon.isChecked()) {
            MainActivity.alarms.get(alarmPositioninList).addAlarmRepeat(Days.MONDAY);
        }
        if (chTue.isChecked()) {
            MainActivity.alarms.get(alarmPositioninList).addAlarmRepeat(Days.TUESDAY);
        }
        if (chWed.isChecked()) {
            MainActivity.alarms.get(alarmPositioninList).addAlarmRepeat(Days.WEDNESDAY);
        }
        if (chThu.isChecked()) {
            MainActivity.alarms.get(alarmPositioninList).addAlarmRepeat(Days.THUESDAY);
        }
        if (chFri.isChecked()) {
            MainActivity.alarms.get(alarmPositioninList).addAlarmRepeat(Days.FRIDAY);
        }
        if (chSun.isChecked()) {
            MainActivity.alarms.get(alarmPositioninList).addAlarmRepeat(Days.SUNDAY);
        }
        if (chSat.isChecked()) {
            MainActivity.alarms.get(alarmPositioninList).addAlarmRepeat(Days.SATURDAY);
        }

    }

    @Override
    public void onStart() {
        if (getArguments() != null) {
            alarmPositioninList = getArguments().getInt(ARG_SETTINGS);
        }
        initList(alarmPositioninList);

    }

    public void initList(int alarmPositioninList) {
        if (settingList.isActivated()) {

            settingList.removeAllViews();
            adapter = new SettingsAdapter(alarmPositioninList);
            settingList.setAdapter(adapter);

        } else {
            adapter = new SettingsAdapter(alarmPositioninList);
            settingList.setAdapter(adapter);
        }

        super.onStart();

    }

    public void updateSettingList(int positioinInList) {

        initList(positioinInList);

    }

}
