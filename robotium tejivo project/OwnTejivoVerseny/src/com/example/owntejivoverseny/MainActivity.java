package com.example.owntejivoverseny;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

public class MainActivity extends ActionBarActivity {

    public static boolean isLogin = true;
    
    public static String Country;
    public static String City;
    public static String UserName;
    public static int Height;
    public static int Weight;
    public static int Age;
    public static boolean isMale;
    
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickLogin(View v) {

        isLogin = true;
        Intent intent = new Intent(this, LoginActivity.class);

        startActivity(intent);
        finish();

    }

    public void onClickSignUp(View v) {
        isLogin = false;

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
       

    }

}
