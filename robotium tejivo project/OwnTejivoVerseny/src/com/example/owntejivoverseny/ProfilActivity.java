package com.example.owntejivoverseny;

import java.util.Random;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

public class ProfilActivity extends ActionBarActivity {

    private TextView txtName;
    private TextView txtNumber;
    private TextView txtAge;
    private TextView txtCountry;
    private TextView txtCity;
    private TextView txtSex;
    private TextView txtHeight;
    private TextView txtWeight;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        txtName = (TextView) findViewById(R.id.textViewName);
        txtNumber = (TextView) findViewById(R.id.textViewNumber);
        txtAge = (TextView) findViewById(R.id.textViewAge);
        txtCountry = (TextView) findViewById(R.id.textViewCountry);
        txtCity = (TextView) findViewById(R.id.textViewCity);
        txtSex = (TextView) findViewById(R.id.textViewSex);
        txtHeight = (TextView) findViewById(R.id.textViewHeight);
        txtWeight = (TextView) findViewById(R.id.textViewWeight);


        

        txtName.setText(MainActivity.UserName);
        txtNumber.setText((randInt(10000, 200000)) + "");
        txtAge.setText(MainActivity.Age+" �v");
        txtCity.setText(MainActivity.City);
        txtCountry.setText(MainActivity.Country);
        
        if (MainActivity.isMale) {
            txtSex.setText("Male"); 
        }else  txtSex.setText("Female"); 
        txtHeight.setText(MainActivity.Height+" cm");
        txtWeight.setText(MainActivity.Weight+" kg");
        

    }

    public void onClickLogout(View v) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
       

    }

    public static int randInt(int min, int max) {
        Random rand = new Random();

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

}
