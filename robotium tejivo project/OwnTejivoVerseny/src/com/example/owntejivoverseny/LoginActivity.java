package com.example.owntejivoverseny;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends ActionBarActivity {

    public static final String USER_NAME = "Username";
    private Button btnSignUp;
    private TextView txtTitle;
    private EditText edName;
    private EditText edPassword;
    private String userName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnSignUp = (Button) findViewById(R.id.buttonSignUp);
        txtTitle = (TextView) findViewById(R.id.textViewTitle);
        edName = (EditText) findViewById(R.id.editTextUswerName);
        edPassword = (EditText) findViewById(R.id.editTextPassword);

        if (MainActivity.isLogin) {
            btnSignUp.setText("Login");
            txtTitle.setText("Login");
        } else {
            btnSignUp.setText("Continue");
            txtTitle.setText("Registration Page 1");
        }
    }

    public void onClickSignIn(View v) {
        userName = "";
        if (MainActivity.isLogin) {
            MainActivity.UserName = edName.getText().toString();
            Intent intent = new Intent(this, ProfilActivity.class);
            startActivity(intent);
        } else {

            MainActivity.UserName = edName.getText().toString();
            Intent intent = new Intent(this, RegPage2Activity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        edPassword.setText("");
        edName.setText("");
        super.onResume();
    }

}
