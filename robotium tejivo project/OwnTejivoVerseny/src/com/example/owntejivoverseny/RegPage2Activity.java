package com.example.owntejivoverseny;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class RegPage2Activity extends ActionBarActivity {

    
    private EditText edtxAge;
    private EditText edtxCity;
    private EditText edtxCountry;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_page2);
        
        edtxAge = (EditText) findViewById(R.id.editTextAge);
        edtxCity = (EditText) findViewById(R.id.editTextCity);
        edtxCountry = (EditText) findViewById(R.id.editTextCountry);
        

        String userName = getIntent().getStringExtra(LoginActivity.USER_NAME);
        
        
        
    }
    
    public void onClickRegTwoContinue(View v){
        MainActivity.Age=Integer.parseInt(edtxAge.getText().toString());
        MainActivity.City=(edtxCity.getText().toString());
        MainActivity.Country=(edtxCountry.getText().toString());
        
        Intent intent = new Intent(this, RegPage3Activity.class);
        
        startActivity(intent);
        
    }

   
}
