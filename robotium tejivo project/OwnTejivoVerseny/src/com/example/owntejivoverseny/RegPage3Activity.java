package com.example.owntejivoverseny;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class RegPage3Activity extends ActionBarActivity {

    
    private EditText edtxHeight;
    private EditText edtxWeight;
    private RadioButton rbIsMale;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_page3);
        
        edtxHeight = (EditText) findViewById(R.id.editTextHeight);
        edtxWeight = (EditText) findViewById(R.id.editTextWeight);
        rbIsMale = (RadioButton) findViewById(R.id.radioGroupisMale);
    }

public void onClickRegThreeContinue(View v){
    MainActivity.Height=Integer.parseInt(edtxHeight.getText().toString());
    MainActivity.Weight=Integer.parseInt(edtxWeight.getText().toString());
    MainActivity.isMale=(rbIsMale.isChecked());
    
    Intent intent = new Intent(this, ProfilActivity.class);
    
    startActivity(intent);
        
    }
}
