package com.example.owntejivoverseny.test;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.owntejivoverseny.LoginActivity;
import com.example.owntejivoverseny.MainActivity;
import com.example.owntejivoverseny.ProfilActivity;
import com.robotium.solo.Solo;

public class TestLogin extends ActivityInstrumentationTestCase2<MainActivity> {

    private Solo solo;

    public TestLogin() {
        super("com.example.owntejivoverseny", MainActivity.class);

    }

    /**
     * @param activityClass
     */
    public TestLogin(Class<MainActivity> activityClass) {
        super(activityClass);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation(), getActivity());

        setActivityInitialTouchMode(false);
        solo.waitForActivity(MainActivity.class);
    }

    @Override
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

    public void testPressLoginButtonInputUsernameAndPassworandPressLoginButtonAndLogOut() {
        //        fail();
        solo.assertCurrentActivity("wrong activity", MainActivity.class);

        Button btnLoginMain = (Button) solo.getView(com.example.owntejivoverseny.R.id.buttonMainLogin);
        solo.clickOnView(btnLoginMain);

        solo.assertCurrentActivity("wrong activity", LoginActivity.class);

        EditText editTextUser = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextUswerName);

        solo.clickOnView(editTextUser);
        solo.typeText(editTextUser, "Malacember");

        EditText editTextPassword = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextPassword);
        solo.clickOnView(editTextPassword);
        solo.typeText(editTextPassword, "12354Malac");

        Button loginButton = (Button) solo.getView(com.example.owntejivoverseny.R.id.buttonSignUp);
        assertEquals("Login", loginButton.getText().toString());

        solo.clickOnView(loginButton);
        solo.assertCurrentActivity("wrong activity" , ProfilActivity.class);

        Log.i("BaseTest", "blabla: " + ((TextView) solo.getView(com.example.owntejivoverseny.R.id.textViewName)).getText().toString());
        assertEquals("Malacember", ((TextView) solo.getView(com.example.owntejivoverseny.R.id.textViewName)).getText().toString());

        Button btnLogOut = (Button) solo.getView(com.example.owntejivoverseny.R.id.buttonLogOut);
        solo.clickOnView(btnLogOut);

        editTextUser = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextUswerName);
        Log.i("BaseTest", "Sz�veg: " + (editTextUser.getText().toString()));
        assertEquals("", (editTextUser.getText().toString()));
       
       
        solo.sendKey(KeyEvent.KEYCODE_BACK);
      
      
      

    }

}
