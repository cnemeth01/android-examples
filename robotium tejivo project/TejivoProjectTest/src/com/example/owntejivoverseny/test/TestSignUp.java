package com.example.owntejivoverseny.test;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.owntejivoverseny.LoginActivity;
import com.example.owntejivoverseny.MainActivity;
import com.example.owntejivoverseny.ProfilActivity;
import com.example.owntejivoverseny.RegPage2Activity;
import com.example.owntejivoverseny.RegPage3Activity;
import com.robotium.solo.Solo;

public class TestSignUp extends ActivityInstrumentationTestCase2<MainActivity> {

    
    private Solo solo;

    public TestSignUp() {
        // TODO Auto-generated constructor stub
    
        super("com.example.owntejivoverseny", MainActivity.class);

    }

    /**
     * @param activityClass
     */
    public TestSignUp(Class<MainActivity> activityClass) {
        super(activityClass);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation(), getActivity());

        setActivityInitialTouchMode(false);
        solo.waitForActivity(MainActivity.class);
    }

    @Override
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

    public void testPressSignUpButtonInputUsernameAndPassworandPressLoginButtonAndLogOut() {
        //        fail();
        solo.assertCurrentActivity("wrong activity", MainActivity.class);

        Button btnLoginMain = (Button) solo.getView(com.example.owntejivoverseny.R.id.buttonMainSignUp);
        solo.clickOnView(btnLoginMain);

        solo.assertCurrentActivity("wrong activity", LoginActivity.class);

        EditText editTextUser = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextUswerName);

        solo.clickOnView(editTextUser);
        solo.typeText(editTextUser, "Nemeth Csaba");

        EditText editTextPassword = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextPassword);
        solo.clickOnView(editTextPassword);
        solo.typeText(editTextPassword, "12354Malac");

        Button loginButton = (Button) solo.getView(com.example.owntejivoverseny.R.id.buttonSignUp);
        assertEquals("Continue", loginButton.getText().toString());

        solo.clickOnView(loginButton);
        solo.assertCurrentActivity("wrong activity" , RegPage2Activity.class);

        EditText editTextAge = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextAge); 
        EditText editTextCity = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextCity);
        EditText editTextCountry = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextCountry);
        
        
        solo.typeText(editTextAge, "38");
        solo.typeText(editTextCity, "Budapest");
        solo.typeText(editTextCountry, "Hungary");
        
        Button btnContinue2 = (Button) solo.getView(com.example.owntejivoverseny.R.id.buttonContinue1);
        solo.clickOnView(btnContinue2);
        solo.assertCurrentActivity("wrong activity" , RegPage3Activity.class);
        
        EditText editTextHeight = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextHeight); 
        EditText editTextWeight = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextWeight);
        
        RadioButton rbIsMale = (RadioButton) solo.getView(com.example.owntejivoverseny.R.id.radioGroupisMale);
        solo.typeText(editTextHeight, "183");
        solo.typeText(editTextWeight, "89");
        solo.clickOnView(rbIsMale);
        
        Button btnContinue3 = (Button) solo.getView(com.example.owntejivoverseny.R.id.buttonContinue2);
        solo.clickOnView(btnContinue3);
        solo.assertCurrentActivity("wrong activity" , ProfilActivity.class);
                   
       
        assertEquals("Nemeth Csaba", ((TextView) solo.getView(com.example.owntejivoverseny.R.id.textViewName)).getText().toString());
        assertEquals("38 �v", ((TextView) solo.getView(com.example.owntejivoverseny.R.id.textViewAge)).getText().toString());
        assertEquals("Budapest", ((TextView) solo.getView(com.example.owntejivoverseny.R.id.textViewCity)).getText().toString());
        assertEquals("Hungary", ((TextView) solo.getView(com.example.owntejivoverseny.R.id.textViewCountry)).getText().toString());
        assertEquals("183 cm", ((TextView) solo.getView(com.example.owntejivoverseny.R.id.textViewHeight)).getText().toString());
        assertEquals("89 kg", ((TextView) solo.getView(com.example.owntejivoverseny.R.id.textViewWeight)).getText().toString());
        assertEquals("Male", ((TextView) solo.getView(com.example.owntejivoverseny.R.id.textViewSex)).getText().toString());
      
        
        Button btnLogOut = (Button) solo.getView(com.example.owntejivoverseny.R.id.buttonLogOut);
        solo.clickOnView(btnLogOut);

        editTextUser = (EditText) solo.getView(com.example.owntejivoverseny.R.id.editTextUswerName);
        Log.i("BaseTest", "Sz�veg: " + (editTextUser.getText().toString()));
        assertEquals("", (editTextUser.getText().toString()));
        
       
        solo.sendKey(KeyEvent.KEYCODE_BACK);

    }

}
