package com.divetto.localservice_one;

import java.util.Arrays;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class LocalService extends Service {
    private static final String TAG = "LocalService";
    public static final String BROADCAST_ACTION = "broadcast_action_of_primes";
    public static final String BROADCAST_DATA_NUMBER = "nubmber_of_primes";
    private boolean[] primes;
    private int numberOfPrimes;

    @Override
    public void onStart(Intent intent, int startId) {
	Log.e(TAG, "onStart");
	super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
	Log.e(TAG, "onStartCommand");

	new Thread() {
	    public void run() {
		// This is how can you start a Service in a separate Thread...
		findPrime();
		Intent outintent = new Intent(BROADCAST_ACTION).putExtra(BROADCAST_DATA_NUMBER, numberOfPrimes);
		LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(outintent);

	    };
	}.start();

	return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
	Log.e(TAG, "onCreate");
	super.onCreate();
    }

    @Override
    public void onDestroy() {
	Log.e(TAG, "onDestory");
	super.onDestroy();
    }

    private void findPrime() {
	// will contain true or false values for the first 10,000 integers
	primes = new boolean[500];
	// set up the primesieve
	fillSieve();
	for (int i = 0; i < primes.length; i++) {
	    Log.d("isPrime:SERVICE" + i, String.valueOf(primes[i]));
	    if (!primes[i]) {
		++numberOfPrimes;
	    }
	}

	try {

	    Thread.sleep(7000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }

    public void fillSieve() {
	Arrays.fill(primes, true); // assume all integers are prime.
	primes[0] = primes[1] = false; // we know 0 and 1 are not prime.
	for (int i = 2; i < primes.length; i++) {
	    // if the number is prime,
	    // then go through all its multiples and make their values false.
	    if (primes[i]) {
		for (int j = 2; i * j < primes.length; j++) {
		    primes[i * j] = false;
		    Log.d("calculating_SERVICE:" + i, "doing....");
		}
	    }
	}
    }

    public boolean isPrime(int n) {
	return primes[n]; // simple, huh?
    }

    @Override
    public IBinder onBind(Intent intent) {
	// TODO Auto-generated method stub
	return null;
    }
}
