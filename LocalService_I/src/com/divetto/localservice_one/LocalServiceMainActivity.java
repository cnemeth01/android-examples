package com.divetto.localservice_one;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

public class LocalServiceMainActivity extends Activity {

    private BroadcastReceiver br = new BroadcastReceiver() {

	@Override
	public void onReceive(Context context, Intent intent) {
	    if (intent.getAction().equalsIgnoreCase(LocalService.BROADCAST_ACTION)) {
		Toast.makeText(LocalServiceMainActivity.this,
			String.valueOf(intent.getExtras().get(LocalService.BROADCAST_DATA_NUMBER)), Toast.LENGTH_SHORT)
			.show();
	    }
	}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_local_service_main);

	IntentFilter filter = new IntentFilter(LocalIntentService.BROADCAST_ACTION);
	LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(br, filter);

	 startINTENT_SERVICE();

	// mind that: Service started on UI thread by default
	startNormalService();

	TextView tw = (TextView) findViewById(R.id.mtw);
	tw.setText("Kalapacs");

    }

    private void startNormalService() {
	Intent mServiceIntent = new Intent(this, LocalService.class);
	startService(mServiceIntent);
    }

    private void startINTENT_SERVICE() {
	Intent mServiceIntent = new Intent(this, LocalIntentService.class);
	startService(mServiceIntent);

    }

    @Override
    protected void onDestroy() {
	LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(br);

	super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.local_service_main, menu);
	return true;
    }

}
