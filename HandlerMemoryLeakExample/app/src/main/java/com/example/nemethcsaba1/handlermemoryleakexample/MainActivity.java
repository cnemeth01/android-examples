package com.example.nemethcsaba1.handlermemoryleakexample;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

import java.lang.ref.WeakReference;


public class MainActivity extends Activity {

    public static final String MESSAGE_KEY = "message key";
    public static final int TEST_MESSAGE_KIND = 123;
    private static final String TAG = "MainActivity";
    TextView mainText;

    private Handler myHandler = new MyHandler(this);

    private final Runnable myRunnable = new MyRunnable(myHandler);


//    private final Handler myHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            Log.e(TAG, "handler getmessage: " + msg.getData().getString(MESSAGE_KEY));
//            if (msg.what == TEST_MESSAGE_KIND) {
//                if (msg.getData() != null) {
//                    Bundle bundle = msg.getData();
//                    if (bundle.getString(MESSAGE_KEY) != null) {
//                        String message = bundle.getString(MESSAGE_KEY);
//                        mainText.setText(message);
//                    }
//                }
//            }
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainText = (TextView) findViewById(R.id.main_text);

        sendMessageVersionOne();
    }

    private void sendMessageVersionOne() {
//        myHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Message message = new Message();
//                Bundle bundle = new Bundle();
//                bundle.putString(MESSAGE_KEY, "message from a bottle");
//                message.setData(bundle);
//                message.what = TEST_MESSAGE_KIND;
//                myHandler.sendMessage(message);
//            }
//        }, 1000 * 5);
        myHandler.postDelayed(myRunnable, 1000 * 5);

        /*finish();*/
    }


    private static class MyHandler extends Handler {
        WeakReference<MainActivity> weakReference;

        public MyHandler(MainActivity mainActivity) {
            weakReference = new WeakReference<MainActivity>(mainActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity activity = weakReference.get();
            if (activity != null) {
                if (msg.what == TEST_MESSAGE_KIND) {
                    if (msg.getData() != null) {
                        Bundle bundle = msg.getData();
                        if (bundle.getString(MESSAGE_KEY) != null) {
                            String message = bundle.getString(MESSAGE_KEY);
                            activity.mainText.setText(message);
                        }
                    }
                }
            }
        }
    }

    private class MyRunnable implements Runnable {

        WeakReference<Handler> handlerWeakReference;

        public MyRunnable(Handler myHandler) {
            handlerWeakReference = new WeakReference<>(myHandler);
        }

        @Override
        public void run() {
            Message message = new Message();
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_KEY, "message from a bottle");
            message.setData(bundle);
            message.what = TEST_MESSAGE_KIND;
            myHandler.sendMessage(message);
        }
    }
}

