package com.example.csabatodoapp;


import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;



public class MainActivity extends Activity {

	List<ListItem> todo;
	private todoDAO db;
	private Adapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		db=new todoDAO(this);
		
		
		todo=db.getAllItems();
		
		
		adapter = new Adapter(this,todo);
		ListView listView = (ListView) findViewById(R.id.listView1);
		listView.setAdapter(adapter);
		registerForContextMenu(listView);
		
		listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3) {
                    ListItem li= (ListItem) arg0.getItemAtPosition(arg2);    
                   
              
                    Intent intent = new Intent(getApplicationContext(),ItemActivity.class);
               		intent.putExtra("todo_index", li);
               		startActivity(intent); 
                    
            }
            
            
        });
		
//		
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		
		boolean valasztva=false;		
		if (item.getItemId()==R.id.contects_new) {			
			 Intent intent = new Intent(getApplicationContext(),UjItemActivity.class);
			 startActivityForResult(intent,1); 
			valasztva=true;
		}
		return valasztva;
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		
		getMenuInflater().inflate(R.menu.context, menu);
	}
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		int selelected = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
		
		boolean valasztva=false;		
		if (item.getItemId()==R.id.contects_delet) {			
			db.deleteItem(todo.get(selelected));
			todo.remove(selelected);
			adapter.notifyDataSetChanged();
			Toast toast=Toast.makeText(getApplicationContext(), "Item Deleteted", Toast.LENGTH_LONG);
			toast.show();
			valasztva=true;
		}
		return valasztva;
	}
	
	

		

}
