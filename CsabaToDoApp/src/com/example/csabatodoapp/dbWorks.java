package com.example.csabatodoapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class dbWorks extends SQLiteOpenHelper {

	private static final String DB_NAME = "csabitodo1.db";
	private static final int DB_VERSION = 1;

	private static final String DB_CREATE = "CREATE TABLE todo (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
			"megnevezes TEXT, hatarido_ev INTEGER, hatarido_honap INTEGER, hatarido_nap INTEGER, fontossag INTEGER)";
	
	public dbWorks(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase datab) {
		
		datab.execSQL(DB_CREATE);
		datab.execSQL("INSERT INTO todo (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES ('Vizsga', 2014,04,03,1)");
		datab.execSQL("INSERT INTO todo (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES ('Bev�s�rl�s', 2014,04,05,0)");
		datab.execSQL("INSERT INTO todo (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES ('Suli', 2014,04,06,2)");
		datab.execSQL("INSERT INTO todo (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES ('Mel�', 2014,04,03,0)");
		datab.execSQL("INSERT INTO todo (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES ('Pihen�s', 2014,04,05,0)");
		datab.execSQL("INSERT INTO todo (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES ('Pia', 2014,04,06,1)");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase datab, int arg1, int arg2) {
		datab.execSQL("DROP TABLE todo");
		onCreate(datab);
		
	}

}
