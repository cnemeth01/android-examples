package com.example.csabatodoapp;



import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class todoDAO {

	private SQLiteDatabase datab;
	private dbWorks dbWorks;
	
	
	
	public todoDAO(SQLiteDatabase datab,
			com.example.csabatodoapp.dbWorks dbWorks) {
		super();
		this.datab = datab;
		this.dbWorks = dbWorks;
	}

	public todoDAO(Context context) {
		dbWorks= new dbWorks(context);
		datab = dbWorks.getWritableDatabase();
		
	}
	
	public void close() {
		datab.close();
	}
	
	public List<ListItem> getAllItems() {
		List<ListItem> list = new ArrayList<ListItem>();
		Cursor cursor = datab.rawQuery("SELECT * FROM todo", null);
		while ( cursor.moveToNext()) {
			ListItem li = new ListItem(cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getInt(3),cursor.getInt(4),cursor.getInt(5));
			list.add(li);
		}
		
		return list;
	}
	public void deleteItem(ListItem li) {
		datab.execSQL("DELETE FROM todo WHERE _id=?", new Integer[]{li.getId()});
	}
	
	public void saveItem(ListItem li) {
			datab.execSQL("INSERT INTO todo (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES (?,?,?,?,?)", new Object[]{li.getMegnevezes(),li.getHatarido_ev(),li.getHatarido_honap()
					,li.getHatarido_nap(),li.getFontossag()});
	}
}
