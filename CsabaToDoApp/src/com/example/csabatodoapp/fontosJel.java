package com.example.csabatodoapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class fontosJel extends View {
	 private RectF ballBounds;      // Needed for Canvas.drawOval  
	   private Paint paint;   
	   private int fontos;
	   private float ballRadius = 24; // Ball's radius  
	   private float ballX = ballRadius+5 ;  // Ball's center (x,y)  
	   private float ballY = ballRadius + 15; 
	   ListItem item=new ListItem();
	   
	   
	   public int getFontos() {
		return fontos;
	}
	public void setFontos(int fontos) {
		this.fontos = fontos;
	}
	public fontosJel(Context context, AttributeSet attrs) {
		super(context, attrs);
		 ballBounds = new RectF();  
	      paint = new Paint();
	      
	      //to enable keypad  
	     
	      
	}
	   @Override
	protected void onDraw(Canvas canvas) {
		   ballBounds.set(ballX-ballRadius, ballY-ballRadius, ballX+ballRadius, ballY+ballRadius);  
		   if (getFontos()==0) {
			   paint.setColor(Color.GRAY);  
		} if (getFontos()==1) {
			   paint.setColor(Color.DKGRAY);  
		} if (getFontos()==2) {
			   paint.setColor(Color.RED);  
		}
		   
		      canvas.drawOval(ballBounds, paint);  
		
	}

}
