package com.example.csabatodoapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.widget.TextView;

public class ItemActivity extends Activity {
	Intent result;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		result=getIntent();
		ListItem li=(ListItem)result.getSerializableExtra("todo_index");
		
		setContentView(R.layout.item);
		TextView megnev=(TextView)findViewById(R.id.tenniItemText);
		megnev.setText(li.getMegnevezes());
		TextView ido=(TextView)findViewById(R.id.idoItemText);
		ido.setText(li.getHatarido_ev()+"."+li.getHatarido_honap()+"."+li.getHatarido_nap()+".");
		
		TextView fontos=(TextView)findViewById(R.id.fonosItemText);
		if (li.getFontossag()==0) {
			fontos.setText("Szarni r�");
		}if (li.getFontossag()==1) {
			fontos.setText("El�g fontos");
		}if (li.getFontossag()==2) {
			fontos.setText("Csin�ld!");
		}
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.item, menu);
		return true;
	}

}
