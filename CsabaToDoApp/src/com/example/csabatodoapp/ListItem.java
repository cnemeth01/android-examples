package com.example.csabatodoapp;

import java.io.Serializable;

public class ListItem implements Serializable {

	
 public String megnevezes;
 public int hatarido_ev;
 public int hatarido_honap;
 public int hatarido_nap;
 public int fontossag;
 public int id;
 
 
public ListItem(String megnevezes, int hatarido_ev, int hatarido_honap,
		int hatarido_nap, int fontossag) {
	super();
	this.megnevezes = megnevezes;
	this.hatarido_ev = hatarido_ev;
	this.hatarido_honap = hatarido_honap;
	this.hatarido_nap = hatarido_nap;
	this.fontossag = fontossag;
}


public ListItem(int id,String megnevezes, int hatarido_ev, int hatarido_honap,
		int hatarido_nap, int fontossag ) {
	super();
	this.megnevezes = megnevezes;
	this.hatarido_ev = hatarido_ev;
	this.hatarido_honap = hatarido_honap;
	this.hatarido_nap = hatarido_nap;
	this.fontossag = fontossag;
	this.id=id;
}


public ListItem() {
	super();
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getMegnevezes() {
	return megnevezes;
}
public void setMegnevezes(String megnevezes) {
	this.megnevezes = megnevezes;
}
public int getHatarido_ev() {
	return hatarido_ev;
}
public void setHatarido_ev(int hatarido_ev) {
	this.hatarido_ev = hatarido_ev;
}
public int getHatarido_honap() {
	return hatarido_honap;
}
public void setHatarido_honap(int hatarido_honap) {
	this.hatarido_honap = hatarido_honap;
}
public int getHatarido_nap() {
	return hatarido_nap;
}
public void setHatarido_nap(int hatarido_nap) {
	this.hatarido_nap = hatarido_nap;
}
public int getFontossag() {
	return fontossag;
}
public void setFontossag(int fontossag) {
	this.fontossag = fontossag;
}
 
 
}
