package com.example.csabatodoapp;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;



public class Adapter extends ArrayAdapter<ListItem>{
	
	public Adapter(Context context, List<ListItem> objects) {
		super(context, R.layout.listview, objects);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListItem li=getItem(position);
		if (convertView==null) {
			convertView=LayoutInflater.from(getContext()).inflate(R.layout.listview, null);
		}
		TextView itemToDo=(TextView) convertView.findViewById(R.id.toDoText);
		TextView itemDate=(TextView) convertView.findViewById(R.id.dateText );
		fontosJel itemFonos=(fontosJel) convertView.findViewById(R.id.fontosJel1);
		itemToDo.setText(li.getMegnevezes());
		itemDate.setText(li.getHatarido_ev()+"."+li.getHatarido_honap()+"."+li.getHatarido_nap()+".");
		itemFonos.setFontos(li.getFontossag());
		return convertView;
		
	}
}
