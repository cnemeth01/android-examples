package com.example.owncounterwithservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	private TextView textTime;

	private BroadcastReceiver responsReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			textTime = (TextView) findViewById(R.id.textViewTime);
			String text = intent.getStringExtra(CountService.COUNT_SERVICE_EXTRA_TIME);

			System.out.println("received text: " + text);
			textTime.setText(text);

		}

	};
	private Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		IntentFilter filter = new IntentFilter(CountService.ACTION_TIME_CHANGE);
		filter.addCategory(Intent.CATEGORY_DEFAULT);

		registerReceiver(responsReceiver, filter);

	}

	public void startService(View v) {
		intent = new Intent(this, CountService.class);
		startService(intent);
		System.out.println("pushed start button " );

	}
	public void stopService(View v) {
		stopService(intent);
		System.out.println("pushed stop button " );
	}
	@Override
	protected void onPause() {
		unregisterReceiver(responsReceiver);
		super.onPause();
	}
}
