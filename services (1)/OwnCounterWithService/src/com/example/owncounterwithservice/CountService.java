package com.example.owncounterwithservice;
import java.util.Date;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class CountService extends Service {
	
	public static final String ACTION_TIME_CHANGE= "action time change";
	private static final int ONE_SEC = 1000;
	public static final String COUNT_SERVICE_EXTRA_TIME = "count service extra time";
	public volatile boolean running = false;

	@Override
	public void onCreate() {
	 System.out.println("onCreate");
		super.onCreate();
	}
	
	public class CountThread extends Thread {

		
		@Override
		public void run() {
			while (running) {
				try {
					sleep(ONE_SEC);
				
				String currentTime =new Date(System.currentTimeMillis()).toString();
				Intent broadcastIntent =new Intent();
				broadcastIntent.setAction(CountService.ACTION_TIME_CHANGE);
				broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
				broadcastIntent.putExtra(COUNT_SERVICE_EXTRA_TIME, currentTime);
				sendBroadcast(broadcastIntent);
				System.out.println("broadcast sent");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (!running) {
			running=true;
			new CountThread().start();
			System.out.println("onStartCommand: startThread " );

		}
		
		// TODO Auto-generated method stub
		return super.onStartCommand(intent, flags, startId);
	}
	@Override
	public void onDestroy() {
		running=false;
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
