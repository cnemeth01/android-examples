package com.epam.alarmclock_ncsaba.adapters;

import java.util.ArrayList;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.epam.alarmclock_ncsaba.MainActivity;
import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.R.drawable;
import com.epam.alarmclock_ncsaba.alarm_manage.AlarmService;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;

public class MainAdapter extends ArrayAdapter<AlarmModel> {

	public ArrayList<AlarmModel> alarms;
	private Context context;
	private static final String ID = "alarm id";
	private static final String NAME = "alarm name";
	private static final String TIME_HOUR = "alarm hour";
	private static final String TIME_MINUTE = "al�arm minute";
	private static final String TONE = "alarm tone";

	public MainAdapter(Context context, int resource) {
		super(context, resource);
		this.alarms = MainActivity.alarms;
		this.context = context;
	}

	@Override
	public int getCount() {
		return this.alarms.size();
	}

	@Override
	public AlarmModel getItem(int position) {

		return this.alarms.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}
	private void handleAlarm(AlarmModel alarm) {
		AlarmManager alarmMng = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent intent = createPendingIntent(context, alarm);
		if (alarm.isActive()) {
			alarmMng.setRepeating(AlarmManager.RTC_WAKEUP, alarm.getTimInMilis(), 1000 * 60, intent);
			Log.d(MainAdapter.class.toString(),"Alarm set - "+ alarm.getAlarmTimeHour() +" - "+alarm.getAlarmTimeMinutes());
		} else {
			Log.d(MainAdapter.class.toString(),"Alarm canceled - "+ alarm.getAlarmTimeHour() +" - "+alarm.getAlarmTimeMinutes());
			alarmMng.cancel(intent);
		}
	
		
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		final AlarmModel alarm = alarms.get(position);

		if (convertView == null) {

			LayoutInflater nInflate = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = nInflate.inflate(R.layout.main_row, parent, false);
			holder = new ViewHolder();

			holder.textAlarmTime = (TextView) convertView.findViewById(R.id.tw_alarm_time);
			holder.textAlarmLabel = (TextView) convertView.findViewById(R.id.tw_alarm_label);
			holder.textAlarmRepeat = (TextView) convertView.findViewById(R.id.tw_alarm_days);
			holder.imageWakeUpType = (ImageView) convertView.findViewById(R.id.iw_wake_kind);
			holder.checkIsActive = (CheckBox) convertView.findViewById(R.id.cb_is_active);
			convertView.setTag(holder);
		} else {

			holder = (ViewHolder) convertView.getTag();
		}
		holder.textAlarmTime.setText(alarm.getAlarmTimeHour() + ":" + alarm.getAlarmTimeMinutes());
		holder.textAlarmLabel.setText(alarm.getLabel());
		holder.textAlarmRepeat.setText(alarm.getAlarmRepeatString());
		holder.checkIsActive.setChecked(alarm.isActive());
		holder.checkIsActive.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					alarm.setActive(true);
					handleAlarm(alarm);
				} else {
					alarm.setActive(false);
					handleAlarm(alarm);
				}

			}

		});

		switch (alarm.getWakeUpType()) {
			case BUTTON :
				holder.imageWakeUpType.setImageResource(drawable.ic_button);
				break;
			case SHAKE :
				holder.imageWakeUpType.setImageResource(drawable.ic_shake1);
				break;
			case MATH :
				holder.imageWakeUpType.setImageResource(drawable.ic_count);
				break;
			case PICTURE :
				holder.imageWakeUpType.setImageResource(drawable.ic_guess);
				break;

			default :

				holder.imageWakeUpType.setImageResource(drawable.ic_button);
				break;
		}

		return convertView;

	}

	static class ViewHolder {
		TextView textAlarmTime;
		TextView textAlarmRepeat;
		TextView textAlarmLabel;
		ImageView imageWakeUpType;
		CheckBox checkIsActive;

	}
	private static PendingIntent createPendingIntent(Context context, AlarmModel model) {
		Intent intent = new Intent(context, MainActivity.class);
		intent.putExtra(ID, model.getId());
		intent.putExtra(NAME, model.getLabel());
		intent.putExtra(TIME_HOUR, model.getAlarmTimeHour());
		intent.putExtra(TIME_MINUTE, model.getAlarmTimeMinutes());
		intent.putExtra(TONE, model.getAlarmtone());

		return PendingIntent.getActivity(context, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
	}
}
