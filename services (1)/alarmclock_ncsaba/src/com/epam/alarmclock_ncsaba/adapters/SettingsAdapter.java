package com.epam.alarmclock_ncsaba.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.epam.alarmclock_ncsaba.MainActivity;
import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;

public class SettingsAdapter extends BaseAdapter {

	private ArrayList<String[]> settings = new ArrayList<String[]>();


	public SettingsAdapter(int alarmPositionInList ) {
		AlarmModel alarmSetting = MainActivity.alarms.get(alarmPositionInList);

		if (alarmSetting == null) {
			settings.add(new String[]{"Válasszon egy elemet", "", null});
		} else {

			settings.add(new String[]{"Time", alarmSetting.getAlarmTimeHour() + ":" + alarmSetting.getAlarmTimeMinutes(), null});
			settings.add(new String[]{"Repeat", alarmSetting.getAlarmRepeatString(), null});
			settings.add(new String[]{"Label", alarmSetting.getLabel(), null});
			settings.add(new String[]{"Sound Type", alarmSetting.getSoundType() + "", null});
			settings.add(new String[]{"Dissmiss method", alarmSetting.getWakeUpType() + "", null});

		}
		
		
		
	}

	@Override
	public int getCount() {
		return settings.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;

		String[] set = settings.get(position);
//		Log.d(SettingsAdapter.class.toString(), "settings[] lenght" + set.length);

		if (convertView == null) {

			LayoutInflater nInflate = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = nInflate.inflate(R.layout.alarm_settings_row, parent, false);
			holder = new ViewHolder();

			holder.textSettingTitle = (TextView) convertView.findViewById(R.id.tw_setting_title);
			holder.textSetting = (TextView) convertView.findViewById(R.id.tw_setting);
			holder.checkbisActive = (CheckBox) convertView.findViewById(R.id.cb_setting_is_active);
			convertView.setTag(holder);
		} else {

			holder = (ViewHolder) convertView.getTag();
		}

		holder.textSettingTitle.setText(set[0]);
		holder.textSetting.setText(set[1]);

		if (set[2] != null) {
			holder.checkbisActive.setChecked(Boolean.valueOf(set[2]));
		} else
			holder.checkbisActive.setVisibility(View.GONE);

		return convertView;

	}

	static class ViewHolder {

		TextView textSettingTitle;
		TextView textSetting;
		CheckBox checkbisActive;

	}

}
