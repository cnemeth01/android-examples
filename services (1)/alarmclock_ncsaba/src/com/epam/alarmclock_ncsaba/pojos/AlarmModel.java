package com.epam.alarmclock_ncsaba.pojos;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import android.net.Uri;

public class AlarmModel implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private int alarmTimeHour;
	private int alarmTimeMinutes;
	private Uri Alarmtone;
	private Set<Days> alarmRepeat;
	private WakeUpType wakeUpType;
	private boolean isActive;
	private String label;
	private SoundType soundType;
	private String alarmRepeatString;

	public AlarmModel() {

		
		alarmRepeat = new HashSet<Days>();

		wakeUpType = WakeUpType.BUTTON;

		isActive = false;

		label = "Alarm";

	}

	public String getAlarmRepeatString() {

		StringBuilder sb = new StringBuilder();

		if (this.alarmRepeat.contains(Days.MONDAY))
			sb.append("mon, ");
		if (this.alarmRepeat.contains(Days.TUESDAY))
			sb.append("tue, ");
		if (this.alarmRepeat.contains(Days.WEDNESDAY))
			sb.append("wed, ");
		if (this.alarmRepeat.contains(Days.THUESDAY))
			sb.append("thu, ");
		if (this.alarmRepeat.contains(Days.FRIDAY))
			sb.append("fri, ");
		if (this.alarmRepeat.contains(Days.SATURDAY))
			sb.append("sat, ");
		if (this.alarmRepeat.contains(Days.SUNDAY))
			sb.append("sun, ");

		alarmRepeatString = sb.toString();

		return alarmRepeatString;
	}
	
	public long getTimInMilis(){
		
		Calendar cal =Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		cal.set(Calendar.HOUR_OF_DAY, alarmTimeHour);
		cal.set(Calendar.MINUTE, alarmTimeMinutes);
		cal.set(Calendar.SECOND, 0);
		
		return cal.getTimeInMillis();
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAlarmTimeHour() {
        return alarmTimeHour;
    }

    public void setAlarmTimeHour(int alarmTimeHour) {
        this.alarmTimeHour = alarmTimeHour;
    }

    public int getAlarmTimeMinutes() {
        return alarmTimeMinutes;
    }

    public void setAlarmTimeMinutes(int alarmTimeMinutes) {
        this.alarmTimeMinutes = alarmTimeMinutes;
    }

    public Uri getAlarmtone() {
        return Alarmtone;
    }

    public void setAlarmtone(Uri alarmtone) {
        Alarmtone = alarmtone;
    }

    public SoundType getSoundType() {
		return soundType;
	}

	public void setSoundType(SoundType soundType) {
		this.soundType = soundType;
	}

	

	public Set<Days> getAlarmRepeat() {
		return alarmRepeat;
	}

	public void addAlarmRepeat(Days day) {

		alarmRepeat.add(day);
	}

	public WakeUpType getWakeUpType() {
		return wakeUpType;
	}

	public void setWakeUpType(WakeUpType wakeUpType) {
		this.wakeUpType = wakeUpType;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
