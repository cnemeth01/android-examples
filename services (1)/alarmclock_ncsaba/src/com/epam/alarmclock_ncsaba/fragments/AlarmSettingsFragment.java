package com.epam.alarmclock_ncsaba.fragments;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TimePicker;

import com.epam.alarmclock_ncsaba.MainActivity;
import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.adapters.SettingsAdapter;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;
import com.epam.alarmclock_ncsaba.pojos.Days;

public class AlarmSettingsFragment extends Fragment {

	private static final int SET_ALARM_REPEAT_DAYS = 1;
	private static final int SET_ALARM_TIME = 0;
	private SettingsAdapter adapter;
	public static final String TAG = "AlarmSetFragment";
	private int alarmPositioninList = 0;

	public static final String ARG_SETTINGS = "settings";

	private AlarmModel alarm;
	private ListView settingList;
	private ArrayList<String[]> settings = new ArrayList<String[]>();

	public AlarmSettingsFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.alarm_settings, container, false);
		
		setRetainInstance(true);
//		if (savedInstanceState != null) {
//			alarmPositioninList = (Integer) savedInstanceState.get(ARG_SETTINGS);
//		}

		settingList = (ListView) rootView.findViewById(R.id.lv_settings);
		settingList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

				switch (position) {
					case SET_ALARM_TIME :
						Calendar mcurrentTime = Calendar.getInstance();
						int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
						int minute = mcurrentTime.get(Calendar.MINUTE);
						TimePickerDialog mTimePicker;

						mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
							@Override
							public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
								MainActivity.alarms.get(alarmPositioninList).setAlarmTimeHour(selectedHour);
								MainActivity.alarms.get(alarmPositioninList).setAlarmTimeMinutes(selectedMinute);
								initList(alarmPositioninList);
							}
						}, hour, minute, true);// Yes 24 hour time
						mTimePicker.setTitle("Select Time");
						mTimePicker.show();
						break;

					case SET_ALARM_REPEAT_DAYS :
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

						View daysChooser = inflater.inflate(R.layout.days_chooser, null);

						builder.setView(daysChooser).create();

						CheckBox chMon = (CheckBox) daysChooser.findViewById(R.id.checkBoxMon);
						CheckBox chTue = (CheckBox) daysChooser.findViewById(R.id.checkBoxTue);
						CheckBox chWed = (CheckBox) daysChooser.findViewById(R.id.checkBoxWed);
						CheckBox chThu = (CheckBox) daysChooser.findViewById(R.id.checkBoxThu);
						CheckBox chFri = (CheckBox) daysChooser.findViewById(R.id.checkBoxFri);
						CheckBox chSat = (CheckBox) daysChooser.findViewById(R.id.checkBoxSat);
						CheckBox chSun = (CheckBox) daysChooser.findViewById(R.id.checkBoxSun);

						builder.show();

						break;

					default :
						break;
				}

				// selfDatas = inflater.inflate(R.layout.self_datas, null);
				//
				// builder.setView(selfDatas).create();
				//
				// dialogImage = (ImageView)
				// selfDatas.findViewById(R.id.imageViewDialog);
				// dialogFirstname = (TextView)
				// selfDatas.findViewById(R.id.textViewFirsNameDialog);
				// dialogLastname = (TextView)
				// selfDatas.findViewById(R.id.textViewLastNameDialog);
				// dialogisMale = (TextView)
				// selfDatas.findViewById(R.id.textViewIsMaleDialog);
				// dialogMail = (TextView)
				// selfDatas.findViewById(R.id.textViewEmailDialog);
				// dialogCameraButton = (Button)
				// selfDatas.findViewById(R.id.buttonDialog);
				//
				// dialogCameraButton.setOnClickListener(new OnClickListener() {
				//
				// @Override
				// public void onClick(View v) {
				// Intent cameraIntent = new
				// Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				// startActivityForResult(cameraIntent, CAMERA_REQUEST);
				// }
				// });
				//
				// dialogFirstname.setText(pickedUser.getFirstName());
				// dialogLastname.setText(pickedUser.getLastName());
				// dialogMail.setText(pickedUser.getEmail());
				//
				// if (pickedUser.getIsMale()) {
				// dialogisMale.setText("Male");
				// } else
				// dialogisMale.setText("Female");
				//
				// builder.show();
			}

		});

		return rootView;
	}

	@Override
	public void onStart() {
		if (getArguments() != null) {
			alarmPositioninList = getArguments().getInt(ARG_SETTINGS);
		}
		initList(alarmPositioninList);

	}

	public void initList(int alarmPositioninList) {
		if (settingList.isActivated()) {

			settingList.removeAllViews();
			adapter = new SettingsAdapter(alarmPositioninList);
			settingList.setAdapter(adapter);

		} else {
			adapter = new SettingsAdapter(alarmPositioninList);
			settingList.setAdapter(adapter);
		}

		super.onStart();

	}
	public void updateSettingList(int positioinInList) {

		initList(positioinInList);

	}

}
