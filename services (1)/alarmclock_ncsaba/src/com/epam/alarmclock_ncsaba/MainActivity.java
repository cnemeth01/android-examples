package com.epam.alarmclock_ncsaba;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.epam.alarmclock_ncsaba.alarm_manage.AlarmService;
import com.epam.alarmclock_ncsaba.fragments.AlarmListFragment;
import com.epam.alarmclock_ncsaba.fragments.AlarmListFragment.IAlarmListFragmentCallBack;
import com.epam.alarmclock_ncsaba.fragments.AlarmSettingsFragment;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;
import com.epam.alarmclock_ncsaba.pojos.Days;
import com.epam.alarmclock_ncsaba.pojos.SoundType;
import com.epam.alarmclock_ncsaba.pojos.WakeUpType;

public class MainActivity extends Activity implements IAlarmListFragmentCallBack {

	
	private AlarmListFragment mainFragment;
	private AlarmSettingsFragment settingFragment;
	public static ArrayList<AlarmModel> alarms;
	private Thread myThread;





	static {
		alarms = new ArrayList<AlarmModel>();

		AlarmModel al1 = new AlarmModel();

		al1.addAlarmRepeat(Days.MONDAY);
		al1.addAlarmRepeat(Days.THUESDAY);
		al1.addAlarmRepeat(Days.WEDNESDAY);
		al1.addAlarmRepeat(Days.TUESDAY);
		al1.addAlarmRepeat(Days.FRIDAY);
		al1.setAlarmTimeHour(16);
		al1.setAlarmTimeMinutes(10);
		al1.setActive(false);
		al1.setWakeUpType(WakeUpType.MATH);
		al1.setLabel("Daily");
		al1.setSoundType(SoundType.DEFAULT);

		alarms.add(al1);

		AlarmModel al2 = new AlarmModel();

		al2.addAlarmRepeat(Days.THUESDAY);
		al2.addAlarmRepeat(Days.WEDNESDAY);
		al2.setAlarmTimeHour(5);
		al2.setAlarmTimeMinutes(00);
		al2.setActive(false);
		al2.setWakeUpType(WakeUpType.PICTURE);
		al2.setLabel("Runnung");
		al2.setSoundType(SoundType.RINGTONE);

		alarms.add(al2);

		AlarmModel al3 = new AlarmModel();

		al3.addAlarmRepeat(Days.SATURDAY);
		al3.setAlarmTimeHour(9);
		al3.setAlarmTimeMinutes(10);
		al3.setActive(false);
		al3.setWakeUpType(WakeUpType.BUTTON);
		al3.setLabel("Saturday");
		al3.setSoundType(SoundType.DEFAULT);

		alarms.add(al3);

		AlarmModel al4 = new AlarmModel();

		al4.addAlarmRepeat(Days.SUNDAY);
		al4.setAlarmTimeHour(16);
		al4.setAlarmTimeMinutes(05);
		al4.setActive(false);
		al4.setWakeUpType(WakeUpType.SHAKE);
		al4.setLabel("BBQ");
		al4.setSoundType(SoundType.VOICE);

		alarms.add(al4);

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main_fragment_container);

		if (savedInstanceState != null) {

			return;
		}
		mainFragment = (AlarmListFragment) getFragmentManager().findFragmentByTag(AlarmListFragment.TAG);
		if (mainFragment == null) {

			mainFragment = new AlarmListFragment();

			getFragmentManager().beginTransaction().replace(R.id.main_container, mainFragment, AlarmListFragment.TAG).commit();
		}

		if (findViewById(R.id.secondary_container) != null) {
			settingFragment = (AlarmSettingsFragment) getFragmentManager().findFragmentByTag(AlarmSettingsFragment.TAG);

			if (settingFragment == null) {

				Fragment newFragment = new AlarmSettingsFragment();

				FragmentTransaction transaction = getFragmentManager().beginTransaction();

				transaction.replace(R.id.secondary_container, newFragment, AlarmSettingsFragment.TAG);

				// Commit the transaction
				transaction.commit();
			}

		}
		

	}
	
	
	@Override
	public void newAlarm() {

	}
	@Override
	protected void onResume() {
		myThread = null;
		Runnable myRunnableThread = new CountDownRunner();
		myThread = new Thread(myRunnableThread);
		myThread.start();
		System.out.println("onResume: start Thread");
		super.onResume();
	}
	@Override
	protected void onPause() {
		
		myThread.interrupt();
		System.out.println("onPause: Thread Interrupt");
		super.onPause();
	}

	@Override
	public void alarmSettings(int position) {
		AlarmModel alarm =alarms.get(position);
		settingFragment = (AlarmSettingsFragment) getFragmentManager().findFragmentByTag(AlarmSettingsFragment.TAG);

		if (settingFragment != null) {
			settingFragment.updateSettingList(position);
		} else {

			Fragment newFragment = new AlarmSettingsFragment();
			Bundle args = new Bundle();

			args.putInt(AlarmSettingsFragment.ARG_SETTINGS, position);

			newFragment.setArguments(args);
			FragmentTransaction transaction = getFragmentManager().beginTransaction();

			transaction.replace(R.id.main_container, newFragment, AlarmSettingsFragment.TAG);

			transaction.addToBackStack(null);

			// Commit the transaction
			transaction.commit();
		}
	}
	public void doWork() {
		runOnUiThread(new Runnable() {
			public void run() {
				try {

					Calendar c = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
					String strDate = sdf.format(c.getTime());
					if (mainFragment != null) {
						mainFragment.setCurrentTime(strDate);
						System.out.println("Current Time:" + strDate);

					} else
						System.out.println("Current Time(fragment null):" + strDate);

				} catch (Exception e) {
				}
			}
		});
	}
	 public void newAlarmonClick(View v){
	    	
	    }
	    

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				} catch (Exception e) {
				}
			}
		}

	}
}
