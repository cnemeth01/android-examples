package com.example.fragmentstatic;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Use the
 * {@link FragmentMasik#newInstance} factory method to create an instance of
 * this fragment.
 * 
 */
public class FragmentMasik extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.fragment_fragment_masik, container,
				false);
		ImageView imageView = (ImageView)v.findViewById(R.id.kep);
		Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.kep2);
//		int dstWidth;
//		int dstHeight;
//		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bm, dstWidth, dstHeight, true)
		imageView.setImageBitmap(bm);
		return v;		
	}

}
