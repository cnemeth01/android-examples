package com.example.popup;

import com.example.popup.R.id;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;

public class MainActivity extends Activity {

	private Button popUp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		popUp=(Button) findViewById(id.toggleButton1);
	}

	public void onClick(View v){
		if (v.getId()==R.id.toggleButton1) {
			
			LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
			.getSystemService(LAYOUT_INFLATER_SERVICE);
	View popupView = layoutInflater.inflate(R.layout.popup, null);
	final PopupWindow popupWindow = new PopupWindow(popupView,
			LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

	Button btnDismiss = (Button) popupView.findViewById(R.id.btnPopUpOk);
	btnDismiss.setOnClickListener(new Button.OnClickListener() {
		public void onClick(View v) {
			popupWindow.dismiss();
		}
	});

	popupWindow.showAsDropDown(popUp, 100, -30);
			
		}
	}
}
