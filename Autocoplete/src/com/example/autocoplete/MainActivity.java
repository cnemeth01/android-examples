package com.example.autocoplete;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class MainActivity extends Activity {
	
	static final String[] cityNames = new String[] { "Budapest", "Bukarest","Krakk�", "B�cs" };
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		AutoCompleteTextView tv = (AutoCompleteTextView) 
		findViewById(R.id.autoCompleteTextView1);
		ArrayAdapter<String> cityAdapter =
		
				new ArrayAdapter<String>(this,
		android.R.layout.
		simple_dropdown_item_1line, cityNames);
		tv.setAdapter(cityAdapter);
		}

}
