package com.example.bouncingball;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

import android.view.View;  
  
public class BouncingBallActivity extends Activity {  
    /** Called when the activity is first created. */  
    @Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        //setContentView(R.layout.main);  
          
        View bouncingBallView = new BouncingBallView(this);  
        setContentView(bouncingBallView);  
    }  
}  
