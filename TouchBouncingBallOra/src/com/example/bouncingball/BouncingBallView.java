package com.example.bouncingball;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import android.content.Context;  
import android.graphics.Canvas;  
import android.graphics.Color;  
import android.graphics.Paint;  
import android.graphics.PointF;
import android.graphics.RectF;  
import android.view.KeyEvent;  
import android.view.MotionEvent;
import android.view.View;  
     
public class BouncingBallView extends View {  
   private int xMin = 0;          // This view's bounds  
   private int xMax;  
   private int yMin = 0;  
   private int yMax;  
   private float ballRadius = 40; // Ball's radius  
   private float ballX = ballRadius + 20;  // Ball's center (x,y)  
   private float ballY = ballRadius + 40;  
   private float ballSpeedX = 0;  // Ball's speed (x,y)  
   private float ballSpeedY = 0;  
   private RectF ballBounds;      // Needed for Canvas.drawOval  
   private Paint paint;           // The paint (e.g. style, color) used for drawing  
     
   private Map<Integer, PointF> circles = new HashMap<Integer, PointF>();
   
   // Constructor  
   public BouncingBallView(Context context) {  
      super(context);  
      ballBounds = new RectF();  
      paint = new Paint();  
        
      //to enable keypad  
      this.setFocusable(true);  
      this.requestFocus();  
   }  
   
   @Override
	public boolean onTouchEvent(MotionEvent event) {
	   int action = event.getActionMasked();
	   int pointerIndex = event.getActionIndex(); // kivel t�rt�nt lenyom�s/felenged�s
	   int pointerId = event.getPointerId(pointerIndex);
	   PointF pt = new PointF(event.getX(), event.getY());
	   switch (action) {
	   case MotionEvent.ACTION_DOWN: // els� ujj �rint�se
	   case MotionEvent.ACTION_POINTER_DOWN: // t�bbi ujjak �rint�se
		   circles.put(pointerId, pt);
		   return true;
	   case MotionEvent.ACTION_MOVE:
		   for (int i = 0; i < event.getPointerCount(); i++) {
			   pt = new PointF(event.getX(i), event.getY(i));
			   int pId = event.getPointerId(i);
			   circles.put(pId, pt);
		   }
		   return true;
	   case MotionEvent.ACTION_UP:
	   case MotionEvent.ACTION_POINTER_UP:
		   circles.remove(pointerId);
		   return true;
	   default:
		   return true;
	   }
	}



// Called back to draw the view. Also called by invalidate().  
   @Override  
   protected void onDraw(Canvas canvas) {  
      // Draw the ball  
      ballBounds.set(ballX-ballRadius, ballY-ballRadius, ballX+ballRadius, ballY+ballRadius);  
      paint.setColor(Color.GREEN);  
      canvas.drawOval(ballBounds, paint);  
          
      for (PointF p : circles.values()) {
    	  RectF circleBounds = new RectF(p.x-ballRadius, p.y-ballRadius, 
    			  p.x+ballRadius, p.y+ballRadius);
    	  paint.setColor(Color.MAGENTA);
    	  canvas.drawOval(circleBounds, paint);
      }
      
      // Update the position of the ball, including collision detection and reaction.  
      update();  
    
      // Delay  
      try {    
         Thread.sleep(60);    
      } catch (InterruptedException e) { }  
        
      invalidate();  // Force a re-draw  
   }  
     
   // Detect collision and update the position of the ball.  
   private void update() {  
	  float ballAccelX = 0;
	  float ballAccelY = 0;
	  for (PointF p : circles.values()) {
		  ballAccelX += p.x - ballX;
		  ballAccelY += p.y - ballY;
	  }
	  if (circles.size() > 0) {
		  ballAccelX = ballAccelX / 30 / circles.size();
		  ballAccelY = ballAccelY / 30 / circles.size();
	  }
	  ballSpeedX *= 0.98;
	  ballSpeedY *= 0.98;
	  ballSpeedX += ballAccelX;
	  ballSpeedY += ballAccelY;
      // Get new (x,y) position  
      ballX += ballSpeedX;  
      ballY += ballSpeedY;  
      // Detect collision and react  
      if (ballX + ballRadius > xMax) {  
         ballSpeedX = -ballSpeedX;  
         ballX = xMax-ballRadius;  
      } else if (ballX - ballRadius < xMin) {  
         ballSpeedX = -ballSpeedX;  
         ballX = xMin+ballRadius;  
      }  
      if (ballY + ballRadius > yMax) {  
         ballSpeedY = -ballSpeedY;  
         ballY = yMax - ballRadius;  
      } else if (ballY - ballRadius < yMin) {  
         ballSpeedY = -ballSpeedY;  
         ballY = yMin + ballRadius;  
      }  
   }  
     
   // Called back when the view is first created or its size changes.  
   @Override  
   public void onSizeChanged(int w, int h, int oldW, int oldH) {  
      // Set the movement bounds for the ball  
      xMax = w-1;  
      yMax = h-1;  
   }  

     
}  