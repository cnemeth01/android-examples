package com.example.fragmentmultiple;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link FragmentEgyik.OnFragmentInteractionListener} interface to handle
 * interaction events. Use the {@link FragmentEgyik#newInstance} factory method
 * to create an instance of this fragment.
 * 
 */
public class FragmentEgyik extends Fragment {

	public FragmentEgyik() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.fragment_fragment_egyik, container,
				false);
		return v;
	}


}
