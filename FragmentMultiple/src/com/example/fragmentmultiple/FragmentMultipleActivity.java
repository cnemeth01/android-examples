package com.example.fragmentmultiple;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.View;

public class FragmentMultipleActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragment_multiple);
	}
	
	public void elso(View view) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentMasik f2 = (FragmentMasik)fm.findFragmentByTag("FragmentMasik");
		f2.setKep(R.drawable.kep3);
	}
	
	public void masodik(View view) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentMasik f2 = (FragmentMasik)fm.findFragmentByTag("FragmentMasik");
		f2.setKep(R.drawable.kep4);
	}
	
	public void harmadik(View view) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentMasik f2 = (FragmentMasik)fm.findFragmentByTag("FragmentMasik");
		f2.setKep(R.drawable.kep5);
	}


}
