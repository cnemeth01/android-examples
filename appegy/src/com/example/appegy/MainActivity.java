package com.example.appegy;

import java.util.Random;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private int gessNum=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button btnGenerat = (Button) findViewById(R.id.btnGenerate);
		Button gess=(Button) findViewById(R.id.btnGuess);
		final TextView textField=(TextView) findViewById(R.id.textView1);
		final EditText editT=(EditText) findViewById(R.id.editText);
		
		btnGenerat.setOnClickListener(new OnClickListener() {
			
			
			@Override
			public void onClick(View v) {
				Random r =new Random(System.currentTimeMillis());
				
				gessNum=r.nextInt(21);
				Toast.makeText(MainActivity.this, ""+gessNum, Toast.LENGTH_LONG).show();
				
			}
		});
			gess.setOnClickListener(new OnClickListener() {
				
				private int text;

				@Override
				public void onClick(View v) {
					
					if (!editT.getText().toString().isEmpty()) {
						text = Integer.parseInt(editT.getText().toString());
					
					
					if (text==gessNum) {
						textField.setText(R.string.success);
						
					}else if (text>gessNum) {
						textField.setText(R.string.bigger);
					}else if (text<gessNum) {
						textField.setText(R.string.smaller);
					}
					
					Log.d("TEST", "Guess gomb megnyomva");
					}
				}
			});
		
		
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_1:
			Toast.makeText(MainActivity.this, "action1 menu", Toast.LENGTH_LONG).show();
			break;
		case R.id.action_2:
			Toast.makeText(MainActivity.this, "action2 menu", Toast.LENGTH_LONG).show();
			break;

		default:
			break;
		}
		return true;
	}

	
	
}