package com.example.junior_google_map_ncsaba.fragment;


import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.junior_google_map_ncsaba.MainActivity;
import com.example.junior_google_map_ncsaba.adapter.OwnAdapter;
import com.example.junior_google_map_ncsaba.pojos.Locations;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsFragment extends MapFragment implements ICallBackToActivity {

	public static final String ARG_POSITION = "map_position";
	public static final String ARG_MARKERS = "map_markers";
	private GoogleMap map;
	private Integer position_in_array;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (savedInstanceState != null) {
			position_in_array = savedInstanceState.getInt(ARG_POSITION);
			addMarkers((ArrayList<Locations>) savedInstanceState.getSerializable(ARG_POSITION));
		}
		
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void listItemClicked(int position, Locations loc) {
		
		if (loc != null) {
			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(new LatLng(loc.getLat(),loc.getLongi()))
					.zoom(12).build();
			map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		}	
	}

	public void addMarkers(ArrayList<Locations> locs) {
		for (Locations loc : locs) {
			map.addMarker( new MarkerOptions().position(new LatLng(loc.getLat(), loc.getLongi())).title(loc.getName()));
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		Bundle args = getArguments();
		if (args != null) {
			listItemClicked(args.getInt(ARG_POSITION), null);
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);		
	}
	
	 @Override
	public void onResume() {
		initMap(); 
		super.onResume();
	}     
	
	 
	private void initMap() {
		if (map == null) {

			map = getMap();
			if (map != null) {

				map.setMyLocationEnabled(true); 
				map.getUiSettings().setCompassEnabled(true);// false to disable
				map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			

			}

		}
	}
	@Override
	public void onPause() {
		if (map != null) {
			map.setMyLocationEnabled(false);
			map.getUiSettings().setCompassEnabled(false);
		}
		
		
	
		super.onPause();
	}

}
