package com.example.junior_google_map_ncsaba;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.junior_google_map_ncsaba.fragment.ICallBackToActivity;
import com.example.junior_google_map_ncsaba.fragment.ListViewFragment;
import com.example.junior_google_map_ncsaba.fragment.MapsFragment;
import com.example.junior_google_map_ncsaba.google.CallBackInterface;
import com.example.junior_google_map_ncsaba.google.LocationService;
import com.example.junior_google_map_ncsaba.pojos.Locations;
import com.jsonrespone.dao.JsonResponseDao;

public class MainActivity extends Activity implements ICallBackToActivity,
		CallBackInterface {

	private MapsFragment mapFragment;
	public ListViewFragment listFragment;
	private ArrayList<Locations> markers = new ArrayList<Locations>();

	// public static ArrayList<Locations> DATA = new ArrayList<Locations>();
	//
	// static {
	//
	// DATA.add(new Locations("Szent L�szl� K�rh�z", LocType.HOSPITAL,
	// 47.4280143, 19.1646859));
	// DATA.add(new Locations("Alma Gy�gyszert�r", LocType.PHARMACY, 47.0,
	// 19.0));
	// DATA.add(new Locations("Eg�szs�gcentrum", LocType.HEALTH, 47.2, 19.1));
	// DATA.add(new Locations("Szent J�nos K�rh�z", LocType.HOSPITAL, 47.3,
	// 19.05));
	// DATA.add(new Locations("Gy�gyszert�r", LocType.PHARMACY, 47.02, 19.02));
	// DATA.add(new Locations("Zero K�rh�z", LocType.HOSPITAL, 47.04, 19.07));
	//
	// }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_container);

		if (findViewById(R.id.container) != null) {
			listFragment = new ListViewFragment();
			getFragmentManager().beginTransaction()
					.add(R.id.container, listFragment).commit();

		}
		new LocationService(this).start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void listItemClicked(int position, Locations loc) {

		listFragment = (ListViewFragment) getFragmentManager()
				.findFragmentById(R.id.mlistview);

		if (mapFragment != null) {
			((MapsFragment) mapFragment).listItemClicked(position, loc);
		} else {

			// Create fragment and give it an argument for the selected article
			Fragment newFragment = new MapsFragment();
			Bundle args = new Bundle();
			args.putInt(MapsFragment.ARG_POSITION, position);
			args.putSerializable(MapsFragment.ARG_MARKERS, markers);
			newFragment.setArguments(args);
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();

			// Replace whatever is in the fragment_container view with this
			// fragment,
			// and add the transaction to the back stack so the user can
			// navigate back
			transaction.replace(R.id.container, newFragment);
			transaction.addToBackStack(null);

			// Commit the transaction
			transaction.commit();
		}

	}

	@Override
	public void success(final List<JsonResponseDao> results) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// give data to ListFragment or ListAdapter
				if (results != null) {

					ArrayList<Locations> items = new ArrayList<Locations>();

					for (JsonResponseDao jsonR : results) {
						Locations loc=null;
						String type="";
						for (String s : jsonR.getTypes()) {
							if (s.equals(LocationService.HOSPITAL)) {
								type=LocationService.HOSPITAL;
								
							} else if (s.equals(LocationService.PHARMACY)) {
								if (type.isEmpty()) {
									type=LocationService.PHARMACY;
								}
								
							} else if (s.equals(LocationService.HEALTH)) {
								if (type.isEmpty()) {
									type=LocationService.HEALTH;
								}
							}else{
								if (type.isEmpty()) {
									type=LocationService.HEALTH;
								}
								
								
							}
							
						}
						loc = new Locations(jsonR.getName(),
								type, jsonR
										.getGeometry().getLocation()
										.getLat(), jsonR.getGeometry()
										.getLocation().getLng());
						items.add(loc);
					}

					listFragment.addListItems(items);
					markers.clear();
					markers.addAll(items);
				}
			}
		});
	}

}