package com.example.owntejivofragment.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.owntejivofragment.R;

public class MainFragment extends Fragment {

	public static boolean isLogin = true;
	public static final String TAG = "MainFragment";
	public ICallBackToActivity listener;
	private Fragment fragment;
	private FragmentTransaction fragmentTransaction;
	private Button btnLogin;
	private Button btnSignUp;

	@Override
	public void onAttach(Activity activity) {
		listener = (ICallBackToActivity) activity;
		super.onAttach(activity);
	}

	public MainFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootwiew = inflater.inflate(R.layout.fragment_main, container, false);

		btnSignUp = (Button) rootwiew.findViewById(R.id.button_signup);

		btnLogin = (Button) rootwiew.findViewById(R.id.button_login);

		btnSignUp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				isLogin = true;
				fragment = new EnterFragment();
				fragmentTransaction=getFragmentManager().beginTransaction().replace(R.id.Main_framlayout_container, fragment);
				fragmentTransaction.commit();

			}
		});
		btnLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				isLogin = false;
				fragmentTransaction=getFragmentManager().beginTransaction().replace(R.id.Main_framlayout_container, fragment);
				fragmentTransaction.commit();

			}
		});

		return rootwiew;
	}

	public interface ICallBackToActivity {
		public void onClickLogin();

	}

}
