package com.example.owntejivofragment.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.owntejivofragment.R;

public class EnterFragment extends Fragment {

	public static final String TAG = "EnterFragment";
	public static final String USER_NAME = "Username";
	private Button btnSignUp;
	private TextView txtTitle;
	private EditText edName;
	private EditText edPassword;
	private EditText edPasswordAgain;
	private String userName = "";

	public EnterFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_enter, container, false);
		
		btnSignUp = (Button) rootView.findViewById(R.id.buttonSignUp);
		txtTitle = (TextView) rootView.findViewById(R.id.textViewTitle);
		edName = (EditText) rootView.findViewById(R.id.editTextUswerName);
		edPassword = (EditText) rootView.findViewById(R.id.editTextPassword);
		edPasswordAgain = (EditText) rootView.findViewById(R.id.editTextPasswordAgain);

		if (MainFragment.isLogin) {
			btnSignUp.setText("Login");
			txtTitle.setText("Login");
			edPasswordAgain.setVisibility(View.INVISIBLE);
		} else {
			btnSignUp.setText("SignUp");
			txtTitle.setText("SignUp");
		}
		return rootView;
	}

}
