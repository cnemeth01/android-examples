package com.example.owntejivofragment;

import com.example.owntejivofragment.fragments.MainFragment;
import com.example.owntejivofragment.fragments.MainFragment.ICallBackToActivity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

public class MainActivity extends ActionBarActivity implements ICallBackToActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_container);
		
		if (savedInstanceState==null) {
			getSupportFragmentManager().beginTransaction().add(R.id.Main_framlayout_container, new MainFragment()).commit();
		}
	}
	
	
	public void onClickLogin(View v) {

	}
	public void onClickSignUp(View v) {

	}


	@Override
	public void onClickLogin() {
		// TODO Auto-generated method stub
		
	}

}
