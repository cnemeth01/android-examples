package com.example.owntejivoverseny;

import java.util.Random;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

public class ProfilActivity extends ActionBarActivity {

    private TextView txtName;
    private TextView txtNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        txtName = (TextView) findViewById(R.id.textViewName);
        txtNumber = (TextView) findViewById(R.id.textViewNumber);

        String userName = getIntent().getStringExtra(LoginActivity.USER_NAME);

        txtName.setText(userName);
        txtNumber.setText((randInt(10000, 200000)) + "");

    }

    public void onClickLogout(View v) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();

    }

    public static int randInt(int min, int max) {
        Random rand = new Random();

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

}
