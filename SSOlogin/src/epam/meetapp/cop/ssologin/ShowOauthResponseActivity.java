package epam.meetapp.cop.ssologin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ShowOauthResponseActivity extends Activity {

	public static final String PARAMETER_ACCESS_CODE = "access_code";
	public static final String PARAMETER_ACCESS_TOKEN = "access_token";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.oauth_results);
		Intent intent = getIntent();
		TextView accessTokenDecodedView = (TextView) findViewById(R.id.access_token_decoded);
		ImageView imageView = (ImageView) findViewById(R.id.thumbnail);
		User user = TokenDecoder.getUserFromToken(intent.getStringExtra(PARAMETER_ACCESS_TOKEN));
		if (user.getThumbNail() != null) {
			imageView.setImageBitmap(user.getThumbNailAsBitmap());
		}
		accessTokenDecodedView.setText(user.toString());
	}

	public void onClickGoMain(View view) {
		Intent intent = new Intent(ShowOauthResponseActivity.this,
				MainActivity.class);
		startActivity(intent);
		finish();
	}

}