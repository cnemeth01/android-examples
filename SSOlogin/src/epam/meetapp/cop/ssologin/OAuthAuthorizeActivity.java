package epam.meetapp.cop.ssologin;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.webkit.WebView;

public class OAuthAuthorizeActivity extends Activity {

	public static final String APP_RESOURCE_URI = "meetapp://app";
	public static final String AUTHORIZATION_ENDPOINT = "https://login-prod.epm-sso.projects.epam.com/adfs/oauth2/authorize";
	public static final String TOKEN_ENDPOINT = "https://login-prod.epm-sso.projects.epam.com/adfs/oauth2/token";
	public static final String OAUTH_CLIENT_ID = "oauth-client.epm-budn.meetapp";
	public static final String REDIRECT_URI = "meetapp://sso/callback";
	private WebView webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.oauth);
		initThreadPolicy();
		initSSL();
		setupWebView();
		String codeUrl = buildAccessCodeUri(APP_RESOURCE_URI);
		webView.loadUrl(codeUrl);
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void setupWebView() {
		webView = (WebView) findViewById(R.id.webView);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new MyWebViewClient(APP_RESOURCE_URI, this));
	}

	@SuppressLint("TrulyRandom")
	public static void initSSL() {
		TrustManager[] trustAllCertificate = new TrustManager[] { new X509TrustManager() {

			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(
					java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(
					java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };
		try {
			SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCertificate, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public static String buildAccessCodeUri(String resource) {
		StringBuilder sb = new StringBuilder();
		sb.append(AUTHORIZATION_ENDPOINT);
		sb.append("?client_id=" + urlEncode(OAUTH_CLIENT_ID));
		sb.append("&response_type=code");
		sb.append("&redirect_uri=" + urlEncode(REDIRECT_URI));
		sb.append("&resource=" + urlEncode(resource));
		Log.e("buildAccessCodeUri", sb.toString());
		return sb.toString();
	}

	public void startShowResultsActivity(String accessCode, String accessToken) {
		Intent intent = new Intent(OAuthAuthorizeActivity.this,ShowOauthResponseActivity.class);
		intent.putExtra(ShowOauthResponseActivity.PARAMETER_ACCESS_CODE,accessCode);
		intent.putExtra(ShowOauthResponseActivity.PARAMETER_ACCESS_TOKEN,accessToken);
		startActivity(intent);
		finish();
	}

	private static String urlEncode(String str) {
		try {
			return URLEncoder.encode(str, "utf-8");
		} catch (UnsupportedEncodingException ex) {
			return str;
		}
	}
	
	public void initThreadPolicy(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
	}
}
