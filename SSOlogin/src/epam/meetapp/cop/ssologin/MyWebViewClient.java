package epam.meetapp.cop.ssologin;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyWebViewClient extends WebViewClient {

	private String previousUrl = null;
	private static final String ACCESS_CODE_FRAGMENT = "code=";
	private String resourceUri;
	private OAuthAuthorizeActivity activity;
	
	public MyWebViewClient() {

	}
	
	public MyWebViewClient(String resourceUri, OAuthAuthorizeActivity activity) {
		this.resourceUri = resourceUri;
		this.activity = activity;
	}
	
	@Override
	public void onPageStarted(WebView view, String url, Bitmap favicon) {
		 if (urlContaintsAccessCode(url) && !url.equals(previousUrl)){
			Log.e("onPageStarted", url);
			previousUrl = url;
			String accessCode = getAccessCodeFromUrl(url);
			String accessToken = TokenManager.obtainToken(resourceUri,accessCode);
			view.stopLoading();
			activity.startShowResultsActivity(accessCode, accessToken);
         }
	}
	
	@Override
    public void onReceivedSslError (WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed();
    }
	
	private boolean urlContaintsAccessCode(String url){
		return url != null && url.contains(ACCESS_CODE_FRAGMENT);
	}
	
	private String getAccessCodeFromUrl(String url){
		return url.substring(url.indexOf(ACCESS_CODE_FRAGMENT) + ACCESS_CODE_FRAGMENT.length());
	}
	
}
