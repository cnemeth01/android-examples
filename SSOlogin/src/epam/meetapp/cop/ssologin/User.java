package epam.meetapp.cop.ssologin;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.annotations.SerializedName;

public class User {

	@SerializedName("commonname")
	private String name;
	@SerializedName("email")
	private String email;
	@SerializedName("http://epam.com/claims/title")
	private String title;
	@SerializedName("http://epam.com/claims/thumbnailphoto")
	private String thumbNailPhoto;
	@SerializedName("appid")
	private String appId;
	@SerializedName("auth_time")
	private String authenticationTime;

	public User() {

	}


	@Override
	public String toString() {
		return "User [name=" + name + ", email=" + email + ", title=" + title
				 + ", appId=" + appId
				+ ", authenticationTime=" + authenticationTime + "]";
	}
	
	public String getThumbNail(){
		return thumbNailPhoto;
	}
	
	public Bitmap getThumbNailAsBitmap(){
		/*BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;*/
		byte[] bytes = Base64.decode(thumbNailPhoto, Base64.DEFAULT);
		Bitmap bMap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
		return bMap;
	}

}
