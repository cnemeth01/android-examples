package epam.meetapp.cop.ssologin;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

import android.util.Log;

public class TokenManager {
	
	public static String obtainToken(String resourceUrl, String accessCode){
        try {
            URL url = new URL(OAuthAuthorizeActivity.TOKEN_ENDPOINT);
            HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(buildAccessTokenRequestBody(resourceUrl, accessCode));
            wr.flush();
            wr.close();
            Log.e("HTTP_STAT", "Status Code: " + conn.getResponseCode());
            InputStream is = conn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            JSONObject tokenObj = new JSONObject(response.toString());
            return tokenObj.getString("access_token");
        }
        catch (Exception ex){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            return sw.toString();
        }
    }
	
	public static String buildAccessTokenRequestBody(String resource, String code){
        StringBuilder sb = new StringBuilder();
        sb.append("client_id=" + urlEncode(OAuthAuthorizeActivity.OAUTH_CLIENT_ID));
        sb.append("&redirect_uri=" + urlEncode(OAuthAuthorizeActivity.REDIRECT_URI));
        sb.append("&code=" + code);
        sb.append("&grant_type=authorization_code");
        Log.e("AOuth", sb.toString());
        return sb.toString();
    }
	
    private static String urlEncode(String str){
        try{
            return URLEncoder.encode(str, "utf-8");
        }
        catch (UnsupportedEncodingException ex){
            return str;
        }
    }
}
