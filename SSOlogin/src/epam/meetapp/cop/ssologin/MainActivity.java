package epam.meetapp.cop.ssologin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onLoginClick(View view){
        Intent oauthIntent = new Intent(MainActivity.this, OAuthAuthorizeActivity.class);
        startActivity(oauthIntent);
        finish();
    }
    
    public void removeCookies(View view){
        CookieManager m = CookieManager.getInstance();
        m.removeAllCookie();
    }
    
}
