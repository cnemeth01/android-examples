package epam.meetapp.cop.ssologin;

import android.util.Base64;

import com.google.gson.Gson;

public class TokenDecoder {

	public static String decodeToken(String token) {
		try {
			String[] parts = token.split("\\.");
			String header = convertBase64ToJSON(parts[0]);
			String payload = convertBase64ToJSON(parts[1]);
			return header + " " + payload;
		} catch (Exception ex) {
			return "Cannot decode token!";
		}
	}

	public static User getUserFromToken(String token) {
		try {
			String[] parts = token.split("\\.");
			String payload = convertBase64ToJSON(parts[1]);
			User user = new Gson().fromJson(payload, User.class);
			return user;
		} catch (Exception ex) {
			return new User();
		}
	}

	public static String convertBase64ToJSON(String data) throws Exception {
		byte[] base64EncodedBytes = data.getBytes("UTF-8");
		byte[] decodedBytes = Base64.decode(base64EncodedBytes, Base64.DEFAULT);
		String jsonText = new String(decodedBytes, "UTF-8");
		return jsonText;
	}

}
