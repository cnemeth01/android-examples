package com.example.mymodule.test;

import java.util.Arrays;
import java.util.List;


/**
 * Created by csaba_bela_nemeth on 10/29/2014.
 */
public class FindItemsInteractorImlementation implements FindItemInteractor {
    @Override
    public void findItems(final OnFinshedListener listener) {
        listener.onFinished(createArrayList());
    }

    private List<String> createArrayList() {
        return Arrays.asList(
                "Item 1",
                "Item 2",
                "Item 3",
                "Item 4",
                "Item 5",
                "Item 6",
                "Item 7",
                "Item 8",
                "Item 9",
                "Item 10"
        );
    }
}
