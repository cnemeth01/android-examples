package com.example.mymodule.test;

import java.util.List;

/**
 * Created by csaba_bela_nemeth on 10/29/2014.
 */
public interface OnFinshedListener {

    void onFinished(List<String> items);
}
