package main;

/**
 * Created by csaba_bela_nemeth on 10/29/2014.
 */
public interface MainPresenter {

    public void onResume();

    public void onItemClicked(int position);


}
