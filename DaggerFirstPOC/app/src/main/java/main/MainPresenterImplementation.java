package main;

import java.util.List;

import com.example.mymodule.test.FindItemInteractor;
import com.example.mymodule.test.OnFinshedListener;

/**
 * Created by csaba_bela_nemeth on 10/29/2014.
 */
public class MainPresenterImplementation implements MainPresenter, OnFinshedListener {

    private MainView mainView;
    private FindItemInteractor findItemInteractor;

    public MainPresenterImplementation(MainView mainView, FindItemInteractor findItemInteractor) {
        this.mainView = mainView;
        this.findItemInteractor = findItemInteractor;
    }

    @Override
    public void onResume() {
        mainView.showProgress();
        findItemInteractor.findItems(this);

    }

    @Override
    public void onItemClicked(int position) {
        mainView.showMassage(String.format("Position %d clicked", position + 1));
    }

    @Override
    public void onFinished(List<String> items) {
        mainView.setItems(items);
        mainView.hideProgress();

    }
}
