package main;

import javax.inject.Singleton;

import moduls.AppModule;
import dagger.Module;
import dagger.Provides;
import com.example.mymodule.test.FindItemInteractor;

/**
 * Created by csaba_bela_nemeth on 10/29/2014.
 */

@Module(
        injects = MainActivity.class,
        addsTo = AppModule.class
)
public class MainModule {

    private MainView view;

    public MainModule(MainView view) {
        this.view = view;
    }

    @Provides @Singleton public MainView provideView(){
        return view;
    }

    @Provides @Singleton public MainPresenter providePeresenter(MainView loginView, FindItemInteractor findItemsInteractor){
        return new MainPresenterImplementation(view, findItemsInteractor);
    }


}
