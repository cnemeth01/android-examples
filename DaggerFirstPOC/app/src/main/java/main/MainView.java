package main;

import java.util.List;

/**
 * Created by csaba_bela_nemeth on 10/29/2014.
 */
public interface MainView {

    public void showProgress();

    public  void hideProgress();

    public void setItems(List<String> items);

    public void showMassage(String message);

}
