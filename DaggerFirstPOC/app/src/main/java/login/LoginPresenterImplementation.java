package login;

import android.util.Log;

import interactors.LoginInteractor;

/**
 * Created by csaba_bela_nemeth on 10/27/2014.
 */
public class LoginPresenterImplementation implements LoginPresenter,OnLoginFinishedListener  {

    private LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenterImplementation(LoginView loginView, LoginInteractor loginInteractor) {
        this.loginView = loginView;
        this.loginInteractor = loginInteractor;
    }

    @Override public void validateCredentials(String username, String password) {
        loginView.showProgress();
        Log.d(LoginPresenterImplementation.class.getName(),"validateCredentials called" );
        loginInteractor.login(username, password, this);
    }

    @Override public void onUsernameError() {
        loginView.setUsernameError();
        loginView.hideProgress();
    }

    @Override public void onPasswordError() {
        loginView.setPasswordError();
        loginView.hideProgress();
    }

    @Override public void onSuccess() {
        Log.d(LoginPresenterImplementation.class.getName(),"onSuccess called");
        loginView.navigateToHome();
    }
}
