package login;

/**
 * Created by csaba_bela_nemeth on 10/27/2014.
 */
public interface LoginView {

    public void showProgress();

    public void hideProgress();

    public void setUsernameError();

    public void setPasswordError();

    public void navigateToHome();
}
