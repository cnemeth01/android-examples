package login;

import javax.inject.Singleton;

import moduls.AppModule;
import dagger.Module;
import dagger.Provides;
import interactors.LoginInteractor;

/**
 * Created by csaba_bela_nemeth on 10/27/2014.
 */

@Module(
        injects = LoginActivity.class,
        addsTo = AppModule.class

)
public class LoginModule {

    private LoginView view;

    public LoginModule(LoginView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public LoginView provideView() {
        return view;
    }

    @Provides
    @Singleton
    public LoginPresenter providePresenter(LoginView loginView, LoginInteractor loginInteractor) {
        return new LoginPresenterImplementation(loginView, loginInteractor);
    }


}
