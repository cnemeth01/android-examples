package login;

/**
 * Created by csaba_bela_nemeth on 10/27/2014.
 */
public interface LoginPresenter {
    public void validateCredentials(String username, String password);
}
