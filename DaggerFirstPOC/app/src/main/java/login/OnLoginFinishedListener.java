package login;

/**
 * Created by csaba_bela_nemeth on 10/27/2014.
 */
public interface OnLoginFinishedListener {
    public void onUsernameError();

    public void onPasswordError();

    public void onSuccess();
}
