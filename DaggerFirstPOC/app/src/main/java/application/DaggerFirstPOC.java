package application;

import android.app.Application;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import moduls.AnalyticsManager;
import moduls.AppModule;
import dagger.ObjectGraph;
/**
 * Created by csaba_bela_nemeth on 10/27/2014.
 */
public class DaggerFirstPOC extends Application {

    private ObjectGraph objectGraph;

    @Inject
    AnalyticsManager analyticsManager;

    @Override
    public void onCreate() {
        super.onCreate();
        objectGraph=ObjectGraph.create(getModules().toArray());
        objectGraph.inject(this);
        analyticsManager.showToast();
    }
    private List<Object> getModules(){
        return Arrays.<Object>asList(new AppModule(this));
    }

    public ObjectGraph createScopedGraph(Object... modules) {
        return objectGraph.plus(modules);
    }

}
