package moduls;

import android.app.Application;
import android.widget.Toast;

/**
 * Created by csaba_bela_nemeth on 10/27/2014.
 */
public class AnalyticsManager {
    Application app;

    public AnalyticsManager(Application app) {
        this.app=app;
    }

    public void showToast(){
        Toast.makeText(app,"Hello Dagger",Toast.LENGTH_SHORT).show();
    }
}
