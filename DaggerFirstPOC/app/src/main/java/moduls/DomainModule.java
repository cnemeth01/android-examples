package moduls;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by csaba_bela_nemeth on 10/27/2014.
 */

@Module(complete = false,
        library = true
)
public class DomainModule {

    @Provides
    @Singleton
    public AnalyticsManager provideAnalyticsManager(Application app){
        return new AnalyticsManager(app);
    }

}
