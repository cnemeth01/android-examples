package moduls;

/**
 * Created by csaba_bela_nemeth on 10/27/2014.
 */

import android.app.Application;

import javax.inject.Singleton;

import application.DaggerFirstPOC;
import dagger.Module;
import dagger.Provides;
import interactors.InteractorsModule;

@Module(injects = {
        DaggerFirstPOC.class
        },
        includes = {
                DomainModule.class,
                InteractorsModule.class
        })
public class AppModule {

    private DaggerFirstPOC daggerFirstPOC;

    public AppModule(DaggerFirstPOC daggerFirstPOC) {
        this.daggerFirstPOC = daggerFirstPOC;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return daggerFirstPOC;
    }
}
