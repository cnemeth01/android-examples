package com.epam.daggerfirstpoc;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

import application.DaggerFirstPOC;
import dagger.ObjectGraph;


public abstract class BaseActivity extends Activity {

    private ObjectGraph objectGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       objectGraph=((DaggerFirstPOC)getApplication()).createScopedGraph(getModules().toArray());
       objectGraph.inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        objectGraph = null;
    }

    protected abstract List<Object> getModules();

}
