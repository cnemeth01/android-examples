package interactors;

import login.OnLoginFinishedListener;

/**
 * Created by csaba_bela_nemeth on 10/27/2014.
 */
public interface LoginInteractor {
    public void login(String username, String password, OnLoginFinishedListener listener);
}
