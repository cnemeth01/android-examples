package interactors;

import com.example.mymodule.test.FindItemInteractor;
import com.example.mymodule.test.FindItemsInteractorImlementation;

import dagger.Module;
import dagger.Provides;

/**
 * Created by csaba_bela_nemeth on 10/28/2014.
 */

@Module(
          library = true
)
public class InteractorsModule {

    @Provides public LoginInteractor provideLoginInteractor() {
        return new LoginInteractorImplementation();
    }

    @Provides public FindItemInteractor provideFindItemInteractor(){
        return new FindItemsInteractorImlementation();
    }
}
