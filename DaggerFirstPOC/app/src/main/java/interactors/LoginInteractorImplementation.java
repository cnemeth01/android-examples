package interactors;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import login.LoginPresenterImplementation;
import login.OnLoginFinishedListener;

/**
 * Created by csaba_bela_nemeth on 10/28/2014.
 */
public class LoginInteractorImplementation implements LoginInteractor {
    @Override
    public void login(final String username, final String password, final OnLoginFinishedListener listener) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            boolean error=false;

                if (TextUtils.isEmpty(password)){
                    listener.onPasswordError();
                    error=true;
                }else if (TextUtils.isEmpty(username)){
                    listener.onUsernameError();
                    error=true;
                }else if (error == false) {
                    listener.onSuccess();
                    Log.d(LoginPresenterImplementation.class.getName(),"onSuccess called");
                }
          }
        },2000);
    }
}
