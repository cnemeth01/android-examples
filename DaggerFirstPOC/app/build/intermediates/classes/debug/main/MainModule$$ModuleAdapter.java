// Code generated by dagger-compiler.  Do not edit.
package main;

import dagger.internal.Binding;
import dagger.internal.BindingsGroup;
import dagger.internal.Linker;
import dagger.internal.ModuleAdapter;
import dagger.internal.ProvidesBinding;
import java.util.Set;
import javax.inject.Provider;

/**
 * A manager of modules and provides adapters allowing for proper linking and
 * instance provision of types served by {@code @Provides} methods.
 */
public final class MainModule$$ModuleAdapter extends ModuleAdapter<MainModule> {
  private static final String[] INJECTS = { "members/main.MainActivity", };
  private static final Class<?>[] STATIC_INJECTIONS = { };
  private static final Class<?>[] INCLUDES = { };

  public MainModule$$ModuleAdapter() {
    super(main.MainModule.class, INJECTS, STATIC_INJECTIONS, false /*overrides*/, INCLUDES, true /*complete*/, false /*library*/);
  }

  /**
   * Used internally obtain dependency information, such as for cyclical
   * graph detection.
   */
  @Override
  public void getBindings(BindingsGroup bindings, MainModule module) {
    bindings.contributeProvidesBinding("main.MainView", new ProvideViewProvidesAdapter(module));
    bindings.contributeProvidesBinding("main.MainPresenter", new ProvidePeresenterProvidesAdapter(module));
  }

  /**
   * A {@code Binding<main.MainView>} implementation which satisfies
   * Dagger's infrastructure requirements including:
   *
   * Being a {@code Provider<main.MainView>} and handling creation and
   * preparation of object instances.
   */
  public static final class ProvideViewProvidesAdapter extends ProvidesBinding<MainView>
      implements Provider<MainView> {
    private final MainModule module;

    public ProvideViewProvidesAdapter(MainModule module) {
      super("main.MainView", IS_SINGLETON, "main.MainModule", "provideView");
      this.module = module;
      setLibrary(false);
    }

    /**
     * Returns the fully provisioned instance satisfying the contract for
     * {@code Provider<main.MainView>}.
     */
    @Override
    public MainView get() {
      return module.provideView();
    }
  }

  /**
   * A {@code Binding<main.MainPresenter>} implementation which satisfies
   * Dagger's infrastructure requirements including:
   *
   * Owning the dependency links between {@code main.MainPresenter} and its
   * dependencies.
   *
   * Being a {@code Provider<main.MainPresenter>} and handling creation and
   * preparation of object instances.
   */
  public static final class ProvidePeresenterProvidesAdapter extends ProvidesBinding<MainPresenter>
      implements Provider<MainPresenter> {
    private final MainModule module;
    private Binding<MainView> loginView;
    private Binding<com.example.mymodule.test.FindItemInteractor> findItemsInteractor;

    public ProvidePeresenterProvidesAdapter(MainModule module) {
      super("main.MainPresenter", IS_SINGLETON, "main.MainModule", "providePeresenter");
      this.module = module;
      setLibrary(false);
    }

    /**
     * Used internally to link bindings/providers together at run time
     * according to their dependency graph.
     */
    @Override
    @SuppressWarnings("unchecked")
    public void attach(Linker linker) {
      loginView = (Binding<MainView>) linker.requestBinding("main.MainView", MainModule.class, getClass().getClassLoader());
      findItemsInteractor = (Binding<com.example.mymodule.test.FindItemInteractor>) linker.requestBinding("com.example.mymodule.test.FindItemInteractor", MainModule.class, getClass().getClassLoader());
    }

    /**
     * Used internally obtain dependency information, such as for cyclical
     * graph detection.
     */
    @Override
    public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> injectMembersBindings) {
      getBindings.add(loginView);
      getBindings.add(findItemsInteractor);
    }

    /**
     * Returns the fully provisioned instance satisfying the contract for
     * {@code Provider<main.MainPresenter>}.
     */
    @Override
    public MainPresenter get() {
      return module.providePeresenter(loginView.get(), findItemsInteractor.get());
    }
  }
}
