package com.example.own_counter_ncsaba;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;

public class MainFragment extends Fragment {

	GridView gridView;
	EditText txNumber;
	IMainFragment listener;
	int result = 0;
	String numberOne = "";
	String numberTwo = "";
	char operator;
	StringBuilder sb = new StringBuilder();

	@Override
	public void onAttach(Activity activity) {
		listener = (IMainFragment) activity;
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_main, container, false);
		txNumber = (EditText) v.findViewById(R.id.et_input_number);
		gridView = (GridView) v.findViewById(R.id.gridview);
		String[] numbers = new String[] { "7", "8", "9", "+", "6", "5", "4",
				"-", "3", "2", "1", "*", "0", "C", "=", "/" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, numbers);

		gridView.setAdapter(adapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				switch (position) {
				case 0:
					sb.append("7");
					break;
				case 1:
					sb.append("8");
					break;
				case 2:
					sb.append("9");
					break;
				case 3:
					operator = '+';
					if (txNumber.getText().toString().length() != 0) {
						if (numberOne.length() == 0) {
							numberOne = txNumber.getText().toString();
							sb.setLength(0);
						} else {
							result = Integer.parseInt(txNumber.getText()
									.toString()) + Integer.parseInt(numberOne);
							sb.setLength(0);
							numberOne = result + "";
							sb.append(numberOne);
							result = 0;
							numberOne = "";
							txNumber.setText("");

						}
					}
					break;
				case 4:
					sb.append("6");
					break;
				case 5:
					sb.append("5");
					break;
				case 6:
					sb.append("4");
					break;
				case 7:
					operator = '-';
					if (txNumber.getText().toString().length() != 0) {
						if (numberOne.length() == 0) {
							numberOne = txNumber.getText().toString();
							sb.setLength(0);
						} else {
							result = Integer.parseInt(numberOne)
									- Integer.parseInt(txNumber.getText()
											.toString());
							sb.setLength(0);
							numberOne = result + "";
							sb.append(numberOne);
							result = 0;
							numberOne = "";
							txNumber.setText("");

						}
					}
					break;
				case 8:
					sb.append("3");
					break;
				case 9:
					sb.append("2");
					break;
				case 10:
					sb.append("1");
					break;
				case 11:
					operator = '*';
					if (txNumber.getText().toString().length() != 0) {
						if (numberOne.length() == 0) {
							numberOne = txNumber.getText().toString();
							sb.setLength(0);
						} else {
							result = Integer.parseInt(txNumber.getText()
									.toString()) * Integer.parseInt(numberOne);
							sb.setLength(0);
							numberOne = result + "";
							sb.append(numberOne);
							result = 0;
							numberOne = "";
							txNumber.setText("");

						}
					}
					break;
				case 12:
					if (txNumber.getText().toString().length() != 0) {
						sb.append("0");
					}
					break;
				case 13:
					sb.setLength(0);
					numberOne = "";
					result = 0;
					break;
				case 14:

					switch (operator) {
					case '+':
						if (txNumber.getText().toString().length() != 0) {
							if (numberOne.length() == 0) {
								numberOne = txNumber.getText().toString();
								sb.setLength(0);
							} else {
								int result = Integer.parseInt(txNumber
										.getText().toString())
										+ Integer.parseInt(numberOne);
								sb.setLength(0);
								numberOne = result + "";
								listener.onResult(result+"");
								sb.append(numberOne);
								numberOne = "";
								result = 0;
								txNumber.setText("");

							}
						}
						break;
					case '*':

						if (txNumber.getText().toString().length() != 0) {
							if (numberOne.length() == 0) {
								numberOne = txNumber.getText().toString();
								sb.setLength(0);
							} else {
								result = Integer.parseInt(txNumber.getText()
										.toString())
										* Integer.parseInt(numberOne);
								sb.setLength(0);
								numberOne = result + "";
								listener.onResult(result+"");
								sb.append(numberOne);
								result = 0;
								numberOne = "";
								txNumber.setText("");

							}
						}
						
						break;
					case '-':

						if (txNumber.getText().toString().length() != 0) {
							if (numberOne.length() == 0) {
								numberOne = txNumber.getText().toString();
								sb.setLength(0);
							} else {
								result = Integer.parseInt(numberOne)
										- Integer.parseInt(txNumber.getText()
												.toString());
								sb.setLength(0);
								numberOne = result + "";
								listener.onResult(result+"");
								sb.append(numberOne);
								result = 0;
								numberOne = "";
								txNumber.setText("");

							}
						}
						break;
					case '/':
						if (txNumber.getText().toString().length() != 0) {
							if (numberOne.length() == 0) {
								numberOne = txNumber.getText().toString();
								sb.setLength(0);

							} else {
								result = Integer.parseInt(numberOne)
										/ Integer.parseInt(txNumber.getText()
												.toString());

								sb.setLength(0);
								numberOne = result + "";
								listener.onResult(result+"");
								sb.append(numberOne);
								result = 0;
								numberOne = "";
								txNumber.setText("");
							}

						}
						break;
					default:

						break;
					}
					break;
				case 15:
					operator = '/';

					if (txNumber.getText().toString().length() != 0) {
						if (numberOne.length() == 0) {
							numberOne = txNumber.getText().toString();
							sb.setLength(0);

						} else {
							result = Integer.parseInt(numberOne)
									/ Integer.parseInt(txNumber.getText()
											.toString());

							sb.setLength(0);
							numberOne = result + "";
							sb.append(numberOne);
							result = 0;
							numberOne = "";
							txNumber.setText("");
						}

					}
					break;
				default:
					break;
				}
				txNumber.setText(sb);

			}
		});

		return v;
	}

	public interface IMainFragment {
		public void onResult(String s);
	}
}
