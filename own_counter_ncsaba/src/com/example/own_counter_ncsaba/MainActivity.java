package com.example.own_counter_ncsaba;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;


import com.example.own_counter_ncsaba.MainFragment.IMainFragment;

public class MainActivity extends Activity implements IMainFragment {

	public static final String RESULT = "result";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (findViewById(R.id.fragment_container) != null) {

			if (savedInstanceState != null) {
				return;
			}

			MainFragment firstFragment = new MainFragment();

			getFragmentManager().beginTransaction()
					.add(R.id.fragment_container, firstFragment).commit();

		}

	}

	@Override
	public void onResult(String resultString) {
		SecondFragment resultFragment = (SecondFragment) getFragmentManager()
				.findFragmentById(R.id.result_fragment);

		if (resultFragment != null) {
			resultFragment.updateText(resultString);
		} else {

			SecondFragment newFragment = new SecondFragment();
			Bundle args = new Bundle();
			args.putString(RESULT, resultString);
			newFragment.setArguments(args);
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();

			transaction.replace(R.id.fragment_container, newFragment);
			transaction.addToBackStack(null);

			transaction.commit();
		}
	}

}
