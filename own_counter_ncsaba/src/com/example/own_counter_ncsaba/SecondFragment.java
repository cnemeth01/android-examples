package com.example.own_counter_ncsaba;



import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SecondFragment extends Fragment{

	TextView txResult;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		  View v = inflater.inflate(R.layout.fragment_second, container, false);
	        txResult = (TextView) v.findViewById(R.id.tx_sec_result);
	        return v;
	}
	@Override
	public void onStart() {
		Bundle args = getArguments();
        if (args != null) {
          
            txResult.setText((args.getString(MainActivity.RESULT)));
        } else {
        	txResult.setText("no result yet");
        }
		super.onStart();
	}
	public void updateText(String result){
		txResult.setText(result);
	}
}
