/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.supersoftwares.myannottation;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.supersoftwares.myannottation";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
