package com.supersoftwares.myannottation;

import android.app.Application;

import annotations.CheckAnnotations;

/**
 * Created by Csaba_Bela_Nemeth on 6/2/2015.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        CheckAnnotations.checkAnnottations();

    }
}
