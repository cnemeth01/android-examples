package annotations;

import com.supersoftwares.myannottation.MainActivity;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Created by Csaba_Bela_Nemeth on 6/2/2015.
 */
public class CheckAnnotations {

    private static final String TAG = CheckAnnotations.class.getSimpleName();
    private static final String BASE_PACKAGE =
            "com/supersoftwares/myannotattion";
    public static boolean checkAnnottations() {

        Class<MainActivity> activity = MainActivity.class;
        Annotation[] annotations = activity.getAnnotations();

        for (Annotation annotation : annotations) {

            if (annotation instanceof NeedLogTAG) {
                for (Field f : activity.getDeclaredFields()) {
                    System.out.println(f.toString());
                    if (f.toString().contains("TAG")) {
                        return true;
                    }
                }
                throw new RuntimeException("How many times I have to tell you, you HAVE to add a static final String TAG to your Logs. Motherfucker!");
            } else {
                return false;
            }
        }
        return false;
    }

    private static void checkClasses() {

        ClassPathScanningCandidateComponentProvider scanner =
                new ClassPathScanningCandidateComponentProvider(true);

        scanner.addIncludeFilter(new AnnotationTypeFilter(NeedLogTAG.class));

        for (BeanDefinition bd : scanner.findCandidateComponents(BASE_PACKAGE)){

            System.out.println(bd.getBeanClassName());
        }
    }


}
