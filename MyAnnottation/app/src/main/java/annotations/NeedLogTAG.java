package annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Csaba_Bela_Nemeth on 6/2/2015.
 *
 * This check a CLASS has a private static String field named TAG for using log
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface NeedLogTAG {
}
