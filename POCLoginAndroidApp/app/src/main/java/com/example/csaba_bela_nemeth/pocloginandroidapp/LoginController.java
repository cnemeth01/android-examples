package com.example.csaba_bela_nemeth.pocloginandroidapp;

/**
 * Created by Csaba_Bela_Nemeth on 1/12/2015.
 */
public interface LoginController {

    public boolean isValidPassword(String password);
    public boolean isValidUserName(String username);
    public boolean isValidPassword(User user);
    public void onCreate();

}
