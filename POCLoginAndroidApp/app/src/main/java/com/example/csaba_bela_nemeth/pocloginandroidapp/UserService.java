package com.example.csaba_bela_nemeth.pocloginandroidapp;

/**
 * Created by Csaba_Bela_Nemeth on 1/12/2015.
 */
public interface UserService {

   public User getUserByUsername (String username);

}
