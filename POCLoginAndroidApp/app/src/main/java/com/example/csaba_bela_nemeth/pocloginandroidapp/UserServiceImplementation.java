package com.example.csaba_bela_nemeth.pocloginandroidapp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Csaba_Bela_Nemeth on 1/12/2015.
 */
public class UserServiceImplementation implements UserService {

    static final List<User> users= new ArrayList<>();

    static {
        users.add(new User("Csaba Nemeth","hhhhhhhh123"));
        users.add(new User("Gergo Pinter","pppppppp123"));
        users.add(new User("Peter Nagy","nnnnnnnn123"));
        users.add(new User("Petra Kiss","kkkkkkkk123"));
    }

    @Override
    public User getUserByUsername(String username) {
        for (User user : users) {
            if (user.getUsername().equals(username)){
                return user;

            }
        }

        return null;
    }


}
