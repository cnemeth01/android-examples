package com.example.csaba_bela_nemeth.pocloginandroidapp;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;


public class LoginActivity extends ActionBarActivity {

    private CustomLoginView customLoginView;
    private LoginController loginController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customLoginView=new CustomLoginView(this);
        loginController = new LoginControllerImplementation(new UserServiceImplementation(), customLoginView);
        setContentView(customLoginView);
        loginController.onCreate();
    }

}
