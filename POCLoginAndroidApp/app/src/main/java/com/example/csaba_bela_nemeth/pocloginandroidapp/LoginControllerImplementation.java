package com.example.csaba_bela_nemeth.pocloginandroidapp;

import android.view.View;

/**
 * Created by Csaba_Bela_Nemeth on 1/12/2015.
 */
public class LoginControllerImplementation implements LoginController {

    private UserService userService;
    private View loginView;


    public LoginControllerImplementation(UserService userService, View loginView ) {
        this.userService = userService;
        this.loginView = loginView;
    }

    @Override
    public boolean isValidPassword(String password) {
        return true;
    }

    @Override
    public boolean isValidUserName(String username) {
        return true;
    }

    @Override
    public boolean isValidPassword(User user) {
        User validUser = userService.getUserByUsername(user.getUsername());
        if (validUser != null) {
            return true;
        }

        return false;
    }

    @Override
    public void onCreate() {
        loginView

    }
}
