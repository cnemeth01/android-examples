package com.example.sulyregiszter_1user;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.sulyregiszter.R;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SulyActivity extends Activity {
	
	private EditText datumET;
	private EditText tenyET;
	private WebView webView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_suly);
		
		datumET = (EditText) findViewById(R.id.datumEditText);
		tenyET = (EditText) findViewById(R.id.tenyEditText);
		webView = (WebView) findViewById(R.id.webView);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.loadUrl("http://www.programozas-oktatas.hu/fogyas/webindex.php?u=gipszjakab");
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setInitialScale(50);
	    webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webView.getSettings().setUseWideViewPort(true);
		webView.requestFocus();
		//outputTV = (TextView) findViewById(R.id.outputTextView);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		datumET.setText(sdf.format(new Date()));
	}
	
	public void frissit(View view) {
		webView.reload();
	}
	
	public void elkuld(View view) {
		AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... arg0) {
				try {
					String urlString = "http://www.programozas-oktatas.hu/fogyas/insert.php";
					String urlParameters = "nev=gipszjakab&datum="+datumET.getText().toString()+
							"&teny="+tenyET.getText().toString();
					URL url = new URL(urlString);
					URLConnection conn = url.openConnection();
			
					conn.setDoOutput(true);
			
					OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			
					writer.write(urlParameters);
					writer.flush();
					String line;
					String output = "";
					BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					while ((line = reader.readLine()) != null) {
					    output += "\n" + line;
					}
					writer.close();
					reader.close();
					//return output;
					return "OK";
				} catch (IOException e) {
					Log.w("UPLOADER", e);
					return e.getMessage();
				}
			}
			
			@Override
			protected void onPostExecute(String result) {
				if ("OK".equals(result)) {
					Toast.makeText(getApplicationContext(), "Rendben", Toast.LENGTH_LONG).show();
					webView.reload();
				} else {
					Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
				}
			}
		};
		task.execute();
 
	}


}
