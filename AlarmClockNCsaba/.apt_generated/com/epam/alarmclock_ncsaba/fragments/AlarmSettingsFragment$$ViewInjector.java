// Generated code from Butter Knife. Do not modify!
package com.epam.alarmclock_ncsaba.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class AlarmSettingsFragment$$ViewInjector {
  public static void inject(Finder finder, final com.epam.alarmclock_ncsaba.fragments.AlarmSettingsFragment target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131099728, "field 'settingList'");
    target.settingList = (android.widget.ListView) view;
  }

  public static void reset(com.epam.alarmclock_ncsaba.fragments.AlarmSettingsFragment target) {
    target.settingList = null;
  }
}
