// Generated code from Butter Knife. Do not modify!
package com.epam.alarmclock_ncsaba.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class AlarmListFragment$$ViewInjector {
  public static void inject(Finder finder, final com.epam.alarmclock_ncsaba.fragments.AlarmListFragment target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131099731, "field 'textViewNoAlarm'");
    target.textViewNoAlarm = (android.widget.TextView) view;
  }

  public static void reset(com.epam.alarmclock_ncsaba.fragments.AlarmListFragment target) {
    target.textViewNoAlarm = null;
  }
}
