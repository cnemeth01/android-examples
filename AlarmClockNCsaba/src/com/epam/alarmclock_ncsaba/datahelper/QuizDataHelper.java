package com.epam.alarmclock_ncsaba.datahelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.pojos.QuizModel;

public class QuizDataHelper {

    private static List<QuizModel> pictureQuizes;
    private static List<QuizModel> mathQuizes;
    private static Set<Integer> randomSet=new HashSet<Integer>();
    private static int randomQuizNum;

    public static QuizModel getRandomPictureQuiz() {

        pictureQuizes = new ArrayList<QuizModel>();

        pictureQuizeStore();

        int randomNumber = getRandomNum(pictureQuizes.size());
        return pictureQuizes.get(randomNumber);
    }

    public static QuizModel getRandomMathQuiz() {

        mathQuizes = new ArrayList<QuizModel>();
        mathQuizeStore();

        int randomNumber = getRandomNum(mathQuizes.size());
        return mathQuizes.get(randomNumber);

    }

    private static int getRandomNum(int arraySize) {
        Random random = new Random();
        randomQuizNum = random.nextInt(arraySize);
        while (!randomSet.add(randomQuizNum)) {
            randomQuizNum = random.nextInt(arraySize);
        }

        return randomQuizNum;

    }

    private static void pictureQuizeStore() {

        pictureQuizes.add(new QuizModel("Who is he?", "E�tv�s L�r�nd", "Liszt Ferenc", "E�tv�s Cs�pi", "Albert Einstein", 'D',
                R.drawable.pic_einstein));
        pictureQuizes.add(new QuizModel("Who is he?", "Hom�r Simpson", "Kyle Brovslovski", "Bart Simpson", "nevermind", 'C',
                R.drawable.pic_bart_simpson));
        pictureQuizes.add(new QuizModel("Who is he?", "Eric Cartman", "Kyle Brovslovski", "Chef", "Bart Simpson", 'B', R.drawable.pic_kyle));
        pictureQuizes.add(new QuizModel("Who is he?", "Denzel Washington", "Mel Gibson", "Samuel L. Jackson", "Nelson Mandela", 'D',
                R.drawable.pic_mandela));
        pictureQuizes.add(new QuizModel("Who is he?", "XIV. Lajos", " John Lennon", "Napoleon", "Franco t�bornok", 'C', R.drawable.pic_napoleon));
        pictureQuizes.add(new QuizModel("Who is she?", "Sharon Stone", "Lady Diana", "Lady Bianca", "Diane Keaton", 'B', R.drawable.pic_lady_d));
        pictureQuizes.add(new QuizModel("Who is he?", "Al Pacino", "Charlie Chaplin", "Chopin", "Spencer Tracy", 'B', R.drawable.pic_charlie_ch));
        pictureQuizes.add(new QuizModel("Who is he?", "Hobo", "Cs�nyi S�ndor", "Ringo Star", "Paul Mccartney", 'D', R.drawable.pic_paul_mc));
        pictureQuizes.add(new QuizModel("Who is she?", "Agatha Christie", "Margaret Thatcher", "Merilyn Monroe", "Meryl Streep", 'B',
                R.drawable.pic_margaret_teacher));
        pictureQuizes.add(new QuizModel("Who is she?", "Omar Sharif", "Gandira Indie", "Marie Curie", "Indira Gandhi", 'D',
                R.drawable.pic_indira_gandi));
        pictureQuizes.add(new QuizModel("Who is he?", "Michael Douglas", "Bud Bundy", "Ed O'Neill", "Harrison Ford", 'C',
                R.drawable.pic_ed));
        pictureQuizes.add(new QuizModel("Who is he?", "Fernando Alonso", "Julio Cesar", "Sebastian Vettel", "Felipe Massa", 'D',
                R.drawable.pic_massa));
        pictureQuizes.add(new QuizModel("Who is he?", "Mr. Pi", "Rowan Atkinson", "Johnnie British", "Hugh Laurie", 'B',
                R.drawable.pic_rowan));
        pictureQuizes.add(new QuizModel("Who is she?", "Meryl Streep", "Goldie Hawn", "Kate Hudson", "Lenke n�ni", 'A',
                R.drawable.pic_meryl_streep));
         pictureQuizes.add(new QuizModel("Who is he?", "Dolph Lundgren", "Gordon Ramsey", "Jamie Oliver", "Michael Dudicoff", 'B',
                R.drawable.pic_remsey));
        pictureQuizes.add(new QuizModel("Who is he?", "Denzel Washington", "Mike Tyson", "Muhammad Ali", "Michael Jackson", 'C',
                R.drawable.pic_ali));
        pictureQuizes.add(new QuizModel("Who is he?", "Family Gay", "Hom�r Simpson", "Family Guy", "Eric Cartman", 'C',
                R.drawable.pic_family));
        pictureQuizes.add(new QuizModel("Who is he?", "Family Guy", "American Dad", "Bob Burger", "Clivland Brown", 'D',
                R.drawable.pic_clivland));
        pictureQuizes.add(new QuizModel("Who is he?", "Kyle Brovlovski", "Eric Cartmen", "Kenny McCormick", "Stan Marsh", 'B',
                R.drawable.pic_eric_cartman));
       
        
     
    }

    private static void mathQuizeStore() {

        mathQuizes.add(new QuizModel("6 * 9 =?", "51", "56", "54", "55", 'C', -1));
        mathQuizes.add(new QuizModel("5 * 12 =?", "59", "60", "57", "58", 'B', -1));
        mathQuizes.add(new QuizModel("8 * 13 =?", "104", "111", "108", "98", 'A', -1));
        mathQuizes.add(new QuizModel("7 * 14 =?", "97", "94", "98", "107", 'C', -1));
        mathQuizes.add(new QuizModel("4 * 23 =?", "92", "93", "94", "99", 'A', -1));
        mathQuizes.add(new QuizModel("6 * 21 =?", "124", "112", "126", "122", 'C', -1));
        mathQuizes.add(new QuizModel("17 * 3 =?", "57", "53", "55", "51", 'D', -1));
        mathQuizes.add(new QuizModel("4 * 37 =?", "144", "148", "140", "142", 'B', -1));
        mathQuizes.add(new QuizModel("12 + 17 =?", "31", "28", "29", "27", 'C', -1));
        mathQuizes.add(new QuizModel("11 + 23 =?", "34", "32", "44", "28", 'A', -1));
        mathQuizes.add(new QuizModel("9 + 27 =?", "31", "36", "37", "35", 'B', -1));
        mathQuizes.add(new QuizModel("17 + 19 =?", "35", "36", "33", "32", 'B', -1));
        mathQuizes.add(new QuizModel("13 + 34 =?", "39", "43", "47", "46", 'C', -1));
        mathQuizes.add(new QuizModel("7 + 47 =?", "54", "42", "55", "52", 'A', -1));
        mathQuizes.add(new QuizModel("28 - 17 =?", "12", "14", "10", "11", 'D', -1));
        mathQuizes.add(new QuizModel("39 - 24 =?", "13", "14", "15", "10", 'C', -1));
        mathQuizes.add(new QuizModel("52 - 37 =?", "17", "25", "5", "15", 'D', -1));
        mathQuizes.add(new QuizModel("25 - 17 =?", "9", "8", "10", "11", 'B', -1));
        mathQuizes.add(new QuizModel("69 - 17 =?", "53", "52", "54", "55", 'B', -1));
    }
}
