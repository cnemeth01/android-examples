package com.epam.alarmclock_ncsaba.datahelper;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.epam.alarmclock_ncsaba.pojos.AlarmModel;

public class AlarmDataHelper {

    private static final String KEY_LAST_SET_ALARM = "key last set alarm";
    private static final String LAST_SET_ALARM = "last set alarm";
    private static final String KEY_ALARM_LIST = "key_alarm_list";
    private static final String DB_ALARM_LIST = "alarms list";
    private static ArrayList<AlarmModel> alarms;
    private static int lastSetAlarmPositionInList = -1;

    public static void saveAlarms(Context context) {

        if (null == alarms) {
            alarms = new ArrayList<AlarmModel>();
        }

        SharedPreferences prefs = context.getSharedPreferences(DB_ALARM_LIST, Context.MODE_PRIVATE);
        Editor editor = prefs.edit();

        try {

            editor.putString(KEY_ALARM_LIST, ObjectSerializer.serialize(alarms));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Csacsi debug", "alarms save with sharedpref ");
        editor.apply();

    }

    public static ArrayList<AlarmModel> getAlarms(Context context) {
        if (alarms == null) {
            alarms = new ArrayList<AlarmModel>();

            SharedPreferences prefs = context.getSharedPreferences(DB_ALARM_LIST, Context.MODE_PRIVATE);

            try {

                @SuppressWarnings("unchecked")
                ArrayList<AlarmModel> deserialize = (ArrayList<AlarmModel>) ObjectSerializer.deserialize(prefs.getString(KEY_ALARM_LIST,
                        ObjectSerializer.serialize(new ArrayList<AlarmModel>())));
                Log.d("Csacsi debug", "alarms get from sharedpref ");
                return alarms = deserialize;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return alarms;
    }

    public static void addAlarm(AlarmModel alarm, Context context) {
        alarms.add(alarm);
        saveAlarms(context);

    }

    public static void removeAlarm(int position, Context context) {
        alarms.remove(alarms.get(position));
        saveAlarms(context);

    }

    public static AlarmModel getAlarmByPosition(int alarmPositionInList, Context context) {
        return getAlarms(context).get(alarmPositionInList);

    }

    public static void setNextAlarmPosition(int alarmPositionInList, Context context) {

        lastSetAlarmPositionInList = alarmPositionInList;
        SharedPreferences prefs = context.getSharedPreferences(LAST_SET_ALARM, Context.MODE_PRIVATE);
        Editor editor = prefs.edit();
        editor.putInt(KEY_LAST_SET_ALARM, lastSetAlarmPositionInList);
        editor.apply();
        Log.d("Csacsi debug", "last set alarm serialized " + lastSetAlarmPositionInList);

    }

    public static int getLastSetAlarmPositionInList(Context context) {
    	
        if (lastSetAlarmPositionInList != -1) {
            return lastSetAlarmPositionInList;
        }
        SharedPreferences prefs = context.getSharedPreferences(LAST_SET_ALARM, Context.MODE_PRIVATE);
        Log.d("Csacsi debug", "last set alarm get from sharedpref ");
        return prefs.getInt(KEY_LAST_SET_ALARM, -1);        
    }

    public static int getNextAlarmPositonToSet(Context context) {
    	AlarmModel result = null;

        int settingAlarmPositionInList = -1;

        ArrayList<AlarmModel> alarms = AlarmDataHelper.getAlarms(context); 
        
        for (int i = 0; i < alarms.size(); i++) {
            if (alarms.get(i).isActive() && alarms.get(i).getNextAlarmTimeInMillis() != -1) {

                if (result == null || alarms.get(i).getNextAlarmTimeInMillis() < result.getNextAlarmTimeInMillis()) {
                    result = alarms.get(i);
                    settingAlarmPositionInList = i;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.  HH:mm", Locale.getDefault());
                    String alarmDate = sdf.format(new Date(alarms.get(i).getNextAlarmTimeInMillis()));

                    Log.d("Csacsi debug", "getnextalarm date i: " + i + " edik " + alarmDate);
                }
            }

        }

        return settingAlarmPositionInList;
    }

    }
