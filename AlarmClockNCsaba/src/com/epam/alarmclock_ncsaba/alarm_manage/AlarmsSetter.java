package com.epam.alarmclock_ncsaba.alarm_manage;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.activities.WakeUpActivity;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.fragments.AlarmListFragment;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;
import com.epam.alarmclock_ncsaba.widget.NewAppWidget;

@SuppressLint("NewApi")
public class AlarmsSetter extends BroadcastReceiver {
	
	private static final int NO_SNOOZED_ALARM = 1;

	public static final String LAST_SET_ALARM_POSITION = "last set alarm position";

	private static AlarmManager alarmMng;
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.  HH:mm", Locale.getDefault());

	@Override
	public void onReceive(Context context, Intent arg1) {
		Log.d("Csacsi debug", " onReceive after Reboot");
		int lastSetAlarmPositionInList = AlarmDataHelper.getLastSetAlarmPositionInList(context);

		if (lastSetAlarmPositionInList == -1)
			return;

		AlarmModel lastSetAlarm = AlarmDataHelper.getAlarmByPosition(lastSetAlarmPositionInList, context);

		if (lastSetAlarm != null && lastSetAlarm.getAlarmRepeat().isEmpty()) {
			lastSetAlarm.setActive(false);
		}

		setAlarms(context);

	}

	public static void setAlarms(Context context) {
		alarmMng = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		cancelAlarms(context);
		saveAlarms(context, NO_SNOOZED_ALARM);

	}

	public static void setAlarms(Context context, int snoozedAlarmPositon) {
		alarmMng = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		cancelAlarms(context);
		saveAlarms(context, snoozedAlarmPositon);

	}

	public static void saveAlarms(Context context, int snoozedAlarmPosition) {
		int settingAlarmPositionInList;
		if (snoozedAlarmPosition == NO_SNOOZED_ALARM) {
			settingAlarmPositionInList = AlarmDataHelper.getNextAlarmPositonToSet(context);
		} else
			settingAlarmPositionInList = snoozedAlarmPosition;

		if (settingAlarmPositionInList != -1) {

			PendingIntent intent = createPendingIntent(context, settingAlarmPositionInList);
			AlarmModel settingAlarm = AlarmDataHelper.getAlarms(context).get(settingAlarmPositionInList);

			if (settingAlarm.isActive()) {
				AlarmDataHelper.setNextAlarmPosition(settingAlarmPositionInList, context);
				if (android.os.Build.VERSION.SDK_INT >= 19) {
					alarmMng.setExact(AlarmManager.RTC_WAKEUP, settingAlarm.getNextAlarmTimeInMillis(), intent);

				} else {

					alarmMng.set(AlarmManager.RTC_WAKEUP, settingAlarm.getNextAlarmTimeInMillis(), intent);
				}

				String alarmDate = sdf.format(new Date(settingAlarm.getNextAlarmTimeInMillis()));
				Log.d("Csacsi debug", "set next alarm date: " + alarmDate);
				Log.d("Csacsi debug", "set next alarm position: " + settingAlarmPositionInList);

				if (context instanceof FragmentActivity) {
					AlarmListFragment myFragment = (AlarmListFragment) ((FragmentActivity) context).getSupportFragmentManager().findFragmentByTag(
							AlarmListFragment.TAG);
					if (myFragment != null && myFragment.isVisible()) {

						showToast(context);
					}
				}

				updateWidget(context);

			}
		}

	}

	public static void showToast(Context context) {

		int lastSetAlarmPosition = AlarmDataHelper.getLastSetAlarmPositionInList(context);
		AlarmModel settingAlarm = AlarmDataHelper.getAlarmByPosition(lastSetAlarmPosition, context);
		String alarmDate = sdf.format(new Date(settingAlarm.getNextAlarmTimeInMillis()));

		Toast alarmToast = Toast.makeText(context, context.getString(R.string.set_alarm_time_) + alarmDate, Toast.LENGTH_LONG);
		View view = alarmToast.getView();
		TextView text = (TextView) view.findViewById(android.R.id.message);
		text.setBackgroundColor(Color.TRANSPARENT);
		alarmToast.show();
	}

	private static void updateWidget(Context context) {
		Intent widgetIntent = new Intent(context, NewAppWidget.class);
		widgetIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
		int[] ids = {0, 1, 2, 3};
		widgetIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
		context.sendBroadcast(widgetIntent);
	}

	public static void cancelAlarms(Context context) {
		Log.d("Csacsi debug", "cancelalarm called, pos:  " + AlarmDataHelper.getLastSetAlarmPositionInList(context));
		if (AlarmDataHelper.getLastSetAlarmPositionInList(context) != -1) {

			PendingIntent intent = createPendingIntent(context, AlarmDataHelper.getLastSetAlarmPositionInList(context));

			if (intent != null) {
				alarmMng.cancel(intent);

				Log.d("Csacsi debug", "alarm position cancelled  " + AlarmDataHelper.getLastSetAlarmPositionInList(context));
				updateWidget(context);
			}
		}

	}

	private static PendingIntent createPendingIntent(Context context, int alarmPositionInList) {
		Intent intent = new Intent(context, WakeUpActivity.class);
		intent.putExtra(LAST_SET_ALARM_POSITION, alarmPositionInList);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);

		return PendingIntent.getActivity(context, alarmPositionInList, intent, PendingIntent.FLAG_UPDATE_CURRENT);

	}

}
