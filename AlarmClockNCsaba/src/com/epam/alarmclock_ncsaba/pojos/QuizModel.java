package com.epam.alarmclock_ncsaba.pojos;

public class QuizModel {

    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;
    private String question;
    private int pictureId;
    private char rightAnswer;

    /**
     * 
     */
    public QuizModel() {

    }

    /**
     * @param answerA
     * @param answerB
     * @param answerC
     * @param answerD
     * @param question
     * @param picturePath
     * @param rightAnswer
     */
    public QuizModel(String question,String answerA, String answerB, String answerC, String answerD,  char rightAnswer, int pictureId) {
        super();
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
        this.question = question;
        this.pictureId = pictureId;
        this.rightAnswer = rightAnswer;
    }

    public int getPictureId() {
        return pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public char getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(char rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    public String getAnswerA() {
        return answerA;
    }

    public void setAnswerA(String answerA) {
        this.answerA = answerA;
    }

    public String getAnswerB() {
        return answerB;
    }

    public void setAnswerB(String answerB) {
        this.answerB = answerB;
    }

    public String getAnswerC() {
        return answerC;
    }

    public void setAnswerC(String answerC) {
        this.answerC = answerC;
    }

    public String getAnswerD() {
        return answerD;
    }

    public void setAnswerD(String answerD) {
        this.answerD = answerD;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

   

}
