package com.epam.alarmclock_ncsaba.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.epam.alarmclock_ncsaba.R;

public class AlarmModel implements Serializable {

	private static final long serialVersionUID = 1L;
	private int alarmTimeHour;
	private int quizRepeat;
	private int alarmTimeMinutes;
	private String alarmTone;
	private Set<Days> alarmRepeat;
	private DismissType dismissType;
	private boolean isActive;
	private String label;
	private WakeUpType wakeUpType;
	private String alarmRepeatString;
	private boolean isNewAlarm;
	private boolean isRinging;
	private boolean isSnoozable;
	private boolean isSnoozing;
	private String snoozeLabel;
	private int snoozedAlarmTimeHour;
	private int snoozedAlarmTimeMinutes;

	private int snoozeTime;

	public AlarmModel() {

		Calendar mcurrentTime = Calendar.getInstance();
		alarmTimeHour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
		alarmTimeMinutes = mcurrentTime.get(Calendar.MINUTE);
		alarmTone = Settings.System.DEFAULT_ALARM_ALERT_URI.toString();
		Log.d("Csacsi debug", "default tone: " + Settings.System.DEFAULT_ALARM_ALERT_URI);
		isNewAlarm = true;
		isSnoozable = true;
		snoozeTime = 5;
		quizRepeat = 5;
		wakeUpType = WakeUpType.BOTH;
		alarmRepeat = new HashSet<Days>();
		dismissType = DismissType.PICTURE;
		isActive = false;

	}

	public String getAlarmRepeatString(Context context) {

		StringBuilder sb = new StringBuilder();

		ArrayList<Days> days = new ArrayList<Days>();

		days.addAll(alarmRepeat);
		Collections.sort(days);
		for (int i = 0; i < days.size(); i++) {
			sb.append(context.getResources().getStringArray(R.array.week_days)[days.get(i).ordinal()]);
			if (i != days.size() - 1) {
				sb.append(", ");
			}

		}
		
		alarmRepeatString = sb.toString();

		return alarmRepeatString;
	}

	public void cleareRepeatDays() {

		alarmRepeat.clear();

	}

	public long getAlarmTimeInMilis() {

		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		cal.set(Calendar.HOUR_OF_DAY, alarmTimeHour);
		cal.set(Calendar.MINUTE, alarmTimeMinutes);
		cal.set(Calendar.SECOND, 0);

		return cal.getTimeInMillis();
	}

	public String getAlarmTimeString() {
		String hour;
		String minutes;

		if (getAlarmTimeHour() < 10) {
			hour = "0" + getAlarmTimeHour();
		} else
			hour = getAlarmTimeHour() + "";

		if (getAlarmTimeMinutes() < 10) {
			minutes = "0" + getAlarmTimeMinutes();
		} else
			minutes = getAlarmTimeMinutes() + "";
		return hour + ":" + minutes;
	}

	public Long getNextAlarmTimeInMillis() {
        Calendar calToday = Calendar.getInstance();
        calToday.setTimeInMillis(System.currentTimeMillis());

        Calendar alarm = Calendar.getInstance();
        alarm.setTimeInMillis(getAlarmTimeInMilis());

        int today = calToday.get(Calendar.DAY_OF_WEEK) - 1;

        if (alarmRepeat.isEmpty()) {
            if (alarm.compareTo(calToday) == -1) {
                alarm.add(Calendar.DAY_OF_WEEK, 1);
            }

            return alarm.getTimeInMillis();

        }

        if (alarmRepeat.contains(Days.values()[today])) {       
            if (alarm.compareTo(calToday) == 1) {
                return alarm.getTimeInMillis();             
            }
            
            if (alarmRepeat.size() == 1)
            {
                alarm.add(Calendar.DAY_OF_WEEK, 7);
                return alarm.getTimeInMillis();
            }               
        }

        int weekDay = today + 1;

        for (int i = 1; i <= 7; i++) {

            if ((weekDay) > 6)
                weekDay = 0;

            if (alarmRepeat.contains(Days.values()[weekDay])) {
                alarm.add(Calendar.DAY_OF_WEEK, i);
                break;
            }
            weekDay++;
        }
        return alarm.getTimeInMillis();
    }


	public WakeUpType getWakeUpType() {
		return wakeUpType;
	}

	public void setWakeUpType(WakeUpType wakeUpType) {
		this.wakeUpType = wakeUpType;
	}

	public boolean isSnoozing() {
		return isSnoozing;
	}

	public void setSnoozing(boolean isSnoozing) {
		this.isSnoozing = isSnoozing;
	}

	public String getSnoozeLabel() {
		return snoozeLabel;
	}

	public void setSnoozeLabel(String snoozeLabel) {
		this.snoozeLabel = snoozeLabel;
	}

	public int getSnoozedAlarmTimeHour() {
		return snoozedAlarmTimeHour;
	}

	public void setSnoozedAlarmTimeHour(int snoozedAlarmTimeHour) {
		this.snoozedAlarmTimeHour = snoozedAlarmTimeHour;
	}

	public int getSnoozedAlarmTimeMinutes() {
		return snoozedAlarmTimeMinutes;
	}

	public void setSnoozedAlarmTimeMinutes(int snoozedAlarmTimeMinutes) {
		this.snoozedAlarmTimeMinutes = snoozedAlarmTimeMinutes;
	}

	public boolean isSnoozable() {
		return isSnoozable;
	}

	public void setSnoozable(boolean isSnoozable) {
		this.isSnoozable = isSnoozable;
	}

	public int getSnoozeTime() {
		return snoozeTime;
	}

	public void setSnoozeTime(int snoozeTime) {
		this.snoozeTime = snoozeTime;
	}

	public int getQuizRepeat() {
		return quizRepeat;
	}

	public void setQuizRepeat(int quizRepeat) {
		this.quizRepeat = quizRepeat;
	}

	public boolean isRinging() {
		return isRinging;
	}

	public void setRinging(boolean isRinging) {
		this.isRinging = isRinging;
	}

	public boolean isNewAlarm() {
		return isNewAlarm;
	}

	public void setNewAlarm(boolean isNewAlarm) {
		this.isNewAlarm = isNewAlarm;
	}

	public int getAlarmTimeHour() {
		return alarmTimeHour;
	}

	public void setAlarmTimeHour(int alarmTimeHour) {
		this.alarmTimeHour = alarmTimeHour;
	}

	public int getAlarmTimeMinutes() {
		return alarmTimeMinutes;
	}

	public void setAlarmTimeMinutes(int alarmTimeMinutes) {
		this.alarmTimeMinutes = alarmTimeMinutes;
	}

	public String getAlarmTone() {
		return alarmTone;
	}

	public void setAlarmtone(String alarmtone) {
		this.alarmTone = alarmtone;
	}

	public WakeUpType getSoundType() {
		return wakeUpType;
	}

	public void setSoundType(WakeUpType soundType) {
		this.wakeUpType = soundType;
	}

	public Set<Days> getAlarmRepeat() {
		return alarmRepeat;
	}

	public void addAlarmRepeat(Days day) {

		alarmRepeat.add(day);
	}

	public DismissType getDismissType() {
		return dismissType;
	}

	public void setDismissType(DismissType dismissType) {
		this.dismissType = dismissType;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
