package com.epam.alarmclock_ncsaba.widget;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.activities.MainActivity;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;

/**
 * Implementation of App Widget functionality.
 */
public class NewAppWidget extends AppWidgetProvider {

	private static RemoteViews views;

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		// There may be multiple widgets active, so update all of them
		ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
		int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
		for (int i = 0; i < allWidgetIds.length; i++) {
			updateAppWidget(context, appWidgetManager, allWidgetIds[i]);
		}
	}

	@Override
	public void onEnabled(Context context) {
		// Enter relevant functionality for when the first widget is created
	}

	@Override
	public void onDisabled(Context context) {
		// Enter relevant functionality for when the last widget is disabled
	}

	static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
		int nextSetAlarmPosition = AlarmDataHelper.getNextAlarmPositonToSet(context);
		views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
		intentToGetBackToMainActivity(context, appWidgetManager, appWidgetId);

		views.setTextViewText(R.id.appwidget_text, context.getString(R.string.no_active));

		if (nextSetAlarmPosition != -1) {
			AlarmModel lastSetAlarm = AlarmDataHelper.getAlarmByPosition(nextSetAlarmPosition, context);

			SimpleDateFormat sdf = new SimpleDateFormat("EEE, HH:mm", Locale.getDefault());

			String widgetAlarmTime = sdf.format(new Date(lastSetAlarm.getNextAlarmTimeInMillis()));

			CharSequence widgetText = widgetAlarmTime;

			views.setTextViewText(R.id.appwidget_text, widgetText);
			views.setImageViewResource(R.id.imageView_Widget, R.drawable.ic_launcher);

		}
		appWidgetManager.updateAppWidget(appWidgetId, views);

	}

	private static void intentToGetBackToMainActivity(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
		Intent intent = new Intent(context, MainActivity.class);

		intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetId);

		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
		views.setOnClickPendingIntent(R.id.LinearLayoutWidget, pendingIntent);
		appWidgetManager.updateAppWidget(appWidgetId, views);
	}
}
