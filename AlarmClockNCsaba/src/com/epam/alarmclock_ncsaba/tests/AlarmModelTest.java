package com.epam.alarmclock_ncsaba.tests;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.easymock.EasyMock;
import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;

import com.epam.alarmclock_ncsaba.pojos.AlarmModel;

public class AlarmModelTest extends EasyMockSupport {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetNextAlarmTimeInMillis() {
//		fail("Not yet implemented");
		// GIVEN
		AlarmModel alarmModel = createMock(AlarmModel.class);
		
//		Calendar cal =createMock(Calendar.getInstance(Locale.getDefault()));
		
		Calendar cal=Calendar.getInstance();
		cal.set(2014, 7, 25, 10, 15);
				
		// EXPECT
		EasyMock.expect(alarmModel.getNextAlarmTimeInMillis()).andReturn(cal.getTimeInMillis());	
		replayAll();
		// WHEN
		alarmModel.setAlarmTimeHour(10);
		alarmModel.setAlarmTimeMinutes(15);
		long result=alarmModel.getNextAlarmTimeInMillis();
		// THEN
		assertEquals(cal.getTimeInMillis(), result);
		
		verifyAll();

	}

}
