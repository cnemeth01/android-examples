package com.epam.alarmclock_ncsaba.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.R.drawable;
import com.epam.alarmclock_ncsaba.alarm_manage.AlarmsSetter;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;

public class MainAdapter extends ArrayAdapter<AlarmModel> {

	public ArrayList<AlarmModel> alarms = new ArrayList<AlarmModel>();
	private Context context;
	

	public MainAdapter(Context context, int resource, ArrayList<AlarmModel> alarms) {
		super(context, resource, alarms);
		this.alarms = alarms;
		this.context = context;
	}

	@Override
	public int getCount() {
		return this.alarms.size();
	}
	public void setAdapter(ArrayList<AlarmModel> items) {
		this.alarms = items;

	}

	@Override
	public AlarmModel getItem(int position) {
		return this.alarms.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		final AlarmModel alarm = alarms.get(position);

		if (convertView == null) {

			LayoutInflater nInflate = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = nInflate.inflate(R.layout.main_row, parent, false);
			holder = new ViewHolder();

			holder.textAlarmTime = (TextView) convertView.findViewById(R.id.tw_alarm_time);
			holder.textAlarmLabel = (TextView) convertView.findViewById(R.id.tw_alarm_label);
			holder.textAlarmRepeat = (TextView) convertView.findViewById(R.id.tw_alarm_days);
			holder.imageWakeUpType = (ImageView) convertView.findViewById(R.id.iw_wake_kind);
			holder.checkIsActive = (CheckBox) convertView.findViewById(R.id.cb_is_active);
			convertView.setTag(holder);
		} else {

			holder = (ViewHolder) convertView.getTag();
		}

		holder.textAlarmTime.setText(alarm.getAlarmTimeString());
		holder.textAlarmLabel.setText(alarm.getLabel());
		holder.textAlarmRepeat.setText(alarm.getAlarmRepeatString(context));
		holder.checkIsActive.setChecked(alarm.isActive());
		holder.checkIsActive.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					alarm.setActive(true);
				Log.d("Csacsi debug" ,"alarmanager set allarms called is active checkbox true");
					AlarmDataHelper.saveAlarms(context);
					AlarmsSetter.setAlarms(context);
					
				} else {
					alarm.setActive(false);
				Log.d("Csacsi debug" ,"alarmanager set allarms called is active checkbox false");
					AlarmDataHelper.saveAlarms(context);
					AlarmsSetter.setAlarms(context);
					
				}
				

			}

		});

		switch (alarm.getDismissType()) {
			case BUTTON :
				holder.imageWakeUpType.setImageResource(drawable.ic_button);
				break;
			case SHAKE :
				holder.imageWakeUpType.setImageResource(drawable.ic_shake1);
				break;
			case MATH :
				holder.imageWakeUpType.setImageResource(drawable.ic_count);
				break;
			case PICTURE :
				holder.imageWakeUpType.setImageResource(drawable.ic_guess);
				break;

			default :

				holder.imageWakeUpType.setImageResource(drawable.ic_button);
				break;
		}

		return convertView;

	}

	static class ViewHolder {
		TextView textAlarmTime;
		TextView textAlarmRepeat;
		TextView textAlarmLabel;
		ImageView imageWakeUpType;
		CheckBox checkIsActive;

	}

}
