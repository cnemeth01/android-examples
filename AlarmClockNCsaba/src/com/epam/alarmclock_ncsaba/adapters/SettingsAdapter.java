package com.epam.alarmclock_ncsaba.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.settings_menu.SettingsListFactory;

public class SettingsAdapter extends BaseAdapter {

	private ArrayList<String[]> settings = new ArrayList<String[]>();
	private int alarmPositionInList;
	private Context context;

	public SettingsAdapter(ArrayList<String[]> alarmSettings, Context context, int alarmPositionInList) {
		this.context = context;
		this.alarmPositionInList = alarmPositionInList;
		settings = alarmSettings;
	}

	public void setAdapter(ArrayList<String[]> alarmSettings, int alarmPositionInList) {
		this.alarmPositionInList = alarmPositionInList;
		settings = alarmSettings;
		this.notifyDataSetChanged();

	}

	@Override
	public int getCount() {
		return settings.size();
	}

	@Override
	public long getItemId(int position) {

		return 0;
	}

	@Override
	public Object getItem(int position) {

		return null;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;

		String[] set = settings.get(position);

		if (convertView == null) {

			LayoutInflater nInflate = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = nInflate.inflate(R.layout.alarm_settings_row, parent, false);
			holder = new ViewHolder();

			holder.textSettingTitle = (TextView) convertView.findViewById(R.id.tw_setting_title);
			holder.textSetting = (TextView) convertView.findViewById(R.id.tw_setting);
			holder.checkbisActive = (CheckBox) convertView.findViewById(R.id.cb_setting_is_active);
			convertView.setTag(holder);
		} else {

			holder = (ViewHolder) convertView.getTag();
		}

		holder.textSettingTitle.setText(set[0]);

		if (position == SettingsListFactory.SET_SNOOZE_TIME) {
			holder.textSetting.setText(set[1] + context.getResources().getString(R.string._minutes));
		} else if (position == SettingsListFactory.SET_QUIZ_REPEATING) {
			holder.textSetting.setText(set[1] + context.getResources().getString(R.string._times));

		} else if (position == SettingsListFactory.SET_ALARM_REPEAT_DAYS) {

			if (set[1].isEmpty()) {
				holder.textSetting.setText(set[1] + context.getResources().getString(R.string.never));
			}else
				holder.textSetting.setText(set[1]);
		} else
			holder.textSetting.setText(set[1]);
			

		if (set[2] != null) {
			holder.checkbisActive.setChecked(Boolean.valueOf(set[2]));
			holder.checkbisActive.setFocusable(false);
			holder.checkbisActive.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked) {
						AlarmDataHelper.getAlarmByPosition(alarmPositionInList, context).setSnoozable(true);
					} else {
						AlarmDataHelper.getAlarmByPosition(alarmPositionInList, context).setSnoozable(false);
					}

				}
			});
		} else
			holder.checkbisActive.setVisibility(View.GONE);

		return convertView;

	}

	static class ViewHolder {

		TextView textSettingTitle;
		TextView textSetting;
		CheckBox checkbisActive;

	}

}
