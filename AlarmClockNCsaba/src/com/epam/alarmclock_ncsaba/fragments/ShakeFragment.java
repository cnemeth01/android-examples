package com.epam.alarmclock_ncsaba.fragments;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.interfaces.ICallBackToWakeUpActivity;

public class ShakeFragment extends Fragment {

	public static final String TAG = "ShakeFragment";
	private ICallBackToWakeUpActivity listener;
	private TextView tVShake;
	private SensorManager mSensorManager;
	private float mAccel; // acceleration apart from gravity
	private float mAccelCurrent; // current acceleration including gravity
	private float mAccelLast; // last acceleration including gravity
	private int timer = 10;
	private final SensorEventListener mSensorListener = new SensorEventListener() {

		

		public void onSensorChanged(SensorEvent se) {
			float x = se.values[0];
			float y = se.values[1];
			float z = se.values[2];
			mAccelLast = mAccelCurrent;
			mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
			float delta = mAccelCurrent - mAccelLast;
			mAccel = mAccel * 0.9f + delta; // perform low-cut filter

			if (mAccel > 12) {
				timer--;

				tVShake.setText(timer+"");
				if (timer == 0) {

					listener.finishAlarm();
				}

			}
		}

		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};

	public ShakeFragment() {
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.shake, container, false);
		tVShake = (TextView) rootView.findViewById(R.id.textViewShake);
		tVShake.setText(timer+"");
		initAccelSensor();

		return rootView;
	}

	private void initAccelSensor() {
		mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
		mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
		mAccel = 0.00f;
		mAccelCurrent = SensorManager.GRAVITY_EARTH;
		mAccelLast = SensorManager.GRAVITY_EARTH;
	}

	@Override
	public void onAttach(Activity activity) {
		listener = (ICallBackToWakeUpActivity) getActivity();
		super.onAttach(activity);
	}

	@Override
	public void onResume() {
		mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
		super.onResume();
	}

	@Override
	public void onPause() {
		mSensorManager.unregisterListener(mSensorListener);
		super.onPause();
	}

}
