package com.epam.alarmclock_ncsaba.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.activities.WakeUpActivity;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.datahelper.QuizDataHelper;
import com.epam.alarmclock_ncsaba.interfaces.ICallBackToWakeUpActivity;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;
import com.epam.alarmclock_ncsaba.pojos.QuizModel;
import com.epam.alarmclock_ncsaba.pojos.DismissType;

public class QuizFragment extends Fragment {

    public static final String TAG = "QuizFragment";

    private Button btnAnswerA;
    private Button btnAnswerB;
    private Button btnAnswerC;
    private Button btnAnswerD;
    private Button btnNewQuiz;
    private TextView txVQuestion;
    private TextView txRemainNewQuiz;
    private ImageView imVPicture;
    private String question;
    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;
    private ICallBackToWakeUpActivity listener;

    private int alarmPositionInList;

    private QuizModel randomQuiz;

    private int currentQuizeRepeatRemains;

    private AlarmModel currentAlarm;

    public QuizFragment() {
        super();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.quiz, container, false);

        initView(rootView);
        updateRemain();

        return rootView;
    }

    private void initView(View rootView) {
        btnAnswerA = (Button) rootView.findViewById(R.id.btn_answer_a);
        btnAnswerB = (Button) rootView.findViewById(R.id.btn_answer_b);
        btnAnswerC = (Button) rootView.findViewById(R.id.btn_answer_c);
        btnAnswerD = (Button) rootView.findViewById(R.id.btn_answer_d);
        btnNewQuiz = (Button) rootView.findViewById(R.id.btn_new_quiz);
        txVQuestion = (TextView) rootView.findViewById(R.id.textViewQuestion);
        imVPicture = (ImageView) rootView.findViewById(R.id.imageViewPicture);
        txRemainNewQuiz = (TextView) rootView.findViewById(R.id.textViewRemainNewn);

        btnNewQuiz.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                currentQuizeRepeatRemains--;

                if (currentQuizeRepeatRemains > 0) {
                    initQuizUI();
                    updateRemain();
                } else
                    listener.finishAlarm();

            }

        });

        initQuizButtons(btnAnswerA, 'A');
        initQuizButtons(btnAnswerB, 'B');
        initQuizButtons(btnAnswerC, 'C');
        initQuizButtons(btnAnswerD, 'D');
    }

    private void initQuizButtons(Button button, final char buttonChar) {

        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
             
                
                if (isRightAnswer(buttonChar)) {
                    listener.finishAlarm();
                } else if (currentQuizeRepeatRemains > 0) {
                    currentQuizeRepeatRemains--;
                    initQuizUI();
                    updateRemain();
                } else
                    listener.finishAlarm();

            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        listener = (ICallBackToWakeUpActivity) getActivity();
        super.onAttach(activity);
    }

    @Override
    public void onStart() {
        if (getArguments() != null && alarmPositionInList == -1) {
            alarmPositionInList = getArguments().getInt(WakeUpActivity.ARG_ALARM_POSITION_IN_LIST, 0);
        }
        initNewQuizRemains();
        initQuizUI();
        updateAlarm(alarmPositionInList);
        super.onStart();
    }

    private void initNewQuizRemains() {
        currentAlarm = AlarmDataHelper.getAlarmByPosition(alarmPositionInList, getActivity());
        currentQuizeRepeatRemains = currentAlarm.getQuizRepeat();
        txRemainNewQuiz.setText(currentQuizeRepeatRemains + getResources().getString(R.string._tries_remain));
    }

    private void updateRemain() {

        txRemainNewQuiz.setText(currentQuizeRepeatRemains + getResources().getString(R.string._tries_remain));

    }

    private void initQuizUI() {

        if (currentAlarm.getDismissType() == DismissType.MATH) {

            imVPicture.setVisibility(View.GONE);
            
            txVQuestion.setTextSize(60);
            randomQuiz = QuizDataHelper.getRandomMathQuiz();
            question=randomQuiz.getQuestion();
            setUI(randomQuiz);

        } else if (currentAlarm.getDismissType() == DismissType.PICTURE) {
           
            randomQuiz = QuizDataHelper.getRandomPictureQuiz();
            question=getActivity().getString(R.string.who_is_this_);
            setUI(randomQuiz);

        }
    }

    private void setUI(QuizModel randomQuiz) {
    	
        
        answerA = randomQuiz.getAnswerA();
        answerB = randomQuiz.getAnswerB();
        answerC = randomQuiz.getAnswerC();
        answerD = randomQuiz.getAnswerD();

        txVQuestion.setText(question);
        btnAnswerA.setText(answerA);
        btnAnswerB.setText(answerB);
        btnAnswerC.setText(answerC);
        btnAnswerD.setText(answerD);

        if (randomQuiz.getPictureId() != -1) {

            imVPicture.setImageResource(randomQuiz.getPictureId());

        }

    }

    public boolean isRightAnswer(char button) {

        if (button == randomQuiz.getRightAnswer()) {
            return true;
        }
        return false;

    }

    public void updateAlarm(int alarmPositionInList) {
        this.alarmPositionInList = alarmPositionInList;

    }

}
