package com.epam.alarmclock_ncsaba.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.epam.alarmclock_ncsaba.R;

/**
 * A fragment with a Google +1 button. Activities that contain this fragment
 * must implement the {@link AnalogeClockFragment.OnFragmentInteractionListener}
 * interface to handle interaction events. Use the
 * {@link AnalogeClockFragment#newInstance} factory method to create an instance
 * of this fragment.
 * 
 */
public class AnalogeClockFragment extends Fragment {

    
    public static AnalogeClockFragment newInstance() {
       
        AnalogeClockFragment fragment = new AnalogeClockFragment();

        return fragment;
    }

    public AnalogeClockFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_analog_clock, container, false);

       
        return view;
    }

   

    @Override
    public void onDetach() {
        super.onDetach();
       
    }

}
