package com.epam.alarmclock_ncsaba.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.adapters.MainAdapter;
import com.epam.alarmclock_ncsaba.alarm_manage.AlarmsSetter;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.interfaces.ICalBackToMainActivity;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;

public class AlarmListFragment extends Fragment implements OnItemClickListener {

    public static final String TAG = "AlarmlistFragment";

    private MainAdapter adapter;
    private TextView txtTime;
    private ICalBackToMainActivity listener;
    private ListView mainList;
    private AlertDialog dialogDeleteAlarm;

    private int alarmPositionInList;
    private Button btnNewAlarm;

    @InjectView(R.id.tv_no_alarm)
    TextView textViewNoAlarm;

    private ArrayList<AlarmModel> alarms;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        //      setRetainInstance(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_alarms_list, container, false);
        ButterKnife.inject(this, rootView);
        initDigitalClock(rootView);

        initNewButton(rootView);
        mainList = (ListView) rootView.findViewById(R.id.lv_main);

        initList();
        mainList.setOnItemClickListener(this);
        initLongClickListener();

        return rootView;
    }

    private void initDigitalClock(View rootView) {
        txtTime = (TextView) rootView.findViewById(R.id.tv_main_clock);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
        String strDate = sdf.format(c.getTime());
        txtTime.setText(strDate);
    }

    private void initNewButton(View rootView) {
        btnNewAlarm = (Button) rootView.findViewById(R.id.btn_new_alarm);

        btnNewAlarm.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                AlarmModel newAlarm = new AlarmModel();
                newAlarm.setLabel(getResources().getString(R.string.new_alarm));

                AlarmDataHelper.addAlarm(newAlarm, getActivity());
                alarmPositionInList++;
                updateList(alarmPositionInList);

                listener.setSettingsFragment(AlarmDataHelper.getAlarms(getActivity()).size() - 1, true);

            }
        });
    }

    public void setCurrentTime(String currentTime) {
        txtTime.setText(currentTime);

    }

    @Override
    public void onStart() {

        if (getArguments() != null) {
            alarmPositionInList = getArguments().getInt(AlarmSettingsFragment.ARG_POSITION);
        }

        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {

        try {
            listener = (ICalBackToMainActivity) activity;
        } catch (ClassCastException ce) {
            Log.e(TAG, "Parent Activity does not implement listener interface!");
        } catch (Exception e) {
            Log.e(TAG, "Unexpected exception!");
            e.printStackTrace();
        }
        super.onAttach(activity);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerForContextMenu(mainList);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        view.setSelected(true);
        Log.d("Csacsi debug", "position on item " + position);

        listener.setSettingsFragment(position, true);

    }

    public void initLongClickListener() {
        mainList.setLongClickable(true);
        mainList.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {

                createDeleteDialog(pos);
                return true;
            }
        });

    }

    private void createDeleteDialog(final int alarmPosition) {
        AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
        ab.setTitle(R.string.delete_this_item_);
        ab.setMessage("");
        android.content.DialogInterface.OnClickListener mylistener = new android.content.DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (DialogInterface.BUTTON_POSITIVE == which) {
                    AlarmDataHelper.removeAlarm(alarmPosition, getActivity());

                    listener.alarmDeleted();

                    AlarmsSetter.setAlarms(getActivity());
                    Log.d("Csacsi debug", "alarmanager set allarms called delete alarm");
                    updateList(alarmPositionInList);
                    setNoAlarmText();

                } else if (DialogInterface.BUTTON_NEGATIVE == which) {
                    dialogDeleteAlarm.dismiss();

                }
            }
        };
        ab.setPositiveButton(R.string.delete, mylistener);
        ab.setNegativeButton(R.string.cancel, mylistener);
        ab.setCancelable(false); // back button is disabled
        dialogDeleteAlarm = ab.create();

        dialogDeleteAlarm.show();
    }

    @Override
    public void onResume() {

        initList();
        setNoAlarmText();
        super.onResume();
    }

    public void updateList(int positioninInList) {
        this.alarmPositionInList = positioninInList;
        adapter.setAdapter(AlarmDataHelper.getAlarms(getActivity()));
        AlarmsSetter.setAlarms(getActivity());
        adapter.notifyDataSetChanged();
        // showToast();

    }

    public void setNoAlarmText() {
        if (AlarmDataHelper.getAlarms(getActivity()).size() == 0) {
            textViewNoAlarm.setVisibility(View.VISIBLE);
        } else {

            textViewNoAlarm.setVisibility(View.GONE);
        }

    }

    public void initList() {
        Log.d("Csacsi debug", "AlarmListFragment get alarms size:" + AlarmDataHelper.getAlarms(getActivity()).size());

        alarms = AlarmDataHelper.getAlarms(getActivity());

        adapter = new MainAdapter(getActivity(), R.id.lv_main, alarms);
        mainList.setAdapter(adapter);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

}
