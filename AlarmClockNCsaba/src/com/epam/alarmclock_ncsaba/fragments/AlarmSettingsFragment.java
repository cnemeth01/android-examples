package com.epam.alarmclock_ncsaba.fragments;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.adapters.SettingsAdapter;
import com.epam.alarmclock_ncsaba.alarm_manage.AlarmsSetter;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;
import com.epam.alarmclock_ncsaba.settings_menu.SettingsDialogFactory;
import com.epam.alarmclock_ncsaba.settings_menu.SettingsListFactory;

public class AlarmSettingsFragment extends Fragment {

    private static final int RING_TONE_INTENT = 5;
    public static final String ARG_BUTTON_PRESS_TYPE = "type";
    public static final String ARG_POSITION = "position";
    public static final String ARG_SETTINGS = "settings";

    public static final String TAG = "AlarmSetFragment";

    private int alarmPositionInList = -1;
    private SettingsAdapter adapter;
    private ArrayList<String[]> settings;

    @InjectView(R.id.lv_settings)
    ListView settingList;

    private static MessageHandler mMessageHandler;

    public AlarmSettingsFragment() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        mMessageHandler = new MessageHandler(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_alarm_settings, container, false);

        ButterKnife.inject(this, rootView);
        setListOnClic();

        return rootView;
    }

    private void setListOnClic() {

        settingList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                if (position == SettingsListFactory.SET_RINGTONE_TYPE) {
                    initToneChooser(alarmPositionInList);
                } else if (position == SettingsListFactory.SET_ALARM_TIME) {
                    SettingsDialogFactory.createTimePickerDialog(alarmPositionInList, getActivity());
                } else {
                    SettingsDialogFactory.createDialog(alarmPositionInList, getActivity(), position);
                }
            }
        });
    }

    protected void initToneChooser(int alarmPositionInList) {
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
        this.startActivityForResult(intent, RING_TONE_INTENT);

    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        getActivity();
        if (resultCode == Activity.RESULT_OK && requestCode == RING_TONE_INTENT) {
            Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

            if (uri != null) {
                String uriString = uri.toString();

                AlarmDataHelper.getAlarms(getActivity()).get(alarmPositionInList).setAlarmtone(uriString);
                AlarmDataHelper.getAlarms(getActivity()).get(alarmPositionInList).setNewAlarm(false);

            } else

                AlarmDataHelper.getAlarms(getActivity().getApplicationContext()).get(alarmPositionInList)
                        .setAlarmtone(Settings.System.DEFAULT_ALARM_ALERT_URI.toString());
        }
        updateSettingList(alarmPositionInList);

    }

    @Override
    public void onStart() {
        if (getArguments() != null && alarmPositionInList == -1) {

            alarmPositionInList = getArguments().getInt(ARG_SETTINGS, 0);

        }

        initList(alarmPositionInList);
        super.onStart();
    }

    public void initList(int alarmPositioninList) {

        settings = SettingsListFactory.createSettingsList(alarmPositioninList, getActivity());
        adapter = new SettingsAdapter(settings, getActivity(), alarmPositioninList);
        settingList.setAdapter(adapter);

    }

    public void updateSettingList(int alarmPositionInInList) {
        alarmPositionInList = alarmPositionInInList;
        settings = SettingsListFactory.createSettingsList(alarmPositionInList, getActivity());
        adapter.setAdapter(settings, alarmPositionInInList);

        AlarmDataHelper.saveAlarms(getActivity());
        AlarmsSetter.setAlarms(getActivity());
        AlarmModel alarmByPosition = AlarmDataHelper.getAlarmByPosition(alarmPositionInList, getActivity());
        Log.d("Csacsi debug",
                "settinlist update cseng�hang: " + alarmByPosition.getAlarmTone() + ", wake up type: " + alarmByPosition.getWakeUpType());
        AlarmListFragment alarmListFragmetnt = (AlarmListFragment) getFragmentManager().findFragmentByTag(AlarmListFragment.TAG);

        if (alarmListFragmetnt.isVisible()) {
            alarmListFragmetnt.updateList(alarmPositionInInList);
        }

    }

    public static void sendMessage(String message) {
        if (mMessageHandler != null) {
            Message msg = new Message();
            msg.obj = message;
            mMessageHandler.sendMessage(msg);
        }
    }

    private static class MessageHandler extends Handler {
        private final WeakReference<AlarmSettingsFragment> mTarget;

        MessageHandler(AlarmSettingsFragment target) {
            mTarget = new WeakReference<AlarmSettingsFragment>(target);
        }

        @Override
        public void handleMessage(Message message) {
            final AlarmSettingsFragment target = mTarget.get();
            if (target == null) {
                return;
            }

            Integer msg = Integer.parseInt(message.obj.toString());
            target.updateSettingList(msg);

        }
    }

}
