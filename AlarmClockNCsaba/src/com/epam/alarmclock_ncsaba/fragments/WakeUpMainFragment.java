package com.epam.alarmclock_ncsaba.fragments;

import java.util.Calendar;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.activities.WakeUpActivity;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.interfaces.ICallBackToWakeUpActivity;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;
import com.epam.alarmclock_ncsaba.pojos.DismissType;

public class WakeUpMainFragment extends Fragment {

    public static final String TAG = "WakeUpMainFragment";
    public static final String ARG_ALARM_POSITION_IN_LIST = null;

    private ICallBackToWakeUpActivity listener;
    private Button btnDismiss;
    private Button btnSnooze;
    private TextView tVAlarmTime;
    private TextView tVAlarmLabel;

    private int alarmPositionInList = -1;

    private AlarmModel alarm;

    public WakeUpMainFragment() {
        super();

    }

    @Override
    public void onAttach(Activity activity) {
        listener = (ICallBackToWakeUpActivity) getActivity();
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.wake_up, container, false);

        initDismissButton(rootView);
        initSnoozeButton(rootView);

        initView(rootView);

        return rootView;
    }

    private void initView(View rootView) {
        tVAlarmTime = (TextView) rootView.findViewById(R.id.textViewAlarmTime);
        tVAlarmLabel = (TextView) rootView.findViewById(R.id.textViewAlarmLabel);

        if (alarmPositionInList != -1) {
            alarm = AlarmDataHelper.getAlarmByPosition(alarmPositionInList, getActivity());

            tVAlarmLabel.setText(alarm.getLabel());
            tVAlarmTime.setText(alarm.getAlarmTimeString());

            if (!alarm.isSnoozable()) {
                btnSnooze.setVisibility(View.GONE);
            }
        }
    }

    private void initSnoozeButton(View rootView) {
        btnSnooze = (Button) rootView.findViewById(R.id.buttonSnooze);
        btnSnooze.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                Calendar calCurrentTime = Calendar.getInstance();
                calCurrentTime.add(Calendar.MINUTE, alarm.getSnoozeTime());

                if (!alarm.isSnoozing()) {
                    alarm.setSnoozedAlarmTimeHour(alarm.getAlarmTimeHour());
                    alarm.setSnoozedAlarmTimeMinutes(alarm.getAlarmTimeMinutes());
                    alarm.setSnoozeLabel(alarm.getLabel());
                }
                alarm.setSnoozing(true);
                alarm.setLabel("Snoozing...");
                alarm.setAlarmTimeHour(calCurrentTime.get(Calendar.HOUR_OF_DAY));
                alarm.setAlarmTimeMinutes(calCurrentTime.get(Calendar.MINUTE));

                listener.snoozeAlarm(alarmPositionInList);

            }
        });

    }

    private void initDismissButton(View rootView) {
        btnDismiss = (Button) rootView.findViewById(R.id.buttonDismiss);
        btnDismiss.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                if (alarm.getDismissType() == DismissType.BUTTON) {

                    listener.finishAlarm();

                } else if (alarm.getDismissType() == DismissType.SHAKE) {

                    ShakeFragment shakeFragment = (ShakeFragment) getFragmentManager().findFragmentByTag(ShakeFragment.TAG);
                    if (shakeFragment == null) {
                        shakeFragment = new ShakeFragment();
                    }

                    getFragmentManager().popBackStack();
                    getFragmentManager().beginTransaction().replace(R.id.wake_up_container, shakeFragment, ShakeFragment.TAG).commit();

                } else if (alarm.getDismissType() == DismissType.PICTURE || alarm.getDismissType() == DismissType.MATH) {
                    QuizFragment quizFragment = (QuizFragment) getFragmentManager().findFragmentByTag(QuizFragment.TAG);
                    if (quizFragment == null) {
                        quizFragment = new QuizFragment();

                        Bundle bundle = new Bundle();
                        bundle.putInt(ARG_ALARM_POSITION_IN_LIST, alarmPositionInList);
                        quizFragment.setArguments(bundle);
                    }

                    quizFragment.updateAlarm(alarmPositionInList);
                    getFragmentManager().popBackStack();
                    getFragmentManager().beginTransaction().replace(R.id.wake_up_container, quizFragment, QuizFragment.TAG).commit();

                }

            }
        });
    }

    @Override
    public void onStart() {
        setArguments();
        updateAlarm(alarmPositionInList);
        super.onStart();
    }

    private void setArguments() {

        if (getArguments() != null && alarmPositionInList == -1) {

            alarmPositionInList = getArguments().getInt(WakeUpActivity.ARG_ALARM_POSITION_IN_LIST, 0);

        }

    }

    public void updateAlarm(int alarmPositionInList) {
        this.alarmPositionInList = alarmPositionInList;

    }
}
