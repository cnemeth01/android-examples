package com.epam.alarmclock_ncsaba.interfaces;

public interface ICalBackToMainActivity {
	
	public void setSettingsFragment(int position, boolean putToBackStack);
	public void alarmDeleted();

}
