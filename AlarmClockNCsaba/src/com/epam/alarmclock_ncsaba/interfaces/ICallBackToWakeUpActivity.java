package com.epam.alarmclock_ncsaba.interfaces;

public interface ICallBackToWakeUpActivity {

    public void finishAlarm();

    public void snoozeAlarm(int snoozedAlarmPosition);

}
