package com.epam.alarmclock_ncsaba.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.net.Uri;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.activities.WakeUpActivity;
import com.epam.alarmclock_ncsaba.pojos.WakeUpType;

public class AlarmWakeUpService extends Service implements OnPreparedListener, OnBufferingUpdateListener, OnCompletionListener, OnErrorListener,
    OnSeekCompleteListener, OnInfoListener {

    private static final long HALF_SECOND = 500;
    private MediaPlayer mPlayer;
    private Notification note;
    private PendingIntent pendingIntent;
    private boolean isVibrating = false;
    private Vibrator myVibrate;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {

        mPlayer = new MediaPlayer();

        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnErrorListener(this);
        mPlayer.setOnSeekCompleteListener(this);
        mPlayer.setOnInfoListener(this);
        mPlayer.reset();
        super.onCreate();
    }

    @SuppressLint("NewApi")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isVibrating = false;
        Log.d("Csacsi debug", "MPlayService onStartCommand Called");

        setNotification();

        int wakeUpType = intent.getIntExtra("WAKEUPTYPE", -1);

        if (wakeUpType == WakeUpType.BOTH.ordinal() || wakeUpType == WakeUpType.VIBRATE.ordinal()) {
            isVibrating = true;
        }

        if (wakeUpType == WakeUpType.BOTH.ordinal() || wakeUpType == WakeUpType.RING.ordinal()) {

            Uri tone = Uri.parse(intent.getStringExtra("RINGTONE"));
            try {
                if (tone != null && !tone.equals("")) {

                    mPlayer.setDataSource(this, tone);
                    mPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                    mPlayer.setLooping(true);
                    mPlayer.prepareAsync();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            vibrating();
        }

        return START_STICKY;
    }

    private void setNotification() {

        Intent i = new Intent(this, WakeUpActivity.class);

        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        pendingIntent = PendingIntent.getActivity(this, 0, i, 0);
        note = new NotificationCompat.Builder(this).setContentTitle("Alarm ringing...").setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pendingIntent).build();

        note.flags |= Notification.FLAG_NO_CLEAR;

        startForeground(1337, note);
    }

    @Override
    public void onDestroy() {
        Log.d("Csacsi debug", "MPlayService onDestroy Called stopPlay");
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.release();
        }

        isVibrating = false;
        if (myVibrate != null) {
            myVibrate.cancel();
        }
        stopSelf();

        super.onDestroy();
    }

    @Override
    public boolean onInfo(MediaPlayer arg0, int arg1, int arg2) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void onSeekComplete(MediaPlayer arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onError(MediaPlayer arg0, int arg1, int arg2) {
        stopSelf();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer arg0) {
        stopSelf();

    }

    @Override
    public void onBufferingUpdate(MediaPlayer arg0, int arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPrepared(MediaPlayer mp) {

        mPlayer.start();

        vibrating();

    }

    private void vibrating() {

        if (isVibrating) {
            long[] vibratePattern = {HALF_SECOND, HALF_SECOND};
            myVibrate = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            myVibrate.vibrate(vibratePattern, 0);

        }

    }

}
