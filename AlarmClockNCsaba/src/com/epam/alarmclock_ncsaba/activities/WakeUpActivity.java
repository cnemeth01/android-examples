package com.epam.alarmclock_ncsaba.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.alarm_manage.AlarmsSetter;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.fragments.WakeUpMainFragment;
import com.epam.alarmclock_ncsaba.interfaces.ICallBackToWakeUpActivity;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;
import com.epam.alarmclock_ncsaba.services.AlarmWakeUpService;

public class WakeUpActivity extends FragmentActivity implements ICallBackToWakeUpActivity {

	private static final int WAKELOCK_TIMEOUT = 60 * 1000;

	public static final String SERVICE_KEY_ALARM_POSITION = "service key alarm position";
	public static final String ARG_ALARM_POSITION_IN_LIST = "arg alaprm position in list";

	public final String TAG = this.getClass().getSimpleName();

	private WakeLock mWakeLock;
	private WakeUpMainFragment wakeUpFragment;
	private Intent intent;

	private int alarmPositionInList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.container_wake_up_fragment);

		Log.d("Csacsi debug", "database getalarms, alarm size: " + AlarmDataHelper.getAlarms(this).size());

	}

	private void initFragments() {

        alarmPositionInList = getIntent().getIntExtra(AlarmsSetter.LAST_SET_ALARM_POSITION, -1);       
        AlarmModel alarm = AlarmDataHelper.getAlarmByPosition(alarmPositionInList, this);
        
        if (alarmPositionInList != -1 && !alarm.isRinging()) {
             alarm.setRinging(true);
             AlarmDataHelper.saveAlarms(this);
             intent = new Intent(this, AlarmWakeUpService.class);
             intent.putExtra("WAKEUPTYPE", alarm.getWakeUpType().ordinal());
             intent.putExtra("RINGTONE", alarm.getAlarmTone());
             startService(intent);
             
             Log.d("Csacsi debug", "startMediaPlayService");
        }

        wakeUpFragment = (WakeUpMainFragment) getSupportFragmentManager().findFragmentByTag(WakeUpMainFragment.TAG);
        if (wakeUpFragment == null) {
            wakeUpFragment = new WakeUpMainFragment();

            Bundle bundle = new Bundle();
            bundle.putInt(ARG_ALARM_POSITION_IN_LIST, alarmPositionInList);
            wakeUpFragment.setArguments(bundle);
        }

        wakeUpFragment.updateAlarm(alarmPositionInList);

        getSupportFragmentManager().beginTransaction().replace(R.id.wake_up_container, wakeUpFragment, WakeUpMainFragment.TAG).commit();
	}

	
	private void wakeLockReleaser() {
		Runnable releaseWakelock = new Runnable() {

			@Override
			public void run() {
				getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
				getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
				getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
				getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

				if (mWakeLock != null && mWakeLock.isHeld()) {
					mWakeLock.release();
				}
			}
		};

		new Handler().postDelayed(releaseWakelock, WAKELOCK_TIMEOUT);
	}

	@Override
	public void onResume() {
		super.onResume();
		initFragments();

		wakeLockReleaser();

		acquireWakeLock();

	}

	private void acquireWakeLock() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

		PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
		if (mWakeLock == null) {
			mWakeLock = pm.newWakeLock((PowerManager.FULL_WAKE_LOCK | PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), TAG);
		}

		if (!mWakeLock.isHeld()) {
			mWakeLock.acquire();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		if (mWakeLock != null && mWakeLock.isHeld()) {
			mWakeLock.release();
		}
	}

	@Override
	public void finishAlarm() {
		stopService(intent);

		AlarmModel alarm = AlarmDataHelper.getAlarmByPosition(alarmPositionInList, this);
		alarm.setRinging(false);

		if (alarm.isSnoozing()) {
			alarm.setAlarmTimeHour(alarm.getSnoozedAlarmTimeHour());
			alarm.setAlarmTimeMinutes(alarm.getSnoozedAlarmTimeMinutes());
			alarm.setLabel(alarm.getSnoozeLabel());

		}
		alarm.setSnoozing(false);

		if (alarm.getAlarmRepeat().isEmpty()) {
			alarm.setActive(false);
		}

		AlarmDataHelper.saveAlarms(this);

		AlarmsSetter.setAlarms(this);
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();

	}

	@Override
	public void snoozeAlarm(int snoozedAlarmPosition) {
		stopService(intent);

		AlarmModel snoozedAlarm = AlarmDataHelper.getAlarmByPosition(alarmPositionInList, this);

		snoozedAlarm.setRinging(false);

		AlarmDataHelper.saveAlarms(this);

		AlarmsSetter.setAlarms(this, snoozedAlarmPosition);

		Toast alarmToast = Toast.makeText(this, "snoozing for " + snoozedAlarm.getSnoozeTime() + " minutes", Toast.LENGTH_LONG);
		View view = alarmToast.getView();
		TextView text = (TextView) view.findViewById(android.R.id.message);
		text.setBackgroundColor(Color.TRANSPARENT);
		alarmToast.show();

		finish();

	}

}
