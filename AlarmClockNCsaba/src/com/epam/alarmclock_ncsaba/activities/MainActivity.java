package com.epam.alarmclock_ncsaba.activities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.alarm_manage.AlarmsSetter;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.fragments.AlarmListFragment;
import com.epam.alarmclock_ncsaba.fragments.AlarmSettingsFragment;
import com.epam.alarmclock_ncsaba.fragments.AnalogeClockFragment;
import com.epam.alarmclock_ncsaba.interfaces.ICalBackToMainActivity;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;

public class MainActivity extends FragmentActivity implements ICalBackToMainActivity {

	private static final String ALARMPOSITION = "alarmposition";
	private static final String SETTINGSVISIBLE = "settingsvisible";

	private AlarmListFragment mainFragment;
	private AlarmSettingsFragment settingFragment;
	private Thread myThread;

	private int alarmPosition;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("Csacsi debug", "MainActivity onCreate called");
		setContentView(R.layout.container_main_fragment_);
		
		alarmPosition = -1;

		mainFragment = new AlarmListFragment();

		initFragments(savedInstanceState);

	}

	private void startWakeUpActivityIfIsRinging() {

		ArrayList<AlarmModel> activeAlarms = AlarmDataHelper.getAlarms(this);

		for (int i = 0; i < activeAlarms.size(); i++) {
			if (activeAlarms.get(i).isRinging() || activeAlarms.get(i).isSnoozing()) {
				this.finish();
				Intent intent = new Intent(this, WakeUpActivity.class);
				intent.putExtra(AlarmsSetter.LAST_SET_ALARM_POSITION, i);
				startActivity(intent);				
				return;
			}
		}
	}

	private void initFragments(Bundle savedInstanceState) {
		if (savedInstanceState == null)
			getSupportFragmentManager().beginTransaction().replace(R.id.main_container, mainFragment, AlarmListFragment.TAG).commit();
		else {

			alarmPosition = savedInstanceState.getInt(ALARMPOSITION);

			if (savedInstanceState.getBoolean(SETTINGSVISIBLE, false) && findViewById(R.id.secondary_container) == null) {
				setSettingsFragment(alarmPosition, false);
				return;

			} else {
				getSupportFragmentManager().beginTransaction().replace(R.id.main_container, mainFragment, AlarmListFragment.TAG).commit();
			}

		}
		if (findViewById(R.id.secondary_container) != null) {

			initTabletView();
		}
	}

	private void initTabletView() {

		if (alarmPosition == -1) {

			getSupportFragmentManager().beginTransaction().replace(R.id.secondary_container, AnalogeClockFragment.newInstance()).commit();

		} else {
			getSupportFragmentManager().popBackStack();
			setSettingsFragment(alarmPosition, false);
		}

	}

	@Override
	protected void onResume() {		
		startWakeUpActivityIfIsRinging();
		myThread = null;
		Runnable myRunnableThread = new CountDownRunner();
		myThread = new Thread(myRunnableThread);
		myThread.start();
		Log.d("Csacsi debug", "onResume: start Thread");
		super.onResume();
	}
		
	@Override
	protected void onPause() {
		Log.d("Csacsi debug", "mainActivity onPaused called");
		AlarmDataHelper.saveAlarms(this);
		Log.d("Csacsi debug", "database saved, alarm size: " + AlarmDataHelper.getAlarms(this).size());
		AlarmModel alarmByPosition = AlarmDataHelper.getAlarmByPosition(0, this);
		Log.d("Csacsi debug", "onPause 0-dik elem: "+alarmByPosition.getAlarmTone()+", wake up type: "+alarmByPosition.getWakeUpType());
		myThread.interrupt();
		Log.d("Csacsi debug", "onPause: Thread Interrupt");
		super.onPause();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		outState.putInt(ALARMPOSITION, alarmPosition);

		AlarmSettingsFragment myFragment = (AlarmSettingsFragment) getSupportFragmentManager().findFragmentByTag(AlarmSettingsFragment.TAG);

		if (myFragment != null && myFragment.isVisible() && findViewById(R.id.secondary_container) == null) {

			outState.putBoolean(SETTINGSVISIBLE, true);
		}

		super.onSaveInstanceState(outState);
	}

	public void setSettingsFragment(int position, boolean putToBackstack) {
		alarmPosition = position;
		settingFragment = new AlarmSettingsFragment();

		Bundle args = new Bundle();
		args.putInt(AlarmSettingsFragment.ARG_SETTINGS, position);

		settingFragment.setArguments(args);

		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

		if (findViewById(R.id.secondary_container) != null) {

			transaction.replace(R.id.secondary_container, settingFragment, AlarmSettingsFragment.TAG).commit();

		} else {

			transaction.replace(R.id.main_container, settingFragment, AlarmSettingsFragment.TAG);

			if (putToBackstack)
				transaction.addToBackStack(null);

			transaction.commit();
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					timeUpdater();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				} catch (Exception e) {
				}
			}
		}

	}

	public void timeUpdater() {
		runOnUiThread(new Runnable() {
			public void run() {
				try {

					Calendar c = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
					String strDate = sdf.format(c.getTime());
					if (mainFragment != null) {
						mainFragment.setCurrentTime(strDate);

					}

				} catch (Exception e) {
				}
			}
		});
	}

	@Override
	public void alarmDeleted() {
		if (findViewById(R.id.secondary_container) != null) {

			getSupportFragmentManager().beginTransaction().replace(R.id.secondary_container, AnalogeClockFragment.newInstance()).commit();

		}

	}
}
