package com.epam.alarmclock_ncsaba.settings_menu;

import java.util.Calendar;
import java.util.Set;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.alarm_manage.AlarmsSetter;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.fragments.AlarmSettingsFragment;
import com.epam.alarmclock_ncsaba.pojos.Days;
import com.epam.alarmclock_ncsaba.pojos.DismissType;
import com.epam.alarmclock_ncsaba.pojos.WakeUpType;

public class SettingsDialogFactory {

	private static AlertDialog mDialog;
	private static RadioButton rbFourth;
	private static RadioButton rbFirs;
	private static RadioButton rbSecond;
	private static RadioButton rbThird;
	private static CheckBox chMon;
	private static CheckBox chTue;
	private static CheckBox chWed;
	private static CheckBox chThu;
	private static CheckBox chFri;
	private static CheckBox chSat;
	private static CheckBox chSun;
	private static View dialogView;
	private static AlertDialog.Builder builder;
	private static LayoutInflater inflater;
	private static android.content.DialogInterface.OnClickListener mylistenerD;
	private static Spinner spinnerRepeat;
	private static EditText editTLabel;

	public static Dialog createDialog(final int alarmPositionInList, final Context context, final int dialogType) {

		builder = new AlertDialog.Builder(context);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		switch (dialogType) {
			case SettingsListFactory.SET_SNOOZE_TIME :

				dialogView = inflater.inflate(R.layout.dialog_with_radiogroup, null);
				setMyDialogOnClickListener(alarmPositionInList, context, dialogType);
				setDialgogBuilder(R.string.done, R.string.select_snooze_time);
				findRadioGroup(dialogView);
				setAlreadySelectedSnooze(alarmPositionInList, context);
				break;

			case SettingsListFactory.SET_QUIZ_REPEATING :
				dialogView = inflater.inflate(R.layout.dialog_set_quiz_repeat, null);
				spinnerRepeat = (Spinner) dialogView.findViewById(R.id.spinnerQuizRepeat);
				spinnerRepeat.setSelection(AlarmDataHelper.getAlarms(context).get(alarmPositionInList).getQuizRepeat() - 1);
				setMyDialogOnClickListener(alarmPositionInList, context, dialogType);
				setDialgogBuilder(R.string.done, R.string.select_new_quiz_repeat_times);
				findRadioGroup(dialogView);
				break;

			case SettingsListFactory.SET_ALARM_REPEAT_DAYS :

				dialogView = inflater.inflate(R.layout.dialog_days_chooser, null);
				setMyDialogOnClickListener(alarmPositionInList, context, dialogType);
				setDialgogBuilder(R.string.done, R.string.select_repeat_days);
				findDaysCheckboxes(dialogView);
				setAlreadySelectedDays(alarmPositionInList, context);

				break;
			case SettingsListFactory.SET_DISSMISS_METHOD :

				dialogView = inflater.inflate(R.layout.dialog_with_radiogroup, null);
				setMyDialogOnClickListener(alarmPositionInList, context, dialogType);
				setDialgogBuilder(R.string.done, R.string.select_wake_up_type);
				findRadioGroup(dialogView);
				setAlreadySelectedDissmissType(alarmPositionInList, context);
				break;
			case SettingsListFactory.SET_ALARM_LABEL :

				dialogView = inflater.inflate(R.layout.dialog_set_label, null);
				editTLabel = (EditText) dialogView.findViewById(R.id.editTextLabel);
				editTLabel.setText(AlarmDataHelper.getAlarms(context).get(alarmPositionInList).getLabel());
				editTLabel.setSelectAllOnFocus(true);
				
				setMyDialogOnClickListener(alarmPositionInList, context, dialogType);
				setDialgogBuilder(R.string.done, R.string.set_alarm_label);
				 editTLabel.clearFocus();

				break;
			case SettingsListFactory.SET_WAKE_UP_TYPE :

				dialogView = inflater.inflate(R.layout.dialog_with_radiogroup, null);
				findRadioGroup(dialogView);
				rbFourth.setVisibility(View.GONE);
				setMyDialogOnClickListener(alarmPositionInList, context, dialogType);
				setDialgogBuilder(R.string.done, R.string.set_wake_up_type);
				setAlreadySelectedWakeUpType(alarmPositionInList, context);

				break;

			default :
				break;
		}

		mDialog.show();
		return mDialog;

	}
	private static void dialogOnClick(final int alarmPositionInList, final Context context, int dialogType) {
		switch (dialogType) {
			case SettingsListFactory.SET_SNOOZE_TIME :
				setSnoozeTime(alarmPositionInList, context);
				break;
			case SettingsListFactory.SET_QUIZ_REPEATING :
				AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setQuizRepeat(spinnerRepeat.getSelectedItemPosition() + 1);
				AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setNewAlarm(false);
				break;
			case SettingsListFactory.SET_ALARM_REPEAT_DAYS :
				setDays(alarmPositionInList, context);
				break;
			case SettingsListFactory.SET_DISSMISS_METHOD :
				setDissmissType(alarmPositionInList, context);
				break;
			case SettingsListFactory.SET_ALARM_LABEL :
			   
				AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setLabel(editTLabel.getText().toString());
				AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setNewAlarm(false);

				break;
			case SettingsListFactory.SET_WAKE_UP_TYPE :
				setWakeUpType(alarmPositionInList, context);
				break;

		}
		
		

	}
	private static void setMyDialogOnClickListener(final int alarmPositionInList, final Context context, final int dialogType) {
		mylistenerD = new android.content.DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (DialogInterface.BUTTON_POSITIVE == which) {
					dialogOnClick(alarmPositionInList, context, dialogType);
					mDialog.dismiss();
					AlarmSettingsFragment.sendMessage(alarmPositionInList + "");
				}
			}

		};
	}

	private static void setDialgogBuilder(int positiveButtonStringResorceId, int titelStringResourceId) {
		builder.setPositiveButton(positiveButtonStringResorceId, mylistenerD);
		builder.setTitle(titelStringResourceId);
		mDialog = builder.setView(dialogView).create();
	}

	public static Dialog createTimePickerDialog(final int alarmPositionInList, final Context context) {

		int hour = 0;
		int minute = 0;

		if (AlarmDataHelper.getAlarms(context).get(alarmPositionInList).isNewAlarm()) {
			Calendar mcurrentTime = Calendar.getInstance();
			hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
			minute = mcurrentTime.get(Calendar.MINUTE);
		} else {
			hour = AlarmDataHelper.getAlarms(context).get(alarmPositionInList).getAlarmTimeHour();
			minute = AlarmDataHelper.getAlarms(context).get(alarmPositionInList).getAlarmTimeMinutes();
		}

		TimePickerDialog mTimePicker;

		mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
			@Override
			public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
				AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setAlarmTimeHour(selectedHour);
				AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setAlarmTimeMinutes(selectedMinute);
				AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setNewAlarm(false);

				AlarmSettingsFragment.sendMessage(alarmPositionInList + "");

			}
		}, hour, minute, true);
		mTimePicker.setTitle("Set alarm time");
		mTimePicker.show();

		return mTimePicker;

	}

	private static void findDaysCheckboxes(View daysChooser) {
		chMon = (CheckBox) daysChooser.findViewById(R.id.checkBoxMon);
		chTue = (CheckBox) daysChooser.findViewById(R.id.checkBoxTue);
		chWed = (CheckBox) daysChooser.findViewById(R.id.checkBoxWed);
		chThu = (CheckBox) daysChooser.findViewById(R.id.checkBoxThu);
		chFri = (CheckBox) daysChooser.findViewById(R.id.checkBoxFri);
		chSat = (CheckBox) daysChooser.findViewById(R.id.checkBoxSat);
		chSun = (CheckBox) daysChooser.findViewById(R.id.checkBoxSun);
	}

	private static void findRadioGroup(final View setDissmissType) {

		rbFirs = (RadioButton) setDissmissType.findViewById(R.id.rb_first);
		rbSecond = (RadioButton) setDissmissType.findViewById(R.id.rb_second);
		rbThird = (RadioButton) setDissmissType.findViewById(R.id.rb_third);
		rbFourth = (RadioButton) setDissmissType.findViewById(R.id.rb_fourth);
	}

	public static void setAlreadySelectedDays(int alarmPositionInList, Context context) {
		Set<Days> alreadySelectedDays = AlarmDataHelper.getAlarms(context).get(alarmPositionInList).getAlarmRepeat();

		if (alreadySelectedDays.contains(Days.MONDAY)) {
			chMon.setChecked(true);
		}
		if (alreadySelectedDays.contains(Days.TUESDAY)) {
			chTue.setChecked(true);
		}
		if (alreadySelectedDays.contains(Days.WEDNESDAY)) {
			chWed.setChecked(true);
		}
		if (alreadySelectedDays.contains(Days.THURSDAY)) {
			chThu.setChecked(true);
		}
		if (alreadySelectedDays.contains(Days.FRIDAY)) {
			chFri.setChecked(true);
		}
		if (alreadySelectedDays.contains(Days.SATURDAY)) {
			chSat.setChecked(true);
		}
		if (alreadySelectedDays.contains(Days.SUNDAY)) {
			chSun.setChecked(true);
		}
	}

	public static void setAlreadySelectedDissmissType(int alarmPositionInList, Context context) {

		switch (AlarmDataHelper.getAlarms(context).get(alarmPositionInList).getDismissType()) {
			case MATH :
				rbFirs.setChecked(true);
				break;
			case PICTURE :
				rbSecond.setChecked(true);
				break;
			case SHAKE :
				rbThird.setChecked(true);
				break;
			case BUTTON :
				rbFourth.setChecked(true);
				break;

			default :
				rbFourth.setChecked(true);
				break;
		}

	}

	public static void setAlreadySelectedSnooze(int alarmPositionInList, Context context) {

		rbFirs.setText(R.string._5_minutes);
		rbSecond.setText(R.string._10_minutes);
		rbThird.setText(R.string._15_minutes);
		rbFourth.setText(R.string._20_minutes);

		switch (AlarmDataHelper.getAlarms(context).get(alarmPositionInList).getSnoozeTime()) {
			case 5 :
				rbFirs.setChecked(true);
				break;
			case 10 :
				rbSecond.setChecked(true);
				break;
			case 15 :
				rbThird.setChecked(true);
				break;
			case 20 :
				rbFourth.setChecked(true);
				break;

			default :
				rbFirs.setChecked(true);
				break;
		}

	}
	public static void setAlreadySelectedWakeUpType(int alarmPositionInList, Context context) {

		rbFirs.setText(R.string.alarm_sound);
		rbSecond.setText(R.string.vibrating);
		rbThird.setText(R.string.sound_and_vibrating);

		switch (AlarmDataHelper.getAlarms(context).get(alarmPositionInList).getWakeUpType()) {
			case RING :
				rbFirs.setChecked(true);
				break;
			case VIBRATE :
				rbSecond.setChecked(true);
				break;
			case BOTH :
				rbThird.setChecked(true);
				break;

			default :
				rbFirs.setChecked(true);
				break;
		}

	}

	public static void setDissmissType(int alarmPositionInList, Context context) {

		if (rbFirs.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setDismissType(DismissType.MATH);
		}
		if (rbSecond.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setDismissType(DismissType.PICTURE);
		}
		if (rbThird.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setDismissType(DismissType.SHAKE);
		}
		if (rbFourth.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setDismissType(DismissType.BUTTON);
		}
		AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setNewAlarm(false);

	}

	public static void setSnoozeTime(int alarmPositionInList, Context context) {

		if (rbFirs.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setSnoozeTime(5);
		}
		if (rbSecond.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setSnoozeTime(10);
		}
		if (rbThird.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setSnoozeTime(15);
		}
		if (rbFourth.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setSnoozeTime(20);
		}
		AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setNewAlarm(false);

	}
	public static void setWakeUpType(int alarmPositionInList, Context context) {

		if (rbFirs.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setWakeUpType(WakeUpType.RING);
		}
		if (rbSecond.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setWakeUpType(WakeUpType.VIBRATE);
		}
		if (rbThird.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setWakeUpType(WakeUpType.BOTH);
		}
		AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setNewAlarm(false);

	}

	public static void setDays(int alarmPositionInList, Context context) {
		AlarmDataHelper.getAlarms(context).get(alarmPositionInList).cleareRepeatDays();

		if (chMon.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).addAlarmRepeat(Days.MONDAY);
		}
		if (chTue.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).addAlarmRepeat(Days.TUESDAY);
		}
		if (chWed.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).addAlarmRepeat(Days.WEDNESDAY);
		}
		if (chThu.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).addAlarmRepeat(Days.THURSDAY);
		}
		if (chFri.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).addAlarmRepeat(Days.FRIDAY);
		}
		if (chSun.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).addAlarmRepeat(Days.SUNDAY);
		}
		if (chSat.isChecked()) {
			AlarmDataHelper.getAlarms(context).get(alarmPositionInList).addAlarmRepeat(Days.SATURDAY);
		}

		AlarmDataHelper.getAlarms(context).get(alarmPositionInList).setNewAlarm(false);
	}

}
