package com.epam.alarmclock_ncsaba.settings_menu;

import java.util.ArrayList;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.datahelper.AlarmDataHelper;
import com.epam.alarmclock_ncsaba.pojos.AlarmModel;

public class SettingsListFactory {

	public static final int SET_WAKE_UP_TYPE = 7;
	public static final int SET_SNOOZE_TIME = 6;
	public static final int SET_QUIZ_REPEATING = 5;
	public static final int SET_DISSMISS_METHOD = 4;
	public static final int SET_RINGTONE_TYPE = 3;
	public static final int SET_ALARM_LABEL = 2;
	public static final int SET_ALARM_REPEAT_DAYS = 1;
	public static final int SET_ALARM_TIME = 0;
	private static ArrayList<String[]> settings;

	public static ArrayList<String[]> createSettingsList(int alarmPositionInList, Context context) {
		AlarmModel alarmSetting = AlarmDataHelper.getAlarms(context).get(alarmPositionInList);
		settings = new ArrayList<String[]>();
		if (alarmSetting == null) {
			settings.add(new String[]{context.getResources().getString(R.string.choose_or_make_an_alarm), "", null});
		} else {
			Uri ringtoneUri = Uri.parse(alarmSetting.getAlarmTone());
			Ringtone ringtone = RingtoneManager.getRingtone(context, ringtoneUri);
			String name = ringtone.getTitle(context);
			settings.add(new String[]{context.getResources().getString(R.string.time), alarmSetting.getAlarmTimeString(), null});
			settings.add(new String[]{context.getResources().getString(R.string.repeat), alarmSetting.getAlarmRepeatString(context), null});
			settings.add(new String[]{context.getResources().getString(R.string.label), alarmSetting.getLabel(), null});
			settings.add(new String[]{context.getResources().getString(R.string.ringtone), name + "", null});
			settings.add(new String[]{context.getResources().getString(R.string.dissmiss_method), alarmSetting.getDismissType() + "", null});
			settings.add(new String[]{context.getResources().getString(R.string.quiz_tries), alarmSetting.getQuizRepeat() + "", null});
			settings.add(new String[]{context.getResources().getString(R.string.snooze), alarmSetting.getSnoozeTime() + "",
					Boolean.toString(alarmSetting.isSnoozable())});
			settings.add(new String[]{context.getResources().getString(R.string.set_wake_up_type), alarmSetting.getWakeUpType() + "", null});
		}

		return settings;
	}

}
