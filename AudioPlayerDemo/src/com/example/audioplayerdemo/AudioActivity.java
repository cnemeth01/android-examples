package com.example.audioplayerdemo;

import java.io.IOException;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class AudioActivity extends Activity {
	
	private MediaPlayer mPlayer;
	private Ringtone r;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_audio);
		mPlayer = new MediaPlayer(); 
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mPlayer.release();
	}
	
	public void csengetes(View view){
		csendet(null);
		Uri uriCsengo = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
		r = RingtoneManager.getRingtone(this, uriCsengo);
		r.play();
	}

	public void eroforras(View view) {
		csendet(null);
		mPlayer = MediaPlayer.create(this, R.raw.maniac);
		mPlayer.start();
	}
	
	public void helyiFajl(View view) {
		csendet(null);
		String musicName = Environment.getExternalStorageDirectory().getAbsolutePath()
				+ "/Music/Fire.mp3";
		Uri uriMusic = Uri.parse(musicName);
		mPlayer = new MediaPlayer();
		mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		try {
			mPlayer.setDataSource(this, uriMusic);
			mPlayer.prepare();
			mPlayer.start();
		} catch (IOException e) {
			Log.w("LOCAL_PLAYER", e);
		}
		
	}

	public void http(View view) {
		csendet(null);
		String url = "http://www.programozas-oktatas.hu/eye_of_the_tiger.mp3";
		mPlayer = new MediaPlayer();
		mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		try {
			mPlayer.setDataSource(url); 
			mPlayer.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer arg0) {
					mPlayer.start();
				}
			});
			mPlayer.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mPlayer) {
					Toast.makeText(AudioActivity.this, "HTTP-lej�tsz�s v�ge", Toast.LENGTH_LONG).show();
				}
			});
			mPlayer.prepareAsync();
		} catch (IOException e) {
			Log.w("HTTP_PLAYER", e);
		}
	}
	
	public void csendet(View view) {
		if (mPlayer != null) mPlayer.stop();
		if (r != null) r.stop();
	}
	
}
