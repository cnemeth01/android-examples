package com.example.fragmentmultipledynamic;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;

public class FragmentMultipleActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragment_multiple_dynamic);
	}
	
	private void kepvalto(int id) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentMasik f2 = (FragmentMasik)fm.findFragmentByTag("FragmentMasik");
		if (f2 == null) {
			f2 = FragmentMasik.newInstance(id);
		} else {
			f2.setKep(id);
		}
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		ft.replace(R.id.kepframe, f2, "FragmentMasik");
		ft.commit();
		
	}
	
	public void elso(View view) {
		kepvalto(R.drawable.kep6);
	}
	
	public void masodik(View view) {
		kepvalto(R.drawable.kep7);
	}
	
	public void harmadik(View view) {
		kepvalto(R.drawable.kep8);
	}
	
	public void leiras(View view) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentLeiras fLeiras = (FragmentLeiras)fm.findFragmentByTag("FragmentLeiras");
		if (fLeiras == null) {
			fLeiras = new FragmentLeiras();
		}
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		ft.replace(R.id.kepframe, fLeiras, "FragmentLeiras");
		ft.commit();
	}


}
