package com.example.junior_google_map_ncsaba.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.junior_google_map_ncsaba.R;
import com.example.junior_google_map_ncsaba.adapter.OwnAdapter;


public class ListViewFragment extends Fragment implements OnItemClickListener {
    private ListView listView;
   

    private ICallBackToActivity callBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
       
//        String[] data_str = new String[MainActivity.DATA.size()];
//        for (int i = 0; i < MainActivity.DATA.size(); i++) {
//            data_str[i] = MainActivity.DATA.get(i).getName();
//            Log.d("data list name", MainActivity.DATA.get(i).getName());
//        }
//       
        listView = (ListView) rootView.findViewById(R.id.mlistview);
//        listView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, data_str));
       
        listView.setAdapter(new OwnAdapter(container.getContext(), R.layout.list_row));
        listView.setOnItemClickListener(this);
        
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callBack = ((ICallBackToActivity) activity);
       
        
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        callBack.listItemClicked(position);
    }
}
