package com.example.junior_google_map_ncsaba.google;

public class Place {

    private String keyword;
    private double lat;    
    private double lng;
    private int radius;
     String name;
    
    public Place(){
                       
    }
    public String getName() {
        return name;
    }
    public void name(String name) {
        this.name = name;
    }
    public Place(String keyword, double lat, double lng, int radius, String name) {
        this.name=name;
        this.keyword = keyword;
        this.lat = lat;
        this.lng = lng;
        this.radius = radius;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

}
