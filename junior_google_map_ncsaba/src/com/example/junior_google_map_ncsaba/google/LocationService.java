package com.example.junior_google_map_ncsaba.google;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.google.gson.Gson;
import com.jsonrespone.dao.JsonResponseDao;

public class LocationService extends Thread {

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=";

    // KEY!
    private static final String API_KEY = "AIzaSyBDJRsgWb_RkzXi14j8pPC81cuJXJaWMnc";

    private CallBackInterface callBack;

    public LocationService(CallBackInterface c) {
        this.callBack = c;
    }

    @Override
    public void run() {

        request();
    }

    private void request() {
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE);
            sb.append("47.4656417,19.1274166");
            sb.append("&types=hospital");
            sb.append("&radius=5000");
            sb.append("&key=" + URLEncoder.encode(API_KEY, "UTF-8"));

            HttpClient httpclient = new DefaultHttpClient();

            HttpGet request = new HttpGet();
            URI website = new URI(sb.toString());
            request.setURI(website);
            HttpResponse response = httpclient.execute(request);
            if (response.getStatusLine().getStatusCode() == 200) {
                //HTTP OK a valasz...
                BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                StringBuilder responseBuilder = new StringBuilder();

                String read;
                while ((read = in.readLine()) != null) {
                    responseBuilder.append(read);
                }
                String jsonResponse = responseBuilder.toString();

                JSONObject obj = new JSONObject(jsonResponse);
                JSONArray sexy = (JSONArray) obj.get("results");
                System.out.println(sexy.toString());

                Gson gson = new Gson();
                List<JsonResponseDao> results = Arrays.asList(gson.fromJson(sexy.toString(), JsonResponseDao[].class));
                
                if (results != null && results.size() > 0) {
                    callBack.success(results);
                }
                for (JsonResponseDao result : results) {
                    System.out.println(result.getName());
                }
            }

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }
        //

    }

}
