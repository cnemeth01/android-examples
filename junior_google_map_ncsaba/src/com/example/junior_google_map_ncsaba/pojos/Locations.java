package com.example.junior_google_map_ncsaba.pojos;

public class Locations {
    
    public enum LocType{HOSPITAL,HEALTH,PHARMACY}
    
    private LocType locType;
    private String name;
    private double lat;
    private double longi;

    public Locations(String n, LocType locType, double lat, double longi) {
        this.name = n;
        this.lat = lat;
        this.longi = longi;
        this.locType=locType;
    }

    
    public LocType getLocType() {
        return locType;
    }


    public void setLocType(LocType locType) {
        this.locType = locType;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }

}
