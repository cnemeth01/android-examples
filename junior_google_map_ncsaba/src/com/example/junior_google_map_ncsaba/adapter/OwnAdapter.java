package com.example.junior_google_map_ncsaba.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.junior_google_map_ncsaba.MainActivity;
import com.example.junior_google_map_ncsaba.R;
import com.example.junior_google_map_ncsaba.R.drawable;
import com.example.junior_google_map_ncsaba.pojos.Locations;

public class OwnAdapter extends ArrayAdapter<Locations> {

    private ArrayList<Locations> items = new ArrayList<Locations>();

    public OwnAdapter(Context context, int resource) {
        super(context, resource);
        items = MainActivity.DATA;
        for (Locations element : items) {
            Log.d("lista elem", element.getName());
        }

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Locations getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder vh = null;

        if (convertView == null) {

            LayoutInflater nInflate = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = nInflate.inflate(R.layout.list_row, parent, false);

            vh = new ViewHolder();

            vh.tw = (TextView) convertView.findViewById(R.id.loc_name);
            vh.iw = (ImageView) convertView.findViewById(R.id.loc_image);

            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();

        }

        vh.tw.setText(items.get(position).getName());

        switch (items.get(position).getLocType()) {
        case HOSPITAL:
            vh.iw.setImageResource(drawable.hospital);
            break;
        case HEALTH:
            vh.iw.setImageResource(drawable.health);
            break;
        case PHARMACY:
            vh.iw.setImageResource(drawable.pharmatie);
            break;
        default:
            vh.iw.setImageResource(drawable.hospital);
            break;
        }
        return convertView;
    }

    static class ViewHolder {
        private TextView tw;
        private ImageView iw;

    }
}
