package com.example.junior_google_map_ncsaba.pojos;

import java.io.Serializable;

public class Locations implements Serializable,Comparable<Locations> {
    
   
	private static final long serialVersionUID = 1L;

	
    
    private String locType;
    private String name;
    private double lat;
    private double longi;
    private int distance;


    public Locations(String name, String locType, double lat, double longi,int distance) {
        this.name = name;
        this.lat = lat;
        this.longi = longi;
        this.locType=locType;
        this.distance=distance;
    }

    
    public int getDistance() {
		return distance;
	}


	public void setDistance(int distance) {
		this.distance = distance;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getLocType() {
        return locType;
    }


    public void setLocType(String locType) {
        this.locType = locType;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }


	@Override
	public int compareTo(Locations another) {
		
		if(this.distance < another.getDistance()) return -1;
		if(this.distance == another.getDistance()) return 0;
		if(this.distance > another.getDistance()) return 1;
		
		else return 0;
	}

}
