package com.example.junior_google_map_ncsaba.adapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.junior_google_map_ncsaba.R;
import com.example.junior_google_map_ncsaba.R.drawable;
import com.example.junior_google_map_ncsaba.google.LocationService;
import com.example.junior_google_map_ncsaba.pojos.Locations;

public class OwnAdapter extends ArrayAdapter<Locations> {

    private ArrayList<Locations> items = new ArrayList<Locations>();
    
    public OwnAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public void addAll(Collection<? extends Locations> collection) {    	
    	items.addAll(collection);
    	Collections.sort(items);
    }
    
    public ArrayList<Locations> getItems() {
    	return items;
    }
    
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Locations getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder vh = null;

        if (convertView == null) {

            LayoutInflater nInflate = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = nInflate.inflate(R.layout.list_row, parent, false);

            vh = new ViewHolder();

            vh.tw = (TextView) convertView.findViewById(R.id.loc_name);
            vh.twDistance = (TextView) convertView.findViewById(R.id.loc_distance);
            vh.iw = (ImageView) convertView.findViewById(R.id.loc_image);

            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();

        }

        vh.tw.setText(items.get(position).getName());
        float disFloat=(float)items.get(position).getDistance()/1000;
        vh.twDistance.setText( String.format("%,.2f km",disFloat));
        if (items.get(position).getLocType().equals(LocationService.HOSPITAL)) {
        	vh.iw.setImageResource(drawable.hospital);
        } else if (items.get(position).getLocType().equals(LocationService.HEALTH)) {
        	vh.iw.setImageResource(drawable.health);
        } else if (items.get(position).getLocType().equals(LocationService.PHARMACY)) {
        	vh.iw.setImageResource(drawable.pharmatie);
        }        	
        
        return convertView;
    }

    static class ViewHolder {
        private TextView tw;
        private TextView twDistance;
        private ImageView iw;

    }
}
