package com.example.junior_google_map_ncsaba.fragment;

import java.util.ArrayList;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.junior_google_map_ncsaba.R;
import com.example.junior_google_map_ncsaba.pojos.Locations;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsFragment extends MapFragment implements ICallBackToActivity {

    public static final String ARG_POSITION = "map_position";
    public static final String ARG_MARKERS = "map_markers";
    private GoogleMap map;
    private ArrayList<Locations> loc;

    @Override
    public void onCreate(Bundle savedInstanceState) {
       
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
    	
    	if (getArguments() != null) {
    		loc = (ArrayList<Locations>) getArguments().getSerializable(ARG_MARKERS);
        	if (loc != null) {
        		Log.d(MapsFragment.class.toString(), loc.size()+" marker �rkezett!");
        		showMarkers(loc);
        	}	
        	else
        		Log.d(MapsFragment.class.toString(), "nem �rkezett marker");
        	
        	if (getArguments().containsKey(ARG_POSITION)) {
        		listItemClicked(0, (Locations) getArguments().getSerializable(ARG_POSITION));
        	}
        	
    	}
    	
    	
    	super.onStart();
    }
    
    @Override
    public void listItemClicked(int position, Locations loc) {
      
        if (loc != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(loc.getLat(), loc.getLongi())).zoom(14).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    public void addMarkers(Locations location) {
           map.addMarker(new MarkerOptions()
           				.position(new LatLng(location.getLat(), location.getLongi()))
           				.title(location.getName()));
        
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        Bundle args = getArguments();
//        if (args != null) {
//           
//            double longi=args.getDouble(MainActivity.ARG_LONG);
//            double lat=args.getDouble(MainActivity.ARG_LATI);
//            String name=args.getString(MainActivity.LOC_NAME);
//            if (longi!=0) {
////                addMarkers(longi,lat, name);
//            }
//            
//          
//            listItemClicked(args.getInt(ARG_POSITION), null);
//           
//        }
//    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	initMap();
        super.onActivityCreated(savedInstanceState);
    }
    
    private void initMap() {
        if (map == null) {

            map = getMap();
            if (map != null) {
                
                map.setMyLocationEnabled(true);
                map.getUiSettings().setCompassEnabled(true);// false to disable
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);                
            }

        }
    }
    
    public void showMarkers(ArrayList<Locations> loc) {
    	if (loc != null) {
            for (Locations location : loc) {
            	addMarkers(location);
            }
        }
    }

    @Override
    public void onPause() {
        if (map != null) {
            map.setMyLocationEnabled(false);
            map.getUiSettings().setCompassEnabled(false);
        }

        super.onPause();
    }

}
