package com.example.junior_google_map_ncsaba.fragment;

import com.example.junior_google_map_ncsaba.pojos.Locations;

public interface ICallBackToActivity {

    void listItemClicked(int position, Locations loc);
}
