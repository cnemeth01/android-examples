package com.example.junior_google_map_ncsaba;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.junior_google_map_ncsaba.fragment.ICallBackToActivity;
import com.example.junior_google_map_ncsaba.fragment.ListViewFragment;
import com.example.junior_google_map_ncsaba.fragment.MapsFragment;
import com.example.junior_google_map_ncsaba.google.CallBackInterface;
import com.example.junior_google_map_ncsaba.google.LocationService;
import com.example.junior_google_map_ncsaba.pojos.Locations;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationListener;
import com.jsonrespone.dao.JsonResponseDao;

public class MainActivity extends Activity implements ICallBackToActivity, CallBackInterface, LocationListener, android.location.LocationListener {

	public static final String LOC_NAME = "locname";
	public static final String ARG_LATI = "lati";
	public static final String ARG_LONG = "long";
	private static final String TAG_MAPFRAGMENT = "tag_mapfragment";
	private MapsFragment mapFragment;
	public ListViewFragment listFragment;
	private ArrayList<Locations> markers = new ArrayList<Locations>();
	double mLatitude = 0;
	double mLongitude = 0;
	Location ownLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_container);

		if (findViewById(R.id.container) != null) {
			listFragment = new ListViewFragment();
			getFragmentManager().beginTransaction().add(R.id.container, listFragment).commit();

		}
		if (mLatitude == 0) {
			getCurrentLocation();
			new LocationService(this, this, mLatitude, mLongitude).execute();
		}

	}

	public void getCurrentLocation() {
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

		if (status != ConnectionResult.SUCCESS) { // Google Play Services are
													// not available

			int requestCode = 10;
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
			dialog.show();

		} else { // Google Play Services are available

			// // Getting reference to the SupportMapFragment
			// SupportMapFragment fragment = ( SupportMapFragment)
			// getSupportFragmentManager().findFragmentById(R.id.map);

			// Getting LocationManager object from System Service
			// LOCATION_SERVICE
			LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

			// Creating a criteria object to retrieve provider
			Criteria criteria = new Criteria();

			// Getting the name of the best provider
			String provider = locationManager.getBestProvider(criteria, true);

			// Getting Current Location From GPS
			ownLocation = locationManager.getLastKnownLocation(provider);

			if (ownLocation != null) {
				onLocationChanged(ownLocation);
			}
			locationManager.requestLocationUpdates(provider, 20000, 0, this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void listItemClicked(int position, Locations loc) {

		mapFragment = (MapsFragment) getFragmentManager().findFragmentById(R.id.map);
		listFragment = (ListViewFragment) getFragmentManager().findFragmentById(R.id.mlistview);

		if (mapFragment != null) {
			((MapsFragment) mapFragment).listItemClicked(position, loc);
		} else {

			// Create fragment and give it an argument for the selected article
			Fragment newFragment = new MapsFragment();
			Bundle args = new Bundle();
			args.putSerializable(MapsFragment.ARG_POSITION, loc);
			args.putSerializable(MapsFragment.ARG_MARKERS, markers);

			Log.d(MainActivity.class.toString(), markers.size() + " db marker �tadva!");

			FragmentTransaction transaction = getFragmentManager().beginTransaction().setCustomAnimations(R.animator.card_flip_right_in,
					R.animator.card_flip_right_out, R.animator.card_flip_left_in, R.animator.card_flip_left_out);

			// Replace whatever is in the fragment_container view with this
			// fragment,
			// and add the transaction to the back stack so the user can
			// navigate back
			newFragment.setArguments(args);
			transaction.replace(R.id.container, newFragment);
			transaction.addToBackStack(null);

			// Commit the transaction
			transaction.commit();
		}
	}

	@Override
	public void success(final List<JsonResponseDao> results) {
		// // TODO Auto-generated method stub
		// runOnUiThread(new Runnable() {
		//
		// @Override
		// public void run() {
		// // give data to ListFragment or ListAdapter
		if (results != null) {

			ArrayList<Locations> items = new ArrayList<Locations>();

			for (JsonResponseDao jsonR : results) {
				String distance = "";
				Locations loc = null;
				String type = "";
				for (String s : jsonR.getTypes()) {
					if (s.equals(LocationService.HOSPITAL)) {
						type = LocationService.HOSPITAL;

					} else if (s.equals(LocationService.PHARMACY)) {
						if (type.isEmpty()) {
							type = LocationService.PHARMACY;
						}

					} else if (s.equals(LocationService.HEALTH)) {
						if (type.isEmpty()) {
							type = LocationService.HEALTH;
						}
					} else {
						if (type.isEmpty()) {
							type = LocationService.HEALTH;
						}

					}

				}
				float[] res=new float[3];
				Location.distanceBetween(mLatitude, mLongitude, jsonR.getGeometry().getLocation().getLat(),jsonR.getGeometry().getLocation().getLng(), res);
				
				int destinitonDistance=(int)res[0];
				System.out.println("res distance:"+ destinitonDistance);
			

				loc = new Locations(jsonR.getName(), type, jsonR.getGeometry().getLocation().getLat(), jsonR.getGeometry().getLocation().getLng(),destinitonDistance);
				items.add(loc);
				
			}	

			listFragment.addListItems(items);
			markers.clear();
			markers.addAll(items);
			Log.d(MainActivity.class.toString(), "Markers : " + markers.size());
		}
		// }
		// });
	}

	@Override
	public void onLocationChanged(Location location) {

		mLatitude = location.getLatitude();
		mLongitude = location.getLongitude();

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

}