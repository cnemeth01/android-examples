package com.example.junior_google_map_ncsaba.google;

import java.util.List;

import com.jsonrespone.dao.JsonResponseDao;

public interface CallBackInterface {
    void success(List<JsonResponseDao> results);
}
