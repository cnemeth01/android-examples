package com.example.junior_google_map_ncsaba.google;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.junior_google_map_ncsaba.R;
import com.google.gson.Gson;
import com.jsonrespone.dao.JsonResponseDao;

public class LocationService extends AsyncTask<Integer, Integer,List<JsonResponseDao> >  {

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=";
    public static final String HOSPITAL = "hospital";
    public static final String PHARMACY = "pharmacy";
    public static final String HEALTH = "health";
    
    View alertDialog;
	AlertDialog dialogAlert;
	ProgressBar progressBar;
	TextView textProgress;
	private static final long TWO_SECOND = 2000;
	
   
    // KEY!
    private static final String API_KEY = "AIzaSyBDJRsgWb_RkzXi14j8pPC81cuJXJaWMnc";

    private CallBackInterface callBack;
    List<JsonResponseDao> results;
    Context context;
    double lat;
    double longi;

//    public LocationService(CallBackInterface c, double lat, double longi) {
//        this.callBack = c;
//        this.lat=lat;
//        this.longi =longi;
//    }
//    
    public LocationService(Context context,CallBackInterface c,double lat, double longi) {
        this.callBack = c;
        this.context=context;
        this.lat=lat;
        this.longi =longi;
    }


   

    @Override
	protected void onPreExecute() {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		alertDialog = inflater.inflate(R.layout.alert_dialog, null);

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		dialogAlert =builder.setView(alertDialog).show();
		
		progressBar = (ProgressBar) dialogAlert.findViewById(R.id.progressBar);
		progressBar.setMax(100);
		progressBar.setProgress(0);

		textProgress = (TextView)  dialogAlert.findViewById(R.id.textView1);
		textProgress.setText("HTTP request in progress...");

		super.onPreExecute();
	}

	@Override
	protected List<JsonResponseDao> doInBackground(Integer... params) {
		try {
			StringBuilder sb = new StringBuilder(PLACES_API_BASE);
			sb.append(lat);
			sb.append(",");
			sb.append(longi);
			sb.append("&types=" + URLEncoder.encode(HOSPITAL + "|" + PHARMACY + "|" + HEALTH, "UTF-8"));
			sb.append("&radius=5000");
			sb.append("&key=" + URLEncoder.encode(API_KEY, "UTF-8"));

			HttpClient httpclient = new DefaultHttpClient();

			HttpGet request = new HttpGet();
			URI website = new URI(sb.toString());
			request.setURI(website);
			HttpResponse response = httpclient.execute(request);
			if (response.getStatusLine().getStatusCode() == 200) {
				// HTTP OK a valasz...
				BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuilder responseBuilder = new StringBuilder();

			
				String read;

				while ((read = in.readLine()) != null) {
					responseBuilder.append(read);

				}

				String jsonResponse = responseBuilder.toString();

				JSONObject obj = new JSONObject(jsonResponse);
				synchronized (this) {
					try {
						this.wait(TWO_SECOND);
					} catch (InterruptedException e) {

						e.printStackTrace();
					}
				}

				publishProgress(35);
				JSONArray sexy = (JSONArray) obj.get("results");
				// System.out.println(sexy.toString());

				Gson gson = new Gson();
			results = Arrays.asList(gson.fromJson(sexy.toString(), JsonResponseDao[].class));
				synchronized (this) {
					try {
						this.wait(TWO_SECOND);
					} catch (InterruptedException e) {

						e.printStackTrace();
					}
				}
				publishProgress(75);

				// if (results != null && results.size() > 0) {
				// this.wait(TWO_SECOND);
				// Message m = new Message();
				// m.obj = results;

				// handler.sendMessage(m);
				// callBack.success();
				// }
				// for (JsonResponseDao result : results) {
				// System.out.println(result.getName());
				// for (String s : result.getTypes()) {
				// System.out.println(s);
				// }

				// }
				synchronized (this) {
					try {
						this.wait(TWO_SECOND);
					} catch (InterruptedException e) {

						e.printStackTrace();
					}
				}
			}

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}
		return results;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		System.out.println("progress value" + values[0]);
		textProgress.setText("Json parse in progress...");

		progressBar.setProgress(values[0]);

	}
	@Override
	protected void onPostExecute(List<JsonResponseDao> results) {
		
		if (results!=null && callBack!=null) {
			callBack.success(results);
		}else System.out.println("callback null");
		 dialogAlert.dismiss();
		

        
	}
	

}
