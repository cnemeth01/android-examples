package com.example.fragmentdialog;

import java.util.Calendar;

import android.os.Bundle;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class FragmentDialogActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragment_dialog);
	}
	
	private void kepvalto(int id) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentMasik f2 = (FragmentMasik)fm.findFragmentByTag("FragmentMasik");
		if (f2 == null) {
			f2 = FragmentMasik.newInstance(id);
		} else {
			f2.setKep(id);
		}
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		ft.replace(R.id.kepframe, f2, "FragmentMasik");
		ft.commit();
		
	}
	
	public void elso(View view) {
		kepvalto(R.drawable.kep9);
	}
	
	public void masodik(View view) {
		kepvalto(R.drawable.kep10);
	}
	
	public void harmadik(View view) {
		kepvalto(R.drawable.kep11);
	}
	
	public void leiras(View view) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentLeiras fLeiras = (FragmentLeiras)fm.findFragmentByTag("FragmentLeiras");
		if (fLeiras == null) {
			fLeiras = new FragmentLeiras();
		}
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		ft.replace(R.id.kepframe, fLeiras, "FragmentLeiras");
		ft.commit();
	}
	
    public void selectDate(View view) {
        DialogFragment newFragment = new SelectDateFragment(this);
        newFragment.show(getSupportFragmentManager(), "DatePicker");
    }
    
    public void populateSetDate(int year, int month, int day) {
    	TextView datumTV = (TextView)findViewById(R.id.datum);
    	datumTV.setText(String.format("%d-%02d-%02d", year, month, day));
    }


}

class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
	private FragmentDialogActivity aThis;
	
	public SelectDateFragment(FragmentDialogActivity aThis) {
		this.aThis = aThis;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Calendar calendar = Calendar.getInstance();
		int yy = calendar.get(Calendar.YEAR);
		int mm = calendar.get(Calendar.MONTH);
		int dd = calendar.get(Calendar.DAY_OF_MONTH);
		return new DatePickerDialog(getActivity(), this, yy, mm, dd);
	}
	
	public void onDateSet(DatePicker view, int yy, int mm, int dd) {
		aThis.populateSetDate(yy, mm+1, dd);
	}
}
