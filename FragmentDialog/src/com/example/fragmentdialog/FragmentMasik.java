package com.example.fragmentdialog;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Use the
 * {@link FragmentMasik#newInstance} factory method to create an instance of
 * this fragment.
 * 
 */
public class FragmentMasik extends Fragment {
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_RES_ID = "ARG_RESOURCE_ID";

	private int mResId;

	/**
	 * Use this factory method to create a new instance of this fragment using
	 * the provided parameters.
	 * 
	 * @param param1
	 *            Parameter 1.
	 * @param param2
	 *            Parameter 2.
	 * @return A new instance of fragment FragmentLeiras.
	 */
	// TODO: Rename and change types and number of parameters
	public static FragmentMasik newInstance(int resId) {
		FragmentMasik fragment = new FragmentMasik();
		Bundle args = new Bundle();
		args.putInt(ARG_RES_ID, resId);
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentMasik() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mResId = getArguments().getInt(ARG_RES_ID);
		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.fragment_fragment_masik, container,
				false);
		ImageView imageView = (ImageView)v.findViewById(R.id.kep);
		Bitmap bm = BitmapFactory.decodeResource(getResources(), mResId);
		imageView.setImageBitmap(bm);
		return v;		
	}
	
	public void setKep(int id) {
		ImageView imageView = (ImageView)getView().findViewById(R.id.kep);
		Bitmap bm = BitmapFactory.decodeResource(getResources(), id);
		imageView.setImageBitmap(bm);
	}

}
