package com.example.csaba_bela_nemeth.oldtvscreenimageview;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ImageView;


public class MainActivity extends ActionBarActivity {

    ImageView tvScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvScreen = (ImageView) findViewById(R.id.tvImage);
        bitmapMasking();

    }

    public void bitmapMasking(){
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Starting with Honeycomb, we can load the bitmap as mutable.
            options.inMutable = true;
        }

        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Resources res = getResources();
        Bitmap source = BitmapFactory.decodeResource(res, R.drawable.cleavland_big, options);
        Bitmap bitmap;
        if (source.isMutable()) {
            bitmap = source;
        } else {
            bitmap = source.copy(Bitmap.Config.ARGB_8888, true);
            source.recycle();
        }

        bitmap.setHasAlpha(true);

        Canvas canvas = new Canvas(bitmap);
        Bitmap mask = BitmapFactory.decodeResource(res, R.drawable.tvshape_big);
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(mask, 0, 0, paint);

        mask.recycle();

        tvScreen.setImageBitmap(bitmap);

    }


}
