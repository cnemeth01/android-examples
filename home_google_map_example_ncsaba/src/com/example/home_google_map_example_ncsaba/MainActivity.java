package com.example.home_google_map_example_ncsaba;

import com.example.home_google_map_example_ncsaba.fragments.ICallBackToActivity;
import com.example.home_google_map_example_ncsaba.fragments.ListViewFragment;
import com.example.home_google_map_example_ncsaba.fragments.MapsFragment;
import com.example.home_google_map_example_ncsaba.pojos.Locations;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity implements ICallBackToActivity {

	private Fragment mapFragment;
    private Fragment listFragment;
   
    public static final Locations[] DATA = {

    new Locations("Csabi Lak�s", 47.4280143,19.1646859), new Locations("MH17 Malaizian Airlines", 47.9901174,37.7615206),
        new Locations("New York", 40.7056308,-73.9780035), new Locations("New Zeland", -43.3744881,172.4662705),
        new Locations("Hawaii",21.3094444,-157.8725), new Locations("Zero point", 0.0, 0.0),
       

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);

        if (findViewById(R.id.container) != null) {
            listFragment = new ListViewFragment();
            getFragmentManager().beginTransaction().add(R.id.container, listFragment).commit();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void listItemClicked(int position) {

        mapFragment = (Fragment) getFragmentManager().findFragmentById(R.id.map);
        listFragment = (Fragment) getFragmentManager().findFragmentById(R.id.mlistview);

        if (mapFragment != null) {
            ((MapsFragment) mapFragment).listItemClicked(position);
        } else {

            // Create fragment and give it an argument for the selected article
            Fragment newFragment = new MapsFragment();
            Bundle args = new Bundle();
            args.putInt(MapsFragment.ARG_POSITION, position);
            newFragment.setArguments(args);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        }

    }
}
