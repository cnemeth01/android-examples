package com.example.home_google_map_example_ncsaba.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.home_google_map_example_ncsaba.MainActivity;
import com.example.home_google_map_example_ncsaba.R;

public class ListViewFragment extends Fragment implements OnItemClickListener {
    private ListView listView;

    private ICallBackToActivity callBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        listView = (ListView) rootView.findViewById(R.id.mlistview);

        String[] data_str = new String[MainActivity.DATA.length];

        for (int i = 0; i < MainActivity.DATA.length; i++) {
            data_str[i] = MainActivity.DATA[i].getName();
        }

        listView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, data_str));
        listView.setOnItemClickListener(this);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callBack = ((ICallBackToActivity) activity);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        callBack.listItemClicked(position);
    }
}
