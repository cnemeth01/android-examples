package com.example.home_google_map_example_ncsaba.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.home_google_map_example_ncsaba.MainActivity;
import com.example.home_google_map_example_ncsaba.pojos.Locations;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsFragment extends MapFragment implements ICallBackToActivity {

	public static final String ARG_POSITION = "map_position";
	private GoogleMap map;
	private Integer position_in_array;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (savedInstanceState != null) {
			position_in_array = savedInstanceState.getInt(ARG_POSITION);

		}
		
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void listItemClicked(int position) {
		CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(MainActivity.DATA[position].getLat(), MainActivity.DATA[position].getLongi()))
				.zoom(4).build();
		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

	private void addMarker(Locations loc) {

		// create marker
		MarkerOptions marker = new MarkerOptions().position(new LatLng(loc.getLat(), loc.getLongi())).title(loc.getName());

		// adding marker
		map.addMarker(marker);
	}

	@Override
	public void onStart() {
		super.onStart();
		Bundle args = getArguments();
		if (args != null) {
			listItemClicked(args.getInt(ARG_POSITION));
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);

		initMap(); // adding markers
		for (int i = 0; i < MainActivity.DATA.length; i++) {
			addMarker(MainActivity.DATA[i]);
		}
	}

	private void initMap() {
		
		
		if (map == null) {

			map = getMap();
			if (map != null) {

				map.setMyLocationEnabled(true); 
				
				map.getUiSettings().setCompassEnabled(true);// false to disable
				map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

			}

		}
	}

}
