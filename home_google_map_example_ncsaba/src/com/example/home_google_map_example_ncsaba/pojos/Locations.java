package com.example.home_google_map_example_ncsaba.pojos;

public class Locations {

    private String name;
    private double lat;
    private double longi;

    public Locations(String n, double lat, double longi) {
        this.name = n;
        this.lat = lat;
        this.longi = longi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }

}
