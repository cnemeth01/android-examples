
import javax.inject.Inject;

import dagger.ObjectGraph;
/*
 * CoffeApp is the main Runnable Class * 
 */
public class CoffeeApp implements Runnable {
	//CooffeeMaker Instance is injected with Dagger, the instance is made in DripCoffeModule
    @Inject
	CoffeeMaker coffeeMaker;

	public void run() {
		coffeeMaker.brew();
	}

	public static void main(String[] args) {

		ObjectGraph objectGraph = ObjectGraph.create(new DripCoffeeModule());
		CoffeeApp coffeeApp = objectGraph.get(CoffeeApp.class);
		coffeeApp.run();
	}
}
