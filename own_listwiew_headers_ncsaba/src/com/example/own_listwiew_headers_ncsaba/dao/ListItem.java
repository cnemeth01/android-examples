package com.example.own_listwiew_headers_ncsaba.dao;




public class ListItem {
   	
	private String itemLabel;
	private ItemType itemType;
	private Integer imageId;
	
	public ListItem(String itemLabel, ItemType itemType, Integer imageId) {
		super();
		this.itemLabel = itemLabel;
		this.itemType = itemType;
		this.imageId = imageId;
	}

	public String getItemLabel() {
		return itemLabel;
	}

	public void setItemLabel(String itemLabel) {
		this.itemLabel = itemLabel;
	}

	public ItemType getItemType() {
		return itemType;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public Integer getImageId() {
		return imageId;
	}

	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}
	
	
	
}
