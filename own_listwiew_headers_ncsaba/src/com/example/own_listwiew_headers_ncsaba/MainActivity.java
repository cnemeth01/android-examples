package com.example.own_listwiew_headers_ncsaba;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.own_listwiew_headers_ncsaba.dao.ItemType;
import com.example.own_listwiew_headers_ncsaba.dao.ListItem;

public class MainActivity extends Activity {

	private static final int REQ_NEW_MEMBER = 1;
	ArrayList<ListItem> mListItems = new ArrayList<ListItem>();
	HashMap<String, Integer> mFooterMap = new HashMap<String, Integer>();
	ListView mListView;
	private String newMember;
	Adapter listAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mListItems.add(new ListItem("A", ItemType.HEADER, null));
		mListItems.add(new ListItem("Alad�r Zolt�n", ItemType.ITEM,
				R.drawable.ic_launcher));
		mListItems.add(new ListItem("Alm�si L�szl�", ItemType.ITEM,
				R.drawable.ic_launcher));
		mListItems.add(new ListItem("Ajtonyi Tam�s", ItemType.ITEM,
				R.drawable.ic_launcher));
		mListItems.add(new ListItem("Alma G�za", ItemType.ITEM,
				R.drawable.ic_launcher));
		mListItems.add(new ListItem("", ItemType.FOOTER, null));
		mListItems.add(new ListItem("B", ItemType.HEADER, null));
		mListItems.add(new ListItem("Boldis Legolasz", ItemType.ITEM,
				R.drawable.ic_launcher));
		mListItems.add(new ListItem("Balla B�la", ItemType.ITEM,
				R.drawable.ic_launcher));
		mListItems.add(new ListItem("Balogh Istv�n", ItemType.ITEM,
				R.drawable.ic_launcher));
		mListItems.add(new ListItem("", ItemType.FOOTER, null));
		mListItems.add(new ListItem("C", ItemType.HEADER, null));
		mListItems.add(new ListItem("Cica Korn�l", ItemType.ITEM,
				R.drawable.ic_launcher));
		mListItems.add(new ListItem("Czutor Jen�", ItemType.ITEM,
				R.drawable.ic_launcher));
		mListItems.add(new ListItem("", ItemType.FOOTER, null));
		mListItems.add(new ListItem("S", ItemType.HEADER, null));
		mListItems.add(new ListItem("Szab� Zolt�n", ItemType.ITEM,
				R.drawable.ic_launcher));
		mListItems.add(new ListItem("", ItemType.FOOTER, null));

		buildFooters();

		mListView = (ListView) findViewById(R.id.main_listview);
		listAdapter = new Adapter(this,
				android.R.layout.simple_list_item_1);
		mListView.setAdapter(listAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		 MenuInflater inflater = getMenuInflater();
		    inflater.inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId()==R.id.new_member) {
			Intent i= new Intent(this,NewMemberActivity.class);
			startActivityForResult(i, REQ_NEW_MEMBER);
		}
		return super.onOptionsItemSelected(item);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

	    if (requestCode == REQ_NEW_MEMBER) {
	        if(resultCode == RESULT_OK){
	            newMember=data.getStringExtra(NewMemberActivity.NEW_MEMBER_RESULT);
	            mListItems.add(new ListItem(newMember, ItemType.ITEM,
	    				R.drawable.ic_launcher));
	           listAdapter.notifyDataSetChanged();
	            
	        }
	        if (resultCode == RESULT_CANCELED) {
	            //Write your code if there's no result
	        }
	    }
	}

	public void buildFooters() {
		for (ListItem listItem : mListItems) {

			if (listItem.getItemType() == ItemType.ITEM) {
				String firstLetter = listItem.getItemLabel().substring(0, 1);
				if (mFooterMap.containsKey(firstLetter)) {
					int value = mFooterMap.get(firstLetter);
					mFooterMap.put(firstLetter, value + 1);
				} else {
					mFooterMap.put(firstLetter, 1);
				}
			}
		}
	}

	private class Adapter extends ArrayAdapter<ListItem> {

		public Adapter(Context context, int resource) {
			super(context, resource);
			// TODO Auto-generated constructor stub
		}

		@Override
		public ListItem getItem(int position) {
			return mListItems.get(position);
		}

		@Override
		public int getCount() {

			return mListItems.size();
		}

		@Override
		public int getItemViewType(int position) {
			return mListItems.get(position).getItemType().ordinal();
		}

		@Override
		public int getViewTypeCount() {
			return 3;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder vh = new ViewHolder();

			ListItem item = getItem(position);
			if (convertView == null) {

				switch (item.getItemType()) {
				case ITEM:
					convertView = LayoutInflater.from(getContext()).inflate(
							R.layout.list_item, null);
					vh.tw = (TextView) convertView
							.findViewById(R.id.item_textview);
					vh.iw = (ImageView) convertView
							.findViewById(R.id.item_imageview);
					break;
				case HEADER:
					convertView = LayoutInflater.from(getContext()).inflate(
							R.layout.list_header, null);
					vh.tw = (TextView) convertView
							.findViewById(R.id.header_textview);
					break;
				case FOOTER:
					convertView = LayoutInflater.from(getContext()).inflate(
							R.layout.list_footer, null);
					vh.tw = (TextView) convertView
							.findViewById(R.id.footer_textview);
					break;

				default:

					convertView = LayoutInflater.from(getContext()).inflate(
							R.layout.list_item, null);
					vh.tw = (TextView) convertView
							.findViewById(R.id.item_textview);
					vh.iw = (ImageView) convertView
							.findViewById(R.id.item_imageview);
					break;
				}

				convertView.setTag(vh);
			} else {
				vh = (ViewHolder) convertView.getTag();
			}

			switch (item.getItemType()) {
			case HEADER:
				vh.tw.setText(item.getItemLabel());
				break;

			case ITEM:
				vh.tw.setText(item.getItemLabel());
				vh.iw.setImageDrawable(getResources().getDrawable(
						item.getImageId()));
				break;

			case FOOTER:
				Integer counter = mFooterMap.get(mListItems.get(position - 1)
						.getItemLabel().substring(0, 1));
				vh.tw.setText(footerBuilder(counter,position,getItem(position-1)));
					
				break;
			}

			return convertView;
		}

	}

	private static class ViewHolder {
		TextView tw;
		ImageView iw;
	}

	public String footerBuilder(Integer counter, int position, ListItem item ) {
		
		StringBuilder sb=new StringBuilder();
		sb.append(counter);
		sb.append(" ");
		sb.append("db ");
		sb.append(item.getItemLabel().substring(0, 1));
		sb.append(" elem van.");
		
		
		return sb.toString();
	}
}
