package com.example.own_listwiew_headers_ncsaba;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class NewMemberActivity extends Activity {

	public static final String NEW_MEMBER_RESULT = "result";
	private Button btSave;
	private Button btCancel;
	private EditText etNewMember;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_member);
	
	initUI();
	
	}
	private void initUI() {
		
		btSave=(Button) findViewById(R.id.bt_save);
		btCancel =(Button) findViewById(R.id.bt_ok);
		etNewMember=(EditText) findViewById(R.id.et_new_member);
		
		btSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				String newMember= nameStringValide(etNewMember.getText().toString());
				
				
				if (etNewMember!=null) {
					
					Intent returnIntent = new Intent();
					returnIntent.putExtra(NEW_MEMBER_RESULT,newMember);
					setResult(RESULT_OK,returnIntent);
					finish();
				}
				
			}
		});
		
		btCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			finish();	
				
			}
		});
		
		
	}
	protected String nameStringValide(String string) {
		
			return string;
		
	}

	
	
}
