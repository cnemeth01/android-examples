package com.example.ownmp3player;

import java.io.IOException;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MusicPlayService extends Service implements OnBufferingUpdateListener, OnPreparedListener, OnCompletionListener, OnErrorListener,
    OnSeekCompleteListener, OnInfoListener {

    private String name = "MyMusicPlayerService";
    private MediaPlayer myMediaPlayer;
    private String strMusic = "Gnarls Barkley - Crazy.mp3";
    private static final String TAG = "MusicPlayerService";
    private LocalBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {

        public MusicPlayService getServiceInstance() {
            return MusicPlayService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("onStartCommand", "onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");

        myMediaPlayer = new MediaPlayer();

        myMediaPlayer.setOnPreparedListener(this);
        myMediaPlayer.setOnCompletionListener(this);
        myMediaPlayer.setOnErrorListener(this);
        myMediaPlayer.setOnSeekCompleteListener(this);
        myMediaPlayer.setOnInfoListener(this);
        myMediaPlayer.reset();

//        new Thread() {
//
//            public void run() {
                if (!myMediaPlayer.isPlaying()) {

                    try {
                        ;
                        myMediaPlayer.setDataSource(getAssets().openFd(strMusic).getFileDescriptor(), getAssets().openFd(strMusic).getStartOffset(),
                                getAssets().openFd(strMusic).getLength());

                        Log.e("onCreate", "MediaPlayerPrepareAsync");
                        myMediaPlayer.prepareAsync();
                        Log.e("onCreate", "MediaPlayerStart");

                    } catch (IllegalArgumentException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (SecurityException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

//            };
//        }.start();

        super.onCreate();
    }

    public void playMusic() {
   
        if (!myMediaPlayer.isPlaying()) {
            Log.e("onStartCommand", "MediaPlayerStart");
            myMediaPlayer.start();
        }
    }

    public void stopMusic() {
       
        if (myMediaPlayer.isPlaying()) {
            myMediaPlayer.stop();
        }
    }

    public void pauseMusic() {
        if (myMediaPlayer.isPlaying()) {
            myMediaPlayer.pause();
        }
    }

    @Override
    public void onDestroy() {

        if (myMediaPlayer != null) {
            if (myMediaPlayer.isPlaying()) {
                myMediaPlayer.stop();
            }
            myMediaPlayer.release();
        }

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "onBind");
        return mBinder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Toast.makeText(this, "ERROR WITH MEDIA PLAYER", Toast.LENGTH_LONG).show();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stopMedia();
        stopSelf();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        playMusic();

    }

    private void playMedia() {
        if (!myMediaPlayer.isPlaying()) {
            Log.e("onStartCommand", "MediaPlayerStart");
            myMediaPlayer.start();
        }

    }

    private void stopMedia() {
        if (myMediaPlayer.isPlaying()) {
            myMediaPlayer.stop();
        }

    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        // TODO Auto-generated method stub

    }
}
