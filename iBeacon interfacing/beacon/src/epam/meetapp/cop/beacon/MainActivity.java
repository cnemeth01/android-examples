package epam.meetapp.cop.beacon;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

public class MainActivity extends Activity {

	public static final int CHECKIN_ID = 48382;
	private Handler handler;
	public static final String TAG = "MainActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(TAG, "onCreate");
		setContentView(R.layout.activity_main);
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			if(extras.containsKey(MyApplication.DEVICE_FOUND)){
				//Activity gets intent from the background (by MyApplication), device was found
			}
		}else{
			//Application starts for the first time, additional device search is required
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
