package epam.meetapp.cop.beacon;

import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MyApplication extends Application implements BootstrapNotifier, BeaconConsumer {
	
	public static final String MEETAPP_BEACON_ID = "5c2a2bb7-7149-4a52-b12a-862ec7e54f5c";
	public static final String KONTAKT_BEACON_SPEC = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25";
	public static final String DEVICE_FOUND = "device_found";
	private static final String TAG = ".MyApplication";
	private RegionBootstrap regionBootstrap;
	private BackgroundPowerSaver backgroundPowerSaver;
	private BeaconManager beaconManager;

	@Override
	public void onCreate() {
		super.onCreate();
		Log.e(TAG, "onCreate");
		Region region = new Region("RegionID",Identifier.parse(MEETAPP_BEACON_ID),null,null);
		regionBootstrap = new RegionBootstrap(this, region);
		backgroundPowerSaver = new BackgroundPowerSaver(this);
		setupBeaconManager();
	}
	
	public void setupBeaconManager(){
		beaconManager = BeaconManager.getInstanceForApplication(this);
		BeaconParser kontaktBeaconParser = new BeaconParser().setBeaconLayout(KONTAKT_BEACON_SPEC);
		beaconManager.getBeaconParsers().add(kontaktBeaconParser);
		beaconManager.setBackgroundMode(true);
	}

	@Override
	public void didDetermineStateForRegion(int arg0, Region arg1) {
		Log.e(TAG, "didDetermineStateForRegion");
	}

	@Override
	public void didEnterRegion(Region region) {
		Log.e(TAG, "didEnterRegion");
		Intent intent = new Intent(this, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		Bundle args = new Bundle();
		args.putString(DEVICE_FOUND, "WHATEVER");
		startActivity(intent, args);
	}

	@Override
	public void didExitRegion(Region arg0) {
		Log.e(TAG, "didExitRegion");
	}

	@Override
	public void onBeaconServiceConnect() {
		Log.e(TAG, "onBeaconServiceConnect");
	}

}
