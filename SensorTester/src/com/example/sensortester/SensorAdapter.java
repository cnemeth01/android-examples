package com.example.sensortester;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Sensor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SensorAdapter extends ArrayAdapter<Sensor> {

	SensorTypeMap sensorTypeMap = new SensorTypeMap();
	
	public SensorAdapter(Context context, int resource, List<Sensor> objects) {
		super(context, resource, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Sensor s = getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.sensorlistitem, null);
		}
		TextView nameVersion = (TextView)convertView.findViewById(R.id.sensor_name_version);
		TextView type = (TextView)convertView.findViewById(R.id.sensor_type);
		TextView vendor = (TextView)convertView.findViewById(R.id.sensor_vendor);
		nameVersion.setText(s.getName()+" v"+s.getVersion());
		type.setText(sensorTypeMap.get(s.getType()));
		vendor.setText(s.getVendor());
		return convertView;
	}
	
}
