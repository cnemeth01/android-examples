package com.example.sensortester;

import java.util.HashMap;
import java.util.Map;

import android.hardware.Sensor;
import android.webkit.WebView.FindListener;

public class SensorTypeMap extends HashMap<Integer, String> {

	public SensorTypeMap() {
		super();
		put(Sensor.TYPE_ACCELEROMETER, "ACCELEROMETER");
		put(Sensor.TYPE_AMBIENT_TEMPERATURE, "AMBIENT_TEMPERATURE");
		put(Sensor.TYPE_GAME_ROTATION_VECTOR, "GAME_ROTATION_VECTOR");
		put(Sensor.TYPE_GRAVITY, "GRAVITY");
		put(Sensor.TYPE_GYROSCOPE, "GYROSCOPE");
		put(Sensor.TYPE_GYROSCOPE_UNCALIBRATED, "GYROSCOPE_UNCALIBRATED");
		put(Sensor.TYPE_LIGHT, "LIGHT");
		put(Sensor.TYPE_LINEAR_ACCELERATION, "LINEAR_ACCELERATION");
		put(Sensor.TYPE_MAGNETIC_FIELD, "MAGNETIC_FIELD");
		put(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED, "MAGNETIC_FIELD_UNCALIBRATED");
		put(Sensor.TYPE_ORIENTATION, "ORIENTATION");
		put(Sensor.TYPE_PRESSURE, "PRESSURE");
		put(Sensor.TYPE_PROXIMITY, "PROXIMITY");
		put(Sensor.TYPE_RELATIVE_HUMIDITY, "RELATIVE_HUMIDITY");
		put(Sensor.TYPE_ROTATION_VECTOR, "ROTATION_VECTOR");
		put(Sensor.TYPE_SIGNIFICANT_MOTION, "SIGNIFICANT_MOTION");
		put(Sensor.TYPE_TEMPERATURE, "TEMPERATURE");
	}

	@Override
	public String get(Object key) {
		if (!keySet().contains(key)) {
			return "UNKNOWN #"+key;
		} else {
			return super.get(key);
		}
	}
	
	

}
