package com.example.sensortester;

import java.util.List;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.widget.TextView;

public class SensorActivity extends Activity implements SensorEventListener {

	private List<Sensor> sensors;
	private Sensor sensor;
	private SensorManager mgr;
	private TextView nameVersion;
	private TextView type;
	private TextView vendor;
	private TextView[] sensorTVs = new TextView[3];
	private SensorTypeMap sensorTypeMap = new SensorTypeMap();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sensor);
		mgr = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		sensors = mgr.getSensorList(Sensor.TYPE_ALL);
		Intent intent = getIntent();
		int sensorIndex = intent.getIntExtra("sensor_index", -1);
		nameVersion = (TextView)findViewById(R.id.detail_sensor_name_version);
		type = (TextView)findViewById(R.id.detail_sensor_type);
		vendor = (TextView)findViewById(R.id.detail_sensor_vendor);
		if (sensorIndex != -1) {
			sensor = sensors.get(sensorIndex);
			nameVersion.setText(sensor.getName()+" v"+sensor.getVersion());
			type.setText(sensorTypeMap.get(sensor.getType()));
			vendor.setText(sensor.getVendor());
		}
		sensorTVs[0] = (TextView)findViewById(R.id.detail_sensor_value_0);
		sensorTVs[1] = (TextView)findViewById(R.id.detail_sensor_value_1);
		sensorTVs[2] = (TextView)findViewById(R.id.detail_sensor_value_2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sensor, menu);
		return true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (sensor != null) {
			mgr.unregisterListener(this);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (sensor != null) {
			mgr.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor s, int arg1) {
	}

	@Override
	public void onSensorChanged(SensorEvent se) {
		int len = se.values.length;
		int max = len > 3 ? 3 : len;
		for (int i = 0; i < max; i++) {
			sensorTVs[i].setText(se.values[i]+"");
		}
	}

}
