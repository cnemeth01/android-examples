package com.example.sensortester;

import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.view.Menu;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements AdapterView.OnItemClickListener {

	private ListView listView;
	private List<Sensor> sensors;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		listView = (ListView)findViewById(R.id.listView);
		
		SensorManager mgr = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		sensors = mgr.getSensorList(Sensor.TYPE_ALL);
		SensorAdapter adapter = new SensorAdapter(this, R.layout.sensorlistitem, sensors);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Sensor s = sensors.get(position);
		Intent intent = new Intent(this, SensorActivity.class);
		intent.putExtra("sensor_index", position);
		startActivity(intent);
	}
	
}
