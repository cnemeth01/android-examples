package hu.pasztuhov.android.szamologapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.res.Resources;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends Activity {

	private EditText egyikET;
	private EditText masikET;
	private Spinner muveletSpinner;
	private EditText eredmenyET;
	private static final String TAG = "MainActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		egyikET = (EditText)findViewById(R.id.egyikEditText);
		masikET = (EditText)findViewById(R.id.masikEditText);
		muveletSpinner = (Spinner)findViewById(R.id.muveletSpinner);
		eredmenyET = (EditText)findViewById(R.id.eredmenyEditText);
		Resources res = getResources();
		String[] muvjelek = res.getStringArray(R.array.muvjelek);
		muveletSpinner.setAdapter(new ArrayAdapter<String>(
				this, R.layout.muveletitem, muvjelek));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void szamol(View view) {
		Log.d(TAG, "szamol() meghívása");
		String egyikStr = egyikET.getText().toString();
		String masikStr = masikET.getText().toString();
		int egyik, masik;
		try {
			egyik = Integer.parseInt(egyikStr);
			masik = Integer.parseInt(masikStr);
			String muvelet = (String)muveletSpinner.getSelectedItem();
			int eredmeny = 0;
			if ("+".equals(muvelet)) {
				eredmeny = egyik + masik;
			} else if ("-".equals(muvelet)) {
				eredmeny = egyik - masik;
			} else if ("*".equals(muvelet)) {
				eredmeny = egyik * masik;
			} else if ("/".equals(muvelet)) {
				eredmeny = egyik / masik; 
			}
			eredmenyET.setText(eredmeny + "");
		} catch (NumberFormatException ex) {
			eredmenyET.setText("Hiba a számok konverziójánál");
		}
	}

}
