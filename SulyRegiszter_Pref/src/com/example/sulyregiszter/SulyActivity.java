package com.example.sulyregiszter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SulyActivity extends Activity {

	private EditText datumET;
	private EditText tenyET;
	private WebView webView;
	private static final int REQCODE_OPTIONS = 1;
	private String username;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_suly);

		acquireUsername();

		datumET = (EditText) findViewById(R.id.datumEditText);
		tenyET = (EditText) findViewById(R.id.tenyEditText);
		webView = (WebView) findViewById(R.id.webView);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.loadUrl("http://www.programozas-oktatas.hu/fogyas/webindex.php?u="
				+ username);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setInitialScale(50);
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webView.getSettings().setUseWideViewPort(true);
		webView.requestFocus();
		// outputTV = (TextView) findViewById(R.id.outputTextView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.options, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intent = new Intent(this, OptionsActivity.class);
			startActivityForResult(intent, REQCODE_OPTIONS);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQCODE_OPTIONS) {
			String regiUsername = username;
			acquireUsername();
			if (!username.equals(regiUsername)) {
				webView.loadUrl("http://www.programozas-oktatas.hu/fogyas/webindex.php?u="
						+ username);
			}
			// webView.reload();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		datumET.setText(sdf.format(new Date()));
	}

	public void frissit(View view) {
		webView.reload();
	}

	public void elkuld(View view) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
		
		AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... arg0) {
				try {
					String urlString = "http://www.programozas-oktatas.hu/fogyas/insert.php";
					String urlParameters = "nev=" + username + "&datum="
							+ datumET.getText().toString() + "&teny="
							+ tenyET.getText().toString();
					URL url = new URL(urlString);
					URLConnection conn = url.openConnection();

					conn.setDoOutput(true);
					OutputStreamWriter writer = new OutputStreamWriter(
							conn.getOutputStream());
					writer.write(urlParameters);
					writer.flush();

					String line;
					String output = "";
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(conn.getInputStream()));
					while ((line = reader.readLine()) != null) {
						output += "\n" + line;
					}
					writer.close();
					reader.close();
					// return output;
					return "OK";
				} catch (IOException e) {
					Log.w("UPLOADER", e);
					return e.getMessage();
				}
			}

			@Override
			protected void onPostExecute(String result) {
				if ("OK".equals(result)) {
					Toast.makeText(getApplicationContext(), "Rendben",
							Toast.LENGTH_LONG).show();
					webView.reload();
				} else {
					Toast.makeText(getApplicationContext(), result,
							Toast.LENGTH_LONG).show();
				}
			}
		};
		task.execute();

	}

	private void acquireUsername() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		username = prefs.getString("username", "gipszjakab");
		Toast.makeText(this, "A beállított user: " + username,
				Toast.LENGTH_LONG).show();
	}

}
