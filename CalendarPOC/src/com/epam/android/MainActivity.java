package com.epam.android;

import java.util.Calendar;
import java.util.TimeZone;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);		
	}
	
	
	/*
	 * In order to prompt the user to create a calendar event in their existing calendar, you�ll need to create the appropriate intent.
	 *  This intent will launch the Calendar application with a screen that allows them to create a new event. The user must confirm this 
	 *  event and can add or change any of the event data associated with the event.
	 *  
	 *  NEM TESZI BELE AZ ADATOKAT, A USERNEK KELL KITOLTENI + SAVELNI
	 */
	public void addToCalendar(View view){
		Intent calIntent = new Intent(Intent.ACTION_INSERT); 
		calIntent.setData(CalendarContract.Events.CONTENT_URI);
		startActivity(calIntent);
	}
	
	
	/*
	 * A USER NEM IS LATJA, AUTOMATIKUSAN BESZURJA AZ APP A CALENDARBA A MEGFELELO ADATOKKAL AZ EVENTET
	 */
	public void addToCalendar3(View view){
		
		ContentResolver contentResolver = getContentResolver();
		ContentValues contentValues = new ContentValues();
		
		Calendar eventStart = Calendar.getInstance();
		eventStart.set(2014, 9, 8, 11, 52);
		
		Calendar eventEnd = Calendar.getInstance();
		eventEnd.set(2014, 9, 8, 12, 00);
		
		contentValues.put(CalendarContract.Events.CALENDAR_ID, 1); // must have an id 
		contentValues.put(CalendarContract.Events.TITLE, "Adam birthday");
		contentValues.put(CalendarContract.Events.DESCRIPTION, "We are going to celebrate his birthday");
		contentValues.put(CalendarContract.Events.DTSTART, eventStart.getTimeInMillis());
		contentValues.put(CalendarContract.Events.DTEND, eventEnd.getTimeInMillis());
		contentValues.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
		
		contentResolver.insert(CalendarContract.Events.CONTENT_URI, contentValues);
		
		setAlarmManager();
		
		Toast.makeText(this, "Calendar events have been updated...", Toast.LENGTH_SHORT).show();
	}
	
	private void setAlarmManager() {
		
		
	}

	/*
	 * You can seed the information associated with a calendar event by supplying a number of intent extras.
	 * Extras can be used to describe event details, the date and time of the event, as well as calendar event settings 
	 * like whether the event can be seen by others and whether it is busy or available time. The typical calendar event
	 *  settings are all available as Extras. 
	 *  
	 *  BELETESZI AZ ADATOKAT DE A USERNEK KELL JOVAHAGYNI
	 */
	public void addToCalendar2(View view){	
		Intent calendarIntent = new Intent(Intent.ACTION_INSERT);
		calendarIntent.setType("vnd.android.cursor.item/event");
		calendarIntent.putExtra(Events.TITLE, "Dani's birthday");
		calendarIntent.putExtra(Events.EVENT_LOCATION, "Budapest Sasadi ut 21");
		calendarIntent.putExtra(Events.DESCRIPTION, "We are going to celebrate our friends 25th birthday. Feel free to invite anyone you want.");
		
		Calendar eventStart = Calendar.getInstance();
		eventStart.set(2014,10,9,7,30);
		
		Calendar eventEnd = Calendar.getInstance();
		eventStart.set(2014,10,9,8,30);
		
		calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, eventStart.getTimeInMillis());
		calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, eventEnd.getTimeInMillis());
		
		startActivity(calendarIntent);	
	}
}
