/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.own_styles_exercise_ncsaba;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int my_button_colorlist=0x7f040006;
        public static final int my_button_colorlist_red=0x7f040007;
        public static final int myblack=0x7f040000;
        public static final int myblue=0x7f040003;
        public static final int mygrey=0x7f040005;
        public static final int myorange=0x7f040004;
        public static final int myred=0x7f040002;
        public static final int mywhite=0x7f040001;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Example customization of dimensions originally defined in res/values/dimens.xml
         (such as screen margins) for screens with more than 820dp of available width. This
         would include 7" and 10" devices in landscape (~960dp and ~1280dp respectively).
    
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
    }
    public static final class id {
        public static final int LinearLayout1=0x7f090000;
        public static final int action_settings=0x7f090001;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
    }
    public static final class menu {
        public static final int main=0x7f080000;
    }
    public static final class string {
        public static final int action_settings=0x7f060002;
        public static final int app_name=0x7f060000;
        public static final int click_on_me_handsome=0x7f060006;
        public static final int hello_world=0x7f060001;
        public static final int lorem_ipsum=0x7f060005;
        public static final int push_me_please=0x7f060007;
        public static final int registration_form=0x7f060003;
        public static final int this_is_a_note=0x7f060004;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.




    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.




        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070001;
        public static final int own_button_black=0x7f070008;
        public static final int own_button_red=0x7f070009;
        public static final int own_edittext_hint_firstname=0x7f070002;
        public static final int own_edittext_hint_lastname=0x7f070003;
        public static final int own_edittext_password=0x7f070004;
        public static final int own_textview_normal=0x7f070007;
        public static final int own_textview_note=0x7f070006;
        public static final int own_textview_title=0x7f070005;
    }
}
