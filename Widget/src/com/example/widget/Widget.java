package com.example.widget;

import hu.bute.daai.amorg.examples.R;

import java.util.Date;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class Widget extends AppWidgetProvider {
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, 
	  int[] appWidgetIds)
	{
		// ID-k lek�rdez�se
		ComponentName thisWidget = new ComponentName(context,
				Widget.class);
		int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
		for (int widgetId : allWidgetIds) {
			RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.activity_main);
			// Sz�veg be�ll�t�sa
			remoteViews.setTextViewText(R.id.tvStatus, new Date(System.currentTimeMillis()).toLocaleString());
	
			// Kattint�s esem�ny kezel�se, hat�s�ra friss�l ism�t a widget
			Intent intent = new Intent(context, Widget.class);
	
			intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
	
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
					0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.tvStatus, pendingIntent);
			appWidgetManager.updateAppWidget(widgetId, remoteViews);
		}
	}
}

