package com.example.owndownloaderintentservice;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;
	
	
public class MainActivity extends ActionBarActivity {

	TextView twResult;
	
   public Handler handler =new Handler(){
	 public void handleMessage(Message message){
		 if (message.arg1==RESULT_OK) {
			twResult.setText("Result Ok");
			 
		}else twResult.setText("Result Faild");
	 };
	   
   };
		
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        twResult=(TextView) findViewById(R.id.tw_result);
        
        Intent intent=new Intent(this,DownloadIntentService.class);
        
        Messenger messenger =new Messenger(handler);
        intent.putExtra(DownloadIntentService.EXTRA_MESSENGER, messenger);
        intent.putExtra(DownloadIntentService.EXTRA_URL, "www.example.com/file.txt");
        startService(intent);
    }

    
}
