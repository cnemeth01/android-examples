package com.example.owndownloaderintentservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class DownloadIntentService extends IntentService {

	public static final String EXTRA_URL = "extra url";
	public static final String EXTRA_MESSENGER = "extra messenger";
	public static final String SERVICE_NAME ="DownloadIntentService"; 
	private int result =Activity.RESULT_CANCELED;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public DownloadIntentService() {
		super("DownloadIntentService");
		// TODO Auto-generated constructor stub
	}
	



	public DownloadIntentService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		result =Activity.RESULT_CANCELED;
		InputStream inputStream=null;
		
		try {
			URL url= new URL(intent.getStringExtra(EXTRA_URL));
			inputStream=url.openConnection().getInputStream();
			InputStreamReader reader = new InputStreamReader(inputStream);
			int readerData=-1;
			while ((readerData= reader.read())!=-1) {
				//do somthing with the downloaded
				
			}
			result = Activity.RESULT_OK;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if (inputStream!=null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		Bundle extras =intent.getExtras();
		if (extras!=null) {
			Messenger messenger = (Messenger) extras.get(EXTRA_MESSENGER);
			Message msg =new Message();
			msg.arg1=result;
			
			try {
				messenger.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}
@Override
public void onDestroy() {
	stopSelf();
	super.onDestroy();
}
}
