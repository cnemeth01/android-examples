package com.epam.alarmclock_ncsaba.test;

import android.test.ActivityInstrumentationTestCase2;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.epam.alarmclock_ncsaba.R;
import com.epam.alarmclock_ncsaba.activities.MainActivity;
import com.robotium.solo.Solo;

public class TestSetAlarm extends ActivityInstrumentationTestCase2<MainActivity> {

    private Solo solo;

    @SuppressWarnings("deprecation")
    public TestSetAlarm() {
        super("com.example.alarmclock_ncsaba", MainActivity.class);
    }

    /**
     * @param activityClass
     */
    public TestSetAlarm(Class<MainActivity> activityClass) {
        super(activityClass);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation(), getActivity());

        setActivityInitialTouchMode(false);
        solo.waitForActivity(MainActivity.class);
    }

    @Override
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

    public void testSettingAlarms() throws Throwable {
        // fail();

        solo.assertCurrentActivity("wrong activity", MainActivity.class);

        Button newAlarmButton = (Button) solo.getView(R.id.btn_new_alarm);
        solo.clickOnView(newAlarmButton);

        solo.clickInList(1);
        ListView mSettingsListView = (ListView) solo.getView(R.id.lv_settings);
        assertNotNull("ListView not allowed to be null", mSettingsListView);

        solo.setTimePicker(0, 07, 15);
        solo.clickOnButton(solo.getString(R.string.done));

        View listRow = mSettingsListView.getChildAt(0);
        TextView settingListTimeData = (TextView) listRow.findViewById(R.id.tw_setting);

        solo.clickInList(2);

        solo.clickOnCheckBox(0);
        solo.clickOnCheckBox(1);
        solo.clickOnCheckBox(2);
        solo.clickOnCheckBox(3);
        solo.clickOnCheckBox(4);

        solo.clickOnButton(solo.getString(R.string.done));

        View lrRepeat = mSettingsListView.getChildAt(1);
        TextView settingRepeatData = (TextView) lrRepeat.findViewById(R.id.tw_setting);

        solo.clickInList(3);

        EditText alarmLabel = (EditText) solo.getView(com.epam.alarmclock_ncsaba.R.id.editTextLabel);
        solo.clickOnView(alarmLabel);
        solo.clearEditText(alarmLabel);
        solo.typeText(alarmLabel, "Reggeli");
        solo.clickOnButton(solo.getString(R.string.done));
        View lrLabel = mSettingsListView.getChildAt(2);
        TextView settingLabelData = (TextView) lrLabel.findViewById(R.id.tw_setting);

        solo.clickInList(5);

        solo.clickOnRadioButton(0);
        solo.clickOnButton(solo.getString(R.string.done));
        View lrWakeUp = mSettingsListView.getChildAt(4);
        TextView settingToneData = (TextView) lrWakeUp.findViewById(R.id.tw_setting);

        solo.clickInList(6);

        final Spinner triesSpinner = (Spinner) solo.getView(com.epam.alarmclock_ncsaba.R.id.spinnerQuizRepeat);
        runTestOnUiThread(new Runnable() {
            public void run() {
                triesSpinner.setSelection(4);
            }
        });

        solo.clickOnButton(solo.getString(R.string.done));
        View lrQuizeTries = mSettingsListView.getChildAt(5);
        TextView settingQuizeTries = (TextView) lrQuizeTries.findViewById(R.id.tw_setting);

        solo.clickInList(7);

        solo.clickOnRadioButton(0);
        solo.clickOnButton(solo.getString(R.string.done));
        View lrSnooze = mSettingsListView.getChildAt(6);
        TextView settingSnoozeData = (TextView) lrSnooze.findViewById(R.id.tw_setting);

        solo.clickInList(8);

        solo.clickOnRadioButton(0);
        solo.clickOnButton(solo.getString(R.string.done));
        View lrWakeUpType = mSettingsListView.getChildAt(7);
        TextView settingWakeUpType = (TextView) lrWakeUpType.findViewById(R.id.tw_setting);

        Thread.sleep(2000);

        assertEquals("Reggeli", settingLabelData.getText().toString());
        assertEquals("H, K, Sze, Cs, P", settingRepeatData.getText().toString());
        assertEquals("07:15", settingListTimeData.getText().toString());
        assertEquals("MATH", settingToneData.getText().toString());
        assertEquals("5 alkalommal", settingQuizeTries.getText().toString());
        assertEquals("5 perc", settingSnoozeData.getText().toString());
        assertEquals("RING", settingWakeUpType.getText().toString());

        solo.sendKey(KeyEvent.KEYCODE_BACK);

        Thread.sleep(2000);

        ListView mMainListView = (ListView) solo.getView(R.id.lv_main);
        assertNotNull("ListView not allowed to be null", mMainListView);
        solo.clickLongInList(1);
        solo.clickOnButton(solo.getString(R.string.delete));
        
        Thread.sleep(3000);
        
        
        

    }

}
