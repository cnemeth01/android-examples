package com.example.wishlistappora;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class WishItemDAO {
	
	private WishItemDBHelper helper;
	
	public WishItemDAO(Context context){
		helper = new WishItemDBHelper(context);
	}
	
	public List<WishItem> getAllWishItems() {
		SQLiteDatabase db = helper.getReadableDatabase();
		List<WishItem> resultList = new ArrayList<WishItem>();
		Cursor cursor = db.rawQuery("SELECT * FROM wishitem", null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			WishItem item = new WishItem(
					cursor.getInt(0),
					cursor.getString(1),
					cursor.getString(2),
					cursor.getFloat(3),
					cursor.getFloat(4),
					cursor.getString(5));
			resultList.add(item);
			cursor.moveToNext();
		}
		cursor.close();
		db.close();
		return resultList;
	}
	public void saveWishItem(WishItem wi) {
		SQLiteDatabase db = helper.getWritableDatabase();
		if (wi.getId() == -1) {
			// insert
			db.execSQL("INSERT INTO wishitem (name, place, lat, lng, img) VALUES (?,?,?,?,?)", 
					new Object[]{wi.getName(), wi.getPlace(), wi.getLat(), wi.getLng(), wi.getImg()});
		} else {
			// update
			db.execSQL("UPDATE wishitem SET name=?, place=?, lat=?, lng=?, img=? WHERE _id=?",
					new Object[]{wi.getName(), wi.getPlace(), wi.getLat(), wi.getLng(),wi.getImg(), wi.getId()});
		}
		db.close();
	}
	public void deleteWishItem(WishItem wi) {
		SQLiteDatabase db = helper.getWritableDatabase();
		db.execSQL("DELETE FROM wishitem WHERE _id = ?", 
				new Integer[]{wi.getId()});
	}
		
}
	

