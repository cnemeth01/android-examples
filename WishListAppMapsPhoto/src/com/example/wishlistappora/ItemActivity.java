package com.example.wishlistappora;

import java.io.File;
import java.io.IOException;
import java.util.List;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class ItemActivity extends Activity implements LocationListener {

	private EditText nevEdit;
	private EditText helyEdit;
	private Intent result;
	private LocationManager lm;
	private double lat;
	private double lng;
	private File imageFile;
	
	private static final int CAMERA_IMAGE_REQUEST = 1;
	String FILE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/wishimgs/";
	private ImageView imageView;
	
	private int originalId = -1;
	private WishItem originalWishItem;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item);
		
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);

		nevEdit = (EditText)findViewById(R.id.nevText);
		helyEdit = (EditText)findViewById(R.id.helyText);
		
		imageView = (ImageView) findViewById(R.id.kep);
		
		result = getIntent();
		originalWishItem = (WishItem)result.getSerializableExtra("item");
		if (originalWishItem != null) {
			nevEdit.setText(originalWishItem.getName());
			helyEdit.setText(originalWishItem.getPlace());
			originalId = originalWishItem.getId();
			imageFile = new File(originalWishItem.getImg());
			if (originalWishItem.getImg() != null && !originalWishItem.getImg().equals("")) {
				imageView.post(new Runnable()
			    {
			        public void run()
			        {
						Bitmap bmp = BitmapFactory.decodeFile(originalWishItem.getImg());
						Bitmap smallBmp = Bitmap.createScaledBitmap(bmp, imageView.getWidth(), imageView.getHeight(), true);
						imageView.setImageBitmap(smallBmp);
			        }
			    });
			}
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.item, menu);
		return true;
	}


	
	public void fenykepez(View view) {
		String kepnev="def.jpg";
		if (!nevEdit.getText().equals("")) {
			kepnev=nevEdit.getText().toString();
			kepnev = kepnev.replaceAll(" ", "_");
			kepnev = kepnev+".jpg";
		}
	    imageFile = new File(FILE_PATH+kepnev);
	    imageFile.getParentFile().mkdirs();
		Uri imageUri = Uri.fromFile(imageFile);
		
		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		startActivityForResult(cameraIntent, CAMERA_IMAGE_REQUEST);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_IMAGE_REQUEST) {
			if (resultCode == RESULT_OK) {
				
				Bitmap bmp = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
				Bitmap smallBmp = Bitmap.createScaledBitmap(bmp, imageView.getWidth(), imageView.getHeight(), true);
				imageView.setImageBitmap(smallBmp);
			} else if (resultCode == RESULT_CANCELED) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Nincs k�p!")
				       .setMessage("A f�nyk�pk�sz�t�s le�ll�tva")
				       .setCancelable(true);
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		}
	}
	
	public void mentes(View view) {
		WishItem wi = new WishItem(
				originalId,
				nevEdit.getText().toString(),
				helyEdit.getText().toString(),
				lat, lng, imageFile == null ? "" : imageFile.getAbsolutePath());
		result.putExtra("item", wi);
		setResult(RESULT_OK, result);
		finish();
	}
	
	public void megsem(View view) {
		setResult(RESULT_CANCELED);
		finish();
	}
	
	public void getLocation(View view) {
		lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		
	}

	@Override
	public void onLocationChanged(Location loc) {
		
		lm.removeUpdates(this);
		
		new AsyncTask<Double, Void, String>() {
			@Override
			protected String doInBackground(Double... pos) {
				double latitude = pos[0];
				double longitude = pos[1];
				Geocoder gc = new Geocoder(ItemActivity.this);
				List<Address> addresses;
				try {
					addresses = gc.getFromLocation(latitude, longitude, 1);
					if (addresses != null && addresses.size()>0) {
						Address a = addresses.get(0);
						String line1 = a.getAddressLine(0);
						String line2 = a.getAddressLine(1);
						String result = line1 + (line2 != null ? ", "+line2 : "");
						return result;
					} else {
						return latitude+", "+longitude;
					}
				} catch (IOException e) {
					Log.i("GEOCODER", e.getMessage());
					return latitude+", "+longitude;
				}
			}
			
			protected void onPostExecute(String result) {
				String hely = result;
				helyEdit.setText(hely);
			}
			
		}.execute(new Double[]{loc.getLatitude(), loc.getLongitude()});
		lat = loc.getLatitude();
		lng = loc.getLongitude();
	}

	
	
	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

}
