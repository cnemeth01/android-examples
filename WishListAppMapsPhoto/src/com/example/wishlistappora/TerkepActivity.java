package com.example.wishlistappora;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class TerkepActivity extends Activity {

	private MapView mapView;
	private GoogleMap map;
	private WishItemDAO dao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_terkep);
		
		// Gets the MapView from the XML layout and creates it
 		mapView = (MapView) findViewById(R.id.mapview);
 		mapView.onCreate(savedInstanceState);
  
 		// Gets to GoogleMap from the MapView and does initialization stuff
 		map = mapView.getMap();
 		map.getUiSettings().setMyLocationButtonEnabled(true);
 		map.setMyLocationEnabled(true);
 		map.getUiSettings().setZoomControlsEnabled(true); 
 		map.getUiSettings().setZoomGesturesEnabled(true);

 		dao = new WishItemDAO(this);
 		/*map.addMarker(new MarkerOptions().position(new LatLng(47.5, 19.1)));
 		List<LatLng> points = new ArrayList<LatLng>();
 		points.add(new LatLng(47.5, 19.15));
        points.add(new LatLng(47.5, 19.16));
 		Polyline elso = map.addPolyline(new PolylineOptions().color(R.color.vonal).addAll(points));
 		points.add(new LatLng(47.51, 19.15));
        elso.setPoints(points);*/
	        
	}
	
	@Override
	public void onResume() {
		mapView.onResume();
		super.onResume();
 		map.clear();
		List<WishItem> items = dao.getAllWishItems();
		for (WishItem wi : items) {
			map.addMarker(new MarkerOptions()
				.position(new LatLng(wi.getLat(), wi.getLng()))
				.title(wi.getName())
				.snippet(wi.getPlace()));
		}
	}
 
	@Override
	public void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
	}
 
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapView.onLowMemory();
	}

}
