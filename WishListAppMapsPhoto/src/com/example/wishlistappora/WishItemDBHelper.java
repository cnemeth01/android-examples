package com.example.wishlistappora;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class WishItemDBHelper extends SQLiteOpenHelper {

	public WishItemDBHelper(Context context) {
		super(context, "wishlist.db", null, 3);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE wishitem (" +
				"_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"name TEXT," +
				"place TEXT," +
				"lat REAL," +
				"lng REAL,img TEXT)");
		db.execSQL("INSERT INTO wishitem (name, place, lat, lng, img) VALUES ('Cs�p�s kobranyelv', '1013 Budapest, L�nch�d u. 5.', 47.498097, 19.040592,'')");
		db.execSQL("INSERT INTO wishitem (name, place, lat, lng, img) VALUES ('F�kaorr', '1051 Budapest, Bajcsy-Zsilinszky �t 16.', 47.500706, 19.054817,'')");
		db.execSQL("INSERT INTO wishitem (name, place, lat, lng, img) VALUES ('Borzt�d�', '1106 Budapest, �rk�d', 47.502475, 19.138738,'')");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion == 1 && newVersion == 2) {
//			db.execSQL("ALTER TABLE wishitem ")
			db.execSQL("DROP TABLE wishitem");
			onCreate(db);
		} else {
			db.execSQL("DROP TABLE wishitem");
			onCreate(db);
		}
	}

}
