package com.example.wishlistappora;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;



public class WishItemAdapter extends ArrayAdapter<WishItem> {

	public WishItemAdapter(Context context, List<WishItem> items) {
		super(context, R.layout.wishitem, items);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		WishItem wi = getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).
					inflate(R.layout.wishitem, null);
		}
		TextView wishItemName = 
				(TextView)convertView.findViewById(R.id.item_name);
		TextView wishItemPlace =
				(TextView)convertView.findViewById(R.id.item_place);
		wishItemName.setText(wi.getName());
		wishItemPlace.setText(wi.getPlace());
		return convertView;
	}
	
	
}
