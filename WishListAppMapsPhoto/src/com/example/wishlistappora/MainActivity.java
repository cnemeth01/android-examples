package com.example.wishlistappora;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private List<WishItem> wishList = new ArrayList<WishItem>();
	private WishItemAdapter adapter;
	private ListView listView;
	private static final int REQUEST_EDIT = 1;
	private static final int REQUEST_NEW = 2;
	private static final String PROX_ALERT_INTENT = "hu.example.PROXIMITY";
	private static final int REQUEST_PROX = 2;
	private ProximityIntentReceiver pxr;
	
	private WishItemDAO dao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		dao = new WishItemDAO(this);
		wishList = dao.getAllWishItems();
		
		for (WishItem wi : wishList) {	
			activateProximityAlert(wi);
		}

		
//		wishList.add(new WishItem("Rig�f�tty", "Colosseum"));
//		wishList.add(new WishItem("F�lem�lem�j", "Colosseum"));
//		wishList.add(new WishItem("F�kaorr", "Colosseum"));
		
		adapter = new WishItemAdapter(this, wishList);
		listView = (ListView)findViewById(R.id.listView1);
		listView.setAdapter(adapter);
		
		registerForContextMenu(listView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_new_item:
			//Toast.makeText(this, "�j elem felv�tele", Toast.LENGTH_LONG).show();
			Intent intent = new Intent(this, ItemActivity.class);
			startActivityForResult(intent, REQUEST_NEW);
			return true;
		case R.id.action_removeall_items:
			for (WishItem wi : wishList) {
				dao.deleteWishItem(wi);
			}
			adapter.clear();
			Toast.makeText(this, "Lista t�rl�se megt�rt�nt", Toast.LENGTH_LONG).show();
			return true;
		
		case R.id.action_options:
			Intent intent2 = new Intent(this, OptionsActivity.class);
			startActivity(intent2);
			return true;
			
		case R.id.action_map:
			Intent mapIntent = new Intent(this, TerkepActivity.class);
			startActivity(mapIntent);
			return true;
			
		case R.id.action_export:
			export();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void export() {
		SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(this);
		String exportDir = sh.getString("prefDirectory", "");
		FileOutputStream out;
		try {
			if (exportDir.equals("")) {
				// bels� t�rol�
				out = openFileOutput("output.csv", Context.MODE_PRIVATE);
				PrintWriter pw = new PrintWriter(out);
				for (WishItem wi : wishList) {
					pw.println(wi.getName()+";"+wi.getPlace());
				}
				pw.close();
			} else {
				// k�ls� t�rol�
				if (Environment.getExternalStorageState()
						.equals(Environment.MEDIA_MOUNTED)) {
					File extDir = Environment.getExternalStorageDirectory();
					// extDir / exportDir / output.csv
					File outFile = new File(
							extDir.getAbsolutePath() + "/" +
							exportDir + 
							"/output.csv"
					);
					outFile.getParentFile().mkdirs();
					PrintWriter pw = new PrintWriter(outFile);
					for (WishItem wi : wishList) {
						pw.println(wi.getName()+";"+wi.getPlace());
					}
					pw.close();
				}
			}
		} catch (IOException ex) {
			Log.w("EXPORT", ex.getMessage());
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)
				item.getMenuInfo();
		int index = info.position;
		
		switch (item.getItemId()) {
		case R.id.action_remove_item:
			//Toast.makeText(this, "Elem["+index+"] t�rl�se", Toast.LENGTH_LONG).show();
			dao.deleteWishItem(wishList.get(index));
			adapter.remove(wishList.get(index));
			return true;
		case R.id.action_edit_item:
			//Toast.makeText(this, "Elem["+index+"] szerkeszt�se", Toast.LENGTH_LONG).show();
			Intent intent = new Intent(this, ItemActivity.class);
			WishItem wi = wishList.get(index);
			intent.putExtra("item", wi);
			intent.putExtra("index", index);
			startActivityForResult(intent, REQUEST_EDIT);
			return true;
		}
		return super.onContextItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);
		menu.setHeaderTitle("Elem m�veletei");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_NEW && resultCode == RESULT_OK) {
			WishItem wi = (WishItem)data.getSerializableExtra("item");
			//wishList.add(wi);
			adapter.add(wi);
			dao.saveWishItem(wi); // !!!
			activateProximityAlert(wi);
		}
		if (requestCode == REQUEST_EDIT && resultCode == RESULT_OK) {
			WishItem wi = (WishItem)data.getSerializableExtra("item");
			int index = (Integer)data.getIntExtra("index", 0);
			wishList.set(index, wi);
			adapter.notifyDataSetChanged();
			dao.saveWishItem(wi); // !!!
			activateProximityAlert(wi);
		}
		
		//notifyDataSetChanged();
	}
	
	class ProximityIntentReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String key = LocationManager.KEY_PROXIMITY_ENTERING;
			boolean entering = intent.getBooleanExtra(key, false);
			String name = intent.getStringExtra("name");
			NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this);
			
			Intent appIntent = new Intent(MainActivity.this, MainActivity.class);
			PendingIntent appPendingIntent = PendingIntent.getActivity(
					MainActivity.this, 0, appIntent, 0);
			
			if (entering)  {
				builder.setContentTitle("R�gz�tett hely k�zel�be �rt�l")
		        .setContentText("Most v�gre megveheted a "+name+"-t!")
		        .setSmallIcon(R.drawable.ic_launcher)
		        .setContentIntent(appPendingIntent)
		        .setVibrate(new long[]{2000});
			} 
			
		    Notification n = builder.build();
			
			NotificationManager notificationManager = 
					  (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

			notificationManager.notify(0, n);
			
		}
		
	}
	
	
	private void activateProximityAlert(WishItem wi) {
		
		IntentFilter intentFilter = new IntentFilter(
				PROX_ALERT_INTENT);
		pxr = new ProximityIntentReceiver();
		registerReceiver(pxr, intentFilter);
		
		// 1000 meteren belul
		float radius = 100f;
		// soha nem j�r le
		long expiration = -1;
		
		LocationManager locationManager = (LocationManager) MainActivity.this
				.getSystemService(Context.LOCATION_SERVICE);
		Intent intent = new Intent(PROX_ALERT_INTENT);
		intent.putExtra("name", wi.getName());
		PendingIntent proxyIntent = PendingIntent.getBroadcast(getApplicationContext(),
				wi.getId(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
		locationManager.removeProximityAlert(proxyIntent);
		locationManager.addProximityAlert(wi.getLat(), wi.getLng(), radius, expiration,
				proxyIntent);
		
	}

}
