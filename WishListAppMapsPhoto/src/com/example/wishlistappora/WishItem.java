package com.example.wishlistappora;

import java.io.Serializable;

public class WishItem implements Serializable {

	private int id;
	private String name;
	private String place;
	private double lat;
	private double lng;
	private String img;
	
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public WishItem() {
		this.id = -1;
	}
	

	public WishItem(int id, String name, String place, double lat, double lng,
			String img) {
		super();
		this.id = id;
		this.name = name;
		this.place = place;
		this.lat = lat;
		this.lng = lng;
		this.img = img;
	}

	public WishItem(String name, String place, double lat, double lng) {
		super();
		this.id = -1;
		this.name = name;
		this.place = place;
		this.lat = lat;
		this.lng = lng;
	}

	public WishItem(int id, String name, String place, double lat, double lng) {
		super();
		this.id = id;
		this.name = name;
		this.place = place;
		this.lat = lat;
		this.lng = lng;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLat() {
		return lat;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public void setLng(double lng) {
		this.lng = lng;
	}

	public boolean equals(Object obj) {
		WishItem other = (WishItem)obj;
		// FIXME nullpointerexception kezel�se
		return other.name.equals(name) && 
				other.place.equals(place);
	}
	
}
