package com.example.androidimagescroll;

import android.content.Context;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private int[] images = {R.drawable.audi1,R.drawable.audi2,R.drawable.audi3};
    private final String[] descriptions = {"Audi 1","Audi 2","Audi 3"};
    private Context context;

    public ViewPagerAdapter(android.support.v4.app.FragmentManager supportFragmentManager, Context applicationContext) {
        super(supportFragmentManager);
        this.context =applicationContext;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {     
        return  MyViewPagerFragment.newInsatnce(position, images[position],descriptions[position]);       
    }

}
