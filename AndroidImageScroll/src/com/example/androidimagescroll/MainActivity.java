package com.example.androidimagescroll;

import com.example.androidimagescroll.DescriptionFragment.OnFragmentInteractionListener;
import com.example.androidimagescroll.MyViewPagerFragment.callBackToMainActivity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements callBackToMainActivity,OnFragmentInteractionListener {

    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private static TextView textView;
    private FrameLayout descriptionContainer;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.images_fragment);

        descriptionContainer = (FrameLayout) findViewById(R.id.descriptionContainer);
        
        getSupportFragmentManager().beginTransaction().replace(R.id.descriptionContainer, DescriptionFragment.newInstance("", ""),"tag").commit();
        
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager(),getApplicationContext());
        
        viewPager.setAdapter(adapter);
        
                    
    }
    
    public static void updateText(String string){
        textView.setText(string);
    }

    @Override
    public void onImageClick(int position) {
        DescriptionFragment descriptionFragment= (DescriptionFragment) getSupportFragmentManager().findFragmentByTag("tag");
        descriptionFragment.setDescreptoonText(position);
        
        
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        // TODO Auto-generated method stub
        
    }  
}
