package com.example.androidimagescroll;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

public class MyViewPagerFragment extends Fragment {

    private ImageView imageView;
    private int imageId;
    private int position;
    private callBackToMainActivity mListener;

    public MyViewPagerFragment() {

    }

    public static MyViewPagerFragment newInsatnce(int position, int imageId, String description) {
        MyViewPagerFragment myFragment = new MyViewPagerFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bundle.putInt("imageId", imageId);
        bundle.putString("description", description);
        myFragment.setArguments(bundle);

        return myFragment;
    }
    
    @Override
    public void onAttach(Activity activity) {
      mListener=(callBackToMainActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.viewpager_item, container, false);

        String description = getArguments().getString("description");

        imageView = (ImageView) view.findViewById(R.id.image_item);

        return view;

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        imageId = getArguments().getInt("imageId");
        position = getArguments().getInt("position");

        imageView.setImageResource(imageId);

        imageView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mListener.onImageClick(position);
//                Toast.makeText(getActivity(), "ID: " + position, Toast.LENGTH_SHORT).show();

            }

        });

    }
    
    public interface callBackToMainActivity{
        public void onImageClick(int position);
    }
}
