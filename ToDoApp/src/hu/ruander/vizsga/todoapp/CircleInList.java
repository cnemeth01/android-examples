package hu.ruander.vizsga.todoapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class CircleInList extends View {

	public CircleInList(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}
	
	
	public CircleInList(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public CircleInList(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}




	private int priority;
	private Paint paint=new Paint();
	private RectF r=new RectF(10,10,50,50);
	
	
	public void setPriority(int p) {
		this.priority=p;
	}

	

	
	@Override
	protected void onDraw(Canvas canvas) {
		
		paint.setColor(Color.GRAY);
		switch (priority) {
		case 0: 
			paint.setColor(Color.GREEN);
			break;
		case 1:
			paint.setColor(Color.YELLOW);
			break;
		case 2:
			paint.setColor(Color.RED);
			break;
		}		
		canvas.drawOval(r, paint);
	}

	
	
	
}
