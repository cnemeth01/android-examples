package hu.ruander.vizsga.todoapp;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ToDoDbDAO {

		private ToDoAppDBHelper helper;
		private SQLiteDatabase db;
		
		
		public ToDoDbDAO(Context context) {
			helper = new ToDoAppDBHelper(context);
			db = helper.getWritableDatabase();
		}
		
		public void close() {
			db.close();
		}
		
		public List<ToDoItem> getAllItem() {
			List<ToDoItem> list = new ArrayList<ToDoItem>();
			Cursor cursor = db.rawQuery("SELECT * FROM todoitem", null);
			while ( cursor.moveToNext()) {
				ToDoItem ti = new ToDoItem(cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getInt(3),cursor.getInt(4),cursor.getInt(5));
				list.add(ti);
			}
			
			return list;
		}

		public void deleteItem(ToDoItem ti) {
			db.execSQL("DELETE FROM todoitem WHERE _id=?", new Integer[]{ti.getId()});
		}
		
		public void saveItem(ToDoItem ti) {
				db.execSQL("INSERT INTO todoitem (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES (?,?,?,?,?)", new Object[]{ti.getTitle(),ti.getEv(),ti.getHo(),ti.getNap(),ti.getPriority()});
		}
}
