package hu.ruander.vizsga.todoapp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class EditItem_Activity extends Activity {

	EditText title;
	EditText dateText;
	TextView errorText;
	RadioGroup prioGrp;
	Intent result;
	Date date=new Date();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_item);
		title = (EditText) findViewById(R.id.txtMegnevezes);
		dateText = (EditText) findViewById(R.id.txtHatarido);
		prioGrp = (RadioGroup) findViewById(R.id.radioGroup1);
		errorText = (TextView) findViewById(R.id.txtError);
		result = getIntent();
	}

	public void mentes(View v) {
		
		int priority=0;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy.MM.dd");
		Calendar cal=Calendar.getInstance();
		try {
			date=sdf.parse(dateText.getText().toString());
			cal.setTime(date);
		switch (prioGrp.getCheckedRadioButtonId()) {
			case R.id.radio0:
				priority=0;
				break;
			case R.id.radio1:
				priority=1;
				break;
			case R.id.radio2:
				priority=2;
				break;	
		}
		
		ToDoItem ti = new ToDoItem(title.getText().toString(), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(cal.DAY_OF_MONTH),priority);
		Bundle extras=new Bundle();
		extras.putSerializable("item", ti);
		result.putExtras(extras);
		
		setResult(RESULT_OK, result);
		finish();
		} catch (ParseException e) {			
			errorText.setText("Hib�s d�tum!");
		}
	}
}
