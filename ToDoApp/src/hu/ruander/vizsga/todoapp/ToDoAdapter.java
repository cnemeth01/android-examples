package hu.ruander.vizsga.todoapp;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ToDoAdapter extends ArrayAdapter<ToDoItem> {

	public ToDoAdapter(Context context, List<ToDoItem> objects) {
		super(context,R.layout.todoitem_layout, objects);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ToDoItem ti = getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.todoitem_layout, null);
		}
		TextView title = (TextView) convertView.findViewById(R.id.tvMegnevezes);
		TextView due = (TextView) convertView.findViewById(R.id.tvHatarido);
		CircleInList cv=(CircleInList) convertView.findViewById(R.id.circleInList1);
		
		cv.setPriority(ti.getPriority());
		title.setText(ti.getTitle());
		due.setText(""+ti.getEv()+"."+ti.getHo()+"."+ti.getNap());
		
		return convertView;
	}


}
