package hu.ruander.vizsga.todoapp;




import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MainActivity extends Activity {

	List<ToDoItem> items;
	ToDoAdapter adapter;
	private ToDoDbDAO dao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		items=new ArrayList<ToDoItem>();
		
		dao = new ToDoDbDAO(this);
		items = dao.getAllItem();
		
		adapter = new ToDoAdapter(this, items);
		ListView listView = (ListView) findViewById(R.id.lvTodos);
		listView.setAdapter(adapter);
		registerForContextMenu(listView);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.contextmenu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		final int selIndex = ((AdapterContextMenuInfo) item.getMenuInfo()).position;
		
		boolean choosen=false;		
		if (item.getItemId()==R.id.action_delete) {			
			dao.deleteItem(items.get(selIndex));
			items.remove(selIndex);
			adapter.notifyDataSetChanged();
			Toast toast=Toast.makeText(getApplicationContext(), "T�r�lve", Toast.LENGTH_LONG);
			toast.show();
			choosen=true;
		}
		return choosen;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean choosen=false;
		if (item.getItemId()==R.id.action_new){
			Intent intent=new Intent(getApplicationContext(),EditItem_Activity.class);
			startActivityForResult(intent, 0);
			choosen=true;
		}
		return choosen;
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode==0 && resultCode==RESULT_OK) {
			Bundle bundle=new Bundle();
			bundle = data.getExtras();
			ToDoItem ti = (ToDoItem) bundle.get("item");
			dao.saveItem(ti);
			items.add(ti);
			adapter.notifyDataSetChanged();			
		}
		super.onActivityResult(requestCode, resultCode, data);
		
	}
	
}
