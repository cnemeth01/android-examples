package hu.ruander.vizsga.todoapp;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ToDoAppDBHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "todolist.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "CREATE TABLE todoitem (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
			"megnevezes TEXT, hatarido_ev INTEGER, hatarido_honap INTEGER, hatarido_nap INTEGER, fontossag INTEGER)";
	
	public ToDoAppDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);		
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
		db.execSQL("INSERT INTO todoitem (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES ('Android vizsg�ra felk�sz�lni', 2014,04,03,1)");
		db.execSQL("INSERT INTO todoitem (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES ('Robinak v�laszolni', 2014,04,05,0)");
		db.execSQL("INSERT INTO todoitem (megnevezes,hatarido_ev,hatarido_honap,hatarido_nap,fontossag) VALUES ('Inni m�g s�rt', 2014,04,06,2)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE todoitem");
		onCreate(db);

	}

}
