package hu.ruander.vizsga.todoapp;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class ToDoItem implements Serializable {
	private int id;
	private String title;
	private int ev;
	private int ho;
	private int nap;
	private int priority;

	ToDoItem(int id, String megn, int ev, int ho, int nap, int prio) {
		this.id = id;
		this.title = megn;
		this.priority = prio;
		this.ev = ev;
		this.ho = ho;
		this.nap = nap;
	}

	ToDoItem(String megn, int ev, int ho, int nap, int prio) {
		this.id = -1;
		this.title = megn;
		this.priority = prio;
		this.ev = ev;
		this.ho = ho;
		this.nap = nap;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getEv() {
		return ev;
	}

	public void setEv(int ev) {
		this.ev = ev;
	}

	public int getHo() {
		return ho;
	}

	public void setHo(int ho) {
		this.ho = ho;
	}

	public int getNap() {
		return nap;
	}

	public void setNap(int nap) {
		this.nap = nap;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
