package com.supersoftwares.webview.services;

import com.sportingbet.sportingbetcommon.services.LocationService;
import com.sportingbet.sportingbetcommon.services.UserServices;

/**
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public interface ServiceLocator {

    UserServices getUserService();

    LocationService getLocationSrvice();

    GameHistory getGameHistory();
}
