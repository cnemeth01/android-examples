package com.supersoftwares.webview.services;

import java.util.List;

/**
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public class GameHistory {

    private List<Game> gameList;

    public List<Game> getGameList() {
        return gameList;
    }

    public void setGameList(List<Game> gameList) {
        this.gameList = gameList;
    }
}
