package com.supersoftwares.webview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.supersoftwares.webview.core.User;
import com.supersoftwares.webview.databinding.ListItemBinding;

import java.util.List;

/**
 * Created by Csaba_Bela_Nemeth on 8/27/2015.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<Drawable> mDataset;
    private Context context;
    private ListItemBinding binding;
    private User user;

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<Drawable> myDataset) {
        mDataset = myDataset;
        user = new User();
    }


    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        context = parent.getContext();
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item, null);

        binding = ListItemBinding.bind(v);
        binding.setUser(user);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        String url = "http://pngimg.com/upload/lion_PNG573.png";
        user.setFullName("JohnDoe" + position);
        //      holder.textView.setText("John Doe" + position);
        Picasso.with(context).load(url).placeholder(R.drawable.loader).error(R.drawable.network_wired_disconnected).into(holder.imageView);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView textView;

        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.image_view_item);
            textView = (TextView) v.findViewById(R.id.binded_text_view);
        }
    }
}

