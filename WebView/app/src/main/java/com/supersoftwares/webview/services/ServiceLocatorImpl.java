package com.supersoftwares.webview.services;

import com.sportingbet.sportingbetcommon.authentication.AuthenticationInterceptor;
import com.sportingbet.sportingbetcommon.services.CommonServiceFactory;
import com.sportingbet.sportingbetcommon.services.LocationService;
import com.sportingbet.sportingbetcommon.services.UserServices;
import com.supersoftwares.webview.BuildConfig;

import retrofit.client.OkClient;
import retrofit.converter.Converter;

/**
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public class ServiceLocatorImpl implements ServiceLocator {

    OkClient okClient;
    String serviceEndPoint;
    Converter converter;
    AuthenticationInterceptor authenticationInterceptor;

    private UserServices userService;
    private LocationService locationSrvice;
    private GameHistory gameHistory;

    public ServiceLocatorImpl(OkClient okClient, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        this.okClient = new OkClient();
        this.converter = converter;
        this.authenticationInterceptor = authenticationInterceptor;
    }

    @Override
    public UserServices getUserService() {
        String serviceEndPoint = BuildConfig.ACCOUNT_SERVICE_ENDPOINT;
        return CommonServiceFactory.createUserService(okClient, serviceEndPoint, converter, authenticationInterceptor);
    }

    @Override
    public LocationService getLocationSrvice() {
        return CommonServiceFactory.createLocationService(okClient, serviceEndPoint, converter, authenticationInterceptor);
    }

    @Override
    public GameHistory getGameHistory() {
        return null;
    }


}
