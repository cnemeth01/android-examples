package com.supersoftwares.webview;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.supersoftwares.webview.core.User;

;

/**
 * Created by CSABA_BELA_NEMETH on 9/1/2015.
 */
public class ContentFragment extends Fragment {

    WebView webView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_fragment, container, false);
        webView = (WebView) v.findViewById(R.id.web_view);
        final Activity activity = getActivity();
        initWebView(activity);

        User user = new User();
        user.setFullName("John Dow");
        return v;
    }

    private void initWebView(final Activity activity) {

        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                activity.setProgress(progress * 1000);
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });
        webView.getSettings().setBuiltInZoomControls(true);

        webView.loadUrl("http://cnemeth01.kmdns.net/html/snowing.html");

    }
}

