package com.supersoftwares.webview;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;


public class MainActivity extends AppCompatActivity implements DrawerLayout.DrawerListener {


    DrawerLayout drawerLayout;
    HorizontalScrollView horizontalScrollView;
    NavigationView mnavigationViewnu;
    private Toolbar toolbar;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        // Let's display the progress in the activity title bar, like the
        // browser app does.
        setContentView(R.layout.activity_main);
        mnavigationViewnu = (NavigationView) findViewById(R.id.navigation_view);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        initFragment();
        initToolBar();
        initMenu();
        initHorizontalScrollView();
        initDrawerLayout();


    }

    private void initFragment() {
        ContentFragment fragment = new ContentFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame,fragment);
        fragmentTransaction.commit();
    }

    private void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

    }

    private void initMenu() {
        mnavigationViewnu.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                switch (menuItem.getItemId()) {
                    case R.id.navigation_item_1:
                        Intent intent = new Intent(MainActivity.this, WheelViewActivity.class);
                        startActivity(intent);
                        return true;
                    case R.id.navigation_item_2:
                        BouncyCastleEncryptToolTest helper = new BouncyCastleEncryptToolTest();
                        helper.encryptBC();
                        return true;
                    default:
                        return true;
                }
            }
        });
    }



    private void initDrawerLayout() {
        drawerLayout.setDrawerListener(this);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        drawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();

    }

    private void initHorizontalScrollView() {
        horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horizontal_scroll_view);

        DataBase dataBase = new DataBase(this);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.scrollview_container);

        for (Drawable drawable : dataBase.getDrawables()) {
            LinearLayout linearLayout1 = new LinearLayout(this);
            linearLayout1.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            linearLayout1.setPadding(16, 16, 16, 16);

            ImageView imageView = new ImageView(this);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            imageView.setLayoutParams(layoutParams);
            imageView.setPadding(8, 8, 8, 8);
            imageView.setBackground(drawable);
            linearLayout.addView(imageView);
            linearLayout.addView(linearLayout1);
        }

        horizontalScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
                } else {
                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                }
                System.out.println("onTouch(View v, MotionEvent event): " + event.toString());
                return false;
            }
        });
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        System.out.println("onDrawerOpened(View drawerView): " + drawerView.toString());
        horizontalScrollView.requestDisallowInterceptTouchEvent(false);

    }

    @Override
    public void onDrawerClosed(View drawerView) {
        System.out.println("onDrawerClosed(View drawerView): " + drawerView.toString());
        horizontalScrollView.requestDisallowInterceptTouchEvent(true);
    }

    @Override
    public void onDrawerStateChanged(int newState) {
        System.out.println("onDrawerStateChanged(int newState): " + newState);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        System.out.println("on touch event: " + event.toString());
        return super.onTouchEvent(event);
    }


}
