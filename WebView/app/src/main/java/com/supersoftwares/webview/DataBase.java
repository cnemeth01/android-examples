package com.supersoftwares.webview;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Csaba_Bela_Nemeth on 8/27/2015.
 */
public class DataBase {

    private static Context context;
    private static List<Drawable> drawables;

    public DataBase(Context context) {
        this.context = context;
        createDrawabels();
    }

    public synchronized List<Drawable> getDrawables() {
        if (drawables != null) {
            List<Drawable> copy = new ArrayList<>();
            copy.addAll(drawables);
            return copy;
        } else {
            createDrawabels();
            return drawables;
        }
    }

    private void createDrawabels() {
        drawables = new ArrayList<>();

        drawables.add(context.getResources().getDrawable(R.drawable.w, null));
        drawables.add(context.getResources().getDrawable(R.drawable.k, null));
        drawables.add(context.getResources().getDrawable(R.drawable.a, null));
        drawables.add(context.getResources().getDrawable(R.drawable.b, null));
        drawables.add(context.getResources().getDrawable(R.drawable.c, null));
        drawables.add(context.getResources().getDrawable(R.drawable.h, null));
        drawables.add(context.getResources().getDrawable(R.drawable.l, null));
        drawables.add(context.getResources().getDrawable(R.drawable.u, null));
        drawables.add(context.getResources().getDrawable(R.drawable.a, null));
        drawables.add(context.getResources().getDrawable(R.drawable.b, null));
        drawables.add(context.getResources().getDrawable(R.drawable.c, null));
        drawables.add(context.getResources().getDrawable(R.drawable.h, null));
        drawables.add(context.getResources().getDrawable(R.drawable.l, null));
        drawables.add(context.getResources().getDrawable(R.drawable.k, null));


    }

}
