package com.supersoftwares.webview.core;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.supersoftwares.webview.BR;

/**
 * Created by CSABA_BELA_NEMETH on 9/7/2015.
 */
public class User extends BaseObservable {

    private String fullName;

    @Bindable
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
        notifyPropertyChanged(BR.fullName);
    }
}
