package com.supersoftwares.webview;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;

/**
 * Created by CSABA_BELA_NEMETH on 9/2/2015.
 */
public class BouncyCastleEncryptToolTest {
    private final String salt = "A long, but constant phrase that will be used each time as the salt.";
    private final int iterations = 2000;
    private final int keyLength = 256;
    private final SecureRandom random = new SecureRandom();

    public void encryptBC() {
        Security.insertProviderAt(new BouncyCastleProvider(), 1);

        String passphrase = "The quick brown fox jumped over the lazy brown dog";
        String plaintext = "hello world";
        byte[] ciphertext = new byte[0];
        String recoveredPlaintext = null;
        try {
            ciphertext = encrypt(passphrase, plaintext);
            recoveredPlaintext = decrypt(passphrase, ciphertext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(recoveredPlaintext);
    }


    private byte[] encrypt(String passphrase, String plaintext) throws Exception {
        SecretKey key = generateKey(passphrase);

        Cipher cipher = Cipher.getInstance("AES/CTR/NOPADDING");
        cipher.init(Cipher.ENCRYPT_MODE, key, generateIV(cipher), random);
        return cipher.doFinal(plaintext.getBytes());
    }

    private String decrypt(String passphrase, byte[] ciphertext) throws Exception {
        SecretKey key = generateKey(passphrase);

        Cipher cipher = Cipher.getInstance("AES/CTR/NOPADDING");
        cipher.init(Cipher.DECRYPT_MODE, key, generateIV(cipher), random);
        return new String(cipher.doFinal(ciphertext));
    }

    private SecretKey generateKey(String passphrase) throws Exception {
        PBEKeySpec keySpec = new PBEKeySpec(passphrase.toCharArray(), salt.getBytes(), iterations, keyLength);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWITHSHA256AND256BITAES-CBC-BC");
        return keyFactory.generateSecret(keySpec);
    }

    private IvParameterSpec generateIV(Cipher cipher) throws Exception {
        byte[] ivBytes = new byte[cipher.getBlockSize()];
        random.nextBytes(ivBytes);
        return new IvParameterSpec(ivBytes);
    }

}
