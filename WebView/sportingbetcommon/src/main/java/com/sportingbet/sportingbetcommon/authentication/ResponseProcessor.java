package com.sportingbet.sportingbetcommon.authentication;

import retrofit.client.Response;

/**
 *
 */
public interface ResponseProcessor {

    void processResponse(Response response);
}
