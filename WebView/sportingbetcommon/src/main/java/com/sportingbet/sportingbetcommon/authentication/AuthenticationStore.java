package com.sportingbet.sportingbetcommon.authentication;

/**
 *
 */
public interface AuthenticationStore {

    String getActiveSessionId();

    void setActiveSessionId(String activeSessionId);
}
