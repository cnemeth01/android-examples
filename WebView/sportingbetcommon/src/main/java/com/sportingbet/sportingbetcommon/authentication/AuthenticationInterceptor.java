package com.sportingbet.sportingbetcommon.authentication;

import retrofit.RequestInterceptor;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public class AuthenticationInterceptor implements RequestInterceptor, ResponseProcessor, AuthenticationStore {
    private static final String ACTIVE_SESSION_ID_KEY = "ASID";

    private String activeSessionId;

    public void processResponse(Response response) {
        for (Header header : response.getHeaders()) {
            if (header.getName().equals(ACTIVE_SESSION_ID_KEY)) {
                activeSessionId = header.getValue();
            }
        }
    }

    @Override
    public void intercept(RequestInterceptor.RequestFacade requestFacade) {
        requestFacade.addHeader(ACTIVE_SESSION_ID_KEY, activeSessionId);
    }

    @Override
    public String getActiveSessionId() {
        return activeSessionId;
    }

    @Override
    public void setActiveSessionId(String activeSessionId) {
        this.activeSessionId = activeSessionId;
    }
}
