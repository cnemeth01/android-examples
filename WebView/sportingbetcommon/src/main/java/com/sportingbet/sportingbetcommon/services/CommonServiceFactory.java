package com.sportingbet.sportingbetcommon.services;

import com.sportingbet.sportingbetcommon.BuildConfig;
import com.sportingbet.sportingbetcommon.authentication.AuthenticationInterceptor;

import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.OkClient;
import retrofit.converter.Converter;

/**
 *
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public class CommonServiceFactory {

    public static LocationService createLocationService(OkClient okClient, String serviceEndPoint, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        RestAdapter.Builder builder = getRestAdapterBuilder(serviceEndPoint, okClient, converter);
        builder.setRequestInterceptor(authenticationInterceptor);
        return builder.build().create(LocationService.class);
    }

    public static SessionService createSessionService(OkClient okClient,String serviceEndPoint,Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        RestAdapter.Builder builder = getRestAdapterBuilder(serviceEndPoint, okClient, converter);
        builder.setRequestInterceptor(authenticationInterceptor);
        return builder.build().create(SessionService.class);
    }

    public static UserServices createUserService(OkClient okClient,String serviceEndPoint, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        RestAdapter.Builder builder = getRestAdapterBuilder(serviceEndPoint, okClient, converter);
        builder.setRequestInterceptor(authenticationInterceptor);
        return builder.build().create(UserServices.class);
    }

    public static ConfigService createConfigService(String serviceEndPoint, OkClient okClient, Converter converter) {
        return getRestAdapterBuilder(serviceEndPoint, okClient, converter).build().create(ConfigService.class);
    }

    public static RestAdapter.Builder getRestAdapterBuilder(String serviceEndpoint, OkClient okClient, Converter converter) {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(serviceEndpoint);
        builder.setConverter(converter);
        builder.setClient(okClient);
        builder.setLogLevel(getLogLevel()).setLog(new AndroidLog("Retrofit"));
        return builder;
    }

    private static RestAdapter.LogLevel getLogLevel() {
        if (BuildConfig.DEBUG) {
            return RestAdapter.LogLevel.FULL;
        } else {
            return RestAdapter.LogLevel.NONE;
        }
    }
}
