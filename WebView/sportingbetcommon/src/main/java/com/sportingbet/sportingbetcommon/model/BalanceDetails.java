package com.sportingbet.sportingbetcommon.model;

/**
 * Model class which represents the User's balance data's
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public class BalanceDetails {

    private String currencyId;

    private Double tradingBalance;

    public String toString() {
        return String.format("%.2f %s", tradingBalance, currencyId);
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public Double getTradingBalance() {
        return tradingBalance;
    }
}
