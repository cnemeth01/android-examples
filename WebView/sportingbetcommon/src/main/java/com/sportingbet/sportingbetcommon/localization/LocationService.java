package com.sportingbet.sportingbetcommon.localization;

/**
 * Created by CSABA_BELA_NEMETH on 9/7/2015.
 */
public interface LocationService {
    void validateLocationWithCompletion();

    void updateLocationWithCompletion();
}
