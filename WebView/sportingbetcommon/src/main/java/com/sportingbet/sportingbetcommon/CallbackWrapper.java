package com.sportingbet.sportingbetcommon;


import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Csaba_Bela_Nemeth on 9/11/2015.
 */
public class CallbackWrapper<T> implements Callback<T> {

    private CommonNetworkCallback commonNetworkCallback;

    public CallbackWrapper(CommonNetworkCallback commonNetworkCallback) {
        this.commonNetworkCallback = commonNetworkCallback;
    }

    @Override
    public void success(T t, Response response) {

    }

    @Override
    public void failure(RetrofitError error) {

    }
}
