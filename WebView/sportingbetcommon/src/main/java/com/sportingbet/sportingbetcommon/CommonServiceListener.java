package com.sportingbet.sportingbetcommon;

/**
 * Created by Csaba_Bela_Nemeth on 9/11/2015.
 */
public interface CommonServiceListener {

    void success();

    void error(Throwable error);
}
