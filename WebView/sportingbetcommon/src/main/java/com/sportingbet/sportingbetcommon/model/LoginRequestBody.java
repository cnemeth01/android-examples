package com.sportingbet.sportingbetcommon.model;

/**
 * Class represents login credentials
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public class LoginRequestBody {

    private String userName;

    private String password;

    public LoginRequestBody(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
