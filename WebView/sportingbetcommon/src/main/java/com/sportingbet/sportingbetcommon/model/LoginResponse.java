package com.sportingbet.sportingbetcommon.model;

import com.sportingbet.sportingbetcommon.model.BalanceDetails;
import com.sportingbet.sportingbetcommon.model.CustomerDetails;

/**
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public class LoginResponse {

    private CustomerDetails customerDetails;

    private BalanceDetails balanceDetails;

    public BalanceDetails getBalanceDetails() {
        return balanceDetails;
    }

    public void setBalanceDetails(BalanceDetails balanceDetails) {
        this.balanceDetails = balanceDetails;
    }
}
