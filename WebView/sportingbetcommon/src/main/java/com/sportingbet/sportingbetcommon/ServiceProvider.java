package com.sportingbet.sportingbetcommon;

import com.sportingbet.sportingbetcommon.services.ConfigService;
import com.sportingbet.sportingbetcommon.services.LocationService;
import com.sportingbet.sportingbetcommon.services.SessionService;

/**
 * Created by Csaba_Bela_Nemeth on 9/24/2015.
 */
public interface ServiceProvider {

    ConfigService getConfigService();

    LocationService getLocationService();

    SessionService getSessionService();
    
}
