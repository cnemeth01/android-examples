package com.sportingbet.sportingbetcommon.model;

/**
 * Class represents user logout reason.
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public class LogoutRequestBody {

    public static final LogoutRequestBody NORMAL = new LogoutRequestBody("NORMAL");

    private String logOffReason;

    private LogoutRequestBody(String logOffReason) {
        this.logOffReason = logOffReason;
    }

    public String getLogOffReason() {
        return logOffReason;
    }
}
