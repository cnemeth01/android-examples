package com.sportingbet.sportingbetcommon.authentication;

import java.io.IOException;

import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;

/**
 *
 */
public class InterceptingOkClient extends OkClient {

    private final ResponseProcessor responseProcessor;

    public InterceptingOkClient(ResponseProcessor responseProcessor) {
        super();
        this.responseProcessor = responseProcessor;
    }

    @Override
    public Response execute(Request request) throws IOException {
        Response response = super.execute(request);
        responseProcessor.processResponse(response);
        return response;
    }
}
