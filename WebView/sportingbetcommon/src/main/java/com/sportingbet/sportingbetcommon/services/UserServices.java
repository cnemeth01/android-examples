package com.sportingbet.sportingbetcommon.services;

import com.sportingbet.sportingbetcommon.model.BalanceDetails;
import com.sportingbet.sportingbetcommon.model.LoginRequestBody;
import com.sportingbet.sportingbetcommon.model.LoginResponse;
import com.sportingbet.sportingbetcommon.model.LogoutRequestBody;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public interface UserServices {

    @POST("/login")
    void login(@Body LoginRequestBody loginRequestBody, Callback<LoginResponse> callback);

    @POST("/logout")
    void logout(@Body LogoutRequestBody logoutRequestBody, Callback<Response> callback);

    @GET("/balance")
    void getBalance(Callback<BalanceDetails> callback);

}
