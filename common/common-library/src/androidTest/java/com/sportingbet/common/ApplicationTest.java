package com.sportingbet.common;

import android.app.Application;
import android.test.ApplicationTestCase;

import com.sportingbet.common.model.CommonBuildConfig;
import com.sportingbet.common.model.Session;
import com.sportingbet.common.services.SessionService;
import com.sportingbet.common.util.ServiceProvider;
import com.sportingbet.common.util.ServiceProviderImpl;

import junit.framework.Assert;

import java.util.concurrent.CountDownLatch;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    private CommonBuildConfig buildConfig;
    private ServiceProvider serviceProvider;
    private SessionService sut;

    public ApplicationTest() {
        super(Application.class);
    }

    public void setUp() throws Exception {
        buildConfig = new CommonBuildConfig();
    }

    public void testSessionServiceWhenCallStableV1_2ApiGetActiveSessionId() {
        //given
        buildConfig.setSessionServiceEndpoint("https://api-stable.sbs.dev2.sportingbet.com/SessionServices/V1/ServiceV1_2/");
        buildConfig.setAppId("ANDROID-SPORTS-APP-V1");
        buildConfig.setDomainId("sportingbet-uk");
        buildConfig.setDebug(true);
        serviceProvider = new ServiceProviderImpl(buildConfig);
        sut = serviceProvider.getSessionService();

        final CountDownLatch signal = new CountDownLatch(1);

        //when
        sut.getActiveSessionId(buildConfig.getDomainId(), buildConfig.getAppId(), new Callback<Session>() {

            @Override
            public void success(Session session, Response response) {
                Assert.assertEquals("LOCAL",session.getLocation());
                signal.countDown();
            }

            @Override
            public void failure(RetrofitError error) {
                Assert.fail();
                signal.countDown();
            }
        });
        //then
        try {
            signal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
