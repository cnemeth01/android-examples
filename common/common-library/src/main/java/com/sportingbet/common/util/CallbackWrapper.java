package com.sportingbet.common.util;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Class used to wrap the Retrofit {@lint Callback} interface Created by Csaba_Bela_Nemeth on 9/11/2015.
 *
 * @param <T> is the return tpye of the callback
 */
public class CallbackWrapper<T> implements Callback<T> {

    private CommonNetworkCallback networkCallBack;

    /**
     * Constructor to the class, with one callback parameter.
     * @param networkCallBack
     */
    public CallbackWrapper(final CommonNetworkCallback networkCallBack) {
        this.networkCallBack = networkCallBack;
    }

    @Override
    public void success(T t, Response response) {

    }

    @Override
    public void failure(RetrofitError error) {

    }
}
