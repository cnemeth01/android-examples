package com.sportingbet.common.model;

/**
 * Created by Csaba_Bela_Nemeth on 9/7/2015.
 */
public class User {

    private String fullName;


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;

    }

    @Override
    public String toString() {
        return "User{"
                + "fullName='" + fullName + '\''
                + '}';
    }
}
