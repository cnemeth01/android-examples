package com.sportingbet.common.network;

import com.sportingbet.common.model.CommonBuildConfig;
import com.sportingbet.common.services.SessionService;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.Client;
import retrofit.converter.Converter;

/**
 * This class create a needed network Service object with Retrofit RestAdapter Builder Created by Csaba_Bela_Nemeth on 9/30/2015.
 */
public class ServiceFactory {

    private CommonBuildConfig configurations;
    private Client okClient;
    private Converter converter;
    private RequestInterceptor authenticationInterceptor;

    /**
     * Constructor, to configure ServiceFactory.
     *
     * @param configurations {@link CommonBuildConfig} object to get BuildConfig inforomation from Applicaton
     * @param okClient OkClient object to manage Http communication
     * @param converter Json converter type.
     * @param authenticationInterceptor Retrofit class which be able to capture responses and requests
     */
    public ServiceFactory(CommonBuildConfig configurations, Client okClient, Converter converter, RequestInterceptor authenticationInterceptor) {
        this.configurations = configurations;
        this.okClient = okClient;
        this.converter = converter;
        this.authenticationInterceptor = authenticationInterceptor;
    }

    /**
     * Creates a SessionService object to be able to start session request.
     *
     * @return {@link SessionService}
     */
    public SessionService createSessionService() {
        RestAdapter.Builder builder = getRestAdapterBuilder(configurations.getSessionServiceEndpoint());
        builder.setRequestInterceptor(authenticationInterceptor);
        return builder.build().create(SessionService.class);
    }

    private RestAdapter.Builder getRestAdapterBuilder(String serviceEndpoint) {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(serviceEndpoint);
        builder.setConverter(converter);
        builder.setClient(okClient);
        builder.setLogLevel(getLogLevel()).setLog(new AndroidLog("Retrofit"));
        return builder;
    }

    private RestAdapter.LogLevel getLogLevel() {
        if (configurations.isDebug()) {
            return RestAdapter.LogLevel.FULL;
        } else {
            return RestAdapter.LogLevel.NONE;
        }
    }

}
