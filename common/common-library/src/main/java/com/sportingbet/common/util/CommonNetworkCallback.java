package com.sportingbet.common.util;

import android.support.annotation.Nullable;

import retrofit.client.Response;

/**
 * Common callback interface which provide callbacks about sportingbet common networking.
 * @param <T> type of the return object.
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public interface CommonNetworkCallback<T> {

    /**
     * Callback method which will be called when networking lasts for more time than usual.
     */
    void onLongTermRunning();

    /**
     * Callback method which will be called when networking finished successfully.
     *
     * @param t        is the {@link T} type object. if it's null there is no callback object, but the
     *                 method run down successfully.
     * @param response is {@link Response} type object.
     */
    void success(@Nullable T t, Response response);

    /**
     * Callback method which will be called when networking finished with failure.
     *
     * @param error is the {@link Throwable} type object
     */
    void failure(Throwable error);
}
