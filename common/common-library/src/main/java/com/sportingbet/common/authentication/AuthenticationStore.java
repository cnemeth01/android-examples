package com.sportingbet.common.authentication;

/**
 * Interface of an memory store of the SessionId.
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public interface AuthenticationStore {
    /**
     * Returns an Active Session ID.
     *
     * @return String ID.
     */
    String getActiveSessionId();

    /**
     * Set the Active Session ID.
     *
     * @param activeSessionId
     */
    void setActiveSessionId(String activeSessionId);
}
