package com.sportingbet.common.services;

import com.sportingbet.common.model.Session;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Query;

/**
 * Interface to create network Rest client requests to Sportingbet API Session Services. Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public interface SessionService {

    /**
     * Method start a GET request to get an Active Session ID.
     *
     * @param domainId not optional query parameter.
     * @param apId     not optional header value.
     * @param callback listener, which returns asynchronously with the result of the request.
     */
    @GET("/session/start")
    void getActiveSessionId(@Query("domainId") String domainId, @Header(value = "APPID") String apId, Callback<Session> callback);

    /**
     * * Method start a GET request to get an Temporary Session ID.
     *
     * @param callback listener, which returns asynchronously with the result of the request.
     */
    @GET("/session/temp")
    void getTempSessionId(Callback<Response> callback);
}
