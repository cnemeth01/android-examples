package com.sportingbet.common.authentication;

import retrofit.client.Response;

/**
 * Interface to capture response object with Retrofit Interceptor.
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public interface ResponseProcessor {
    /**
     *Methode to process response object.
     * @param response
     */
    void processResponse(Response response);
}
