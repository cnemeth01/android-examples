package com.sportingbet.common.authentication;

import java.io.IOException;

import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;

/**
 * Extended class from OkClient, to be able to pass response object to the {@lint ResponseProcessor} object.
 * Created by Csaba_Bela_Nemeth on 9/14/2015.
 */
public class InterceptingOkClient extends OkClient {

    private final ResponseProcessor responseProcessor;

    /**
     * Constructor with a {@link ResponseProcessor} parameter.
     * @param responseProcessor
     */
    public InterceptingOkClient(ResponseProcessor responseProcessor) {
        super();
        this.responseProcessor = responseProcessor;
    }

    @Override
    public Response execute(Request request) throws IOException {
        Response response = super.execute(request);
        responseProcessor.processResponse(response);
        return response;
    }
}
