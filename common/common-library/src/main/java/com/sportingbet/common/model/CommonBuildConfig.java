package com.sportingbet.common.model;

/**
 * This model class represent the needed Application BuildConfig file fields in the common-library
 * It needed to set this class when the application start to run.
 * Created by Csaba_Bela_Nemeth on 9/30/2015.
 */
public final class CommonBuildConfig {

    private boolean debug;
    private String domainId;
    private String accountServiceEndpoint;
    private String configServiceEndpoint;
    private boolean chrashlyticsEnabled;
    private String sessionServiceEndpoint;
    private String appId;

    /**
     * Method get back if debug flag true or false.
     * @return boolean debag true or not.
     */
    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(final String domainId) {
        this.domainId = domainId;
    }

    public String getAccountServiceEndpoint() {
        return accountServiceEndpoint;
    }

    public void setAccountServiceEndpoint(final String accountServiceEndpoint) {
        this.accountServiceEndpoint = accountServiceEndpoint;
    }

    public String getConfigServiceEndpoint() {
        return configServiceEndpoint;
    }

    public void setConfigServiceEndpoint(final String configServiceEndpoint) {
        this.configServiceEndpoint = configServiceEndpoint;
    }

    public boolean isChrashlyticsEnabled() {
        return chrashlyticsEnabled;
    }

    public void setChrashlyticsEnabled(final boolean chrashlyticsEnabled) {
        this.chrashlyticsEnabled = chrashlyticsEnabled;
    }

    public String getSessionServiceEndpoint() {
        return sessionServiceEndpoint;
    }

    public void setSessionServiceEndpoint(final String sessionServiceEndpoint) {
        this.sessionServiceEndpoint = sessionServiceEndpoint;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(final String appId) {
        this.appId = appId;
    }
}
