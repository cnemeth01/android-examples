package com.sportingbet.common.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sportingbet.common.authentication.AuthenticationInterceptor;
import com.sportingbet.common.model.CommonBuildConfig;
import com.sportingbet.common.network.ServiceFactory;
import com.sportingbet.common.services.SessionService;

import retrofit.client.OkClient;
import retrofit.converter.Converter;
import retrofit.converter.JacksonConverter;

import static com.sportingbet.common.network.OkHttpClinetFactory.getUnsafeOkHttpClient;

/**
 * Created by Csaba_Bela_Nemeth on 9/30/2015.
 */
public class ServiceProviderImpl implements ServiceProvider {

    private CommonBuildConfig configurations;
    private ServiceFactory serviceFactory;
    private OkClient okClient;
    private Converter converter;
    private AuthenticationInterceptor authenticationInterceptor;

    /**
     * Constructor with one parameter, wich set the configurations.
     * @param configurations
     */
    public ServiceProviderImpl(final CommonBuildConfig configurations) {
        this.configurations = configurations;
        createDependencies();
        serviceFactory = new ServiceFactory(configurations, okClient, converter, authenticationInterceptor);
    }

    private void createDependencies() {
        createOkClient();
        ObjectMapper objectMapper = createMapper();
        converter = new JacksonConverter(objectMapper);
        authenticationInterceptor = new AuthenticationInterceptor();
    }

    private void createOkClient() {
        if (!configurations.isDebug()) {
            okClient = new OkClient();
        } else {
            okClient = new OkClient(getUnsafeOkHttpClient());
        }

    }

    private ObjectMapper createMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public SessionService getSessionService() {
        return serviceFactory.createSessionService();
    }
}
