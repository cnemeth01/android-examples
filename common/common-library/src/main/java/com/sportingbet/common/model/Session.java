package com.sportingbet.common.model;

/**
 * Created by Csaba_Bela_Nemeth on 10/1/2015.
 */
public class Session {

    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
