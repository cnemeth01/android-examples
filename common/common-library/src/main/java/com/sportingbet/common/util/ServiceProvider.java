package com.sportingbet.common.util;

import com.sportingbet.common.services.SessionService;

/**
 * Interface of Common-library public APIs
 * Created by Csaba_Bela_Nemeth on 9/24/2015.
 */
public interface ServiceProvider {
    /**
     * Returns a SessionService object.
     * @return {@link SessionService} object
     */
    SessionService getSessionService();

}
