package com.sportingbet.common;

import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.util.Collections;

import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Class Mocking Clinet for testing reasons Created by Csaba_Bela_Nemeth on 10/2/2015.
 */
public class MockClient implements Client {

    public static final int STATUS = 200;

    @Override
    public final Response execute(Request request) throws IOException {
        Uri uri = Uri.parse(request.getUrl());

        Log.d("MOCK SERVER", "fetching uri: " + uri.toString());

        String responseString = "";

        if ("/path/of/interest".equals(uri.getPath())) {
            responseString = "JSON STRING HERE";
        } else {
            responseString = "OTHER JSON RESPONSE STRING";
        }

        return new Response(request.getUrl(), STATUS, "nothing", Collections.<Header> emptyList(), new TypedByteArray("application/json", responseString.getBytes()));
    }
}
