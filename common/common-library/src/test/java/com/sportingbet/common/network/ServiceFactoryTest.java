package com.sportingbet.common.network;

import com.sportingbet.common.BuildConfig;
import com.sportingbet.common.model.CommonBuildConfig;
import com.sportingbet.common.model.Session;
import com.sportingbet.common.services.SessionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.converter.JacksonConverter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by nemethcsaba1 on 2015. 10. 03..
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(manifest = "/src/main/AndroidManifest.xml", constants = BuildConfig.class, sdk = 21)
public class ServiceFactoryTest {

    CommonBuildConfig buildConfig;

    @Captor
    private ArgumentCaptor<RequestInterceptor> retrofitCallbackArgumentCaptor;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        buildConfig = new CommonBuildConfig();
        buildConfig.setSessionServiceEndpoint("https://api-stable.sbs.dev2.sportingbet.com/SessionServices/V1/ServiceV1_2/");
        buildConfig.setAppId("ANDROID-SPORTS-APP-V1");
        buildConfig.setDomainId("sportingbet-uk");
        buildConfig.setDebug(true);

    }

    @Test
    public void testSessionServiceIfCreateRequestWhenGivenProperConfigurations() throws Exception {
        // Given
        Client mockedOkHttpClient = mock(Client.class);
        RequestInterceptor mockedRequestInterceptor = mock(RequestInterceptor.class);
        ServiceFactory sut = new ServiceFactory(buildConfig, mockedOkHttpClient, new JacksonConverter(), mockedRequestInterceptor);
        Callback<Session> mockCallback = mock(Callback.class);
        // When
        SessionService sessionService = sut.createSessionService();
        sessionService.getActiveSessionId(buildConfig.getDomainId(), buildConfig.getAppId(), mockCallback);
        // Than
        verify(mockedRequestInterceptor, times(1)).intercept((RequestInterceptor.RequestFacade) retrofitCallbackArgumentCaptor.capture());
        String rightURL = "https://api-stable.sbs.dev2.sportingbet.com/SessionServices/V1/ServiceV1_2/session/start?domainId=sportingbet-uk";
        Header rightAppIDHeader = new Header("APPID:", "ANDROID-SPORTS-APP-V1");
        Header rightASIDHeader = new Header("ASID:", "");
        List<Header> rightHeaders = new ArrayList<>();
        rightHeaders.add(rightAppIDHeader);
        rightHeaders.add(rightASIDHeader);
        Request request = new Request("GET", rightURL, rightHeaders, null);
        RequestInterceptor.RequestFacade requestFacade = new RequestInterceptor.RequestFacade() {
            @Override
            public void addHeader(String name, String value) {

            }

            @Override
            public void addPathParam(String name, String value) {

            }

            @Override
            public void addEncodedPathParam(String name, String value) {

            }

            @Override
            public void addQueryParam(String name, String value) {

            }

            @Override
            public void addEncodedQueryParam(String name, String value) {

            }
        };
        retrofitCallbackArgumentCaptor.getValue().intercept(requestFacade);

    }

    @Test
    public void testSessionServiceRequestWhenGivenProperConfigurations() throws Exception {
        // Given
        Client mockedOkHttpClient = mock(OkClient.class);
        RequestInterceptor requestInterceptor = mock(RequestInterceptor.class);
        ServiceFactory sut = new ServiceFactory(buildConfig, mockedOkHttpClient, new JacksonConverter(), requestInterceptor);
        Callback<Session> mockedCallback = mock(Callback.class);
        SessionService sessionService = sut.createSessionService();
        String rightURL = "https://api-stable.sbs.dev2.sportingbet.com/SessionServices/V1/ServiceV1_2/session/start?domainId=sportingbet-uk";
        Header rightAppIDHeader = new Header("APPID:", "ANDROID-SPORTS-APP-V1");
        Header rightASIDHeader = new Header("ASID:", "");
        List<Header> rightHeaders = new ArrayList<>();
        rightHeaders.add(rightAppIDHeader);
        rightHeaders.add(rightASIDHeader);
        Request request = new Request("GET", rightURL, rightHeaders, null);
        // When
        sessionService.getActiveSessionId(buildConfig.getDomainId(), buildConfig.getAppId(), mockedCallback);
        // Than
        Thread.sleep(500);
        verify(mockedOkHttpClient, times(1)).execute(request);


    }
}
