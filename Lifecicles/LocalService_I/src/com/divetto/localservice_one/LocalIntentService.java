package com.divetto.localservice_one;

import java.util.Arrays;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class LocalIntentService extends IntentService {

    private static final String TAG = "LocalINTENTService";
    public static final String BROADCAST_ACTION = "broadcast_action_of_primes";
    public static final String BROADCAST_DATA_NUMBER = "nubmber_of_primes";
    private boolean[] primes;
    private int numberOfPrimes;

    public LocalIntentService(String name) {
	super(name);
    }

    public LocalIntentService() {
	super("LocalServiceOfPrimes");
    }

    /*
     * The IntentService calls this method from the default worker thread with the intent that started the service. When
     * this method returns, IntentService stops the service, as appropriate.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
	findPrime();
	Intent outintent = new Intent(BROADCAST_ACTION).putExtra(BROADCAST_DATA_NUMBER, numberOfPrimes);
	LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(outintent);
    }

    /*
     * You should not override this method for your IntentService. Instead, override onHandleIntent(Intent), which the
     * system calls when the IntentService receives a start request
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
	Log.e(TAG, "onStartCommand");
	return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
	Log.e(TAG, "onCreate");
	super.onCreate();
    }

    @Override
    public void onDestroy() {
	Log.e(TAG, "onDestory");
	super.onDestroy();
    }

    private void findPrime() {
	// will contain true or false values for the first 10,000 integers
	primes = new boolean[5];
	// set up the primesieve
	fillSieve();
	for (int i = 0; i < primes.length; i++) {
	    Log.w("isPrime:_INTENT_SERVICE" + i, String.valueOf(primes[i]));
	    if (!primes[i]) {
		++numberOfPrimes;
	    }
	}

	try {
	    Thread.sleep(3000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}

    }

    public void fillSieve() {
	Arrays.fill(primes, true); // assume all integers are prime.
	primes[0] = primes[1] = false; // we know 0 and 1 are not prime.
	for (int i = 2; i < primes.length; i++) {
	    // if the number is prime,
	    // then go through all its multiples and make their values false.
	    if (primes[i]) {
		for (int j = 2; i * j < primes.length; j++) {
		    primes[i * j] = false;
		    Log.d("calculating:_INTENTService" + i, "doing....");
		}
	    }
	}
    }

    public boolean isPrime(int n) {
	return primes[n]; // simple, huh?
    }

}
