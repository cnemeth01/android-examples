package com.example.ownmp3player;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

import com.example.ownmp3player.MusicPlayService.LocalBinder;

public class MainActivity extends ActionBarActivity {
    private MusicPlayService mService;
    private Intent mIntent;
    private boolean binded;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            binded = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.e("MusicPlayerActivity", "onServiceConnected");
            binded = true;
            LocalBinder mLocalBinder = (LocalBinder) service;
            mService = mLocalBinder.getServiceInstance();
            // need some seconds until done
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mIntent = new Intent(MainActivity.this, MusicPlayService.class);

        bindService(mIntent, mConnection, BIND_AUTO_CREATE);

    }

    public void startPlay(View v) {
        mService.playMusic();
    }

    public void stopPlay(View v) {
        mService.stopMusic();
    }

    public void pausePlay(View v) {
        mService.pauseMusic();
    }

    @Override
    protected void onStop() {
        if (binded) {
            unbindService(mConnection);
            stopService(mIntent);
        }
        super.onStop();
    }

}
