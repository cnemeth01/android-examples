package com.example.lifecicles;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class ThirdActivity extends Activity {

	private int test3Int=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_third);
	//	if (!savedInstanceState.isEmpty()) {
	//		test3Int=savedInstanceState.getInt("testNum");
	//	}
		
		Log.d("CSABI", "3. activity onCreate");

		
	}
	public void onClick(View v){
		if (v.getId()==R.id.buttonThird1) {
			Log.d("CSABI", "3. activity onClick go to 2.Activity");
			finish();
		}
		if (v.getId()==R.id.buttonThird2) {
			Log.d("CSABI", "3. activity onClick go to 1.Activity");
			
			
			Intent i=new Intent();
			i.setClass(this, MainActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
		}
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		
		Log.d("CSABI", "3. activity onSaveInstanceState");
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		test3Int=123;
		outState.putInt("testNum", test3Int);
		
		
		
		Log.d("CSABI", "3. activity onSaveInstanceState");
		super.onSaveInstanceState(outState);
	}
@Override
protected void onStart() {
	Log.d("CSABI", "3. activity onStart");
	super.onStart();
}
@Override
protected void onRestart() {
	Log.d("CSABI", "3. activity onRestart");
	super.onRestart();
}
@Override
protected void onResume() {
	Log.d("CSABI", "3. activity onResume");
	Toast.makeText(this, ""+test3Int, Toast.LENGTH_LONG).show();
	super.onResume();
}
@Override
protected void onPause() {
	Log.d("CSABI", "3. activity onPause");
	super.onPause();
}
@Override
protected void onStop() {
	Log.d("CSABI", "3. activity onStop");
	super.onStop();
}
@Override
protected void onDestroy() {
	Log.d("CSABI", "3. activity onDestroy");
	super.onDestroy();
}
	
	
}
