package com.example.lifecicles;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	private int test1Int=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	//	if (!savedInstanceState.isEmpty()) {
	//		test1Int=savedInstanceState.getInt("testNum");
	//	}
		
		
		Log.d("CSABI", "1. activity onCreate");
		

		
	}

public void onClick(View v){
	if (v.getId()==R.id.button1) {
		Log.d("CSABI", "1. activity onClick go to 2.Activity");
		Intent i=new Intent();
		i.setClass(this, SecActivity.class);
		startActivity(i);
	}
	if (v.getId()==R.id.button2) {
		Log.d("CSABI", "1. activity onClick go to 3.Activity");
		Intent i=new Intent();
		i.setClass(this, ThirdActivity.class);
		startActivity(i);
	}
}
@Override
protected void onRestoreInstanceState(Bundle savedInstanceState) {
	
	Log.d("CSABI", "1. activity onSaveInstanceState");
	super.onRestoreInstanceState(savedInstanceState);
}

@Override
protected void onSaveInstanceState(Bundle outState) {
	test1Int=111;
	outState.putInt("testNum", test1Int);
	Log.d("CSABI", "1. activity onSaveInstanceState");
	super.onSaveInstanceState(outState);
}
@Override
protected void onStart() {
Log.d("CSABI", "1. activity onStart");
super.onStart();
}
@Override
protected void onRestart() {
Log.d("CSABI", "1. activity onRestart");
super.onRestart();
}
@Override
protected void onResume() {
	Toast.makeText(this, ""+test1Int, Toast.LENGTH_LONG).show();
Log.d("CSABI", "1. activity onResume");
super.onResume();
}
@Override
protected void onPause() {
Log.d("CSABI", "1. activity onPause");
super.onPause();
}
@Override
protected void onStop() {
Log.d("CSABI", "1. activity onStop");
super.onStop();
}
@Override
protected void onDestroy() {
Log.d("CSABI", "1. activity onDestroy");
super.onDestroy();
}

}