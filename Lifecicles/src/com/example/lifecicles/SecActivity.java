package com.example.lifecicles;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class SecActivity extends Activity {

	private int test2Int=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sec);
	//	if (!savedInstanceState.isEmpty()) {
		//	test2Int=savedInstanceState.getInt("testNum");
	//	}
		
		
		Log.d("CSABI", "2. activity onCreate");

		
	}
	public void onClick(View v){
		if (v.getId()==R.id.buttonSec1) {
			Log.d("CSABI", "2. activity onClick go to 1.Activity");
			finish();
		}
		if (v.getId()==R.id.buttonSec2) {
			Log.d("CSABI", "2. activity onClick go to 3.Activity");
			Intent i=new Intent();
			i.setClass(this, ThirdActivity.class);
			startActivity(i);
		}
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		
		Log.d("CSABI", "2. activity onSaveInstanceState");
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		test2Int=222;
		
		outState.putInt("testNum", test2Int);
		
		Log.d("CSABI", "2. activity onSaveInstanceState");
		super.onSaveInstanceState(outState);
	}
@Override
protected void onStart() {
	Log.d("CSABI", "2. activity onStart");
	super.onStart();
}
@Override
protected void onRestart() {
	Log.d("CSABI", "2. activity onRestart");
	super.onRestart();
}
@Override
protected void onResume() {
	Log.d("CSABI", "2. activity onResume");
	Toast.makeText(this, ""+test2Int, Toast.LENGTH_LONG).show();
	super.onResume();
}
@Override
protected void onPause() {
	Log.d("CSABI", "2. activity onPause");
	super.onPause();
}
@Override
protected void onStop() {
	Log.d("CSABI", "2. activity onStop");
	super.onStop();
}
@Override
protected void onDestroy() {
	Log.d("CSABI", "2. activity onDestroy");
	super.onDestroy();
}
	
}