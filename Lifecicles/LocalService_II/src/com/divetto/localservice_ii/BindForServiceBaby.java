package com.divetto.localservice_ii;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class BindForServiceBaby extends Service {

    private String name = "MyLocalService";
    private int value = 5;

    private static final String TAG = "LocalServiceII";
    private LocalBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {

	public BindForServiceBaby getServiceInstance() {
	    return BindForServiceBaby.this;
	}
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
	Log.e("onStartCommand", "onStartCommand");
	return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
	Log.e(TAG, "onBind");
	return mBinder;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public int getValue() {
	return value;
    }

    public void setValue(int value) {
	this.value = value;
    }

}
