package com.example.audiorecorderdemo;

import java.io.File;
import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnInfoListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;

public class AudioRecorderActivity extends Activity {

	private MediaRecorder recorder;
	private MediaPlayer player;
	private static final String outputPath = 
			Environment.getExternalStorageDirectory().getAbsolutePath() + "/soundtest.3gp";
	private ImageButton recordButton;
	private ImageButton playButton;
	private ImageButton stopButton;
	private Chronometer chronometer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_audio_recorder);
		
		recordButton = (ImageButton)findViewById(R.id.recordButton);
		playButton = (ImageButton)findViewById(R.id.playButton);
		stopButton = (ImageButton)findViewById(R.id.stopButton);
		chronometer = (Chronometer)findViewById(R.id.chronometer);
	}

	public void record(View view) {
		Log.d("RECORDER", outputPath);
		if (player != null) {
			stopPlayer();
		}
		recordButton.setEnabled(false);
		recorder = new MediaRecorder();
		recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
		recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		recorder.setMaxDuration(10000);
		File outputFile = new File(outputPath);
		if (outputFile.exists()) 
			outputFile.delete();
		recorder.setOutputFile(outputPath);
		recorder.setOnInfoListener(new OnInfoListener() {
			
			@Override
			public void onInfo(MediaRecorder mr, int what, int extra) {
				if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
					stopRecorder();
				}
			}
		});
		try {
			recorder.prepare();
			chronometer.setBase(SystemClock.elapsedRealtime());
			chronometer.start();
			recorder.start();
		} catch (IOException e) {
			Log.w("RECORDER", e);
			stopRecorder();
		}
		
	}
	
	public void play(View view) {
		if (recorder != null) {
			stopRecorder();
		}
		playButton.setEnabled(false);
		player = new MediaPlayer();
		try {
			player.setDataSource(outputPath);
			player.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					chronometer.stop();
					playButton.setEnabled(true);
				}
			});
			player.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer arg0) {
					chronometer.setBase(SystemClock.elapsedRealtime());
					chronometer.start();
					player.start();
				}
			});
			player.prepare();
		} catch (IOException e) {
			Log.w("PLAYER", e);
			stopPlayer();
		}
	}
	
	public void stop(View view) {
		stopRecorder();
		stopPlayer();
		chronometer.setBase(SystemClock.elapsedRealtime());
	}
	
	public void stopPlayer() {
		if (player != null) {
			chronometer.stop();
			playButton.setEnabled(true);
			player.stop();
			player.release();
			player = null;
		}
	}
	
	public void stopRecorder() {
		if (recorder != null) {
			chronometer.stop();
			recordButton.setEnabled(true);
			recorder.stop();
			recorder.release();
			recorder = null;
		}
	}

}
