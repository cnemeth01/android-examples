package com.epam.pocmeetapp.Parse;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.epam.pocmeetapp.activities.MainScheduleActivity;
import com.epam.pocmeetapp.interfaces.ParseCallBack;
import com.epam.pocmeetapp.pojos.MeetUp;
import com.epam.pocmeetapp.pojos.Participant;
import com.epam.pocmeetapp.pojos.Speakers;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseACL;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRole;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SignUpCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Csaba_Bela_Nemeth on 10/3/2014.
 */
public class ParseHelper {

    private Context context;
    private ParseCallBack mListener;
    public static final List<MeetUp> meetUpList = new ArrayList<MeetUp>();
    public static final List<Participant> participantList = new ArrayList<Participant>();
    private ParseRole userRole;
    private ParseRole administratorRole;
    private ParseUser adminUser;
    private ParseACL roleACL;


    public ParseHelper(Context context,ParseCallBack mListener) {
        this.context = context;
        this.mListener= mListener;
    }



    public void getMeetUps() {

        ParseQuery<MeetUp> query = ParseQuery.getQuery(MeetUp.class);
        query.orderByAscending("Theme");

        query.findInBackground(new FindCallback<MeetUp>() {
            public void done(List<MeetUp> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    meetUpList.clear();
                    meetUpList.addAll(scoreList);
                    mListener.parseQueryDone(true);
                    //if (participantList.isEmpty()) {
                    //    getParticipiants();
                    //}
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }



    private void getParticipiants() {
        ParseQuery<Participant> query = ParseQuery.getQuery(Participant.class);
        query.orderByAscending("Theme");
        query.findInBackground(new FindCallback<Participant>() {
            public void done(List<Participant> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "participiants Retrieved  " + scoreList.size() + " scores");
                    participantList.clear();
                    participantList.addAll(scoreList);

                    addParticipiants();

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });


    }

    private void addParticipiants() {
        for (MeetUp meetUp : meetUpList) {
            //for (Participant participant : participantList) {
            //    meetUp.addParticipant(participant);
            //}
            meetUp.put("participants", participantList);
            meetUp.saveInBackground();
        }
    }
// There ar two roles, User, and Administrator, User allows to create comments and allows to delete, modify its own objects
// Administrator allows to delete and modify all type of objects
    public void initParse(Intent intent)  {
        setParseAdminUser();
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        defaultACL.setPublicReadAccess(true);

// Administrator can also modify these objects
        defaultACL.setRoleWriteAccess("Administrator",true);
// And the user can read and modify its own objects
        ParseACL.setDefaultACL(defaultACL, true);

        ParseAnalytics.trackAppOpened(intent);
        PushService.setDefaultPushCallback(context, MainScheduleActivity.class);
        ParseObject.registerSubclass(MeetUp.class);
        ParseObject.registerSubclass(Participant.class);
        ParseObject.registerSubclass(Speakers.class);

        // By specifying no write privileges for the ACL, we can ensure the role cannot be altered.
        roleACL = new ParseACL();
        roleACL.setPublicReadAccess(true);
        userRole = new ParseRole("User", roleACL);
        userRole.saveInBackground();

    }

    private void setParseAdminUser() {
        // By specifying write privileges for the ACL, we can ensure the role cannot be altered.
        ParseACL administratorRoleACL = new ParseACL();
        administratorRoleACL.setPublicWriteAccess(true);
        administratorRole = new ParseRole("Administrator", administratorRoleACL);
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        ParseUser adminUser= null;
        try {
            adminUser = query.get("LCFuIgLuby");
            Log.d("admin user query ",adminUser.getUsername());
            administratorRole.getUsers().add(adminUser);
            Log.d("admin added", "done");
            administratorRole.saveInBackground();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }


    public void setUser(String mPassword, String mEmail) {
        final ParseUser user = new ParseUser();
        user.setUsername(mEmail);
        user.setPassword(mPassword);
        user.setEmail(mEmail);

        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                if (e == null) {

                    ParseRole role = new ParseRole("user", roleACL);
                    role.getUsers().add(user);

                    getMeetUps();
                } else {
                    Toast.makeText(context,""+ e,Toast.LENGTH_LONG).show();
                    mListener.parseQueryDone(false);

                }
            }
        });


    }

    public void authenticationUser(String password, String email) {

        ParseUser.logInInBackground(email, password, new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                if (user != null) {
                    getMeetUps();
                } else {
                    Toast.makeText(context,""+ e,Toast.LENGTH_LONG).show();
                    mListener.parseQueryDone(false);
                }
            }
        });


    }
}
