package com.epam.pocmeetapp.activities;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;

import android.app.Activity;
import android.util.Log;




/**
 * A base class to wrap communication with the Google Play Services PlusClient.
 */
public abstract class PlusBaseActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
