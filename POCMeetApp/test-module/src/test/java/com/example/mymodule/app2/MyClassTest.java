package com.example.mymodule.app2;

import org.junit.Test;

import static org.junit.Assert.*;

public class MyClassTest {

    @Test
    public void testGetHelloWorld() throws Exception {
        MyClass myClass = new MyClass();
        String text = myClass.getHelloWorld();
        assertEquals(text,"Hello World");

    }
}