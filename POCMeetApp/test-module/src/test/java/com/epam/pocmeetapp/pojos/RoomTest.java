package com.epam.pocmeetapp.pojos;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RoomTest {

    private Room sut;

    @Before
    public void setUp() throws Exception {
        sut = new Room();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetRoomColorWhenSetRoom() throws Exception {
        sut.setRoomColor(1001);
        assertEquals(1001, sut.getRoomColor());
    }

    @Test
    public void testSetRoomColor() throws Exception {

    }

    @Test
    public void testGetRoomNumber() throws Exception {

    }

    @Test
    public void testSetRoomNumber() throws Exception {

    }
}