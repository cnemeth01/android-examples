package com.example.sulyregiszter_wv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.sulyregiszter.R;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SulyActivity extends Activity {
	
	private WebView webView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_suly);

		webView = (WebView) findViewById(R.id.webView);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.loadUrl("http://www.programozas-oktatas.hu/fogyas/webindex.php?u=gipszjakab");
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setInitialScale(50);
	    webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webView.getSettings().setUseWideViewPort(true);
		webView.requestFocus();
		//outputTV = (TextView) findViewById(R.id.outputTextView);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	public void frissit(View view) {
		webView.reload();
	}

}
