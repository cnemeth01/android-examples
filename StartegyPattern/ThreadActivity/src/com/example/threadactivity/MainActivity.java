package com.example.threadactivity;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

    private ScrollView sw;
    private int i = 0;
    private MyThread thread;
    private LinearLayout lin;
 
  

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sw = (ScrollView) findViewById(R.id.sw_main);
        lin=(LinearLayout) findViewById(R.id.lin_main);

    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        public void onReceive(android.content.Context context, android.content.Intent intent) {

            ArrayList<Integer> primes = intent.getIntegerArrayListExtra("primes");
          
          

            for (Integer integer : primes) {
             
               
                TextView tw = new TextView(MainActivity.this);
                tw.setText("Prime: " + integer);

                lin.addView(tw);
            }

            thread.setRunning(false);

        };
    };

    @Override
    protected void onResume() {
        initReceiverAndStartThread();
        super.onResume();
    }

    private void initReceiverAndStartThread() {
        IntentFilter filter = new IntentFilter(KEYS.BROADCAST_THREAD_KEY);
        registerReceiver(br, filter);

        thread = new MyThread(getApplicationContext());
        thread.start();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(br);
        if (i < 5) {
            thread.setRunning(false);
        }
        super.onPause();
    }

}
