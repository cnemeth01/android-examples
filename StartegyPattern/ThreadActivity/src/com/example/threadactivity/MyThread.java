package com.example.threadactivity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;

public class MyThread extends Thread {

    private static final long TWO_SECOND = 2000;
    private Context context;
    private boolean run;
    private boolean isPrim = false;
    private ArrayList<Integer> primes = new ArrayList<Integer>();

    public MyThread(Context c) {
        context = c;
        run = true;
    }

    public void setRunning(boolean stop) {
        run = stop;
    }

    @Override
    public void run() {
        while (run) {

            for (int i = 2; i <= 50000; i++) {

                if (isPrime(i))
                    primes.add(i);

                if (i == 10000 || i == 20000 || i == 30000 || i == 40000 || i == 50000) {

                    synchronized (this) {
                        try {

                            this.wait(TWO_SECOND);
                            sendBroadCast(primes);

                            primes.clear();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }

            }
        }

    }

    private void sendBroadCast(ArrayList<Integer> primes) {
        Intent intent = new Intent(KEYS.BROADCAST_THREAD_KEY);
        intent.putIntegerArrayListExtra("primes", primes);
        context.sendBroadcast(intent);
    }

    public static boolean isPrime(int num) {
        boolean prime = true;
        int limit = (int) Math.sqrt(num);

        for (int i = 2; i <= limit; i++) {
            if (num % i == 0) {
                prime = false;
                break;
            }
        }

        return prime;
    }

}
