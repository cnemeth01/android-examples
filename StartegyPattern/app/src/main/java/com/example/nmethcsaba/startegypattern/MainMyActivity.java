package com.example.nmethcsaba.startegypattern;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;

import fragments.PlayGroundFragment;


public class MainMyActivity extends Activity implements PlayGroundFragment.OnFragmentInteractionListener {

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main);

        setContentView(R.layout.activity_main_my);
        fragmentManager = getFragmentManager();

        onPlayGroundFragment();

    }

    private void onPlayGroundFragment() {
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlayGroundFragment.newInstance("", ""))
                .commit();
    }




    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
