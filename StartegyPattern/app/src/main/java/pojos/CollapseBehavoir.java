package pojos;

/**
 * Created by Németh Csaba on 2014.09.21..
 */
public abstract class CollapseBehavoir {

    public abstract Double ballSpeedXAfterCollapse(Double ballSpeedX);
    public abstract Double ballSpeedYAfterCollapse(Double ballSpeedY);
}
