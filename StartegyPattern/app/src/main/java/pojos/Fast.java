package pojos;

/**
 * Created by Németh Csaba on 2014.09.21..
 */
public class Fast extends CollapseBehavoir {
    @Override
    public Double ballSpeedXAfterCollapse(Double ballSpeedX) {
        return ballSpeedX = -ballSpeedX * 1.1;
    }

    @Override
    public Double ballSpeedYAfterCollapse(Double ballSpeedY) {
        return ballSpeedY = -ballSpeedY * 1.1;
    }
}
