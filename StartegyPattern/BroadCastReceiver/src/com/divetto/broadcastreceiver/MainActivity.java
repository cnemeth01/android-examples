package com.divetto.broadcastreceiver;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends Activity {

    private static TextView tw;
    private int i = 0;
    private MyThread thread;

    private BroadcastReceiver br = new BroadcastReceiver() {
        public void onReceive(android.content.Context context, android.content.Intent intent) {
            ++i;
            tw.setText(String.valueOf(i));
            if (i == 5) {
                thread.setRunning(false);
            }
        };
    };

    @Override
    protected void onResume() {
        initReceiverAndStartThread();
        super.onResume();
    }

    private void initReceiverAndStartThread() {
        IntentFilter filter = new IntentFilter(KEYS.BROADCAST_THREAD_KEY);
        registerReceiver(br, filter);
        thread = new MyThread(getApplicationContext());
        thread.start();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(br);
        if (i < 5) {
            thread.setRunning(false);
        }
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            tw = (TextView) rootView.findViewById(R.id.tw);
            return rootView;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }
}
