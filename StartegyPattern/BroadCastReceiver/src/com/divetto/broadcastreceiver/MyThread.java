package com.divetto.broadcastreceiver;

import android.content.Context;
import android.content.Intent;

public class MyThread extends Thread {

    private static final long THREE_SECOND = 3000;
    private Context context;
    private boolean run;

    public MyThread(Context c) {
        context = c;
        run = true;
    }

    public void setRunning(boolean stop) {
        run = stop;
    }

    @Override
    public void run() {
        while (run) {

            synchronized (this) {
                try {

                    this.wait(THREE_SECOND);
                    sendBroadCast();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void sendBroadCast() {
        Intent intent = new Intent(KEYS.BROADCAST_THREAD_KEY);
        context.sendBroadcast(intent);
    }

}
