package com.shared.preferences_example;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class SharedPreferenciesMainActivity extends Activity {
    public static final String PREFS_NAME = "MyPrefsFile";
    public static final String USERNAME_KEY = "username_key";
    public static final String FIRST_NAME_KEY = "first_name_key";
    public static final String LAST_NAME_KEY = "last_name_key";
    public static final String EMAIL_KEY = "email_key";
    public static final String PASSWORD_KEY = "password_key";

    private EditText username;
    private EditText firstname;
    private EditText Lastname;
    private EditText email;
    private EditText password;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferencies_main);

        username = (EditText) findViewById(R.id.username);
        firstname = (EditText) findViewById(R.id.Firstname);
        Lastname = (EditText) findViewById(R.id.Lastname);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        textView = (TextView) findViewById(R.id.text_to_display);
    }

    @Override
    protected void onResume() {
        restore();
        super.onResume();
    }

    private void restore() {
        // Restore preferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String str_username = settings.getString(USERNAME_KEY, "none");
        String str_firstname = settings.getString(FIRST_NAME_KEY, "none");
        String str_lastname = settings.getString(LAST_NAME_KEY, "none");
        String str_email = settings.getString(EMAIL_KEY, "none");

        textView.setText(str_username + "\n" + str_firstname + "\n" + str_lastname + "\n" + str_email);
    }

    @Override
    protected void onStop() {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
       
        SharedPreferences.Editor editor = settings.edit();
        
        editor.putString(USERNAME_KEY, username.getText().toString());
        editor.putString(FIRST_NAME_KEY, firstname.getText().toString());
        editor.putString(LAST_NAME_KEY, Lastname.getText().toString());
        editor.putString(EMAIL_KEY, email.getText().toString());
        editor.putString(PASSWORD_KEY, password.getText().toString());

        // Commit the edits!
        editor.commit();
        super.onStop();
    }

}
