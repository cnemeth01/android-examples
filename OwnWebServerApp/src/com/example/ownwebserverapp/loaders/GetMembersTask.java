package com.example.ownwebserverapp.loaders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.os.Handler;
import android.os.Message;

import com.example.ownwebserver.helpers.NetworkHelper;
import com.example.ownwebserverapp.interfaces.URLs;
import com.example.ownwebserverapp.php.SendPHPRequest;
import com.google.gson.Gson;

public class GetMembersTask implements Runnable {

    Handler handler;
    private SendPHPRequest myPHP;

    public GetMembersTask(Handler handler) {

        this.handler = handler;
    }

    @Override
    public void run() {

        List<NameValuePair> postParams = new ArrayList<NameValuePair>();
        String name = "Csabi";
        String jsonString = new Gson().toJson(name);
        postParams.add(new BasicNameValuePair("name", jsonString));
        String responseFromServer = "nincs valasz";
        myPHP = new SendPHPRequest();
        try {
            responseFromServer = NetworkHelper.sendHttpPost(postParams, URLs.HTTP_GET_MEMBERS_PHP);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Message msg = new Message();
        msg.obj = responseFromServer;
        handler.sendMessage(msg);

    }

}
