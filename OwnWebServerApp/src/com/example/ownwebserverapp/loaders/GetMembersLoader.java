package com.example.ownwebserverapp.loaders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.example.ownwebserver.helpers.NetworkHelper;
import com.example.ownwebserverapp.interfaces.URLs;
import com.example.ownwebserverapp.pojos.MemberModel;
import com.example.ownwebserverapp.pojos.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class GetMembersLoader extends AsyncTaskLoader<Response> {

	private MemberModel member;
	private String jsonResponse;
	private Response response = new Response();

	public GetMembersLoader(Context context) {
		super(context);
	}

	@Override
	public Response loadInBackground() {

		try {

			jsonResponse = NetworkHelper.sendHttpGet(URLs.HTTP_GET_MEMBERS_PHP);
			Log.d("CS getmembers: ", jsonResponse);
			List<MemberModel> members;
			members = new Gson().fromJson(jsonResponse, new TypeToken<List<MemberModel>>() {
			}.getType());

			response.setObject(members);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

}
