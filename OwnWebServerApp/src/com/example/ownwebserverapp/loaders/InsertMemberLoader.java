package com.example.ownwebserverapp.loaders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.example.ownwebserver.helpers.NetworkHelper;
import com.example.ownwebserverapp.interfaces.URLs;
import com.example.ownwebserverapp.pojos.MemberModel;
import com.example.ownwebserverapp.pojos.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class InsertMemberLoader extends AsyncTaskLoader<Response> {

	private MemberModel member;
	private String jsonResponse;
	private Response response = new Response();

	public InsertMemberLoader(Context context, MemberModel member) {
		super(context);
		this.member = member;
	}

	@Override
	public Response loadInBackground() {

		Response response = new Response();
		String jsonMember = new Gson().toJson(member);
		List<NameValuePair> postParams = new ArrayList<NameValuePair>();
		try {
			jsonResponse = "Problem with net";
			postParams.add(new BasicNameValuePair("member", jsonMember));
			jsonResponse = NetworkHelper.sendHttpPost(postParams, URLs.HTTP_INSERT_MEMBER_PHP);

			Log.d("Json response :", jsonResponse);
			if (jsonResponse.contains("Succes")) {
				response.setSuccess(true);

			} else {
				response.setSuccess(false);
				response.setErrorMessage("Writing database failed");
			}
		} catch (IOException e) {
			response.setSuccess(false);
			response.setErrorMessage(e.toString());
		}
		return response;
	}
}
