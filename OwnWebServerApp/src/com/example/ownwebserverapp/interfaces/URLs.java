package com.example.ownwebserverapp.interfaces;

public class URLs {

    public static final String HTTP_GET_MEMBERS_PHP = "http://csacsicsabi.esy.es/get_members.php";
    public static final String HTTP_INSERT_MEMBER_PHP = "http://csacsicsabi.esy.es/insert_query.php";
    public static final String HTTP_DELETE_MEMBER_PHP = "http://csacsicsabi.esy.es/delete_query.php";
    public static final String HTTP_UPDATE_MEMBER_PHP = "http://csacsicsabi.esy.es/update_query.php";
   
}
