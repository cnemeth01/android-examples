package com.example.ownwebserverapp.pojos;

public class MemberModel implements Comparable<MemberModel> {

    private String name;
    private String email;
    private int id;
    
    


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

	@Override
	public int compareTo(MemberModel another) {
		
		return this.name.compareToIgnoreCase(another.getName());
	}

}
