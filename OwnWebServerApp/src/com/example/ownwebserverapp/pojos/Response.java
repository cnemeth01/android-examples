package com.example.ownwebserverapp.pojos;

public class Response {
    
    
    private boolean success;
    private Object object;
    private String errorMessage;
    private boolean conectionError;
    public boolean isSuccess() {
        return success;
    }
    public void setSuccess(boolean success) {
        this.success = success;
    }
    public Object getObject() {
        return object;
    }
    public void setObject(Object object) {
        this.object = object;
    }
    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    public boolean isConectionError() {
        return conectionError;
    }
    public void setConectionError(boolean conectionError) {
        this.conectionError = conectionError;
    }
    
    
    
    
    
}

