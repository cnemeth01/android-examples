package com.example.ownwebserver.dataservices;

import android.content.Context;
import android.os.Handler;

import com.example.ownwebserverapp.interfaces.IWebDataService;
import com.example.ownwebserverapp.loaders.GetMembersTask;
import com.example.ownwebserverapp.pojos.MemberModel;

public class WebDataServiceWithThread implements IWebDataService {

    @Override
    public void getMembers(Handler handler) {

        GetMembersTask getMembers = new GetMembersTask(handler);
        Thread getDataThread = new Thread(getMembers);
        getDataThread.start();

    }

    @Override
    public void getMemberById(Handler handler) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addMember(MemberModel newMember, Handler handler) {
        // TODO Auto-generated method stub

    }

    @Override
    public void deleteMember(MemberModel deletMember, Handler handler) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updatemember(MemberModel updateMember, Handler handler) {
        // TODO Auto-generated method stub

    }

}
