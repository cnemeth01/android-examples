package com.supersoftwares.materialdesign;

import java.util.EnumSet;
import java.util.Set;

/**
 * Created by Csaba_Bela_Nemeth on 6/1/2015.
 */
public class Text {
    Set<Style> styles;

    public void applyStyles(Set<Style> styles) {
        this.styles = styles;
    }

    public enum Style {BOLD, ITALIC, UNDERLINE}

    public EnumSet<Style> getStyles(){
        return (EnumSet<Style>) styles;
    }
}
