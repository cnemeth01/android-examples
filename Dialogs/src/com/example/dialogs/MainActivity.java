package com.example.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		
	}
	public void onClick(View v){
		if (v.getId()==R.id.btnOpnenDialog) {
			showAlertMessage("MeglepetÚs !!");
		}
	}

	private void showAlertMessage(final String aMessage) {
        AlertDialog.Builder alertbox = 
          new AlertDialog.Builder(this);
        alertbox.setMessage(aMessage);
        alertbox.setNeutralButton("Ok",
          new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0,
             int arg1) {
              Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_LONG).show();
            }
          });
        alertbox.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Toast.makeText(MainActivity.this,
          item.getTitle(), Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }

}
