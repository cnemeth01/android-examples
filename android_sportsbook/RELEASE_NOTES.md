Release Notes
================



- #NANDROID-191 - Side Bar Six Pack - Sport selected state - http://jira.gvcgroup.local/browse/NANDROID-191
- #NANDROID-282 - Selected state within Side Bar - Remove - http://jira.gvcgroup.local/browse/NANDROID-282
- #NANDROID-250 - Icon: LIVE Betting - http://jira.gvcgroup.local/browse/NANDROID-250
- #NANDROID-253 - My Bets - Remove VIEW MY ACCOUNT - http://jira.gvcgroup.local/browse/NANDROID-253
- #NANDROID-187 - Sub Header 4tabs to match IOS - http://jira.gvcgroup.local/browse/NANDROID-187
- #NANDROID-269 - My Bets. Cash Out tab shouldn't be opened after refreshing All tab - http://jira.gvcgroup.local/browse/NANDROID-269
- #NANDROID-270 - Cash Out - P1 OutCome Successful - Bet Receipt - http://jira.gvcgroup.local/browse/NANDROID-270
- #NANDROID-276 - My Bets. Sometimes Cash out button is disabled - http://jira.gvcgroup.local/browse/NANDROID-276
- #NANDROID-217 - Cash Out - P1 OutCome Temp Unavailable/ Any other error - http://jira.gvcgroup.local/browse/NANDROID-217
- #NANDROID-203 - Coupon Vertical Spacing - http://jira.gvcgroup.local/browse/NANDROID-203
- #NANDROID-223 - Cash Out - P1 OutCome Perm Unavailable - http://jira.gvcgroup.local/browse/NANDROID-223
- #NANDROID-221 - Cash Out - P1 Press Cash Out/ Count Down timer - http://jira.gvcgroup.local/browse/NANDROID-221
- #NANDROID-163 - Bet placement - Show countdown animation when placing in:running bets - http://jira.gvcgroup.local/browse/NANDROID-163
- #NANDROID-218 - Cash Out - P1 OutCome Price change - http://jira.gvcgroup.local/browse/NANDROID-218
- #NANDROID-224 - Cash Out - P1 OutCome Successful - http://jira.gvcgroup.local/browse/NANDROID-224
- #NANDROID-203 - Coupon Vertical Spacing - http://jira.gvcgroup.local/browse/NANDROID-203
- #NANDROID-263 - Application crashes after opening Bet Slip with E/W selections - http://jira.gvcgroup.local/browse/NANDROID-263
- #NANDROID-225 - Cash Out - P1 Default view - http://jira.gvcgroup.local/browse/NANDROID-225
- #NANDROID-246 - Up Next Time + Score Colour - http://jira.gvcgroup.local/browse/NANDROID-246
- #NANDROID-123 - Coupon. There is no bar with "1, 2" above Match Prices cell - http://jira.gvcgroup.local/browse/NANDROID-123
- #NANDROID-198 - Bet Slip Horizontal Stake Input - http://jira.gvcgroup.local/browse/NANDROID-198
- #NANDROID-123 - Coupon. There is no bar with "1, 2" above Match Prices cell - http://jira.gvcgroup.local/browse/NANDROID-123
- #NANDROID-165 - Bet-slip - Update the stake entry field to only allow a maximum of 7 digits (9,999,999.00) to be entered - http://jira.gvcgroup.local/browse/NANDROID-165
- #NANDROID-242 - Cash Out - Cashed Out for value incorrect - http://jira.gvcgroup.local/browse/NANDROID-242
- #NANDROID-241 - MyBets - login - http://jira.gvcgroup.local/browse/NANDROID-241
- #NANDROID-244 - My Bets - Handicap - http://jira.gvcgroup.local/browse/NANDROID-244
- #NANDROID-222 - My Bets - Open/Settled - http://jira.gvcgroup.local/browse/NANDROID-222
- #NANDROID-50 - Betslip - Error Handling - http://jira.gvcgroup.local/browse/NANDROID-50
- #NANDROID-214 - Bet Slip space at bottom when no bets in Bet Slip = Grey - http://jira.gvcgroup.local/browse/NANDROID-214
- #NANDROID-207 - Header Colour #1fa9ee - http://jira.gvcgroup.local/browse/NANDROID-207
- #NANDROID-201 - Up Next Time - colour - http://jira.gvcgroup.local/browse/NANDROID-201
- #NANDROID-200 - By Event Bold, Arrow direction/animation - http://jira.gvcgroup.local/browse/NANDROID-200
- #NANDROID-204 - By Event - Remove animation when expanding - http://jira.gvcgroup.local/browse/NANDROID-204
- #NANDROID-183 - Bet slip icon is to be changed to red (#ff0033) rather than the current green. Plus add drop shadow - http://jira.gvcgroup.local/browse/NANDROID-183
- #NANDROID-144 - Horse racing links from UK to international and vice versa - http://jira.gvcgroup.local/browse/NANDROID-144
- #NANDROID-232 - MyBets - Bet Card Spacing - http://jira.gvcgroup.local/browse/NANDROID-232
- #NANDROID-210 - Create auto-refresh for live betslip - http://jira.gvcgroup.local/browse/NANDROID-210
- #NANDROID-62 - Create auto-refresh for live event pages - http://jira.gvcgroup.local/browse/NANDROID-62
- #NANDROID-123 - Coupon. There is no bar with "1, 2" above Match Prices cell - http://jira.gvcgroup.local/browse/NANDROID-123
- #NANDROID-91 - My Bets - http://jira.gvcgroup.local/browse/NANDROID-91
- #NANDROID-119 - Betslip - E/W Terms in the bet slip - http://jira.gvcgroup.local/browse/NANDROID-119
- #NANDROID-144 - Horse racing links from UK to international and vice versa - http://jira.gvcgroup.local/browse/NANDROID-144
- #NANDROID-140 - Bet Slip icon should always be displayed - http://jira.gvcgroup.local/browse/NANDROID-140
- #NANDROID-165 - Bet-slip - Update the stake entry field to only allow a maximum of 7 digits to be entered - http://jira.gvcgroup.local/browse/NANDROID-165
- #NANDROID-156 - After bet placement, balance should update - http://jira.gvcgroup.local/browse/NANDROID-156
- #NANDROID-119 - Betslip - E/W Terms in the bet slip - http://jira.gvcgroup.local/browse/NANDROID-119
- #NANDROID-134 - Bet receipt - Partially successful bets - http://jira.gvcgroup.local/browse/NANDROID-134
- #NANDROID-130 - SP betting - http://jira.gvcgroup.local/browse/NANDROID-130
- #NANDROID-168 - Total Possible Payout isn't updated when price is changed - http://jira.gvcgroup.local/browse/NANDROID-168
- #NANDROID-147 - Remove splash screens during app launch - http://jira.gvcgroup.local/browse/NANDROID-147
- #NANDROID-145 - Help & settings - http://jira.gvcgroup.local/browse/NANDROID-145
- #NANDROID-132 - Bet receipt - All successful bets - http://jira.gvcgroup.local/browse/NANDROID-132
- #NANDROID-59 - Home page - iOS style - http://jira.gvcgroup.local/browse/NANDROID-59
- #NANDROID-159 - Update beta and prod API end-points - http://jira.gvcgroup.local/browse/NANDROID-159
- #NANDROID-133 - Bet receipt - All failed bets - http://jira.gvcgroup.local/browse/NANDROID-133
- #NANDROID-120 - When user opens coupon, title of event is shown for a few seconds and then changes to the title of coupon - http://jira.gvcgroup.local/browse/NANDROID-120
- #NANDROID-142 - It's hard to scroll screen up, pull to refresh functionality activated - http://jira.gvcgroup.local/browse/NANDROID-142
- #NANDROID-120 - When user opens coupon, title of event is shown for a few seconds and then changes to the title of coupon - http://jira.gvcgroup.local/browse/NANDROID-120
- #NANDROID-137 - There is no "pull to refresh" functionality on screens with 'no betting opportunities available - http://jira.gvcgroup.local/browse/NANDROID-137
- #NANDROID-139 - Incorrect screen is opened after tapping Android Back button on Successful Registration screen - http://jira.gvcgroup.local/browse/NANDROID-139
- #NANDROID-136 - Bet Slip. There is should be a bar with username and balance - http://jira.gvcgroup.local/browse/NANDROID-136
- #NANDROID-118 - Bet Slip. Stake fields shouldn't be cleared after reopening Bet Slip - http://jira.gvcgroup.local/browse/NANDROID-118
- #NANDROID-131 - Login and place bet - http://jira.gvcgroup.local/browse/NANDROID-131
- #NANDROID-138 - Create "No In Play" screen - http://jira.gvcgroup.local/browse/NANDROID-138
- #NANDROID-117 - Event Details (Coupon). Embedded cells are invisible - http://jira.gvcgroup.local/browse/NANDROID-117
- #NANDROID-100 - Bet Slip UI fixes - http://jira.gvcgroup.local/browse/NANDROID-100


Version 0.0.28
-------

 - #NANDROID-102 - Bet Slip icon should disappear when scrolling screen in order to see all texts on screen - http://jira.gvcgroup.local/browse/NANDROID-102
 - #NANDROID-89 - Create "In Play" screen - http://jira.gvcgroup.local/browse/NANDROID-89
 - #NANDROID-131 - Login and place bet - http://jira.gvcgroup.local/browse/NANDROID-131
 - #NANDROID-49 - Betslip - Place bet - http://jira.gvcgroup.local/browse/NANDROID-49
 - #NANDROID-99 - Bet Slip. Suspended and Closed markets - http://jira.gvcgroup.local/browse/NANDROID-99
 - #NANDROID-102 - Bet Slip icon should disappear when scrolling screen in order to see all texts on screen - http://jira.gvcgroup.local/browse/NANDROID-102
 - #NANDROID-118 - Bet Slip. Stake fields shouldn't be cleared after reopening Bet Slip - http://jira.gvcgroup.local/browse/NANDROID-118
 - #NANDROID-127 - Limit of events for 'Up Next' category - http://jira.gvcgroup.local/browse/NANDROID-127
 - #NANDROID-107 - Bet Slip. There is should be red line at the left side of cell when there is error - http://jira.gvcgroup.local/browse/NANDROID-107


Version 0.0.27
-------

 - #NANDROID-108 - When marked is closed there is no market title and type and when market is suspended there is no such error - http://jira.gvcgroup.local/browse/NANDROID-108
 - #NANDROID-129 - Bet Slip. App crashes after opening Single/Multiples tab until list of bets isn't loaded - http://jira.gvcgroup.local/browse/NANDROID-129
 - #NANDROID-110 - Bet Slip. Tab by default - http://jira.gvcgroup.local/browse/NANDROID-109
 - #NANDROID-109 - Crash. Please see crashlytics - http://jira.gvcgroup.local/browse/NANDROID-109


Version 0.0.26
-------

 - #NANDROID-104 - Selection which is added to Bet Slip isn't highlighted in the list of events - http://jira.gvcgroup.local/browse/NANDROID-104
 - #NANDROID-103 - Application crashes after opening Bet Slip (after logging out from system) - http://jira.gvcgroup.local/browse/NANDROID-103
 - #NANDROID-72 - Application crashes after opening future event with scoreboard - http://jira.gvcgroup.local/browse/NANDROID-72


Version 0.0.25
-------

 - #NANDROID-66 - If there is E/W and marketing information of event then it should be displayed under bar of Betting title and at the bottom of coupons list - http://jira.gvcgroup.local/browse/NANDROID-66
 - #NANDROID-122 - Marketing information is cut off  - http://jira.gvcgroup.local/browse/NANDROID-122
 - #NANDROID-73 - Make all links tappable on Login screen - http://jira.gvcgroup.local/browse/NANDROID-73


Version 0.0.24
-------

 - #NANDROID-104 - Selection which is added to Bet Slip isn't highlighted in the list of events - http://jira.gvcgroup.local/browse/NANDROID-104
 - #NANDROID-113 - App crashes after opening Bet Slip when there is suspended market - http://jira.gvcgroup.local/browse/NANDROID-113
 - #NANDROID-112 - Application crashes when user adds selection EVS or SP or E/W to Bet Slip - http://jira.gvcgroup.local/browse/NANDROID-112
 - #NANDROID-72 - Application crashes after opening future event with scoreboard - http://jira.gvcgroup.local/browse/NANDROID-72
 - #NANDROID-73 - Make all links tappable on Login screen - http://jira.gvcgroup.local/browse/NANDROID-73
 - #NANDROID-42 - Create login - http://jira.gvcgroup.local/browse/NANDROID-42
 - #NANDROID-66 - If there is E/W and marketing information of event then it should be displayed under bar of Betting title and at the bottom of coupons list - http://jira.gvcgroup.local/browse/NANDROID-66
 - #NANDROID-116 - Games "By event" when user expands event cell and then goes back it should be still expanded instead of minimised - http://jira.gvcgroup.local/browse/NANDROID-116
 - #NANDROID-68 - Event has finished - http://jira.gvcgroup.local/browse/NANDROID-68


Version 0.0.23
-------

 - #NANDROID-98 - Betslip - Remove Selection from the bet slip - http://jira.gvcgroup.local/browse/NANDROID-98
 - #NANDROID-48 - Betslip - Single and Multiple Tabs - http://jira.gvcgroup.local/browse/NANDROID-48
 - #NANDROID-51 - Betslip - Delete Selections - http://jira.gvcgroup.local/browse/NANDROID-51
 - #NANDROID-72 - Application crashes after opening future event with scoreboard - http://jira.gvcgroup.local/browse/NANDROID-72


Version 0.0.22
-------

 - #NANDROID-47 - Betslip - Add Selection to the bet slip - http://jira.gvcgroup.local/browse/NANDROID-47
 - #NANDROID-98 - Betslip - Remove Selection from the bet slip - http://jira.gvcgroup.local/browse/NANDROID-98
 - #NANDROID-97 - Betslip - View bet slip - http://jira.gvcgroup.local/browse/NANDROID-97


Version 0.0.21
-------

 - #NANDROID-88 - When user is logged in appropriate bar with username and balance should be displayed under tabs on Home page - http://jira.gvcgroup.local/browse/NANDROID-88
 - #NANDROID-94 - Login. Save username switch should be turned on by default - http://jira.gvcgroup.local/browse/NANDROID-94
 - #NANDROID-95 - Market isn't suspended when it should be - http://jira.gvcgroup.local/browse/NANDROID-95


Version 0.0.20
-------

 - #NANDROID-30 - Odds type 'EVS' isn't shown in app - http://jira.gvcgroup.local/browse/NANDROID-30
 - #NANDROID-90 - "SP" odds type isn't displayed in app - http://jira.gvcgroup.local/browse/NANDROID-90
 - #NANDROID-79 - When user opens app from background, user should be still logged in - http://jira.gvcgroup.local/browse/NANDROID-79


Version 0.0.19
-------

 - #NANDROID-79 - When user opens app from background, user should be still logged in - http://jira.gvcgroup.local/browse/NANDROID-79
 - #NANDROID-82 - Embedded cells are invisible - http://jira.gvcgroup.local/browse/NANDROID-82
 - #NANDROID-80 - Football item isn't highlighted in Side Menu - http://jira.gvcgroup.local/browse/NANDROID-80


Version 0.0.18
-------

 - #NANDROID-81 - Side Menu should be closed when user logs out - http://jira.gvcgroup.local/browse/NANDROID-81
 - #NANDROID-76 - On successful login the side menu isn't closed - http://jira.gvcgroup.local/browse/NANDROID-76
 - #NANDROID-75 - Login. Error message isn't fully visible - http://jira.gvcgroup.local/browse/NANDROID-75
 - #NANDROID-74 - Login. Error message about incorrect username/password should be shown a few seconds and then disappears - http://jira.gvcgroup.local/browse/NANDROID-74
 - #NANDROID-78 - Register screen. There is no loader while screen is loading - http://jira.gvcgroup.local/browse/NANDROID-78
 - #NANDROID-70 - Event page. Application crashes after tapping cell 2 times - http://jira.gvcgroup.local/browse/NANDROID-70


Version 0.0.17
-------

 - #NANDROID-67 - App crashes after opening screen with coupons - http://jira.gvcgroup.local/browse/NANDROID-67
 - #NANDROID-63 - Golf "By Event". If there is only 1 embedding then next screen with coupons should be opened - http://jira.gvcgroup.local/browse/NANDROID-63
 - #NANDROID-71 - When there is no time of game, scoreboard shouldn't be displayed - http://jira.gvcgroup.local/browse/NANDROID-71


Version 0.0.16
-------

 - #NANDROID-56 - In game Scoreboard - http://jira.gvcgroup.local/browse/NANDROID-56
 - #NANDROID-61 - Add mechanism to getting sports configuration from server  - http://jira.gvcgroup.local/browse/NANDROID-61
 - #NANDROID-42 - Create login - http://jira.gvcgroup.local/browse/NANDROID-42
 - #NANDROID-43 - Create Logout - http://jira.gvcgroup.local/browse/NANDROID-43
 - #NANDROID-44 - Registration - http://jira.gvcgroup.local/browse/NANDROID-44
 - #NANDROID-45 - Create Deposit button - http://jira.gvcgroup.local/browse/NANDROID-45
 - #NANDROID-46 - Create My Account button - http://jira.gvcgroup.local/browse/NANDROID-46


Version 0.0.15
-------

 - #NANDROID-41 - Create Event page - http://jira.gvcgroup.local/browse/NANDROID-41
 - #NANDROID-60 - Fixing scrollbar fade duration. Scrollbar should disappear immediately after scrolling.  - http://jira.gvcgroup.local/browse/NANDROID-60
 - #NANDROID-33 - If odds are too long, system should decrease font - http://jira.gvcgroup.local/browse/NANDROID-33


Version 0.0.14
-------

 - #NANDROID-37 - When there is nothing in "In Play" category appropriate message should be displayed - http://jira.gvcgroup.local/browse/NANDROID-37
 - #NANDROID-38 - Application crashes when user navigates between Highlights/In Play/Up next/By event screens - http://jira.gvcgroup.local/browse/NANDROID-38
 - #NANDROID-39 - By event screen. First cell is thinner than others - http://jira.gvcgroup.local/browse/NANDROID-39
 - #NANDROID-40 - Expand/Minimise icon is incorrect - http://jira.gvcgroup.local/browse/NANDROID-40


Version 0.0.13
-------


Version 0.0.12
-------

 - #NANDROID-28 - Three ball coupon - http://jira.gvcgroup.local/browse/NANDROID-28
 - #NANDROID-19 - Default Coupon Page header per Sport - http://jira.gvcgroup.local/browse/NANDROID-19


Version 0.0.11
-------

 - #NANDROID-29 - Simple coupon - http://jira.gvcgroup.local/browse/NANDROID-29
 - #NANDROID-26 - User can see Sport highlights when navigating to Sport from blue box at Side Menu - http://jira.gvcgroup.local/browse/NANDROID-26
 - #NANDROID-34 - Coupon with empty item is shown instead of "Bet now" - http://jira.gvcgroup.local/browse/NANDROID-34
 - #NANDROID-23 - Implement way of handling expiredToken - http://jira.gvcgroup.local/browse/NANDROID-23
 - #NANDROID-35 - Small configuration changes to implement before release - http://jira.gvcgroup.local/browse/NANDROID-35


Version 0.0.10
-------

 - #NANDROID-15 - Last dynamic Item in Header Sports Navigation - http://jira.gvcgroup.local/browse/NANDROID-15
 - #NANDROID-22 - Application crashes when relaunching it after longer period of time. - http://jira.gvcgroup.local/browse/NANDROID-22
 - #NANDROID-31 - "In Play" should be written with space in Side Menu and in Main Menu - http://jira.gvcgroup.local/browse/NANDROID-31


Version 0.0.9
-------

 - #NANDROID-26 - User can see Sport highlights when navigating to any Sport tab. - http://jira.gvcgroup.local/browse/NANDROID-26


Version 0.0.8
-------

 - #NANDROID-25 - Coupon - Over/Under - http://jira.gvcgroup.local/browse/NANDROID-25
 - #NANDROID-14 - UK domain should have the specific items in Header Sports Navigation. - http://jira.gvcgroup.local/browse/NANDROID-14


Version 0.0.7
-------

 - #NANDROID-20 - Coupon - TeamTie + PlayTie - http://jira.gvcgroup.local/browse/NANDROID-20
 - #NANDROID-24 - Coupon - TeamNoTie + PlayNoTie - http://jira.gvcgroup.local/browse/NANDROID-24


Version 0.0.6
-------

 - #NANDROID-20 - Coupon - TeamTie + PlayTie - http://jira.gvcgroup.local/browse/NANDROID-20


Version 0.0.5
-------

 - #NANDROID-9 - Header Sports Navigation - http://jira.gvcgroup.local/browse/NANDROID-9


Version 0.0.4
-------

 - #NANDROID-11 #BUG - Action bar is cut off at the top when user opens application - http://jira.gvcgroup.local/browse/NANDROID-11
 - #NANDROID-16 - Content Screen and Side Menu should update each other about selected sport. - http://jira.gvcgroup.local/browse/NANDROID-16
 - #NANDROID-17 - Adding Monkey-Talk build. - http://jira.gvcgroup.local/browse/NANDROID-17


Version 0.0.3
-------

 - #NANDROID-11 #BUG - Action bar is cut off at the top when user opens application
 - #NANDROID-12 - Side Menu - Sports List - http://jira.gvcgroup.local/browse/NANDROID-6
 - #NANDROID-13 - Side Menu - 6 pack - http://jira.gvcgroup.local/browse/NANDROID-7


Version 0.0.2
-------

 - #NANDROID-2 - Create side menu container and animation - http://jira.gvcgroup.local/browse/NANDROID-2
 - #NANDROID-6 - Side Menu - Sports List - http://jira.gvcgroup.local/browse/NANDROID-6
 - #NANDROID-7 - Side Menu - 6 pack - http://jira.gvcgroup.local/browse/NANDROID-7


Version 0.0.1
-------

 - #NANDROID-3 - Create Homepage container - http://jira.gvcgroup.local/browse/NANDROID-3
 - #NANDROID-1 - Create Splash screens - http://jira.gvcgroup.local/browse/NANDROID-1
 - #NANDROID-8 - Setting up build system. - http://jira.gvcgroup.local/browse/NANDROID-8
