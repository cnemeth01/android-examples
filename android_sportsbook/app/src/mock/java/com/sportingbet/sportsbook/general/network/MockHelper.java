package com.sportingbet.sportsbook.general.network;

import android.content.Context;
import android.os.Handler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MockHelper {

    private final Context context;
    private final ObjectMapper objectMapper;

    public MockHelper(Context context, ObjectMapper objectMapper) {
        this.context = context;
        this.objectMapper = objectMapper;
    }

    public <T> T getObjectFromRawResource(int rawResource, Class<T> type) {
        try {
            return objectMapper.readValue(getRawResourceInputStream(rawResource), type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T getRandomObjectFromRawResource(int[] rawResources, Class<T> type) {
        try {
            return objectMapper.readValue(getRawResourceInputStream(rawResources[new Random().nextInt(rawResources.length)]), type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T getObjectFromRawResource(int rawResource, TypeReference valueTypeRef) {
        try {
            return objectMapper.readValue(getRawResourceInputStream(rawResource), valueTypeRef);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void runWithDelay(Runnable runnable) {
        runWithDelay(runnable, 100);
    }

    public void runWithDelay(Runnable runnable, int delay) {
        new Handler().postDelayed(runnable, delay);
    }

    private InputStream getRawResourceInputStream(int rawResource) {
        return context.getResources().openRawResource(rawResource);
    }

    public RetrofitError getSessionExpiredError() {
        Response response = new Response("url", 403, "SessionExpired", Lists.<Header>newArrayList(), null);
        return RetrofitError.httpError("url", response, null, null);
    }

    public RetrofitError getNoContentError() {
        Response response = new Response("url", 204, "No Content", Lists.<Header>newArrayList(), null);
        return RetrofitError.httpError("url", response, null, null);
    }

    public RetrofitError getErrorWithUnexpectedException(Throwable throwable) {
        return RetrofitError.unexpectedError("url", throwable);
    }
}
