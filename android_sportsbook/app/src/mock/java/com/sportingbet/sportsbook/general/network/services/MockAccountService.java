package com.sportingbet.sportsbook.general.network.services;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.network.MockHelper;
import com.sportingbet.sportsbook.home.account.model.BalanceDetailsWrapper;
import com.sportingbet.sportsbook.home.account.model.LoginRequestBody;
import com.sportingbet.sportsbook.home.account.model.LoginResponse;
import com.sportingbet.sportsbook.home.account.model.LogoutRequestBody;
import com.sportingbet.sportsbook.mybets.model.MyBetType;
import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MockAccountService implements AccountService {

    private MockHelper mockHelper;

    public MockAccountService(MockHelper mockHelper) {
        this.mockHelper = mockHelper;
    }

    @Override
    public void login(LoginRequestBody loginRequestBody, final Callback<LoginResponse> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(mockHelper.getObjectFromRawResource(R.raw.login, LoginResponse.class), null);
            }
        });
    }

    @Override
    public void logout(@Body LogoutRequestBody logoutRequestBody, final Callback<Response> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(null, null);
            }
        });
    }

    @Override
    public void getBalance(final Callback<BalanceDetailsWrapper> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(mockHelper.getObjectFromRawResource(R.raw.balance, BalanceDetailsWrapper.class), null);
            }
        });
    }

    @Override
    public void getMyBets(final MyBetType betType, final Callback<MyBetsResponse> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(mockHelper.getObjectFromRawResource(getResource(), MyBetsResponse.class), null);
            }

            private int getResource() {
                switch (betType) {
                    case open:
                        return R.raw.mybets_empty;
                    default:
                        return R.raw.mybets_settled;
                }
            }
        });
    }
}
