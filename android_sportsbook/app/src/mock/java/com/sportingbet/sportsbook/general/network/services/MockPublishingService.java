package com.sportingbet.sportsbook.general.network.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.network.MockHelper;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.event.DetailsWrapper;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.model.event.EventsWrapperImpl;
import com.sportingbet.sportsbook.model.events.EventClassWrapper;
import com.sportingbet.sportsbook.model.events.ParentEventClassWrapper;

import java.util.List;
import java.util.Random;

import retrofit.Callback;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MockPublishingService implements PublishingService {

    private MockHelper mockHelper;

    public MockPublishingService(MockHelper mockHelper) {
        this.mockHelper = mockHelper;
    }

    @Override
    public void getSports(final Callback<List<Sport>> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                TypeReference<List<Sport>> typeReference = new TypeReference<List<Sport>>() {
                };
                callback.success(mockHelper.<List<Sport>>getObjectFromRawResource(R.raw.sports, typeReference), null);
            }
        });
    }

    @Override
    public void getHighlights(final String sportIds, int noOfEvents, final Callback<DetailsWrapper> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(getEventWrapper(getHighlightsIdentifier(Long.parseLong(sportIds))), null);
            }

            private int getHighlightsIdentifier(long sportId) {
                switch ((int) sportId) {
                    case 8:
                        return R.raw.highlights_8;
                    case 190:
                        return R.raw.highlights_190;
                    case 370:
                        return R.raw.highlights_370;
                    default:
                        return R.raw.highlights;
                }
            }
        });
    }

    @Override
    public void getInPlay(final long sportId, final Callback<DetailsWrapper> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(getEventWrapper(getInplayIdentifier(sportId)), null);
            }

            private int getInplayIdentifier(long sportId) {
                switch ((int) sportId) {
                    case 4:
                        return R.raw.inplay_4;
                    case 8:
                        return R.raw.inplay_8;
                    case 190:
                        return R.raw.inplay_190;
                    default:
                        return R.raw.inplay;
                }
            }
        });
    }

    @Override
    public void getUpNext(final long sportId, int noOfEvents, final Callback<DetailsWrapper> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(getEventWrapper(getUpNextIdentifier(sportId)), null);
            }

            private int getUpNextIdentifier(long sportId) {
                switch ((int) sportId) {
                    case 8:
                        return R.raw.upnext_8;
                    default:
                        return R.raw.upnext;
                }
            }
        });
    }

    @Override
    public void getParentEventClasses(final long sportId, final Callback<List<ParentEventClassWrapper>> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                TypeReference<List<ParentEventClassWrapper>> typeReference = new TypeReference<List<ParentEventClassWrapper>>() {
                };
                callback.success(mockHelper.<List<ParentEventClassWrapper>>getObjectFromRawResource(getByEventIdentifier(sportId), typeReference), null);
            }

            private int getByEventIdentifier(long sportId) {
                switch ((int) sportId) {
                    case 8:
                        return R.raw.byevent_8;
                    case 171:
                        return R.raw.byevent_171;
                    default:
                        return R.raw.byevent;
                }
            }
        });
    }

    @Override
    public void getEventClasses(final long parentEventClassId, final Callback<EventClassWrapper> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(mockHelper.getObjectFromRawResource(getEventClassesIdentifier(), EventClassWrapper.class), null);
            }

            protected int getEventClassesIdentifier() {
                switch ((int) parentEventClassId) {
                    case 1057436:
                        return R.raw.event_classes_171;
                    default:
                        return R.raw.event_classes;
                }
            }
        });
    }

    @Override
    public void getEvents(final long eventClass, final Callback<EventsWrapperImpl> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(mockHelper.getObjectFromRawResource(getEventsIdentifier(), EventsWrapperImpl.class), null);
            }

            protected int getEventsIdentifier() {
                if (eventClass > 1057436 && eventClass < 1057445) {
                    return R.raw.events_171;
                }
                return R.raw.events;
            }
        });
    }

    @Override
    public void getEvent(final long eventId, final Callback<Event> callback) {
        mockHelper.runWithDelay(new Runnable() {

            private int[] events = {R.raw.event, R.raw.event_4, R.raw.event_4_1, R.raw.event_8, R.raw.event_8_no_scores, R.raw.event_8_no_name, R.raw.event_102, R.raw.event_171};

            @Override
            public void run() {
                switch (new Random().nextInt(8)) {
                    case 0:
                        callback.failure(mockHelper.getNoContentError());
                        break;
                    default:
                        callback.success(mockHelper.getObjectFromRawResource(getEventIdentifier(), Event.class), null);
                        break;
                }
            }

            private int getEventIdentifier() {
                switch ((int) eventId) {
                    case 4503180:
                        return R.raw.event_171_4503180;
                    case 4528324:
                        return R.raw.event_370;
                    default:
                        return events[new Random().nextInt(events.length)];
                }
            }
        });
    }

    @Override
    public void getEventWithSpecificBook(long eventId, String openSpecifiedBooks, Callback<Event> callback) {
        getEvent(eventId, callback);
    }

    private DetailsWrapper getEventWrapper(int rawResource) {
        return mockHelper.getObjectFromRawResource(rawResource, DetailsWrapper.class);
    }
}
