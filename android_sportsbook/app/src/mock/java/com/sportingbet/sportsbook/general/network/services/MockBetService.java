package com.sportingbet.sportsbook.general.network.services;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.betslip.placebets.model.BetRequest;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetRequest;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetResponse;
import com.sportingbet.sportsbook.coupons.betslip.model.BetslipResponse;
import com.sportingbet.sportsbook.coupons.betslip.model.BetslipSelectionGroup;
import com.sportingbet.sportsbook.general.network.MockHelper;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetRequestType;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetsRequest;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetsResponseType;

import java.util.Random;

import retrofit.Callback;
import retrofit.http.Body;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MockBetService implements BetService {

    private final MockHelper mockHelper;

    public MockBetService(MockHelper mockHelper) {
        this.mockHelper = mockHelper;
    }

    @Override
    public void addBet(BetslipSelectionGroup betslipSelectionGroup, final Callback<BetslipResponse> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(new BetslipResponse(1), null);
            }
        });
    }

    @Override
    public void removeBet(String selectionIds, Boolean deleteAll, final Callback<BetslipResponse> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(new BetslipResponse(0), null);
            }
        });
    }

    @Override
    public void getBets(final Callback<BetslipInformation> callback) {
        mockHelper.runWithDelay(new Runnable() {

            private int[] betslips = {R.raw.betslip, R.raw.betslip_multiples, R.raw.betslip_changes,
                    R.raw.betslip_empty, R.raw.betslip_susp, R.raw.betslip_for_response}; //TODO add sp handling: 'R.raw.betslip_sp'

            @Override
            public void run() {
                callback.success(mockHelper.getObjectFromRawResource(getEventIdentifier(), BetslipInformation.class), null);
            }

            private int getEventIdentifier() {
                return betslips[new Random().nextInt(betslips.length)];
            }
        });
    }

    @Override
    public void placeBet(@Body final PlaceBetRequest betslipSelectionGroup, final Callback<PlaceBetResponse> callback) {
        mockHelper.runWithDelay(new Runnable() {

            private int[] betResponses = {R.raw.bet_response_success, R.raw.bet_response_success_multiple,
                    R.raw.bet_response_success_each_way, R.raw.bet_response_mixed};

            @Override
            public void run() {
                callback.success(mockHelper.getObjectFromRawResource(getEventIdentifier(), PlaceBetResponse.class), null);
            }

            private int getEventIdentifier() {
                if (hasBetId("SINGLE-374144668", "SINGLE-373636160")) {
                    return R.raw.bet_response_error;
                }
                return betResponses[new Random().nextInt(betResponses.length)];
            }

            private boolean hasBetId(String... betIds) {
                BetRequest betRequest = betslipSelectionGroup.getBetRequests().get(0);
                for (String betId : betIds) {
                    if (betId.equals(betRequest.getBetId())) {
                        return true;
                    }
                }
                return false;
            }
        }, 1000);
    }

    @Override
    public void cashOut(@Body final CashoutBetRequestType cashoutBetRequestType, final Callback<CashoutBetsResponseType> callback) {
        mockHelper.runWithDelay(new Runnable() {

            @Override
            public void run() {
                callback.success(mockHelper.getObjectFromRawResource(getReferenceIdentifier(), CashoutBetsResponseType.class), null);
            }

            private int getReferenceIdentifier() {
                if (hasRefId("65201139")) {
                    return R.raw.cashout_success;
                }
                return R.raw.cashout_error;
            }

            private boolean hasRefId(String... cashoutRefIds) {
                CashoutBetsRequest cashoutBetsRequest = cashoutBetRequestType.getCashoutBetRequests().get(0);
                for (String refID : cashoutRefIds) {
                    if (refID.equals(cashoutBetsRequest.getBetReferenceId())) {
                        return true;
                    }
                }
                return false;
            }
        }, 1000);
    }
}
