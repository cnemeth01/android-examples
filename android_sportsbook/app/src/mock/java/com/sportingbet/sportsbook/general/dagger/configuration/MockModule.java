package com.sportingbet.sportsbook.general.dagger.configuration;

import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sportingbet.sportsbook.general.network.MockHelper;
import com.sportingbet.sportsbook.general.network.services.AccountService;
import com.sportingbet.sportsbook.general.network.services.BetService;
import com.sportingbet.sportsbook.general.network.services.ConfigService;
import com.sportingbet.sportsbook.general.network.services.MockAccountService;
import com.sportingbet.sportsbook.general.network.services.MockBetService;
import com.sportingbet.sportsbook.general.network.services.MockConfigService;
import com.sportingbet.sportsbook.general.network.services.MockPublishingService;
import com.sportingbet.sportsbook.general.network.services.MockSessionService;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.general.network.services.SessionService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        includes = {
                ApplicationModule.class,
        },
        library = true,
        overrides = true
)
public final class MockModule {

    @Provides
    @Singleton
    MockHelper provideMockUtils(Context applicationContext, ObjectMapper objectMapper) {
        return new MockHelper(applicationContext, objectMapper);
    }

    @Provides
    @Singleton
    SessionService provideSessionService(MockHelper mockHelper) {
        return new MockSessionService(mockHelper);
    }

    @Provides
    @Singleton
    PublishingService providePublishingService(MockHelper mockHelper) {
        return new MockPublishingService(mockHelper);
    }

    @Provides
    @Singleton
    ConfigService provideConfigService(MockHelper mockHelper) {
        return new MockConfigService(mockHelper);
    }

    @Provides
    @Singleton
    AccountService provideAccountService(MockHelper mockHelper) {
        return new MockAccountService(mockHelper);
    }

    @Provides
    @Singleton
    BetService provideBetService(MockHelper mockHelper) {
        return new MockBetService(mockHelper);
    }
}
