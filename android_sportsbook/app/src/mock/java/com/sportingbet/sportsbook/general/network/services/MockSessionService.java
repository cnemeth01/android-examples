package com.sportingbet.sportsbook.general.network.services;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.general.network.MockHelper;

import retrofit.Callback;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MockSessionService implements SessionService {

    private final MockHelper mockHelper;

    public MockSessionService(MockHelper mockHelper) {
        this.mockHelper = mockHelper;
    }

    @Override
    public void getActiveSessionId(String domainId, String appId, final Callback<Response> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(null, null);
            }
        });
    }

    @Override
    public void getTempSessionId(final Callback<Response> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                callback.success(new Response("getTempSessionId", 200, "SessionExpired", Lists.newArrayList(new Header("TSID", "tempSessionId")), null), null);
            }
        });
    }
}
