package com.sportingbet.sportsbook.general.network.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.network.MockHelper;
import com.sportingbet.sportsbook.sports.configuration.SportConfig;

import java.util.List;

import retrofit.Callback;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MockConfigService implements ConfigService {

    private final MockHelper mockHelper;

    public MockConfigService(MockHelper mockHelper) {
        this.mockHelper = mockHelper;
    }

    @Override
    public void getSportConfigs(final Callback<List<SportConfig>> callback) {
        mockHelper.runWithDelay(new Runnable() {
            @Override
            public void run() {
                TypeReference<List<SportConfig>> typeReference = new TypeReference<List<SportConfig>>() {
                };
                callback.success(mockHelper.<List<SportConfig>>getObjectFromRawResource(R.raw.sports_config, typeReference), null);
            }
        });
    }
}
