package com.sportingbet.sportsbook;

import android.app.Application;

import com.sportingbet.sportsbook.general.dagger.configuration.ApplicationModule;
import com.sportingbet.sportsbook.general.dagger.configuration.NetworkModule;

public final class Modules {

    public static Object[] create(Application application) {
        return new Object[]{
                new ApplicationModule(application),
                new NetworkModule()
        };
    }
}
