package com.sportingbet.sportsbook.home.pager;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class PagerControllerTest {

    private ViewPager viewPager;

    private PagerAdapter pagerAdapter;

    private PagerController.PagerControllerDelegate delegate;

    PagerController pagerController;

    @Before
    public void setUp() throws Exception {
        pagerAdapter = Mockito.mock(PagerAdapter.class);
        pagerController = new PagerController(pagerAdapter);

        delegate = Mockito.mock(PagerController.PagerControllerDelegate.class);
        viewPager = Mockito.mock(ViewPager.class);

        registerActivity();
    }

    private void registerActivity() throws Exception {
        FragmentActivity fragmentActivity = Mockito.mock(FragmentActivity.class);
        Mockito.when(fragmentActivity.findViewById(R.id.view_pager)).thenReturn(viewPager);
        Mockito.when(fragmentActivity.findViewById(R.id.view_pager_indicator)).thenReturn(Mockito.mock(PagerSlidingTabStrip.class));

        pagerController.registerActivity(fragmentActivity);
        pagerController.registerDelegate(delegate);
    }

    @Test
    public void onPageScrollStateChangedTest() throws Exception {
        // given
        AbstractNavigationFragment navigationFragment = Mockito.mock(AbstractNavigationFragment.class);
        Mockito.when(pagerAdapter.getItem(1)).thenReturn(navigationFragment);
        Mockito.when(pagerAdapter.getItemIdentifier(1)).thenReturn(120L);
        Mockito.when(viewPager.getCurrentItem()).thenReturn(1);

        // when
        pagerController.onPageScrollStateChanged(ViewPager.SCROLL_STATE_IDLE);

        // then
        Mockito.verify(delegate).didSelectPageWithId(Mockito.eq(120L));
        Mockito.verify(navigationFragment).onPageSelected();
    }

    @Test
    public void onPageScrollStateSecondTime() throws Exception {
        // given
        AbstractNavigationFragment navigationFragment1 = Mockito.mock(AbstractNavigationFragment.class);
        AbstractNavigationFragment navigationFragment2 = Mockito.mock(AbstractNavigationFragment.class);
        Mockito.when(pagerAdapter.getItem(0)).thenReturn(navigationFragment1);
        Mockito.when(pagerAdapter.getItem(2)).thenReturn(navigationFragment2);
        Mockito.when(viewPager.getCurrentItem()).thenReturn(0, 0, 2, 2);     // fragment 1 is currently selected

        // when
        pagerController.onPageScrollStateChanged(ViewPager.SCROLL_STATE_SETTLING);
        pagerController.onPageScrollStateChanged(ViewPager.SCROLL_STATE_IDLE);
        pagerController.onPageScrollStateChanged(ViewPager.SCROLL_STATE_SETTLING);
        pagerController.onPageScrollStateChanged(ViewPager.SCROLL_STATE_IDLE);

        // then
        Mockito.verify(navigationFragment2).onPageSelected();
        Mockito.verify(navigationFragment1, Mockito.never()).onPageSelected();
        Mockito.verify(navigationFragment1).onPageDisabled();
    }

    @Test
    public void selectPageWithId() throws Exception {
        // given
        Mockito.when(pagerAdapter.getPositionFromId(14)).thenReturn(2);

        // when
        pagerController.selectPageWithId(14);

        // then
        Mockito.verify(viewPager).setCurrentItem(2, true);
    }

    @Test
    public void selectPageWithIdDoNotCallDelegate() throws Exception {
        // given
        Mockito.when(pagerAdapter.getItem(1)).thenReturn(Mockito.mock(AbstractNavigationFragment.class));
        Mockito.when(pagerAdapter.getPositionFromId(104)).thenReturn(1);

        // when
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                pagerController.onPageSelected(1);
                pagerController.onPageScrollStateChanged(ViewPager.SCROLL_STATE_IDLE);
                return null;
            }
        }).when(viewPager).setCurrentItem(1, true);
        pagerController.selectPageWithId(104);

        // then
        Mockito.verify(delegate, Mockito.never()).didSelectPageWithId(Mockito.eq(1));
    }
}
