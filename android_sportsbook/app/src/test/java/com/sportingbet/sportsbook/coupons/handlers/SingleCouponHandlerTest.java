package com.sportingbet.sportsbook.coupons.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.assertions.CouponLayoutAssert;
import com.sportingbet.sportsbook.coupons.CouponHandler;
import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.coupons.model.SingleCoupon;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;
import com.sportingbet.sportsbook.mock.MockSelection;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

import org.fest.assertions.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.ANDROID.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class SingleCouponHandlerTest {

    @Inject
    Context context;

    protected CouponHandler couponHandler;

    @Module(
            injects = {
                    SingleCouponHandlerTest.class,
            },
            includes = GeneralTestModule.class
    )
    class TestModule {
    }

    @Before
    public void setUp() throws Exception {
        couponHandler = new SingleCouponHandler(Mockito.mock(BetslipCouponDelegate.class));
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);
    }

    protected RecyclerView.ViewHolder createViewHolder() {
        return couponHandler.onCreateViewHolder(context, null);
    }

    @Test
    public void objectBuilt() {
        Assertions.assertThat(couponHandler).isNotNull();
    }

    @Test
    public void onCreateViewHolderCreated() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        Assertions.assertThat(viewHolder).isNotNull();
    }

    @SuppressWarnings("unchecked")
    public <T extends TextView> T getView(RecyclerView.ViewHolder viewHolder, int resourceId) {
        return (T) viewHolder.itemView.findViewById(resourceId);
    }

    public CouponLayout getCouponLayout(RecyclerView.ViewHolder viewHolder, int first_team) {
        return (CouponLayout) viewHolder.itemView.findViewById(first_team);
    }

    @Test
    public void onCreateViewHolderHasTextViews() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View overText = viewHolder.itemView.findViewById(R.id.title);
        assertThat(overText).isNotNull().isInstanceOf(TextView.class);

        View underText = viewHolder.itemView.findViewById(R.id.button);
        assertThat(underText).isNotNull().isInstanceOf(CouponLayout.class);
    }

    private Event createEvent(boolean visible) {
        Event event = Mockito.mock(Event.class);
        Mockito.when(event.isVisible()).thenReturn(visible);
        return event;
    }

    private Book createBook(boolean visible) {
        Book book = Mockito.mock(Book.class);
        Mockito.when(book.isVisible()).thenReturn(visible);
        return book;
    }

    @Test
    public void onBindViewHolderWithOneSelection() {
        // given
        Coupon coupon = new SingleCoupon(createEvent(true), createBook(true), Lists.<Selection>newArrayList(new MockSelection("Liverpool v Arsenal", "1/2")));
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // when
        couponHandler.onBindViewHolder(viewHolder, coupon, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Liverpool v Arsenal");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.button))
                .isEnabled()
                .hasText("1/2")
                .isVisible();
    }

    @Test
    public void onBindViewHolderWithOneSelection2() {
        // given
        Coupon coupon = new SingleCoupon(createEvent(true), createBook(true), Lists.<Selection>newArrayList(new MockSelection("Draw", "2/3")));
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // when
        couponHandler.onBindViewHolder(viewHolder, coupon, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Draw");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.button))
                .isEnabled()
                .hasText("2/3")
                .isVisible();
    }

    @Test
    public void onBindViewHolderWithOneSelectionNotOffered() {
        // given
        Coupon coupon = new SingleCoupon(createEvent(true), createBook(true), Lists.<Selection>newArrayList(new MockSelection("Draw", "2/3", false)));
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // when
        couponHandler.onBindViewHolder(viewHolder, coupon, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Draw");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.button))
                .isDisabled()
                .hasText("NO")
                .isVisible();
    }

    private LinearLayout getButtonContainer(RecyclerView.ViewHolder viewHolder) {
        return (LinearLayout) viewHolder.itemView.findViewById(R.id.button_container);
    }

    @Test
    public void onBindViewHolderWithTwoSelections() {
        // given
        Coupon coupon = new SingleCoupon(createEvent(true), createBook(true), Lists.<Selection>newArrayList(new MockSelection("Liverpool", "1/2"), new MockSelection("Liverpool", "2/5")));
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // when
        couponHandler.onBindViewHolder(viewHolder, coupon, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Liverpool");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.button))
                .isEnabled()
                .hasText("1/2")
                .isVisible();

        LinearLayout buttonContainer = getButtonContainer(viewHolder);
        assertThat(buttonContainer).hasChildCount(1);
        CouponLayoutAssert.assertThat((CouponLayout) buttonContainer.getChildAt(0))
                .isEnabled()
                .hasText("2/5")
                .isVisible();
    }

    @Test
    public void onBindViewHolderWithThreeSelections() {
        // given
        Coupon coupon = new SingleCoupon(createEvent(true), createBook(true), Lists.<Selection>newArrayList(new MockSelection("Liverpool", "1/2"), new MockSelection("Liverpool", "2/5"), new MockSelection("Liverpool", "1/30")));
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // when
        couponHandler.onBindViewHolder(viewHolder, coupon, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Liverpool");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.button))
                .isEnabled()
                .hasText("1/2")
                .isVisible();

        LinearLayout buttonContainer = getButtonContainer(viewHolder);
        assertThat(buttonContainer).hasChildCount(2);
        CouponLayoutAssert.assertThat((CouponLayout) buttonContainer.getChildAt(0))
                .isEnabled()
                .hasText("2/5")
                .isVisible();
        CouponLayoutAssert.assertThat((CouponLayout) buttonContainer.getChildAt(1))
                .isEnabled()
                .hasText("1/30")
                .isVisible();
    }

    @Test
    public void onBindViewHolderWithThreeSelectionsDisabled() {
        // given
        Coupon coupon = new SingleCoupon(createEvent(true), createBook(false), Lists.<Selection>newArrayList(new MockSelection("Liverpool", "1/2"), new MockSelection("Liverpool", "2/5"), new MockSelection("Liverpool", "1/30")));
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // when
        couponHandler.onBindViewHolder(viewHolder, coupon, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Liverpool");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.button))
                .isDisabled()
                .hasText("1/2")
                .isVisible();

        LinearLayout buttonContainer = getButtonContainer(viewHolder);
        assertThat(buttonContainer).hasChildCount(2);
        CouponLayoutAssert.assertThat((CouponLayout) buttonContainer.getChildAt(0))
                .isDisabled()
                .hasText("2/5")
                .isVisible();
        CouponLayoutAssert.assertThat((CouponLayout) buttonContainer.getChildAt(1))
                .isDisabled()
                .hasText("1/30")
                .isVisible();
    }

    @Test
    public void onBindViewHolderNormal() {
        // given
        Coupon coupon = new SingleCoupon(createEvent(true), createBook(true), Lists.<Selection>newArrayList(new MockSelection("Draw", "2/3", true)));
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // when
        couponHandler.onBindViewHolder(viewHolder, coupon, null);

        // then
        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.button))
                .isEnabled()
                .hasText("2/3")
                .isVisible();
    }

    @Test
    public void onBindViewHolderWithNotVisibleBook() {
        // given
        Coupon coupon = new SingleCoupon(createEvent(true), createBook(false), Lists.<Selection>newArrayList(new MockSelection("Draw", "2/3", true)));
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // when
        couponHandler.onBindViewHolder(viewHolder, coupon, null);

        // then
        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.button))
                .isDisabled()
                .hasText("2/3")
                .isVisible();
    }

    @Test
    public void onBindViewHolderWithNotVisibleEvent() {
        // given
        Coupon coupon = new SingleCoupon(createEvent(false), createBook(true), Lists.<Selection>newArrayList(new MockSelection("Draw", "2/3", true)));
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // when
        couponHandler.onBindViewHolder(viewHolder, coupon, null);

        // then
        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.button))
                .isDisabled()
                .hasText("2/3")
                .isVisible();
    }

    @Test
    public void onBindViewHolderWithNotVisibleBookAndEvent() {
        // given
        Coupon coupon = new SingleCoupon(createEvent(false), createBook(false), Lists.<Selection>newArrayList(new MockSelection("Draw", "2/3", true)));
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // when
        couponHandler.onBindViewHolder(viewHolder, coupon, null);

        // then
        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.button))
                .isDisabled()
                .hasText("2/3")
                .isVisible();
    }
}