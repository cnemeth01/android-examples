package com.sportingbet.sportsbook.scoreboard;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.ResourceProvider;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;
import com.sportingbet.sportsbook.model.event.Event;

import org.fest.assertions.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.ANDROID.assertThat;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class ScoreboardFactoryTest {

    @Inject
    Context context;

    @Inject
    ResourceProvider resourceProvider;

    @Inject
    ScoreboardFactory scoreboardFactory;

    @Module(
            injects = ScoreboardFactoryTest.class,
            includes = {
                    GeneralTestModule.class,
            },
            overrides = true
    )
    class TestModule {
    }

    @Before
    public void setUp() throws Exception {
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);
    }

    @Test
    public void objectBuilt() {
        Assertions.assertThat(scoreboardFactory).isNotNull();
    }

    @Test
    public void runningFootballEvent() {
        // given
        LinearLayout linearLayout = new LinearLayout(context);
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_1.json");

        // when
        scoreboardFactory.prepareScoreboardForEvent(linearLayout, event);

        // then
        assertThat(linearLayout).hasChildCount(1);
    }

    @Test
    public void notRunningFootballEvent() {
        // given
        LinearLayout linearLayout = new LinearLayout(context);
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_not_running.json");

        // when
        scoreboardFactory.prepareScoreboardForEvent(linearLayout, event);

        // then
        assertThat(linearLayout).hasChildCount(1);
    }

    @Test
    public void notRunningEmptyFootballEvent() {
        // given
        LinearLayout linearLayout = new LinearLayout(context);
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_world_cup.json");

        // when
        scoreboardFactory.prepareScoreboardForEvent(linearLayout, event);

        // then
        assertThat(linearLayout).hasChildCount(0);
    }

    @Test
    public void noScoresTennisEvent() {
        // given
        LinearLayout linearLayout = new LinearLayout(context);
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tennis_no_scores.json");

        // when
        scoreboardFactory.prepareScoreboardForEvent(linearLayout, event);

        // then
        assertThat(linearLayout).hasChildCount(1);
        assertThat((ViewGroup) linearLayout.findViewById(R.id.period_text).getParent()).hasChildCount(1);
    }
}
