package com.sportingbet.sportsbook.home.navigation.configuration;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.general.ResourceProvider;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;
import com.sportingbet.sportsbook.home.navigation.NavigationItem;
import com.sportingbet.sportsbook.home.navigation.NavigationItemFactory;
import com.sportingbet.sportsbook.home.navigation.fragments.HomeNavigationFragment;
import com.sportingbet.sportsbook.home.navigation.fragments.InPlayNavigationFragment;
import com.sportingbet.sportsbook.home.navigation.fragments.SportNavigationFragment;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.sports.SportsStore;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class NavigationConfigurationTest {

    @Module(
            injects = NavigationConfigurationTest.class,
            includes = {
                    GeneralTestModule.class,
            },
            overrides = true
    )
    class TestModule {
    }

    @Inject
    ResourceProvider resourceProvider;

    @Inject
    SportsStore sportsStore;

    @Inject
    NavigationConfiguration navigationConfiguration;

    @Inject
    NavigationItemFactory navigationItemFactory;

    @Before
    public void setUp() throws Exception {
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);

        sportsStore.setSports(resourceProvider.provideSports());
    }

    @Test
    public void objectSuccessfullyCreated() {
        assertThat(navigationConfiguration).isNotNull();
    }

    @Test
    public void getListItems() {
        // given
        // when
        List<? extends NavigationItem> listItems = navigationConfiguration.getListItems();

        // then
        assertThat(listItems).isNotNull().isSorted().doesNotHaveDuplicates().hasSize(32);
    }

    @Test
    public void getListItemsFromSportsStore() {
        // given
        // when
        List<Sport> listItems = sportsStore.getSports();

        // then
        assertThat(listItems).isNotNull().isSorted().doesNotHaveDuplicates().hasSize(32).isInstanceOf(ArrayList.class);
    }

    @Test
    public void getMenuItems() {
        // given
        NavigationItem inPlayItem = navigationItemFactory.getNavigationItemForId(BuildConfig.INPLAY_ITEM_ID);

        // when
        List<NavigationItem> menuItems = navigationConfiguration.getMenuItems();

        // then
        assertThat(menuItems).isNotNull().hasSize(6).containsSequence(new Sport(102), new Sport(8), new Sport(171), new Sport(190), new Sport(4), inPlayItem);
    }

    @Test
    public void createNavigationFragmentsTest() {
        // given
        // when
        List<AbstractNavigationFragment> navigationFragments = navigationConfiguration.createNavigationFragments();

        // then Sports 102, 8, 171, 190, 4
        assertThat(navigationFragments).isNotNull().hasSize(7);
        assertThat(navigationFragments.get(0)).isNotNull().isInstanceOf(HomeNavigationFragment.class);
        for (int i = 1; i < 5; i++) {
            assertThat(navigationFragments.get(i)).isNotNull().isInstanceOf(SportNavigationFragment.class);
        }
        assertThat(navigationFragments.get(6)).isNotNull().isInstanceOf(InPlayNavigationFragment.class);
    }

    @Test
    public void createSportNavigationFragmentTest() {
        // given
        long sportId = 4L;

        // when
        AbstractNavigationFragment sportNavigationFragment = navigationConfiguration.createSportNavigationFragment(sportId);

        // then Sports 102, 8, 171, 190, 4
        assertThat(sportNavigationFragment).isNotNull().isInstanceOf(SportNavigationFragment.class);
        assertThat(sportNavigationFragment.getIdentifier()).isEqualTo(sportId);
        assertThat(sportNavigationFragment.getTitle()).isEqualTo("Golf");
    }
}
