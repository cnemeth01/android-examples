package com.sportingbet.sportsbook.coupons.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.general.ResourceProvider;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.RobolectricTestRunner;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.Assertions.assertThat;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
public abstract class AbstractCouponHandlerTest {

    @Inject
    ResourceProvider resourceProvider;

    @Inject
    Context context;

    BetslipCouponDelegate betslipCouponDelegate = Mockito.mock(BetslipCouponDelegate.class);

    protected EventCouponHandler couponHandler;

    @Module(
            injects = {
                    TieCouponHandlerTest.class,
                    NoTieCouponHandlerTest.class,
                    TieAlternativeCouponHandlerTest.class,
                    NoTieAlternativeCouponHandlerTest.class,
                    OverUnderCouponHandlerTest.class,
                    EmptyCouponHandlerTest.class,
                    ThreeBallCouponHandlerTest.class,
            },
            includes = GeneralTestModule.class
    )
    class TestModule {
    }

    @Before
    public void setUp() throws Exception {
        couponHandler = createCouponHandler();
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);
    }

    protected abstract EventCouponHandler createCouponHandler();

    protected RecyclerView.ViewHolder createViewHolder() {
        return couponHandler.onCreateViewHolder(context, null);
    }

    @Test
    public void objectBuilt() {
        assertThat(couponHandler).isNotNull();
    }

    @Test
    public void onCreateViewHolderCreated() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        assertThat(viewHolder).isNotNull();
    }

    @SuppressWarnings("unchecked")
    public <T extends TextView> T getView(RecyclerView.ViewHolder viewHolder, int first_team) {
        return (T) viewHolder.itemView.findViewById(first_team);
    }

    public CouponLayout getCouponLayout(RecyclerView.ViewHolder viewHolder, int first_team) {
        return (CouponLayout) viewHolder.itemView.findViewById(first_team);
    }
}
