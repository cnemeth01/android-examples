package com.sportingbet.sportsbook;

import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;

import dagger.ObjectGraph;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class TestSportsbookApplication extends SportsbookApplication {

    @Override
    protected void setupApplication() {
        objectGraph = ObjectGraph.create(TestModules.createApplication(this));
    }
}

