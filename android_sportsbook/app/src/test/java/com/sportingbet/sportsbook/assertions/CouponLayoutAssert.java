package com.sportingbet.sportsbook.assertions;

import com.sportingbet.sportsbook.coupons.ui.CouponLayout;

import org.fest.assertions.api.Assertions;
import org.fest.assertions.api.android.widget.AbstractRelativeLayoutAssert;

public class CouponLayoutAssert extends AbstractRelativeLayoutAssert<CouponLayoutAssert, CouponLayout> {

    public static CouponLayoutAssert assertThat(CouponLayout actual) {
        return new CouponLayoutAssert(actual);
    }

    public CouponLayoutAssert(CouponLayout actual) {
        super(actual, CouponLayoutAssert.class);
    }

    public CouponLayoutAssert hasText(String text) {
        Assertions.assertThat(actual.getText())
                .overridingErrorMessage("CouponLayout expected text <%s> but was <%s>.", text, actual.getText())
                .isEqualTo(text);
        return this;
    }
}