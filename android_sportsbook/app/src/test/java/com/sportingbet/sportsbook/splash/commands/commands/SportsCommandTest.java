package com.sportingbet.sportsbook.splash.commands.commands;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.general.commands.CommandProgressDelegate;
import com.sportingbet.sportsbook.general.commands.NetworkCommand;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.splash.commands.SportsCommand;
import com.sportingbet.sportsbook.splash.commands.SportsRequester;
import com.sportingbet.sportsbook.sports.SportsStore;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import retrofit.RetrofitError;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportsCommandTest {

    private SportsRequester sportsRequester;

    private SportsStore sportsStore;

    private CommandProgressDelegate delegate;

    SportsCommand sportsCommand;

    @Before
    public void setUp() throws Exception {
        sportsRequester = Mockito.mock(SportsRequester.class);
        sportsStore = Mockito.mock(SportsStore.class);
        delegate = Mockito.mock(CommandProgressDelegate.class);

        sportsCommand = new SportsCommand(sportsRequester, sportsStore);
        sportsCommand.setDelegate(delegate);
    }

    @Test
    public void objectBuilt() {
        assertThat(sportsCommand).isNotNull().isInstanceOf(NetworkCommand.class);
    }

    @Test
    public void callsNetworkCommandOnExecute() {
        // given
        // when
        sportsCommand.execute();

        // then
        verify(sportsRequester).getSports(Mockito.any(NetworkResponseListener.class));
    }

    @Test
    public void notifyingSportsContainerAfterSuccess() {
        // given
        List<Sport> sports = Lists.newArrayList();

        // when
        sportsCommand.success(sports);

        // then
        verify(sportsStore).setSports(sports);
    }

    @Test
    public void notifyingDelegateAfterSuccess() {
        // given

        // when
        sportsCommand.success(Lists.<Sport>newArrayList());

        // then
        verify(delegate).commandDidFinish(sportsCommand);
    }

    @Test
    public void notifyingDelegateAfterFailure() {
        // given
        RetrofitError retrofitError = RetrofitError.unexpectedError("url", new Throwable());

        // when
        sportsCommand.failure(retrofitError);

        // then
        verify(delegate).commandDidFinishWithError(sportsCommand, retrofitError);
    }

    @Test
    public void notNotifyingDelegateAfterCancelAndFailure() {
        // given
        RetrofitError retrofitError = RetrofitError.unexpectedError("url", new Throwable());
        sportsCommand.cancel();

        // when
        sportsCommand.failure(retrofitError);

        // then
        verify(delegate, Mockito.never()).commandDidFinishWithError(sportsCommand, retrofitError);
    }

    @Test
    public void notNotifyingDelegateAfterCancelAndSuccess() {
        // given
        sportsCommand.cancel();

        // when
        sportsCommand.success(Lists.<Sport>newArrayList());

        // then
        verify(delegate, Mockito.never()).commandDidFinish(sportsCommand);
    }
}
