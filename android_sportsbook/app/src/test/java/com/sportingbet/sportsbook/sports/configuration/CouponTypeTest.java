package com.sportingbet.sportsbook.sports.configuration;

import com.sportingbet.sportsbook.model.book.WebCouponTemplate;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CouponTypeTest {


    // getItemTypeForTemplate
    @Test
    public void getItemTypeForTemplateTypeForCPNTEAMTIE() {
        // given
        WebCouponTemplate couponTemplate = WebCouponTemplate.CPNTEAMTIE;

        // when
        int typeForTemplate = CouponType.getItemTypeForTemplate(couponTemplate);

        // then
        assertThat(typeForTemplate).isEqualTo(3);
    }

    @Test
    public void getItemTypeForTemplateTypeForCPNPLAYTIE() {
        // given
        WebCouponTemplate couponTemplate = WebCouponTemplate.CPNPLAYTIE;

        // when
        int typeForTemplate = CouponType.getItemTypeForTemplate(couponTemplate);

        // then
        assertThat(typeForTemplate).isEqualTo(3);
    }

    @Test
    public void getItemTypeForTemplateTypeForCPN2BALLS() {
        // given
        WebCouponTemplate couponTemplate = WebCouponTemplate.CPN2BALLS;

        // when
        int typeForTemplate = CouponType.getItemTypeForTemplate(couponTemplate);

        // then
        assertThat(typeForTemplate).isEqualTo(3);
    }

    @Test
    public void getItemTypeForTemplateTypeForCPNTEAMNOTIE() {
        // given
        WebCouponTemplate couponTemplate = WebCouponTemplate.CPNTEAMNOTIE;

        // when
        int typeForTemplate = CouponType.getItemTypeForTemplate(couponTemplate);

        // then
        assertThat(typeForTemplate).isEqualTo(2);
    }

    @Test
    public void getItemTypeForTemplateTypeForCPNPLAYNOTIE() {
        // given
        WebCouponTemplate couponTemplate = WebCouponTemplate.CPNPLAYNOTIE;

        // when
        int typeForTemplate = CouponType.getItemTypeForTemplate(couponTemplate);

        // then
        assertThat(typeForTemplate).isEqualTo(2);
    }

    @Test
    public void getItemTypeForTemplateTypeForCPNTOTALS() {
        // given
        WebCouponTemplate couponTemplate = WebCouponTemplate.CPNTOTALS;

        // when
        int typeForTemplate = CouponType.getItemTypeForTemplate(couponTemplate);

        // then
        assertThat(typeForTemplate).isEqualTo(4);
    }

    @Test
    public void getItemTypeForTemplateTypeForCPNOVERUNDER() {
        // given
        WebCouponTemplate couponTemplate = WebCouponTemplate.CPNOVERUNDER;

        // when
        int typeForTemplate = CouponType.getItemTypeForTemplate(couponTemplate);

        // then
        assertThat(typeForTemplate).isEqualTo(4);
    }

    @Test
    public void getItemTypeForTemplateTypeForCPN3BALLS() {
        // given
        WebCouponTemplate couponTemplate = WebCouponTemplate.CPN3BALLS;

        // when
        int typeForTemplate = CouponType.getItemTypeForTemplate(couponTemplate);

        // then
        assertThat(typeForTemplate).isEqualTo(7);
    }
}
