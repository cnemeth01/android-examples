package com.sportingbet.sportsbook.general.commands;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.general.dagger.configuration.ActivityModule;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.splash.commands.SessionCommand;
import com.sportingbet.sportsbook.splash.commands.SessionRequester;
import com.sportingbet.sportsbook.splash.commands.SportsCommand;
import com.sportingbet.sportsbook.splash.commands.SportsRequester;
import com.sportingbet.sportsbook.splash.commands.SportConfigCommand;
import com.sportingbet.sportsbook.splash.commands.SportConfigRequester;
import com.sportingbet.sportsbook.sports.configuration.SportConfig;
import com.sportingbet.sportsbook.sports.configuration.SportConfigPersistentStore;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.ObjectGraph;
import dagger.Provides;
import retrofit.RetrofitError;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CommandControllerTest {

    @Module(
            injects = CommandControllerTest.class,
            includes = {
                    GeneralTestModule.class,
                    ActivityModule.class
            },
            overrides = true
    )
    class TestModule {

        @Provides
        @Singleton
        SessionRequester provideSessionRequester() {
            return mock(SessionRequester.class);
        }

        @Provides
        @Singleton
        SportsRequester provideSportsRequester() {
            return mock(SportsRequester.class);
        }

        @Provides
        @Singleton
        SportConfigRequester provideSportConfigRequester() {
            return mock(SportConfigRequester.class);
        }

        @Provides
        @Singleton
        SportConfigPersistentStore provideSportConfigPersistentStore() {
            return mock(SportConfigPersistentStore.class);
        }
    }

    @Inject
    @Named("splash")
    CommandController commandController;

    @Inject
    SessionRequester sessionRequester;

    @Inject
    SportsRequester sportsRequester;

    @Inject
    SportConfigRequester sportConfigRequester;

    private ProgressController progressController;
    private CommandController.Delegate delegate;

    @Before
    public void setUp() throws Exception {
        ObjectGraph.create(TestModules.create(null, new TestModule())).inject(this);

        // mocking
        progressController = Mockito.mock(ProgressController.class);
        commandController.setProgressController(progressController);

        delegate = mock(CommandController.Delegate.class);
        commandController.setDelegate(delegate);
    }

    private SportsCommand getGetSportsCommand() {
        return (SportsCommand) commandController.commands.get(1);
    }

    private SessionCommand getAuthenticationCommand() {
        return (SessionCommand) commandController.commands.get(0);
    }

    private SportConfigCommand getGetSportConfigCommand() {
        return (SportConfigCommand) commandController.commands.get(2);
    }

    @Test
    public void objectBuilt() {
        assertThat(commandController).isNotNull();
    }

    @Test
    public void firstCommandExecutedOnStart() {
        // given
        // when
        commandController.start();

        // then
        verify(sessionRequester).getActiveSessionId(any(NetworkResponseListener.class));
    }

    @Test
    public void progressControllerInvokedOnStart() {
        // given
        // when
        commandController.start();

        // then
        verify(progressController).preExecute();
    }

    @Test
    public void startingSecondCommandAfterFirstFinishedWithSuccess() {
        // given
        // when
        getAuthenticationCommand().success(null);

        // then
        verify(sportsRequester).getSports(any(NetworkResponseListener.class));
    }

    @Test
    public void informingDelegateAfterFirstCommandFinishedWithError() {
        // given
        RetrofitError retrofitError = RetrofitError.unexpectedError("url", new Throwable());

        // when
        getAuthenticationCommand().failure(retrofitError);

        // then
        verify(delegate).commandControllerDidFinishWithError(retrofitError);
    }

    @Test
    public void informingProgressControllerAfterFirstCommandFinishedWithError() {
        // given
        RetrofitError retrofitError = RetrofitError.unexpectedError("url", new Throwable());

        // when
        getAuthenticationCommand().failure(retrofitError);

        // then
        verify(progressController).postExecuteWithSuccess(false);
    }

    @Test
    public void informDelegateAfterGetSportsCommand() {
        // given
        // when
        getGetSportsCommand().success(Lists.<Sport>newArrayList());

        // then
        verify(sportConfigRequester).requestSportConfigs(any(NetworkResponseListener.class));
    }

    @Test
    public void informDelegateOnFailInGetSportsCommand() {
        // given
        RetrofitError retrofitError = RetrofitError.unexpectedError("url", new Throwable());

        // when
        getGetSportsCommand().failure(retrofitError);

        // then
        verify(progressController).postExecuteWithSuccess(false);

    }

    @Test
    public void informDelegateOnLastCommandFinish() {
        // given
        // when
        getGetSportConfigCommand().success(Lists.<SportConfig>newArrayList());

        // then
        verify(delegate).commandControllerDidFinish();
    }

    @Test
    public void informProgressControllerOnLastCommandFinish() {
        // given
        // when
        getGetSportConfigCommand().success(Lists.<SportConfig>newArrayList());

        // then
        verify(progressController).postExecuteWithSuccess(true);
    }

    @Test
    public void cancelCommandsWhenControllerIsCanceled() {
        // given
        commandController.commands.clear();
        SportsCommand sportsCommand = Mockito.mock(SportsCommand.class);
        SessionCommand sessionCommand = Mockito.mock(SessionCommand.class);
        commandController.commands.add(sportsCommand);
        commandController.commands.add(sessionCommand);

        // when
        commandController.cancel();

        // then
        verify(sportsCommand).cancel();
        verify(sessionCommand).cancel();
    }
}
