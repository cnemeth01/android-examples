package com.sportingbet.sportsbook.coupons.handlers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.model.event.Event;

import org.junit.Test;
import org.robolectric.annotation.Config;

import static org.fest.assertions.api.ANDROID.assertThat;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class TieAlternativeCouponHandlerTest extends AbstractCouponHandlerTest {

    @Override
    protected EventCouponHandler createCouponHandler() {
        return new TieAlternativeCouponHandler();
    }

    @Test
    public void onCreateViewHolderHasTextViews() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View firstTeamText = viewHolder.itemView.findViewById(R.id.first_team);
        assertThat(firstTeamText).isNotNull().isInstanceOf(TextView.class);
        View firstTeamScoreText = viewHolder.itemView.findViewById(R.id.first_team_score);
        assertThat(firstTeamScoreText).isNotNull().isInstanceOf(TextView.class);

        View subtitleText = viewHolder.itemView.findViewById(R.id.subtitle);
        assertThat(subtitleText).isNotNull().isInstanceOf(TextView.class);

        View secondTeamText = viewHolder.itemView.findViewById(R.id.second_team);
        assertThat(secondTeamText).isNotNull().isInstanceOf(TextView.class);
        View secondTeamScoreText = viewHolder.itemView.findViewById(R.id.second_team_score);
        assertThat(secondTeamScoreText).isNotNull().isInstanceOf(TextView.class);
    }

    @Test
    public void onCreateViewHolderHasButtons() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View firstButton = viewHolder.itemView.findViewById(R.id.betnow_button);
        assertThat(firstButton).isNotNull().isInstanceOf(Button.class);
    }

    @Test
    public void onBindViewHolderWithTestEvent1() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_1.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Liverpool v Arsenal");

        assertThat(getView(viewHolder, R.id.second_team))
                .isGone();

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .hasText("1st Half");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isVisible()
                .hasText("0");

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isVisible()
                .hasText("5");

        assertThat(getView(viewHolder, R.id.betnow_button))
                .isVisible()
                .isNotClickable()
                .isEnabled()
                .hasText("Bet now");
    }

    @Test
    public void onBindViewHolderWithTestEvent2() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_2.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Selenium Bet Factor Test ");

        assertThat(getView(viewHolder, R.id.second_team))
                .isGone();

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .hasText("Tommorow");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.betnow_button))
                .isVisible()
                .isNotClickable()
                .isEnabled()
                .hasText("Bet now");
    }

    @Test
    public void onBindViewHolderWithTestEvent3() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_3.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Liverpool v Milan");

        assertThat(getView(viewHolder, R.id.second_team))
                .isGone();

        assertThat(getView(viewHolder, R.id.subtitle))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.betnow_button))
                .isVisible()
                .isNotClickable()
                .isEnabled()
                .hasText("Bet now");
    }

    @Test
    public void onBindViewHolderWithTestEventDisabled() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_disabled.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Gungoren v Bayburt Genclikspor");

        assertThat(getView(viewHolder, R.id.second_team))
                .isGone();

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isDisabled()
                .hasText("89' 2nd Half");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isVisible()
                .hasText("0");

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isVisible()
                .hasText("3");

        assertThat(getView(viewHolder, R.id.betnow_button))
                .isVisible()
                .isNotClickable()
                .isDisabled()
                .hasText("Bet now");
    }

    @Test
    public void onBindViewHolderWithTestEventDisabled2() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_disabled_2.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Shkola Myacha v Zelenograd");

        assertThat(getView(viewHolder, R.id.second_team))
                .isGone();

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isDisabled()
                .hasText("10:00 BST");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isGone();

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isGone();

        assertThat(getView(viewHolder, R.id.betnow_button))
                .isVisible()
                .isNotClickable()
                .isDisabled()
                .hasText("Bet now");
    }

    @Test
    public void onBindViewHolderWithTestEventDisabled3() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_disabled_3.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.betnow_button))
                .isVisible()
                .isNotClickable()
                .isEnabled()
                .hasText("Bet now");
    }

    @Test
    public void onBindViewHolderWithEventUpNextTest() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_upnext.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Burnley v Arsenal");

        assertThat(getView(viewHolder, R.id.second_team))
                .isGone();

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasText("Tomorrow 17:30 BST");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.betnow_button))
                .isVisible()
                .isNotClickable()
                .hasText("Bet now");
    }

    @Test
    public void onBindViewHolderWithNotRunningEventTest() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_not_running.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Yambol v Balkan");

        assertThat(getView(viewHolder, R.id.second_team))
                .isGone();

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasText("16:00 BST");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.betnow_button))
                .isVisible()
                .isNotClickable()
                .hasText("Bet now");
    }
}
