package com.sportingbet.sportsbook.general.dagger.configuration;

import android.app.Application;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.Modules;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public final class TestModules {

    public static Object[] create(Application application, Object module) {
        List<Object> modules = Lists.newArrayList(Modules.create(application));
        modules.add(new ActivityModule(null));
        modules.add(new GeneralTestModule());
        modules.add(module);
        return modules.toArray();
    }

    public static Object[] createApplication(Application application) {
        List<Object> modules = Lists.newArrayList(Modules.create(application));
        modules.add(new GeneralTestModule());
        return modules.toArray();
    }
}
