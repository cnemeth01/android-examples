package com.sportingbet.sportsbook.general.network.requesters;

import com.sportingbet.sportsbook.content.fragments.highlights.HighlightsRequester;
import com.sportingbet.sportsbook.general.network.CallbackWrapper;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.model.event.DetailsWrapper;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import retrofit.RetrofitError;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HighlightsRequesterTest {

    private PublishingService publishingService;

    HighlightsRequester highlightsRequester;

    @Before
    public void setUp() throws Exception {
        publishingService = Mockito.mock(PublishingService.class);

        highlightsRequester = new HighlightsRequester(publishingService);
    }

    @Test
    public void objectBuilt() {
        assertThat(highlightsRequester).isNotNull();
    }

    @Test
    public void initializingRequesterShouldNotNotifyProgressController() {
        // given
        ProgressController progressController = Mockito.mock(ProgressController.class);

        // when
        highlightsRequester.setupResponseListener(Mockito.mock(NetworkResponseListener.class));

        // then
        Mockito.verify(progressController, Mockito.never()).preExecute();
    }

    @Test
    public void requestHighlightsShouldNotifyProgressController() {
        // given
        ProgressController progressController = Mockito.mock(ProgressController.class);
        highlightsRequester.setupResponseListener(Mockito.mock(NetworkResponseListener.class));

        // when
        highlightsRequester.prepareRequest(progressController);
        highlightsRequester.requestHighlightsForSport(102);

        // then
        Mockito.verify(progressController).preExecute();
    }

    @Test
    public void requestHighlightsForSportShouldCallPublishingService() {
        // given
        highlightsRequester.setupResponseListener(Mockito.mock(NetworkResponseListener.class));

        // when
        highlightsRequester.requestHighlightsForSport(102);

        // then
        Mockito.verify(publishingService).getHighlights(Mockito.eq("102"), Mockito.eq(10), Mockito.any(CallbackWrapper.class));
    }

    @Test
    public void sucessShouldNotifyResponseListener() {
        // given
        NetworkResponseListener responseListener = Mockito.mock(NetworkResponseListener.class);
        highlightsRequester.setupResponseListener(responseListener);
        DetailsWrapper detailsWrapper = new DetailsWrapper();

        // when
        highlightsRequester.prepareRequest();
        highlightsRequester.requestHighlightsForSport(102);
        highlightsRequester.getCallback().success(detailsWrapper, null);

        // then
        Mockito.verify(responseListener).success(detailsWrapper);
    }

    @Test
    public void failureShouldNotifyResponseListener() {
        // given
        NetworkResponseListener responseListener = Mockito.mock(NetworkResponseListener.class);
        highlightsRequester.setupResponseListener(responseListener);
        RetrofitError retrofitError = RetrofitError.unexpectedError("url", new Throwable());

        // when
        highlightsRequester.prepareRequest();
        highlightsRequester.requestHighlightsForSport(102);
        highlightsRequester.getCallback().failure(retrofitError);

        // then
        Mockito.verify(responseListener).failure(retrofitError);
    }

    @Test
    public void cancelBeforeSucessShouldNotNotifyResponseListener() {
        // given
        NetworkResponseListener responseListener = Mockito.mock(NetworkResponseListener.class);
        highlightsRequester.setupResponseListener(responseListener);
        DetailsWrapper detailsWrapper = new DetailsWrapper();

        // when
        highlightsRequester.prepareRequest();
        highlightsRequester.requestHighlightsForSport(102);
        highlightsRequester.cancelRequest();
        highlightsRequester.getCallback().success(detailsWrapper, null);

        // then
        Mockito.verify(responseListener, Mockito.never()).success(detailsWrapper);
    }

    @Test
    public void cancelBeforeFailureShouldNotNotifyResponseListener() {
        // given
        NetworkResponseListener responseListener = Mockito.mock(NetworkResponseListener.class);
        highlightsRequester.setupResponseListener(responseListener);
        RetrofitError retrofitError = RetrofitError.unexpectedError("url", new Throwable());

        // when
        highlightsRequester.prepareRequest();
        highlightsRequester.requestHighlightsForSport(102);
        highlightsRequester.cancelRequest();
        highlightsRequester.getCallback().failure(retrofitError);

        // then
        Mockito.verify(responseListener, Mockito.never()).failure(retrofitError);
    }

    @Test
    public void cancelShouldNotifyProgressController() {
        // given
        ProgressController progressController = Mockito.mock(ProgressController.class);
        highlightsRequester.setupResponseListener(Mockito.mock(NetworkResponseListener.class));

        // when
        highlightsRequester.prepareRequest(progressController);
        highlightsRequester.requestHighlightsForSport(102);
        highlightsRequester.cancelRequest();

        // then
        Mockito.verify(progressController).postExecuteWithSuccess(false);
    }

    @Test
    public void responseListenerNotifiedAfterSuccessAfterCancellation() {
        // given
        ProgressController progressController = Mockito.mock(ProgressController.class);
        NetworkResponseListener responseListener = Mockito.mock(NetworkResponseListener.class);
        highlightsRequester.setupResponseListener(responseListener);
        DetailsWrapper detailsWrapper = new DetailsWrapper();

        // when
        highlightsRequester.prepareRequest(progressController);
        highlightsRequester.requestHighlightsForSport(102);
        highlightsRequester.cancelRequest();
        highlightsRequester.prepareRequest(progressController);
        highlightsRequester.requestHighlightsForSport(102);
        highlightsRequester.getCallback().success(detailsWrapper, null);

        // then
        Mockito.verify(progressController).postExecuteWithSuccess(false);
        Mockito.verify(responseListener).success(detailsWrapper);
    }

    @Test
    public void cancellingTwoTimesInTheRow() {
        // given
        highlightsRequester.setupResponseListener(Mockito.mock(NetworkResponseListener.class));

        // when
        highlightsRequester.requestHighlightsForSport(102);
        highlightsRequester.cancelRequest();
        highlightsRequester.cancelRequest();
    }
}
