package com.sportingbet.sportsbook.coupons.event;

import com.sportingbet.sportsbook.coupons.handlers.EmptyCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.NoTieAlternativeCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.NoTieCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.TieAlternativeCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.TieCouponHandler;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;
import com.sportingbet.sportsbook.model.book.WebCouponTemplate;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.sports.configuration.CouponType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class EventCouponHandlerFactoryTest {

    @Inject
    EventCouponHandlerFactory eventCouponHandlerFactory;

    @Module(
            injects = EventCouponHandlerFactoryTest.class,
            includes = {
                    GeneralTestModule.class,
            },
            overrides = true
    )
    class TestModule {
    }

    @Before
    public void setUp() throws Exception {
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);
    }

    @Test
    public void objectBuilt() {
        assertThat(eventCouponHandlerFactory).isNotNull();
    }

    private Event createEvent(long sportId, WebCouponTemplate couponTemplate, int selectionCount) {
        Event event = Mockito.mock(Event.class);
        Mockito.when(event.getWebCouponTemplate()).thenReturn(couponTemplate);
        Mockito.when(event.getSportId()).thenReturn(sportId);
        Mockito.when(event.getSelectionCount()).thenReturn(selectionCount);
        return event;
    }

    // getItemTypeForEvent
    @Test
    public void getItemTypeForEventForFootballEventDefaultCoupon() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.CPNTEAMTIE, 3);

        // when
        int typeForTemplate = eventCouponHandlerFactory.getItemTypeForEvent(event, CouponType.CouponTypeTie);

        // then
        assertThat(typeForTemplate).isEqualTo(CouponType.CouponTypeTie.value());
    }

    @Test
    public void getItemTypeForEventForFootballEventDefaultCouponDifferentWebTemplate() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.CPNPLAYTIE, 3);

        // when
        int typeForTemplate = eventCouponHandlerFactory.getItemTypeForEvent(event, CouponType.CouponTypeTie);

        // then
        assertThat(typeForTemplate).isEqualTo(CouponType.CouponTypeTie.value());
    }

    @Test
    public void getItemTypeForEventForFootballEventAlternativeCoupon() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.UNKNOWN, 1);

        // when
        int typeForTemplate = eventCouponHandlerFactory.getItemTypeForEvent(event, CouponType.CouponTypeTie);

        // then
        assertThat(typeForTemplate).isEqualTo(CouponType.CouponTypeTieAlternative.value());
    }

    @Test
    public void getItemTypeForEventForTennisEventDefaultCoupon() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNPLAYNOTIE, 2);

        // when
        int typeForTemplate = eventCouponHandlerFactory.getItemTypeForEvent(event, CouponType.CouponTypeNoTie);

        // then
        assertThat(typeForTemplate).isEqualTo(CouponType.CouponTypeNoTie.value());
    }

    @Test
    public void getItemTypeForEventForTennisEventDefaultCouponDifferentWebTemplate() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNTEAMNOTIE, 2);

        // when
        int typeForTemplate = eventCouponHandlerFactory.getItemTypeForEvent(event, CouponType.CouponTypeNoTie);

        // then
        assertThat(typeForTemplate).isEqualTo(CouponType.CouponTypeNoTie.value());
    }

    @Test
    public void getItemTypeForEventForTennisEventAlternativeCoupon() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNTEAMTIE, 3);

        // when
        int typeForTemplate = eventCouponHandlerFactory.getItemTypeForEvent(event, CouponType.CouponTypeNoTie);

        // then
        assertThat(typeForTemplate).isEqualTo(CouponType.CouponTypeNoTieAlternative.value());
    }

    @Test
    public void getItemTypeForEventFor136EventAlternativeCoupon() {
        // given
        Event event = createEvent(136L, WebCouponTemplate.UNKNOWN, 1);

        // when
        int typeForTemplate = eventCouponHandlerFactory.getItemTypeForEvent(event, CouponType.CouponTypeEmpty);

        // then
        assertThat(typeForTemplate).isEqualTo(CouponType.CouponTypeEmpty.value());
    }

    @Test
    public void getItemTypeForEventForFootballEventInTennisList() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.CPNPLAYTIE, 3);

        // when
        int typeForTemplate = eventCouponHandlerFactory.getItemTypeForEvent(event, CouponType.CouponTypeNoTie);

        // then
        assertThat(typeForTemplate).isEqualTo(CouponType.CouponTypeNoTieAlternative.value());
    }

    @Test
    public void getItemTypeForEventForTennisEventInFootballList() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNTEAMNOTIE, 2);

        // when
        int typeForTemplate = eventCouponHandlerFactory.getItemTypeForEvent(event, CouponType.CouponTypeTie);

        // then
        assertThat(typeForTemplate).isEqualTo(CouponType.CouponTypeTieAlternative.value());
    }

    @Test
    public void getItemTypeForEventForTennisEventInLiveBetting() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNTEAMNOTIE, 2);

        // when
        int typeForTemplate = eventCouponHandlerFactory.getItemTypeForEvent(event, CouponType.CouponTypeEmpty);

        // then
        assertThat(typeForTemplate).isEqualTo(CouponType.CouponTypeEmpty.value());
    }

    // getHandlerForEvent
    @Test
    public void getHandlerForEventForFootballEventDefaultCoupon() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.CPNTEAMTIE, 3);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeTie);

        // then
        assertThat(couponHandler).isInstanceOf(TieCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForFootballEventDefaultCouponDifferentWebTemplate() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.CPNPLAYTIE, 3);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeTie);

        // then
        assertThat(couponHandler).isInstanceOf(TieCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForFootballEventAlternativeCoupon() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.CPNTOTALS, 3);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeTie);

        // then
        assertThat(couponHandler).isInstanceOf(TieAlternativeCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForFootballEventAlternativeCouponOnly2Selections() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.CPNTEAMTIE, 2);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeTie);

        // then
        assertThat(couponHandler).isInstanceOf(TieAlternativeCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForFootballEventAlternativeCouponWithNoSelections() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.CPNPLAYTIE, 0);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeTie);

        // then
        assertThat(couponHandler).isInstanceOf(TieAlternativeCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForTennisEventDefaultCoupon() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNTEAMNOTIE, 2);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeNoTie);

        // then
        assertThat(couponHandler).isInstanceOf(NoTieCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForTennisEventDefaultCouponDifferentWebTemplate() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNPLAYNOTIE, 2);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeNoTie);

        // then
        assertThat(couponHandler).isInstanceOf(NoTieCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForTennisEventAlternativeCoupon() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNTOTALS, 2);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeNoTie);

        // then
        assertThat(couponHandler).isInstanceOf(NoTieAlternativeCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForTennisEventAlternativeCouponOnly1Selection() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNTEAMNOTIE, 1);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeNoTie);

        // then
        assertThat(couponHandler).isInstanceOf(NoTieAlternativeCouponHandler.class);
    }


    @Test
    public void getHandlerForEventForTennisEventAlternativeCouponWithNoSelections() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNPLAYNOTIE, 0);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeNoTie);

        // then
        assertThat(couponHandler).isInstanceOf(NoTieAlternativeCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForTennisEventInFootballList() {
        // given
        Event event = createEvent(8L, WebCouponTemplate.CPNPLAYNOTIE, 2);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeTie);

        // then
        assertThat(couponHandler).isInstanceOf(TieAlternativeCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForFootballEventInTennisList() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.CPNTEAMTIE, 3);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeNoTie);

        // then
        assertThat(couponHandler).isInstanceOf(NoTieAlternativeCouponHandler.class);
    }

    @Test
    public void getHandlerForEventForFootballEventInLiveBetting() {
        // given
        Event event = createEvent(102L, WebCouponTemplate.CPNTEAMTIE, 2);

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForEvent(event, CouponType.CouponTypeEmpty);

        // then
        assertThat(couponHandler).isInstanceOf(EmptyCouponHandler.class);
    }

    // getHandlerForItemType
    @Test
    public void getHandlerForCouponTypeTie() {
        // given
        int itemType = CouponType.CouponTypeTie.value();

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(couponHandler).isInstanceOf(TieCouponHandler.class);
    }

    @Test
    public void getHandlerForCouponTypeNoTie() {
        // given
        int itemType = CouponType.CouponTypeNoTie.value();

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(couponHandler).isInstanceOf(NoTieCouponHandler.class);
    }

    @Test
    public void getHandlerForCouponTypeTieAlternative() {
        // given
        int itemType = CouponType.CouponTypeTieAlternative.value();

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(couponHandler).isInstanceOf(TieAlternativeCouponHandler.class);
    }

    @Test
    public void getHandlerForCouponTypeNoTieAlternative() {
        // given
        int itemType = CouponType.CouponTypeNoTieAlternative.value();

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(couponHandler).isInstanceOf(NoTieAlternativeCouponHandler.class);
    }

    @Test
    public void getHandlerForCouponTypeEmpty() {
        // given
        int itemType = CouponType.CouponTypeEmpty.value();

        // when
        EventCouponHandler couponHandler = eventCouponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(couponHandler).isInstanceOf(EmptyCouponHandler.class);
    }
}
