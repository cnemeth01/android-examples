package com.sportingbet.sportsbook.coupons.handlers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.assertions.CouponLayoutAssert;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.model.event.Event;

import org.junit.Test;
import org.robolectric.annotation.Config;

import static org.fest.assertions.api.ANDROID.assertThat;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class OverUnderCouponHandlerTest extends AbstractCouponHandlerTest {

    @Override
    protected EventCouponHandler createCouponHandler() {
        return new OverUnderCouponHandler(betslipCouponDelegate);
    }

    @Test
    public void onCreateViewHolderHasTextViews() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View overText = viewHolder.itemView.findViewById(R.id.first_team);
        assertThat(overText).isNotNull().isInstanceOf(TextView.class);

        View underText = viewHolder.itemView.findViewById(R.id.second_team);
        assertThat(underText).isNotNull().isInstanceOf(TextView.class);
    }

    @Test
    public void onCreateViewHolderHasButtons() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View overButton = viewHolder.itemView.findViewById(R.id.first_button);
        assertThat(overButton).isNotNull().isInstanceOf(CouponLayout.class);

        View underButton = viewHolder.itemView.findViewById(R.id.second_button);
        assertThat(underButton).isNotNull().isInstanceOf(CouponLayout.class);
    }

    @Test
    public void onBindViewHolderWithTestOverUnder() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_over_under.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Over 5.5");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Under 5.5");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("1/18");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("13/2");
    }

    @Test
    public void onBindViewHolderWithTestOverUnderDisabled() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_over_under_disabled.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Over 5.5");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Under 5.5");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isDisabled()
                .hasText("1/18");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isDisabled()
                .hasText("13/2");
    }
}
