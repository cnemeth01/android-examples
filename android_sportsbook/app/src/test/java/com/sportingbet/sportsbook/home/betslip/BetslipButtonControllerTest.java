package com.sportingbet.sportsbook.home.betslip;

import android.widget.Button;

import com.google.common.eventbus.EventBus;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipButtonControllerTest {

    private BetslipButtonController betslipButtonController;
    private EventBus eventBus;

    @Before
    public void setUp() throws Exception {
        eventBus = Mockito.mock(EventBus.class);
        betslipButtonController = new BetslipButtonController(eventBus);
    }

    @Test
    public void register() throws Exception {
        // given
        Button button = Mockito.mock(Button.class);

        // when
        betslipButtonController.register(button);

        // then
        Mockito.verify(eventBus).register(betslipButtonController);
    }

    @Test
    public void unregister() throws Exception {
        // given
        // when
        betslipButtonController.unregister();

        // then
        Mockito.verify(eventBus).unregister(betslipButtonController);
    }
}
