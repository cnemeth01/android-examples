package com.sportingbet.sportsbook.sports.configuration;

import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class SportConfigProviderTest {

    @Inject
    SportConfigProvider sportConfigProvider;

    @Module(
            injects = SportConfigProviderTest.class,
            includes = {
                    GeneralTestModule.class,
            },
            overrides = true
    )
    class TestModule {
    }

    @Before
    public void setUp() throws Exception {
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);
    }

    @Test
    public void objectSuccessfullyCreated() {
        assertThat(sportConfigProvider).isNotNull();
    }

    @Test
    public void getDefaultCouponTypeForFootball() {
        // given
        long sportId = 102; // football

        // when
        CouponType defaultCouponType = sportConfigProvider.getDefaultCouponTypeForSportId(sportId);

        // then
        assertThat(defaultCouponType).isEqualTo(CouponType.CouponTypeTie);
    }

    @Test
    public void getDefaultCouponTypeForTennis() {
        // given
        long sportId = 8; // tennis

        // when
        CouponType defaultCouponType = sportConfigProvider.getDefaultCouponTypeForSportId(sportId);

        // then
        assertThat(defaultCouponType).isEqualTo(CouponType.CouponTypeNoTie);
    }

    @Test
    public void getDefaultCouponTypeForSport1() {
        // given
        long sportId = 1;

        // when
        CouponType defaultCouponType = sportConfigProvider.getDefaultCouponTypeForSportId(sportId);

        // then
        assertThat(defaultCouponType).isEqualTo(CouponType.CouponTypeTie);
    }

    @Test
    public void getDefaultCouponTypeForSport136() {
        // given
        long sportId = 136;

        // when
        CouponType defaultCouponType = sportConfigProvider.getDefaultCouponTypeForSportId(sportId);

        // then
        assertThat(defaultCouponType).isEqualTo(CouponType.CouponTypeEmpty);
    }

    @Test
    public void getDefaultCouponTypeForSport4() {
        // given
        long sportId = 4;

        // when
        CouponType defaultCouponType = sportConfigProvider.getDefaultCouponTypeForSportId(sportId);

        // then
        assertThat(defaultCouponType).isEqualTo(CouponType.CouponTypeEmpty);
    }

    @Test
    public void getDefaultCouponTypeForSport1000() {
        // given
        long sportId = 1000; // not defined sport

        // when
        CouponType defaultCouponType = sportConfigProvider.getDefaultCouponTypeForSportId(sportId);

        // then
        assertThat(defaultCouponType).isEqualTo(CouponType.CouponTypeEmpty);
    }

    // getTabLayoutTypeForSportId
    @Test
    public void getTabLayoutTypeForSportIdFootball() {
        // given
        long sportId = 102; // football

        // when
        TabLayoutType tabLayoutType = sportConfigProvider.getTabLayoutTypeForSportId(sportId);

        // then
        assertThat(tabLayoutType).isEqualTo(TabLayoutType.TabLayoutTypeFourTabs);
    }

    @Test
    public void getTabLayoutTypeForSportIdTennis() {
        // given
        long sportId = 8; // tennis

        // when
        TabLayoutType tabLayoutType = sportConfigProvider.getTabLayoutTypeForSportId(sportId);

        // then
        assertThat(tabLayoutType).isEqualTo(TabLayoutType.TabLayoutTypeFourTabs);
    }

    @Test
    public void getTabLayoutTypeForSportId1() {
        // given
        long sportId = 1;

        // when
        TabLayoutType tabLayoutType = sportConfigProvider.getTabLayoutTypeForSportId(sportId);

        // then
        assertThat(tabLayoutType).isEqualTo(TabLayoutType.TabLayoutTypeThreeTabs);
    }

    @Test
    public void getTabLayoutTypeForSportId190() {
        // given
        long sportId = 190;

        // when
        TabLayoutType tabLayoutType = sportConfigProvider.getTabLayoutTypeForSportId(sportId);

        // then
        assertThat(tabLayoutType).isEqualTo(TabLayoutType.TabLayoutTypeFourTabs);
    }

    @Test
    public void getTabLayoutTypeForSportId133() {
        // given
        long sportId = 133;

        // when
        TabLayoutType tabLayoutType = sportConfigProvider.getTabLayoutTypeForSportId(sportId);

        // then
        assertThat(tabLayoutType).isEqualTo(TabLayoutType.TabLayoutTypeThreeTabs);
    }

    @Test
    public void getTabLayoutTypeForSportIdNotDefinedSport() {
        // given
        long sportId = 1000; // not defined sport

        // when
        TabLayoutType tabLayoutType = sportConfigProvider.getTabLayoutTypeForSportId(sportId);

        // then
        assertThat(tabLayoutType).isEqualTo(TabLayoutType.TabLayoutTypeThreeTabs);
    }

    // getInPlayDefaultCouponTypeForSportId
    @Test
    public void getInPlayDefaultCouponTypeForSportId1() {
        // given
        long sportId = 1;

        // when
        CouponType defaultCouponType = sportConfigProvider.getInPlayDefaultCouponTypeForSportId(sportId);

        // then
        assertThat(defaultCouponType).isEqualTo(CouponType.CouponTypeTie);
    }

    @Test
    public void getInPlayDefaultCouponTypeForSportId136() {
        // given
        long sportId = 136;

        // when
        CouponType defaultCouponType = sportConfigProvider.getInPlayDefaultCouponTypeForSportId(sportId);

        // then
        assertThat(defaultCouponType).isEqualTo(CouponType.CouponTypeEmpty);
    }

    @Test
    public void getInPlayDefaultCouponTypeForSportId190() {
        // given
        long sportId = 190;

        // when
        CouponType defaultCouponType = sportConfigProvider.getInPlayDefaultCouponTypeForSportId(sportId);

        // then
        assertThat(defaultCouponType).isEqualTo(CouponType.CouponTypeNoTie);
    }

    // getInPlayDefaultCouponTypeForSportId
    @Test
    public void getScoreboardLayoutTypeForSportId1() {
        // given
        long sportId = 1;

        // when
        ScoreboardLayoutType scoreboardLayout = sportConfigProvider.getScoreboardLayoutTypeForSportId(sportId);

        // then
        assertThat(scoreboardLayout).isEqualTo(ScoreboardLayoutType.ScoreboardLayoutTypeNoScore);
    }

    @Test
    public void getScoreboardLayoutTypeForSportId102() {
        // given
        long sportId = 102;

        // when
        ScoreboardLayoutType scoreboardLayout = sportConfigProvider.getScoreboardLayoutTypeForSportId(sportId);

        // then
        assertThat(scoreboardLayout).isEqualTo(ScoreboardLayoutType.ScoreboardLayoutTypeFootball);
    }

    @Test
    public void getScoreboardLayoutTypeForSportId177() {
        // given
        long sportId = 177;

        // when
        ScoreboardLayoutType scoreboardLayout = sportConfigProvider.getScoreboardLayoutTypeForSportId(sportId);

        // then
        assertThat(scoreboardLayout).isEqualTo(ScoreboardLayoutType.ScoreboardLayoutTypeVolleyball);
    }

    @Test
    public void getScoreboardLayoutTypeForSportId8() {
        // given
        long sportId = 8;

        // when
        ScoreboardLayoutType scoreboardLayout = sportConfigProvider.getScoreboardLayoutTypeForSportId(sportId);

        // then
        assertThat(scoreboardLayout).isEqualTo(ScoreboardLayoutType.ScoreboardLayoutTypeTennis);
    }

    @Test
    public void getScoreboardLayoutTypeForSportId190() {
        // given
        long sportId = 190;

        // when
        ScoreboardLayoutType scoreboardLayout = sportConfigProvider.getScoreboardLayoutTypeForSportId(sportId);

        // then
        assertThat(scoreboardLayout).isEqualTo(ScoreboardLayoutType.ScoreboardLayoutTypeFootball);
    }

    @Test
    public void getScoreboardLayoutTypeForSportId9() {
        // given
        long sportId = 9;

        // when
        ScoreboardLayoutType scoreboardLayout = sportConfigProvider.getScoreboardLayoutTypeForSportId(sportId);

        // then
        assertThat(scoreboardLayout).isEqualTo(ScoreboardLayoutType.ScoreboardLayoutTypeFootball);
    }

    @Test
    public void getScoreboardLayoutTypeForSportId410() {
        // given
        long sportId = 410;

        // when
        ScoreboardLayoutType scoreboardLayout = sportConfigProvider.getScoreboardLayoutTypeForSportId(sportId);

        // then
        assertThat(scoreboardLayout).isEqualTo(ScoreboardLayoutType.ScoreboardLayoutTypeFootball);
    }
}
