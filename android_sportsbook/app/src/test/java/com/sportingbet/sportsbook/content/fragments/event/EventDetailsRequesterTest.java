package com.sportingbet.sportsbook.content.fragments.event;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.general.network.services.PublishingService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import retrofit.Callback;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class EventDetailsRequesterTest {

    private PublishingService publishingService;

    EventDetailsRequester eventDetailsRequester;

    @Before
    public void setUp() throws Exception {
        publishingService = Mockito.mock(PublishingService.class);

        eventDetailsRequester = new EventDetailsRequester(publishingService);
    }

    @Test
    public void requestEventWithSpecificBookTest() throws Exception {
        // given
        List<Long> bookIds = Lists.newArrayList(1L, 2L, 3L, 4L);

        // when
        eventDetailsRequester.requestEventWithSpecificBook(1, bookIds);

        // then
        Mockito.verify(publishingService).getEventWithSpecificBook(Matchers.eq(1L), Matchers.eq("1,2,3,4"), Mockito.any(Callback.class));
    }
}
