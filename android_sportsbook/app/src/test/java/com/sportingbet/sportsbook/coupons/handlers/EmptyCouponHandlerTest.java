package com.sportingbet.sportsbook.coupons.handlers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.model.event.Event;

import org.junit.Test;
import org.robolectric.annotation.Config;

import static org.fest.assertions.api.ANDROID.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class EmptyCouponHandlerTest extends AbstractCouponHandlerTest {

    @Override
    protected EventCouponHandler createCouponHandler() {
        return new EmptyCouponHandler();
    }

    @Test
    public void onCreateViewHolderHasTextViews() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View titleText = viewHolder.itemView.findViewById(R.id.title);
        assertThat(titleText).isNotNull().isInstanceOf(TextView.class);

        View subtitleText = viewHolder.itemView.findViewById(R.id.subtitle);
        assertThat(subtitleText).isNotNull().isInstanceOf(TextView.class);
    }

    @Test
    public void onBindViewHolderWithTestEvent1() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_1.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Bonzi, B v Sidorenko, A");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isEnabled()
                .hasCurrentTextColor(context.getResources().getColor(R.color.primaryColor))
                .isVisible();
    }

    @Test
    public void onBindViewHolderWithTestEvent2() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_2.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Bonzi, B v Sidorenko, A");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasCurrentTextColor(context.getResources().getColor(R.color.primaryColor))
                .hasText("6-3 1-1 40-40*");
    }

    @Test
    public void onBindViewHolderWithTestEventDisabled() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_disabled.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Simon, Tobias v Castelnuovo, Luca");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isDisabled()
                .hasText("6-7,6-3 2-2 0-0*");
    }

    @Test
    public void onBindViewHolderWithEventUpNextTest() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_upnext.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Burnley v Arsenal");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasText("Tomorrow 17:30 BST");
    }

    @Test
    public void onBindViewHolderWithNotRunningEventTest() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_not_running.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.title))
                .isVisible()
                .hasText("Yambol v Balkan");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasText("16:00 BST");
    }
}