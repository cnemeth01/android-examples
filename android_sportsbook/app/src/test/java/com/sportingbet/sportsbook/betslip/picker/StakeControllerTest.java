package com.sportingbet.sportsbook.betslip.picker;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.betslip.picker.value.StakeValueProvider;
import com.sportingbet.sportsbook.betslip.picker.value.PickerValueProvider;

import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.fest.assertions.api.Assertions.assertThat;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class StakeControllerTest {

    private PickerValueProvider stakeController;

    @Before
    public void setUp() throws Exception {
        stakeController = new StakeValueProvider(Lists.newArrayList(1.0, 2.0, 3.0, 4.0, 5.0), 2.0);
    }

    @Test
    public void hasNextValueForNull() throws Exception {
        // given
        // when
        boolean hasNextValue = stakeController.hasNextValue(null);

        // then
        assertThat(hasNextValue).isTrue();
    }

    @Test
    public void hasNextValueForAny() throws Exception {
        // given
        // when
        boolean hasNextValue = stakeController.hasNextValue(1.0);

        // then
        assertThat(hasNextValue).isTrue();
    }

    @Test
    public void hasNextValueForAny2() throws Exception {
        // given

        // when
        boolean hasNextValue = stakeController.hasNextValue(1.5);

        // then
        assertThat(hasNextValue).isTrue();
    }

    @Test
    public void hasNextValueForAny3() throws Exception {
        // given
        // when
        boolean hasNextValue = stakeController.hasNextValue(4.9);

        // then
        assertThat(hasNextValue).isTrue();
    }

    @Test
    public void hasNextValueForMax() throws Exception {
        // given
        // when
        boolean hasNextValue = stakeController.hasNextValue(5.0);

        // then
        assertThat(hasNextValue).isFalse();
    }

    @Test
    public void hasNextValueForAbove() throws Exception {
        // given
        // when
        boolean hasNextValue = stakeController.hasNextValue(6.0);

        // then
        assertThat(hasNextValue).isFalse();
    }

    @Test
    public void hasNextValueForAbove2() throws Exception {
        // given
        // when
        boolean hasNextValue = stakeController.hasNextValue(6000.0);

        // then
        assertThat(hasNextValue).isFalse();
    }

    @Test
    public void hasPreviousValue() throws Exception {
        // given
        // when
        Random random = new Random();

        // then
        for (int i = 0; i < 10000; i++) {
            boolean hasNextValue = stakeController.hasPreviousValue(random.nextDouble() * random.nextInt(1000000));
            assertThat(hasNextValue).isTrue();
        }
    }

    @Test
    public void getNextValueForNull() throws Exception {
        // given
        // when
        Double nextValue = stakeController.getNextValue(null);

        // then
        assertThat(nextValue).isEqualTo(2.0);
    }

    @Test
    public void getNextValueFor1() throws Exception {
        // given
        // when
        Double nextValue = stakeController.getNextValue(1.0);

        // then
        assertThat(nextValue).isEqualTo(2.0);
    }

    @Test
    public void getNextValueFor0() throws Exception {
        // given
        // when
        Double nextValue = stakeController.getNextValue(0.0);

        // then
        assertThat(nextValue).isEqualTo(1.0);
    }

    @Test
    public void getNextValueFor09() throws Exception {
        // given
        // when
        Double nextValue = stakeController.getNextValue(0.9);

        // then
        assertThat(nextValue).isEqualTo(1.0);
    }

    @Test
    public void getNextValueFor2() throws Exception {
        // given
        // when
        Double nextValue = stakeController.getNextValue(2.0);

        // then
        assertThat(nextValue).isEqualTo(3.0);
    }

    @Test
    public void getNextValueFor4() throws Exception {
        // given
        // when
        Double nextValue = stakeController.getNextValue(4.0);

        // then
        assertThat(nextValue).isEqualTo(5.0);
    }

    @Test
    public void getNextValueForMax() throws Exception {
        // given
        // when
        Double nextValue = stakeController.getNextValue(5.0);

        // then
        assertThat(nextValue).isEqualTo(5.0);
    }

    @Test
    public void getNextValueForAboveMax() throws Exception {
        // given
        // when
        Double nextValue = stakeController.getNextValue(5323.0);

        // then
        assertThat(nextValue).isEqualTo(5.0);
    }

    @Test
    public void getPreviousValueForNull() throws Exception {
        // given
        // when
        Double previousValue = stakeController.getPreviousValue(null);

        // then
        assertThat(previousValue).isEqualTo(2.0);
    }

    @Test
    public void getPreviousValueForMin() throws Exception {
        // given
        // when
        Double previousValue = stakeController.getPreviousValue(1.0);

        // then
        assertThat(previousValue).isNull();
    }

    @Test
    public void getPreviousValueForBelowMin() throws Exception {
        // given
        // when
        Double previousValue = stakeController.getPreviousValue(0.0);

        // then
        assertThat(previousValue).isNull();
    }

    @Test
    public void getPreviousValueForAboveMax() throws Exception {
        // given
        // when
        Double previousValue = stakeController.getPreviousValue(5323.0);

        // then
        assertThat(previousValue).isEqualTo(5.0);
    }

    @Test
    public void getPreviousValueForMax() throws Exception {
        // given
        // when
        Double previousValue = stakeController.getPreviousValue(5.0);

        // then
        assertThat(previousValue).isEqualTo(4.0);
    }

    @Test
    public void getPreviousValueFor3_9() throws Exception {
        // given
        // when
        Double previousValue = stakeController.getPreviousValue(3.9);

        // then
        assertThat(previousValue).isEqualTo(3.0);
    }

    @Test
    public void getPreviousValueFor3() throws Exception {
        // given
        // when
        Double previousValue = stakeController.getPreviousValue(3.0);

        // then
        assertThat(previousValue).isEqualTo(2.0);
    }

    @Test
    public void getPreviousValueFor2() throws Exception {
        // given
        // when
        Double previousValue = stakeController.getPreviousValue(2.0);

        // then
        assertThat(previousValue).isEqualTo(1.0);
    }
}
