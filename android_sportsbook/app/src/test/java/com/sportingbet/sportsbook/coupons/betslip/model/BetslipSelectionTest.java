package com.sportingbet.sportsbook.coupons.betslip.model;

import com.sportingbet.sportsbook.general.ResourceProvider;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.event.Event;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.robolectric.RuntimeEnvironment;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipSelectionTest {

    @Inject
    ResourceProvider resourceProvider;

    @Module(
            injects = {
                    BetslipSelectionTest.class,
            },
            includes = GeneralTestModule.class
    )
    class TestModule {

    }

    @Before
    public void setUp() throws Exception {
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);
    }

    @Test
    public void createNormalBetSelection() throws Exception {
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_upnext.json");
        Book book = event.getFirstBook();

        BetslipSelection selection = BetslipSelection.create(event, book, book.getFirstOffer().getSelections().get(0));
        assertThat(selection.getEventId()).isEqualTo(4140531);
        assertThat(selection.getBookId()).isEqualTo(87576244);
        assertThat(selection.getSelectionId()).isEqualTo(333444600);
        assertThat(selection.getFractionalOdds()).isEqualTo("5/1");
        assertThat(selection.getDecimalOdds()).isEqualTo("6");
        assertThat(selection.getAmericanOdds()).isEqualTo("500");
        assertThat(selection.getIsSPSelection()).isFalse();
    }

    @Test
    public void createSPBetSelection() throws Exception {
        Event event = Mockito.mock(Event.class);
        Mockito.when(event.getId()).thenReturn(10200L);
        Book book = resourceProvider.provideObjectFromResource(Book.class, "book_multiple_offers.json");

        BetslipSelection selection = BetslipSelection.create(event, book, book.getOffers().get(1).getSelections().get(0));
        assertThat(selection.getEventId()).isEqualTo(10200L);
        assertThat(selection.getBookId()).isEqualTo(89637973);
        assertThat(selection.getSelectionId()).isEqualTo(348887853);
        assertThat(selection.getIsSPSelection()).isTrue();
        assertThat(selection.getFractionalOdds()).isNull();
        assertThat(selection.getDecimalOdds()).isNull();
        assertThat(selection.getAmericanOdds()).isNull();
    }
}
