package com.sportingbet.sportsbook.betslip;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.SportsbookApplication;
import com.sportingbet.sportsbook.betslip.adapter.multiples.MultiplesAdapter;
import com.sportingbet.sportsbook.betslip.adapter.single.SingleAdapter;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.general.ResourceProvider;
import com.sportingbet.sportsbook.general.dagger.configuration.ApplicationModule;

import org.fest.assertions.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.fest.assertions.api.ANDROID.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class BetslipTabControllerTest {

    @Module(
            injects = BetslipTabControllerTest.class,
            addsTo = ApplicationModule.class
    )
    class TestModule {

        @Provides
        @Singleton
        ResourceProvider provideApplicationContext(ObjectMapper objectMapper) {
            return new ResourceProvider(objectMapper);
        }
    }

    @Inject
    ResourceProvider resourceProvider;

    private BetslipActivity betslipActivity;
    private RecyclerView recyclerView;

    private BetslipTabController betslipTabController;

    @Before
    public void setUp() throws Exception {
        SportsbookApplication application = (SportsbookApplication) RuntimeEnvironment.application;
        application.getApplicationGraph().plus(new TestModule()).inject(this);

        betslipActivity = Robolectric.setupActivity(BetslipActivity.class);
        recyclerView = Mockito.mock(RecyclerView.class);

        betslipTabController = new BetslipTabController(betslipActivity, recyclerView);
    }

    @Test
    public void createBetslipTabControllerTest() throws Exception {
        // given
        // when
        // then
        Assertions.assertThat(betslipTabController).isNotNull();
    }

    private View getViewById(int viewId) {
        return betslipActivity.findViewById(viewId);
    }

    @Test
    public void initializeButtonsTest() throws Exception {
        // given
        // when
        // then
        assertThat(getViewById(R.id.first_tab))
                .isNotNull()
                .isVisible()
                .isDisabled();

        assertThat(getViewById(R.id.second_tab))
                .isNotNull()
                .isVisible()
                .isDisabled();
    }

    @Test
    public void emptyBetslipInformationTest() throws Exception {
        // given
        BetslipInformation betslipInformation = Mockito.mock(BetslipInformation.class);
        Mockito.when(betslipInformation.isEmpty()).thenReturn(true);

        // when
        betslipTabController.setBetslipInformation(betslipInformation);

        // then
        assertThat(getViewById(android.R.id.empty))
                .isNotNull()
                .isVisible();
    }

    private void assertViewSelected(View view) {
        assertThat(view)
                .isNotNull()
                .isVisible()
                .isActivated()
                .isEnabled();
    }

    private void assertViewNotSelected(View view) {
        assertThat(view)
                .isNotNull()
                .isVisible()
                .isNotActivated()
                .isEnabled();
    }

    protected void assertViewDisabled(View view) {
        assertThat(view)
                .isNotNull()
                .isVisible()
                .isNotActivated()
                .isDisabled();
    }

    @Test
    public void singlesOnlyTest() throws Exception {
        // given
        BetslipInformation betslipInformation = resourceProvider.provideObjectFromResource(BetslipInformation.class, "betslip.json");

        // when
        betslipTabController.setBetslipInformation(betslipInformation);

        // then
        assertViewSelected(getViewById(R.id.first_tab));

        assertViewDisabled(getViewById(R.id.second_tab));

        Mockito.verify(recyclerView).setAdapter(Mockito.any(SingleAdapter.class));
    }

    @Test
    public void multiplesOnlyTest() throws Exception {
        // given
        BetslipInformation betslipInformation = resourceProvider.provideObjectFromResource(BetslipInformation.class, "betslip_multiple.json");

        // when
        betslipTabController.setBetslipInformation(betslipInformation);

        // then
        assertViewNotSelected(getViewById(R.id.first_tab));

        assertViewSelected(getViewById(R.id.second_tab));

        Mockito.verify(recyclerView).setAdapter(Mockito.any(MultiplesAdapter.class));
    }

    @Test
    public void singleThenSingleTest() throws Exception {
        // given
        BetslipInformation betslipInformation = resourceProvider.provideObjectFromResource(BetslipInformation.class, "betslip.json");

        // when
        betslipTabController.setBetslipInformation(betslipInformation);
        betslipTabController.setBetslipInformation(betslipInformation);

        // then
        assertViewSelected(getViewById(R.id.first_tab));
        assertViewDisabled(getViewById(R.id.second_tab));
        Mockito.verify(recyclerView).setAdapter(Mockito.any(SingleAdapter.class));
    }

    @Test
    public void multipleThenSwitchToSingleThenMultipleTest() throws Exception {
        // given
        BetslipInformation betslipInformation = resourceProvider.provideObjectFromResource(BetslipInformation.class, "betslip_multiple.json");

        // when
        betslipTabController.setBetslipInformation(betslipInformation);
        getViewById(R.id.first_tab).performClick();
        betslipTabController.setBetslipInformation(betslipInformation);

        // then
        assertViewSelected(getViewById(R.id.first_tab));
        assertViewNotSelected(getViewById(R.id.second_tab));
    }

    @Test
    public void multipleThenSingleTest() throws Exception {
        // given
        BetslipInformation betslipInformation = resourceProvider.provideObjectFromResource(BetslipInformation.class, "betslip_multiple.json");
        BetslipInformation betslipInformationSecond = resourceProvider.provideObjectFromResource(BetslipInformation.class, "betslip.json");

        // when
        betslipTabController.setBetslipInformation(betslipInformation);
        betslipTabController.setBetslipInformation(betslipInformationSecond);

        // then
        assertViewSelected(getViewById(R.id.first_tab));
        assertViewDisabled(getViewById(R.id.second_tab));
    }

    @Test
    public void multipleThenSwitchToSingleThenSingleTest() throws Exception {
        // given
        BetslipInformation betslipInformation = resourceProvider.provideObjectFromResource(BetslipInformation.class, "betslip_multiple.json");
        BetslipInformation betslipInformationSecond = resourceProvider.provideObjectFromResource(BetslipInformation.class, "betslip.json");

        // when
        betslipTabController.setBetslipInformation(betslipInformation);
        getViewById(R.id.first_tab).performClick();
        betslipTabController.setBetslipInformation(betslipInformationSecond);

        // then
        assertViewSelected(getViewById(R.id.first_tab));
        assertViewDisabled(getViewById(R.id.second_tab));
    }

    @Test
    public void singleThenMultiplesTest() throws Exception {
        // given
        BetslipInformation betslipInformation = resourceProvider.provideObjectFromResource(BetslipInformation.class, "betslip.json");
        BetslipInformation betslipInformationSecond = resourceProvider.provideObjectFromResource(BetslipInformation.class, "betslip_multiple.json");

        // when
        betslipTabController.setBetslipInformation(betslipInformation);
        betslipTabController.setBetslipInformation(betslipInformationSecond);

        // then
        assertViewNotSelected(getViewById(R.id.first_tab));
        assertViewSelected(getViewById(R.id.second_tab));
    }
}
