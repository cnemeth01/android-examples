package com.sportingbet.sportsbook.home.navigation;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;
import com.sportingbet.sportsbook.home.navigation.fragments.HomeNavigationFragment;
import com.sportingbet.sportsbook.home.navigation.fragments.InPlayNavigationFragment;
import com.sportingbet.sportsbook.home.navigation.fragments.SportNavigationFragment;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;
import com.sportingbet.sportsbook.model.Sport;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class NavigationItemFactoryTest {

    @Module(
            injects = NavigationItemFactoryTest.class,
            includes = {
                    GeneralTestModule.class,
            },
            overrides = true
    )
    class TestModule {

    }

    @Inject
    NavigationItemFactory navigationItemFactory;

    @Before
    public void setUp() throws Exception {
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);
    }

    @Test
    public void objectSuccessfullyCreated() {
        assertThat(navigationItemFactory).isNotNull();
    }

    @Test
    public void getNavigationItemForInPlay() {
        // given
        int itemId = BuildConfig.INPLAY_ITEM_ID;

        // when
        NavigationItem navigationItem = navigationItemFactory.getNavigationItemForId(itemId);

        // then
        assertThat(navigationItem).isNotNull();
        assertThat(navigationItem.getId()).isEqualTo(BuildConfig.INPLAY_ITEM_ID);
        assertThat(navigationItem.getName()).isEqualTo("In Play");
    }

    @Test
    public void getNavigationItemForHome() {
        // given
        int itemId = BuildConfig.HOME_ITEM_ID;

        // when
        NavigationItem navigationItem = navigationItemFactory.getNavigationItemForId(itemId);

        // then
        assertThat(navigationItem).isNotNull();
        assertThat(navigationItem.getId()).isEqualTo(BuildConfig.HOME_ITEM_ID);
        assertThat(navigationItem.getName()).isEqualTo("Home");
    }

    @Test(expected = RuntimeException.class)
    public void getNavigationItemForUndefinedItem() {
        // given
        int itemId = -1002;

        // when
        navigationItemFactory.getNavigationItemForId(itemId);
    }

    @Test
    public void createFragmentForFootballItem() {
        // given
        Sport sport = Mockito.mock(Sport.class);
        Mockito.when(sport.getId()).thenReturn(102L);
        Mockito.when(sport.getName()).thenReturn("Football");

        // when
        AbstractNavigationFragment fragment = navigationItemFactory.createFragmentForNavigationItem(sport);

        // then
        assertThat(fragment).isNotNull().isInstanceOf(SportNavigationFragment.class);
        assertThat(fragment.getIdentifier()).isEqualTo(102);
        assertThat(fragment.getTitle()).isEqualTo("Football");
    }

    @Test
    public void createFragmentForBasketballItem() {
        // given
        Sport sport = Mockito.mock(Sport.class);
        Mockito.when(sport.getId()).thenReturn(190L);
        Mockito.when(sport.getName()).thenReturn("Basketball");

        // when
        AbstractNavigationFragment fragment = navigationItemFactory.createFragmentForNavigationItem(sport);

        // then
        assertThat(fragment).isNotNull().isInstanceOf(SportNavigationFragment.class);
        assertThat(fragment.getIdentifier()).isEqualTo(190);
        assertThat(fragment.getTitle()).isEqualTo("Basketball");
    }

    @Test
    public void createFragmentForHome() {
        // given
        NavigationItem navigationItem = new NavigationItem(BuildConfig.HOME_ITEM_ID, "Home");

        // when
        AbstractNavigationFragment fragment = navigationItemFactory.createFragmentForNavigationItem(navigationItem);

        // then
        assertThat(fragment.getIdentifier()).isEqualTo(BuildConfig.HOME_ITEM_ID);
        assertThat(fragment.getTitle()).isEqualTo("Home");
        assertThat(fragment).isInstanceOf(HomeNavigationFragment.class);
    }

    @Test
    public void createFragmentForInPlay() {
        // given
        NavigationItem navigationItem = new NavigationItem(BuildConfig.INPLAY_ITEM_ID, "In Play");

        // when
        AbstractNavigationFragment fragment = navigationItemFactory.createFragmentForNavigationItem(navigationItem);

        // then
        assertThat(fragment).isNotNull().isInstanceOf(InPlayNavigationFragment.class);
        assertThat(fragment.getIdentifier()).isEqualTo(BuildConfig.INPLAY_ITEM_ID);
        assertThat(fragment.getTitle()).isEqualTo("In Play");
    }
}
