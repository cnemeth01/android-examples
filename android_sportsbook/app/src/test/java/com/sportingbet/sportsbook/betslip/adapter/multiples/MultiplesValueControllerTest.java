package com.sportingbet.sportsbook.betslip.adapter.multiples;

import com.sportingbet.sportsbook.betslip.adapter.BetTypeValueControllerTest;
import com.sportingbet.sportsbook.betslip.adapter.BetslipValueProvider;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MultiplesValueControllerTest extends BetTypeValueControllerTest {

    @Override
    protected BetslipValueProvider createBetTypeValueController(BetsValueContainer betsValueContainer) {
        return new MultiplesValueController(betsValueContainer);
    }

    @Test
    public void totalPaymentNoInputTest() throws Exception {
        // given
        BetslipInformation betslipInformation = createBetslipInformation("betslip_multiple.json");
        getBetTypeValueController().setup(betslipInformation);
        StakePicker stakePicker = createStakePicker();
        getBetTypeValueController().registerPickerAdapter(stakePicker, null, betslipInformation.getBetTypeForId("LUCKY15-365853453,366968331,366926493,366986409"));

        // when
        String totalPayout = betslipValueProvider.getTotalPayout();
        double totalStake = betslipValueProvider.getTotalStake();
        boolean hasTotalPayout = betslipValueProvider.hasTotalPayout();

        // then
        assertThat(hasTotalPayout).isFalse();
        assertThat(totalStake).isZero();
        assertThat(totalPayout).isNotEmpty().isEqualTo("0.0");
    }

    @Test
    public void totalPaymentTest() throws Exception {
        // given
        BetslipInformation betslipInformation = createBetslipInformation("betslip_multiple.json");
        getBetTypeValueController().setup(betslipInformation);
        StakePicker stakePicker = createStakePicker();
        getBetTypeValueController().registerPickerAdapter(stakePicker, null, betslipInformation.getBetTypeForId("LUCKY15-365853453,366968331,366926493,366986409"));

        // when
        stakePicker.getButtonPlus().performClick();
        String totalPayout = betslipValueProvider.getTotalPayout();
        double totalStake = betslipValueProvider.getTotalStake();
        boolean hasTotalPayout = betslipValueProvider.hasTotalPayout();

        // then
        assertThat(hasTotalPayout).isTrue();
        assertThat(totalStake).isEqualTo(150);
        assertThat(totalPayout).isNotEmpty().isEqualTo("498.9063636363636");
    }

    @Test
    public void totalPaymentWithSPNoInputTest() throws Exception {
        // given
        BetslipInformation betslipInformation = createBetslipInformation("betslip_sp.json");
        getBetTypeValueController().setup(betslipInformation);
        StakePicker stakePicker = createStakePicker();
        getBetTypeValueController().registerPickerAdapter(stakePicker, null, betslipInformation.getBetTypeForId("ACCUMULATOR-381164336,381162946,381166975,380693304"));

        // when
        String totalPayout = betslipValueProvider.getTotalPayout();
        double totalStake = betslipValueProvider.getTotalStake();
        boolean hasTotalPayout = betslipValueProvider.hasTotalPayout();

        // then
        assertThat(hasTotalPayout).isFalse();
        assertThat(totalStake).isZero();
        assertThat(totalPayout).isNotEmpty().isEqualTo("0.0");
    }

    @Test
    public void totalPaymentWithSPTest() throws Exception {
        // given
        BetslipInformation betslipInformation = createBetslipInformation("betslip_sp.json");
        getBetTypeValueController().setup(betslipInformation);
        StakePicker stakePicker = createStakePicker();
        getBetTypeValueController().registerPickerAdapter(stakePicker, null, betslipInformation.getBetTypeForId("ACCUMULATOR-381164336,381162946,381166975,380693304"));

        // when
        stakePicker.getButtonPlus().performClick();
        String totalPayout = betslipValueProvider.getTotalPayout();
        double totalStake = betslipValueProvider.getTotalStake();
        boolean hasTotalPayout = betslipValueProvider.hasTotalPayout();

        // then
        assertThat(hasTotalPayout).isTrue();
        assertThat(totalStake).isEqualTo(10);
        assertThat(totalPayout).isNotEmpty().isEqualTo("SP");
    }
}