package com.sportingbet.sportsbook.coupons.handlers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.assertions.CouponLayoutAssert;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.model.event.Event;

import org.junit.Test;
import org.robolectric.annotation.Config;

import static org.fest.assertions.api.ANDROID.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class ThreeBallCouponHandlerTest extends AbstractCouponHandlerTest {

    @Override
    protected EventCouponHandler createCouponHandler() {
        return new ThreeBallCouponHandler(betslipCouponDelegate);
    }

    @Test
    public void onCreateViewHolderHasTextViews() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View firstTeamText = viewHolder.itemView.findViewById(R.id.first_player);
        assertThat(firstTeamText).isNotNull().isInstanceOf(TextView.class);

        View secondTeamText = viewHolder.itemView.findViewById(R.id.second_player);
        assertThat(secondTeamText).isNotNull().isInstanceOf(TextView.class);

        View thirdTeamText = viewHolder.itemView.findViewById(R.id.third_player);
        assertThat(thirdTeamText).isNotNull().isInstanceOf(TextView.class);
    }

    @Test
    public void onCreateViewHolderHasButtons() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View firstButton = viewHolder.itemView.findViewById(R.id.first_button);
        assertThat(firstButton).isNotNull().isInstanceOf(CouponLayout.class);
        View secondButton = viewHolder.itemView.findViewById(R.id.second_button);
        assertThat(secondButton).isNotNull().isInstanceOf(CouponLayout.class);
        View thirdButton = viewHolder.itemView.findViewById(R.id.third_button);
        assertThat(thirdButton).isNotNull().isInstanceOf(CouponLayout.class);
    }

    @Test
    public void onBindViewHolderWithThreeBallEvent() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_three_ball.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_player))
                .isVisible()
                .hasText("Robert Allenby");

        assertThat(getView(viewHolder, R.id.second_player))
                .isVisible()
                .hasText("Stuart Appleby");

        assertThat(getView(viewHolder, R.id.third_player))
                .isVisible()
                .hasText("Woody Austin");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("4/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("4/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isEnabled()
                .hasText("5/1");
    }

    @Test
    public void onBindViewHolderWithThreeBallEvent2() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_three_ball_2.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_player))
                .isVisible()
                .hasText("Dustin Johnson");

        assertThat(getView(viewHolder, R.id.second_player))
                .isVisible()
                .hasText("Hee Kyung Seo");

        assertThat(getView(viewHolder, R.id.third_player))
                .isVisible()
                .hasText("Westwood/Jamieson");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("5/3");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("45/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isEnabled()
                .hasText("40/1");
    }

    @Test
    public void onBindViewHolderWithThreeBallEventNotOffered() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_three_ball_not_offered.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_player))
                .isVisible()
                .hasText("Dustin Johnson");

        assertThat(getView(viewHolder, R.id.second_player))
                .isVisible()
                .hasText("Hee Kyung Seo");

        assertThat(getView(viewHolder, R.id.third_player))
                .isVisible()
                .hasText("Westwood/Jamieson");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("5/3");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("45/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isDisabled()
                .hasText("SUSP");
    }

    @Test
    public void onBindViewHolderWithThreeBallEventDisabled() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_three_ball_disabled.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_player))
                .isVisible()
                .hasText("Robert Allenby");

        assertThat(getView(viewHolder, R.id.second_player))
                .isVisible()
                .hasText("Stuart Appleby");

        assertThat(getView(viewHolder, R.id.third_player))
                .isVisible()
                .hasText("Woody Austin");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isDisabled()
                .hasText("4/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isDisabled()
                .hasText("4/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isDisabled()
                .hasText("NO");
    }

    @Test
    public void onBindViewHolderWithThreeBallEventDisabled2() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_three_ball_disabled_2.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isDisabled()
                .hasText("4/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isDisabled()
                .hasText("4/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isDisabled()
                .hasText("NO");
    }

    @Test
    public void onBindViewHolderWithThreeBallEventDisabled3() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_three_ball_disabled_3.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isDisabled()
                .hasText("4/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isDisabled()
                .hasText("4/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isDisabled()
                .hasText("NO");
    }
}