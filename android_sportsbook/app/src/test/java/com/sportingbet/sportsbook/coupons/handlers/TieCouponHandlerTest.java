package com.sportingbet.sportsbook.coupons.handlers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.assertions.CouponLayoutAssert;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.model.event.Event;

import org.junit.Test;
import org.robolectric.annotation.Config;

import static org.fest.assertions.api.ANDROID.assertThat;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class TieCouponHandlerTest extends AbstractCouponHandlerTest {

    @Override
    protected EventCouponHandler createCouponHandler() {
        return new TieCouponHandler(betslipCouponDelegate);
    }

    @Test
    public void onCreateViewHolderHasTextViews() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View firstTeamText = viewHolder.itemView.findViewById(R.id.first_team);
        assertThat(firstTeamText).isNotNull().isInstanceOf(TextView.class);
        View firstTeamScoreText = viewHolder.itemView.findViewById(R.id.first_team_score);
        assertThat(firstTeamScoreText).isNotNull().isInstanceOf(TextView.class);

        View subtitleText = viewHolder.itemView.findViewById(R.id.subtitle);
        assertThat(subtitleText).isNotNull().isInstanceOf(TextView.class);

        View secondTeamText = viewHolder.itemView.findViewById(R.id.second_team);
        assertThat(secondTeamText).isNotNull().isInstanceOf(TextView.class);
        View secondTeamScoreText = viewHolder.itemView.findViewById(R.id.second_team_score);
        assertThat(secondTeamScoreText).isNotNull().isInstanceOf(TextView.class);
    }

    @Test
    public void onCreateViewHolderHasButtons() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View firstButton = viewHolder.itemView.findViewById(R.id.first_button);
        assertThat(firstButton).isNotNull().isInstanceOf(CouponLayout.class);
        View secondButton = viewHolder.itemView.findViewById(R.id.second_button);
        assertThat(secondButton).isNotNull().isInstanceOf(CouponLayout.class);
        View thirdButton = viewHolder.itemView.findViewById(R.id.third_button);
        assertThat(thirdButton).isNotNull().isInstanceOf(CouponLayout.class);
    }

    @Test
    public void onBindViewHolderWithTestEvent1() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_1.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Liverpool");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Arsenal");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .hasText("1st Half");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isVisible()
                .hasText("0");

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isVisible()
                .hasText("5");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("1/5");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("3/10");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isEnabled()
                .hasText("1/4");
    }

    @Test
    public void onBindViewHolderWithTestEvent2() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_2.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Dinamo Moscow");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("CSKA Kiev");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .hasText("Tommorow");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isNotVisible();

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("1/750");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("1/3");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isEnabled()
                .hasText("1/2");
    }

    @Test
    public void onBindViewHolderWithTestEvent3() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_3.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Liverpool");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Milan");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isNotVisible();

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isDisabled()
                .hasText("NO");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("4/6");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isEnabled()
                .hasText("7/10");
    }

    @Test
    public void onBindViewHolderWithTestEventNotOffered() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_not_offered.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Gungoren");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Bayburt Genclikspor");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .hasText("89' 2nd Half");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isVisible()
                .hasText("0");

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isVisible()
                .hasText("3");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("100/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("20/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isDisabled()
                .hasText("NO");
    }

    @Test
    public void onBindViewHolderWithTestEventDisabled() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_disabled.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isDisabled()
                .hasText("89' 2nd Half");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isVisible()
                .isDisabled()
                .hasText("0");

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isVisible()
                .isDisabled()
                .hasText("3");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isDisabled()
                .hasText("100/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isDisabled()
                .hasText("20/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isDisabled()
                .hasText("NO");
    }

    @Test
    public void onBindViewHolderWithTestEventDisabled2() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_disabled_2.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Shkola Myacha");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Zelenograd");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isDisabled()
                .hasText("10:00 BST");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isGone();

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isGone();

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isDisabled()
                .hasText("11/10");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isDisabled()
                .hasText("14/5");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isDisabled()
                .hasText("31/20");
    }


    @Test
    public void onBindViewHolderWithEventUpNextTest() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_upnext.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Burnley");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Arsenal");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasText("Tomorrow 17:30 BST");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isNotVisible();

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("5/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("16/5");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isEnabled()
                .hasText("4/7");
    }

    @Test
    public void onBindViewHolderWithNotRunningEventTest() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_tie_not_running.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Yambol");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Balkan");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasText("16:00 BST");

        assertThat(getView(viewHolder, R.id.first_team_score))
                .isNotVisible();

        assertThat(getView(viewHolder, R.id.second_team_score))
                .isNotVisible();

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("7/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("16/1");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.third_button))
                .isVisible()
                .isEnabled()
                .hasText("1/14");
    }
}
