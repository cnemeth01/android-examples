package com.sportingbet.sportsbook.general.dagger.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.sportingbet.sportsbook.general.ResourceProvider;

import org.mockito.Mockito;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Module(
        includes = {
                ApplicationModule.class,
        },
        overrides = true,
        library = true
)
public class GeneralTestModule {

    @Provides
    @Singleton
    ResourceProvider provideApplicationContext(ObjectMapper objectMapper) {
        return new ResourceProvider(objectMapper);
    }

    @Provides
    @Singleton
    EventBus provideEventBus() {
        return Mockito.mock(EventBus.class);
    }
}
