package com.sportingbet.sportsbook.betslip.adapter;

import android.content.Context;

import com.sportingbet.sportsbook.betslip.adapter.multiples.MultiplesValueControllerTest;
import com.sportingbet.sportsbook.betslip.adapter.single.SingleValueControllerTest;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.general.ResourceProvider;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public abstract class BetTypeValueControllerTest {

    protected BetslipValueProvider betslipValueProvider;

    @Module(
            injects = {
                    MultiplesValueControllerTest.class,
                    SingleValueControllerTest.class
            },
            includes = {
                    GeneralTestModule.class,
            },
            overrides = true
    )
    class TestModule {
    }

    @Inject
    Context context;

    @Inject
    ResourceProvider resourceProvider;

    protected BetTypeValueController getBetTypeValueController() {
        return (BetTypeValueController) betslipValueProvider;
    }

    protected StakePicker createStakePicker() {
        return new StakePicker(context, null);
    }

    protected BetslipInformation createBetslipInformation(String resourceName) {
        return resourceProvider.provideObjectFromResource(BetslipInformation.class, resourceName);
    }

    @Before
    public void setUp() throws Exception {
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);

        betslipValueProvider = createBetTypeValueController(Mockito.mock(BetsValueContainer.class));
        getBetTypeValueController().setDelegate(Mockito.mock(BetTypeValueController.Delegate.class));
    }

    protected abstract BetslipValueProvider createBetTypeValueController(BetsValueContainer betsValueContainer);

    @Test
    public void createObjectTest() throws Exception {
        assertThat(betslipValueProvider).isNotNull();
    }

    @Test
    public void emptyTotalPayment() throws Exception {
        // given
        // when
        boolean hasTotalPayout = betslipValueProvider.hasTotalPayout();
        String totalPayout = betslipValueProvider.getTotalPayout();
        double totalStake = betslipValueProvider.getTotalStake();

        // then
        assertThat(hasTotalPayout).isFalse();
        assertThat(totalPayout).isNotNull().isNotEmpty().isEqualTo("0.0");
        assertThat(totalStake).isNotNull().isZero();
    }
}
