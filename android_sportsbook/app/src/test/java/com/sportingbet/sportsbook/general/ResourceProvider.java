package com.sportingbet.sportsbook.general;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sportingbet.sportsbook.model.Sport;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ResourceProvider {

    private final ObjectMapper objectMapper;

    public ResourceProvider(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private InputStream getStreamFromResource(String name) throws FileNotFoundException {
        return new FileInputStream("src/test/resources/" + name);
    }

    public <T> T provideObjectFromResource(Class<T> type, String resourceName) {
        try {
            return objectMapper.readValue(getStreamFromResource(resourceName), type);
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    private <T> T provideObjectFromResource(String resourceName, TypeReference typeReference) {
        try {
            return objectMapper.readValue(getStreamFromResource(resourceName), typeReference);
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    public List<Sport> provideSports() {
        TypeReference<List<Sport>> typeReference = new TypeReference<List<Sport>>() {
        };
        return provideObjectFromResource("sports.json", typeReference);
    }
}
