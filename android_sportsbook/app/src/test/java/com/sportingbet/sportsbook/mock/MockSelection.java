package com.sportingbet.sportsbook.mock;

import com.sportingbet.sportsbook.model.book.price.Price;
import com.sportingbet.sportsbook.model.book.selection.Selection;

import lombok.Getter;

public class MockSelection extends Selection {

    @Getter
    private String name;

    @Getter
    private Integer templateNumber;

    @Getter
    private Price price = new MockPrice();

    private String displayOdds;

    private boolean isOffered;

    public MockSelection(String name, Integer templateNumber) {
        this(name, "");
        this.templateNumber = templateNumber;
    }

    public MockSelection(String name, String displayOds) {
        this(name, displayOds, true);
    }

    public MockSelection(String name, String displayOds, boolean isOffered) {
        this.name = name;
        this.displayOdds = displayOds;
        this.isOffered = isOffered;
    }

    private class MockPrice extends Price {

        @Override
        public String getDisplayOdds() {
            return displayOdds;
        }

        @Override
        public boolean isOffered() {
            return isOffered;
        }

        @Override
        public String getNotOfferedReason() {
            return "NO";
        }
    }
}



