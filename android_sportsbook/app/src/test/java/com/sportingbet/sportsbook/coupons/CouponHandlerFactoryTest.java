package com.sportingbet.sportsbook.coupons;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.coupons.handlers.NoTieCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.OverUnderCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.SingleCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.ThreeBallCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.TieCouponHandler;
import com.sportingbet.sportsbook.coupons.model.BookCoupon;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.coupons.model.SingleCoupon;
import com.sportingbet.sportsbook.general.ResourceProvider;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;
import com.sportingbet.sportsbook.mock.MockSelection;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.Offer;
import com.sportingbet.sportsbook.model.book.WebCouponTemplate;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.sports.configuration.CouponType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class CouponHandlerFactoryTest {

    @Inject
    ResourceProvider resourceProvider;

    @Inject
    CouponHandlerFactory couponHandlerFactory;

    @Module(
            injects = CouponHandlerFactoryTest.class,
            includes = {
                    GeneralTestModule.class,
            },
            overrides = true
    )
    class TestModule {
    }

    @Before
    public void setUp() throws Exception {
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);
    }

    @Test
    public void objectBuilt() {
        assertThat(couponHandlerFactory).isNotNull();
    }

    @Test
    public void getItemTypeForTemplate() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPNTEAMTIE;

        // when
        int itemType = couponHandlerFactory.getItemTypeForTemplate(webCouponTemplate);

        // then
        assertThat(itemType).isEqualTo(CouponType.CouponTypeTie.value());
    }

    // getHandlerForItemType
    @Test
    public void getHandlerForItemTypeTie() throws Exception {
        // given
        int itemType = CouponType.CouponTypeTie.value();

        // when
        CouponHandler handlerForTemplate = couponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(handlerForTemplate).isNotNull().isInstanceOf(TieCouponHandler.class);
    }

    @Test
    public void getHandlerForItemTypeForNoTie() {
        // given
        int itemType = CouponType.CouponTypeNoTie.value();

        // when
        CouponHandler handlerForTemplate = couponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(handlerForTemplate).isNotNull().isInstanceOf(NoTieCouponHandler.class);
    }

    @Test
    public void getHandlerForItemTypeForOverUnder() {
        // given
        int itemType = CouponType.CouponTypeOverUnder.value();

        // when
        CouponHandler handlerForTemplate = couponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(handlerForTemplate).isNotNull().isInstanceOf(OverUnderCouponHandler.class);
    }

    @Test
    public void getHandlerForItemTypeForTieAlternative() {
        // given
        int itemType = CouponType.CouponTypeTieAlternative.value();

        // when
        CouponHandler handlerForTemplate = couponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(handlerForTemplate).isNull();
    }

    @Test
    public void getHandlerForItemTypeForNoTieAlternative() {
        // given
        int itemType = CouponType.CouponTypeNoTieAlternative.value();

        // when
        CouponHandler handlerForTemplate = couponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(handlerForTemplate).isNull();
    }

    @Test
    public void getHandlerForItemTypeForThreeBall() {
        // given
        int itemType = CouponType.CouponTypeThreeBall.value();

        // when
        CouponHandler handlerForTemplate = couponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(handlerForTemplate).isNotNull().isInstanceOf(ThreeBallCouponHandler.class);
    }

    @Test
    public void getHandlerForItemTypeForEmpty() {
        // given
        int itemType = CouponType.CouponTypeEmpty.value();

        // when
        CouponHandler handlerForTemplate = couponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(handlerForTemplate).isNotNull().isInstanceOf(SingleCouponHandler.class);
    }

    @Test
    public void getHandlerForItemTypeForSingle() {
        // given
        int itemType = CouponType.CouponTypeSingle.value();

        // when
        CouponHandler handlerForTemplate = couponHandlerFactory.getHandlerForItemType(itemType);

        // then
        assertThat(handlerForTemplate).isNotNull().isInstanceOf(SingleCouponHandler.class);
    }

    // getCouponsForBook
    private Event createEvent() {
        return Mockito.mock(Event.class);
    }

    private Book createBook(WebCouponTemplate couponTemplate, Integer... templateNumbers) {
        Book book = Mockito.mock(Book.class);
        Mockito.when(book.getWebCouponTemplate()).thenReturn(couponTemplate);
        Mockito.when(book.getSelectionCount()).thenReturn(templateNumbers.length);
        Offer offer = Mockito.mock(Offer.class);
        Mockito.when(offer.getSelections()).thenReturn(createSelections(templateNumbers));
        Mockito.when(book.getFirstOffer()).thenReturn(offer);
        Mockito.when(book.getOffers()).thenReturn(Lists.newArrayList(offer));
        return book;
    }

    private List<Selection> createSelections(Integer... templateNumbers) {
        List<Selection> selections = Lists.newArrayList();
        for (int i = 0; i < templateNumbers.length; i++) {
            selections.add(new MockSelection("" + i, templateNumbers[i]));
        }
        return selections;
    }

    private void validateBookCoupon(WebCouponTemplate webCouponTemplate, Coupon coupon) {
        assertThat(coupon).isExactlyInstanceOf(BookCoupon.class);
        assertThat(coupon.getWebCouponTemplate()).isEqualTo(webCouponTemplate);
    }

    private void validateSingleCoupon(Coupon coupon) {
        assertThat(coupon).isExactlyInstanceOf(SingleCoupon.class);
        assertThat(coupon.getWebCouponTemplate()).isEqualTo(WebCouponTemplate.UNKNOWN);
    }

    @Test
    public void getCouponsForBookCPNTEAMTIE() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPNTEAMTIE;
        Book book = createBook(webCouponTemplate, 1, 2, 0);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(1);
        validateBookCoupon(webCouponTemplate, coupons.get(0));
    }

    @Test
    public void getCouponsForBookCPNPLAYTIE() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPNPLAYTIE;
        Book book = createBook(webCouponTemplate, 1, 2, 0);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(1);
        validateBookCoupon(webCouponTemplate, coupons.get(0));
    }

    @Test
    public void getCouponsForBookCPNPLAYTIEMissingSelection() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPNTEAMTIE;
        Book book = createBook(webCouponTemplate, 1, 2);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(2);
        validateSingleCoupon(coupons.get(0));
        validateSingleCoupon(coupons.get(1));
    }

    @Test
    public void getCouponsForBookCPNPLAYTIEMissingTemplateNumbers() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPNPLAYTIE;
        Book book = createBook(webCouponTemplate, null, null, null);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(3);
        validateSingleCoupon(coupons.get(0));
        validateSingleCoupon(coupons.get(1));
        validateSingleCoupon(coupons.get(2));
    }

    @Test
    public void getCouponsForBookCPNTEAMTIEMissingTemplateNumbers() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPNTEAMTIE;
        Book book = createBook(webCouponTemplate, null, null, null);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(3);
        validateSingleCoupon(coupons.get(0));
        validateSingleCoupon(coupons.get(1));
        validateSingleCoupon(coupons.get(2));
    }

    @Test
    public void getCouponsForBookCPN2BALLSMissingTemplateNumbers() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPN2BALLS;
        Book book = createBook(webCouponTemplate, null, null, null);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(3);
        validateSingleCoupon(coupons.get(0));
        validateSingleCoupon(coupons.get(1));
        validateSingleCoupon(coupons.get(2));
    }

    @Test
    public void getCouponsForBookCPN2BALLS() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPN2BALLS;
        Book book = createBook(webCouponTemplate, 1, 2, 0);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(1);
        validateBookCoupon(webCouponTemplate, coupons.get(0));
    }

    @Test
    public void getCouponsForBookCPNTEAMNOTIE() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPNTEAMNOTIE;
        Book book = createBook(webCouponTemplate, null, null);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(1);
        validateBookCoupon(webCouponTemplate, coupons.get(0));
    }

    @Test
    public void getCouponsForBookCPNPLAYNOTIE() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPNPLAYNOTIE;
        Book book = createBook(webCouponTemplate, null, null);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(1);
        validateBookCoupon(webCouponTemplate, coupons.get(0));
    }

    @Test
    public void getCouponsForBookCPNTOTALS() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPNTOTALS;
        Book book = createBook(webCouponTemplate, null, null);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(1);
        validateBookCoupon(webCouponTemplate, coupons.get(0));
    }

    @Test
    public void getCouponsForBookCPNOVERUNDER() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPNTOTALS;
        Book book = createBook(webCouponTemplate, null, null);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(1);
        validateBookCoupon(webCouponTemplate, coupons.get(0));
    }

    @Test
    public void getCouponsForBookCPN3BALLS() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPN3BALLS;
        Book book = createBook(webCouponTemplate, 1, 2, 0);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(1);
        Coupon coupon = coupons.get(0);
        validateBookCoupon(webCouponTemplate, coupon);
    }

    @Test
    public void getCouponsForBookCPN3BALLSMissingSelection() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.CPN3BALLS;
        Book book = createBook(webCouponTemplate, null, null);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(2);
        validateSingleCoupon(coupons.get(0));
        validateSingleCoupon(coupons.get(1));
    }

    @Test
    public void getCouponsForBookUNKNOWN() throws Exception {
        // given
        WebCouponTemplate webCouponTemplate = WebCouponTemplate.UNKNOWN;
        Book book = createBook(webCouponTemplate, 1, 2, 0);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(createEvent(), book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(3);
        validateSingleCoupon(coupons.get(0));
        validateSingleCoupon(coupons.get(1));
        validateSingleCoupon(coupons.get(2));
    }

    @Test
    public void getCouponsForBookMultipleOffers() throws Exception {
        // given
        Book book = resourceProvider.provideObjectFromResource(Book.class, "book_multiple_offers.json");
        Event event = Mockito.mock(Event.class);
        Mockito.when(event.getFirstBook()).thenReturn(book);

        // when
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(event, book);

        // then
        assertThat(coupons).isNotNull().isNotEmpty().hasSize(10);
        validateDoubleCoupon(coupons.get(0), "Falcon's Song");
        validateDoubleCoupon(coupons.get(1), "Frenzified");
        validateDoubleCoupon(coupons.get(2), "Mill Springs");
        validateDoubleCoupon(coupons.get(3), "Scots Fern");
        validateDoubleCoupon(coupons.get(4), "Flamme Fantastique");
        validateDoubleCoupon(coupons.get(5), "Regal Ways");
        validateDoubleCoupon(coupons.get(6), "Sant'elia");
        validateDoubleCoupon(coupons.get(7), "Snow Cover");
        validateDoubleCoupon(coupons.get(8), "Lara Lipton");
        validateSingleCoupon(coupons.get(9));
    }

    private void validateDoubleCoupon(Coupon coupon, String name) {
        validateSingleCoupon(coupon);
        assertThat(coupon.getSelections()).hasSize(2);
        assertThat(coupon.getSelections().get(0).getName()).isNotNull().isEqualTo(name);
    }

}
