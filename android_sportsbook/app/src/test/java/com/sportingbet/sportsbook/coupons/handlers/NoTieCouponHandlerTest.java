package com.sportingbet.sportsbook.coupons.handlers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.assertions.CouponLayoutAssert;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.model.event.Event;

import org.junit.Test;
import org.robolectric.annotation.Config;

import static org.fest.assertions.api.ANDROID.assertThat;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class NoTieCouponHandlerTest extends AbstractCouponHandlerTest {

    @Override
    protected EventCouponHandler createCouponHandler() {
        return new NoTieCouponHandler(betslipCouponDelegate);
    }

    @Test
    public void onCreateViewHolderHasTextViews() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View firstTeamText = viewHolder.itemView.findViewById(R.id.first_team);
        assertThat(firstTeamText).isNotNull().isInstanceOf(TextView.class);
        View secondTeamText = viewHolder.itemView.findViewById(R.id.second_team);
        assertThat(secondTeamText).isNotNull().isInstanceOf(TextView.class);

        View subtitleText = viewHolder.itemView.findViewById(R.id.subtitle);
        assertThat(subtitleText).isNotNull().isInstanceOf(TextView.class);
    }

    @Test
    public void onCreateViewHolderHasButtons() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        View firstButton = viewHolder.itemView.findViewById(R.id.first_button);
        assertThat(firstButton).isNotNull().isInstanceOf(CouponLayout.class);
        View secondButton = viewHolder.itemView.findViewById(R.id.second_button);
        assertThat(secondButton).isNotNull().isInstanceOf(CouponLayout.class);
        View thirdButton = viewHolder.itemView.findViewById(R.id.third_button);
        assertThat(thirdButton).isNull();
    }

    @Test
    public void onBindViewHolderWithTestEvent1() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_1.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Bonzi, B");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Sidorenko, A");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasCurrentTextColor(context.getResources().getColor(R.color.primaryColor))
                .hasText("6-3 1-1 40-40*");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("2/7");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("11/5");
    }

    @Test
    public void onBindViewHolderWithTestEvent2() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_2.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Morcinek, T");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Fisher, M");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasCurrentTextColor(context.getResources().getColor(R.color.primaryColor))
                .hasText("6-3 1-1 40-40*");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("1/7");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("7/1");
    }

    @Test
    public void onBindViewHolderWithTestEventNotOffered() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_not_offered.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Przysiezny, M");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Betov, S");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasCurrentTextColor(context.getResources().getColor(R.color.primaryColor))
                .hasText("7-5 5-3 *40-30");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("1/2");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isDisabled()
                .hasText("SUSP");
    }

    @Test
    public void onBindViewHolderWithTestEventDisabled() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_disabled.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Simon, Tobias");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Castelnuovo, Luca");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isDisabled()
                .hasText("6-7,6-3 2-2 0-0*");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isDisabled()
                .hasText("2/5");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isDisabled()
                .hasText("31/20");
    }

    @Test
    public void onBindViewHolderWithTestEventDisabled2() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_disabled_2.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isDisabled()
                .hasText("2/5");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isDisabled()
                .hasText("31/20");
    }

    @Test
    public void onBindViewHolderWithTestEventDisabled3() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_disabled_3.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isDisabled()
                .hasText("2/5");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isDisabled()
                .hasText("31/20");
    }

    @Test
    public void onBindViewHolderWithUpNextEvent() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_upnext.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Kerber, A");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Begu, I");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasText("Tomorrow 00:10 BST");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("13/20");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("23/20");
    }

    @Test
    public void onBindViewHolderWithNotRunningEvent() {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        Event event = resourceProvider.provideObjectFromResource(Event.class, "event_no_tie_not_running.json");

        // when
        couponHandler.onBindViewHolder(viewHolder, event, null);

        // then
        assertThat(getView(viewHolder, R.id.first_team))
                .isVisible()
                .hasText("Radwanska, A");

        assertThat(getView(viewHolder, R.id.second_team))
                .isVisible()
                .hasText("Koukalova, K");

        assertThat(getView(viewHolder, R.id.subtitle))
                .isVisible()
                .isEnabled()
                .hasText("19:25 BST");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.first_button))
                .isVisible()
                .isEnabled()
                .hasText("1/12");

        CouponLayoutAssert.assertThat(getCouponLayout(viewHolder, R.id.second_button))
                .isVisible()
                .isEnabled()
                .hasText("6/1");
    }
}
