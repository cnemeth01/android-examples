package com.sportingbet.sportsbook.betslip.adapter.single;

import android.view.View;

import com.sportingbet.sportsbook.betslip.adapter.BetTypeValueControllerTest;
import com.sportingbet.sportsbook.betslip.adapter.BetslipValueProvider;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

import org.junit.Test;
import org.mockito.Mockito;

import static org.fest.assertions.api.Assertions.assertThat;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SingleValueControllerTest extends BetTypeValueControllerTest {

    @Override
    protected BetslipValueProvider createBetTypeValueController(BetsValueContainer betsValueContainer) {
        return new SingleValueController(betsValueContainer);
    }

    @Test
    public void totalPaymentNoInputTest() throws Exception {
        // given
        BetslipInformation betslipInformation = createBetslipInformation("betslip_sp.json");
        getBetTypeValueController().setup(betslipInformation);
        StakePicker stakePicker = createStakePicker();
        getBetTypeValueController().registerPickerAdapter(stakePicker, Mockito.mock(View.class), betslipInformation.getBetTypeForId("SINGLE-380693304"));

        // when
        String totalPayout = betslipValueProvider.getTotalPayout();
        double totalStake = betslipValueProvider.getTotalStake();
        boolean hasTotalPayout = betslipValueProvider.hasTotalPayout();

        // then
        assertThat(hasTotalPayout).isFalse();
        assertThat(totalStake).isZero();
        assertThat(totalPayout).isEqualTo("0.0");
    }

    @Test
    public void totalPaymentTest() throws Exception {
        // given
        BetslipInformation betslipInformation = createBetslipInformation("betslip_sp.json");
        getBetTypeValueController().setup(betslipInformation);
        StakePicker stakePicker = createStakePicker();
        getBetTypeValueController().registerPickerAdapter(stakePicker, Mockito.mock(View.class), betslipInformation.getBetTypeForId("SINGLE-380693304"));

        // when
        stakePicker.getButtonPlus().performClick();
        String totalPayout = betslipValueProvider.getTotalPayout();
        double totalStake = betslipValueProvider.getTotalStake();
        boolean hasTotalPayout = betslipValueProvider.hasTotalPayout();

        // then
        assertThat(hasTotalPayout).isTrue();
        assertThat(totalStake).isEqualTo(10.0);
        assertThat(totalPayout).isEqualTo("14.8");
    }

    @Test
    public void totalPaymentSPTest() throws Exception {
        // given
        BetslipInformation betslipInformation = createBetslipInformation("betslip_sp.json");
        getBetTypeValueController().setup(betslipInformation);
        StakePicker stakePicker = createStakePicker();
        getBetTypeValueController().registerPickerAdapter(stakePicker, Mockito.mock(View.class), betslipInformation.getBetTypeForId("SINGLE-381164336"));

        // when
        stakePicker.getButtonPlus().performClick();
        String totalPayout = betslipValueProvider.getTotalPayout();
        double totalStake = betslipValueProvider.getTotalStake();
        boolean hasTotalPayout = betslipValueProvider.hasTotalPayout();

        // then
        assertThat(hasTotalPayout).isTrue();
        assertThat(totalStake).isEqualTo(10.0);
        assertThat(totalPayout).isEqualTo("SP");
    }

    @Test
    public void totalPaymentNormalAndSPNormalStakeTest() throws Exception {
        // given
        BetslipInformation betslipInformation = createBetslipInformation("betslip_sp.json");
        getBetTypeValueController().setup(betslipInformation);
        StakePicker stakePicker = createStakePicker();
        StakePicker stakePicker2 = createStakePicker();
        getBetTypeValueController().registerPickerAdapter(stakePicker, Mockito.mock(View.class), betslipInformation.getBetTypeForId("SINGLE-381164336"));
        getBetTypeValueController().registerPickerAdapter(stakePicker2, Mockito.mock(View.class), betslipInformation.getBetTypeForId("SINGLE-380693304"));

        // when
        stakePicker2.getButtonPlus().performClick();
        String totalPayout = betslipValueProvider.getTotalPayout();
        double totalStake = betslipValueProvider.getTotalStake();
        boolean hasTotalPayout = betslipValueProvider.hasTotalPayout();

        // then
        assertThat(hasTotalPayout).isTrue();
        assertThat(totalStake).isEqualTo(10);
        assertThat(totalPayout).isEqualTo("14.8");
    }

    @Test
    public void totalPaymentNormalAndSPBothStakeTest() throws Exception {
        // given
        BetslipInformation betslipInformation = createBetslipInformation("betslip_sp.json");
        getBetTypeValueController().setup(betslipInformation);
        StakePicker stakePicker = createStakePicker();
        StakePicker stakePicker2 = createStakePicker();
        getBetTypeValueController().registerPickerAdapter(stakePicker, Mockito.mock(View.class), betslipInformation.getBetTypeForId("SINGLE-381164336"));
        getBetTypeValueController().registerPickerAdapter(stakePicker2, Mockito.mock(View.class), betslipInformation.getBetTypeForId("SINGLE-380693304"));

        // when
        stakePicker.getButtonPlus().performClick();
        stakePicker2.getButtonPlus().performClick();
        String totalPayout = betslipValueProvider.getTotalPayout();
        double totalStake = betslipValueProvider.getTotalStake();
        boolean hasTotalPayout = betslipValueProvider.hasTotalPayout();

        // then
        assertThat(hasTotalPayout).isTrue();
        assertThat(totalStake).isEqualTo(20.0);
        assertThat(totalPayout).isEqualTo("SP");
    }
}
