package com.sportingbet.sportsbook.home;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.SportsbookApplication;
import com.sportingbet.sportsbook.coupons.betslip.model.BetslipResponse;
import com.sportingbet.sportsbook.general.ResourceProvider;
import com.sportingbet.sportsbook.general.dagger.configuration.ApplicationModule;
import com.sportingbet.sportsbook.home.betslip.BetslipButtonController;
import com.sportingbet.sportsbook.home.betslip.BetslipButtonEvent;
import com.sportingbet.sportsbook.home.drawer.list.DrawerItem;
import com.sportingbet.sportsbook.home.pager.PagerController;
import com.sportingbet.sportsbook.sports.SportsStore;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class HomeActivityTest {

    @Module(
            injects = HomeActivityTest.class,
            addsTo = ApplicationModule.class
    )
    class TestModule {

        @Provides
        @Singleton
        ResourceProvider provideApplicationContext(ObjectMapper objectMapper) {
            return new ResourceProvider(objectMapper);
        }
    }

    @Inject
    SportsStore sportsStore;

    @Inject
    ResourceProvider resourceProvider;

    @Inject
    EventBus eventBus;

    private ActivityController<HomeActivity> activityController;

    @Before
    public void setUp() throws Exception {
        SportsbookApplication application = (SportsbookApplication) RuntimeEnvironment.application;
        application.getApplicationGraph().plus(new TestModule()).inject(this);

        sportsStore.setSports(resourceProvider.provideSports());

        activityController = Robolectric.buildActivity(HomeActivity.class);
    }

    @Test
    public void getActivityTest() throws Exception {
        // given
        // when
        HomeActivity homeActivity = activityController.get();

        // then
        assertThat(homeActivity).isNotNull();
    }

    @Test
    public void createActivityTest() throws Exception {
        // given
        // when
        HomeActivity homeActivity = activityController.create().get();

        // then
        assertThat(homeActivity).isNotNull();
        assertThat(homeActivity.getSupportActionBar()).isNotNull();
        int displayOptions = homeActivity.getSupportActionBar().getDisplayOptions();
        assertThat(displayOptions).isEqualTo(ActionBar.DISPLAY_USE_LOGO | ActionBar.DISPLAY_SHOW_HOME);
    }

    @Test
    public void postCreateActivityTest() throws Exception {
        HomeActivity homeActivity = activityController.create().postCreate(null).get();
        assertThat(homeActivity).isNotNull();
    }

    @Test
    public void onBackPressedTest() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().get();

        // when
        homeActivity.onBackPressed();

        // then
        assertThat(homeActivity.isFinishing()).isTrue();
    }

    @Test
    public void onBackPressedWithDrawerOpenTest() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().get();
        DrawerLayout drawerLayout = (DrawerLayout) homeActivity.findViewById(R.id.drawer_layout);
        drawerLayout.openDrawer(GravityCompat.START);

        // when
        homeActivity.onBackPressed();

        // then
        assertThat(homeActivity.isFinishing()).isFalse();
        assertThat(drawerLayout.isDrawerOpen(GravityCompat.START)).isFalse();
    }

    @Test
    public void didSelectPageWithIdFootballShouldSelectListItemTest() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().get();

        // when
        homeActivity.didSelectPageWithId(102);

        // then
        ListView drawerListView = (ListView) homeActivity.findViewById(R.id.drawer_list);
        assertThat(drawerListView.getCheckedItemPosition()).isEqualTo(11);
    }

    @Test
    public void didSelectPageWithIdBasketballShouldSelectListItemTest() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().get();

        // when
        homeActivity.didSelectPageWithId(190);

        // then
        ListView drawerListView = (ListView) homeActivity.findViewById(R.id.drawer_list);
        assertThat(drawerListView.getCheckedItemPosition()).isEqualTo(6);
    }

    @Test
    public void didSelectDrawerItemWithIdShouldCloseDrawer() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().get();
        homeActivity.pagerController = Mockito.mock(PagerController.class);

        DrawerLayout drawerLayout = (DrawerLayout) homeActivity.findViewById(R.id.drawer_layout);
        drawerLayout.openDrawer(GravityCompat.START);

        // when
        homeActivity.didSelectDrawerItemWithId(102);

        // then
        assertThat(drawerLayout.isDrawerOpen(GravityCompat.START)).isFalse();
    }

    @Test
    public void didSelectDrawerItemWithIdInfomPagerController() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().get();
        homeActivity.pagerController = Mockito.mock(PagerController.class);

        // when
        homeActivity.didSelectDrawerItemWithId(102);
        homeActivity.drawerToggle.onDrawerClosed(null);

        // then
        Mockito.verify(homeActivity.pagerController).selectPageWithId(102);
    }

    @Test
    public void onActivityLifecycleTest() throws Exception {
        HomeActivity homeActivity = activityController
                .create()
                .postCreate(null)
                .start()
                .resume()
                .postResume()
                .pause()
                .stop()
                .destroy()
                .get();

        assertThat(homeActivity).isNotNull();
    }

    @Test
    public void drawerControllerOnItemClickFootball() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().get();
        homeActivity.pagerController = Mockito.mock(PagerController.class);

        // when
        homeActivity.drawerController.onItemClick(null, null, 11, 102);
        homeActivity.drawerToggle.onDrawerClosed(null);

        // then
        ListView drawerListView = (ListView) homeActivity.findViewById(R.id.drawer_list);
        assertThat(drawerListView.getCheckedItemPosition()).isEqualTo(11);

        Mockito.verify(homeActivity.pagerController).selectPageWithId(102);
    }

    @Test
    public void drawerControllerOnItemClickBasketball() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().get();
        homeActivity.pagerController = Mockito.mock(PagerController.class);

        // when
        homeActivity.drawerController.onItemClick(null, null, 6, 190);
        homeActivity.drawerToggle.onDrawerClosed(null);

        // then
        ListView drawerListView = (ListView) homeActivity.findViewById(R.id.drawer_list);
        assertThat(drawerListView.getCheckedItemPosition()).isEqualTo(6);

        Mockito.verify(homeActivity.pagerController).selectPageWithId(190);
    }

    @Test
    public void drawerControllerOnItemClickedFootball() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().get();
        homeActivity.pagerController = Mockito.mock(PagerController.class);

        // when
        homeActivity.drawerController.onItemClicked(null, new DrawerItem(102, null, 0));
        homeActivity.drawerToggle.onDrawerClosed(null);

        // then
        ListView drawerListView = (ListView) homeActivity.findViewById(R.id.drawer_list);
        assertThat(drawerListView.getCheckedItemPosition()).isEqualTo(11);

        Mockito.verify(homeActivity.pagerController).selectPageWithId(102);
    }

    @Test
    public void drawerControllerOnItemClickedBasketball() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().get();
        homeActivity.pagerController = Mockito.mock(PagerController.class);

        // when
        homeActivity.drawerController.onItemClicked(null, new DrawerItem(190, null, 0));
        homeActivity.drawerToggle.onDrawerClosed(null);

        // then
        ListView drawerListView = (ListView) homeActivity.findViewById(R.id.drawer_list);
        assertThat(drawerListView.getCheckedItemPosition()).isEqualTo(6);

        Mockito.verify(homeActivity.pagerController).selectPageWithId(190);
    }

    @Test
    public void betslipButtonControllerLifecycle() throws Exception {
        // given when
        activityController.create().destroy().get();

        // then
        Mockito.verify(eventBus).register(Mockito.any(BetslipButtonController.class));
        Mockito.verify(eventBus).unregister(Mockito.any(BetslipButtonController.class));
    }

    protected Button createButton(HomeActivity homeActivity, int visibility) {
        Button button = Mockito.mock(Button.class);
        Mockito.when(button.getVisibility()).thenReturn(visibility);
        Mockito.when(button.getContext()).thenReturn(homeActivity);
        return button;
    }

    @Test
    public void betslipButtonControllerHideButton() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().destroy().get();
        BetslipButtonController betslipButtonController = homeActivity.betslipButtonController;
        betslipButtonController.unregister();
        // initialize betslipButtonController
        Button button = createButton(homeActivity, View.VISIBLE);
        betslipButtonController.register(button);
        betslipButtonController.handleBetResponse(new BetslipResponse(1));

        // when
        betslipButtonController.handleBetslipEvent(BetslipButtonEvent.HIDE_BUTTON);

        // then
        Mockito.verify(button).setVisibility(View.GONE);
    }

    @Test
    public void betslipButtonControllerHideButtonWhenListEmpty() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().destroy().get();
        BetslipButtonController betslipButtonController = homeActivity.betslipButtonController;
        betslipButtonController.unregister();
        // initialize betslipButtonController
        Button button = createButton(homeActivity, View.VISIBLE);
        betslipButtonController.register(button);

        // when
        betslipButtonController.handleBetslipEvent(BetslipButtonEvent.HIDE_BUTTON);

        // then
        Mockito.verify(button, Mockito.never()).setVisibility(View.GONE);
    }

    @Test
    public void betslipButtonControllerHideGONEButton() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().destroy().get();
        BetslipButtonController betslipButtonController = homeActivity.betslipButtonController;
        betslipButtonController.unregister();
        // initialize betslipButtonController
        Button button = createButton(homeActivity, View.GONE);
        betslipButtonController.register(button);

        // when
        betslipButtonController.handleBetslipEvent(BetslipButtonEvent.HIDE_BUTTON);

        // then
        Mockito.verify(button, Mockito.never()).setVisibility(View.GONE);
    }

    @Test
    public void betslipButtonControllerShowButton() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().destroy().get();
        BetslipButtonController betslipButtonController = homeActivity.betslipButtonController;
        betslipButtonController.unregister();
        // initialize betslipButtonController
        Button button = createButton(homeActivity, View.GONE);
        Mockito.when(button.getContext()).thenReturn(homeActivity);
        betslipButtonController.register(button);
        betslipButtonController.handleBetResponse(new BetslipResponse(1));

        // when
        betslipButtonController.handleBetslipEvent(BetslipButtonEvent.SHOW_BUTTON);

        // then
        Mockito.verify(button, Mockito.times(2)).setVisibility(View.VISIBLE);
    }

    @Test
    public void betslipButtonControllerShowButtonWhenListEmpty() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().destroy().get();
        BetslipButtonController betslipButtonController = homeActivity.betslipButtonController;
        betslipButtonController.unregister();
        // initialize betslipButtonController
        Button button = createButton(homeActivity, View.GONE);
        Mockito.when(button.getContext()).thenReturn(homeActivity);
        betslipButtonController.register(button);

        // when
        betslipButtonController.handleBetslipEvent(BetslipButtonEvent.SHOW_BUTTON);

        // then
        Mockito.verify(button, Mockito.never()).setVisibility(View.VISIBLE);
    }

    @Test
    public void betslipButtonControllerShowVISIBLEButton() throws Exception {
        // given
        HomeActivity homeActivity = activityController.create().destroy().get();
        BetslipButtonController betslipButtonController = homeActivity.betslipButtonController;
        betslipButtonController.unregister();
        // initialize betslipButtonController
        Button button = createButton(homeActivity, View.VISIBLE);
        Mockito.when(button.getContext()).thenReturn(homeActivity);
        betslipButtonController.register(button);

        // when
        betslipButtonController.handleBetslipEvent(BetslipButtonEvent.SHOW_BUTTON);

        // then
        Mockito.verify(button, Mockito.never()).setVisibility(View.VISIBLE);
    }
}
