package com.sportingbet.sportsbook.betslip.adapter.single;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.adapter.BetslipAdapter;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.model.BetSelection;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.general.ResourceProvider;
import com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule;
import com.sportingbet.sportsbook.general.dagger.configuration.TestModules;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

import org.fest.assertions.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import dagger.Module;
import dagger.ObjectGraph;

import static org.fest.assertions.api.ANDROID.assertThat;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 18, manifest = "src/main/AndroidManifest.xml")
public class SingleAdapterTest {

    @Inject
    Context context;

    @Inject
    ResourceProvider resourceProvider;

    @Module(
            injects = SingleAdapterTest.class,
            includes = {
                    GeneralTestModule.class,
            },
            overrides = true
    )
    class TestModule {
    }

    private BetslipAdapter.Delegate delegate;

    private SingleAdapter singleAdapter;

    @Before
    public void setUp() throws Exception {
        ObjectGraph.create(TestModules.create(RuntimeEnvironment.application, new TestModule())).inject(this);

        delegate = createBetslipAdapterDelegate();

        singleAdapter = new SingleAdapter(context, delegate);
    }

    private BetslipAdapter.Delegate createBetslipAdapterDelegate() {
        BetslipAdapter.Delegate delegate = Mockito.mock(BetslipAdapter.Delegate.class);
        Mockito.when(delegate.getBetsValueContainer()).thenReturn(Mockito.mock(BetsValueContainer.class));
        return delegate;
    }

    private RecyclerView.ViewHolder createViewHolder() {
        return singleAdapter.createViewHolder(null, 0);
    }

    private View getViewById(RecyclerView.ViewHolder viewHolder, int viewId) {
        return viewHolder.itemView.findViewById(viewId);
    }

    private TextView getTextViewById(RecyclerView.ViewHolder viewHolder, int viewId) {
        return (TextView) viewHolder.itemView.findViewById(viewId);
    }

    @Test
    public void buildObjectTest() throws Exception {
        Assertions.assertThat(singleAdapter).isNotNull();
    }

    @Test
    public void onCreateViewHolderHasTextViews() {
        // given
        // when
        RecyclerView.ViewHolder viewHolder = createViewHolder();

        // then
        assertThat(getViewById(viewHolder, R.id.delete_icon))
                .isNotNull()
                .isInstanceOf(ImageView.class);

        assertThat(getViewById(viewHolder, R.id.delete_layout))
                .isNotNull()
                .isNotVisible()
                .isInstanceOf(LinearLayout.class);

        assertThat(getViewById(viewHolder, R.id.warning_indicator))
                .isNotNull()
                .isNotVisible()
                .isInstanceOf(View.class);

        assertThat(getViewById(viewHolder, R.id.odds))
                .isNotNull()
                .isVisible()
                .isInstanceOf(TextView.class);

        assertThat(getViewById(viewHolder, R.id.selection))
                .isNotNull()
                .isVisible()
                .isInstanceOf(TextView.class);

        assertThat(getViewById(viewHolder, R.id.book))
                .isNotNull()
                .isVisible()
                .isInstanceOf(TextView.class);

        assertThat(getViewById(viewHolder, R.id.event))
                .isNotNull()
                .isVisible()
                .isInstanceOf(TextView.class);

        assertThat(getViewById(viewHolder, R.id.warning_text))
                .isNotNull()
                .isNotVisible()
                .isInstanceOf(TextView.class);

        assertThat(getViewById(viewHolder, R.id.stake_picker))
                .isNotNull()
                .isVisible()
                .isInstanceOf(StakePicker.class);
    }

    private void setupBetslipInformation(String betslipResource) {
        BetslipInformation betslipInformation = resourceProvider.provideObjectFromResource(BetslipInformation.class, betslipResource);
        singleAdapter.setBetslipInformation(betslipInformation);
    }

    private void assertViewNotVisible(RecyclerView.ViewHolder viewHolder, int viewId) {
        assertThat(getViewById(viewHolder, viewId))
                .isNotVisible();
    }

    private void assertViewWithText(RecyclerView.ViewHolder viewHolder, int viewId, String text) {
        assertThat(getTextViewById(viewHolder, viewId))
                .isVisible()
                .isEnabled()
                .hasText(text);
    }

    private void assertViewTextAndColor(RecyclerView.ViewHolder viewHolder, int viewId, String text, int color) {
        assertThat(getTextViewById(viewHolder, viewId))
                .isVisible()
                .hasCurrentTextColor(context.getResources().getColor(color))
                .hasText(text);
    }

    private BetSelection getBetSelectionBySelectionId(long selectionId) {
        for (BetSelection betSelection : singleAdapter.getBetslipInformation().getBetSelections()) {
            if (betSelection.getSelectionId() == selectionId) {
                return betSelection;
            }
        }
        return null;
    }

    @Test
    public void normalBetSelection() throws Exception {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        setupBetslipInformation("betslip_susp.json");

        BetSelection betSelection = getBetSelectionBySelectionId(369568598);

        // when
        singleAdapter.onBindViewHolder(viewHolder, betSelection);

        // then
        assertViewWithText(viewHolder, R.id.odds, "5/4");
        assertViewWithText(viewHolder, R.id.selection, "Cervantes");
        assertViewWithText(viewHolder, R.id.book, "Match Winner");
        assertViewWithText(viewHolder, R.id.event, "Cervantes, I v Herbert, P");

        assertThat(getViewById(viewHolder, R.id.stake_picker))
                .isVisible();

        assertViewNotVisible(viewHolder, R.id.warning_indicator);
        assertViewNotVisible(viewHolder, R.id.warning_text);
    }

    @Test
    public void priceChangeBetSelection() throws Exception {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        setupBetslipInformation("betslip_susp.json");

        BetSelection betSelection = getBetSelectionBySelectionId(369565977);

        // when
        singleAdapter.onBindViewHolder(viewHolder, betSelection);

        // then
        assertViewTextAndColor(viewHolder, R.id.odds, "9/10", R.color.positive_color);

        assertViewWithText(viewHolder, R.id.selection, "Menendez-Maceiras");
        assertViewWithText(viewHolder, R.id.book, "Match Winner");
        assertViewWithText(viewHolder, R.id.event, "Menendez-Maceiras, A v Berrer, M");

        assertViewWithText(viewHolder, R.id.warning_text, "Please note there has been a price change");
        assertThat(getViewById(viewHolder, R.id.warning_indicator))
                .isVisible();
        assertThat(getViewById(viewHolder, R.id.stake_picker))
                .isVisible();
    }

    @Test
    public void suspendedBetSelection() throws Exception {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        setupBetslipInformation("betslip_susp.json");

        BetSelection betSelection = getBetSelectionBySelectionId(369600577);

        // when
        singleAdapter.onBindViewHolder(viewHolder, betSelection);

        // then
        assertViewTextAndColor(viewHolder, R.id.odds, "9/10", R.color.textDisabled);
        assertViewTextAndColor(viewHolder, R.id.selection, "Linette, M/Minella, M", R.color.textDisabled);
        assertViewTextAndColor(viewHolder, R.id.book, "Match Winner", R.color.textDisabled);
        assertViewTextAndColor(viewHolder, R.id.event, "Linette, M / Minella, M v Cepelova, J / Voegele, S", R.color.textDisabled);

        assertViewWithText(viewHolder, R.id.warning_text, "This market has been suspended or closed");
        assertThat(getViewById(viewHolder, R.id.warning_indicator))
                .isVisible();
        assertThat(getViewById(viewHolder, R.id.stake_picker))
                .isVisible();
    }

    @Test
    public void closedBetSelection() throws Exception {
        // given
        RecyclerView.ViewHolder viewHolder = createViewHolder();
        setupBetslipInformation("betslip_susp.json");

        BetSelection betSelection = getBetSelectionBySelectionId(372202170);

        // when
        singleAdapter.onBindViewHolder(viewHolder, betSelection);

        // then
        assertViewTextAndColor(viewHolder, R.id.odds, "1/12", R.color.textDisabled);
        assertViewTextAndColor(viewHolder, R.id.selection, "Albury Wodonga Bandits", R.color.textDisabled);
        assertViewTextAndColor(viewHolder, R.id.book, "Match Winner (No Tie)", R.color.textDisabled);
        assertViewTextAndColor(viewHolder, R.id.event, "Albury Wodonga Bandits v Brisbane Spartans", R.color.textDisabled);

        assertViewWithText(viewHolder, R.id.warning_text, "This bet is no longer available");
        assertThat(getViewById(viewHolder, R.id.warning_indicator)).isVisible();
        assertViewNotVisible(viewHolder, R.id.stake_picker);
    }
}
