package com.sportingbet.sportsbook.general;

import com.sportingbet.sportsbook.betslip.receipt.BalanceRequester;
import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.home.account.model.BalanceDetails;
import com.sportingbet.sportsbook.home.account.model.BalanceDetailsWrapper;

import retrofit.RetrofitError;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BalanceUpdateController implements NetworkResponseListener<BalanceDetailsWrapper> {

    private final BalanceRequester balanceRequester;
    private final LoginResponseStore loginResponseStore;
    private final ErrorHandler errorHandler;

    public BalanceUpdateController(BalanceRequester balanceRequester, LoginResponseStore loginResponseStore, ErrorHandler errorHandler) {
        this.balanceRequester = balanceRequester;
        this.balanceRequester.setupResponseListener(this);
        this.loginResponseStore = loginResponseStore;
        this.errorHandler = errorHandler;
    }

    public void requestBalanceUpdate(ProgressController... progressController) {
        balanceRequester.prepareRequest(progressController);
        balanceRequester.requestGetBalance();
    }

    @Override
    public void success(BalanceDetailsWrapper object) {
        updateLoginResponse(object.getBalanceDetails());
    }

    private void updateLoginResponse(BalanceDetails balanceDetails) {
        loginResponseStore.getLoginResponse().setBalanceDetails(balanceDetails);
    }

    @Override
    public void failure(RetrofitError retrofitError) {
        errorHandler.handleError(retrofitError);
    }
}
