package com.sportingbet.sportsbook.general.dagger.components;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sportingbet.sportsbook.SportsbookApplication;
import com.sportingbet.sportsbook.general.dagger.configuration.ActivityModule;

import dagger.ObjectGraph;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportsbookActivity extends AppCompatActivity {

    private ObjectGraph activityGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityGraph = getSportsbookApplication().getApplicationGraph().plus(new ActivityModule(this));
        activityGraph.inject(this);
    }

    @Override
    protected void onDestroy() {
        activityGraph = null;
        super.onDestroy();
    }

    private SportsbookApplication getSportsbookApplication() {
        return (SportsbookApplication) getApplication();
    }

    public void inject(Object object) {
        activityGraph.inject(object);
    }

    public ObjectGraph getObjectGraph() {
        return activityGraph;
    }
}
