package com.sportingbet.sportsbook.home.navigation;

import com.google.common.base.Objects;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class NavigationItem implements android.os.Parcelable {

    @Getter
    protected long id;

    @Getter
    protected String name;

    protected NavigationItem(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        return getClass() == o.getClass() && Objects.equal(id, ((NavigationItem) o).getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
