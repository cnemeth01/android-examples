package com.sportingbet.sportsbook.general.network.error;

import android.app.Activity;

import com.nispok.snackbar.Snackbar;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public class ActivitySnackbarErrorHandler extends SnackbarErrorHandler {

    public ActivitySnackbarErrorHandler(Activity activity) {
        super(activity);
    }

    @Override
    protected void showSnackbar(Snackbar snackbar) {
        snackbar.show((Activity) context);
    }
}
