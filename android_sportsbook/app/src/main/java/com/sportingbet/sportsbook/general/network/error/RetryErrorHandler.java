package com.sportingbet.sportsbook.general.network.error;

import android.app.Activity;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.listeners.ActionClickListener;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public class RetryErrorHandler extends ActivitySnackbarErrorHandler {

    private ActionClickListener actionClickListener;

    public RetryErrorHandler(Activity activity) {
        super(activity);
    }

    public void registerAction(final Runnable retryAction) {
        actionClickListener = new ActionClickListener() {
            @Override
            public void onActionClicked(Snackbar snackbar) {
                retryAction.run();
            }
        };
    }

    @Override
    protected void updateSnackbar(Snackbar snackbar) {
        super.updateSnackbar(snackbar);
        snackbar.actionListener(actionClickListener);
    }

    public void dismiss() {
        SnackbarManager.dismiss();
    }
}
