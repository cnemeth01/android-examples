package com.sportingbet.sportsbook.mybets.fragments.filter;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.mybets.model.MyBet;
import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;

import java.util.List;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsResponseFilter {

    public List<MyBet> createItems(MyBetsResponse myBetsResponse) {
        return filterItems(myBetsResponse.getBets().getBet());
    }

    public List<MyBet> createCashOutItems(MyBetsResponse myBetsResponse) {
        return filterCashOutItems(createItems(myBetsResponse));
    }

    public List<MyBet> filterItems(List<MyBet> myBets) {
        List<MyBet> items = Lists.newArrayList();
        for (MyBet myBet : myBets) {
            if (!isFailedBet(myBet)) {
                items.add(myBet);
            }
        }
        return items;
    }

    public List<MyBet> filterCashOutItems(List<MyBet> items) {
        List<MyBet> cashItems = Lists.newArrayList();
        for (MyBet myBet : items) {
            if (myBet.getCashoutAmount() != null && myBet.getCashoutAmount() > 0) {
                cashItems.add(myBet);
            }
        }
        return cashItems;
    }

    private boolean isFailedBet(MyBet myBet) {
        switch (myBet.getBetStatus()) {
            case CLOSED:
            case VOIDED:
            case CANCELLED:
            case CASHEDOUT:
                return true;
            default:
                return false;
        }
    }
}
