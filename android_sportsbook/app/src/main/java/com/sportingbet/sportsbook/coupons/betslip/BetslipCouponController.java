package com.sportingbet.sportsbook.coupons.betslip;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.betslip.controller.BetslipController;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.general.network.error.FragmentSnackbarErrorHandler;
import com.sportingbet.sportsbook.general.network.progress.BetProgressBarController;
import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipCouponController implements BetslipCouponDelegate {

    private final List<Selection> loadingSelections = Lists.newArrayList();

    private BetslipController betslipController;
    
    private View view;
    private RecyclerView.Adapter adapter;

    public BetslipCouponController(BetslipController betslipController) {
        this.betslipController = betslipController;
    }

    @Override
    public void register(View view, RecyclerView.Adapter adapter) {
        this.view = view;
        this.adapter = adapter;
    }

    @Override
    public void onBetClicked(final View view, Event event, Book book, final Selection selection) {
        if (view.isActivated()) {
            removeBet(view, selection);
        } else {
            addBet(view, event, book, selection);
        }
    }

    private void addBet(final View view, Event event, Book book, final Selection selection) {
        betslipController.addBet(event, book, selection, createErrorHandler(),
                new BetProgressBarController((CouponLayout) view), new LoadingProgressController(selection));
    }

    private void removeBet(final View view, final Selection selection) {
        betslipController.removeBet(selection, createErrorHandler(),
                new BetProgressBarController((CouponLayout) view), new LoadingProgressController(selection));
    }

    private ErrorHandler createErrorHandler() {
        return new FragmentSnackbarErrorHandler(view.getContext(), (ViewGroup) view);
    }

    @Override
    public boolean betslipContainsSelection(Selection selection) {
        return betslipController.containsSelection(selection.getId());
    }

    @Override
    public boolean loadingSelection(Selection selection) {
        return loadingSelections.contains(selection);
    }

    private class LoadingProgressController implements ProgressController {

        private final Selection selection;

        public LoadingProgressController(Selection selection) {
            this.selection = selection;
        }

        @Override
        public void preExecute() {
            loadingSelections.add(selection);
        }

        @Override
        public void postExecuteWithSuccess(boolean success) {
            loadingSelections.remove(selection);
            adapter.notifyDataSetChanged();
        }
    }
}
