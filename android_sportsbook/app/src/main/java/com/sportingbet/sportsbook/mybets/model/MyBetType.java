package com.sportingbet.sportsbook.mybets.model;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public enum MyBetType {
    open, settled, cashout, all;
}
