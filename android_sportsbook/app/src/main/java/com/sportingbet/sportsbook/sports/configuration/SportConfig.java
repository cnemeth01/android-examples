package com.sportingbet.sportsbook.sports.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportConfig {

    @Getter
    @JsonProperty("sport")
    private long id;

    @Getter
    @JsonProperty("default_coupon")
    private CouponType defaultCouponType;

    @Getter
    @JsonProperty("tab_layout")
    private TabLayoutType tabLayoutType;

    @Getter
    @JsonProperty("scoreboard_layout")
    private ScoreboardLayoutType scoreboardLayoutType;
}
