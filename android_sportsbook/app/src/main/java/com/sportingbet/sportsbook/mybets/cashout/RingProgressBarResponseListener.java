package com.sportingbet.sportsbook.mybets.cashout;

import com.sportingbet.sportsbook.general.network.progress.RingProgressBarController;
import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;

import lombok.Getter;
import lombok.Setter;
import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class RingProgressBarResponseListener<T> implements NetworkResponseListener<T>, RingProgressBarController.ProgressControllerListener {

    private final ErrorHandler errorHandler;

    @Getter
    private final RingProgressBarController ringProgressBarController;

    @Setter
    private Delegate<T> delegate;

    private T object;
    private boolean progressHasFinished;

    public RingProgressBarResponseListener(ErrorHandler errorHandler, RingProgressBarController ringProgressBarController) {
        this.errorHandler = errorHandler;
        this.ringProgressBarController = ringProgressBarController;
        this.ringProgressBarController.setProgressControllerListener(this);
    }

    @Override
    public synchronized void success(T object) {
        if (progressHasFinished) {
            delegate.success(object);
        } else {
            this.object = object;
        }
    }

    @Override
    public void failure(RetrofitError error) {
        errorHandler.handleError(error);
    }

    @Override
    public synchronized void onProgressFinished() {
        if (object != null) {
            delegate.success(object);
        } else {
            progressHasFinished = true;
        }
    }

    public interface Delegate<T> {

        void success(T object);
    }
}
