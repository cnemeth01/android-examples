package com.sportingbet.sportsbook.coupons.betslip.requesters;

import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.general.network.requesters.Requester;
import com.sportingbet.sportsbook.general.network.services.BetService;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class GetBetsRequester extends Requester<BetslipInformation> {

    protected final BetService betService;

    public GetBetsRequester(BetService betService) {
        this.betService = betService;
    }

    public void getBets() {
        betService.getBets(getCallback());
    }
}
