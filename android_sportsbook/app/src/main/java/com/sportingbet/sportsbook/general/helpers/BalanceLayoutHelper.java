package com.sportingbet.sportsbook.general.helpers;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.home.account.model.LoginResponse;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BalanceLayoutHelper {

    public static void setupBalanceLayout(Activity activity, LoginResponse loginResponse) {
        View balanceLayout = activity.findViewById(R.id.balance_layout);
        if (loginResponse != null) {
            balanceLayout.setVisibility(View.VISIBLE);
            BalanceLayoutHelper.setupCustomerDetails(balanceLayout, loginResponse);
        } else {
            balanceLayout.setVisibility(View.GONE);
        }
    }

    public static void setupCustomerDetails(View accountLayout, LoginResponse loginResponse) {
        getTextView(accountLayout, R.id.customer_name).setText(loginResponse.getCustomerDetails().getFullName());
        getTextView(accountLayout, R.id.customer_balance).setText(loginResponse.getBalanceDetails().toString());
    }

    private static TextView getTextView(View accountLayout, int resourceId) {
        return (TextView) accountLayout.findViewById(resourceId);
    }
}
