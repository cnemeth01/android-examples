package com.sportingbet.sportsbook.content.fragments.upnext;

import com.sportingbet.sportsbook.content.general.fragments.events.EventsContentFragment;
import com.sportingbet.sportsbook.model.event.DetailsWrapper;

import javax.inject.Inject;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class UpNextFragment extends EventsContentFragment<DetailsWrapper> {

    @Inject
    @Getter
    UpNextRequester networkRequester;

    public void onRefresh() {
        super.onRefresh();
        networkRequester.requestUpNextForSport(getSport().getId());
    }
}
