package com.sportingbet.sportsbook.content.fragments.event.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.receipt.adapter.handlers.AbstractViewHolderHandler;

public class DescriptionViewHolderHandler extends AbstractViewHolderHandler<String, DescriptionViewHolderHandler.ViewHolder> {

    public DescriptionViewHolderHandler(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(createView(parent, R.layout.coupon_description));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, String text) {
        viewHolder.getTitleView().setText(text);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);
        }

        public TextView getTitleView() {
            return (TextView) itemView;
        }
    }
}
