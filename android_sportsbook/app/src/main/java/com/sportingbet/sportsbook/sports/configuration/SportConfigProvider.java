package com.sportingbet.sportsbook.sports.configuration;

import com.google.common.collect.Maps;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportConfigProvider {

    private static final CouponType DEFAULT_COUPON = CouponType.CouponTypeEmpty;
    private static final TabLayoutType DEFAULT_TAB_LAYOUT = TabLayoutType.TabLayoutTypeThreeTabs;
    private static final ScoreboardLayoutType DEFAULT_SCOREBOARD_LAYOUT = ScoreboardLayoutType.ScoreboardLayoutTypeNoScore;

    private static final int BASKETBALL_SPORT_ID = 190;

    private Map<Long, SportConfig> sportConfigMap;

    public SportConfigProvider(SportConfigPersistentStore sportConfigPersistentStore) {
        try {
            List<SportConfig> sportConfigs = sportConfigPersistentStore.getSportConfigs();
            sportConfigMap = Maps.newHashMapWithExpectedSize(sportConfigs.size());
            for (SportConfig sportConfig : sportConfigs) {
                sportConfigMap.put(sportConfig.getId(), sportConfig);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public CouponType getDefaultCouponTypeForSportId(long sportId) {
        try {
            return sportConfigMap.get(sportId).getDefaultCouponType();
        } catch (NullPointerException e) {
            return DEFAULT_COUPON;
        }
    }

    public CouponType getInPlayDefaultCouponTypeForSportId(long sportId) {
        switch ((int) sportId) {
            case BASKETBALL_SPORT_ID:
                return CouponType.CouponTypeNoTie;
            default:
                return getDefaultCouponTypeForSportId(sportId);
        }
    }

    public TabLayoutType getTabLayoutTypeForSportId(long sportId) {
        try {
            return sportConfigMap.get(sportId).getTabLayoutType();
        } catch (NullPointerException e) {
            return DEFAULT_TAB_LAYOUT;
        }
    }

    public ScoreboardLayoutType getScoreboardLayoutTypeForSportId(long sportId) {
        try {
            return sportConfigMap.get(sportId).getScoreboardLayoutType();
        } catch (NullPointerException e) {
            return DEFAULT_SCOREBOARD_LAYOUT;
        }
    }
}
