package com.sportingbet.sportsbook.general.dagger.configuration;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.sportingbet.sportsbook.betslip.controller.BetslipController;
import com.sportingbet.sportsbook.betslip.controller.BetslipControllerImpl;
import com.sportingbet.sportsbook.betslip.controller.BetslipStore;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.content.fragments.byevent.links.SportLinkHelper;
import com.sportingbet.sportsbook.content.general.controllers.refresh.RefreshTimeProvider;
import com.sportingbet.sportsbook.coupons.CouponHandlerFactory;
import com.sportingbet.sportsbook.coupons.CouponHandlerFactoryImpl;
import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponController;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandlerFactory;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandlerFactoryImpl;
import com.sportingbet.sportsbook.general.LoginResponseStore;
import com.sportingbet.sportsbook.general.helpers.SharedPreferencesHelper;
import com.sportingbet.sportsbook.general.network.authentication.AuthenticationInterceptor;
import com.sportingbet.sportsbook.general.network.authentication.AuthenticationStore;
import com.sportingbet.sportsbook.general.network.authentication.InterceptingOkClient;
import com.sportingbet.sportsbook.general.network.services.AccountService;
import com.sportingbet.sportsbook.general.network.services.BetService;
import com.sportingbet.sportsbook.general.network.services.ConfigService;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.general.network.services.ServiceFactory;
import com.sportingbet.sportsbook.general.network.services.SessionService;
import com.sportingbet.sportsbook.home.navigation.NavigationItemFactory;
import com.sportingbet.sportsbook.home.navigation.NavigationItemFactoryImpl;
import com.sportingbet.sportsbook.home.navigation.configuration.NavigationConfiguration;
import com.sportingbet.sportsbook.home.navigation.configuration.NavigationConfigurationImpl;
import com.sportingbet.sportsbook.scoreboard.ScoreboardFactory;
import com.sportingbet.sportsbook.sports.SportIconsProvider;
import com.sportingbet.sportsbook.sports.SportProvider;
import com.sportingbet.sportsbook.sports.SportsStore;
import com.sportingbet.sportsbook.sports.configuration.SportConfigPersistentStore;
import com.sportingbet.sportsbook.sports.configuration.SportConfigProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.client.OkClient;
import retrofit.converter.Converter;
import retrofit.converter.JacksonConverter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Module(
        library = true
)
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    AuthenticationInterceptor provideAuthenticationInterceptor() {
        return new AuthenticationInterceptor();
    }

    @Provides
    AuthenticationStore provideAuthenticationStore(AuthenticationInterceptor authenticationInterceptor) {
        return authenticationInterceptor;
    }

    @Provides
    @Singleton
    ObjectMapper provideObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    @Provides
    @Singleton
    Converter provideConverter(ObjectMapper objectMapper) {
        return new JacksonConverter(objectMapper);
    }

    @Provides
    @Singleton
    OkClient provideOkClient(AuthenticationInterceptor authenticationInterceptor) {
        return new InterceptingOkClient(authenticationInterceptor);
    }

    @Provides
    @Singleton
    SessionService provideSessionService(OkClient okClient, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        return ServiceFactory.createSessionService(okClient, converter, authenticationInterceptor);
    }

    @Provides
    @Singleton
    PublishingService providePublishingService(OkClient okClient, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        return ServiceFactory.createPublishingService(okClient, converter, authenticationInterceptor);
    }

    @Provides
    @Singleton
    AccountService provideAccountService(OkClient okClient, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        return ServiceFactory.createAccountService(okClient, converter, authenticationInterceptor);
    }

    @Provides
    @Singleton
    BetService provideBetService(OkClient okClient, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        return ServiceFactory.createBetService(okClient, converter, authenticationInterceptor);
    }

    @Provides
    @Singleton
    ConfigService provideConfigService(Converter converter) {
        return ServiceFactory.createConfigService(new OkClient(), converter);
    }

    @Provides
    @Singleton
    NavigationConfigurationImpl provideNavigationConfigurationImpl(NavigationItemFactory navigationItemFactory) {
        return new NavigationConfigurationImpl(navigationItemFactory);
    }

    @Provides
    NavigationConfiguration provideNavigationConfiguration(NavigationConfigurationImpl navigationConfiguration) {
        return navigationConfiguration;
    }

    @Provides
    SportsStore provideSportsStore(NavigationConfigurationImpl navigationConfiguration) {
        return navigationConfiguration;
    }

    @Provides
    SportProvider provideSportProvider(NavigationConfigurationImpl navigationConfiguration) {
        return navigationConfiguration;
    }

    @Provides
    SportConfigPersistentStore provideSportConfigStore(Context context, ObjectMapper objectMapper) {
        return new SportConfigPersistentStore(context, objectMapper);
    }

    @Provides
    @Singleton
    SportConfigProvider provideSportConfigProvider(SportConfigPersistentStore sportConfigPersistentStore) {
        return new SportConfigProvider(sportConfigPersistentStore);
    }

    @Provides
    SportIconsProvider provideSportIconsProvider(Context context) {
        return new SportIconsProvider(context);
    }

    @Provides
    @Singleton
    NavigationItemFactory provideNavigationItemFactory(Context context) {
        return new NavigationItemFactoryImpl(context);
    }

    @Provides
    @Singleton
    BetslipController provideBetslipController(BetService betService, EventBus eventBus, BetsValueContainer betsValueContainer) {
        return new BetslipControllerImpl(betService, eventBus, betsValueContainer);
    }

    @Provides
    BetslipStore provideBetslipStore(BetslipController betslipController) {
        return betslipController;
    }

    @Provides
    BetslipCouponController provideBetslipCouponController(BetslipController betslipController) {
        return new BetslipCouponController(betslipController);
    }

    @Provides
    EventCouponHandlerFactory provideEventCouponHandlerFactory(BetslipCouponController betslipCouponController) {
        return new EventCouponHandlerFactoryImpl(betslipCouponController);
    }

    @Provides
    CouponHandlerFactory provideBookCouponFactory(BetslipCouponController betslipCouponController) {
        return new CouponHandlerFactoryImpl(betslipCouponController);
    }

    @Provides
    @Singleton
    ScoreboardFactory provideScoreboardFactory(Context context, SportConfigProvider sportConfigProvider) {
        return new ScoreboardFactory(context, sportConfigProvider);
    }

    @Provides
    @Singleton
    LoginResponseStore provideLoginResponseStore() {
        return new LoginResponseStore();
    }

    @Provides
    @Singleton
    BetsValueContainer provideBetsValueContainer() {
        return new BetsValueContainer();
    }

    @Provides
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    SharedPreferencesHelper provideSharedPreferencesHelper(SharedPreferences sharedPreferences) {
        return new SharedPreferencesHelper(sharedPreferences);
    }

    @Provides
    @Singleton
    EventBus provideEventBus() {
        return new EventBus();
    }

    @Provides
    @Singleton
    SportLinkHelper provideSportLinksHelper(SportProvider sportProvider, SportIconsProvider sportIconsProvider) {
        return new SportLinkHelper(sportProvider, sportIconsProvider);
    }

    @Provides
    @Singleton
    RefreshTimeProvider provideRefreshTimeProvider(LoginResponseStore loginResponseStore) {
        return new RefreshTimeProvider(loginResponseStore);
    }
}
