package com.sportingbet.sportsbook.general.network;

import com.sportingbet.sportsbook.general.network.error.model.ServiceErrorGroup;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Copyright 2014 Tomasz Morcinek. All rights reserved.
 */
public class CallbackWrapper<T> implements Callback<T> {

    private NetworkResponseListener<T> responseListener;

    private ProgressController[] progressControllers;

    public CallbackWrapper(NetworkResponseListener<T> responseListener, ProgressController... progressControllers) {
        this.responseListener = responseListener;
        this.progressControllers = progressControllers;
        preExecute();
    }

    public void cancel() {
        responseListener = null;
        postExecuteWithSuccess(false);
        progressControllers = new ProgressController[0];
    }

    private void preExecute() {
        for (ProgressController progressController : progressControllers) {
            progressController.preExecute();
        }
    }

    private void postExecuteWithSuccess(boolean success) {
        for (ProgressController progressController : progressControllers) {
            progressController.postExecuteWithSuccess(success);
        }
    }

    @Override
    public void success(final T object, Response response) {
        if (responseListener != null) {
            responseListener.success(object);
            postExecuteWithSuccess(true);
        }
    }

    @Override
    public void failure(final RetrofitError error) {
        if (responseListener != null) {
            responseListener.failure(error);
            postExecuteWithSuccess(false);
        }
    }
}
