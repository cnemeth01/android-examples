package com.sportingbet.sportsbook.content.fragments.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.general.controllers.EventsHeaderController;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandlerFactory;
import com.sportingbet.sportsbook.general.adapter.handlers.history.HistoryHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.adapter.handlers.ViewHolderHandler;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.sports.configuration.CouponType;
import com.sportingbet.sportsbook.sports.configuration.SportConfigProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.Getter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HomeAdapter extends HistoryHandlersRecyclerViewAdapter<Event> {

    private final EventsHeaderController eventsHeaderController;

    private final SportConfigProvider sportConfigProvider;

    private final EventCouponHandlerFactory eventCouponHandlerFactory;

    @Getter
    private Map<Sport, List<Event>> headerEvents;

    protected HomeAdapter(Context context, EventsHeaderController eventsHeaderController, EventCouponHandlerFactory eventCouponHandlerFactory, SportConfigProvider sportConfigProvider) {
        super(context);
        this.eventsHeaderController = eventsHeaderController;
        this.eventCouponHandlerFactory = eventCouponHandlerFactory;
        this.sportConfigProvider = sportConfigProvider;
        addViewHolderHandler(Sport.class, new SportViewHolderHandler());
    }

    public void setHeaderEvents(Map<Sport, List<Event>> headerEvents) {
        this.headerEvents = headerEvents;
        setList(createItems(headerEvents));
    }

    private List<Object> createItems(Map<Sport, List<Event>> headerEvents) {
        List<Object> eventList = Lists.newArrayList();
        for (Sport sport : headerEvents.keySet()) {
            eventList.add(sport);
            if (getItemCount() == 0 || isHeaderOpened(sport)) {
                eventList.addAll(headerEvents.get(sport));
            }
        }
        return eventList;
    }

    @Override
    protected int getTypeItemViewType(Event item) {
        return eventCouponHandlerFactory.getItemTypeForEvent(item, getCouponTypeForEvent(item.getSportId()));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateTypeViewHolder(ViewGroup parent, int viewType) {
        return eventCouponHandlerFactory.getHandlerForItemType(viewType).onCreateViewHolder(context, parent);
    }

    @Override
    protected void onBindTypeViewHolder(RecyclerView.ViewHolder holder, Event item, Event previousItem, int position) {
        initializeOnClickListener(holder, item);
        eventCouponHandlerFactory.getHandlerForEvent(item, getCouponTypeForEvent(item.getSportId())).onBindViewHolder(holder, item, previousItem);
    }

    private class SportViewHolderHandler implements ViewHolderHandler<Sport, SportViewHolderHandler.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            return new ViewHolder(layoutInflater.inflate(R.layout.home_header_item, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, Sport object) {
            initializeOnClickListener(holder, object);
            setupHeaderItem(holder, object);
        }

        private void setupHeaderItem(ViewHolder holder, Sport object) {
            holder.title.setText(context.getString(R.string.sport_highlights, object.getName()));
            if (!isHeaderOpened(object)) {
                holder.eventsHeader.setVisibility(View.GONE);
            } else {
                holder.eventsHeader.setVisibility(View.VISIBLE);
                eventsHeaderController.setup(holder.eventsHeader, getCouponTypeForEvent(object.getId()));
            }
            holder.itemView.setActivated(isHeaderOpened(object));
        }

        protected class ViewHolder extends RecyclerView.ViewHolder {

            public TextView title;

            public ViewGroup eventsHeader;

            public ViewHolder(View itemView) {
                super(itemView);
                title = (TextView) itemView.findViewById(R.id.title);
                eventsHeader = (ViewGroup) itemView.findViewById(R.id.header_container);
            }
        }
    }

    private CouponType getCouponTypeForEvent(long sporitId) {
        return sportConfigProvider.getDefaultCouponTypeForSportId(sporitId);
    }

    public boolean isHeaderOpened(Sport sport) {
        return getItems().contains(headerEvents.get(sport).get(0));
    }

    public void openHeader(Sport sport) {
        List<Event> insertList = headerEvents.get(sport);
        int position = getItems().indexOf(sport);
        notifyItemChanged(position);
        getItems().addAll(position + 1, insertList);
        notifyItemRangeInserted(position + 1, insertList.size());
    }

    public void closeHeader(Sport sport) {
        List<Object> removeList = new ArrayList<Object>(headerEvents.get(sport));
        getItems().removeAll(removeList);
        int position = getItems().indexOf(sport);
        notifyItemChanged(position);
        notifyItemRangeRemoved(position + 1, removeList.size());
    }
}