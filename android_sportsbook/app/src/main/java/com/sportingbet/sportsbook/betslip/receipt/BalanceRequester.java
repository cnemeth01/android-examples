package com.sportingbet.sportsbook.betslip.receipt;

import com.sportingbet.sportsbook.general.network.requesters.Requester;
import com.sportingbet.sportsbook.general.network.services.AccountService;
import com.sportingbet.sportsbook.home.account.model.BalanceDetailsWrapper;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BalanceRequester extends Requester<BalanceDetailsWrapper> {

    private final AccountService accountService;

    public BalanceRequester(AccountService accountService) {
        this.accountService = accountService;
    }

    public void requestGetBalance() {
        accountService.getBalance(getCallback());
    }
}
