package com.sportingbet.sportsbook.mybets.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@JsonDeserialize(using = MyBetStatusDeserializer.class)
public enum MyBetStatus {
    LOST, WON, CASHOUT, CLOSED, VOIDED, CANCELLED, CASHEDOUT, UNKNOWN;
}

class MyBetStatusDeserializer extends JsonDeserializer<MyBetStatus> {

    @Override
    public MyBetStatus deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        try {
            return MyBetStatus.valueOf(p.getText());
        } catch (Exception e) {
            return MyBetStatus.UNKNOWN;
        }
    }
}
