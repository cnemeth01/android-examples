package com.sportingbet.sportsbook.mybets;

import com.sportingbet.sportsbook.general.network.CallbackWrapper;
import com.sportingbet.sportsbook.general.network.requesters.Requester;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.network.services.AccountService;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetsResponseType;
import com.sportingbet.sportsbook.mybets.model.MyBetType;
import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsRequester extends Requester<MyBetsResponse> {

    protected final AccountService accountService;

    public MyBetsRequester(AccountService accountService) {
        this.accountService = accountService;
    }

    public void requestMyBets(MyBetType myBetType) {
        accountService.getMyBets(myBetType, getCallback());
    }
}
