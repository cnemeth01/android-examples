package com.sportingbet.sportsbook.general.network.error.model;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ServiceError {

    @Getter
    private String errorCode;

    @Getter
    private String localisedErrorMessage;
}
