package com.sportingbet.sportsbook.general.adapter.handlers;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public interface ViewHolderHandler<T, R extends RecyclerView.ViewHolder> {

    R onCreateViewHolder(ViewGroup parent);

    void onBindViewHolder(R holder, T object);
}
