package com.sportingbet.sportsbook.home.account.webview.activities;

import android.view.Menu;
import android.view.MenuItem;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.home.account.webview.WebURLFactory;
import com.sportingbet.sportsbook.home.account.webview.session.TempSessionWebViewActivity;
import com.sportingbet.sportsbook.general.commands.CommandController;
import com.sportingbet.sportsbook.general.network.progress.ProgressDialogController;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyAccountActivity extends TempSessionWebViewActivity implements CommandController.Delegate {

    @Inject
    @Named("logout")
    CommandController commandController;

    @Override
    protected String getWebViewURL(String tempSessionId) {
        return WebURLFactory.getMyAccountURL(tempSessionId);
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.my_account_title;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_account, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                commandController.setDelegate(this);
                commandController.setProgressController(new ProgressDialogController(this));
                commandController.start();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void commandControllerDidFinish() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void commandControllerDidFinishWithError(RetrofitError networkError) {
        errorHandler.handleError(networkError);
    }
}
