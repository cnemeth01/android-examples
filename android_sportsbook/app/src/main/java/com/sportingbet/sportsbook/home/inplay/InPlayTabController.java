package com.sportingbet.sportsbook.home.inplay;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.home.navigation.configuration.NavigationConfiguration;
import com.sportingbet.sportsbook.home.pager.fragment.NavigationDelegate;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.sports.SportIconsProvider;

import java.util.Map;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class InPlayTabController implements View.OnClickListener {

    private final NavigationConfiguration navigationConfiguration;
    private final SportIconsProvider sportIconsProvider;

    private final Context context;

    private NavigationDelegate delegate;

    private Map<Long, View> views;

    public InPlayTabController(Context context, NavigationConfiguration navigationConfiguration, SportIconsProvider sportIconsProvider) {
        this.navigationConfiguration = navigationConfiguration;
        this.sportIconsProvider = sportIconsProvider;
        this.context = context;
    }

    public void setup(ViewGroup tabLayout, NavigationDelegate delegate) {
        this.delegate = delegate;
        this.views = Maps.newHashMap();
        for (Sport sport : navigationConfiguration.getInPlaySports()) {
            createInPlayTab(tabLayout, sport);
        }
    }

    public void initializeActivatedTabId(Long activatedTabId) {
        if (activatedTabId == null) {
            setupInitialState();
        } else {
            setupView(views.get(activatedTabId), true);
        }
    }

    public Long getActiveTabId() {
        for (Long tabId : views.keySet()) {
            if (views.get(tabId).isActivated()) {
                return tabId;
            }
        }
        return null;
    }

    private void setupInitialState() {
        Sport sport = getFirstSport();
        onClick(views.get(sport.getId()));
    }

    private Sport getFirstSport() {
        return navigationConfiguration.getInPlaySports().get(0);
    }


    private View createInPlayTab(ViewGroup tabLayout, Sport sport) {
        View view = LayoutInflater.from(context).inflate(R.layout.in_play_tab_item, null);
        ImageView inPlayIcon = (ImageView) view.findViewById(R.id.in_play_item_icon);
        TextView inPlayText = (TextView) view.findViewById(R.id.in_play_item_text);
        inPlayIcon.setImageDrawable(sportIconsProvider.getStateListDrawableFromSportId(sport.getId()));
        inPlayText.setText(sport.getName());
        tabLayout.addView(view);
        view.setOnClickListener(this);
        view.setTag(sport);
        views.put(sport.getId(), view);
        return view;
    }

    protected void setupView(View view, boolean selected) {
        view.setActivated(selected);
    }

    @Override
    public void onClick(View clickedView) {
        if (!clickedView.isActivated()) {
            for (View view : views.values()) {
                setupView(view, false);
            }
            setupView(clickedView, true);
            delegate.switchFragment(InPlaySportFragment.newInstance((Sport) clickedView.getTag()));
            delegate.notifyChildFragmentOnResume();
        }
    }
}
