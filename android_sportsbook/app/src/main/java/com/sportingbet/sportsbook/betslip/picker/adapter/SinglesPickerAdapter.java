package com.sportingbet.sportsbook.betslip.picker.adapter;

import android.view.View;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SinglesPickerAdapter extends AbstractPickerAdapter implements View.OnClickListener {

    public SinglesPickerAdapter(Delegate delegate) {
        super(delegate);
    }

    @Override
    public Object getPayout() {
        return 0;
    }

    @Override
    public double getTotalStake() {
        return 0;
    }

    @Override
    public boolean isEachWaySelected() {
        return false;
    }
}
