package com.sportingbet.sportsbook.home.betslip;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.BetslipActivity;
import com.sportingbet.sportsbook.coupons.betslip.model.BetslipResponse;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipButtonController<E> implements View.OnClickListener {

    private final EventBus eventBus;

    private Button button;

    private BetslipResponse betslipResponse;

    public BetslipButtonController(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public void register(Button button) {
        this.button = button;
        eventBus.register(this);
    }

    public void unregister() {
        eventBus.unregister(this);
    }

    public void updateButton(long[] selections) {
        handleBetResponse(new BetslipResponse(selections.length));
    }

    @Subscribe
    public void handleBetslipEvent(BetslipButtonEvent buttonEvent) {
        if (betslipResponse.getNumberOfSelectionsInBetSlip() > 0) {
            switch (buttonEvent) {
                case SHOW_BUTTON:
                    showButton();
                    break;
                case HIDE_BUTTON:
                    hideButton();
                    break;
            }
        }
    }

    @Subscribe
    public void handleBetResponse(BetslipResponse betslipResponse) {
        this.betslipResponse = betslipResponse;
        updateButtonText(betslipResponse.getNumberOfSelectionsInBetSlip());
        if (betslipResponse.getNumberOfSelectionsInBetSlip() > 0) {
            showButton();
        } else {
            hideButton();
        }
    }

    private void updateButtonText(int number) {
        button.setText(String.valueOf(number));
    }

    private void showButton() {
        button.setOnClickListener(this);
        updateButtonVisibility(View.VISIBLE, R.anim.slide_in_bottom);
    }

    private void hideButton() {
        button.setOnClickListener(null);
        updateButtonVisibility(View.GONE, R.anim.slide_out_bottom);
    }

    private void updateButtonVisibility(int visibility, int animId) {
        if (button.getVisibility() != visibility) {
            button.setAnimation(prepareAnimationWithId(animId));
            button.setVisibility(visibility);
        }
    }

    private Animation prepareAnimationWithId(int animId) {
        return AnimationUtils.loadAnimation(button.getContext(), animId);
    }

    @Override
    public void onClick(View view) {
        Activity activity = (Activity) view.getContext();
        activity.startActivity(new Intent(activity, BetslipActivity.class));
    }
}
