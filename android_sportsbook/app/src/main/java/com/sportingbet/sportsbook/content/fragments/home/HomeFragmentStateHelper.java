package com.sportingbet.sportsbook.content.fragments.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.event.Event;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HomeFragmentStateHelper {

    private final HomeAdapter homeAdapter;

    public HomeFragmentStateHelper(HomeAdapter homeAdapter) {
        this.homeAdapter = homeAdapter;
    }

    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            List<Sport> headers = savedInstanceState.getParcelableArrayList(List.class.getName());
            if (headers != null) {
                homeAdapter.setHeaderEvents(createHeaderEvents(headers, savedInstanceState));
            }
        }
    }

    private Map<Sport, List<Event>> createHeaderEvents(List<Sport> headers, @NonNull Bundle savedInstanceState) {
        Map<Sport, List<Event>> headerEvents = Maps.newLinkedHashMap();
        for (Sport sport : headers) {
            headerEvents.put(sport, savedInstanceState.<Event>getParcelableArrayList(sport.getName()));
        }
        return headerEvents;
    }

    public void onSaveInstanceState(Bundle outState) {
        Map<Sport, List<Event>> headerEvents = homeAdapter.getHeaderEvents();
        if (headerEvents != null) {
            outState.putParcelableArrayList(List.class.getName(), Lists.newArrayList(headerEvents.keySet()));
            for (Sport sport : headerEvents.keySet()) {
                outState.putParcelableArrayList(sport.getName(), new ArrayList<>(headerEvents.get(sport)));
            }
        }
    }
}
