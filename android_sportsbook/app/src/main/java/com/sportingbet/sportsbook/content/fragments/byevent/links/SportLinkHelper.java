package com.sportingbet.sportsbook.content.fragments.byevent.links;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.events.EventClass;
import com.sportingbet.sportsbook.sports.SportIconsProvider;
import com.sportingbet.sportsbook.sports.SportProvider;

import java.util.List;
import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportLinkHelper {

    private final Map<Long, SportLink> sportLinks = Maps.newHashMap();

    public SportLinkHelper(SportProvider sportProvider, SportIconsProvider sportIconsProvider) {
        sportLinks.put(Sport.INTERNATIONAL_HORSE_RACING, createSportLink(sportProvider.getSportWithId(Sport.UK_HORSE_RACING), sportIconsProvider));
        sportLinks.put(Sport.UK_HORSE_RACING, createSportLink(new Sport(Sport.INTERNATIONAL_HORSE_RACING, BuildConfig.INTERNATIONAL_HORSE_RACING_NAME), sportIconsProvider));
    }

    private SportLink createSportLink(Sport sport, SportIconsProvider sportIconsProvider) {
        return new SportLink(sport, sportIconsProvider.getWhiteIconResourceIdForSport(sport.getId()));
    }

    public List<Object> prepareItems(List<EventClass> eventClasses, Sport sport) {
        List<Object> objects = Lists.newArrayList();
        objects.addAll(eventClasses);
        if (sportLinks.containsKey(sport.getId())) {
            objects.add(sportLinks.get(sport.getId()));
        }
        return objects;
    }
}
