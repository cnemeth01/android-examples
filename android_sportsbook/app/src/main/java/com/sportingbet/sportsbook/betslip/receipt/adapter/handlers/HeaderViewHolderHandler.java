package com.sportingbet.sportsbook.betslip.receipt.adapter.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.receipt.adapter.model.BetslipReceiptHeader;

public class HeaderViewHolderHandler extends AbstractViewHolderHandler<BetslipReceiptHeader, HeaderViewHolderHandler.ViewHolder> {

    public HeaderViewHolderHandler(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(createView(parent, R.layout.betreceipt_header));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, BetslipReceiptHeader object) {
        holder.title.setText(object.getMessage());
        holder.warningText.setVisibility(object.isHasFailedBets() ? View.VISIBLE : View.GONE);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView title;
        private final View warningText;

        private ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            warningText = view.findViewById(R.id.warning_text);
        }
    }
}
