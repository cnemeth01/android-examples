package com.sportingbet.sportsbook.betslip.adapter.multiples;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.betslip.adapter.BetslipAdapter;
import com.sportingbet.sportsbook.betslip.adapter.FooterViewHolderHandler;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MultiplesAdapter extends BetslipAdapter {

    @Getter
    private final MultiplesValueController betslipValueController;

    public MultiplesAdapter(Context context, Delegate delegate) {
        super(context, delegate);
        betslipValueController = new MultiplesValueController(delegate.getBetsValueContainer());
        betslipValueController.setDelegate(this);

        footerViewHolderHandler = new FooterViewHolderHandler(context, betslipValueController, this, this);
        sectionsViewHolderHandler = new MultiplesSectionsViewHolderHandler(context, this, betslipValueController);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        ViewHolder viewHolder = (ViewHolder) super.onCreateViewHolder(parent);
        viewHolder.stakePicker.setVisibility(View.GONE);
        return viewHolder;
    }
}
