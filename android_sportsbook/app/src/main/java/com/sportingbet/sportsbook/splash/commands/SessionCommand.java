package com.sportingbet.sportsbook.splash.commands;

import com.sportingbet.sportsbook.betslip.controller.BetslipStore;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.general.LoginResponseStore;
import com.sportingbet.sportsbook.general.commands.NetworkCommand;

import retrofit.client.Response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SessionCommand extends NetworkCommand<Response> {

    private final SessionRequester sessionRequester;
    private final LoginResponseStore loginResponseStore;
    private final BetsValueContainer betsValueContainer;
    private BetslipStore betslipStore;

    public SessionCommand(SessionRequester sessionRequester, LoginResponseStore loginResponseStore, BetslipStore betslipStore, BetsValueContainer betsValueContainer) {
        this.sessionRequester = sessionRequester;
        this.loginResponseStore = loginResponseStore;
        this.betsValueContainer = betsValueContainer;
        this.betslipStore = betslipStore;

    }

    @Override
    public void execute() {
        betsValueContainer.clearBetValues();
        loginResponseStore.setLoginResponse(null);
        betslipStore.setSelections(new long[0]);
        sessionRequester.getActiveSessionId(this);
    }
}
