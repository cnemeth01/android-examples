package com.sportingbet.sportsbook.home.mybets;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.LoginResponseStore;
import com.sportingbet.sportsbook.home.HomeActivity;
import com.sportingbet.sportsbook.home.account.login.LoginActivity;
import com.sportingbet.sportsbook.mybets.MyBetsActivity;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsButtonController implements View.OnClickListener {

    private final Activity homeActivity;
    private final LoginResponseStore loginResponseStore;

    public MyBetsButtonController(Activity activity, LoginResponseStore loginResponseStore) {
        this.homeActivity = activity;
        this.loginResponseStore = loginResponseStore;
    }

    public void register(View view) {
        view.setOnClickListener(this);
    }

    private void goToMyBets() {
        homeActivity.startActivity(new Intent(homeActivity, MyBetsActivity.class));
    }

    private void goToLogin() {
        homeActivity.startActivityForResult(createLoginIntent(), HomeActivity.LOGIN_ACTIVITY_MYBETS_REQUEST_CODE);
    }

    private Intent createLoginIntent() {
        Intent intent = new Intent(homeActivity, LoginActivity.class);
        intent.putExtra(Intent.EXTRA_TITLE, R.string.login_my_bets_title);
        return intent;
    }

    public void handleMyBetsButton() {
        if (loginResponseStore.isLoggedIn()) {
            goToMyBets();
        } else {
            goToLogin();
        }
    }

    @Override
    public void onClick(View view) {
        handleMyBetsButton();
    }
}
