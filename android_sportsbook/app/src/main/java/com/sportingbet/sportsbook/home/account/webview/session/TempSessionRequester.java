package com.sportingbet.sportsbook.home.account.webview.session;

import com.sportingbet.sportsbook.general.network.CallbackWrapper;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.network.services.SessionService;

import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class TempSessionRequester implements NetworkResponseListener<Response> {

    private static final String TEMP_SESSION_ID_KEY = "TSID";

    private final SessionService sessionService;

    private NetworkResponseListener<String> responseListener;

    public TempSessionRequester(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void getTempSessionId(NetworkResponseListener<String> responseListener, ProgressController... progressControllers) {
        this.responseListener = responseListener;
        sessionService.getTempSessionId(new CallbackWrapper<>(this, progressControllers));
    }

    public void cancelRequest() {
        responseListener = null;
    }

    @Override
    public void success(Response object) {
        if (responseListener != null) {
            responseListener.success(extractTempSessionId(object));
        }
    }

    private String extractTempSessionId(Response response) {
        for (Header header : response.getHeaders()) {
            if (header.getName().equals(TEMP_SESSION_ID_KEY)) {
                return header.getValue();
            }
        }
        return null;
    }

    @Override
    public void failure(RetrofitError error) {
        if (responseListener != null) {
            responseListener.failure(error);
        }
    }
}
