package com.sportingbet.sportsbook.general.adapter.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.general.adapter.AbstractRecyclerViewAdapter;

import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class AbstractHandlersRecyclerViewAdapter extends AbstractRecyclerViewAdapter<Object, RecyclerView.ViewHolder> {

    private Map<Integer, ViewHolderHandler> typesViewHolderHandlers = Maps.newHashMap();

    public AbstractHandlersRecyclerViewAdapter(Context context) {
        super(context);
    }

    public void addViewHolderHandler(Class type, ViewHolderHandler viewHolderHandler) {
        typesViewHolderHandlers.put(type.hashCode(), viewHolderHandler);
    }

    private ViewHolderHandler getViewHolderHandler(int position) {
        return typesViewHolderHandlers.get(getItemViewType(position));
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getClass().hashCode();
    }

    @Override
    public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return typesViewHolderHandlers.get(viewType).onCreateViewHolder(parent);
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        getViewHolderHandler(position).onBindViewHolder(holder, getItem(position));
    }
}
