package com.sportingbet.sportsbook.general.network.progress;

import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.coupons.util.CouponHelper;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public class BetProgressBarController extends ProgressControllerAdapter {

    private final CouponLayout view;

    public BetProgressBarController(CouponLayout view) {
        this.view = view;
    }

    @Override
    public void preExecute() {
        CouponHelper.setupCouponLayout(view, false, true);
    }
}
