package com.sportingbet.sportsbook.home.account.login;

import android.app.Activity;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.sportingbet.sportsbook.home.account.model.LoginRequestBody;
import com.sportingbet.sportsbook.general.helpers.SharedPreferencesHelper;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class LoginController implements TextWatcher {

    public static final String PREFERENCE_KEY = "USER_NAME";

    private final Activity activity;
    private final SharedPreferencesHelper sharedPreferencesHelper;

    private EditText userName;
    private EditText password;
    private SwitchCompat userSwitch;
    private Button loginButton;

    public LoginController(Activity activity, SharedPreferencesHelper sharedPreferencesHelper) {
        this.activity = activity;
        this.sharedPreferencesHelper = sharedPreferencesHelper;
    }

    public void registerViews(EditText userName, EditText password, SwitchCompat userSwitch, Button loginButton) {
        this.userName = userName;
        this.password = password;
        this.userSwitch = userSwitch;
        this.loginButton = loginButton;

        setupEditTexts();
        setupFocus();
        setupLoginButtonState();
    }

    private void setupEditTexts() {
        userName.addTextChangedListener(this);
        password.addTextChangedListener(this);
    }

    private void setupFocus() {
        if (sharedPreferencesHelper.contains(PREFERENCE_KEY)) {
            this.userName.setText(sharedPreferencesHelper.getString(PREFERENCE_KEY));
            this.userSwitch.setChecked(true);
            this.password.requestFocus();
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        } else {
            this.userName.requestFocus();
        }
    }

    public void update() {
        if (userSwitch.isChecked()) {
            sharedPreferencesHelper.putString(PREFERENCE_KEY, getEditTextString(userName));
        } else {
            sharedPreferencesHelper.remove(PREFERENCE_KEY);
        }
    }

    public LoginRequestBody getLoginRequestBody() {
        return new LoginRequestBody(getEditTextString(userName), getEditTextString(password));
    }

    private String getEditTextString(EditText editText) {
        return editText.getText().toString();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        setupLoginButtonState();
    }

    private void setupLoginButtonState() {
        loginButton.setEnabled(editTextsNotEmpty());
    }

    private boolean editTextsNotEmpty() {
        for (EditText editText : new EditText[]{userName, password}) {
            if (editText.getText().length() == 0) {
                return false;
            }
        }
        return true;
    }
}
