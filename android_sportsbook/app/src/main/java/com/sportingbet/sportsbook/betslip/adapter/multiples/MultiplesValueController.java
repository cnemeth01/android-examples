package com.sportingbet.sportsbook.betslip.adapter.multiples;

import android.view.View;

import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.betslip.adapter.BetTypeValueController;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.betslip.model.types.BetType;
import com.sportingbet.sportsbook.betslip.picker.adapter.BetTypePickerAdapter;
import com.sportingbet.sportsbook.betslip.picker.adapter.PickerAdapter;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MultiplesValueController extends BetTypeValueController {

    private String paymentMark;

    public MultiplesValueController(BetsValueContainer betsValueContainer) {
        super(betsValueContainer);
    }

    @Override
    public void setup(BetslipInformation betslipInformation) {
        super.setup(betslipInformation);
        paymentMark = betslipInformation.getPaymentMark();
        Map<Object, BetTypePickerAdapter> betTypePickerAdapters = Maps.newHashMap();
        for (BetType betType : betslipInformation.getMultiples()) {
            BetTypePickerAdapter pickerAdapter = pickerAdapters.get(betType.getBetTypeName());
            if (pickerAdapter != null) {
                betTypePickerAdapters.put(betType.getBetTypeName(), pickerAdapter);
            }
        }
        pickerAdapters.clear();
        pickerAdapters.putAll(betTypePickerAdapters);
    }

    @Override
    protected void preparePickerAdapter(StakePicker stakePicker, View eachWayView, BetType betType) {
        BetTypePickerAdapter pickerAdapter = pickerAdapters.get(betType.getBetTypeName());
        if (pickerAdapter == null) {
            pickerAdapter = new BetTypePickerAdapter(this);
            pickerAdapters.put(betType.getBetTypeName(), pickerAdapter);
        }
        pickerAdapter.setup(stakePicker, pickerValueProvider, betType, paymentMark);
        updateBetTypePickerAdapter(pickerAdapter);
    }

    @Override
    public void pickerAdapterUpdated(PickerAdapter pickerAdapter) {
        updateBetsValueContainer((BetTypePickerAdapter) pickerAdapter);
        super.pickerAdapterUpdated(pickerAdapter);
    }
}
