package com.sportingbet.sportsbook.betslip.controller;

import com.google.common.collect.Sets;
import com.google.common.eventbus.EventBus;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.model.BetSelection;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.coupons.betslip.model.BetslipResponse;
import com.sportingbet.sportsbook.coupons.betslip.requesters.BetRequester;
import com.sportingbet.sportsbook.coupons.betslip.requesters.GetBetsRequester;
import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.network.services.BetService;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Set;

import lombok.Getter;
import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipControllerImpl implements BetslipController {

    private final BetService betService;

    private final EventBus eventBus;

    @Getter
    private final BetsValueContainer betsValueContainer;

    private Set<Long> selections = Sets.newHashSet();

    @Getter
    private BetslipInformation betslipInformation;

    public BetslipControllerImpl(BetService betService, EventBus eventBus, BetsValueContainer betsValueContainer) {
        this.betService = betService;
        this.eventBus = eventBus;
        this.betsValueContainer = betsValueContainer;
    }

    @Override
    public long[] getSelections() {
        return ArrayUtils.toPrimitive(selections.toArray(new Long[selections.size()]));
    }

    @Override
    public void setSelections(long[] selections) {
        this.selections = Sets.newHashSet(ArrayUtils.toObject(selections));
        notifyBetslipChanged();
    }

    @Override
    public boolean containsSelection(long selectionId) {
        return this.selections.contains(selectionId);
    }

    @Override
    public void addBet(Event event, Book book, final Selection selection, final ErrorHandler errorHandler, ProgressController... progressControllers) {
        BetRequester betRequester = new BetRequester(betService);
        betRequester.setupResponseListener(new NetworkResponseListener<BetslipResponse>() {
            @Override
            public void success(BetslipResponse object) {
                selections.add(selection.getId());
                notifyBetslipChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                errorHandler.handleError(error);
            }
        });
        betRequester.prepareRequest(progressControllers);
        betRequester.addBet(event, book, selection);
    }

    @Override
    public void removeBet(final Selection selection, final ErrorHandler errorHandler, ProgressController... progressControllers) {
        BetRequester betRequester = new BetRequester(betService);
        betRequester.setupResponseListener(new NetworkResponseListener<BetslipResponse>() {
            @Override
            public void success(BetslipResponse object) {
                selections.remove(selection.getId());
                betsValueContainer.removeSelectedBet(selection);
                notifyBetslipChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                errorHandler.handleError(error);
            }
        });
        betRequester.prepareRequest(progressControllers);
        betRequester.removeBet(selection);
    }

    @Override
    public void removeAllBets(final ErrorHandler errorHandler, ProgressController... progressControllers) {
        BetRequester betRequester = new BetRequester(betService);
        betRequester.setupResponseListener(new NetworkResponseListener<BetslipResponse>() {
            @Override
            public void success(BetslipResponse object) {
                clearSelections();
                clearBetsValueContainer();
                notifyBetslipChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                errorHandler.handleError(error);
            }
        });
        betRequester.prepareRequest(progressControllers);
        betRequester.removeAllBets();
    }

    protected void notifyBetslipChanged() {
        eventBus.post(new BetslipResponse(selections.size()));
    }

    @Override
    public void getBets(final BetslipInformationDelegate delegate, final ErrorHandler errorHandler, ProgressController... progressControllers) {
        GetBetsRequester getBetsRequester = new GetBetsRequester(betService);
        getBetsRequester.setupResponseListener(new NetworkResponseListener<BetslipInformation>() {
            @Override
            public void success(BetslipInformation object) {
                setupBetslipInformation(object);
                notifyBetslipChanged();
                delegate.success(object);
            }

            @Override
            public void failure(RetrofitError error) {
                errorHandler.handleError(error);
            }
        });
        getBetsRequester.prepareRequest(progressControllers);
        getBetsRequester.getBets();
    }

    @Override
    public void clearBets() {
        clearSelections();
        notifyBetslipChanged();
    }

    private void setupBetslipInformation(BetslipInformation betslipInformation) {
        this.betslipInformation = betslipInformation;
        clearSelections();
        if (betslipInformation.isEmpty()) {
            clearBetsValueContainer();
        } else {
            setupSelection(betslipInformation);
            setupBetsValueContainer(betslipInformation);
        }
    }

    private void setupSelection(BetslipInformation betslipInformation) {
        for (BetSelection betSelection : betslipInformation.getBetSelections()) {
            selections.add(betSelection.getSelectionId());
        }
    }

    private void clearSelections() {
        selections.clear();
    }

    private void clearBetsValueContainer() {
        betsValueContainer.clearBetValues();
    }

    private void setupBetsValueContainer(BetslipInformation object) {
        betsValueContainer.setupBetGroups(object.getBetTypes().getBetGroups());
    }
}
