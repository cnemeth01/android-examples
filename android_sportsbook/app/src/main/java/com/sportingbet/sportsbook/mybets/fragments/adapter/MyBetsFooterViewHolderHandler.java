package com.sportingbet.sportsbook.mybets.fragments.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.receipt.adapter.handlers.AbstractViewHolderHandler;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsFooterViewHolderHandler extends AbstractViewHolderHandler<String, MyBetsFooterViewHolderHandler.ViewHolder> {

    private final View.OnClickListener onClickListener;

    public MyBetsFooterViewHolderHandler(Context context, View.OnClickListener onClickListener) {
        super(context);
        this.onClickListener = onClickListener;
    }

    @Override
    public MyBetsFooterViewHolderHandler.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(createView(parent, R.layout.my_bets_footer));
    }

    @Override
    public void onBindViewHolder(MyBetsFooterViewHolderHandler.ViewHolder holder, String object) {
        holder.viewAccountButton.setOnClickListener(onClickListener);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final View viewAccountButton;

        private ViewHolder(View view) {
            super(view);
            viewAccountButton = view.findViewById(R.id.view_account_button);
        }
    }
}
