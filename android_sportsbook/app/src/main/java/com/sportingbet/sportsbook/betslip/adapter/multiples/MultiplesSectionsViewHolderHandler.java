package com.sportingbet.sportsbook.betslip.adapter.multiples;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.adapter.AbstractViewHolderHandler;
import com.sportingbet.sportsbook.betslip.model.types.BetType;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MultiplesSectionsViewHolderHandler extends AbstractViewHolderHandler<MultiplesSectionsViewHolderHandler.ViewHolder> {

    private final MultiplesValueController betslipValueController;

    public MultiplesSectionsViewHolderHandler(Context context, DataSource dataSource, MultiplesValueController betslipValueController) {
        super(context, dataSource);
        this.betslipValueController = betslipValueController;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(layoutInflater.inflate(R.layout.betslip_sections, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder) {
        viewHolder.getContainer().removeAllViews();
        for (BetType betType : dataSource.getBetslipInformation().getMultiples()) {
            View stakeView = layoutInflater.inflate(R.layout.betslip_stake_multiple, viewHolder.getContainer(), false);
            viewHolder.getContainer().addView(stakeView);
            setupTitleView(stakeView, betType);
            setupStakePicker(stakeView, betType);
            setupWarning(stakeView, betType);
        }
    }

    private void setupStakePicker(View stakeView, BetType betType) {
        StakePicker stakePicker = (StakePicker) stakeView.findViewById(R.id.stake_picker);
        betslipValueController.registerPickerAdapter(stakePicker, null, betType);
    }

    private void setupTitleView(View stakeView, BetType betType) {
        TextView titleView = (TextView) stakeView.findViewById(R.id.title);
        titleView.setText(context.getString(R.string.betslip_multiples_format, betType.getBetTypeName(), betType.getNumberOfBets()));
    }

    private void setupWarning(View stakeView, BetType betType) {
        TextView warningText = (TextView) stakeView.findViewById(R.id.warning_text);
        View warningIndicator = stakeView.findViewById(R.id.warning_indicator);
        if (betType.getErrorCode() != null) {
            stakeView.setBackgroundResource(R.color.warning_background_shadow);
            warningIndicator.setVisibility(View.VISIBLE);
            warningText.setVisibility(View.VISIBLE);
            warningText.setText(betType.getLocalisedErrorMessage());
        } else {
            stakeView.setBackgroundResource(android.R.color.transparent);
            warningIndicator.setVisibility(View.GONE);
            warningText.setVisibility(View.GONE);
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public ViewGroup getContainer() {
            return (ViewGroup) itemView;
        }
    }
}
