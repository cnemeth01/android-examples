package com.sportingbet.sportsbook.mybets.pager;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.pager.FragmentStatePagerAdapter;
import com.sportingbet.sportsbook.mybets.fragments.MyBetsFragment;
import com.sportingbet.sportsbook.mybets.fragments.MyBetsOpenFragment;
import com.sportingbet.sportsbook.mybets.model.MyBetNavigationItem;
import com.sportingbet.sportsbook.mybets.model.MyBetType;

import java.util.List;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsPagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> fragments = Lists.newArrayList();

    public MyBetsPagerAdapter(Context context, FragmentManager fragmentManager) {
        super(fragmentManager);
        if (fragmentManager.getFragments() == null) {
            fragments.add(createOpenFragment(context));
            fragments.add(createSettledFragment(context));
        } else {
            fragments.addAll(fragmentManager.getFragments());
        }
    }

    private MyBetsFragment createOpenFragment(Context context) {
        return MyBetsFragment.prepareInstance(new MyBetsOpenFragment(), new MyBetNavigationItem(getString(context, R.string.mybets_open), MyBetType.open));
    }

    private MyBetsFragment createSettledFragment(Context context) {
        return MyBetsFragment.prepareInstance(new MyBetsFragment(), new MyBetNavigationItem(getString(context, R.string.mybets_settled), MyBetType.settled));
    }

    private String getString(Context context, int stringId) {
        return context.getResources().getString(stringId);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return ((MyBetsFragment) fragments.get(position)).getTitle();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
