package com.sportingbet.sportsbook.betslip.placebets;

import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetRequest;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetResponse;
import com.sportingbet.sportsbook.general.network.requesters.Requester;
import com.sportingbet.sportsbook.general.network.services.BetService;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class PlaceBetRequester extends Requester<PlaceBetResponse> {

    private final BetService betService;

    public PlaceBetRequester(BetService betService) {
        this.betService = betService;
    }

    public void requestPlaceBet(PlaceBetRequest placeBetRequest) {
        betService.placeBet(placeBetRequest, getCallback());
    }
}
