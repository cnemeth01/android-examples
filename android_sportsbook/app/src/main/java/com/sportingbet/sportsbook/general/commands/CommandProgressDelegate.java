package com.sportingbet.sportsbook.general.commands;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface CommandProgressDelegate<E> {

    void commandDidFinish(Command command);

    void commandDidFinishWithError(Command command, E error);
}
