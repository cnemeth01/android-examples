package com.sportingbet.sportsbook.mybets.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetWrapper {

    @Getter
    private MyBet bet;

    @Setter
    @Getter
    private boolean hasError;

    @Setter
    @Getter
    private boolean isPriceChange;

    @Setter
    @Getter
    private String errorMessage;

    public MyBetWrapper(MyBet myBet) {
        this.bet = myBet;
    }
}
