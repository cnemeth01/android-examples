package com.sportingbet.sportsbook.model.event;

import java.util.List;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class Scores implements android.os.Parcelable {

    @Getter
    private String fullScoreFormatted;

    @Getter
    private String participantATotalSetScore;

    @Getter
    private String participantBTotalSetScore;

    @Getter
    private String participantACurrentSetScore;

    @Getter
    private String participantBCurrentSetScore;

    @Getter
    private String participantACurrentGameScore;

    @Getter
    private String participantBCurrentGameScore;

    @Getter
    private List<String> participantAScoreHistory;

    @Getter
    private List<String> participantBScoreHistory;
}
