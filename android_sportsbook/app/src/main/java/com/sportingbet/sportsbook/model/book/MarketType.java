package com.sportingbet.sportsbook.model.book;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class MarketType implements android.os.Parcelable {

    @Getter
    private String name;

    @Getter
    private WebCouponTemplate webCouponTemplate;
}
