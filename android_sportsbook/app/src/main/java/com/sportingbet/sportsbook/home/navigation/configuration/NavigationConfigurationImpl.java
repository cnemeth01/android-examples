package com.sportingbet.sportsbook.home.navigation.configuration;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.home.navigation.NavigationItem;
import com.sportingbet.sportsbook.home.navigation.NavigationItemFactory;
import com.sportingbet.sportsbook.home.navigation.fragments.SportNavigationFragment;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.sports.SportProvider;
import com.sportingbet.sportsbook.sports.SportsStore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class NavigationConfigurationImpl implements SportProvider, SportsStore, NavigationConfiguration {

    private List<Sport> sports;

    private List<Sport> inPlaySports;

    private List<Sport> homeSports;

    private List<NavigationItem> menuItems;

    private List<NavigationItem> pagerItems;

    private NavigationItemFactory navigationItemFactory;

    public NavigationConfigurationImpl(NavigationItemFactory navigationItemFactory) {
        this.navigationItemFactory = navigationItemFactory;
    }

    @Override
    public Sport getSportWithId(long itemId) {
        for (Sport sport : sports) {
            if (sport.getId() == itemId) {
                return sport;
            }
        }
        return null;
    }

    @Override
    public void setSports(List<Sport> sports) {
        Collections.sort(sports);
        this.sports = sports;
    }

    @Override
    public ArrayList<Sport> getSports() {
        return Lists.newArrayList(sports);
    }

    private List<NavigationItem> getNavigationItemsWithIds(Integer[] popularSportsIds) {
        List<NavigationItem> popularSports = Lists.newArrayList();
        for (int sportId : popularSportsIds) {
            Sport sport = getSportWithId(sportId);
            if (sport != null) {
                popularSports.add(sport);
            } else {
                popularSports.add(navigationItemFactory.getNavigationItemForId(sportId));
            }
        }
        return popularSports;
    }

    private List<Sport> getSportsWithIds(Integer[] sportsIds) {
        List<Sport> sports = Lists.newArrayList();
        for (int sportId : sportsIds) {
            Sport sport = getSportWithId(sportId);
            if (sport != null) {
                sports.add(sport);
            }
        }
        return sports;
    }

    @Override
    public List<? extends NavigationItem> getListItems() {
        return sports;
    }

    @Override
    public List<Sport> getInPlaySports() {
        if (inPlaySports == null) {
            inPlaySports = getSportsWithIds(BuildConfig.INPLAY_SPORTS_IDS);
        }
        return inPlaySports;
    }

    @Override
    public List<Sport> getHomeSports() {
        if (homeSports == null) {
            homeSports = getSportsWithIds(BuildConfig.HIGHLIGHTS_SPORT_IDS);
        }
        return homeSports;
    }

    @Override
    public List<NavigationItem> getMenuItems() {
        if (menuItems == null) {
            menuItems = getNavigationItemsWithIds(BuildConfig.POPULAR_SPORTS_IDS);
        }
        return menuItems;
    }

    private List<NavigationItem> getPagerItems() {
        if (pagerItems == null) {
            pagerItems = Lists.newArrayList();
            pagerItems.add(navigationItemFactory.getNavigationItemForId(BuildConfig.HOME_ITEM_ID));
            pagerItems.addAll(getMenuItems());
        }
        return pagerItems;
    }

    @Override
    public List<AbstractNavigationFragment> createNavigationFragments() {
        List<AbstractNavigationFragment> sportFragments = Lists.newArrayList();
        for (NavigationItem navigationItem : getPagerItems()) {
            sportFragments.add(navigationItemFactory.createFragmentForNavigationItem(navigationItem));
        }
        return sportFragments;
    }

    @Override
    public int getNavigationFragmentsCount() {
        return getPagerItems().size();
    }

    @Override
    public AbstractNavigationFragment createSportNavigationFragment(long itemId) {
        return AbstractNavigationFragment.prepareInstance(new SportNavigationFragment(), getSportWithId(itemId));
    }
}
