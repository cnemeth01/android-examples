package com.sportingbet.sportsbook.betslip.placebets.model;

import com.google.common.collect.Lists;

import java.util.List;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class PlaceBetRequest {

    @Getter
    private List<BetRequest> betRequests = Lists.newArrayList();
}
