package com.sportingbet.sportsbook.home.navigation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.fragments.home.HomeFragment;
import com.sportingbet.sportsbook.home.navigation.NavigationItem;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HomeNavigationFragment extends AbstractNavigationFragment<NavigationItem> {

    @Override
    protected int getLayoutResourceId() {
        return R.layout.home_navigation_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null) {
            switchFragment(new HomeFragment());
            notifyChildFragmentOnResume();
        }
    }
}
