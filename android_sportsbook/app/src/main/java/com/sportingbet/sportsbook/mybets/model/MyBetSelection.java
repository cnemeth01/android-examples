package com.sportingbet.sportsbook.mybets.model;

import lombok.Getter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */

public class MyBetSelection {

    @Getter
    private String event;

    @Getter
    private String market;

    @Getter
    private String selection;

    @Getter
    private String price;

    @Getter
    private String handicap;

    @Getter
    private String eachWayTerms;
}
