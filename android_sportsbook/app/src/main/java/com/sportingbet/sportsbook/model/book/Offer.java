package com.sportingbet.sportsbook.model.book;

import com.sportingbet.sportsbook.model.book.selection.Selection;

import java.util.List;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class Offer implements android.os.Parcelable {

    @Getter
    private String type;

    @Getter
    private List<Selection> selections;

    public boolean isSPType(){
        return "SP".equals(type);
    }

    public int getSelectionCount() {
        return selections.size();
    }
}
