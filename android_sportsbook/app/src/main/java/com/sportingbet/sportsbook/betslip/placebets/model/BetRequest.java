package com.sportingbet.sportsbook.betslip.placebets.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@NoArgsConstructor
public class BetRequest {

    @Getter
    private String betId;

    @Getter
    private double unitStake;

    private boolean isEachWayBet;

    public BetRequest(String betId, double unitStake, boolean isEachWayBet) {
        this.betId = betId;
        this.unitStake = unitStake;
        this.isEachWayBet = isEachWayBet;
    }

    public boolean getIsEachWayBet() {
        return isEachWayBet;
    }
}
