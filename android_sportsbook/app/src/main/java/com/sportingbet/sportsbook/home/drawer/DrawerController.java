package com.sportingbet.sportsbook.home.drawer;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.adapter.AbstractRecyclerViewAdapter;
import com.sportingbet.sportsbook.home.drawer.list.DrawerItem;
import com.sportingbet.sportsbook.home.drawer.list.DrawerListAdapter;
import com.sportingbet.sportsbook.home.drawer.menu.SportMenuAdapter;
import com.sportingbet.sportsbook.home.navigation.NavigationItem;
import com.sportingbet.sportsbook.home.navigation.configuration.NavigationConfiguration;
import com.sportingbet.sportsbook.home.settings.HelpAndSettingsActivity;
import com.sportingbet.sportsbook.sports.SportIconsProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class DrawerController implements AdapterView.OnItemClickListener, AbstractRecyclerViewAdapter.OnItemClickListener<DrawerItem> {

    private final NavigationConfiguration navigationConfiguration;
    private final SportIconsProvider sportIconsProvider;

    private ListView drawerListView;
    private DrawerControllerDelegate delegate;

    private RecyclerView recyclerView;

    public DrawerController(NavigationConfiguration navigationConfiguration, SportIconsProvider sportIconsProvider) {
        this.navigationConfiguration = navigationConfiguration;
        this.sportIconsProvider = sportIconsProvider;
    }

    public void registerActivity(Activity activity, DrawerControllerDelegate delegate) {
        this.delegate = delegate;
        setupDrawerList(activity);
        setupDrawerMenu(activity);
        setupHelpItem(activity);
    }

    private void setupDrawerList(Activity activity) {
        drawerListView = (ListView) activity.findViewById(R.id.drawer_list);
        drawerListView.setOnItemClickListener(this);
        drawerListView.setAdapter(createDrawerListAdapter(activity));
    }

    private DrawerListAdapter createDrawerListAdapter(Activity activity) {
        DrawerListAdapter drawerListAdapter = new DrawerListAdapter(activity);
        drawerListAdapter.setList(prepareListDrawerItems());
        return drawerListAdapter;
    }

    private List<DrawerItem> prepareListDrawerItems() {
        List<DrawerItem> drawerItems = new ArrayList<>();
        for (NavigationItem navigationItem : navigationConfiguration.getListItems()) {
            int icon = sportIconsProvider.getWhiteIconResourceIdForSport(navigationItem.getId());
            drawerItems.add(new DrawerItem(navigationItem.getId(), navigationItem.getName(), icon));
        }
        return drawerItems;
    }

    private void setupDrawerMenu(Activity activity) {
        recyclerView = (RecyclerView) activity.findViewById(R.id.drawer_menu);
        recyclerView.setLayoutManager(new GridLayoutManager(activity, 3));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(createSportMenuAdapter(activity));
    }

    private void setupHelpItem(final Activity activity) {
        activity.findViewById(R.id.help_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, HelpAndSettingsActivity.class));
            }
        });
    }

    private SportMenuAdapter createSportMenuAdapter(Activity activity) {
        SportMenuAdapter sportMenuAdapter = new SportMenuAdapter(activity);
        sportMenuAdapter.setItemClickListener(this);
        sportMenuAdapter.setList(prepareMenuDrawerItems());
        return sportMenuAdapter;
    }

    private List<DrawerItem> prepareMenuDrawerItems() {
        List<DrawerItem> drawerItems = new ArrayList<>();
        for (NavigationItem item : navigationConfiguration.getMenuItems()) {
            int icon = sportIconsProvider.getMenuIconResourceIdForSport(item.getId());
            drawerItems.add(new DrawerItem(item.getId(), item.getName(), icon));
        }
        return drawerItems;
    }

    public void selectDrawerItemWithId(long itemId) {
        updateSportList(getDrawerListItemPositionWithId(itemId));
        updateSportMenu(itemId);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        updateItemWithIdAtPosition(id, position);
    }

    @Override
    public void onItemClicked(View view, DrawerItem item) {
        updateItemWithIdAtPosition(item.getId(), getDrawerListItemPositionWithId(item.getId()));
    }

    private int getDrawerListItemPositionWithId(long itemId) {
        return ((DrawerListAdapter) drawerListView.getAdapter()).getItemPositionWithId(itemId);
    }

    private void updateItemWithIdAtPosition(long itemId, int position) {
        updateSportList(position);
        updateSportMenu(itemId);
        notifyDelegateAboutSelectedItemId(itemId);
    }

    private void updateSportList(int position) {
        drawerListView.setItemChecked(position, true);
    }

    private void updateSportMenu(long itemId) {
        ((SportMenuAdapter) recyclerView.getAdapter()).setCheckedItemId(itemId);
    }

    private void notifyDelegateAboutSelectedItemId(long itemId) {
        delegate.didSelectDrawerItemWithId(itemId);
    }

    public interface DrawerControllerDelegate {

        void didSelectDrawerItemWithId(long itemId);
    }
}