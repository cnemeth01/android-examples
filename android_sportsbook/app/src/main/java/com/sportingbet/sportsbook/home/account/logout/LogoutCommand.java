package com.sportingbet.sportsbook.home.account.logout;

import com.sportingbet.sportsbook.general.commands.NetworkCommand;

import retrofit.client.Response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class LogoutCommand extends NetworkCommand<Response>{

    private final LogoutRequester logoutRequester;

    public LogoutCommand(LogoutRequester logoutRequester) {
        this.logoutRequester = logoutRequester;
    }

    @Override
    public void execute() {
        logoutRequester.requestLogout(this);
    }
}
