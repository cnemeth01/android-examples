package com.sportingbet.sportsbook.mybets.pager;

import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface MyBetsResponseDataSet {

    void setMyBetsResponse(MyBetsResponse myBetsResponse);

    void notifyDataSetChanged();
}
