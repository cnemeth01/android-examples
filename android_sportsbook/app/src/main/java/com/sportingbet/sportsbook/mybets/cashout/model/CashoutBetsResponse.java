package com.sportingbet.sportsbook.mybets.cashout.model;

import java.util.List;

import lombok.Getter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CashoutBetsResponse {

    @Getter
    private List<CashoutBetsSuccesful> successfulCashoutBets;

    @Getter
    private List<CashoutBetsFailed> failedCashoutBets;
}
