package com.sportingbet.sportsbook.coupons.betslip.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@NoArgsConstructor
public class BetslipResponse {

    @Getter
    private int numberOfSelectionsInBetSlip;

    public BetslipResponse(int numberOfSelectionsInBetSlip) {
        this.numberOfSelectionsInBetSlip = numberOfSelectionsInBetSlip;
    }
}
