package com.sportingbet.sportsbook.content.general.helpers;

import android.os.Bundle;
import android.os.Parcelable;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BundleHelper<T extends Parcelable> {

    private final Class type;

    private T object;

    public BundleHelper(Class type) {
        this.type = type;
    }

    public void set(T object) {
        this.object = object;
    }

    public T get() {
        return object;
    }

    public Bundle createBundle(T object) {
        this.object = object;
        Bundle bundle = new Bundle();
        onSaveInstanceState(bundle);
        return bundle;
    }

    public void onRestoreInstanceState(Bundle bundle) {
        object = bundle.getParcelable(type.getName());
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putParcelable(type.getName(), object);
    }
}
