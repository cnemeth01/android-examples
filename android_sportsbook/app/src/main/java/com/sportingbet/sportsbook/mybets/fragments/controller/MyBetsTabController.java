package com.sportingbet.sportsbook.mybets.fragments.controller;

import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;

import lombok.Setter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsTabController implements View.OnClickListener {

    private TextView allButton;
    private TextView cashOutButton;

    private View activeView;

    @Setter
    private Delegate delegate;

    public MyBetsTabController(View view) {
        this.allButton = (TextView) view.findViewById(R.id.first_tab);
        this.cashOutButton = (TextView) view.findViewById(R.id.second_tab);
        setupTabsView();
    }

    private void setupTabsView() {
        setupAllTab();
        setupSettledTab();
    }

    private void setupAllTab() {
        allButton.setText(R.string.mybets_tab_all);
        allButton.setEnabled(true);
        allButton.setOnClickListener(this);
    }

    private void setupSettledTab() {
        cashOutButton.setText(R.string.mybets_tab_cash_out);
        cashOutButton.setEnabled(true);
        cashOutButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.first_tab:
                delegate.onAllTabClick();
                break;
            case R.id.second_tab:
                delegate.onCashOutTabClick();
                break;
        }
    }

    public void setCashOutViewActive() {
        cashOutButton.setActivated(true);
        activeView = cashOutButton;
        allButton.setActivated(false);
    }

    public void setAllViewActive() {
        allButton.setActivated(true);
        activeView = allButton;
        cashOutButton.setActivated(false);
    }

    public View getActiveView() {
        return activeView;
    }

    public interface Delegate {

        void onAllTabClick();

        void onCashOutTabClick();
    }
}
