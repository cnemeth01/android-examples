package com.sportingbet.sportsbook.betslip.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.helpers.StakeHelper;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class FooterViewHolderHandler extends AbstractViewHolderHandler {

    private final BetslipValueProvider betslipValueProvider;
    private final View.OnClickListener onClickListener;

    public FooterViewHolderHandler(Context context, BetslipValueProvider betslipValueProvider, DataSource dataSource, View.OnClickListener onClickListener) {
        super(context, dataSource);
        this.betslipValueProvider = betslipValueProvider;
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new FooterHolder(LayoutInflater.from(context).inflate(R.layout.betslip_footer, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterHolder holder = (FooterHolder) viewHolder;
        String userCurrency = getBetslipInformation().getStakeInformation().getUserCurrency();
        StakeHelper.setupStakeView(context, holder.totalStake, betslipValueProvider.getTotalStake(), userCurrency);
        StakeHelper.setupPotentialReturnView(context, holder.totalPayout, betslipValueProvider.getTotalPayout(), userCurrency);
        holder.placeBetButton.setEnabled(betslipValueProvider.hasTotalPayout());
        holder.placeBetButton.setOnClickListener(onClickListener);
    }

    protected class FooterHolder extends RecyclerView.ViewHolder {

        private final TextView totalStake;
        private final TextView totalPayout;
        private final Button placeBetButton;

        public FooterHolder(View view) {
            super(view);
            totalStake = (TextView) view.findViewById(R.id.total_stake);
            totalPayout = (TextView) view.findViewById(R.id.total_payout);
            placeBetButton = (Button) view.findViewById(R.id.place_bet_button);
        }
    }
}
