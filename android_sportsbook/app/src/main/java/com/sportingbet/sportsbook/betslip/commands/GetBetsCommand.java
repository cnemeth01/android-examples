package com.sportingbet.sportsbook.betslip.commands;

import com.sportingbet.sportsbook.betslip.controller.BetslipController;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class GetBetsCommand extends BetslipCommand implements BetslipController.BetslipInformationDelegate {

    public GetBetsCommand(BetslipController betslipController) {
        super(betslipController);
    }

    @Override
    public void execute() {
        betslipController.getBets(this, this, this);
    }

    @Override
    public void success(BetslipInformation betslipInformation) {
    }
}
