package com.sportingbet.sportsbook.general.adapter.handlers.margin;

import hrisey.Parcelable;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
class Margin {
}
