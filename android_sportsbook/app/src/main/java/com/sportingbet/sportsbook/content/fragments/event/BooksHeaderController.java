package com.sportingbet.sportsbook.content.fragments.event;

import android.content.Context;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.general.controllers.AbstractHeaderController;
import com.sportingbet.sportsbook.sports.configuration.CouponType;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BooksHeaderController extends AbstractHeaderController {

    public BooksHeaderController(Context context) {
        super(context);
    }

    @Override
    public void setup(ViewGroup headerLayout, CouponType defaultCouponType) {
        super.setup(headerLayout, defaultCouponType);
        switch (defaultCouponType) {
            case CouponTypeTie:
                inflateResourceInHeader(headerLayout, R.layout.books_header_coupon_tie);
                break;
            case CouponTypeNoTie:
                inflateResourceInHeader(headerLayout, R.layout.books_header_coupon_no_tie);
                break;
        }
    }
}
