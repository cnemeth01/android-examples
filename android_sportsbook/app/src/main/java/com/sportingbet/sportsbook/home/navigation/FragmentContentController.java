package com.sportingbet.sportsbook.home.navigation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.sportingbet.sportsbook.R;

import java.util.List;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public class FragmentContentController {

    private final FragmentManager fragmentManager;

    public FragmentContentController(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public void switchFragment(Fragment fragment) {
        switchFragment(fragment, false, fragment.getClass().getName());
    }

    public void switchFragment(Fragment fragment, boolean addToBackStack, String tag) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment, tag);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }

    public void addFragment(Fragment fragment) {
        addFragment(fragment, fragment.getClass().getName());
    }

    public void addFragment(Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_bottom, 0, 0, 0);
        fragmentTransaction.add(R.id.content_frame, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    public Fragment getCurrentFragment() {
        List<Fragment> fragments = fragmentManager.getFragments();
        return fragments.get(fragmentManager.getBackStackEntryCount());
    }

    public void replaceFragment(Fragment fragment, Fragment newFragment) {
        removeFragment(fragment);
        fragmentManager.popBackStack();
        addFragmentWithoutAnimation(newFragment, newFragment.getClass().getName());
    }

    private void removeFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(fragment);
        fragmentTransaction.commit();
    }

    private void addFragmentWithoutAnimation(Fragment newFragment, String tag) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content_frame, newFragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }
}
