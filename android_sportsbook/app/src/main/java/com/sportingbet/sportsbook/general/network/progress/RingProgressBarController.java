package com.sportingbet.sportsbook.general.network.progress;

import android.app.Activity;
import android.os.Handler;

import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.ui.dialog.RingProgressBarDialog;

import java.util.concurrent.TimeUnit;

import lombok.Setter;

/**
 * Copyright 2014 Tomasz Morcinek. All rights reserved.
 */
public class RingProgressBarController implements ProgressController, Runnable {

    private static final long REQUEST_INTERVAL = 500;

    private final Activity activity;

    private final RingProgressBarDialog ringProgressBarDialog;

    private final long progressTimeMillis;

    @Setter
    private ProgressControllerListener progressControllerListener;

    public RingProgressBarController(Activity activity, int progressTimeSeconds, int message) {
        this.activity = activity;
        progressTimeMillis = getProgressTimeMillis(progressTimeSeconds);

        ringProgressBarDialog = new RingProgressBarDialog(activity);
        ringProgressBarDialog.setProgressTimeMillis(progressTimeMillis);
        ringProgressBarDialog.setMessage(message);
    }

    private long getProgressTimeMillis(int progressTimeSeconds) {
        return TimeUnit.SECONDS.toMillis(progressTimeSeconds) + REQUEST_INTERVAL;
    }

    @Override
    public void preExecute() {
        ringProgressBarDialog.show();
        if (progressControllerListener != null) {
            new Handler().postDelayed(this, progressTimeMillis);
        }
    }

    @Override
    public void postExecuteWithSuccess(boolean success) {
        if (!success || progressControllerListener == null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    ringProgressBarDialog.cancel();
                }
            });
        }
    }

    @Override
    public void run() {
        if (progressControllerListener != null) {
            progressControllerListener.onProgressFinished();
            ringProgressBarDialog.cancel();
        }
    }

    public interface ProgressControllerListener {

        void onProgressFinished();
    }
}
