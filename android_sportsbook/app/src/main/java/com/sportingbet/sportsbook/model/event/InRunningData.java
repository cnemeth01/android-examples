package com.sportingbet.sportsbook.model.event;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class InRunningData implements android.os.Parcelable {

    @Getter
    private int isInRunning;

    @Getter
    private String participantA;

    @Getter
    private String participantB;

    @Getter
    private String scoreA;

    @Getter
    private String scoreB;

    @Getter
    private Scores scores;

    @Getter
    private String periodText;

    boolean isRunning() {
        return isInRunning == 1;
    }
}
