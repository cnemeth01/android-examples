package com.sportingbet.sportsbook.home.inplay;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sportingbet.sportsbook.content.fragments.inplay.InPlayFragment;
import com.sportingbet.sportsbook.general.helpers.empty.EmptyScreenController;
import com.sportingbet.sportsbook.general.helpers.empty.InPlayEmptyScreenController;
import com.sportingbet.sportsbook.home.pager.PagerController;
import com.sportingbet.sportsbook.model.Sport;

import javax.inject.Inject;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class InPlaySportFragment extends InPlayFragment implements View.OnClickListener {

    @Inject
    PagerController pagerController;

    private Sport sport;

    public static InPlaySportFragment newInstance(Sport sport) {
        Bundle args = new Bundle();
        args.putParcelable(Sport.class.getName(), sport);
        InPlaySportFragment fragment = new InPlaySportFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected EmptyScreenController getEmptyScreenController(View view) {
        return new InPlayEmptyScreenController(view, this, getSport());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public Sport getSport() {
        if (sport == null) {
            sport = getArguments().getParcelable(Sport.class.getName());
        }
        return sport;
    }

    @Override
    public void onClick(View v) {
        pagerController.selectPageWithId(getSport().getId());
    }
}
