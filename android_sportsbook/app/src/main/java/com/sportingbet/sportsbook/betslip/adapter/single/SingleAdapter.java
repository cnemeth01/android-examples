package com.sportingbet.sportsbook.betslip.adapter.single;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportingbet.sportsbook.betslip.adapter.BetslipAdapter;
import com.sportingbet.sportsbook.betslip.adapter.FooterViewHolderHandler;
import com.sportingbet.sportsbook.betslip.helpers.StakeHelper;
import com.sportingbet.sportsbook.betslip.model.BetSelection;
import com.sportingbet.sportsbook.betslip.model.BetSelectionState;
import com.sportingbet.sportsbook.betslip.model.types.BetType;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SingleAdapter extends BetslipAdapter {

    @Getter
    private final SingleValueController betslipValueController;

    public SingleAdapter(Context context, Delegate delegate) {
        super(context, delegate);
        betslipValueController = new SingleValueController(delegate.getBetsValueContainer());
        betslipValueController.setDelegate(this);

        footerViewHolderHandler = new FooterViewHolderHandler(context, betslipValueController, this, this);
        sectionsViewHolderHandler = new SingleSectionsViewHolderHandler(context, this, betslipValueController);
    }

    @Override
    protected void onBindViewHolder(RecyclerView.ViewHolder viewHolder, BetSelection betSelection) {
        super.onBindViewHolder(viewHolder, betSelection);
        ViewHolder holder = (ViewHolder) viewHolder;
        BetType betType = betslipInformation.getBetTypeForSelectionId(betSelection.getSelectionId());
        if (betType.getErrorCode() == null) {
            setupStakePicker(holder, betType);
        } else {
            switch (betType.getErrorCode()) {
                case BET00006:
                    hideStakePicker(holder, betType);
                    break;
                default:
                    setupStakePicker(holder, betType);
                    break;
            }
        }
        setupEachWayView(holder, betType, getBetSelectionState(betSelection));
    }

    private void setupEachWayView(ViewHolder holder, BetType betType, BetSelectionState betSelectionState) {
        holder.eachWay.setVisibility(betType.isEachWay() ? View.VISIBLE : View.GONE);
        if (betType.isEachWay()) {
            StakeHelper.setupEachWayView(context, holder.eachWayText, betSelectionState);
        }
    }

    private BetSelectionState getBetSelectionState(BetSelection betSelection) {
        if (betSelection.getNewState() != null) {
            return betSelection.getNewState();
        }
        return betSelection.getAcceptedState();
    }

    private void setupStakePicker(ViewHolder holder, BetType betType) {
        betslipValueController.registerPickerAdapter(holder.stakePicker, holder.eachWay, betType);
    }

    private void hideStakePicker(ViewHolder holder, BetType betType) {
        betslipValueController.unregisterStakePicker(holder.stakePicker, betType);
    }
}
