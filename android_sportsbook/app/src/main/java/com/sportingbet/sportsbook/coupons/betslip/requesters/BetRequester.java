package com.sportingbet.sportsbook.coupons.betslip.requesters;

import com.sportingbet.sportsbook.general.network.requesters.Requester;
import com.sportingbet.sportsbook.coupons.betslip.model.BetslipResponse;
import com.sportingbet.sportsbook.coupons.betslip.model.BetslipSelection;
import com.sportingbet.sportsbook.coupons.betslip.model.BetslipSelectionGroup;
import com.sportingbet.sportsbook.general.network.services.BetService;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetRequester extends Requester<BetslipResponse> {

    protected final BetService betService;

    public BetRequester(BetService betService) {
        this.betService = betService;
    }

    public void addBet(Event event, Book book, Selection selection) {
        BetslipSelection betslipSelection = BetslipSelection.create(event, book, selection);
        betService.addBet(BetslipSelectionGroup.create(betslipSelection), getCallback());
    }

    public void removeBet(Selection selection) {
        betService.removeBet(String.valueOf(selection.getId()), null, getCallback());
    }

    public void removeAllBets() {
        betService.removeBet(null, true, getCallback());
    }
}
