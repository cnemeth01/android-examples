package com.sportingbet.sportsbook.splash.commands;

import com.sportingbet.sportsbook.general.network.CallbackWrapper;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.model.Sport;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportsRequester {

    protected final PublishingService publishingService;

    public SportsRequester(PublishingService publishingService) {
        this.publishingService = publishingService;
    }

    public void getSports(NetworkResponseListener<List<Sport>> responseListener) {
        publishingService.getSports(new CallbackWrapper<>(responseListener));
    }
}
