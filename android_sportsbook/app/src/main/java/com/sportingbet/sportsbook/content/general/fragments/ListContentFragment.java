package com.sportingbet.sportsbook.content.general.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.general.error.EmptyContentErrorHandler;
import com.sportingbet.sportsbook.general.adapter.AbstractRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.adapter.handlers.TypedHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.helpers.empty.DefaultEmptyScreenController;
import com.sportingbet.sportsbook.general.helpers.empty.EmptyScreenController;
import com.sportingbet.sportsbook.general.network.progress.ProgressBarController;
import com.sportingbet.sportsbook.general.network.progress.RefreshProgressController;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.ui.recyclerview.DefaultItemAnimator;
import com.sportingbet.sportsbook.general.ui.recyclerview.DividerItemDecoration;
import com.sportingbet.sportsbook.general.ui.swipe.SwipeRefreshLayout;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class ListContentFragment<E> extends ContentFragment implements SwipeRefreshLayout.OnRefreshListener, Runnable, AbstractRecyclerViewAdapter.OnItemClickListener<Object> {

    protected ProgressController refreshProgressController;

    protected ProgressController progressBarController;

    protected EmptyContentErrorHandler errorHandler;

    protected EmptyScreenController emptyScreenController;

    private RecyclerView recyclerView;

    private SwipeRefreshLayout swipeRefreshLayout;

    protected abstract TypedHandlersRecyclerViewAdapter<E> createAdapter();

    @Override
    protected int getLayoutResourceId() {
        return R.layout.list_fragment;
    }

    @SuppressWarnings("unchecked")
    protected TypedHandlersRecyclerViewAdapter<E> getAdapter() {
        return (TypedHandlersRecyclerViewAdapter<E>) recyclerView.getAdapter();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupRecyclerView(view);
        setupSwipeRefreshLayout(view);

        getAdapter().setItemClickListener(this);

        refreshProgressController = new RefreshProgressController(swipeRefreshLayout);
        progressBarController = new ProgressBarController(getActivity().findViewById(R.id.progress_bar));

        emptyScreenController = getEmptyScreenController(swipeRefreshLayout);
        errorHandler = new EmptyContentErrorHandler(getActivity(), emptyScreenController);

        errorHandler.registerAction(this);
    }

    protected EmptyScreenController getEmptyScreenController(View view) {
        return new DefaultEmptyScreenController(view);
    }

    @Override
    public void onFragmentSelected() {
        onRefresh();
    }

    @Override
    public void onFragmentDisabled() {
        errorHandler.dismiss();
    }

    private void setupSwipeRefreshLayout(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.accentColor, R.color.primaryDarkColor);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setupRecyclerView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(getDividerItemDecoration());
        recyclerView.setAdapter(createAdapter());
    }

    protected DividerItemDecoration getDividerItemDecoration() {
        return new DividerItemDecoration(getActivity());
    }

    protected final void scrollToItem(final Object item) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollToPosition(getAdapter().getItems().indexOf(item));
            }
        }, getResources().getInteger(android.R.integer.config_shortAnimTime));

    }

    private void scrollToPosition(int itemPosition) {
        if (itemPosition >= 0) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            View itemView = layoutManager.findViewByPosition(itemPosition);
            recyclerView.smoothScrollBy(0, itemView.getTop());
        }
    }

    @Override
    public void onRefresh() {
        getAdapter().notifyDataSetChanged();
    }

    @Override
    public void run() {
        onRefresh();
    }
}
