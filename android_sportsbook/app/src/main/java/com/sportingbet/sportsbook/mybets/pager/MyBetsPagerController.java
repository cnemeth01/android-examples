package com.sportingbet.sportsbook.mybets.pager;

import android.support.v4.app.FragmentActivity;

import com.sportingbet.sportsbook.general.pager.AbstractPagerController;
import com.sportingbet.sportsbook.general.pager.FragmentStatePagerAdapter;
import com.sportingbet.sportsbook.mybets.model.MyBetType;
import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsPagerController extends AbstractPagerController {

    public MyBetsPagerController(FragmentStatePagerAdapter fragmentPagerAdapter) {
        super(fragmentPagerAdapter);
    }

    @Override
    public void registerActivity(FragmentActivity activity) {
        super.registerActivity(activity);
        updateStartPosition();
    }

    public void selectMyBetType(MyBetType myBetType, MyBetsResponse myBetsResponse) {
        int position = getMyBetTypePosition(myBetType);
        MyBetsResponseDataSet responseDataSet = (MyBetsResponseDataSet) fragmentPagerAdapter.getItem(position);
        responseDataSet.setMyBetsResponse(myBetsResponse);
        viewPager.setCurrentItem(position, true);
        responseDataSet.notifyDataSetChanged();
    }

    private int getMyBetTypePosition(MyBetType myBetType) {
        switch (myBetType) {
            case open:
                return 0;
            default:
                return 1;
        }
    }
}
