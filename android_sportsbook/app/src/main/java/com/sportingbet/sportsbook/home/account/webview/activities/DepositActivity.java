package com.sportingbet.sportsbook.home.account.webview.activities;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.home.account.webview.WebURLFactory;
import com.sportingbet.sportsbook.home.account.webview.session.TempSessionWebViewActivity;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class DepositActivity extends TempSessionWebViewActivity {

    @Override
    protected String getWebViewURL(String tempSessionId) {
        return WebURLFactory.getDepositURL(tempSessionId);
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.deposit_title;
    }
}
