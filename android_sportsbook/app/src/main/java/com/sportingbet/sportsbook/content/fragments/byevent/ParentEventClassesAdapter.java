package com.sportingbet.sportsbook.content.fragments.byevent;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.fragments.byevent.links.SportLink;
import com.sportingbet.sportsbook.content.fragments.byevent.links.SportLinkHelper;
import com.sportingbet.sportsbook.general.adapter.handlers.ViewHolderHandler;
import com.sportingbet.sportsbook.general.adapter.handlers.margin.MarginHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.events.EventClass;

import java.util.List;
import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ParentEventClassesAdapter extends MarginHandlersRecyclerViewAdapter<EventClass> {

    private final SportLinkHelper sportLinkHelper;

    private final Map<EventClass, List<EventClass>> openedEventClasses = Maps.newHashMap();

    public ParentEventClassesAdapter(Context context, SportLinkHelper sportLinkHelper) {
        super(context);
        addViewHolderHandler(SportLink.class, new SportLinkViewHolderHandler());
        this.sportLinkHelper = sportLinkHelper;
    }

    @Override
    public ViewHolder onCreateTypeViewHolder(ViewGroup parent, int viewType) {
        switch (EventClass.Category.getCategoryByValue(viewType)) {
            case parentEventClass:
                return createViewHolderWithLayoutId(parent, R.layout.parent_event_class_item);
            default:
                return createViewHolderWithLayoutId(parent, R.layout.event_class_item);
        }
    }

    private ViewHolder createViewHolderWithLayoutId(ViewGroup parent, int layoutId) {
        return new ViewHolder(layoutInflater.inflate(layoutId, parent, false));
    }

    public void setEventClassList(List<EventClass> eventClassList, Sport sport) {
        openedEventClasses.clear();
        setList(sportLinkHelper.prepareItems(eventClassList, sport));
    }

    @Override
    public int getTypeItemViewType(EventClass item) {
        return EventClass.Category.getCategoryValue(item.getCategory());
    }

    @Override
    public void onBindTypeViewHolder(RecyclerView.ViewHolder viewHolder, EventClass parentEventClass, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        initializeOnClickListener(holder, parentEventClass);
        holder.title.setText(parentEventClass.getName());
        holder.itemView.setActivated(isEventClassOpened(parentEventClass));
    }

    public boolean isEventClassOpened(EventClass eventClass) {
        return openedEventClasses.containsKey(eventClass);
    }

    public void insertEventClassWithChildren(EventClass eventClass, List<EventClass> eventClasses) {
        openedEventClasses.put(eventClass, eventClasses);
        int position = getItems().indexOf(eventClass);
        notifyItemChanged(position);
        getItems().addAll(position + 1, eventClasses);
        notifyItemRangeInserted(position + 1, eventClasses.size());
    }

    public void removeEventClassWithChildren(EventClass eventClass) {
        List<EventClass> eventClasses = openedEventClasses.remove(eventClass);
        getItems().removeAll(eventClasses);
        int position = getItems().indexOf(eventClass);
        notifyItemChanged(position);
        notifyItemRangeRemoved(position + 1, eventClasses.size());
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }

    public class SportLinkViewHolderHandler implements ViewHolderHandler<SportLink, SportLinkViewHolderHandler.SportViewHolder> {

        @Override
        public SportViewHolder onCreateViewHolder(ViewGroup parent) {
            return new SportViewHolder(layoutInflater.inflate(R.layout.sport_link_item, parent, false));
        }

        @Override
        public void onBindViewHolder(SportViewHolder holder, SportLink object) {
            initializeOnClickListener(holder, object);
            holder.title.setText(object.getSport().getName());
            holder.icon.setImageResource(object.getIconResource());
        }

        protected class SportViewHolder extends ViewHolder {

            public final ImageView icon;

            public SportViewHolder(View itemView) {
                super(itemView);
                icon = (ImageView) itemView.findViewById(R.id.icon);
            }
        }
    }
}
