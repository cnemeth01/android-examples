package com.sportingbet.sportsbook.coupons.model;

import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.WebCouponTemplate;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

import java.util.List;

import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@NoArgsConstructor
public class SingleCoupon extends BaseCoupon {

    private List<Selection> selections;

    public SingleCoupon(Event event, Book book, List<Selection> selections) {
        super(event, book);
        this.selections = selections;
    }

    @Override
    public WebCouponTemplate getWebCouponTemplate() {
        return WebCouponTemplate.UNKNOWN;
    }

    @Override
    public List<Selection> getSelections() {
        return selections;
    }
}
