package com.sportingbet.sportsbook.coupons.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CouponLayout extends RelativeLayout {

    private TextView button;

    private ProgressBar progressBar;

    public CouponLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private TextView getButton() {
        if (button == null) {
            button = (TextView) findViewById(R.id.coupon_button);
        }
        return button;
    }

    private ProgressBar getProgressBar() {
        if (progressBar == null) {
            progressBar = (ProgressBar) findViewById(R.id.coupon_progress_bar);
        }
        return progressBar;
    }

    public void setText(CharSequence text) {
        getButton().setText(text);
    }

    public void setColorResource(int colorResource) {
        getButton().setTextColor(getResources().getColor(colorResource));
    }

    public void setColorStateResource(int colorResource) {
        getButton().setTextColor(getResources().getColorStateList(colorResource));
    }

    public void setDrawableResource(int left, int top, int right, int bottom) {
        getButton().setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
    }

    public CharSequence getText() {
        return getButton().getText();
    }

    public void showProgressBar() {
        getProgressBar().setVisibility(VISIBLE);
    }

    public void hideProgressBar() {
        getProgressBar().setVisibility(GONE);
    }
}
