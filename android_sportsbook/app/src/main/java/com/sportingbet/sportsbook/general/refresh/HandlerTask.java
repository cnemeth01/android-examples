package com.sportingbet.sportsbook.general.refresh;

import android.os.Handler;

import java.util.TimerTask;

public class HandlerTask extends TimerTask {

    private final Handler handler = new Handler();

    private final Runnable runnable;

    public HandlerTask(Runnable runnable) {
        this.runnable = runnable;
    }

    @Override
    public void run() {
        handler.post(runnable);
    }
}