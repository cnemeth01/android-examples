package com.sportingbet.sportsbook.mybets.fragments.controller;

import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.mybets.fragments.filter.MyBetsResponseFilter;
import com.sportingbet.sportsbook.mybets.model.MyBet;
import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;

import java.util.List;

import lombok.Setter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsResponseController implements MyBetsTabController.Delegate {

    private final MyBetsResponseFilter myBetsResponseFilter;
    private final MyBetsTabController myBetsTabController;

    private List<MyBet> items;

    @Setter
    private Delegate delegate;

    public MyBetsResponseController(MyBetsResponseFilter myBetsResponseFilter, MyBetsTabController myBetsTabController) {
        this.myBetsResponseFilter = myBetsResponseFilter;
        this.myBetsTabController = myBetsTabController;
        this.myBetsTabController.setDelegate(this);
    }

    public int getEmptyScreenTitle() {
        switch (myBetsTabController.getActiveView().getId()) {
            case R.id.second_tab:
                return R.string.my_bets_cashout_empty_bets_text;
            default:
                return R.string.my_bets_open_empty_bets_text;
        }
    }

    public List<MyBet> prepareMyBets(MyBetsResponse object) {
        if (getActiveView() == null) {
            return prepareMyBetsWithoutActiveTab(object);
        } else {
            return prepareMyBetsForSpecificTab(object);
        }
    }

    private List<MyBet> prepareMyBetsWithoutActiveTab(MyBetsResponse myBetsResponse) {
        items = myBetsResponseFilter.createItems(myBetsResponse);
        List<MyBet> cashOutItems = getCashOutMyBets();
        if (cashOutItems.size() > 0) {
            myBetsTabController.setCashOutViewActive();
            return cashOutItems;
        } else {
            myBetsTabController.setAllViewActive();
            return items;
        }
    }

    private List<MyBet> getCashOutMyBets() {
        return myBetsResponseFilter.filterCashOutItems(getItems());
    }

    private List<MyBet> getItems() {
        return myBetsResponseFilter.filterItems(items);
    }

    private List<MyBet> prepareMyBetsForSpecificTab(MyBetsResponse object) {
        switch (getActiveView().getId()) {
            case R.id.first_tab:
                return myBetsResponseFilter.filterItems(myBetsResponseFilter.createItems(object));
            default:
                return myBetsResponseFilter.filterCashOutItems(myBetsResponseFilter.createItems(object));
        }
    }

    @Override
    public void onAllTabClick() {
        myBetsTabController.setAllViewActive();
        delegate.notifyMyBetsChanged(getItems());
    }

    @Override
    public void onCashOutTabClick() {
        myBetsTabController.setCashOutViewActive();
        delegate.notifyMyBetsChanged(getCashOutMyBets());
    }

    private View getActiveView() {
        return myBetsTabController.getActiveView();
    }

    public interface Delegate {

        void notifyMyBetsChanged(List<MyBet> myBets);
    }
}
