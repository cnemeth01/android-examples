package com.sportingbet.sportsbook.scoreboard.providers;

import android.content.Context;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.model.event.Scores;
import com.sportingbet.sportsbook.scoreboard.ScoreboardHelper;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class TennisScoreboardProvider extends AbstractScoreboardProvider {

    private final ScoreboardHelper scoreboardHelper;

    public TennisScoreboardProvider(Context context) {
        super(context);
        scoreboardHelper = new ScoreboardHelper(context);
    }

    protected int getScoreboardLayout() {
        return R.layout.scoreboard_tennis;
    }

    @Override
    public void prepareScoreboard(View view, Event event) {
        getTextView(view, R.id.first_team).setText(scoreboardHelper.getParticipantA(event));
        getTextView(view, R.id.second_team).setText(scoreboardHelper.getParticipantB(event));

        Scores scores = event.getInRunningData().getScores();
        setupIndicators(view, scores, R.drawable.scoreboard_tennis_indicator);
        scoreboardHelper.setupScores(view, scores);

        scoreboardHelper.setupTotalSetScore(view.findViewById(R.id.tennis_sets), scores, R.string.scoreboard_tennis_sets);
        scoreboardHelper.setupCurrentSetScore(view.findViewById(R.id.tennis_games), scores, R.string.scoreboard_tennis_game);
        scoreboardHelper.setupCurrentGameScore(view.findViewById(R.id.tennis_points), scores, R.string.scoreboard_tennis_points);
    }

    private void setupIndicators(View view, Scores scores, int drawableId) {
        scoreboardHelper.setTeamIndicatorVisibility(view, R.id.first_team_indicator, scores.getParticipantACurrentGameScore(), drawableId);
        scoreboardHelper.setTeamIndicatorVisibility(view, R.id.second_team_indicator, scores.getParticipantBCurrentGameScore(), drawableId);
    }
}
