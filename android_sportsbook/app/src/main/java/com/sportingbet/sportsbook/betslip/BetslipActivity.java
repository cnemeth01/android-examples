package com.sportingbet.sportsbook.betslip;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.adapter.BetslipAdapter;
import com.sportingbet.sportsbook.betslip.commands.GetBetsCommand;
import com.sportingbet.sportsbook.betslip.commands.RemoveAllBetsCommand;
import com.sportingbet.sportsbook.betslip.commands.RemoveCommand;
import com.sportingbet.sportsbook.betslip.controller.BetslipBalanceController;
import com.sportingbet.sportsbook.betslip.controller.BetslipController;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.betslip.model.StakeInformation;
import com.sportingbet.sportsbook.betslip.placebets.PlaceBetController;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetRequest;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetResponse;
import com.sportingbet.sportsbook.betslip.receipt.BetslipReceiptActivity;
import com.sportingbet.sportsbook.content.general.controllers.refresh.RefreshController;
import com.sportingbet.sportsbook.general.activity.ToolbarActivity;
import com.sportingbet.sportsbook.general.commands.CommandController;
import com.sportingbet.sportsbook.general.network.error.ActivitySnackbarErrorHandler;
import com.sportingbet.sportsbook.general.network.progress.ProgressBarController;
import com.sportingbet.sportsbook.general.network.progress.ProgressControllerAdapter;
import com.sportingbet.sportsbook.general.network.progress.RefreshProgressController;
import com.sportingbet.sportsbook.general.network.progress.RingProgressBarController;
import com.sportingbet.sportsbook.general.network.response.ContainerProgressController;
import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.ui.recyclerview.DividerItemDecoration;
import com.sportingbet.sportsbook.general.ui.swipe.SwipeRefreshLayout;
import com.sportingbet.sportsbook.model.book.selection.Selection;

import javax.inject.Inject;

import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipActivity extends ToolbarActivity implements SwipeRefreshLayout.OnRefreshListener,
        BetslipController.BetslipInformationDelegate, BetslipAdapter.Delegate, ErrorHandler, CommandController.Delegate, NetworkResponseListener<PlaceBetResponse>, RefreshController.RefreshControllerListener {

    public static final int LOGIN_ACTIVITY_REQUEST_CODE = 0;
    public static final int BETSLIP_RECEIPT_ACTIVITY_REQUEST_CODE = 1;

    private final ErrorHandler errorHandler = new ActivitySnackbarErrorHandler(this);

    @Inject
    RefreshController refreshController;

    @Inject
    BetslipController betslipController;

    @Inject
    PlaceBetController placeBetController;

    @Inject
    BetslipBalanceController betslipBalanceController;

    private ProgressController progressBarController;

    private BetslipTabController tabLayoutController;

    private SwipeRefreshLayout swipeRefreshLayout;

    private BetslipInformation betslipInformation;

    private boolean shouldRefresh = true;

    @Override
    protected int getToolbarTitle() {
        return R.string.betslip_title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_bottom, android.R.anim.fade_out);
        setContentView(R.layout.betslip);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        tabLayoutController = new BetslipTabController(this, recyclerView);

        setupRecyclerView(recyclerView);
        setupSwipeRefreshLayout();
        setupProgressControllers();

        refreshController.registerRefreshController(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshController.restart();
        if (shouldRefresh) {
            onRefresh();
        } else {
            shouldRefresh = true;
        }
        updateBalanceController();
    }

    @Override
    protected void onPause() {
        super.onPause();
        refreshController.cancel();
    }

    private void updateBalanceController() {
        betslipBalanceController.updateLayout();
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
    }

    private void setupSwipeRefreshLayout() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.accentColor, R.color.primaryDarkColor);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setupProgressControllers() {
        progressBarController = new ProgressBarController(findViewById(R.id.progress_bar));
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_out_bottom);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.betslip, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean onPrepareOptionsMenu = super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_more).setVisible(betslipInformation != null && !betslipInformation.isEmpty());
        return onPrepareOptionsMenu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                clearAllBets();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void clearAllBets() {
        CommandController commandController = new CommandController(new RemoveAllBetsCommand(betslipController), new GetBetsCommand(betslipController));
        commandController.setProgressController(new ContainerProgressController(progressBarController, refreshController));
        commandController.setDelegate(this);
        commandController.start();
    }

    @Override
    public void onRefresh() {
        betslipController.getBets(this, this, new RefreshProgressController(swipeRefreshLayout), progressBarController, refreshController);
    }

    @Override
    public void success(BetslipInformation betslipInformation) {
        this.betslipInformation = betslipInformation;
        if (!isFinishing()) {
            supportInvalidateOptionsMenu();
            tabLayoutController.setBetslipInformation(betslipInformation);
        }
    }

    @Override
    public void setSwipeEnabled(boolean enabled) {
        swipeRefreshLayout.setEnabled(enabled);
    }

    @Override
    public void onDeleteSelection(long selectionId, ProgressController progressController) {
        CommandController commandController = new CommandController(new RemoveCommand(betslipController, new Selection(selectionId)), new GetBetsCommand(betslipController));
        commandController.setProgressController(new ContainerProgressController(progressController, progressBarController, refreshController));
        commandController.setDelegate(this);
        commandController.start();
    }

    @Override
    public void onPlaceBetWithDelay(PlaceBetRequest placeBetRequest, int placeBetDelay) {
        placeBetController.preparePlaceBet(this, new UnsuccessfulRefreshProgressController(), new RingProgressBarController(this, placeBetDelay, R.string.betslip_place_bet_progress_message));
        placeBetController.placeBet(placeBetRequest);
    }

    @Override
    public BetsValueContainer getBetsValueContainer() {
        return betslipController.getBetsValueContainer();
    }

    @Override
    public boolean handleError(RetrofitError networkError) {
        return isFinishing() || errorHandler.handleError(networkError);
    }

    @Override
    public void commandControllerDidFinish() {
        success(betslipController.getBetslipInformation());
    }

    @Override
    public void commandControllerDidFinishWithError(RetrofitError networkError) {
        handleError(networkError);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        shouldRefresh = true;
        switch (requestCode) {
            case LOGIN_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    placeBetController.retryPlaceBet();
                    shouldRefresh = false;
                }
                break;
            case BETSLIP_RECEIPT_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    finish();
                }
                break;
        }
    }

    @Override
    public void success(PlaceBetResponse object) {
        if (object.getSuccessfulBets().isEmpty()) {
            placeBetController.updateBetslipInformation(betslipInformation, object);
            tabLayoutController.setBetslipInformation(betslipInformation);
        } else {
            startActivityForResult(prepareBetslipReceiptIntent(object, betslipInformation.getStakeInformation()), BETSLIP_RECEIPT_ACTIVITY_REQUEST_CODE);
        }
    }

    private Intent prepareBetslipReceiptIntent(PlaceBetResponse placeBetResponse, StakeInformation stakeInformation) {
        Intent intent = new Intent(this, BetslipReceiptActivity.class);
        intent.putExtra(PlaceBetResponse.class.getName(), placeBetResponse);
        intent.putExtra(StakeInformation.class.getName(), stakeInformation);
        return intent;
    }

    @Override
    public void failure(RetrofitError error) {
        errorHandler.handleError(error);
    }

    private class UnsuccessfulRefreshProgressController extends ProgressControllerAdapter {

        @Override
        public void postExecuteWithSuccess(boolean success) {
            if (!success) {
                onRefresh();
            }
        }
    }
}
