package com.sportingbet.sportsbook.general.network.authentication;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface AuthenticationStore {

    String getActiveSessionId();

    void setActiveSessionId(String activeSessionId);
}
