package com.sportingbet.sportsbook.general.network.authentication;

import retrofit.RequestInterceptor;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class AuthenticationInterceptor implements RequestInterceptor, ResponseProcessor, AuthenticationStore {

    private static final String ACTIVE_SESSION_ID_KEY = "ASID";

    private String activeSessionId;

    public void processResponse(Response response) {
        for (Header header : response.getHeaders()) {
            if (header.getName().equals(ACTIVE_SESSION_ID_KEY)) {
                activeSessionId = header.getValue();
            }
        }
    }

    @Override
    public void intercept(RequestFacade requestFacade) {
        requestFacade.addHeader(ACTIVE_SESSION_ID_KEY, activeSessionId);
    }

    @Override
    public String getActiveSessionId() {
        return activeSessionId;
    }

    @Override
    public void setActiveSessionId(String activeSessionId) {
        this.activeSessionId = activeSessionId;
    }
}
