package com.sportingbet.sportsbook.coupons;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.handlers.NoTieCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.OverUnderCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.SingleCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.ThreeBallCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.TieCouponHandler;
import com.sportingbet.sportsbook.coupons.model.BookCoupon;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.coupons.model.SingleCoupon;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.Offer;
import com.sportingbet.sportsbook.model.book.WebCouponTemplate;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.sports.configuration.CouponType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CouponHandlerFactoryImpl implements CouponHandlerFactory {

    private final Map<Integer, CouponHandler> couponHandlers = new HashMap<>();

    @Getter
    private final BetslipCouponDelegate betslipCouponDelegate;

    public CouponHandlerFactoryImpl(BetslipCouponDelegate betslipCouponDelegate) {
        this.betslipCouponDelegate = betslipCouponDelegate;
        registerCouponHandlers();
    }

    private void registerCouponHandlers() {
        registerCouponHandler(CouponType.CouponTypeTie, new TieCouponHandler(betslipCouponDelegate));
        registerCouponHandler(CouponType.CouponTypeNoTie, new NoTieCouponHandler(betslipCouponDelegate));
        registerCouponHandler(CouponType.CouponTypeOverUnder, new OverUnderCouponHandler(betslipCouponDelegate));
        registerCouponHandler(CouponType.CouponTypeThreeBall, new ThreeBallCouponHandler(betslipCouponDelegate));
        SingleCouponHandler couponHandler = new SingleCouponHandler(betslipCouponDelegate);
        registerCouponHandler(CouponType.CouponTypeSingle, couponHandler);
        registerCouponHandler(CouponType.CouponTypeEmpty, couponHandler);
    }

    private void registerCouponHandler(CouponType couponType, CouponHandler couponHandler) {
        couponHandlers.put(couponType.value(), couponHandler);
    }

    @Override
    public CouponHandler getHandlerForItemType(int itemType) {
        return couponHandlers.get(itemType);
    }

    @Override
    public int getItemTypeForTemplate(WebCouponTemplate webCouponTemplate) {
        return CouponType.getItemTypeForTemplate(webCouponTemplate);
    }

    @Override
    public CouponType getCouponTypeForTemplate(WebCouponTemplate webCouponTemplate) {
        return CouponType.getCouponTypeForTemplate(webCouponTemplate);
    }

    @Override
    public List<Coupon> getCouponsForEvent(Event event, Book book) {
        if (bookHasUnknownType(book) || bookHasInvalidSelections(book)) {
            return getSimpleCoupons(event, book);
        } else {
            return Lists.<Coupon>newArrayList(new BookCoupon(event, book));
        }
    }

    @Override
    public boolean bookHasInvalidSelections(Book book) {
        return bookHasIncorrectSelectionCount(book) || bookHasEmptyTemplateNumber(book);
    }

    private boolean bookHasUnknownType(Book book) {
        return book.getWebCouponTemplate() == WebCouponTemplate.UNKNOWN;
    }

    private boolean bookHasEmptyTemplateNumber(Book book) {
        switch (book.getWebCouponTemplate()) {
            case CPNTEAMTIE:
            case CPNPLAYTIE:
            case CPN2BALLS:
                return getTemplateNumberFromSelection(book, 0) == null
                        || getTemplateNumberFromSelection(book, 1) == null
                        || getTemplateNumberFromSelection(book, 2) == null;
            default:
                return false;
        }
    }

    private Integer getTemplateNumberFromSelection(Book book, int selection) {
        return book.getFirstOffer().getSelections().get(selection).getTemplateNumber();
    }

    private boolean bookHasIncorrectSelectionCount(Book book) {
        return book.getSelectionCount() != CouponType.getCouponTypeForTemplate(book.getWebCouponTemplate()).getSelectionCount();
    }

    private List<Coupon> getSimpleCoupons(Event event, Book book) {
        Map<String, List<Selection>> nameSelections = createNameSelectionMap(book);
        List<Coupon> coupons = Lists.newArrayList();
        for (String selectionName : nameSelections.keySet()) {
            coupons.add(new SingleCoupon(event, book, nameSelections.get(selectionName)));
        }
        return coupons;
    }

    private Map<String, List<Selection>> createNameSelectionMap(Book book) {
        Map<String, List<Selection>> nameSelections = Maps.newLinkedHashMap();
        for (Offer offer : book.getOffers()) {
            setupSelectionsFromOffer(nameSelections, offer);
        }
        return nameSelections;
    }

    private void setupSelectionsFromOffer(Map<String, List<Selection>> nameSelections, Offer offer) {
        for (Selection selection : offer.getSelections()) {
            List<Selection> selectionList = nameSelections.get(selection.getName());
            if (selectionList == null) {
                selectionList = Lists.newArrayList();
                nameSelections.put(selection.getName(), selectionList);
            }
            selectionList.add(selection);
        }
    }
}
