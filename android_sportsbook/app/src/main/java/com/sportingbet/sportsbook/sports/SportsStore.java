package com.sportingbet.sportsbook.sports;

import com.sportingbet.sportsbook.model.Sport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface SportsStore {

    void setSports(List<Sport> sports);

    ArrayList<Sport> getSports();
}
