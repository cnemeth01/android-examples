package com.sportingbet.sportsbook.betslip.model.types;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class BetGroups {

    @Getter
    private BetGroup multiplesBetGroup;
}
