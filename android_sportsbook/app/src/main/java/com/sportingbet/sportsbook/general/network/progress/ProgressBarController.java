package com.sportingbet.sportsbook.general.network.progress;

import android.view.View;

import com.sportingbet.sportsbook.general.network.response.ProgressController;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public class ProgressBarController implements ProgressController {

    private final View progressBar;

    public ProgressBarController(View progressBar) {
        this.progressBar = progressBar;
    }

    @Override
    public void preExecute() {
        updateProgressBarVisibility(View.VISIBLE);
    }

    @Override
    public void postExecuteWithSuccess(boolean success) {
        updateProgressBarVisibility(View.GONE);
    }

    private void updateProgressBarVisibility(final int visibility) {
        progressBar.setVisibility(visibility);
    }
}
