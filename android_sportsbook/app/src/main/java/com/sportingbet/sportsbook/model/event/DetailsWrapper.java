package com.sportingbet.sportsbook.model.event;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@NoArgsConstructor
public class DetailsWrapper implements EventsWrapper {

    @Getter
    private EventsWrapperImpl details;

    public List<Event> getEvents() {
        return details.getEvents();
    }
}
