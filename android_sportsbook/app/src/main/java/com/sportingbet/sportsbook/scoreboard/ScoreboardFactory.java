package com.sportingbet.sportsbook.scoreboard;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.scoreboard.providers.FootballScoreboardProvider;
import com.sportingbet.sportsbook.scoreboard.providers.NotRunningScoreboardProvider;
import com.sportingbet.sportsbook.scoreboard.providers.TennisScoreboardProvider;
import com.sportingbet.sportsbook.scoreboard.providers.VolleyballScoreboardProvider;
import com.sportingbet.sportsbook.sports.configuration.ScoreboardLayoutType;
import com.sportingbet.sportsbook.sports.configuration.SportConfigProvider;

import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ScoreboardFactory {

    private final Map<ScoreboardLayoutType, ScoreboardProvider> typesScoreboardProviders = Maps.newHashMap();

    private final NotRunningScoreboardProvider notRunningScoreboardProvider;

    private SportConfigProvider sportConfigProvider;

    public ScoreboardFactory(Context context, SportConfigProvider sportConfigProvider) {
        this.sportConfigProvider = sportConfigProvider;
        this.notRunningScoreboardProvider = new NotRunningScoreboardProvider(context);
        registerScoreboardProvider(ScoreboardLayoutType.ScoreboardLayoutTypeFootball, new FootballScoreboardProvider(context));
        registerScoreboardProvider(ScoreboardLayoutType.ScoreboardLayoutTypeTennis, new TennisScoreboardProvider(context));
        registerScoreboardProvider(ScoreboardLayoutType.ScoreboardLayoutTypeVolleyball, new VolleyballScoreboardProvider(context));
    }

    private void registerScoreboardProvider(ScoreboardLayoutType scoreboardLayoutType, ScoreboardProvider scoreboardProvider) {
        typesScoreboardProviders.put(scoreboardLayoutType, scoreboardProvider);
    }

    public void prepareScoreboardForEvent(ViewGroup parent, Event event) {
        parent.removeAllViews();
        ScoreboardProvider scoreboardProvider = getScoreboardProvider(event);
        if (scoreboardProvider != null) {
            prepareScoreboardWithProvider(scoreboardProvider, parent, event);
        }
    }

    private ScoreboardProvider getScoreboardProvider(Event event) {
        ScoreboardLayoutType scoreboardLayoutType = sportConfigProvider.getScoreboardLayoutTypeForSportId(event.getSportId());
        if (event.isRunning()) {
            return typesScoreboardProviders.get(scoreboardLayoutType);
        }
        if (scoreboardLayoutType != ScoreboardLayoutType.ScoreboardLayoutTypeNoScore && !isPeriodAndTextEmpty(event)) {
            return notRunningScoreboardProvider;
        }
        return null;
    }

    private boolean isPeriodAndTextEmpty(Event event) {
        return event.getPeriodAndTime() == null || event.getPeriodAndTime().isEmpty();
    }

    private void prepareScoreboardWithProvider(ScoreboardProvider scoreboardProvider, ViewGroup parent, Event event) {
        View scoreboard = scoreboardProvider.createScoreboard(parent, event);
        scoreboardProvider.prepareScoreboard(scoreboard, event);
    }
}
