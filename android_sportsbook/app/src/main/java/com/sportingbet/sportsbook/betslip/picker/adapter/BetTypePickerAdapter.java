package com.sportingbet.sportsbook.betslip.picker.adapter;

import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.model.types.BetType;
import com.sportingbet.sportsbook.betslip.picker.value.PickerValueProvider;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetTypePickerAdapter extends AbstractPickerAdapter implements View.OnClickListener {

    @Getter
    private BetType betType;

    @Getter
    private String paymentMark;

    private View eachWayView;

    private boolean eachWayActivated;

    public BetTypePickerAdapter(Delegate delegate) {
        super(delegate);
    }

    public void setup(StakePicker stakePicker, PickerValueProvider pickerValueProvider, BetType betType, String paymentMark) {
        setup(stakePicker, pickerValueProvider);
        this.betType = betType;
        this.paymentMark = paymentMark;
    }

    public void setupEachView(View eachWayView) {
        this.eachWayView = eachWayView;
        this.eachWayView.setActivated(eachWayActivated);
        this.eachWayView.setOnClickListener(this);
    }

    public void updateEachWay(boolean eachWayActivated) {
        this.eachWayActivated = eachWayActivated;
        if (eachWayView != null) {
            eachWayView.setActivated(eachWayActivated);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.each_way:
                eachWayActivated = !view.isActivated();
                view.setActivated(eachWayActivated);
                delegate.pickerAdapterUpdated(this);
                break;
            default:
                super.onClick(view);
                break;
        }
    }

    @Override
    public Object getPayout() {
        double stake = getStake();
        if (paymentMark != null && stake > 0) {
            return paymentMark;
        }
        return stake * getStakeMultiplier();
    }

    private double getStakeMultiplier() {
        return isEachWaySelected() ? betType.getEachwayStakeMultiplier() : betType.getStakeMultiplier();
    }

    @Override
    public double getTotalStake() {
        return getStake() * getNumberOfBets();
    }

    private Integer getNumberOfBets() {
        return isEachWaySelected() ? betType.getEachwayNumberOfBets() : betType.getNumberOfBets();
    }

    @Override
    public boolean isEachWaySelected() {
        return eachWayActivated;
    }
}
