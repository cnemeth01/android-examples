package com.sportingbet.sportsbook.betslip.controller;

import android.app.Activity;

import com.sportingbet.sportsbook.general.LoginResponseStore;
import com.sportingbet.sportsbook.general.helpers.BalanceLayoutHelper;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipBalanceController {

    private final Activity activity;

    private final LoginResponseStore loginResponseStore;

    public BetslipBalanceController(Activity activity, LoginResponseStore loginResponseStore) {
        this.activity = activity;
        this.loginResponseStore = loginResponseStore;
    }

    public void updateLayout() {
        BalanceLayoutHelper.setupBalanceLayout(activity, loginResponseStore.getLoginResponse());
    }
}
