package com.sportingbet.sportsbook.betslip.helpers;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.model.BetSelectionState;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class StakeHelper {

    public static void setupPotentialReturnView(Context context, TextView textView, String potentialReturn, String userCurrency) {
        try {
            setupStakeView(context, textView, Double.valueOf(potentialReturn), userCurrency);
        } catch (NumberFormatException e) {
            textView.setText(potentialReturn);
        }
    }

    public static void setupStakeView(Context context, TextView textView, double stake, String userCurrency) {
        textView.setText(context.getString(R.string.betslip_stake_format, stake, userCurrency));
    }

    public static void setupCashOutView(Context context, TextView textView, double amount, String userCurrency) {
        textView.setText(createSpannableStringForCashOut(context, amount, userCurrency));
    }

    private static SpannableString createSpannableStringForCashOut(Context context, double amount, String userCurrency) {
        String amountAndCurrency = context.getString(R.string.my_bets_cashout_amount_currency, amount, userCurrency);
        String textAndAmount = context.getString(R.string.my_bets_cashout_format, amountAndCurrency);
        final StyleSpan bold = new StyleSpan(android.graphics.Typeface.BOLD);
        SpannableString spannableString = new SpannableString(textAndAmount);
        spannableString.setSpan(bold, textAndAmount.length() - amountAndCurrency.length(), textAndAmount.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return spannableString;
    }

    public static void setupEachWayView(Context context, TextView textView, BetSelectionState betSelectionState) {
        textView.setText(context.getString(R.string.coupon_each_way, betSelectionState.getEachWayReduction(), betSelectionState.getEachWayPlaceTerm()));
    }

    public static void setupEachWayView(Context context, TextView textView, long eachWayReduction, long eachWayPlaceTerm) {
        textView.setText(context.getString(R.string.coupon_each_way, eachWayReduction, eachWayPlaceTerm));
    }
}
