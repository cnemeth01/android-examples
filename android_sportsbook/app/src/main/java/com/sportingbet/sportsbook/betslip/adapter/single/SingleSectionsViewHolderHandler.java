package com.sportingbet.sportsbook.betslip.adapter.single;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.adapter.AbstractViewHolderHandler;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SingleSectionsViewHolderHandler extends AbstractViewHolderHandler {

    private final SingleValueController betslipValueController;

    public SingleSectionsViewHolderHandler(Context context, DataSource dataSource, SingleValueController betslipValueController) {
        super(context, dataSource);
        this.betslipValueController = betslipValueController;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        RecyclerView.ViewHolder holder = new RecyclerView.ViewHolder(layoutInflater.inflate(R.layout.betslip_sections, parent, false)) {
        };
        layoutInflater.inflate(R.layout.betslip_stake, (ViewGroup) holder.itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder) {
        View stakeView = ((ViewGroup) viewHolder.itemView).getChildAt(0);
        TextView titleView = (TextView) stakeView.findViewById(R.id.title);
        titleView.setText(context.getString(R.string.betslip_singles_format, betslipValueController.getBetsCount()));
        StakePicker stakePicker = (StakePicker) stakeView.findViewById(R.id.stake_picker);
        betslipValueController.registerSinglesStakePicker(stakePicker);
    }
}
