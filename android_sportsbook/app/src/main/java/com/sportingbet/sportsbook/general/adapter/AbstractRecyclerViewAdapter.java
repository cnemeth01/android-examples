package com.sportingbet.sportsbook.general.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public abstract class AbstractRecyclerViewAdapter<T, H extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<H> {

    protected final Context context;

    protected final LayoutInflater layoutInflater;

    private final List<T> items = new ArrayList<>();

    private OnItemClickListener<T> itemClickListener;

    protected AbstractRecyclerViewAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setItemClickListener(OnItemClickListener<T> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    final protected T getItem(int position) {
        return items.get(position);
    }

    final public List<T> getItems() {
        return items;
    }

    public void setList(List<T> list) {
        items.clear();
        items.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public final int getItemCount() {
        return items == null ? 0 : items.size();
    }

    protected void initializeOnClickListener(final H holder, final T item) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClicked(holder.itemView, item);
            }
        });
    }

    public interface OnItemClickListener<T> {

        void onItemClicked(View view, T item);
    }
}