package com.sportingbet.sportsbook.coupons.util;

import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.model.event.Scores;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
class SubtitleHelper {

    public static String getSubtitle(Event event) {
        switch ((int) event.getSportId()) {
            case Sport.TENNIS:
            case Sport.VOLLEYBALL:
            case Sport.BEACH_VOLLEYBALL:
                if (event.isRunning()) {
                    Scores scores = event.getInRunningData().getScores();
                    if (scores != null) {
                        return scores.getFullScoreFormatted();
                    }
                }
            default:
                return event.getPeriodAndTime();
        }
    }
}
