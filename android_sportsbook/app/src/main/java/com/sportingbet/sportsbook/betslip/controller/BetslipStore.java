package com.sportingbet.sportsbook.betslip.controller;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface BetslipStore {

    long[] getSelections();

    void setSelections(long[] selections);

    boolean containsSelection(long selection);
}
