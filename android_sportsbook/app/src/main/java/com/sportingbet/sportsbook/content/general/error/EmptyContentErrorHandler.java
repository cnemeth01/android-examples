package com.sportingbet.sportsbook.content.general.error;

import android.app.Activity;

import com.sportingbet.sportsbook.general.helpers.empty.EmptyScreenController;
import com.sportingbet.sportsbook.general.network.error.RetryErrorHandler;

import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class EmptyContentErrorHandler extends RetryErrorHandler {

    private final EmptyScreenController emptyScreenController;

    public EmptyContentErrorHandler(Activity activity, EmptyScreenController emptyScreenController) {
        super(activity);
        this.emptyScreenController = emptyScreenController;
    }

    @Override
    public boolean handleError(RetrofitError networkError) {
        if (networkError.getResponse() != null && networkError.getResponse().getStatus() == 204) {
            emptyScreenController.showEmptyView();
            return true;
        }
        return super.handleError(networkError);
    }
}
