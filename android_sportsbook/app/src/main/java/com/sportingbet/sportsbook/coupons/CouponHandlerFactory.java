package com.sportingbet.sportsbook.coupons;

import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.WebCouponTemplate;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.sports.configuration.CouponType;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface CouponHandlerFactory {

    BetslipCouponDelegate getBetslipCouponDelegate();

    CouponHandler getHandlerForItemType(int itemType);

    int getItemTypeForTemplate(WebCouponTemplate webCouponTemplate);

    CouponType getCouponTypeForTemplate(WebCouponTemplate webCouponTemplate);

    List<Coupon> getCouponsForEvent(Event event, Book book);

    boolean bookHasInvalidSelections(Book book);
}
