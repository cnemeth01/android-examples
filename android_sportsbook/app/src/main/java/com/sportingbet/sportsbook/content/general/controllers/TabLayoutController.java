package com.sportingbet.sportsbook.content.general.controllers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.fragments.byevent.ByEventFragment;
import com.sportingbet.sportsbook.content.fragments.highlights.HighlightsFragment;
import com.sportingbet.sportsbook.content.fragments.inplay.InPlayFragment;
import com.sportingbet.sportsbook.content.fragments.upnext.UpNextFragment;
import com.sportingbet.sportsbook.content.general.fragments.ContentFragment;
import com.sportingbet.sportsbook.home.pager.fragment.NavigationDelegate;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.sports.SportIconsProvider;
import com.sportingbet.sportsbook.sports.configuration.SportConfigProvider;
import com.sportingbet.sportsbook.sports.configuration.TabLayoutType;

import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class TabLayoutController implements View.OnClickListener {

    private static final int HIGHLIGHTS_TAB_ID = 1;
    private static final int INPLAY_TAB_ID = 2;
    private static final int UPNEXT_TAB_ID = 3;
    private static final int BYEVENT_TAB_ID = 4;

    private final SportConfigProvider sportConfigProvider;

    private final SportIconsProvider sportIconsProvider;

    private final Context context;

    private NavigationDelegate delegate;

    private Map<Integer, View> views;
    private TabLayoutType tabLayoutType;

    public TabLayoutController(Context context, SportConfigProvider sportConfigProvider, SportIconsProvider sportIconsProvider) {
        this.context = context;
        this.sportConfigProvider = sportConfigProvider;
        this.sportIconsProvider = sportIconsProvider;
    }

    public void setup(ViewGroup tabLayout, Sport sport, NavigationDelegate delegate) {
        this.delegate = delegate;
        this.views = Maps.newHashMap();
        this.tabLayoutType = sportConfigProvider.getTabLayoutTypeForSportId(sport.getId());

        tabLayout.removeAllViews();
        switch (tabLayoutType) {
            case TabLayoutTypeDefault:
                prepareSportTabWithIcon(tabLayout, BYEVENT_TAB_ID, sportIconsProvider.getStateListDrawableFromSportId(sport.getId()));
                break;
            case TabLayoutTypeThreeTabs:
                prepareSportTabWithTitle(tabLayout, BYEVENT_TAB_ID, R.string.byevent_tab_title);
                prepareSeparator(tabLayout);
                prepareSportTabWithTitle(tabLayout, INPLAY_TAB_ID, R.string.inplay_tab_title);
                prepareSeparator(tabLayout);
                prepareSportTabWithTitle(tabLayout, UPNEXT_TAB_ID, R.string.upnext_tab_title);
                break;
            case TabLayoutTypeFourTabs:
                prepareSportTabWithIcon(tabLayout, HIGHLIGHTS_TAB_ID, sportIconsProvider.getStateListDrawableFromSportId(sport.getId()));
                prepareSeparator(tabLayout);
                prepareSportTabWithTitle(tabLayout, INPLAY_TAB_ID, R.string.inplay_tab_title);
                prepareSeparator(tabLayout);
                prepareSportTabWithTitle(tabLayout, UPNEXT_TAB_ID, R.string.upnext_tab_title);
                prepareSeparator(tabLayout);
                prepareSportTabWithTitle(tabLayout, BYEVENT_TAB_ID, R.string.byevent_tab_title);
                break;
        }
    }

    public void initializeActivatedTabId(Integer activatedTabId) {
        if (activatedTabId == null) {
            setupInitialState();
        } else {
            setupView(views.get(activatedTabId), true);
        }
    }

    public int getActiveTabId() {
        for (Integer tabId : views.keySet()) {
            if (views.get(tabId).isActivated()) {
                return tabId;
            }
        }
        return 0;
    }

    private void setupInitialState() {
        switch (tabLayoutType) {
            case TabLayoutTypeDefault:
            case TabLayoutTypeThreeTabs:
                onClick(views.get(BYEVENT_TAB_ID));
                break;
            case TabLayoutTypeFourTabs:
                onClick(views.get(HIGHLIGHTS_TAB_ID));
                break;
        }
    }

    private void prepareSportTabWithTitle(ViewGroup tabLayout, int tabId, int tabTitle) {
        TextView sportTab = (TextView) createSportTab(tabLayout, tabId, R.layout.sport_tab_text);
        sportTab.setText(tabTitle);
        views.put(tabId, sportTab);
    }

    private void prepareSportTabWithIcon(ViewGroup tabLayout, int tabId, Drawable drawable) {
        ImageView sportTab = (ImageView) createSportTab(tabLayout, tabId, R.layout.sport_tab_icon);
        sportTab.setImageDrawable(drawable);
        views.put(tabId, sportTab);
    }

    private void prepareSeparator(ViewGroup tabLayout) {
        tabLayout.addView(LayoutInflater.from(context).inflate(R.layout.sport_tab_separator, tabLayout, false));
    }

    private View createSportTab(ViewGroup tabLayout, int tabId, int layoutId) {
        View view = LayoutInflater.from(context).inflate(layoutId, tabLayout, false);
        view.setId(tabId);
        view.setOnClickListener(this);
        tabLayout.addView(view);
        return view;
    }

    @Override
    public void onClick(View clickedView) {
        if (!clickedView.isActivated()) {
            for (View view : views.values()) {
                setupView(view, false);
            }
            setupView(clickedView, true);
            delegate.switchFragment(createFragmentForTabId(clickedView.getId()));
            delegate.notifyChildFragmentOnResume();
        }
    }

    private ContentFragment createFragmentForTabId(int tabId) {
        switch (tabId) {
            case HIGHLIGHTS_TAB_ID:
                return new HighlightsFragment();
            case INPLAY_TAB_ID:
                return new InPlayFragment();
            case UPNEXT_TAB_ID:
                return new UpNextFragment();
            default:
                return new ByEventFragment();
        }
    }

    protected void setupView(View view, boolean selected) {
        view.setActivated(selected);
    }
}
