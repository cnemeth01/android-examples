package com.sportingbet.sportsbook.betslip.adapter;

import android.view.View;

import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.controller.bets.model.BetTypeState;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.betslip.model.StakeInformation;
import com.sportingbet.sportsbook.betslip.model.types.BetType;
import com.sportingbet.sportsbook.betslip.picker.adapter.BetTypePickerAdapter;
import com.sportingbet.sportsbook.betslip.picker.adapter.PickerAdapter;
import com.sportingbet.sportsbook.betslip.picker.value.PickerValueProvider;
import com.sportingbet.sportsbook.betslip.picker.value.StakeValueProvider;
import com.sportingbet.sportsbook.betslip.placebets.model.BetRequest;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetRequest;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

import java.util.Map;

import lombok.Setter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class BetTypeValueController implements BetslipValueProvider, PickerAdapter.Delegate {

    public interface Delegate {

        void notifyChange();
    }

    protected final Map<Object, BetTypePickerAdapter> pickerAdapters = Maps.newHashMap();

    private final BetsValueContainer betsValueContainer;

    protected PickerValueProvider pickerValueProvider;

    @Setter
    private Delegate delegate;

    public BetTypeValueController(BetsValueContainer betsValueContainer) {
        this.betsValueContainer = betsValueContainer;
    }

    public void setup(BetslipInformation betslipInformation) {
        pickerValueProvider = createStakeController(betslipInformation.getStakeInformation());
    }

    private StakeValueProvider createStakeController(StakeInformation stakeInformation) {
        return new StakeValueProvider(stakeInformation.getStakeIncrements(), stakeInformation.getDefaultStake());
    }

    @Override
    public String getTotalPayout() {
        double sum = 0;
        for (PickerAdapter pickerAdapter : pickerAdapters.values()) {
            Object payout = pickerAdapter.getPayout();
            if (payout instanceof Double) {
                sum += (Double) payout;
            } else {
                return payout.toString();
            }
        }
        return Double.toString(sum);
    }

    @Override
    public boolean hasTotalPayout() {
        try {
            return Double.valueOf(getTotalPayout()) > 0;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    @Override
    public double getTotalStake() {
        double sum = 0;
        for (PickerAdapter pickerAdapter : pickerAdapters.values()) {
            sum += pickerAdapter.getTotalStake();
        }
        return sum;
    }

    @Override
    public PlaceBetRequest getPlaceBetRequest() {
        PlaceBetRequest placeBetRequest = new PlaceBetRequest();
        for (BetTypePickerAdapter pickerAdapter : pickerAdapters.values()) {
            if (pickerAdapter.getStake() > 0) {
                placeBetRequest.getBetRequests().add(new BetRequest(pickerAdapter.getBetType().getBetId(), pickerAdapter.getStake(), pickerAdapter.isEachWaySelected()));
            }
        }
        return placeBetRequest;
    }

    @Override
    public int getPlaceBetDelay() {
        int maxDelay = 1;
        for (BetTypePickerAdapter pickerAdapter : pickerAdapters.values()) {
            if (pickerAdapter.getStake() > 0) {
                maxDelay = Math.max(pickerAdapter.getBetType().getInrunningDelay(), maxDelay);
            }
        }
        return maxDelay;
    }

    protected abstract void preparePickerAdapter(StakePicker stakePicker, View eachWayView, BetType betType);

    public void registerPickerAdapter(StakePicker stakePicker, View eachWayView, BetType betType) {
        stakePicker.setVisibility(View.VISIBLE);
        preparePickerAdapter(stakePicker, eachWayView, betType);
    }

    protected void updateBetTypePickerAdapter(BetTypePickerAdapter pickerAdapter) {
        BetTypeState betValue = getBetTypeState(pickerAdapter);
        pickerAdapter.updateValue(betValue.getStake());
        pickerAdapter.updateEachWay(betValue.isEachWay());
    }

    private BetTypeState getBetTypeState(BetTypePickerAdapter pickerAdapter) {
        BetTypeState betValue = betsValueContainer.getBetValue(pickerAdapter.getBetType());
        if (betValue == null) {
            betValue = new BetTypeState(null, false);
        }
        return betValue;
    }

    protected void updateBetsValueContainer(BetTypePickerAdapter betTypePickerAdapter) {
        BetTypeState betTypeState = new BetTypeState(betTypePickerAdapter.getValue(), betTypePickerAdapter.isEachWaySelected());
        betsValueContainer.putBetValue(betTypePickerAdapter.getBetType(), betTypeState);
    }

    @Override
    public void pickerAdapterUpdated(PickerAdapter pickerAdapter) {
        delegate.notifyChange();
    }
}
