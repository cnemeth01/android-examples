package com.sportingbet.sportsbook.scoreboard.providers;

import android.content.Context;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.model.event.InRunningData;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class FootballScoreboardProvider extends AbstractScoreboardProvider {

    public FootballScoreboardProvider(Context context) {
        super(context);
    }

    protected int getScoreboardLayout() {
        return R.layout.scoreboard_football;
    }

    @Override
    public void prepareScoreboard(View view, Event event) {
        InRunningData runningData = event.getInRunningData();
        getTextView(view, R.id.first_team).setText(runningData.getParticipantA());
        getTextView(view, R.id.second_team).setText(runningData.getParticipantB());
        getTextView(view, R.id.period_text).setText(event.getPeriodAndTime());
        getTextView(view, R.id.first_team_score).setText(runningData.getScoreA());
        getTextView(view, R.id.second_team_score).setText(runningData.getScoreB());
    }
}
