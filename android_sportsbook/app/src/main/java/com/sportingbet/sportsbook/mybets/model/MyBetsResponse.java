package com.sportingbet.sportsbook.mybets.model;

import lombok.Getter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsResponse {

    @Getter
    private Long cashoutGlobalDelay;

    @Getter
    private String webAuthenticationToken;

    @Getter
    private MyBets bets;
}
