package com.sportingbet.sportsbook.general.network.requesters;

import com.sportingbet.sportsbook.general.network.CallbackWrapper;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;

import retrofit.Callback;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class Requester<T> {

    private NetworkResponseListener<T> responseListener;

    private CallbackWrapper<T> callbackWrapper;

    public void setupResponseListener(NetworkResponseListener<T> responseListener) {
        this.responseListener = responseListener;
    }

    public void prepareRequest(ProgressController... progressControllers) {
        callbackWrapper = new CallbackWrapper<>(responseListener, progressControllers);
    }

    protected final Callback<T> getCallback() {
        return callbackWrapper;
    }

    public void cancelRequest() {
        if (callbackWrapper != null) {
            callbackWrapper.cancel();
        }
    }
}
