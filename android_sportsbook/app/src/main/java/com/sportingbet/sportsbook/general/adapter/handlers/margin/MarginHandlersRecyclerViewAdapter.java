package com.sportingbet.sportsbook.general.adapter.handlers.margin;

import android.content.Context;

import com.sportingbet.sportsbook.general.adapter.handlers.TypedHandlersRecyclerViewAdapter;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class MarginHandlersRecyclerViewAdapter<T> extends TypedHandlersRecyclerViewAdapter<T> {

    public MarginHandlersRecyclerViewAdapter(Context context) {
        super(context);
        addViewHolderHandler(Margin.class, new MarginViewHolderHandler(context));
    }

    @Override
    public void setList(List<Object> list) {
        if (shouldAppendMargin(list)) {
            list.add(new Margin());
        }
        super.setList(list);
    }

    private boolean shouldAppendMargin(List<Object> list) {
        return list != null && list.size() > 0 && !(list.get(list.size() - 1) instanceof Margin);
    }
}
