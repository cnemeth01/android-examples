package com.sportingbet.sportsbook.splash.commands;

import com.sportingbet.sportsbook.general.commands.NetworkCommand;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.sports.SportsStore;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportsCommand extends NetworkCommand<List<Sport>> {

    private final SportsRequester sportsRequester;
    private final SportsStore sportsStore;

    public SportsCommand(SportsRequester sportsRequester, SportsStore sportsStore) {
        this.sportsRequester = sportsRequester;
        this.sportsStore = sportsStore;
    }

    @Override
    public void execute() {
        sportsRequester.getSports(this);
    }

    @Override
    public void success(List<Sport> object) {
        sportsStore.setSports(object);
        super.success(object);
    }
}
