package com.sportingbet.sportsbook.coupons.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.coupons.AbstractCouponHandler;
import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.coupons.model.BookCoupon;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.coupons.util.CouponHelper;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class OverUnderCouponHandler extends AbstractCouponHandler<OverUnderCouponHandler.ViewHolder> implements EventCouponHandler<OverUnderCouponHandler.ViewHolder> {

    public OverUnderCouponHandler(BetslipCouponDelegate betslipCouponDelegate) {
        super(betslipCouponDelegate);
    }

    @Override
    public ViewHolder onCreateViewHolder(Context context, ViewGroup parent) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.coupon_over_under, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Coupon coupon, Coupon previousCoupon) {
        CouponHelper.setupCouponTexts(coupon, viewHolder.overText, viewHolder.underText);
        setupButtons(viewHolder, coupon, previousCoupon, coupon.isVisible());
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Event event, Event previousEvent) {
        onBindViewHolder(viewHolder, new BookCoupon(event, event.getFirstBook()), CouponHelper.createPreviousBookCoupon(previousEvent));
    }

    private void setupButtons(ViewHolder viewHolder, Coupon coupon, Coupon previousCoupon, boolean visible) {
        CouponHelper.setupButton(viewHolder.overButton, coupon.getEvent(), coupon.getBook(), CouponHelper.getFirstSelection(coupon), CouponHelper.getFirstSelection(previousCoupon), visible, betslipCouponDelegate);
        CouponHelper.setupButton(viewHolder.underButton, coupon.getEvent(), coupon.getBook(), CouponHelper.getSecondSelection(coupon), CouponHelper.getSecondSelection(previousCoupon), visible, betslipCouponDelegate);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView overText;
        private final TextView underText;

        private final CouponLayout overButton;
        private final CouponLayout underButton;

        public ViewHolder(View view) {
            super(view);
            overText = (TextView) view.findViewById(R.id.first_team);
            underText = (TextView) view.findViewById(R.id.second_team);

            overButton = (CouponLayout) view.findViewById(R.id.first_button);
            underButton = (CouponLayout) view.findViewById(R.id.second_button);
        }
    }
}
