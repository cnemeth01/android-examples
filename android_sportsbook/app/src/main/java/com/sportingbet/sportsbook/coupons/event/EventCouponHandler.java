package com.sportingbet.sportsbook.coupons.event;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.model.event.Event;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface EventCouponHandler<T extends RecyclerView.ViewHolder> {

    T onCreateViewHolder(Context context, ViewGroup parent);

    void onBindViewHolder(T viewHolder, Event event, Event previousEvent);
}
