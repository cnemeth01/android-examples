package com.sportingbet.sportsbook.coupons.event;

import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.handlers.EmptyCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.NoTieAlternativeCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.NoTieCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.OverUnderCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.ThreeBallCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.TieAlternativeCouponHandler;
import com.sportingbet.sportsbook.coupons.handlers.TieCouponHandler;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.sports.configuration.CouponType;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class EventCouponHandlerFactoryImpl implements EventCouponHandlerFactory {

    @Getter
    private final BetslipCouponDelegate betslipCouponDelegate;

    private final Map<Integer, EventCouponHandler> couponHandlers = new HashMap<>();

    public EventCouponHandlerFactoryImpl(BetslipCouponDelegate betslipCouponDelegate) {
        this.betslipCouponDelegate = betslipCouponDelegate;
        registerCouponHandlers();
    }

    protected void registerCouponHandlers() {
        registerCouponHandler(CouponType.CouponTypeEmpty, new EmptyCouponHandler());
        registerCouponHandler(CouponType.CouponTypeTie, new TieCouponHandler(betslipCouponDelegate));
        registerCouponHandler(CouponType.CouponTypeNoTie, new NoTieCouponHandler(betslipCouponDelegate));
        registerCouponHandler(CouponType.CouponTypeOverUnder, new OverUnderCouponHandler(betslipCouponDelegate));
        registerCouponHandler(CouponType.CouponTypeThreeBall, new ThreeBallCouponHandler(betslipCouponDelegate));
        registerCouponHandler(CouponType.CouponTypeTieAlternative, new TieAlternativeCouponHandler());
        registerCouponHandler(CouponType.CouponTypeNoTieAlternative, new NoTieAlternativeCouponHandler());
    }

    private void registerCouponHandler(CouponType couponType, EventCouponHandler couponHandler) {
        couponHandlers.put(couponType.value(), couponHandler);
    }

    @Override
    public int getItemTypeForEvent(Event event, CouponType defaultCouponType) {
        if (shouldUseDefaultCouponHandler(event, defaultCouponType)) {
            return getItemTypeForTemplate(event);
        } else {
            return getAlternativeCoupon(defaultCouponType).value();
        }
    }

    private int getItemTypeForTemplate(Event event) {
        return CouponType.getItemTypeForTemplate(event.getWebCouponTemplate());
    }

    @Override
    public EventCouponHandler getHandlerForEvent(Event event, CouponType defaultCouponType) {
        if (shouldUseDefaultCouponHandler(event, defaultCouponType)) {
            return couponHandlers.get(CouponType.getItemTypeForTemplate(event.getWebCouponTemplate()));
        } else {
            return couponHandlers.get(getAlternativeCoupon(defaultCouponType).value());
        }
    }

    private boolean shouldUseDefaultCouponHandler(Event event, CouponType defaultCouponType) {
        return eventHasDefaultCouponType(event, defaultCouponType) && eventHasDefaultSelectionCount(event, defaultCouponType);
    }

    private boolean eventHasDefaultCouponType(Event event, CouponType defaultCouponType) {
        return getItemTypeForTemplate(event) == defaultCouponType.value();
    }

    private boolean eventHasDefaultSelectionCount(Event event, CouponType defaultCouponType) {
        return event.getSelectionCount() == defaultCouponType.getSelectionCount();
    }

    @Override
    public EventCouponHandler getHandlerForItemType(int itemType) {
        return couponHandlers.get(itemType);
    }

    private CouponType getAlternativeCoupon(CouponType couponType) {
        switch (couponType) {
            case CouponTypeTie:
                return CouponType.CouponTypeTieAlternative;
            case CouponTypeNoTie:
                return CouponType.CouponTypeNoTieAlternative;
            default:
                return CouponType.CouponTypeEmpty;
        }
    }
}
