package com.sportingbet.sportsbook.content.fragments.event;

import android.support.v4.app.Fragment;
import android.view.View;

import com.sportingbet.sportsbook.R;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MarginController {

    private static final String MARGIN_STEP = "MARGIN_STEP";

    private final Fragment fragment;

    public MarginController(Fragment fragment) {
        this.fragment = fragment;
    }

    public void setMarginStep(int step) {
        fragment.getArguments().putInt(MARGIN_STEP, step);
    }

    public void prepareMargin(View view) {
        int pixelSize = fragment.getResources().getDimensionPixelSize(R.dimen.card_margin);
        int marginStep = fragment.getArguments().getInt(MARGIN_STEP);
        view.setPadding(0, pixelSize * (marginStep + 1), 0, 0);
    }
}
