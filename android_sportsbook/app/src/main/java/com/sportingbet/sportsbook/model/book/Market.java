package com.sportingbet.sportsbook.model.book;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class Market implements android.os.Parcelable {

    @Getter
    private int isVisible;

    @Getter
    private int isEachWay;

    @Getter
    private Long eachWayPlaceTerm;

    @Getter
    private Long eachWayReduction;

    boolean isVisible() {
        return isVisible == 1;
    }

    boolean isEachWay() {
        return isEachWay == 1;
    }
}
