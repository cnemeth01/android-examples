package com.sportingbet.sportsbook.scoreboard.providers;

import android.content.Context;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.model.event.Scores;
import com.sportingbet.sportsbook.scoreboard.ScoreboardHelper;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class VolleyballScoreboardProvider extends AbstractScoreboardProvider {

    private final ScoreboardHelper scoreboardHelper;

    public VolleyballScoreboardProvider(Context context) {
        super(context);
        scoreboardHelper = new ScoreboardHelper(context);
    }

    protected int getScoreboardLayout() {
        return R.layout.scoreboard_volleyball;
    }

    @Override
    public void prepareScoreboard(View view, Event event) {
        getTextView(view, R.id.first_team).setText(scoreboardHelper.getParticipantA(event));
        getTextView(view, R.id.second_team).setText(scoreboardHelper.getParticipantB(event));

        Scores scores = event.getInRunningData().getScores();
        setupIndicators(view, scores, R.drawable.scoreboard_volleyball_indicator);
        scoreboardHelper.setupScores(view, scores);

        scoreboardHelper.setupTotalSetScore(view.findViewById(R.id.volleyball_sets), scores);
        scoreboardHelper.setupCurrentSetScore(view.findViewById(R.id.volleyball_points), scores);
    }

    private void setupIndicators(View view, Scores scores, int drawableId) {
        scoreboardHelper.setTeamIndicatorVisibility(view, R.id.first_team_indicator, scores.getParticipantACurrentSetScore(), drawableId);
        scoreboardHelper.setTeamIndicatorVisibility(view, R.id.second_team_indicator, scores.getParticipantBCurrentSetScore(), drawableId);
    }
}
