package com.sportingbet.sportsbook.content.general.fragments.events;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.coupons.event.EventCouponHandlerFactory;
import com.sportingbet.sportsbook.general.adapter.handlers.history.HistoryHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.sports.configuration.CouponType;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class EventsAdapter extends HistoryHandlersRecyclerViewAdapter<Event> {

    private final EventCouponHandlerFactory couponHandlerFactory;
    private final CouponType defaultCouponType;

    public EventsAdapter(Context context, EventCouponHandlerFactory couponHandlerFactory, CouponType defaultCouponType) {
        super(context);
        this.couponHandlerFactory = couponHandlerFactory;
        this.defaultCouponType = defaultCouponType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateTypeViewHolder(ViewGroup parent, int viewType) {
        return couponHandlerFactory.getHandlerForItemType(viewType).onCreateViewHolder(context, parent);
    }

    @Override
    public int getTypeItemViewType(Event item) {
        return couponHandlerFactory.getItemTypeForEvent(item, defaultCouponType);
    }

    @Override
    public void onBindTypeViewHolder(RecyclerView.ViewHolder holder, Event item, Event previousItem, int position) {
        initializeOnClickListener(holder, item);
        couponHandlerFactory.getHandlerForEvent(item, defaultCouponType).onBindViewHolder(holder, item, previousItem);
    }
}
