package com.sportingbet.sportsbook.home.navigation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.general.controllers.TabLayoutController;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;
import com.sportingbet.sportsbook.model.Sport;

import javax.inject.Inject;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportNavigationFragment extends AbstractNavigationFragment<Sport> {

    @Inject
    TabLayoutController tabLayoutController;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.sport_navigation_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayoutController.setup((ViewGroup) view.findViewById(R.id.tab_layout), getNavigationItem(), this);
        tabLayoutController.initializeActivatedTabId(getActivatedTabId(savedInstanceState));
    }

    private Integer getActivatedTabId(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            return savedInstanceState.getInt(TabLayoutController.class.getName());
        }
        return null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Sport.class.getName(), getNavigationItem());
        outState.putInt(TabLayoutController.class.getName(), tabLayoutController.getActiveTabId());
    }

    public Sport getSport() {
        return getNavigationItem();
    }
}
