package com.sportingbet.sportsbook.betslip.commands;

import com.sportingbet.sportsbook.betslip.controller.BetslipController;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class RemoveAllBetsCommand extends BetslipCommand {

    public RemoveAllBetsCommand(BetslipController betslipController) {
        super(betslipController);
    }

    @Override
    public void execute() {
        betslipController.removeAllBets(this, this);
    }
}
