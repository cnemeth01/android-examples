package com.sportingbet.sportsbook.betslip.model;

import java.util.List;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class StakeInformation implements android.os.Parcelable {

    @Getter
    private String userCurrency;

    @Getter
    private Double defaultStake;

    @Getter
    private List<Double> stakeIncrements;
}
