package com.sportingbet.sportsbook.coupons.handlers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.util.CouponHelper;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class NoTieCouponHandler extends OverUnderCouponHandler {

    public NoTieCouponHandler(BetslipCouponDelegate betslipCouponDelegate) {
        super(betslipCouponDelegate);
    }

    @Override
    public ViewHolder onCreateViewHolder(Context context, ViewGroup parent) {
        return new NoTieViewHolder(LayoutInflater.from(context).inflate(R.layout.coupon_no_tie, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Event event, Event previousEvent) {
        super.onBindViewHolder(viewHolder, event, previousEvent);
        CouponHelper.setupSubtitle(((NoTieViewHolder) viewHolder).subtitle, event);
    }

    protected class NoTieViewHolder extends ViewHolder {

        private final TextView subtitle;

        public NoTieViewHolder(View view) {
            super(view);
            subtitle = (TextView) view.findViewById(R.id.subtitle);
        }
    }
}
