package com.sportingbet.sportsbook.model.events;

import com.google.common.base.Objects;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class EventClass implements android.os.Parcelable {

    @Getter
    private long id;

    @Getter
    private String name;

    @Getter
    private Category category;

    public enum Category {
        parentEventClass,
        eventClass;

        public static Category getCategoryByValue(int value) {
            switch (value) {
                case 0:
                    return parentEventClass;
                case 1:
                    return eventClass;
            }
            return values()[value];
        }

        public static int getCategoryValue(Category category) {
            return category.ordinal();
        }
    }

    @Override
    public boolean equals(Object o) {
        return Objects.equal(id, ((EventClass) o).getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}


