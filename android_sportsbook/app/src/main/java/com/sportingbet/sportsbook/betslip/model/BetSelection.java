package com.sportingbet.sportsbook.betslip.model;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class BetSelection {

    @Getter
    @Setter
    private BetSelectionState acceptedState;

    @Getter
    @Setter
    private BetSelectionState newState;

    public long getSelectionId() {
        return acceptedState.getSelectionId();
    }

    public double getOdds() {
        if (newState != null) {
            return newState.getOdds();
        }
        return acceptedState.getOdds();
    }

    public boolean isSPSelection() {
        return acceptedState.getIsSPSelection();
    }

    public boolean handicapUnchanged() {
        return newState == null || newState.getHandicap().equals(acceptedState.getHandicap());
    }
}
