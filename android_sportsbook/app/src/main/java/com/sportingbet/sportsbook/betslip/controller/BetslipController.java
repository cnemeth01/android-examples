package com.sportingbet.sportsbook.betslip.controller;

import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface BetslipController extends BetslipStore {

    void addBet(Event event, Book book, Selection selection, ErrorHandler errorHandler, ProgressController... progressControllers);

    void removeBet(Selection selection, ErrorHandler errorHandler, ProgressController... progressControllers);

    void removeAllBets(ErrorHandler errorHandler, ProgressController... progressControllers);

    void getBets(BetslipInformationDelegate delegate, ErrorHandler errorHandler, ProgressController... progressControllers);

    void clearBets();

    BetslipInformation getBetslipInformation();

    BetsValueContainer getBetsValueContainer();

    interface BetslipInformationDelegate {

        void success(BetslipInformation betslipInformation);
    }
}
