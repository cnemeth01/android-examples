package com.sportingbet.sportsbook.content.general.controllers.refresh;

import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.refresh.HandlerTask;

import java.util.Timer;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class RefreshController implements ProgressController, Runnable {

    private final RefreshTimeProvider refreshTimeProvider;

    private Timer timer;

    private HandlerTask handlerTask;

    private RefreshControllerListener refreshControllerListener;

    public RefreshController(RefreshTimeProvider refreshTimeProvider) {
        this.refreshTimeProvider = refreshTimeProvider;
    }

    public void registerRefreshController(RefreshControllerListener refreshControllerListener) {
        this.refreshControllerListener = refreshControllerListener;
    }

    public void restart() {
        timer = new Timer();
    }

    public void cancel() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void preExecute() {
        if (handlerTask != null) {
            handlerTask.cancel();
            handlerTask = null;
        }
    }

    @Override
    public void postExecuteWithSuccess(boolean success) {
        if (timer != null) {
            handlerTask = new HandlerTask(this);
            timer.schedule(handlerTask, refreshTimeProvider.getRefreshTime());
        }
    }

    @Override
    public void run() {
        refreshControllerListener.onRefresh();
    }

    public interface RefreshControllerListener {

        void onRefresh();
    }
}
