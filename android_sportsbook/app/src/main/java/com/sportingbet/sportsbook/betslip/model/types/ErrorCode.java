package com.sportingbet.sportsbook.betslip.model.types;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */

@JsonDeserialize(using = ErrorCodeDeserializer.class)
public enum ErrorCode {
    BET00005, BET00006, BETFT00014, BETFT00027, UNKNOWN
}

class ErrorCodeDeserializer extends JsonDeserializer<ErrorCode> {

    @Override
    public ErrorCode deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        try {
            return ErrorCode.valueOf(p.getText());
        } catch (Exception e) {
            return ErrorCode.UNKNOWN;
        }
    }
}