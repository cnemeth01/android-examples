package com.sportingbet.sportsbook.general.network.services;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.general.network.authentication.AuthenticationInterceptor;

import java.util.concurrent.Executors;

import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.android.MainThreadExecutor;
import retrofit.client.OkClient;
import retrofit.converter.Converter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ServiceFactory {

    public static PublishingService createPublishingService(OkClient okClient, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        RestAdapter.Builder builder = getRestAdapterBuilder(BuildConfig.PUBLISHING_SERVICE_ENDPOINT, okClient, converter);
        builder.setRequestInterceptor(authenticationInterceptor);
        return builder.build().create(PublishingService.class);
    }

    public static SessionService createSessionService(OkClient okClient, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        RestAdapter.Builder builder = getRestAdapterBuilder(BuildConfig.SESSION_SERVICE_ENDPOINT, okClient, converter);
        builder.setRequestInterceptor(authenticationInterceptor);
        return builder.build().create(SessionService.class);
    }

    public static AccountService createAccountService(OkClient okClient, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        RestAdapter.Builder builder = getRestAdapterBuilder(BuildConfig.ACCOUNT_SERVICE_ENDPOINT, okClient, converter);
        builder.setRequestInterceptor(authenticationInterceptor);
        return builder.build().create(AccountService.class);
    }

    public static BetService createBetService(OkClient okClient, Converter converter, AuthenticationInterceptor authenticationInterceptor) {
        RestAdapter.Builder builder = getRestAdapterBuilder(BuildConfig.BET_SERVICE_ENDPOINT, okClient, converter);
        builder.setRequestInterceptor(authenticationInterceptor);
        builder.setExecutors(Executors.newSingleThreadExecutor(), new MainThreadExecutor());
        return builder.build().create(BetService.class);
    }

    public static ConfigService createConfigService(OkClient okClient, Converter converter) {
        return getRestAdapterBuilder(BuildConfig.CONFIG_SERVICE_ENDPOINT, okClient, converter).build().create(ConfigService.class);
    }

    private static RestAdapter.Builder getRestAdapterBuilder(String serviceEndpoint, OkClient okClient, Converter converter) {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(serviceEndpoint);
        builder.setConverter(converter);
        builder.setClient(okClient);
        builder.setLogLevel(getLogLevel()).setLog(new AndroidLog("Retrofit"));
        return builder;
    }

    private static RestAdapter.LogLevel getLogLevel() {
        if (BuildConfig.DEBUG) {
            return RestAdapter.LogLevel.FULL;
        } else {
            return RestAdapter.LogLevel.NONE;
        }
    }
}
