package com.sportingbet.sportsbook.content.fragments.event;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.fragments.event.books.BooksCouponAdapter;
import com.sportingbet.sportsbook.content.fragments.event.handlers.DescriptionViewHolderHandler;
import com.sportingbet.sportsbook.content.fragments.event.handlers.EachWayViewHolderHandler;
import com.sportingbet.sportsbook.content.general.controllers.AbstractHeaderController;
import com.sportingbet.sportsbook.coupons.CouponHandlerFactory;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.general.adapter.handlers.ViewHolderHandler;
import com.sportingbet.sportsbook.general.adapter.handlers.history.HistoryHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.Market;
import com.sportingbet.sportsbook.model.event.Event;

import java.util.List;
import java.util.Set;

import lombok.Setter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BooksAdapter extends HistoryHandlersRecyclerViewAdapter<Coupon> {

    @Setter
    private long sportId;

    private final BooksCouponAdapter booksCouponAdapter;

    private final CouponHandlerFactory couponHandlerFactory;

    private final AbstractHeaderController headerController;

    public BooksAdapter(Context context, CouponHandlerFactory couponHandlerFactory) {
        super(context);
        this.couponHandlerFactory = couponHandlerFactory;
        this.headerController = new BooksHeaderController(context);
        this.booksCouponAdapter = new BooksCouponAdapter();
        addViewHolderHandler(Book.class, new BookViewHolderHandler());
        addViewHolderHandler(Market.class, new EachWayViewHolderHandler(context));
        addViewHolderHandler(String.class, new DescriptionViewHolderHandler(context));
    }

    @Override
    protected int getTypeItemViewType(Coupon item) {
        return couponHandlerFactory.getItemTypeForTemplate(item.getWebCouponTemplate());
    }

    @Override
    protected RecyclerView.ViewHolder onCreateTypeViewHolder(ViewGroup parent, int viewType) {
        return couponHandlerFactory.getHandlerForItemType(viewType).onCreateViewHolder(context, parent);
    }

    @Override
    protected void onBindTypeViewHolder(RecyclerView.ViewHolder holder, Coupon item, Coupon previousItem, int position) {
        couponHandlerFactory.getHandlerForItemType(getTypeItemViewType(item)).onBindViewHolder(holder, item, previousItem);
    }

    public void setEvent(Event event) {
        setList(prepareBooks(event));
        setSportId(event.getSportId());
    }

    public List<Object> prepareBooks(Event event) {
        Set<Book> newOpenedBooks = Sets.newHashSet();
        List<Object> objects = Lists.<Object>newArrayList(event.getBooks());
        for (Book book : event.getBooks()) {
            if (book.getFirstOffer() != null) {
                List<Object> bookObjects = prepareBookObjects(event, book);
                booksCouponAdapter.putOpenedBooks(book, bookObjects);
                newOpenedBooks.add(book);
                objects.addAll(objects.indexOf(book) + 1, bookObjects);
            }
        }
        booksCouponAdapter.clearRemovedBooks(newOpenedBooks);
        return objects;
    }

    private List<Object> prepareBookObjects(Event event, Book book) {
        List<Object> bookObjects = Lists.newArrayList();
        if (book.isEachWay()) {
            bookObjects.add(book.getMarket());
        }
        List<Coupon> coupons = couponHandlerFactory.getCouponsForEvent(event, book);
        bookObjects.addAll(coupons);
        if (book.hasDescription()) {
            bookObjects.add(book.getDescription());
        }
        return bookObjects;
    }

    public void clearBooks() {
        booksCouponAdapter.clearBooks();
    }

    public Set<Book> getBooks() {
        return booksCouponAdapter.getBooks();
    }

    public boolean containsBook(Book object) {
        return booksCouponAdapter.containsBook(object);
    }

    public void addBook(Book book) {
        booksCouponAdapter.addBook(book);
    }

    public void closeBook(Book book) {
        List<?> coupons = booksCouponAdapter.removeBook(book);
        getItems().removeAll(coupons);
        int position = getItems().indexOf(book);
        notifyItemChanged(position);
        notifyItemRangeRemoved(position + 1, coupons.size());
    }

    private class BookViewHolderHandler implements ViewHolderHandler<Book, BookViewHolderHandler.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            return new ViewHolder(layoutInflater.inflate(R.layout.books_header_item, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, Book book) {
            initializeOnClickListener(holder, book);
            setupHeaderItem(holder, book);
        }

        private void setupHeaderItem(ViewHolder holder, Book book) {
            holder.title.setText(book.getName());
            if (containsBook(book) && bookShouldHaveHeader(book) && !couponHandlerFactory.bookHasInvalidSelections(book)) {
                holder.headerContainer.setVisibility(View.VISIBLE);
                headerController.setup(holder.headerContainer, couponHandlerFactory.getCouponTypeForTemplate(book.getWebCouponTemplate()));
            } else {
                holder.headerContainer.setVisibility(View.GONE);
            }
            holder.itemView.setActivated(containsBook(book));
        }

        private boolean bookShouldHaveHeader(Book book) {
            switch (book.getWebCouponTemplate()) {
                case CPNTEAMTIE:
                case CPNPLAYTIE:
                case CPN2BALLS:
                case CPNTEAMNOTIE:
                case CPNPLAYNOTIE:
                    return true;
                default:
                    return false;
            }
        }

        protected class ViewHolder extends RecyclerView.ViewHolder {

            public final TextView title;
            public ViewGroup headerContainer;

            public ViewHolder(View itemView) {
                super(itemView);
                title = (TextView) itemView.findViewById(R.id.title);
                headerContainer = (ViewGroup) itemView.findViewById(R.id.header_container);
            }
        }
    }
}
