package com.sportingbet.sportsbook.home.navigation;

import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface NavigationItemFactory {

    NavigationItem getNavigationItemForId(int itemId);

    AbstractNavigationFragment createFragmentForNavigationItem(NavigationItem navigationItem);
}
