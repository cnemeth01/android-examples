package com.sportingbet.sportsbook.content.fragments.event.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.helpers.StakeHelper;
import com.sportingbet.sportsbook.betslip.receipt.adapter.handlers.AbstractViewHolderHandler;
import com.sportingbet.sportsbook.model.book.Market;

public class EachWayViewHolderHandler extends AbstractViewHolderHandler<Market, EachWayViewHolderHandler.ViewHolder> {

    public EachWayViewHolderHandler(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(createView(parent, R.layout.coupon_each_way));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Market market) {
        StakeHelper.setupEachWayView(context, viewHolder.getTitleView(), market.getEachWayReduction(), market.getEachWayPlaceTerm());
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);
        }

        public TextView getTitleView() {
            return (TextView) itemView;
        }
    }
}
