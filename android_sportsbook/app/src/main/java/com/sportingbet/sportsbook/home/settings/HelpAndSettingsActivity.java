package com.sportingbet.sportsbook.home.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.aboutlibraries.LibsBuilder;
import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.activity.ToolbarActivity;
import com.sportingbet.sportsbook.general.ui.dialog.WhiteDialog;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HelpAndSettingsActivity extends ToolbarActivity implements View.OnClickListener {

    @Override
    protected int getToolbarTitle() {
        return R.string.help_setting_title;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_and_settings);

        setupPhoneNumber();
        setupEmailAddress();
        setupLinks();
        setupVersionNumber();
    }

    private void setupPhoneNumber() {
        ((TextView) findViewById(R.id.help_phone_number)).setText(BuildConfig.HELP_PHONE_NUMBER);
    }

    private void setupEmailAddress() {
        ((TextView) findViewById(R.id.help_email_address)).setText(BuildConfig.HELP_EMAIL_ADDRESS);
    }

    private void setupLinks() {
        findViewById(R.id.gambling_commission).setOnClickListener(this);
        findViewById(R.id.help_footer_text).setOnClickListener(this);
        findViewById(R.id.underage_gambling).setOnClickListener(this);
    }

    private void setupVersionNumber() {
        ((TextView) findViewById(R.id.version_view)).setText(getString(R.string.help_version, BuildConfig.VERSION_NAME, BuildConfig.FLAVOR_backend));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gambling_commission:
            case R.id.help_footer_text:
                startActionViewWithLink(BuildConfig.LINK_GAMBLING_COMMISSION);
                break;
            case R.id.underage_gambling:
                startActionViewWithLink(BuildConfig.LINK_UNDERAGE_GAMBLING);
                break;
            case android.R.id.button2:
                callPhoneNumber();
                break;
        }
    }

    private void startActionViewWithLink(String urlLink) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlLink)));
    }

    private void callPhoneNumber() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + getPhoneNumber()));
        startActivity(callIntent);
    }

    private String getPhoneNumber() {
        return ((TextView) findViewById(R.id.help_phone_number)).getText().toString();
    }

    public void onRowClick(View view) {
        switch (view.getId()) {
            case R.id.row_phone:
                openCallDialog();
                break;
            case R.id.row_email:
                sendEmail();
                break;
            case R.id.row_responsible_gaming:
                startActionViewWithLink(BuildConfig.LINK_RESPONSIBLE_GAMING);
                break;
            case R.id.row_privacy_statement:
                startActionViewWithLink(BuildConfig.LINK_PRIVACY_STATEMENT);
                break;
            case R.id.row_terms_and_conditions:
                startActionViewWithLink(BuildConfig.LINK_TERMS_CONDITIONS);
                break;
            case R.id.row_licenses:
                openLicences();
                break;
        }
    }

    private void openCallDialog() {
        WhiteDialog whiteDialog = WhiteDialog.newInstance(getString(R.string.help_call_dialog_title), getPhoneNumber(), getString(android.R.string.cancel), getString(R.string.help_call_button), this);
        whiteDialog.setCancelable(true);
        whiteDialog.show(this.getSupportFragmentManager(), null);
    }

    private void sendEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + getEmailAddress()));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.help_email_subject));
        startActivity(Intent.createChooser(emailIntent, getString(R.string.help_email_chooser_text)));
    }

    private String getEmailAddress() {
        return ((TextView) findViewById(R.id.help_email_address)).getText().toString();
    }

    private void openLicences() {
        new LibsBuilder().withFields(R.string.class.getFields()).withActivityTheme(R.style.LicensesTheme).withActivityTitle(getString(R.string.help_licenses)).start(this);
    }
}
