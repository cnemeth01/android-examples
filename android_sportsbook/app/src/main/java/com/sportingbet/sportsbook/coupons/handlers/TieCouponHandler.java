package com.sportingbet.sportsbook.coupons.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.coupons.AbstractCouponHandler;
import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.coupons.model.BookCoupon;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.coupons.util.CouponHelper;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class TieCouponHandler extends AbstractCouponHandler<TieCouponHandler.ViewHolder> implements EventCouponHandler<TieCouponHandler.ViewHolder> {

    public TieCouponHandler(BetslipCouponDelegate betslipCouponDelegate) {
        super(betslipCouponDelegate);
    }

    @Override
    public ViewHolder onCreateViewHolder(Context context, ViewGroup parent) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.coupon_tie, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Coupon coupon, Coupon previousCoupon) {
        CouponHelper.setupCouponTieTexts(coupon, viewHolder.firstTeam, viewHolder.secondTeam);
        setupButtons(viewHolder, coupon, previousCoupon, coupon.isVisible());
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Event event, Event previousEvent) {
        onBindViewHolder(viewHolder, new BookCoupon(event, event.getFirstBook()), CouponHelper.createPreviousBookCoupon(previousEvent));
        CouponHelper.setupSubtitle(viewHolder.subtitle, event);
        CouponHelper.setupScoreTexts(event, viewHolder.firstTeamScore, viewHolder.secondTeamScore);
    }

    private void setupButtons(ViewHolder viewHolder, Coupon coupon, Coupon previousCoupon, boolean visible) {
        setupButtonWithTemplateNumber(viewHolder.firstButton, 1, coupon, previousCoupon, visible);
        setupButtonWithTemplateNumber(viewHolder.secondButton, 0, coupon, previousCoupon, visible);
        setupButtonWithTemplateNumber(viewHolder.thirdButton, 2, coupon, previousCoupon, visible);
    }

    private void setupButtonWithTemplateNumber(CouponLayout firstButton, int templateNumber, Coupon coupon, Coupon previousCoupon, boolean visible) {
        Selection selection = CouponHelper.getSelectionWithTemplateNumber(coupon, templateNumber);
        Selection previousSelection = CouponHelper.getPreviousSelectionWithTemplateNumber(previousCoupon, templateNumber);
        CouponHelper.setupButton(firstButton, coupon.getEvent(), coupon.getBook(), selection, previousSelection, visible, betslipCouponDelegate);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView firstTeam;
        private final TextView secondTeam;
        private final TextView subtitle;
        private final TextView firstTeamScore;
        private final TextView secondTeamScore;

        private final CouponLayout firstButton;
        private final CouponLayout secondButton;
        private final CouponLayout thirdButton;

        public ViewHolder(View view) {
            super(view);
            firstTeam = (TextView) view.findViewById(R.id.first_team);
            secondTeam = (TextView) view.findViewById(R.id.second_team);
            subtitle = (TextView) view.findViewById(R.id.subtitle);

            firstTeamScore = (TextView) view.findViewById(R.id.first_team_score);
            secondTeamScore = (TextView) view.findViewById(R.id.second_team_score);

            firstButton = (CouponLayout) view.findViewById(R.id.first_button);
            secondButton = (CouponLayout) view.findViewById(R.id.second_button);
            thirdButton = (CouponLayout) view.findViewById(R.id.third_button);
        }
    }
}
