package com.sportingbet.sportsbook.general.dagger.configuration;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.betslip.receipt.BalanceRequester;
import com.sportingbet.sportsbook.betslip.placebets.PlaceBetRequester;
import com.sportingbet.sportsbook.content.fragments.byevent.ByEventRequester;
import com.sportingbet.sportsbook.content.fragments.byevent.EventClassesRequester;
import com.sportingbet.sportsbook.content.fragments.byevent.events.EventsRequester;
import com.sportingbet.sportsbook.content.fragments.event.EventDetailsRequester;
import com.sportingbet.sportsbook.content.fragments.highlights.HighlightsRequester;
import com.sportingbet.sportsbook.content.fragments.inplay.InPlayRequester;
import com.sportingbet.sportsbook.content.fragments.upnext.UpNextRequester;
import com.sportingbet.sportsbook.general.network.services.AccountService;
import com.sportingbet.sportsbook.general.network.services.BetService;
import com.sportingbet.sportsbook.general.network.services.ConfigService;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.general.network.services.SessionService;
import com.sportingbet.sportsbook.home.account.login.LoginRequester;
import com.sportingbet.sportsbook.home.account.logout.LogoutRequester;
import com.sportingbet.sportsbook.home.account.webview.session.TempSessionRequester;
import com.sportingbet.sportsbook.mybets.MyBetsRequester;
import com.sportingbet.sportsbook.mybets.cashout.CashoutRequester;
import com.sportingbet.sportsbook.splash.commands.SessionRequester;
import com.sportingbet.sportsbook.splash.commands.SportConfigRequester;
import com.sportingbet.sportsbook.splash.commands.SportsRequester;

import dagger.Module;
import dagger.Provides;

@Module(
        includes = {
                ApplicationModule.class,
        },
        library = true
)
public class NetworkModule {

    @Provides
    SessionRequester provideSessionRequester(SessionService publishingService) {
        return new SessionRequester(publishingService, BuildConfig.DOMAIN_ID, BuildConfig.APP_ID);
    }

    @Provides
    SportsRequester provideSportsRequester(PublishingService publishingService) {
        return new SportsRequester(publishingService);
    }

    @Provides
    HighlightsRequester provideHighlightsRequester(PublishingService publishingService) {
        return new HighlightsRequester(publishingService);
    }

    @Provides
    InPlayRequester provideInPlayRequester(PublishingService publishingService) {
        return new InPlayRequester(publishingService);
    }

    @Provides
    UpNextRequester provideUpNextRequester(PublishingService publishingService) {
        return new UpNextRequester(publishingService);
    }

    @Provides
    ByEventRequester provideByEventRequester(PublishingService publishingService) {
        return new ByEventRequester(publishingService);
    }

    @Provides
    EventClassesRequester provideEventClassesRequester(PublishingService publishingService) {
        return new EventClassesRequester(publishingService);
    }

    @Provides
    EventsRequester provideEventsRequester(PublishingService publishingService) {
        return new EventsRequester(publishingService);
    }

    @Provides
    EventDetailsRequester provideEventDetailsRequester(PublishingService publishingService) {
        return new EventDetailsRequester(publishingService);
    }

    @Provides
    SportConfigRequester provideSportConfigRequester(ConfigService configService) {
        return new SportConfigRequester(configService);
    }

    @Provides
    LoginRequester provideLoginRequester(AccountService accountService) {
        return new LoginRequester(accountService);
    }

    @Provides
    LogoutRequester provideLogoutRequester(AccountService accountService) {
        return new LogoutRequester(accountService);
    }

    @Provides
    TempSessionRequester provideTempSessionRequester(SessionService sessionService) {
        return new TempSessionRequester(sessionService);
    }

    @Provides
    PlaceBetRequester providePlaceBetRequester(BetService betService) {
        return new PlaceBetRequester(betService);
    }

    @Provides
    MyBetsRequester provideMyBetsRequester(AccountService accountService) {
        return new MyBetsRequester(accountService);
    }

    @Provides
    CashoutRequester provideCashoutRequester(BetService betService) {
        return new CashoutRequester(betService);
    }

    @Provides
    BalanceRequester provideBalanceRequester(AccountService accountService) {
        return new BalanceRequester(accountService);
    }
}
