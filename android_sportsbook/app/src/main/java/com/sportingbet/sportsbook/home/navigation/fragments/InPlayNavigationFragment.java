package com.sportingbet.sportsbook.home.navigation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.home.inplay.InPlayTabController;
import com.sportingbet.sportsbook.home.navigation.NavigationItem;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;
import com.sportingbet.sportsbook.model.Sport;

import javax.inject.Inject;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class InPlayNavigationFragment extends AbstractNavigationFragment<NavigationItem> {

    @Inject
    InPlayTabController inPlayTabController;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.in_play_navigation_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inPlayTabController.setup((ViewGroup) view.findViewById(R.id.inplay_tab_indicator), this);
        inPlayTabController.initializeActivatedTabId(getActivatedTabId(savedInstanceState));
    }

    private Long getActivatedTabId(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            return savedInstanceState.getLong(InPlayTabController.class.getName());
        }
        return null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(InPlayTabController.class.getName(), inPlayTabController.getActiveTabId());
    }
}
