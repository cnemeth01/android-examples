package com.sportingbet.sportsbook.betslip.controller.bets.model;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetTypeState {

    @Getter
    private Double stake;

    @Getter
    private boolean isEachWay;

    public BetTypeState(Double stake, boolean isEachWay) {
        this.stake = stake;
        this.isEachWay = isEachWay;
    }
}
