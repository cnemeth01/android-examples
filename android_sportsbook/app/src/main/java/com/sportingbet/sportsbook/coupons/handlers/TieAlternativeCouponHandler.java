package com.sportingbet.sportsbook.coupons.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.coupons.util.CouponHelper;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class TieAlternativeCouponHandler implements EventCouponHandler<TieAlternativeCouponHandler.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(Context context, ViewGroup parent) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.coupon_tie_alternative, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Event event, Event previousEvent) {
        CouponHelper.setupAlternativeTexts(event, viewHolder.firstTeam, viewHolder.secondTeam);
        CouponHelper.setupSubtitle(viewHolder.subtitle, event);
        CouponHelper.setupScoreTexts(event, viewHolder.firstTeamScore, viewHolder.secondTeamScore);
        viewHolder.betnowButton.setEnabled(event.isVisible());
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView firstTeam;
        private final TextView secondTeam;
        private final TextView subtitle;
        private final TextView firstTeamScore;
        private final TextView secondTeamScore;
        private final Button betnowButton;

        public ViewHolder(View view) {
            super(view);
            firstTeam = (TextView) view.findViewById(R.id.first_team);
            secondTeam = (TextView) view.findViewById(R.id.second_team);
            subtitle = (TextView) view.findViewById(R.id.subtitle);

            firstTeamScore = (TextView) view.findViewById(R.id.first_team_score);
            secondTeamScore = (TextView) view.findViewById(R.id.second_team_score);

            betnowButton = (Button) view.findViewById(R.id.betnow_button);
        }
    }
}
