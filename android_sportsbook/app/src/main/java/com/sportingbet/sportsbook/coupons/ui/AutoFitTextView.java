package com.sportingbet.sportsbook.coupons.ui;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

public class AutoFitTextView extends TextView {

    private final float defaultTextSize;

    public AutoFitTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        defaultTextSize = getTextSize();
    }

    private void refitText(String text, int textWidth) {
        if (textWidth > 0) {
            float textSize = defaultTextSize;
            Paint testPaint = new Paint(getPaint());
            testPaint.setTextSize(textSize);
            while (testPaint.measureText(text) > textWidth) {
                textSize--;
                testPaint.setTextSize(textSize);
            }
            setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        refitText(getText().toString(), parentWidth);
    }

    @Override
    protected void onTextChanged(final CharSequence text, final int start, final int before, final int after) {
        refitText(text.toString(), getWidth());
    }
}
