package com.sportingbet.sportsbook.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.commands.CommandController;
import com.sportingbet.sportsbook.general.dagger.components.SportsbookActivity;
import com.sportingbet.sportsbook.general.network.progress.ViewProgressController;
import com.sportingbet.sportsbook.home.HomeActivity;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit.RetrofitError;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SplashActivity extends SportsbookActivity implements CommandController.Delegate, View.OnClickListener {

    private DialogErrorHandler dialogErrorHandler = new DialogErrorHandler(this);

    @Inject
    @Named("splash")
    CommandController commandController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        commandController.setDelegate(this);
        commandController.setProgressController(new ViewProgressController(findViewById(R.id.progress_bar)));

        dialogErrorHandler.initialize(R.string.splash_error, R.string.exit_app, R.string.retry, this);
        commandController.start();
    }

    @Override
    public void commandControllerDidFinish() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void commandControllerDidFinishWithError(RetrofitError networkError) {
        dialogErrorHandler.handleError(networkError);
    }

    @Override
    public void onBackPressed() {
        commandController.cancel();
        ActivityCompat.finishAffinity(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case android.R.id.button1:
                onBackPressed();
                break;
            case android.R.id.button2:
                commandController.start();
                break;
        }
    }
}
