package com.sportingbet.sportsbook.betslip.receipt;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.model.StakeInformation;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetResponse;
import com.sportingbet.sportsbook.betslip.receipt.adapter.BetslipReceiptAdapter;
import com.sportingbet.sportsbook.general.BalanceUpdateController;
import com.sportingbet.sportsbook.general.activity.ToolbarActivity;
import com.sportingbet.sportsbook.general.ui.recyclerview.DividerItemDecoration;

import javax.inject.Inject;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipReceiptActivity extends ToolbarActivity implements View.OnClickListener {

    @Inject
    BetslipReceiptController betslipReceiptController;

    @Inject
    BalanceUpdateController balanceUpdateController;

    @Override
    protected int getToolbarTitle() {
        return R.string.betslip_receipt_title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_bottom, android.R.anim.fade_out);
        setContentView(R.layout.betslip_receipt);

        PlaceBetResponse placeBetResponse = getObjectFromIntent(PlaceBetResponse.class);
        StakeInformation stakeInformation = getObjectFromIntent(StakeInformation.class);
        setupRecyclerView(placeBetResponse, stakeInformation);

        updateResult(placeBetResponse);
        betslipReceiptController.registerActivity(this, placeBetResponse);
    }

    private void setupRecyclerView(PlaceBetResponse placeBetResponse, StakeInformation stakeInformation) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        recyclerView.setAdapter(createAdapter(placeBetResponse, stakeInformation));
    }

    private RecyclerView.Adapter createAdapter(PlaceBetResponse placeBetResponse, StakeInformation stakeInformation) {
        return new BetslipReceiptAdapter(this, this, placeBetResponse, stakeInformation);
    }


    private void updateResult(PlaceBetResponse placeBetResponse) {
        if (!placeBetResponse.hasAnySelectionsRemainedInBetSlip()) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_out_bottom);
    }

    @Override
    public void onClick(View v) {
        finish();
    }

    private <T extends Parcelable> T getObjectFromIntent(Class<T> type) {
        return getIntent().getParcelableExtra(type.getName());
    }
}
