package com.sportingbet.sportsbook.general.helpers.empty;

import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class TextEmptyScreenController extends DefaultEmptyScreenController {

    private TextView emptyTextView;

    public TextEmptyScreenController(View view) {
        super(view);
        emptyTextView = (TextView) view.findViewById(R.id.empty_text);
    }

    public void setupText(String text) {
        emptyTextView.setText(text);
    }
}
