package com.sportingbet.sportsbook.model.book;

import com.google.common.base.Objects;

import java.util.List;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class Book implements android.os.Parcelable {

    @Getter
    private long id;

    @Getter
    private String name;

    @Getter
    private String description;

    @Setter
    private MarketType marketType;

    @Getter
    private Market market;

    @Getter
    private List<Offer> offers;

    public Offer getFirstOffer() {
        if (offers != null && offers.size() > 0) {
            return offers.get(0);
        }
        return null;
    }

    public Offer getSPOffer() {
        for (Offer offer : offers) {
            if (offer.isSPType()) {
                return offer;
            }
        }
        return null;
    }

    public WebCouponTemplate getWebCouponTemplate() {
        return marketType.getWebCouponTemplate();
    }

    public int getSelectionCount() {
        return getFirstOffer().getSelectionCount();
    }

    public boolean isVisible() {
        return market.isVisible();
    }

    public boolean isEachWay() {
        return market.isEachWay();
    }

    public boolean hasDescription() {
        return description != null && !description.isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        return o.getClass() == getClass() && Objects.equal(id, ((Book) o).getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
