package com.sportingbet.sportsbook.general.network.response;


import retrofit.RetrofitError;

/**
 * Copyright 2014 Tomasz Morcinek. All rights reserved.
 */
public interface NetworkResponseListener<T> {

    void success(T object);

    void failure(RetrofitError error);
}