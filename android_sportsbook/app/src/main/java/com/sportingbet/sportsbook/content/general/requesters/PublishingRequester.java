package com.sportingbet.sportsbook.content.general.requesters;

import com.sportingbet.sportsbook.general.network.requesters.Requester;
import com.sportingbet.sportsbook.general.network.services.PublishingService;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class PublishingRequester<T> extends Requester<T> {

    protected final PublishingService publishingService;

    public PublishingRequester(PublishingService publishingService) {
        this.publishingService = publishingService;
    }
}
