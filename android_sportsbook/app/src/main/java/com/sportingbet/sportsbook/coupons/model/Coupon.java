package com.sportingbet.sportsbook.coupons.model;

import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.WebCouponTemplate;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface Coupon {

    Event getEvent();

    Book getBook();

    WebCouponTemplate getWebCouponTemplate();

    List<Selection> getSelections();

    boolean isVisible();
}
