package com.sportingbet.sportsbook.home.account.login;

import com.sportingbet.sportsbook.home.account.model.LoginRequestBody;
import com.sportingbet.sportsbook.home.account.model.LoginResponse;
import com.sportingbet.sportsbook.general.network.CallbackWrapper;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.network.services.AccountService;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class LoginRequester {

    protected final AccountService accountService;

    public LoginRequester(AccountService accountService) {
        this.accountService = accountService;
    }

    public void requestLogin(LoginRequestBody loginRequestBody, NetworkResponseListener<LoginResponse> responseListener, ProgressController... progressController) {
        accountService.login(loginRequestBody, new CallbackWrapper<>(responseListener, progressController));
    }
}
