package com.sportingbet.sportsbook.betslip.controller.bets;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sportingbet.sportsbook.betslip.controller.bets.model.BetTypeState;
import com.sportingbet.sportsbook.betslip.model.types.BetGroup;
import com.sportingbet.sportsbook.betslip.model.types.BetGroups;
import com.sportingbet.sportsbook.betslip.model.types.BetType;
import com.sportingbet.sportsbook.model.book.selection.Selection;

import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetsValueContainer {

    private Map<BetType, BetTypeState> betValues = Maps.newHashMap();

    public void putBetValue(BetType betType, BetTypeState value) {
        betValues.put(betType, value);
    }

    public void clearBetValue(BetType betType) {
        betValues.remove(betType);
    }

    public BetTypeState getBetValue(BetType betType) {
        return betValues.get(betType);
    }

    public void clearBetValues() {
        betValues.clear();
    }

    public void setupBetGroups(BetGroups betGroups) {
        for (BetType betType : Sets.newHashSet(betValues.keySet())) {
            if (!betGroupContains(betGroups.getMultiplesBetGroup(), betType)) {
                betValues.remove(betType);
            }
        }
    }

    public void removeSelectedBet(Selection selection) {
        for (BetType betType : Sets.newHashSet(betValues.keySet())) {
            if (betType.getSelectionId() == selection.getId()) {
                betValues.remove(betType);
            }
        }
    }

    private boolean betGroupContains(BetGroup betGroup, BetType betType) {
        return betGroup.getBetType().contains(betType);
    }
}
