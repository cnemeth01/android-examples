package com.sportingbet.sportsbook.sports.configuration;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sportingbet.sportsbook.R;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportConfigPersistentStore {

    private static final String SPORT_CONFIG_FILE_NAME = "sports_config.json";

    private List<SportConfig> sportConfigs;

    private final Context context;

    private ObjectMapper objectMapper;

    public SportConfigPersistentStore(Context context, ObjectMapper objectMapper) {
        this.context = context;
        this.objectMapper = objectMapper;
    }

    public List<SportConfig> getSportConfigs() throws IOException {
        if (sportConfigs == null) {
            InputStream inputStream = getSportsConfigInputStream();
            TypeReference<List<SportConfig>> typeReference = new TypeReference<List<SportConfig>>() {
            };
            sportConfigs = objectMapper.readValue(inputStream, typeReference);
            inputStream.close();
        }
        return sportConfigs;
    }

    public void setSportConfigs(List<SportConfig> sportConfigs) {
        try {
            FileOutputStream outputStream = context.openFileOutput(SPORT_CONFIG_FILE_NAME, Context.MODE_PRIVATE);
            objectMapper.writeValue(outputStream, sportConfigs);
            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private InputStream getSportsConfigInputStream() {
        try {
            return context.openFileInput(SPORT_CONFIG_FILE_NAME);
        } catch (IOException e) {
            // File does not exist or is invalid
        }
        return context.getResources().openRawResource(R.raw.sports_config);
    }
}
