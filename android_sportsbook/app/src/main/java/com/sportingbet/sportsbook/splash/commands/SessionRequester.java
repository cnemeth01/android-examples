package com.sportingbet.sportsbook.splash.commands;

import com.sportingbet.sportsbook.general.network.CallbackWrapper;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.services.SessionService;

import retrofit.client.Response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SessionRequester {

    protected final SessionService sessionService;
    private final String domainId;
    private final String appId;

    public SessionRequester(SessionService sessionService, String domainId, String appId) {
        this.sessionService = sessionService;
        this.domainId = domainId;
        this.appId = appId;
    }

    public void getActiveSessionId(NetworkResponseListener<Response> responseListener) {
        sessionService.getActiveSessionId(domainId, appId, new CallbackWrapper<>(responseListener));
    }
}
