package com.sportingbet.sportsbook.content.fragments.highlights;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.content.general.requesters.PublishingRequester;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.model.event.DetailsWrapper;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HighlightsRequester extends PublishingRequester<DetailsWrapper> {

    public HighlightsRequester(PublishingService publishingService) {
        super(publishingService);
    }

    public void requestHighlightsForSport(long sportId) {
        publishingService.getHighlights(Long.toString(sportId), BuildConfig.HIGHLIGHTS_NUMBER_OF_EVENTS, getCallback());
    }
}
