package com.sportingbet.sportsbook.home.account.webview;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.WebHistoryItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.activity.ToolbarActivity;
import com.sportingbet.sportsbook.general.dagger.components.SportsbookActivity;
import com.sportingbet.sportsbook.general.network.error.RetryErrorHandler;
import com.sportingbet.sportsbook.general.network.progress.ProgressBarController;

import java.io.IOException;

import javax.inject.Inject;

import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class WebViewActivity extends ToolbarActivity {

    private static final String REGISTRATION_SUCCESS_SUFFIX = "/registration/success";

    private ProgressBarController progressController;

    @Inject
    protected RetryErrorHandler errorHandler;

    protected WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_outside_right);
        setContentView(R.layout.webview_host);

        setupCookieManager();
        setupWebView();

        progressController = new ProgressBarController(findViewById(R.id.progress_bar));
    }

    @SuppressWarnings("deprecation")
    private void setupCookieManager() {
        CookieManager.getInstance().removeAllCookie();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupWebView() {
        webView = (WebView) findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressController.preExecute();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                progressController.postExecuteWithSuccess(false);
                errorHandler.handleError(RetrofitError.networkError(failingUrl, new IOException(description)));
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressController.postExecuteWithSuccess(true);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (!webView.canGoBack() || successfullyRegistered()) {
            super.onBackPressed();
        } else {
            webView.goBack();
        }
    }

    private boolean successfullyRegistered() {
        return getCurrentWebHistoryItem().getUrl().endsWith(REGISTRATION_SUCCESS_SUFFIX);
    }

    private WebHistoryItem getCurrentWebHistoryItem() {
        return webView.copyBackForwardList().getCurrentItem();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_inside_left, R.anim.slide_out_right);
    }
}
