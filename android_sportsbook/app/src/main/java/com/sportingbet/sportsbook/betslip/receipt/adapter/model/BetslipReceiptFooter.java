package com.sportingbet.sportsbook.betslip.receipt.adapter.model;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipReceiptFooter {

    @Getter
    private String totalPotentialReturns;

    @Getter
    private String userCurrency;

    public BetslipReceiptFooter(String totalPotentialReturns, String userCurrency) {
        this.totalPotentialReturns = totalPotentialReturns;
        this.userCurrency = userCurrency;
    }
}
