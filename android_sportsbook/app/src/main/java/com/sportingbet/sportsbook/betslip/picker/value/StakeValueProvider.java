package com.sportingbet.sportsbook.betslip.picker.value;

import com.google.common.collect.Sets;

import java.util.List;
import java.util.TreeSet;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class StakeValueProvider implements PickerValueProvider {

    private final TreeSet<Double> values;

    private final Double defaultValue;

    public StakeValueProvider(List<Double> values, Double defaultValue) {
        this.values = Sets.newTreeSet(values);
        this.defaultValue = defaultValue;
    }

    public boolean hasNextValue(Double value) {
        return value == null || value < values.last();
    }

    public boolean hasPreviousValue(Double value) {
        return true;
    }

    public Double getNextValue(Double value) {
        if (value == null) {
            return defaultValue;
        }
        Double nextValue = values.higher(value);
        if (nextValue == null) {
            nextValue = values.last();
        }
        return nextValue;
    }

    @Override
    public Double getPreviousValue(Double value) {
        if (value == null) {
            return defaultValue;
        }
        return values.lower(value);
    }
}
