package com.sportingbet.sportsbook.home.pager;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.sportingbet.sportsbook.general.pager.AbstractPagerController;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class PagerController extends AbstractPagerController {

    public static final String DYNAMIC_SPORT_IDENTIFIER = "DYNAMIC_SPORT_IDENTIFIER";

    private PagerControllerDelegate delegate;

    public PagerController(PagerAdapter pagerAdapter) {
        super(pagerAdapter);
    }

    private PagerAdapter getPagerAdapter(){
        return (PagerAdapter) fragmentPagerAdapter;
    }

    @Override
    public void registerActivity(FragmentActivity activity) {
        super.registerActivity(activity);
        getPagerAdapter().initialize();
        getPagerAdapter().notifyDataSetChanged();
        updateStartPosition();
    }

    public void registerDelegate(PagerControllerDelegate delegate) {
        this.delegate = delegate;
    }

    public AbstractNavigationFragment getCurrentFragment() {
        return getPagerAdapter().getItem(viewPager.getCurrentItem());
    }

    public void selectPageWithId(long id) {
        PagerControllerDelegate currentDelegate = delegate;
        delegate = null;
        viewPager.setCurrentItem(getPagerAdapter().getPositionFromId(id), true);
        delegate = currentDelegate;
    }

    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        updateStartPosition();
        if (savedInstanceState.containsKey(DYNAMIC_SPORT_IDENTIFIER)) {
            getPagerAdapter().setDynamicSportIdentifier(savedInstanceState.getLong(DYNAMIC_SPORT_IDENTIFIER));
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (getPagerAdapter().getDynamicSportIdentifier() != null) {
            outState.putLong(DYNAMIC_SPORT_IDENTIFIER, getPagerAdapter().getDynamicSportIdentifier());
        }
    }

    @Override
    protected void updateStartPosition() {
        super.updateStartPosition();
        updateDelegateWithItemPosition();
    }

    protected void updateNewFragment() {
        super.updateNewFragment();
        updateDelegateWithItemPosition();
    }

    private void updateDelegateWithItemPosition() {
        if (delegate != null) {
            delegate.didSelectPageWithId(getPagerAdapter().getItemIdentifier(getSelectedItemPosition()));
        }
    }

    public interface PagerControllerDelegate {

        void didSelectPageWithId(long pageId);
    }
}
