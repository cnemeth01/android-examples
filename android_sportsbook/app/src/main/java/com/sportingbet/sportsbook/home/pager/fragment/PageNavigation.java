package com.sportingbet.sportsbook.home.pager.fragment;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface PageNavigation {

    void onPageSelected();

    void onPageDisabled();
}
