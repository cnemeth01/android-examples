package com.sportingbet.sportsbook.content.fragments.byevent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.fragments.byevent.events.EventsFragment;
import com.sportingbet.sportsbook.content.fragments.byevent.links.SportLink;
import com.sportingbet.sportsbook.content.fragments.byevent.links.SportLinkHelper;
import com.sportingbet.sportsbook.content.general.fragments.PublishingContentFragment;
import com.sportingbet.sportsbook.content.general.helpers.FragmentStateHelper;
import com.sportingbet.sportsbook.general.adapter.handlers.TypedHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.dagger.components.SportsbookActivity;
import com.sportingbet.sportsbook.general.network.progress.ItemProgressBarController;
import com.sportingbet.sportsbook.general.network.progress.ProgressControllerAdapter;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.ui.recyclerview.DividerItemDecoration;
import com.sportingbet.sportsbook.home.pager.PagerSwitcher;
import com.sportingbet.sportsbook.model.events.EventClass;
import com.sportingbet.sportsbook.model.events.EventClassWrapper;
import com.sportingbet.sportsbook.model.events.ParentEventClassWrapper;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ByEventFragment extends PublishingContentFragment<List<ParentEventClassWrapper>, EventClass> {

    private FragmentStateHelper fragmentStateHelper;

    @Inject
    PagerSwitcher pagerSwitcher;

    @Inject
    SportLinkHelper sportLinkHelper;

    @Inject
    @Getter
    ByEventRequester networkRequester;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.by_event_list_fragment;
    }

    @Override
    protected TypedHandlersRecyclerViewAdapter<EventClass> createAdapter() {
        return new ParentEventClassesAdapter(getActivity(), sportLinkHelper);
    }

    @Override
    protected ParentEventClassesAdapter getAdapter() {
        return (ParentEventClassesAdapter) super.getAdapter();
    }

    @Override
    protected DividerItemDecoration getDividerItemDecoration() {
        return new DividerItemDecoration(getActivity(), R.drawable.by_event_divider);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentStateHelper = new FragmentStateHelper(getAdapter());
        fragmentStateHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fragmentStateHelper.onSaveInstanceState(outState);
    }

    @Override
    public void success(List<ParentEventClassWrapper> object) {
        List<EventClass> eventClassList = object.get(0).getParentEventClasses();
        setupEventClasses(eventClassList);
        if (eventClassList.isEmpty()) {
            emptyScreenController.showEmptyView();
        } else {
            emptyScreenController.hideEmptyView();
        }
    }

    private void setupEventClasses(List<EventClass> eventClasses) {
        getAdapter().setEventClassList(eventClasses, getSport());
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        networkRequester.requestParentEventClasses(getSport().getId());
    }

    @Override
    public void onItemClicked(View view, Object item) {
        if (item instanceof SportLink) {
            pagerSwitcher.selectSportPage(((SportLink) item).getSport());
        } else {
            eventClassClicked(view, (EventClass) item);
        }
    }

    private void eventClassClicked(View view, EventClass eventClass) {
        switch (eventClass.getCategory()) {
            case parentEventClass:
                onParentEventClassClicked(view, eventClass);
                break;
            default:
                onEventClassClicked(eventClass);
                break;
        }
    }

    private void onEventClassClicked(EventClass eventClass) {
        getNavigationDelegate().addFragment(EventsFragment.newInstance(eventClass));
    }

    private void onParentEventClassClicked(View view, EventClass eventClass) {
        if (getAdapter().isEventClassOpened(eventClass)) {
            closeEventClass(eventClass);
        } else {
            openEventClass(view, eventClass);
        }
    }

    private void openEventClass(View view, EventClass eventClass) {
        EventClassesRequester eventClassesRequester = createEventClassesRequester(view, eventClass);
        eventClassesRequester.requestEventClasses(eventClass.getId());
    }

    private void closeEventClass(EventClass eventClass) {
        getAdapter().removeEventClassWithChildren(eventClass);
    }

    private EventClassesRequester createEventClassesRequester(View view, final EventClass eventClass) {
        SportsbookActivity activity = (SportsbookActivity) getActivity();
        EventClassesRequester eventClassesRequester = activity.getObjectGraph().get(EventClassesRequester.class);
        eventClassesRequester.setupResponseListener(new EventClassWrapperResponseListener(eventClass));
        eventClassesRequester.prepareRequest(new ItemProgressBarController(view), new ProgressControllerAdapter() {
            @Override
            public void postExecuteWithSuccess(boolean success) {
                if (success) {
                    scrollToItem(eventClass);
                }
            }
        });
        return eventClassesRequester;
    }

    private class EventClassWrapperResponseListener implements NetworkResponseListener<EventClassWrapper> {

        private final EventClass eventClass;

        public EventClassWrapperResponseListener(EventClass eventClass) {
            this.eventClass = eventClass;
        }

        @Override
        public void success(EventClassWrapper object) {
            getAdapter().insertEventClassWithChildren(eventClass, object.getEventClasses());
        }

        @Override
        public void failure(RetrofitError error) {
            ByEventFragment.this.failure(error);
        }
    }
}
