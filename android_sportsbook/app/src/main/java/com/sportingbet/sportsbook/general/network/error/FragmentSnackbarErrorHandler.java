package com.sportingbet.sportsbook.general.network.error;

import android.content.Context;
import android.view.ViewGroup;

import com.nispok.snackbar.Snackbar;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public class FragmentSnackbarErrorHandler extends SnackbarErrorHandler {

    private final ViewGroup rootView;

    public FragmentSnackbarErrorHandler(Context context, ViewGroup rootView) {
        super(context);
        this.rootView = rootView;
    }

    @Override
    protected void showSnackbar(Snackbar snackbar) {
        snackbar.show(rootView);
    }
}
