package com.sportingbet.sportsbook.mybets.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.general.fragments.ListContentFragment;
import com.sportingbet.sportsbook.content.general.helpers.BundleHelper;
import com.sportingbet.sportsbook.general.BalanceUpdateController;
import com.sportingbet.sportsbook.general.LoginResponseStore;
import com.sportingbet.sportsbook.general.adapter.handlers.TypedHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.helpers.empty.EmptyScreenController;
import com.sportingbet.sportsbook.general.helpers.empty.TextEmptyScreenController;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.home.account.webview.activities.MyAccountActivity;
import com.sportingbet.sportsbook.home.pager.fragment.PageNavigation;
import com.sportingbet.sportsbook.mybets.MyBetsRequester;
import com.sportingbet.sportsbook.mybets.cashout.CashoutRequester;
import com.sportingbet.sportsbook.mybets.fragments.adapter.MyBetsAdapter;
import com.sportingbet.sportsbook.mybets.fragments.filter.MyBetsResponseFilter;
import com.sportingbet.sportsbook.mybets.model.MyBet;
import com.sportingbet.sportsbook.mybets.model.MyBetNavigationItem;
import com.sportingbet.sportsbook.mybets.model.MyBetType;
import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;
import com.sportingbet.sportsbook.mybets.model.MyBetWrapper;
import com.sportingbet.sportsbook.mybets.pager.MyBetsResponseDataSet;

import java.util.List;

import javax.inject.Inject;

import lombok.Setter;
import retrofit.RetrofitError;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsFragment extends ListContentFragment<MyBetWrapper> implements NetworkResponseListener<MyBetsResponse>, View.OnClickListener, PageNavigation, MyBetsResponseDataSet {

    private final BundleHelper<MyBetNavigationItem> bundleHelper = new BundleHelper<>(MyBetNavigationItem.class);

    protected final MyBetsResponseFilter myBetsResponseFilter = new MyBetsResponseFilter();

    @Inject
    MyBetsRequester myBetsRequester;

    @Inject
    CashoutRequester cashoutRequester;

    @Inject
    BalanceUpdateController balanceUpdateController;

    @Inject
    LoginResponseStore loginResponseStore;

    @Setter
    private MyBetsResponse myBetsResponse;


    protected MyBetType myBetType;

    public static MyBetsFragment prepareInstance(MyBetsFragment myBetsFragment, MyBetNavigationItem myBetNavigationItem) {
        myBetsFragment.setArguments(myBetsFragment.bundleHelper.createBundle(myBetNavigationItem));
        return myBetsFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bundleHelper.onRestoreInstanceState(getArguments());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        bundleHelper.onSaveInstanceState(outState);
    }

    public String getTitle() {
        return bundleHelper.get().getTitle();
    }

    protected String getEmptyScreenTitle() {
        return getString(R.string.my_bets_settled_empty_bets_text);
    }

    @Override
    protected TypedHandlersRecyclerViewAdapter<MyBetWrapper> createAdapter() {
        return new MyBetsAdapter(getActivity(), this, loginResponseStore);
    }

    @Override
    protected MyBetsAdapter getAdapter() {
        return (MyBetsAdapter) super.getAdapter();
    }

    @Override
    protected EmptyScreenController getEmptyScreenController(View view) {
        return new TextEmptyScreenController(view);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myBetsRequester.setupResponseListener(this);
    }

    public void requestMyBets(MyBetType myBetType) {
        this.myBetType = myBetType;
        myBetsRequester.prepareRequest(refreshProgressController, progressBarController);
        switch (myBetType) {
            case open:
                myBetsRequester.requestMyBets(MyBetType.open);
                break;
            case settled:
                myBetsRequester.requestMyBets(MyBetType.settled);
                break;
        }
    }

    @Override
    public void success(MyBetsResponse myBetsResponse) {
        setupMyBets(prepareMyBets(myBetsResponse));
    }

    protected void setupMyBets(List<MyBet> myBets) {
        List<MyBetWrapper> wrapperItems = createMyBetsWrapper(myBets);
        getAdapter().setMyBets(wrapperItems);
        setupEmptyScreen(wrapperItems);
    }

    private void setupEmptyScreen(List<MyBetWrapper> items) {
        if (items.isEmpty()) {
            updateEmptyScreenTitle(getEmptyScreenTitle());
            emptyScreenController.showEmptyView();
        } else {
            emptyScreenController.hideEmptyView();
        }
    }

    protected List<MyBet> prepareMyBets(MyBetsResponse object) {
        return myBetsResponseFilter.createItems(object);
    }

    private List<MyBetWrapper> createMyBetsWrapper(List<MyBet> items) {
        List<MyBetWrapper> wrapperItems = Lists.newArrayList();
        for (MyBet myBet : items) {
            wrapperItems.add(new MyBetWrapper(myBet));
        }
        return wrapperItems;
    }

    protected void updateEmptyScreenTitle(String title) {
        ((TextEmptyScreenController) emptyScreenController).setupText(title);
    }

    @Override
    public void failure(RetrofitError retrofitError) {
        errorHandler.handleError(retrofitError);
    }

    @Override
    public void onItemClicked(View view, Object item) {
    }

    @Override
    public void onClick(View v) {
        startActivityForResult(new Intent(getActivity(), MyAccountActivity.class), 0);
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        requestMyBets(bundleHelper.get().getType());
    }

    @Override
    public void onPageSelected() {
        if (myBetsResponse == null) {
            onFragmentSelected();
        } else {
            success(myBetsResponse);
            myBetsResponse = null;
        }
    }

    @Override
    public void onPageDisabled() {
        onFragmentDisabled();
    }

    @Override
    public void onFragmentDisabled() {
        super.onFragmentDisabled();
        myBetsRequester.cancelRequest();
    }

    @Override
    public void notifyDataSetChanged() {
    }
}
