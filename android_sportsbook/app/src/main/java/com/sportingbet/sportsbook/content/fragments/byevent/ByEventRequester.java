package com.sportingbet.sportsbook.content.fragments.byevent;

import com.sportingbet.sportsbook.content.general.requesters.PublishingRequester;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.model.events.ParentEventClassWrapper;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ByEventRequester extends PublishingRequester<List<ParentEventClassWrapper>> {

    public ByEventRequester(PublishingService publishingService) {
        super(publishingService);
    }

    public void requestParentEventClasses(long sportId) {
        publishingService.getParentEventClasses(sportId, getCallback());
    }
}
