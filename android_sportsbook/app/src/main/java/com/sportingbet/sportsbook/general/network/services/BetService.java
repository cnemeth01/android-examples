package com.sportingbet.sportsbook.general.network.services;

import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetRequest;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetResponse;
import com.sportingbet.sportsbook.coupons.betslip.model.BetslipResponse;
import com.sportingbet.sportsbook.coupons.betslip.model.BetslipSelectionGroup;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetRequestType;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetsResponseType;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface BetService {

    @POST("/betslip/add")
    void addBet(@Body BetslipSelectionGroup betslipSelectionGroup, Callback<BetslipResponse> callback);

    @GET("/betslip/remove")
    void removeBet(@Query("selectionIds") String selectionIds, @Query("deleteAll") Boolean deleteAll, Callback<BetslipResponse> callback);

    @GET("/betslip/view")
    void getBets(Callback<BetslipInformation> callback);

    @POST("/betslip/placewithdelays")
    void placeBet(@Body PlaceBetRequest betslipSelectionGroup, Callback<PlaceBetResponse> callback);

    @POST("/cashoutbetswithdelays")
    void cashOut(@Body CashoutBetRequestType cashoutBetRequestType, Callback<CashoutBetsResponseType> callback);
}
