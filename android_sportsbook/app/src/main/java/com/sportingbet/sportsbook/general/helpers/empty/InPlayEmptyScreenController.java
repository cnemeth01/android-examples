package com.sportingbet.sportsbook.general.helpers.empty;

import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.model.Sport;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class InPlayEmptyScreenController extends AbstractEmptyScreenController {

    public InPlayEmptyScreenController(View view, View.OnClickListener onClickListener, Sport sport) {
        super(view.findViewById(R.id.empty_content_view), view.findViewById(R.id.recycler_view));
        initEmptyViews(view, onClickListener, sport);
    }

    private void initEmptyViews(View emptyView, View.OnClickListener onClickListener, Sport sport) {
        ((TextView) emptyView.findViewById(R.id.no_in_play_content)).setText(String.format(emptyView.getResources().getString(R.string.no_in_play_content_text), sport.getName()));
        emptyView.findViewById(R.id.no_in_play_content_button).setOnClickListener(onClickListener);
    }
}