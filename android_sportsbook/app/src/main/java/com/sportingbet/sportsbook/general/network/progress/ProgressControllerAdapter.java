package com.sportingbet.sportsbook.general.network.progress;

import com.sportingbet.sportsbook.general.network.response.ProgressController;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public abstract class ProgressControllerAdapter implements ProgressController {

    @Override
    public void preExecute() {
    }

    @Override
    public void postExecuteWithSuccess(boolean success) {
    }
}
