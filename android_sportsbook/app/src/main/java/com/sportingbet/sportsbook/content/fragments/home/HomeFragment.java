package com.sportingbet.sportsbook.content.fragments.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.fragments.event.EventDetailsFragment;
import com.sportingbet.sportsbook.content.general.controllers.EventsHeaderController;
import com.sportingbet.sportsbook.content.general.controllers.refresh.RefreshController;
import com.sportingbet.sportsbook.content.general.fragments.ListContentFragment;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandlerFactory;
import com.sportingbet.sportsbook.general.adapter.handlers.TypedHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.sports.configuration.SportConfigProvider;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit.RetrofitError;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HomeFragment extends ListContentFragment<Event> implements HomeFragmentController.HomeControllerDelegate, RefreshController.RefreshControllerListener {

    private HomeFragmentStateHelper fragmentStateHelper;

    @Inject
    RefreshController refreshController;

    @Inject
    EventCouponHandlerFactory eventCouponHandlerFactory;

    @Inject
    HomeFragmentController homeFragmentController;

    @Inject
    SportConfigProvider sportConfigProvider;

    @Inject
    EventsHeaderController eventsHeaderController;

    @Override
    protected TypedHandlersRecyclerViewAdapter<Event> createAdapter() {
        HomeAdapter homeAdapter = new HomeAdapter(this.getActivity(), eventsHeaderController, eventCouponHandlerFactory, sportConfigProvider);
        eventCouponHandlerFactory.getBetslipCouponDelegate().register(getView(), homeAdapter);
        return homeAdapter;
    }

    @Override
    protected HomeAdapter getAdapter() {
        return (HomeAdapter) super.getAdapter();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundResource(R.color.backgroundColor);

        fragmentStateHelper = new HomeFragmentStateHelper(getAdapter());
        fragmentStateHelper.onRestoreInstanceState(savedInstanceState);

        homeFragmentController.registerDelegate(this);

        refreshController.registerRefreshController(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fragmentStateHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onFragmentSelected() {
        refreshController.restart();
        super.onFragmentSelected();
    }

    @Override
    public void onFragmentDisabled() {
        super.onFragmentDisabled();
        homeFragmentController.cancelRequest();
        refreshController.cancel();
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        homeFragmentController.setupCommandController(progressBarController, refreshProgressController, refreshController);
    }

    @Override
    public void controllerDidFinishWithEvents(Map<Sport, List<Event>> events) {
        getAdapter().setHeaderEvents(events);
    }

    @Override
    public void controllerDidFinishWithError(RetrofitError retrofitError) {
        errorHandler.handleError(retrofitError);
    }

    @Override
    public void onItemClicked(View view, Object item) {
        if (item instanceof Sport) {
            onHeaderClicked((Sport) item);
        } else {
            getNavigationDelegate().addFragment(EventDetailsFragment.newInstance((Event) item));
        }
    }

    private void onHeaderClicked(Sport sport) {
        if (getAdapter().isHeaderOpened(sport)) {
            getAdapter().closeHeader(sport);
        } else {
            getAdapter().openHeader(sport);
            scrollToItem(sport);
        }
    }
}
