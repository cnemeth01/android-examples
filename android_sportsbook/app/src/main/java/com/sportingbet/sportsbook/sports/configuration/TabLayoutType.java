package com.sportingbet.sportsbook.sports.configuration;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@JsonDeserialize(using = TabLayoutTypeDeserializer.class)
@JsonSerialize(using = TabLayoutTypeSerializer.class)
public enum TabLayoutType {
    TabLayoutTypeDefault,
    TabLayoutTypeThreeTabs,
    TabLayoutTypeFourTabs,
}

class TabLayoutTypeDeserializer extends JsonDeserializer<TabLayoutType> {

    @Override
    public TabLayoutType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        switch (p.getIntValue()) {
            case 1:
                return TabLayoutType.TabLayoutTypeThreeTabs;
            case 2:
                return TabLayoutType.TabLayoutTypeFourTabs;
            default:
                return TabLayoutType.TabLayoutTypeDefault;
        }
    }
}

class TabLayoutTypeSerializer extends JsonSerializer<TabLayoutType> {

    @Override
    public void serialize(TabLayoutType value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(value.ordinal());
    }
}