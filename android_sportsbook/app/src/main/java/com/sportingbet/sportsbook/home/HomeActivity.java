package com.sportingbet.sportsbook.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.controller.BetslipStore;
import com.sportingbet.sportsbook.general.LoginResponseStore;
import com.sportingbet.sportsbook.general.dagger.components.SportsbookActivity;
import com.sportingbet.sportsbook.general.network.authentication.AuthenticationStore;
import com.sportingbet.sportsbook.general.ui.ViewHelper;
import com.sportingbet.sportsbook.home.account.AccountButtonsController;
import com.sportingbet.sportsbook.home.account.model.LoginResponse;
import com.sportingbet.sportsbook.home.betslip.BetslipButtonController;
import com.sportingbet.sportsbook.home.drawer.DrawerController;
import com.sportingbet.sportsbook.home.drawer.DrawerToggle;
import com.sportingbet.sportsbook.home.pager.PagerController;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.mybets.MyBetsActivity;
import com.sportingbet.sportsbook.home.mybets.MyBetsButtonController;
import com.sportingbet.sportsbook.sports.SportsStore;

import javax.inject.Inject;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HomeActivity extends SportsbookActivity implements DrawerController.DrawerControllerDelegate, PagerController.PagerControllerDelegate {

    public static final int LOGIN_ACTIVITY_MYBETS_REQUEST_CODE = 1;

    @Inject
    LoginResponseStore loginResponseStore;

    @Inject
    AuthenticationStore authenticationStore;

    @Inject
    SportsStore sportsStore;

    @Inject
    BetslipStore betslipStore;

    @Inject
    BetslipButtonController betslipButtonController;

    @Inject
    DrawerController drawerController;

    @Inject
    PagerController pagerController;

    @Inject
    AccountButtonsController accountButtonsController;

    @Inject
    MyBetsButtonController myBetsButtonController;

    DrawerToggle drawerToggle;

    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_top, android.R.anim.fade_out);
        setContentView(R.layout.home);

        if (savedInstanceState != null) {
            sportsStore.setSports(savedInstanceState.<Sport>getParcelableArrayList(SportsStore.class.getName()));
            authenticationStore.setActiveSessionId(savedInstanceState.getString(AuthenticationStore.class.getName()));
            loginResponseStore.setLoginResponse(savedInstanceState.<LoginResponse>getParcelable(LoginResponse.class.getName()));
            betslipStore.setSelections(savedInstanceState.getLongArray(BetslipStore.class.getName()));
        }

        setupControllers();
        setupViews();

        updateAccountButtons();
        updateBetslipButton();
    }

    private void setupControllers() {
        drawerController.registerActivity(this, this);
        pagerController.registerActivity(this);
        pagerController.registerDelegate(this);
        betslipButtonController.register((Button) findViewById(R.id.betslip_button));
        myBetsButtonController.register(findViewById(R.id.my_bets_layout));
    }

    private void setupViews() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerToggle = new DrawerToggle(this, drawerLayout, toolbar);
        drawerLayout.setDrawerListener(drawerToggle);
    }

    private void updateAccountButtons() {
        accountButtonsController.updateButtons(loginResponseStore.getLoginResponse());
    }

    private void updateBetslipButton() {
        betslipButtonController.updateButton(betslipStore.getSelections());
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateAccountButtons();
        AbstractNavigationFragment currentFragment = pagerController.getCurrentFragment();
        if (currentFragment.getActivity() != null) {
            currentFragment.notifyChildFragmentOnResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        AbstractNavigationFragment currentFragment = pagerController.getCurrentFragment();
        if (currentFragment.getActivity() != null) {
            currentFragment.notifyChildFragmentOnPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        betslipButtonController.unregister();
    }

    public boolean isCurrentFragment(AbstractNavigationFragment fragment) {
        return pagerController.getCurrentFragment() == fragment;
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        pagerController.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(SportsStore.class.getName(), sportsStore.getSports());
        outState.putString(AuthenticationStore.class.getName(), authenticationStore.getActiveSessionId());
        outState.putParcelable(LoginResponse.class.getName(), loginResponseStore.getLoginResponse());
        outState.putLongArray(BetslipStore.class.getName(), betslipStore.getSelections());
        pagerController.onSaveInstanceState(outState);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    private void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            FragmentManager fragmentManager = pagerController.getCurrentFragment().getChildFragmentManager();
            if (fragmentManager.getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else {
                fragmentManager.popBackStack();
            }
        }
    }

    @Override
    public void didSelectDrawerItemWithId(final long itemId) {
        drawerToggle.postOnDrawerClosed(new Runnable() {
            @Override
            public void run() {
                pagerController.selectPageWithId(itemId);
            }
        });
        closeDrawer();
    }

    @Override
    public void didSelectPageWithId(long pageId) {
        drawerController.selectDrawerItemWithId(pageId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOGIN_ACTIVITY_MYBETS_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    startActivity(new Intent(this, MyBetsActivity.class));
                }
                break;
            default:
                if (resultCode == RESULT_OK) {
                    ViewHelper.runAfterTransition(new Runnable() {
                        @Override
                        public void run() {
                            closeDrawer();
                        }
                    });
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_bets, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_my_bets:
                myBetsButtonController.handleMyBetsButton();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
