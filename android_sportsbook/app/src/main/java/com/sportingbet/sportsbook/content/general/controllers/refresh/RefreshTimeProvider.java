package com.sportingbet.sportsbook.content.general.controllers.refresh;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.general.LoginResponseStore;

import java.util.concurrent.TimeUnit;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class RefreshTimeProvider {

    private final LoginResponseStore loginResponseStore;

    public RefreshTimeProvider(LoginResponseStore loginResponseStore) {
        this.loginResponseStore = loginResponseStore;
    }

    public long getRefreshTime() {
        return TimeUnit.SECONDS.toMillis(getRefreshTimeInSeconds());
    }

    private long getRefreshTimeInSeconds() {
        return loginResponseStore.isLoggedIn() ? BuildConfig.REFRESH_PERIOD_SECONDS_LOGGED_IN : BuildConfig.REFRESH_PERIOD_SECONDS_LOGGED_OUT;
    }
}
