package com.sportingbet.sportsbook.content.fragments.inplay;

import com.sportingbet.sportsbook.content.general.fragments.events.EventsContentFragment;
import com.sportingbet.sportsbook.model.event.DetailsWrapper;
import com.sportingbet.sportsbook.sports.configuration.CouponType;

import javax.inject.Inject;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class InPlayFragment extends EventsContentFragment<DetailsWrapper> {

    @Inject
    @Getter
    InPlayRequester networkRequester;

    public void onRefresh() {
        super.onRefresh();
        networkRequester.requestInPlayForSport(getSport().getId());
    }

    @Override
    protected CouponType getDefaultCouponType() {
        return sportConfigProvider.getInPlayDefaultCouponTypeForSportId(getSport().getId());
    }
}
