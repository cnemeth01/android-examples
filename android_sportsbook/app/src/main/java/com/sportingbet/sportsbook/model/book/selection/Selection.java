package com.sportingbet.sportsbook.model.book.selection;

import com.google.common.base.Objects;
import com.sportingbet.sportsbook.model.book.price.Price;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class Selection implements android.os.Parcelable {

    @Getter
    private long id;

    @Getter
    private String name;

    @Getter
    private Integer templateNumber;

    @Getter
    private Price price;

    @Getter
    private String handicap;

    @Getter
    private Long handicapId;

    public Selection(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        return Objects.equal(id, ((Selection) o).getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }


    public double getOddsValue() {
        try {
            return Double.valueOf(getPrice().getEuroOdds());
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }
}
