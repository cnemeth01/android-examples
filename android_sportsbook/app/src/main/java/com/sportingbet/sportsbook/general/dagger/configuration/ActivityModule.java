package com.sportingbet.sportsbook.general.dagger.configuration;

import android.content.Context;

import com.google.common.eventbus.EventBus;
import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.BetslipActivity;
import com.sportingbet.sportsbook.betslip.controller.BetslipBalanceController;
import com.sportingbet.sportsbook.betslip.controller.BetslipController;
import com.sportingbet.sportsbook.betslip.controller.BetslipStore;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.placebets.PlaceBetController;
import com.sportingbet.sportsbook.betslip.placebets.PlaceBetRequester;
import com.sportingbet.sportsbook.betslip.receipt.BalanceRequester;
import com.sportingbet.sportsbook.betslip.receipt.BetslipReceiptActivity;
import com.sportingbet.sportsbook.betslip.receipt.BetslipReceiptController;
import com.sportingbet.sportsbook.content.fragments.byevent.ByEventFragment;
import com.sportingbet.sportsbook.content.fragments.byevent.EventClassesRequester;
import com.sportingbet.sportsbook.content.fragments.byevent.events.EventsFragment;
import com.sportingbet.sportsbook.content.fragments.event.EventDetailsFragment;
import com.sportingbet.sportsbook.content.fragments.highlights.HighlightsFragment;
import com.sportingbet.sportsbook.content.fragments.home.HomeFragment;
import com.sportingbet.sportsbook.content.fragments.home.HomeFragmentController;
import com.sportingbet.sportsbook.content.fragments.inplay.InPlayFragment;
import com.sportingbet.sportsbook.content.fragments.upnext.UpNextFragment;
import com.sportingbet.sportsbook.content.general.controllers.EventsHeaderController;
import com.sportingbet.sportsbook.content.general.controllers.TabLayoutController;
import com.sportingbet.sportsbook.content.general.controllers.refresh.RefreshController;
import com.sportingbet.sportsbook.content.general.controllers.refresh.RefreshTimeProvider;
import com.sportingbet.sportsbook.general.BalanceUpdateController;
import com.sportingbet.sportsbook.general.LoginResponseStore;
import com.sportingbet.sportsbook.general.commands.CommandController;
import com.sportingbet.sportsbook.general.dagger.components.SportsbookActivity;
import com.sportingbet.sportsbook.general.helpers.SharedPreferencesHelper;
import com.sportingbet.sportsbook.general.network.error.ActivitySnackbarErrorHandler;
import com.sportingbet.sportsbook.general.network.error.RetryErrorHandler;
import com.sportingbet.sportsbook.general.network.progress.RingProgressBarController;
import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.home.HomeActivity;
import com.sportingbet.sportsbook.home.account.AccountButtonsController;
import com.sportingbet.sportsbook.home.account.login.LoginActivity;
import com.sportingbet.sportsbook.home.account.login.LoginController;
import com.sportingbet.sportsbook.home.account.logout.LogoutCommand;
import com.sportingbet.sportsbook.home.account.logout.LogoutRequester;
import com.sportingbet.sportsbook.home.account.webview.activities.DepositActivity;
import com.sportingbet.sportsbook.home.account.webview.activities.MyAccountActivity;
import com.sportingbet.sportsbook.home.account.webview.activities.RegisterActivity;
import com.sportingbet.sportsbook.home.betslip.BetslipButtonController;
import com.sportingbet.sportsbook.home.drawer.DrawerController;
import com.sportingbet.sportsbook.home.inplay.InPlaySportFragment;
import com.sportingbet.sportsbook.home.inplay.InPlayTabController;
import com.sportingbet.sportsbook.home.mybets.MyBetsButtonController;
import com.sportingbet.sportsbook.home.navigation.configuration.NavigationConfiguration;
import com.sportingbet.sportsbook.home.navigation.fragments.HomeNavigationFragment;
import com.sportingbet.sportsbook.home.navigation.fragments.InPlayNavigationFragment;
import com.sportingbet.sportsbook.home.navigation.fragments.SportNavigationFragment;
import com.sportingbet.sportsbook.home.pager.PagerAdapter;
import com.sportingbet.sportsbook.home.pager.PagerController;
import com.sportingbet.sportsbook.home.pager.PagerSwitcher;
import com.sportingbet.sportsbook.home.settings.HelpAndSettingsActivity;
import com.sportingbet.sportsbook.mybets.MyBetsActivity;
import com.sportingbet.sportsbook.mybets.cashout.CashoutController;
import com.sportingbet.sportsbook.mybets.cashout.CashoutRequester;
import com.sportingbet.sportsbook.mybets.cashout.RingProgressBarResponseListener;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetsResponseType;
import com.sportingbet.sportsbook.mybets.fragments.MyBetsFragment;
import com.sportingbet.sportsbook.mybets.fragments.MyBetsOpenFragment;
import com.sportingbet.sportsbook.splash.SplashActivity;
import com.sportingbet.sportsbook.splash.commands.SessionCommand;
import com.sportingbet.sportsbook.splash.commands.SessionRequester;
import com.sportingbet.sportsbook.splash.commands.SportConfigCommand;
import com.sportingbet.sportsbook.splash.commands.SportConfigRequester;
import com.sportingbet.sportsbook.splash.commands.SportsCommand;
import com.sportingbet.sportsbook.splash.commands.SportsRequester;
import com.sportingbet.sportsbook.sports.SportIconsProvider;
import com.sportingbet.sportsbook.sports.SportsStore;
import com.sportingbet.sportsbook.sports.configuration.SportConfigPersistentStore;
import com.sportingbet.sportsbook.sports.configuration.SportConfigProvider;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Module(
        injects = {
                SplashActivity.class,
                HomeActivity.class,
                LoginActivity.class,
                BetslipActivity.class,
                BetslipReceiptActivity.class,
                RegisterActivity.class,
                MyAccountActivity.class,
                DepositActivity.class,
                HelpAndSettingsActivity.class,
                MyBetsActivity.class,
                SportNavigationFragment.class,
                HomeNavigationFragment.class,
                InPlayNavigationFragment.class,
                HighlightsFragment.class,
                InPlayFragment.class,
                UpNextFragment.class,
                ByEventFragment.class,
                EventClassesRequester.class,
                EventsFragment.class,
                EventDetailsFragment.class,
                InPlaySportFragment.class,
                HomeFragment.class,
                MyBetsFragment.class,
                MyBetsOpenFragment.class
        },
        addsTo = NetworkModule.class,
        library = true
)
public class ActivityModule {

    private final SportsbookActivity sportsbookActivity;

    public ActivityModule(SportsbookActivity sportsbookActivity) {
        this.sportsbookActivity = sportsbookActivity;
    }

    @Provides
    ErrorHandler provideErrorHandler() {
        return new ActivitySnackbarErrorHandler(sportsbookActivity);
    }

    @Provides
    SessionCommand provideSessionCommand(SessionRequester sessionRequester, LoginResponseStore loginResponseStore, BetslipStore betslipStore, BetsValueContainer betsValueContainer) {
        return new SessionCommand(sessionRequester, loginResponseStore, betslipStore, betsValueContainer);
    }

    @Provides
    SportsCommand provideGetSportsCommand(SportsRequester sportsRequester, SportsStore sportsStore) {
        return new SportsCommand(sportsRequester, sportsStore);
    }

    @Provides
    SportConfigCommand provideGetSportsCommand(SportConfigRequester sportConfigRequester, SportConfigPersistentStore sportConfigPersistentStore) {
        return new SportConfigCommand(sportConfigPersistentStore, sportConfigRequester);
    }

    @Provides
    LogoutCommand provideLogoutCommand(LogoutRequester logoutRequester) {
        return new LogoutCommand(logoutRequester);
    }

    @Provides
    @Named("splash")
    CommandController provideSplashController(SessionCommand sessionCommand, SportsCommand sportsCommand, SportConfigCommand sportConfigCommand) {
        return new CommandController(sessionCommand, sportsCommand, sportConfigCommand);
    }

    @Provides
    @Named("logout")
    CommandController provideLogoutController(LogoutCommand logoutCommand, SessionCommand sessionCommand) {
        return new CommandController(logoutCommand, sessionCommand);
    }

    @Provides
    DrawerController provideDrawerController(NavigationConfiguration navigationConfiguration, SportIconsProvider sportIconsProvider) {
        return new DrawerController(navigationConfiguration, sportIconsProvider);
    }

    @Singleton
    @Provides
    PagerController providePagerController(PagerAdapter pagerAdapter) {
        return new PagerController(pagerAdapter);
    }

    @Singleton
    @Provides
    PagerSwitcher providePagerSwitcher(PagerController pagerController) {
        return new PagerSwitcher(pagerController);
    }

    @Provides
    InPlayTabController provideInPlayPagerController(Context context, NavigationConfiguration navigationConfiguration, SportIconsProvider sportIconsProvider) {
        return new InPlayTabController(context, navigationConfiguration, sportIconsProvider);
    }

    @Singleton
    @Provides
    BetslipButtonController provideBetslipButtonController(EventBus eventBus) {
        return new BetslipButtonController(eventBus);
    }

    @Provides
    PagerAdapter providePagerAdapter(NavigationConfiguration navigationConfiguration) {
        return new PagerAdapter(sportsbookActivity.getSupportFragmentManager(), navigationConfiguration);
    }

    @Provides
    HomeFragmentController provideHomeFragmentController(PublishingService publishingService, NavigationConfiguration navigationConfiguration) {
        return new HomeFragmentController(publishingService, navigationConfiguration);
    }

    @Provides
    RetryErrorHandler provideRetryErrorHandler() {
        return new RetryErrorHandler(sportsbookActivity);
    }

    @Provides
    TabLayoutController provideTabLayoutManager(Context context, SportConfigProvider sportConfigProvider, SportIconsProvider sportIconsProvider) {
        return new TabLayoutController(context, sportConfigProvider, sportIconsProvider);
    }

    @Provides
    EventsHeaderController provideEventsHeaderController(Context context) {
        return new EventsHeaderController(context);
    }

    @Provides
    AccountButtonsController provideAccountButtonsController() {
        return new AccountButtonsController(sportsbookActivity);
    }

    @Provides
    MyBetsButtonController provideMyBetsButtonController(LoginResponseStore loginResponseStore) {
        return new MyBetsButtonController(sportsbookActivity, loginResponseStore);
    }

    @Provides
    LoginController provideLoginController(SharedPreferencesHelper sharedPreferencesHelper) {
        return new LoginController(sportsbookActivity, sharedPreferencesHelper);
    }

    @Provides
    PlaceBetController providePlaceBetController(LoginResponseStore loginResponseStore, PlaceBetRequester placeBetRequester) {
        return new PlaceBetController(sportsbookActivity, loginResponseStore, placeBetRequester);
    }

    @Provides
    BetslipBalanceController provideBetslipBalanceController(LoginResponseStore loginResponseStore) {
        return new BetslipBalanceController(sportsbookActivity, loginResponseStore);
    }

    @Provides
    BetslipReceiptController provideBetslipReceiptController(BetslipController betslipController, BetslipBalanceController betslipBalanceController, BalanceUpdateController balanceUpdateController) {
        return new BetslipReceiptController(betslipController, betslipBalanceController, balanceUpdateController);
    }

    @Provides
    RefreshController provideRefreshController(RefreshTimeProvider refreshTimeProvider) {
        return new RefreshController(refreshTimeProvider);
    }

    @Provides
    @Named("cashout")
    RingProgressBarController provideRingProgressBarController(RefreshTimeProvider refreshTimeProvider) {
        return new RingProgressBarController(sportsbookActivity, BuildConfig.CASHOUT_REQUEST_PERIOD_SECONDS, R.string.mybets_cash_out_progress_message);
    }

    @Provides
    @Named("cashout")
    RingProgressBarResponseListener<CashoutBetsResponseType> provideRingProgressBarResponseListener(ErrorHandler errorHandler, @Named("cashout") RingProgressBarController ringProgressBarController) {
        return new RingProgressBarResponseListener<>(errorHandler, ringProgressBarController);
    }

    @Provides
    CashoutController provideCashoutController(CashoutRequester cashoutRequester, BalanceUpdateController balanceUpdateController, @Named("cashout") RingProgressBarResponseListener<CashoutBetsResponseType> responseListener) {
        return new CashoutController(cashoutRequester, balanceUpdateController, responseListener);
    }

    @Provides
    BalanceUpdateController provideBalanceUpdateController(BalanceRequester balanceRequester, LoginResponseStore loginResponseStore, ErrorHandler errorHandler) {
        return new BalanceUpdateController(balanceRequester, loginResponseStore, errorHandler);
    }
}
