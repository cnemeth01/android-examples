package com.sportingbet.sportsbook.mybets.cashout.model;

import lombok.Getter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CashoutError {

    @Getter
    private Double newCashoutAmount;

    @Getter
    private CashoutErrorCode errorCode;

    @Getter
    private String errorText;

    @Getter
    private String localisedErrorMessage;

    @Getter
    private String additionalInformation;
}
