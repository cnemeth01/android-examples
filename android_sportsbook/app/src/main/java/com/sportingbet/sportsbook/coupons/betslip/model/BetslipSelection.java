package com.sportingbet.sportsbook.coupons.betslip.model;

import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.Offer;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class BetslipSelection {

    @Getter
    private long eventId;

    @Getter
    private String eventName;

    @Getter
    private long bookId;

    @Getter
    private String bookName;

    @Getter
    private long selectionId;

    @Getter
    private String selectionName;

    private Boolean isSPSelection;

    @Getter
    private String fractionalOdds;

    @Getter
    private String decimalOdds;

    @Getter
    private String americanOdds;

    @Getter
    private String handicap;

    @Getter
    private Long handicapId;

    @Getter
    private Long eachWayPlaceTerm;

    @Getter
    private Long eachWayReduction;

    public static BetslipSelection create(Event event, Book book, Selection selection) {
        BetslipSelection betslipSelection = new BetslipSelection();
        betslipSelection.eventId = event.getId();
        betslipSelection.eventName = event.getName();
        betslipSelection.bookId = book.getId();
        betslipSelection.bookName = book.getName();
        betslipSelection.selectionId = selection.getId();
        betslipSelection.selectionName = selection.getName();
        Offer offer = book.getSPOffer();
        if (offer != null && offer.getSelections().contains(selection)) {
            betslipSelection.isSPSelection = true;
        } else {
            betslipSelection.fractionalOdds = selection.getPrice().getUkOdds();
            betslipSelection.decimalOdds = selection.getPrice().getEuroOdds();
            betslipSelection.americanOdds = selection.getPrice().getUsOdds();
        }
        betslipSelection.handicap = selection.getHandicap();
        betslipSelection.handicapId = selection.getHandicapId();
        betslipSelection.eachWayPlaceTerm = book.getMarket().getEachWayPlaceTerm();
        betslipSelection.eachWayReduction = book.getMarket().getEachWayReduction();
        return betslipSelection;
    }

    public boolean getIsSPSelection() {
        return isSPSelection != null && isSPSelection;
    }
}
