package com.sportingbet.sportsbook.model.events;

import java.util.List;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class EventClassWrapper {

    @Getter
    private List<EventClass> eventClasses;
}
