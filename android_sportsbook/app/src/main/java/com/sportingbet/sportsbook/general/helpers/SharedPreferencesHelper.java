package com.sportingbet.sportsbook.general.helpers;

import android.content.SharedPreferences;

/**
 * Copyright 2014 Tomasz Morcinek. All rights reserved.
 */
public class SharedPreferencesHelper {

    private SharedPreferences sharedPreferences;

    public SharedPreferencesHelper(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void putString(String preferenceKey, String preferenceValue) {
        sharedPreferences.edit().putString(preferenceKey, preferenceValue).apply();
    }

    public String getString(String preferenceKey) {
        return sharedPreferences.getString(preferenceKey, null);
    }

    public void remove(String preferenceKey) {
        sharedPreferences.edit().remove(preferenceKey).apply();
    }

    public boolean contains(String key) {
        return sharedPreferences.contains(key);
    }
}
