package com.sportingbet.sportsbook.coupons;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */

import android.support.v7.widget.RecyclerView;

import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;

public abstract class AbstractCouponHandler<T extends RecyclerView.ViewHolder> implements CouponHandler<T> {

    protected final BetslipCouponDelegate betslipCouponDelegate;

    public AbstractCouponHandler(BetslipCouponDelegate betslipCouponDelegate) {
        this.betslipCouponDelegate = betslipCouponDelegate;
    }
}


