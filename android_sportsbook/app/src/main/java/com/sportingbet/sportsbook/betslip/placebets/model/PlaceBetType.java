package com.sportingbet.sportsbook.betslip.placebets.model;

import com.sportingbet.sportsbook.betslip.model.types.BetType;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class PlaceBetType extends BetType {

    @Getter
    private Double totalStake;

    private boolean isEachwayBet;

    @Getter
    private String potentialReturn;

    public boolean getIsEachwayBet() {
        return isEachwayBet;
    }
}
