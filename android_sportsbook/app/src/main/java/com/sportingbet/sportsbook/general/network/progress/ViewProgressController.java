package com.sportingbet.sportsbook.general.network.progress;

import android.view.View;

import com.sportingbet.sportsbook.general.network.response.ProgressController;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public class ViewProgressController implements ProgressController {

    private final View view;

    public ViewProgressController(View view) {
        this.view = view;
    }

    @Override
    public void preExecute() {
        updateViewVisibility(View.VISIBLE);
    }

    @Override
    public void postExecuteWithSuccess(boolean success) {
        updateViewVisibility(View.INVISIBLE);
    }

    private void updateViewVisibility(int visibility) {
        view.setVisibility(visibility);
    }
}
