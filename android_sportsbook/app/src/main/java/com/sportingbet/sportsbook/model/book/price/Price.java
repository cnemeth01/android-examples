package com.sportingbet.sportsbook.model.book.price;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class Price implements android.os.Parcelable {

    @Getter
    private long id;

    @Getter
    private String displayOdds;

    @Getter
    private int isOffered;

    @Getter
    private String notOfferedReason;

    @Getter
    private String ukOdds;

    @Getter
    private String euroOdds;

    @Getter
    private String usOdds;

    public boolean isOffered() {
        return isOffered == 1;
    }
}
