package com.sportingbet.sportsbook.general.network.services;

import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.event.DetailsWrapper;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.model.event.EventsWrapperImpl;
import com.sportingbet.sportsbook.model.events.EventClassWrapper;
import com.sportingbet.sportsbook.model.events.ParentEventClassWrapper;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface PublishingService {

    @GET("/sports")
    void getSports(Callback<List<Sport>> callback);

    @GET("/events/highlights/{sportsIds}?showScores=1&showFirstBook=1")
    void getHighlights(@Path("sportsIds") String sportIds, @Query("noOfEvents") int noOfEvents, Callback<DetailsWrapper> callback);

    @GET("/events/inplay/{sportsIds}?sortType=popularity&showScores=1&showFirstBook=1")
    void getInPlay(@Path("sportsIds") long sportId, Callback<DetailsWrapper> callback);

    @GET("/events/upcoming/{sportsIds}?sortType=eventStartTime&showFirstBook=1&page=1")
    void getUpNext(@Path("sportsIds") long sportId, @Query("eventsPerPage") int noOfEvents, Callback<DetailsWrapper> callback);

    @GET("/children/sport/{sportsIds}")
    void getParentEventClasses(@Path("sportsIds") long sportId, Callback<List<ParentEventClassWrapper>> callback);

    @GET("/children/parentEventClass/{parentEventClassId}")
    void getEventClasses(@Path("parentEventClassId") long parentEventClassId, Callback<EventClassWrapper> callback);

    @GET("/children/eventClass/{eventClassId}")
    void getEvents(@Path("eventClassId") long eventClassId, Callback<EventsWrapperImpl> callback);

    @GET("/children/event/{eventId}?openFirstBooksCount=2")
    void getEvent(@Path("eventId") long eventId, Callback<Event> callback);

    @GET("/children/event/{eventId}")
    void getEventWithSpecificBook(@Path("eventId") long eventId, @Query("openSpecifiedBooks") String openSpecifiedBooks, Callback<Event> callback);
}
