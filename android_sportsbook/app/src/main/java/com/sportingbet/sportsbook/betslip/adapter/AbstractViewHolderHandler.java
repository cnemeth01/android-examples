package com.sportingbet.sportsbook.betslip.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.general.adapter.FooterRecyclerViewAdapter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class AbstractViewHolderHandler<T extends RecyclerView.ViewHolder> implements FooterRecyclerViewAdapter.ViewHolderHandler<T> {

    protected final Context context;

    protected final LayoutInflater layoutInflater;

    protected final DataSource dataSource;

    public AbstractViewHolderHandler(Context context, DataSource dataSource) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.dataSource = dataSource;
    }

    public BetslipInformation getBetslipInformation() {
        return dataSource.getBetslipInformation();
    }

    public interface DataSource {

        BetslipInformation getBetslipInformation();
    }
}
