package com.sportingbet.sportsbook.home.account.login;

import android.app.Activity;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.network.error.ActivitySnackbarErrorHandler;

import retrofit.RetrofitError;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
class LoginErrorHandler extends ActivitySnackbarErrorHandler {

    public LoginErrorHandler(Activity activity) {
        super(activity);
    }

    @Override
    protected String provideErrorMessage(RetrofitError networkError) {
        return context.getString(R.string.login_error_message);
    }
}
