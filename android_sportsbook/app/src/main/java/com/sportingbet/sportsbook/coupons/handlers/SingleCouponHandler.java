package com.sportingbet.sportsbook.coupons.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.coupons.AbstractCouponHandler;
import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.coupons.util.CouponHelper;
import com.sportingbet.sportsbook.model.book.selection.Selection;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SingleCouponHandler extends AbstractCouponHandler<SingleCouponHandler.ViewHolder> {

    public SingleCouponHandler(BetslipCouponDelegate betslipCouponDelegate) {
        super(betslipCouponDelegate);
    }

    @Override
    public ViewHolder onCreateViewHolder(Context context, ViewGroup parent) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.coupon_single, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Coupon coupon, Coupon previousCoupon) {
        Selection selection = getCouponSelection(coupon, 0);
        Selection previousSelection = getCouponSelection(previousCoupon, 0);
        viewHolder.title.setText(selection.getName());
        CouponHelper.setupButton(viewHolder.button, coupon.getEvent(), coupon.getBook(), selection, previousSelection, coupon.isVisible(), betslipCouponDelegate);
        viewHolder.buttonContainer.removeAllViews();
        List<Selection> selections = coupon.getSelections();
        if (selections.size() > 1) {
            for (int i = 1; i < selections.size(); i++) {
                setupCouponButton(viewHolder.buttonContainer, coupon, selections.get(i), getCouponSelection(previousCoupon, i));
            }
        }
    }

    private Selection getCouponSelection(Coupon coupon, int position) {
        try {
            return coupon.getSelections().get(position);
        } catch (Exception e) {
            return null;
        }
    }

    private void setupCouponButton(ViewGroup buttonContainer, Coupon coupon, Selection selection, Selection previousSelection) {
        CouponLayout couponButton = (CouponLayout) LayoutInflater.from(buttonContainer.getContext()).inflate(R.layout.coupon_layout, buttonContainer, false);
        CouponHelper.setupButton(couponButton, coupon.getEvent(), coupon.getBook(), selection, previousSelection, coupon.isVisible(), betslipCouponDelegate);
        buttonContainer.addView(couponButton);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView title;
        private final CouponLayout button;
        private final ViewGroup buttonContainer;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            button = (CouponLayout) view.findViewById(R.id.button);
            buttonContainer = (ViewGroup) view.findViewById(R.id.button_container);
        }
    }
}
