package com.sportingbet.sportsbook.content.fragments.byevent.events;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.fragments.event.EventDetailsFragment;
import com.sportingbet.sportsbook.content.general.fragments.events.EventsContentFragment;
import com.sportingbet.sportsbook.content.general.helpers.BundleHelper;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.model.event.EventsWrapperImpl;
import com.sportingbet.sportsbook.model.events.EventClass;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class EventsFragment extends EventsContentFragment<EventsWrapperImpl> implements View.OnClickListener {

    @Inject
    @Getter
    EventsRequester networkRequester;

    protected final BundleHelper<EventClass> bundleHelper;

    private boolean replaceFragment;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.card_events_list_fragment;
    }

    public EventsFragment() {
        bundleHelper = new BundleHelper<>(EventClass.class);
    }

    public static EventsFragment newInstance(EventClass eventClass) {
        EventsFragment eventsFragment = new EventsFragment();
        eventsFragment.setArguments(eventsFragment.bundleHelper.createBundle(eventClass));
        return eventsFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            bundleHelper.onRestoreInstanceState(savedInstanceState);
        }

        EventClass eventClass = bundleHelper.get();
        ((TextView) view.findViewById(R.id.title)).setText(eventClass.getName());
        view.findViewById(R.id.close_button).setOnClickListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        bundleHelper.onSaveInstanceState(outState);
    }

    public void onRefresh() {
        super.onRefresh();
        networkRequester.requestEventsFromEventClass(bundleHelper.get().getId());
    }

    @Override
    public void success(EventsWrapperImpl object) {
        List<Event> events = object.getEvents();
        if (events.size() == 1) {
            replaceFragment = true;
            getNavigationDelegate().replaceFragment(this, EventDetailsFragment.newInstance(events.get(0)));
        } else {
            super.success(object);
        }
    }

    @Override
    public void onItemClicked(View view, Object item) {
        getNavigationDelegate().addFragment(EventDetailsFragment.newInstance((Event) item, 1));
    }

    @Override
    public void onClick(View v) {
        getActivity().onBackPressed();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (!enter && !replaceFragment) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_bottom);
        }
        return super.onCreateAnimation(transit, true, nextAnim);
    }
}
