package com.sportingbet.sportsbook.sports;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportIconsProvider {

    private final Context context;

    public SportIconsProvider(Context context) {
        this.context = context;
    }

    public int getWhiteIconResourceIdForSport(long sportId) {
        return getIconDrawableIdentifier(sportId, "white");
    }

    public int getGreyIconResourceIdForSport(long sportId) {
        return getIconDrawableIdentifier(sportId, "grey");
    }

    public int getBlueIconResourceIdForSport(long sportId) {
        return getIconDrawableIdentifier(sportId, "blue");
    }

    public int getMenuIconResourceIdForSport(long sportId) {
        return getDrawableIdentifier(getMenuIconResourceName(sportId));
    }

    private int getIconDrawableIdentifier(long sportId, String white) {
        return getDrawableIdentifier(getIconResourceName(sportId, white));
    }

    private String getIconResourceName(long sportId, String color) {
        return String.format("icon_%s_%s", getStringSportId(sportId), color);
    }

    private String getMenuIconResourceName(long sportId) {
        return String.format("menu_icon_%s", getStringSportId(sportId));
    }

    private int getDrawableIdentifier(String resourceName) {
        return context.getResources().getIdentifier(resourceName, "drawable", context.getPackageName());
    }

    private String getStringSportId(long sportId) {
        if (sportId < 0) {
            return "_" + Math.abs(sportId);
        }
        return Long.toString(sportId);
    }

    public StateListDrawable getStateListDrawableFromSportId(long sportId) {
        StateListDrawable states = new StateListDrawable();
        int blueIconResource = this.getBlueIconResourceIdForSport(sportId);
        if (blueIconResource != 0) {
            states.addState(new int[]{android.R.attr.state_activated},
                    context.getResources().getDrawable(blueIconResource));
        }
        int whiteIconResource = this.getWhiteIconResourceIdForSport(sportId);
        if (whiteIconResource != 0) {
            states.addState(new int[]{},
                    context.getResources().getDrawable(whiteIconResource));
        }
        return states;
    }
}
