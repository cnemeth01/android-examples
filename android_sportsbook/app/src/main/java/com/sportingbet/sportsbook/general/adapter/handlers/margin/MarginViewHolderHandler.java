package com.sportingbet.sportsbook.general.adapter.handlers.margin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.receipt.adapter.handlers.AbstractViewHolderHandler;

import hrisey.Parcelable;
import lombok.NoArgsConstructor;

class MarginViewHolderHandler extends AbstractViewHolderHandler<Margin, RecyclerView.ViewHolder> {

    public MarginViewHolderHandler(Context context) {
        super(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new RecyclerView.ViewHolder(createView(parent, R.layout.transparent_view)) {
        };
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, Margin margin) {
    }
}
