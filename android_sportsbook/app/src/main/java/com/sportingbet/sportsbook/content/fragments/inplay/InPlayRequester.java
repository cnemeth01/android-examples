package com.sportingbet.sportsbook.content.fragments.inplay;

import com.sportingbet.sportsbook.content.general.requesters.PublishingRequester;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.model.event.DetailsWrapper;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class InPlayRequester extends PublishingRequester<DetailsWrapper> {

    public InPlayRequester(PublishingService publishingService) {
        super(publishingService);
    }

    public void requestInPlayForSport(long sportId) {
        publishingService.getInPlay(sportId, getCallback());
    }
}
