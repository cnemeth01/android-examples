package com.sportingbet.sportsbook.general.network.error;

import android.content.Context;
import android.content.Intent;

import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.splash.SplashActivity;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BaseErrorHandler implements ErrorHandler {

    protected final Context context;

    public BaseErrorHandler(Context context) {
        this.context = context;
    }

    @Override
    public boolean handleError(RetrofitError networkError) {
        Response response = networkError.getResponse();
        if (response != null && response.getStatus() == 403) {
            context.startActivity(new Intent(context, SplashActivity.class));
            return true;
        }
        return false;
    }
}
