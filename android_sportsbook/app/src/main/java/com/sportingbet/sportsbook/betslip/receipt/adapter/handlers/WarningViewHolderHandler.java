package com.sportingbet.sportsbook.betslip.receipt.adapter.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;

public class WarningViewHolderHandler extends AbstractViewHolderHandler<String, WarningViewHolderHandler.ViewHolder> {

    public WarningViewHolderHandler(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(createView(parent, R.layout.betreceipt_warning));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, String object) {
        holder.warningText.setText(object);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView warningText;

        private ViewHolder(View view) {
            super(view);
            warningText = (TextView) view.findViewById(R.id.warning_text);
        }
    }
}
