package com.sportingbet.sportsbook.mybets.model;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class MyBetNavigationItem implements android.os.Parcelable {

    @Getter
    public String title;

    @Getter
    public MyBetType type;

    public MyBetNavigationItem(String title, MyBetType type) {
        this.title = title;
        this.type = type;
    }
}
