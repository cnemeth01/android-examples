package com.sportingbet.sportsbook.betslip.placebets.model;

import com.sportingbet.sportsbook.betslip.model.types.ErrorCode;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class PlaceBetError {

    @Getter
    private ErrorCode errorCode;

    @Getter
    private String localisedErrorMessage;
}
