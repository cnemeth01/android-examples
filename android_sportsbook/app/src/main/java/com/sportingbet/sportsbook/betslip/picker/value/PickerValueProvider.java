package com.sportingbet.sportsbook.betslip.picker.value;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface PickerValueProvider {

    boolean hasNextValue(Double value);

    boolean hasPreviousValue(Double value);

    Double getNextValue(Double value);

    Double getPreviousValue(Double value);
}
