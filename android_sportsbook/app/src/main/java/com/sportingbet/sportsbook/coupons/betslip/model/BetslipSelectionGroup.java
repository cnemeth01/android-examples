package com.sportingbet.sportsbook.coupons.betslip.model;

import com.google.common.collect.Lists;

import java.util.List;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipSelectionGroup {

    @Getter
    private List<BetslipSelection> selections;

    public static BetslipSelectionGroup create(BetslipSelection... betslipSelections) {
        BetslipSelectionGroup betslipSelectionGroup = new BetslipSelectionGroup();
        betslipSelectionGroup.selections = Lists.newArrayList(betslipSelections);
        return betslipSelectionGroup;
    }
}
