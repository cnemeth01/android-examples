package com.sportingbet.sportsbook.betslip.model.types;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class BetGroup {

    @Getter
    private List<BetType> betType;

    @Getter
    @JsonProperty("default")
    private boolean isDefault;
}
