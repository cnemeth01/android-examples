package com.sportingbet.sportsbook.mybets.cashout;

import android.os.Handler;
import android.view.View;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.general.BalanceUpdateController;
import com.sportingbet.sportsbook.general.network.progress.ProgressControllerAdapter;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetRequestType;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetsFailed;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetsRequest;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetsResponseType;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutError;
import com.sportingbet.sportsbook.mybets.fragments.adapter.MyBetsAdapter;
import com.sportingbet.sportsbook.mybets.model.MyBetStatus;
import com.sportingbet.sportsbook.mybets.model.MyBetWrapper;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CashoutController implements MyBetsAdapter.CashOutButtonClickListener, RingProgressBarResponseListener.Delegate<CashoutBetsResponseType> {

    private final CashoutRequester cashoutRequester;
    private final RingProgressBarResponseListener<CashoutBetsResponseType> responseListener;

    private final BalanceUpdateController balanceUpdateController;

    private ProgressController progressController;
    private MyBetsAdapter myBetsAdapter;

    private MyBetWrapper myBetWrapper;
    private boolean isCashOutClick;

    public CashoutController(CashoutRequester cashoutRequester, BalanceUpdateController balanceUpdateController,
                             RingProgressBarResponseListener<CashoutBetsResponseType> responseListener) {
        this.cashoutRequester = cashoutRequester;
        this.cashoutRequester.setupResponseListener(responseListener);
        this.balanceUpdateController = balanceUpdateController;
        this.responseListener = responseListener;
        this.responseListener.setDelegate(this);
    }

    public void register(MyBetsAdapter myBetsAdapter, ProgressController progressController) {
        this.myBetsAdapter = myBetsAdapter;
        this.progressController = progressController;
    }

    @Override
    public void cashOutButtonClicked(MyBetWrapper myBetWrapper, View view) {
        if (!isCashOutClick) {
            this.myBetWrapper = myBetWrapper;
            isCashOutClick = true;
            cashoutRequester.prepareRequest(progressController, responseListener.getRingProgressBarController(), new ButtonEnabledProgressController(view));
            cashoutRequester.requestCashout(CashoutBetRequestType.create(CashoutBetsRequest.create(myBetWrapper.getBet())));
        }
    }

    @Override
    public void success(CashoutBetsResponseType object) {
        isCashOutClick = false;
        if (object.getCashoutBetsResponse().getFailedCashoutBets().size() == 0) {
            updateCasheOutItem(myBetWrapper);
            setupBalanceRefreshController();
        } else {
            handleFailedCahOutBets(object, myBetWrapper);
        }
    }

    private void setupBalanceRefreshController() {
        balanceUpdateController.requestBalanceUpdate(progressController);
    }

    private void handleFailedCahOutBets(CashoutBetsResponseType object, MyBetWrapper myBetWrapper) {
        for (CashoutBetsFailed cashoutBetsFailed : object.getCashoutBetsResponse().getFailedCashoutBets()) {
            handleCashOutError(cashoutBetsFailed.getError(), myBetWrapper);
        }
    }

    private void handleCashOutError(CashoutError object, MyBetWrapper myBetWrapper) {
        final int position = getMyBetPosition(myBetWrapper);
        MyBetWrapper cachOutItem = getCachOutItem(position);
        switch (object.getErrorCode()) {
            case BET00050:
                handlePriceChangeError(object, cachOutItem, position);
                break;
            case BET00051:
            case BET00052:
                handleOutComeUnavailableError(object, position, cachOutItem);
                break;
            default:
                handleAnyOtherErrors(object, position, cachOutItem);
                break;
        }
    }

    private void handlePriceChangeError(CashoutError object, MyBetWrapper myBetWrapper, int position) {
        myBetWrapper.getBet().setCashoutAmount(object.getNewCashoutAmount());
        myBetWrapper.setPriceChange(true);
        notifyCashOutError(object, position, myBetWrapper);
    }

    private void handleOutComeUnavailableError(CashoutError object, int position, MyBetWrapper cachOutItem) {
        cachOutItem.getBet().setCashoutAmount(null);
        notifyCashOutError(object, position, cachOutItem);
        prepareRemoveCashOutItem(cachOutItem);
    }

    private void handleAnyOtherErrors(CashoutError object, int position, MyBetWrapper cachOutItem) {
        notifyCashOutError(object, position, cachOutItem);
    }

    private void notifyCashOutError(CashoutError object, int position, MyBetWrapper myBetWrapper) {
        myBetWrapper.setHasError(true);
        myBetWrapper.setErrorMessage(object.getLocalisedErrorMessage());
        myBetsAdapter.notifyItemChanged(position);
    }

    private void updateCasheOutItem(MyBetWrapper myBetWrapper) {
        final int position = getMyBetPosition(myBetWrapper);
        MyBetWrapper cachOutItem = getCachOutItem(position);
        cachOutItem.getBet().setPotentialReturnAmount(cachOutItem.getBet().getCashoutAmount());
        cachOutItem.getBet().setCashoutAmount(null);
        if (!cachOutItem.isHasError()) {
            cachOutItem.getBet().setBetStatus(MyBetStatus.CASHEDOUT);
        }
        myBetsAdapter.notifyItemChanged(position);
        prepareRemoveCashOutItem(myBetWrapper);
    }

    private MyBetWrapper getCachOutItem(int position) {
        return ((MyBetWrapper) myBetsAdapter.getItems().get(position));
    }

    private int getMyBetPosition(MyBetWrapper myBetWrapper) {
        return myBetsAdapter.getItems().indexOf(myBetWrapper);
    }

    private void prepareRemoveCashOutItem(final MyBetWrapper myBetWrapper) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final int position = getMyBetPosition(myBetWrapper);
                if (position >= 0) {
                    removeCahOutItem(position);
                }
            }
        }, BuildConfig.CASHOUT_DELETE_PERIOD_MILLIS);
    }

    private void removeCahOutItem(int position) {
        myBetsAdapter.getItems().remove(position);
        myBetsAdapter.notifyItemRemoved(position);
    }

    private class ButtonEnabledProgressController extends ProgressControllerAdapter {

        private final View view;

        public ButtonEnabledProgressController(View view) {
            this.view = view;
        }

        @Override
        public void postExecuteWithSuccess(boolean success) {
            if (success) {
                view.setEnabled(false);
            }
        }
    }
}
