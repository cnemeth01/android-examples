package com.sportingbet.sportsbook.coupons.util;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.model.BookCoupon;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.price.Price;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CouponHelper {

    public static Coupon createPreviousBookCoupon(Event previousEvent) {
        if (previousEvent != null) {
            return new BookCoupon(previousEvent, previousEvent.getFirstBook());
        }
        return null;
    }

    public static void setupButton(CouponLayout button, final Event event, final Book book, final Selection selection, Selection previousSelection, boolean visible, final BetslipCouponDelegate delegate) {
        setupButtonWithPrice(button, selection.getPrice());
        setupCouponLayout(button, delegate.betslipContainsSelection(selection), delegate.loadingSelection(selection));
        setupButtonColour(button, selection, previousSelection);
        if (visible && selection.getPrice().isOffered()) {
            button.setEnabled(true);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delegate.onBetClicked(v, event, book, selection);
                }
            });
        } else {
            button.setEnabled(false);
        }
    }

    private static void setupButtonWithPrice(CouponLayout button, Price price) {
        if (price.isOffered()) {
            button.setText(price.getDisplayOdds());
        } else {
            button.setText(price.getNotOfferedReason());
        }
    }

    private static void setupButtonColour(CouponLayout button, Selection selection, Selection previousSelection) {
        if (previousSelection != null) {
            updateButtonScheme(button, selection.getOddsValue() - previousSelection.getOddsValue());
        } else {
            updateButtonScheme(button, 0.0);
        }
    }

    private static void updateButtonScheme(CouponLayout button, double difference) {
        if (difference > 0) {
            button.setColorResource(R.color.positive_color);
            button.setDrawableResource(0, R.drawable.betslip_stake_up_arrow, 0, R.drawable.betslip_stake_blank);
        } else if (difference < 0) {
            button.setColorResource(R.color.negative_color);
            button.setDrawableResource(0, R.drawable.betslip_stake_blank, 0, R.drawable.betslip_stake_down_arrow);
        } else {
            button.setColorStateResource(R.color.light_text_color);
            button.setDrawableResource(0, 0, 0, 0);
        }
    }

    public static void setupCouponLayout(CouponLayout button, boolean activated, boolean loading) {
        if (loading) {
            button.setBackgroundResource(R.drawable.coupon_button_highlighted);
            button.showProgressBar();
        } else {
            button.setBackgroundResource(R.drawable.coupon_button_selector);
            button.hideProgressBar();
            button.setActivated(activated);
        }
    }

    public static void setupCouponTieTexts(Coupon book, TextView firstTeam, TextView secondTeam) {
        firstTeam.setText(getSelectionWithTemplateNumber(book, 1).getName());
        secondTeam.setText(getSelectionWithTemplateNumber(book, 2).getName());
    }

    public static void setupCouponTexts(Coupon coupon, TextView firstTeam, TextView secondTeam) {
        firstTeam.setText(getFirstSelection(coupon).getName());
        secondTeam.setText(getSecondSelection(coupon).getName());
    }

    public static Selection getFirstSelection(Coupon coupon) {
        if (coupon == null) {
            return null;
        }
        return coupon.getSelections().get(0);
    }

    public static Selection getSecondSelection(Coupon coupon) {
        if (coupon == null) {
            return null;
        }
        return coupon.getSelections().get(1);
    }

    @NonNull
    public static Selection getSelectionWithTemplateNumber(Coupon coupon, int templateNumber) {
        for (Selection selection : coupon.getSelections()) {
            if (selection.getTemplateNumber() == templateNumber) {
                return selection;
            }
        }
        throw new RuntimeException(String.format("No selection with templateNumber: %d", templateNumber));
    }

    public static Selection getPreviousSelectionWithTemplateNumber(Coupon coupon, int templateNumber) {
        try {
            for (Selection selection : coupon.getSelections()) {
                if (selection.getTemplateNumber() == templateNumber) {
                    return selection;
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static void setupAlternativeTexts(Event event, TextView firstTeam, TextView secondTeam) {
        firstTeam.setText(event.getName());
        firstTeam.setSingleLine(false);
        firstTeam.setLines(2);

        secondTeam.setVisibility(View.GONE);
    }

    public static void setupSubtitle(TextView subtitleTextView, Event event) {
        String subtitle = SubtitleHelper.getSubtitle(event);
        subtitleTextView.setText(subtitle);
        setVisibilityForViews(subtitle.isEmpty() ? View.GONE : View.VISIBLE, subtitleTextView);
        setEnabledForViews(event.isVisible(), subtitleTextView);
    }

    public static void setupScoreTexts(Event event, TextView firstTeamScore, TextView secondTeamScore) {
        if (event.isRunning()) {
            firstTeamScore.setText(event.getScoreA());
            secondTeamScore.setText(event.getScoreB());
            setVisibilityForViews(View.VISIBLE, firstTeamScore, secondTeamScore);
            setEnabledForViews(event.isVisible(), firstTeamScore, secondTeamScore);
        } else {
            setVisibilityForViews(View.GONE, firstTeamScore, secondTeamScore);
        }
    }

    private static void setVisibilityForViews(int visibility, View... views) {
        for (View view : views) {
            view.setVisibility(visibility);
        }
    }

    private static void setEnabledForViews(boolean visible, View... views) {
        for (View view : views) {
            view.setEnabled(visible);
        }
    }
}
