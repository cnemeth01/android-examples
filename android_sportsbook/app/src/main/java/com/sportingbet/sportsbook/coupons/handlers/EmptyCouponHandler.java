package com.sportingbet.sportsbook.coupons.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.coupons.util.CouponHelper;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class EmptyCouponHandler implements EventCouponHandler<EmptyCouponHandler.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(Context context, ViewGroup parent) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.coupon_empty, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Event event, Event previousEvent) {
        viewHolder.title.setText(event.getName());
        CouponHelper.setupSubtitle(viewHolder.subtitle, event);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView title;
        private final TextView subtitle;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            subtitle = (TextView) view.findViewById(R.id.subtitle);
        }
    }
}
