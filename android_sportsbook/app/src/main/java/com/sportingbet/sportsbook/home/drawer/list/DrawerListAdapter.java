package com.sportingbet.sportsbook.home.drawer.list;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.adapter.AbstractListAdapter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class DrawerListAdapter extends AbstractListAdapter<DrawerItem> {

    public DrawerListAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.drawer_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(viewHolder);
        }
        initializeViews(getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    public int getItemPositionWithId(long itemId) {
        for (int position = 0; position < getCount(); position++) {
            if (getItemId(position) == itemId) {
                return position;
            }
        }
        return -1;
    }

    private void initializeViews(DrawerItem drawerItem, ViewHolder holder) {
        holder.title.setText(drawerItem.getTitle());
        holder.icon.setImageDrawable(getStateListDrawableFromDrawerItem(drawerItem));
    }

    private StateListDrawable getStateListDrawableFromDrawerItem(DrawerItem drawerItem) {
        StateListDrawable states = new StateListDrawable();
        if (drawerItem.hasIcon()) {
            states.addState(new int[]{},
                    context.getResources().getDrawable(drawerItem.getIcon()));
        }
        return states;
    }

    private class ViewHolder {
        public TextView title;
        public ImageView icon;
    }
}
