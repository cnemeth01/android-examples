package com.sportingbet.sportsbook.content.general.fragments;

import com.sportingbet.sportsbook.content.general.controllers.NavigationController;
import com.sportingbet.sportsbook.general.dagger.components.SportsbookFragment;
import com.sportingbet.sportsbook.home.navigation.fragments.SportNavigationFragment;
import com.sportingbet.sportsbook.home.pager.fragment.NavigationDelegate;
import com.sportingbet.sportsbook.model.Sport;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class ContentFragment extends SportsbookFragment implements NavigationController {

    protected NavigationDelegate getNavigationDelegate() {
        return (NavigationDelegate) getParentFragment();
    }

    protected Sport getSport() {
        return ((SportNavigationFragment) getParentFragment()).getSport();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onFragmentDisabled();
    }
}
