package com.sportingbet.sportsbook.home.account.webview;

import com.sportingbet.sportsbook.BuildConfig;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class WebURLFactory {

    public static String getRegistrationURL() {
        return getWebURLWithPath("registration");
    }

    public static String getMyAccountURL(String tempSessionId) {
        return getWebURLWithPath("myaccount", createTempSessionIdParam(tempSessionId));
    }

    public static String getDepositURL(String tempSessionId) {
        return getWebURLWithPath("deposit", createTempSessionIdParam(tempSessionId));
    }

    private static String createTempSessionIdParam(String tempSessionId) {
        return String.format("&TSID=%s", tempSessionId);
    }

    private static String getWebURLWithPath(String path) {
        return getWebURLWithPath(path, "");
    }

    private static String getWebURLWithPath(String path, String params) {
        return String.format("%s%s?external=iOS_casino%s", BuildConfig.WEB_SERVICE_URL, path, params);
    }
}
