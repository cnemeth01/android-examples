package com.sportingbet.sportsbook.betslip.progress;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.network.response.ProgressController;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public class DeleteProgressBarController implements ProgressController {

    private final View view;

    public DeleteProgressBarController(View view) {
        this.view = view;
    }

    @Override
    public void preExecute() {
        updateButtonVisibility(View.VISIBLE, R.anim.slide_in_delete_betslip);
    }

    @Override
    public void postExecuteWithSuccess(boolean success) {
        updateButtonVisibility(View.GONE, R.anim.abc_fade_out);
    }

    private void updateButtonVisibility(int visibility, int animId) {
        view.setAnimation(prepareAnimationWithId(animId));
        view.setVisibility(visibility);
    }

    private Animation prepareAnimationWithId(int animId) {
        return AnimationUtils.loadAnimation(view.getContext(), animId);
    }
}
