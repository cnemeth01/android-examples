package com.sportingbet.sportsbook.betslip.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.model.BetSelection;
import com.sportingbet.sportsbook.betslip.model.BetSelectionState;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.betslip.model.types.BetType;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetRequest;
import com.sportingbet.sportsbook.betslip.progress.DeleteProgressBarController;
import com.sportingbet.sportsbook.general.adapter.FooterRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class BetslipAdapter extends FooterRecyclerViewAdapter<BetSelection> implements BetTypeValueController.Delegate, AbstractViewHolderHandler.DataSource, View.OnClickListener {

    public interface Delegate {

        void setSwipeEnabled(boolean enabled);

        void onDeleteSelection(long selectionId, ProgressController progressController);

        void onPlaceBetWithDelay(PlaceBetRequest placeBetRequest, int placeBetDelay);

        BetsValueContainer getBetsValueContainer();
    }

    protected final Delegate delegate;

    @Getter
    protected BetslipInformation betslipInformation;

    protected abstract BetTypeValueController getBetslipValueController();

    protected BetslipAdapter(Context context, Delegate delegate) {
        super(context);
        this.delegate = delegate;
    }

    public void setBetslipInformation(BetslipInformation betslipInformation) {
        this.betslipInformation = betslipInformation;
        getBetslipValueController().setup(betslipInformation);
        setList(betslipInformation.getBetSelections());
    }

    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder(layoutInflater.inflate(R.layout.betslip_item, parent, false));
        viewHolder.getSwipeLayout().addSwipeListener(swipeListener);
        return viewHolder;
    }

    private SwipeLayout.SwipeListener swipeListener = new SimpleSwipeListener() {

        @Override
        public void onStartOpen(SwipeLayout layout) {
            delegate.setSwipeEnabled(false);
        }

        @Override
        public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
            delegate.setSwipeEnabled(true);
        }
    };

    @Override
    protected void onBindViewHolder(RecyclerView.ViewHolder viewHolder, BetSelection betSelection) {
        ViewHolder holder = (ViewHolder) viewHolder;
        setupSwipeLayout(holder, betSelection);
        BetType betType = betslipInformation.getBetTypeForSelectionId(betSelection.getSelectionId());
        if (betType.getErrorCode() == null) {
            setupViews(holder, betSelection.getAcceptedState(), true);
            setupOdds(holder, betSelection.getAcceptedState(), R.color.primaryDarkColor, 0);
            setupWarning(holder, R.color.backgroundColor, View.GONE);
        } else {
            switch (betType.getErrorCode()) {
                case BET00005:
                case BET00006:
                    setupViews(holder, betSelection.getAcceptedState(), false);
                    setupOdds(holder, betSelection.getAcceptedState(), R.color.textDisabled, 0);
                    break;
                case BETFT00014:
                case BETFT00027:
                    setupViews(holder, betSelection.getNewState(), true);
                    setupModifiedOdds(holder, betSelection.getNewState(), betSelection.getAcceptedState());
                    break;
                default:
                    setupViews(holder, betSelection.getAcceptedState(), true);
                    setupOdds(holder, betSelection.getAcceptedState(), R.color.primaryDarkColor, 0);
                    break;
            }
            setupSelectionTextColor(holder, betSelection);
            setupWarning(holder, R.color.warning_background, View.VISIBLE);
            setupWarningMessage(holder, betType);
        }
    }

    private void setupSwipeLayout(final ViewHolder holder, final BetSelection betSelection) {
        holder.deleteLayout.setVisibility(View.GONE);
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delegate.onDeleteSelection(betSelection.getSelectionId(), new DeleteProgressBarController(holder.deleteLayout));
            }
        });
        holder.getSwipeLayout().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                switch (holder.getSwipeLayout().getOpenStatus()) {
                    case Close:
                        holder.getSwipeLayout().open(true);
                        break;
                    case Open:
                        holder.getSwipeLayout().close(true);
                        break;
                }
                return true;
            }
        });
    }

    private void setupSelectionTextColor(ViewHolder holder, BetSelection betSelection) {
        holder.selection.setTextColor(context.getResources().getColorStateList(getSelectionColor(betSelection)));
    }

    private int getSelectionColor(BetSelection betSelection) {
        return betSelection.handicapUnchanged() ? R.color.text_color : R.color.negative_text_color;
    }

    protected void setupWarning(ViewHolder holder, int warningBackground, int visible) {
        holder.itemView.setBackgroundResource(warningBackground);
        holder.warningText.setVisibility(visible);
        holder.warningIndicator.setVisibility(visible);
    }

    protected void setupWarningMessage(ViewHolder holder, BetType betType) {
        holder.warningText.setText(betType.getLocalisedErrorMessage());
    }

    protected void setupViews(ViewHolder holder, BetSelectionState betSelectionState, boolean enabled) {
        holder.selection.setText(betSelectionState.getSelectionName());
        holder.selection.setEnabled(enabled);
        holder.book.setText(betSelectionState.getBookName());
        holder.book.setEnabled(enabled);
        holder.event.setText(betSelectionState.getEventName());
        holder.event.setEnabled(enabled);
    }

    private void setupModifiedOdds(ViewHolder holder, BetSelectionState newState, BetSelectionState acceptedState) {
        if (newState.hasBiggerOddsThan(acceptedState)) {
            setupOdds(holder, newState, R.color.positive_color, R.drawable.betslip_stake_up_arrow);
        } else if (acceptedState.hasBiggerOddsThan(newState)) {
            setupOdds(holder, newState, R.color.negative_color, R.drawable.betslip_stake_down_arrow);
        } else {
            setupOdds(holder, newState, R.color.primaryDarkColor, 0);
        }
    }

    private void setupOdds(ViewHolder holder, BetSelectionState betSelectionState, int colorId, int iconId) {
        holder.odds.setText(betSelectionState.getDisplayOdds());
        holder.odds.setTextColor(context.getResources().getColor(colorId));
        holder.odds.setCompoundDrawablesWithIntrinsicBounds(iconId, 0, 0, 0);
    }

    @Override
    public void notifyChange() {
        notifyFooterChanged();
    }

    public void onClick(View v) {
        delegate.onPlaceBetWithDelay(getBetslipValueController().getPlaceBetRequest(), getBetslipValueController().getPlaceBetDelay());
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView selection;
        private final TextView book;
        private final TextView event;
        private final TextView odds;
        public final View eachWay;
        public final TextView eachWayText;
        private final TextView warningText;
        private final View warningIndicator;
        public final StakePicker stakePicker;
        private final ImageView deleteButton;
        private final View deleteLayout;

        public ViewHolder(View view) {
            super(view);
            selection = (TextView) view.findViewById(R.id.selection);
            book = (TextView) view.findViewById(R.id.book);
            event = (TextView) view.findViewById(R.id.event);
            odds = (TextView) view.findViewById(R.id.odds);
            eachWay = view.findViewById(R.id.each_way);
            eachWayText = (TextView) view.findViewById(R.id.each_way_text);
            warningText = (TextView) view.findViewById(R.id.warning_text);
            warningIndicator = view.findViewById(R.id.warning_indicator);
            stakePicker = (StakePicker) view.findViewById(R.id.stake_picker);
            deleteButton = (ImageView) view.findViewById(R.id.delete_icon);
            deleteLayout = view.findViewById(R.id.delete_layout);
        }

        public SwipeLayout getSwipeLayout() {
            return (SwipeLayout) itemView;
        }
    }
}
