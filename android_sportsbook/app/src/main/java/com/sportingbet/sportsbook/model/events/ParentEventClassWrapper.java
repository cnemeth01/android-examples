package com.sportingbet.sportsbook.model.events;

import com.google.common.collect.Lists;

import java.util.List;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ParentEventClassWrapper {

    @Getter
    private List<EventClass> parentEventClasses = Lists.newArrayList();
}
