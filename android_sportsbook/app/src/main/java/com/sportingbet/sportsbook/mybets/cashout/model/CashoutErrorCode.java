package com.sportingbet.sportsbook.mybets.cashout.model;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public enum CashoutErrorCode {
    ERR00001, ERR00002, BET00050, BET00051, BET00052, BET00053, BETCASHOUTGEN;
}
