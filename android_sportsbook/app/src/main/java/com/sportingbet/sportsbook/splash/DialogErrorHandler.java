package com.sportingbet.sportsbook.splash;

import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.general.ui.dialog.WhiteDialog;

import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class DialogErrorHandler implements ErrorHandler {

    private final FragmentActivity activity;

    private View.OnClickListener onClickListener;

    private String title;
    private String buttonText1;
    private String buttonText2;

    public DialogErrorHandler(FragmentActivity activity) {
        this.activity = activity;
    }

    public void initialize(int title, int buttonText1, int buttonText2, View.OnClickListener onClickListener) {
        this.title = activity.getString(title);
        this.buttonText1 = activity.getString(buttonText1);
        this.buttonText2 = activity.getString(buttonText2);
        this.onClickListener = onClickListener;
    }

    @Override
    public boolean handleError(RetrofitError networkError) {
        WhiteDialog whiteDialog = WhiteDialog.newInstance(title, networkError.getLocalizedMessage(),
                buttonText1, buttonText2, onClickListener);
        whiteDialog.show(activity.getSupportFragmentManager(), null);
        return true;
    }
}
