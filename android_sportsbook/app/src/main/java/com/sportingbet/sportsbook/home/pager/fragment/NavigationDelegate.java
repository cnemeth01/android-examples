package com.sportingbet.sportsbook.home.pager.fragment;

import com.sportingbet.sportsbook.content.general.fragments.ContentFragment;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface NavigationDelegate {

    void switchFragment(ContentFragment fragment);

    void addFragment(ContentFragment fragment);

    void replaceFragment(ContentFragment fragment, ContentFragment newFragment);

    void notifyChildFragmentOnResume();

    void notifyChildFragmentOnPause();
}
