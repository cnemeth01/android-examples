package com.sportingbet.sportsbook.scoreboard.providers;

import android.content.Context;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class NotRunningScoreboardProvider extends AbstractScoreboardProvider {

    public NotRunningScoreboardProvider(Context context) {
        super(context);
    }

    @Override
    protected int getScoreboardLayout() {
        return R.layout.scoreboard_not_running;
    }

    @Override
    public void prepareScoreboard(View view, Event event) {
        getTextView(view, R.id.period_text).setText(event.getPeriodAndTime());
    }
}
