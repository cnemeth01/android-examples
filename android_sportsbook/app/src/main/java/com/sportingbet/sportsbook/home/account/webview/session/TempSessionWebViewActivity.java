package com.sportingbet.sportsbook.home.account.webview.session;

import android.os.Bundle;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.network.progress.ProgressBarController;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.home.account.webview.WebViewActivity;

import javax.inject.Inject;

import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class TempSessionWebViewActivity extends WebViewActivity implements NetworkResponseListener<String> {

    @Inject
    TempSessionRequester tempSessionRequester;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tempSessionRequester.getTempSessionId(this, new ProgressBarController(findViewById(R.id.progress_bar)));
    }

    protected abstract String getWebViewURL(String tempSessionId);

    @Override
    public void success(String object) {
        webView.loadUrl(getWebViewURL(object));
    }

    @Override
    public void failure(RetrofitError error) {
        errorHandler.handleError(error);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        tempSessionRequester.cancelRequest();
    }
}
