package com.sportingbet.sportsbook.betslip.placebets.model;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.betslip.model.BetSelection;

import java.util.List;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class PlaceBetResponse implements android.os.Parcelable {

    @Getter
    private BetResponses betResponses;

    public List<PlaceBet> getSuccessfulBets() {
        return betResponses.getSuccessfulBets();
    }

    public List<PlaceBet> getFailedBets() {
        return betResponses.getFailedBets();
    }

    public List<BetSelection> getSelections() {
        return betResponses.getSelections();
    }

    public String getTotalPotentialReturns() {
        return getBetResponses().getTotalPotentialReturns();
    }

    public boolean hasFailedBets() {
        return !getFailedBets().isEmpty();
    }

    public boolean hasAnySelectionsRemainedInBetSlip() {
        return getBetResponses().getNumberOfRemainingSelectionsInBetSlip() != 0;
    }

    public List<BetSelection> getSelectionsForPlaceBetType(PlaceBetType placeBetType) {
        List<BetSelection> betSelectionsForPlaceBet = Lists.newArrayList();
        for (BetSelection betSelection : getSelections()) {
            if (betIdContainsSelectionId(placeBetType, betSelection)) {
                betSelectionsForPlaceBet.add(betSelection);
            }
        }
        return betSelectionsForPlaceBet;
    }

    private boolean betIdContainsSelectionId(PlaceBetType placeBetType, BetSelection betSelection) {
        return placeBetType.getBetId().contains(String.valueOf(betSelection.getSelectionId()));
    }
}

@NoArgsConstructor
@Parcelable
class BetResponses {

    @Getter
    private List<PlaceBet> successfulBets;

    @Getter
    private List<PlaceBet> failedBets;

    @Getter
    private List<BetSelection> selections;

    @Getter
    private String totalPotentialReturns;

    @Getter
    private int numberOfRemainingSelectionsInBetSlip;
}
