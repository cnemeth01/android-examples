package com.sportingbet.sportsbook.content.fragments.event.books;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sportingbet.sportsbook.coupons.model.BookCoupon;
import com.sportingbet.sportsbook.model.book.Book;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BooksCouponAdapter {

    private static final int MAXIMUM_OPENED_BOOKS_COUNT = 5;

    private final LinkedHashMap<Book, List<Object>> openedBooks = Maps.newLinkedHashMap();

    public void putOpenedBooks(Book book, List<Object> objects) {
        openedBooks.put(book, objects);
    }

    public void clearRemovedBooks(Set<Book> newOpenedBooks) {
        for (Book book : Sets.newHashSet(openedBooks.keySet())) {
            if (!newOpenedBooks.contains(book)) {
                openedBooks.remove(book);
            }
        }
    }

    public void clearBooks() {
        openedBooks.clear();
    }

    public Set<Book> getBooks() {
        return openedBooks.keySet();
    }

    public boolean containsBook(Book object) {
        return openedBooks.containsKey(object);
    }

    public List<Object> removeBook(Book book) {
        return openedBooks.remove(book);
    }

    public void addBook(Book book) {
        openedBooks.put(book, null);
        if (openedBooks.size() > MAXIMUM_OPENED_BOOKS_COUNT) {
            openedBooks.remove(openedBooks.keySet().iterator().next());
        }
    }
}
