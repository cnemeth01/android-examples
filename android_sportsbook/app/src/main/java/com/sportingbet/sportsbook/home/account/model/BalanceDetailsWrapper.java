package com.sportingbet.sportsbook.home.account.model;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BalanceDetailsWrapper {

    @Getter
    private BalanceDetails balanceDetails;
}
