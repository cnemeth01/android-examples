package com.sportingbet.sportsbook.betslip;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.adapter.BetslipAdapter;
import com.sportingbet.sportsbook.betslip.adapter.multiples.MultiplesAdapter;
import com.sportingbet.sportsbook.betslip.adapter.single.SingleAdapter;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.general.helpers.empty.DefaultEmptyScreenController;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipTabController implements View.OnClickListener {

    private final Activity activity;

    private final DefaultEmptyScreenController emptyTextHelper;

    private TextView singleButton;
    private TextView multiplesButton;

    private RecyclerView recyclerView;

    private BetslipAdapter betslipAdapter;

    public BetslipTabController(Activity activity, RecyclerView recyclerView) {
        this.activity = activity;
        this.recyclerView = recyclerView;
        emptyTextHelper = new DefaultEmptyScreenController(activity.getWindow().getDecorView());

        singleButton = (TextView) activity.findViewById(R.id.first_tab);
        singleButton.setText(R.string.betslip_single);
        singleButton.setOnClickListener(this);
        multiplesButton = (TextView) activity.findViewById(R.id.second_tab);
        multiplesButton.setText(R.string.betslip_multiples);
        multiplesButton.setOnClickListener(this);
    }

    public void setBetslipInformation(BetslipInformation betslipInformation) {
        if (!betslipInformation.isEmpty()) {
            emptyTextHelper.hideEmptyView();
            boolean hasMultiples = betslipInformation.hasMultiples();
            if (noAdapter() || adapterShouldChange(hasMultiples)) {
                prepareAdapter(hasMultiples);
            }
            betslipAdapter.setBetslipInformation(betslipInformation);
            setupMultiplesButton(hasMultiples);
        } else {
            emptyTextHelper.showEmptyView();
        }
    }

    protected boolean noAdapter() {
        return betslipAdapter == null;
    }

    protected boolean adapterShouldChange(boolean hasMultiples) {
        return multiplesHasBecomeAvailable(hasMultiples) || multiplesHasBecomeUnavailable(hasMultiples);
    }

    private boolean multiplesHasBecomeAvailable(boolean hasMultiples) {
        return hasMultiples && !multiplesButton.isEnabled();
    }

    private boolean multiplesHasBecomeUnavailable(boolean hasMultiples) {
        return !hasMultiples && multiplesButton.isEnabled();
    }

    protected void prepareAdapter(boolean multiples) {
        betslipAdapter = createAdapter(multiples);
        recyclerView.setAdapter(betslipAdapter);
        setupButtons(multiples);
    }

    private BetslipAdapter createAdapter(boolean multiples) {
        if (multiples) {
            return new MultiplesAdapter(activity, (BetslipAdapter.Delegate) activity);
        }
        return new SingleAdapter(activity, (BetslipAdapter.Delegate) activity);
    }

    private void setupButtons(boolean multiples) {
        setupView(singleButton, !multiples);
        setupView(multiplesButton, multiples);
    }

    private void setupView(View view, boolean selected) {
        view.setActivated(selected);
        view.setEnabled(true);
    }

    private void setupMultiplesButton(boolean hasMultiples) {
        multiplesButton.setEnabled(hasMultiples);
    }

    @Override
    public void onClick(View view) {
        if (!view.isActivated()) {
            BetslipInformation betslipInformation = betslipAdapter.getBetslipInformation();
            switch (view.getId()) {
                case R.id.first_tab:
                    prepareAdapter(false);
                    break;
                case R.id.second_tab:
                    prepareAdapter(true);
                    break;
            }
            betslipAdapter.setBetslipInformation(betslipInformation);
        }
    }
}
