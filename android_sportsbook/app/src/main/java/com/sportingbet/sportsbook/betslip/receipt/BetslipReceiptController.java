package com.sportingbet.sportsbook.betslip.receipt;

import android.app.Activity;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.controller.BetslipBalanceController;
import com.sportingbet.sportsbook.betslip.controller.BetslipController;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBet;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetResponse;
import com.sportingbet.sportsbook.general.BalanceUpdateController;
import com.sportingbet.sportsbook.general.network.progress.ProgressBarController;
import com.sportingbet.sportsbook.general.network.progress.ProgressControllerAdapter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipReceiptController {

    private final BetslipBalanceController betslipBalanceController;
    private final BalanceUpdateController balanceUpdateController;

    private final BetslipController betslipController;

    public BetslipReceiptController(BetslipController betslipController, BetslipBalanceController betslipBalanceController, BalanceUpdateController balanceUpdateController) {
        this.betslipController = betslipController;
        this.betslipBalanceController = betslipBalanceController;
        this.balanceUpdateController = balanceUpdateController;
    }

    public void registerActivity(Activity activity, PlaceBetResponse placeBetResponse) {
        balanceUpdateController.requestBalanceUpdate(new ProgressBarController(activity.findViewById(R.id.progress_bar)), new BalanceUpdateProgressController());

        betslipBalanceController.updateLayout();

        updateResult(placeBetResponse);
    }

    private void updateResult(PlaceBetResponse placeBetResponse) {
        if (!placeBetResponse.hasAnySelectionsRemainedInBetSlip()) {
            betslipController.getBetsValueContainer().clearBetValues();
            betslipController.clearBets();
        } else {
            clearBetsValueContainerWithSuccessfulBets(placeBetResponse);
        }
    }

    private void clearBetsValueContainerWithSuccessfulBets(PlaceBetResponse placeBetResponse) {
        for (PlaceBet placeBet : placeBetResponse.getSuccessfulBets()) {
            betslipController.getBetsValueContainer().clearBetValue(placeBet.getBetType());
        }
    }

    private class BalanceUpdateProgressController extends ProgressControllerAdapter {

        @Override
        public void postExecuteWithSuccess(boolean success) {
            if (success) {
                betslipBalanceController.updateLayout();
            }
        }
    }
}
