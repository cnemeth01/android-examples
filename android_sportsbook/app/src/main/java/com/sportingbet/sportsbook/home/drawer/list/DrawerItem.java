package com.sportingbet.sportsbook.home.drawer.list;


import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class DrawerItem {

    @Getter
    private final long id;

    @Getter
    private final String title;

    @Getter
    private final int icon;

    public DrawerItem(long id, String title, int icon) {
        this.id = id;
        this.title = title;
        this.icon = icon;
    }

    public boolean hasIcon(){
        return icon != 0;
    }
}
