package com.sportingbet.sportsbook.home.navigation;

import android.content.Context;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.home.navigation.fragments.HomeNavigationFragment;
import com.sportingbet.sportsbook.home.navigation.fragments.InPlayNavigationFragment;
import com.sportingbet.sportsbook.home.navigation.fragments.SportNavigationFragment;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class NavigationItemFactoryImpl implements NavigationItemFactory {

    private final Context context;

    public NavigationItemFactoryImpl(Context context) {
        this.context = context;
    }

    public NavigationItem getNavigationItemForId(int itemId) {
        switch (itemId) {
            case BuildConfig.INPLAY_ITEM_ID:
                return new NavigationItem(BuildConfig.INPLAY_ITEM_ID, context.getString(R.string.in_play_item));
            case BuildConfig.HOME_ITEM_ID:
                return new NavigationItem(BuildConfig.HOME_ITEM_ID, context.getString(R.string.home_item));
            default:
                throw new RuntimeException(String.format("No item with id: '%d'.", itemId));
        }
    }

    @Override
    public AbstractNavigationFragment createFragmentForNavigationItem(NavigationItem navigationItem) {
        return AbstractNavigationFragment.prepareInstance(getNavigationFragment(navigationItem), navigationItem);
    }

    private AbstractNavigationFragment getNavigationFragment(NavigationItem navigationItem) {
        switch ((int) navigationItem.getId()) {
            case BuildConfig.INPLAY_ITEM_ID:
                return new InPlayNavigationFragment();
            case BuildConfig.HOME_ITEM_ID:
                return new HomeNavigationFragment();
            default:
                return new SportNavigationFragment();
        }
    }
}
