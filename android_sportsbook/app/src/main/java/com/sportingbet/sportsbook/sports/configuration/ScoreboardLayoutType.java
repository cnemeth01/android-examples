package com.sportingbet.sportsbook.sports.configuration;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@JsonDeserialize(using = ScoreboardLayoutTypeDeserializer.class)
@JsonSerialize(using = ScoreboardLayoutTypeSerializer.class)
public enum ScoreboardLayoutType {
    ScoreboardLayoutTypeNoScore,
    ScoreboardLayoutTypeTennis,
    ScoreboardLayoutTypeFootball,
    ScoreboardLayoutTypeVolleyball,
}

class ScoreboardLayoutTypeDeserializer extends JsonDeserializer<ScoreboardLayoutType> {

    @Override
    public ScoreboardLayoutType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        switch (p.getIntValue()) {
            case 1:
                return ScoreboardLayoutType.ScoreboardLayoutTypeTennis;
            case 2:
                return ScoreboardLayoutType.ScoreboardLayoutTypeFootball;
            case 3:
                return ScoreboardLayoutType.ScoreboardLayoutTypeVolleyball;
            default:
                return ScoreboardLayoutType.ScoreboardLayoutTypeNoScore;
        }
    }
}

class ScoreboardLayoutTypeSerializer extends JsonSerializer<ScoreboardLayoutType> {

    @Override
    public void serialize(ScoreboardLayoutType value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(value.ordinal());
    }
}
