package com.sportingbet.sportsbook.home.account.model;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class LoginRequestBody {

    @Getter
    private String userName;

    @Getter
    private String password;

    public LoginRequestBody(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }
}
