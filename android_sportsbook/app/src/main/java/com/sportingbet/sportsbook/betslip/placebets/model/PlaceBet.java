package com.sportingbet.sportsbook.betslip.placebets.model;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class PlaceBet {

    @Getter
    private PlaceBetType betType;

    @Getter
    private PlaceBetError error;

    @Getter
    private Long betReferenceId;
}
