package com.sportingbet.sportsbook.general.adapter.handlers.history;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.general.adapter.handlers.margin.MarginHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.refresh.HandlerTask;

import java.util.List;
import java.util.Timer;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class HistoryHandlersRecyclerViewAdapter<T> extends MarginHandlersRecyclerViewAdapter<T> implements Runnable {

    @Getter
    private List<Object> previousList = Lists.newArrayList();

    private final Timer timer = new Timer();

    private HandlerTask task;

    public HistoryHandlersRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public void setList(List<Object> list) {
        previousList = Lists.newArrayList(getItems());
        restartHandlerTask();
        super.setList(list);
    }

    private void restartHandlerTask() {
        if (task != null) {
            task.cancel();
        }
        task = new HandlerTask(this);
        timer.schedule(task, BuildConfig.HISTORY_PERIOD_MILLIS);
    }

    protected final void onBindTypeViewHolder(RecyclerView.ViewHolder holder, T item, int position) {
        onBindTypeViewHolder(holder, item, getPreviousItem(item), position);
    }

    @SuppressWarnings("unchecked")
    protected T getPreviousItem(T item) {
        for (Object previousItem : previousList) {
            if (item.equals(previousItem)) {
                return (T) previousItem;
            }
        }
        return null;
    }

    @Override
    public void run() {
        previousList = Lists.newArrayList();
        notifyDataSetChanged();
    }

    protected abstract void onBindTypeViewHolder(RecyclerView.ViewHolder holder, T item, T previousItem, int position);
}
