package com.sportingbet.sportsbook.home.navigation.configuration;

import com.sportingbet.sportsbook.home.navigation.NavigationItem;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;
import com.sportingbet.sportsbook.model.Sport;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface NavigationConfiguration {

    List<? extends NavigationItem> getListItems();

    List<Sport> getInPlaySports();

    List<Sport> getHomeSports();

    List<NavigationItem> getMenuItems();

    List<AbstractNavigationFragment> createNavigationFragments();

    int getNavigationFragmentsCount();

    AbstractNavigationFragment createSportNavigationFragment(long itemId);
}
