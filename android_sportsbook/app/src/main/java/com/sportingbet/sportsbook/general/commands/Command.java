package com.sportingbet.sportsbook.general.commands;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface Command<E> {

    void setDelegate(CommandProgressDelegate<E> delegate);

    void execute();

    void cancel();
}
