package com.sportingbet.sportsbook.general.ui.picker;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.helpers.DecimalDigitsInputFilter;

import java.text.DecimalFormat;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class StakePicker extends LinearLayout implements View.OnFocusChangeListener, TextWatcher {

    private static final DecimalFormat STAKE_FORMAT = new DecimalFormat("#.##");

    private static final InputFilter inputFilter = new DecimalDigitsInputFilter(BuildConfig.FILTER_INTEGER_DIGITS_NUMBER, BuildConfig.FILTER_FRACTION_DIGITS_NUMBER);

    public interface StakePickerListener {

        void onValueChanged(Double value);
    }

    @Setter
    private StakePickerListener stakePickerListener;

    @Getter
    private EditText stakeEditText;

    @Getter
    private View buttonMinus;

    @Getter
    private View buttonPlus;

    public StakePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(getContext()).inflate(getLayout(), this);

        stakeEditText = (EditText) findViewById(R.id.stake_text);
        stakeEditText.setOnFocusChangeListener(this);
        stakeEditText.setFilters(new InputFilter[]{inputFilter});
        buttonMinus = findViewById(R.id.stake_button_minus);
        buttonPlus = findViewById(R.id.stake_button_plus);
    }

    protected int getLayout() {
        return getOrientation() == LinearLayout.VERTICAL ? R.layout.stake_picker : R.layout.stake_picker_horizontal;
    }

    public void updateValue(Double value) {
        stakeEditText.setText("");
        stakeEditText.append(formatValue(value));
    }

    protected String formatValue(Double value) {
        if (value == null) {
            return "";
        }
        return STAKE_FORMAT.format(value);
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus) {
            stakeEditText.addTextChangedListener(this);
        } else {
            stakeEditText.removeTextChangedListener(this);
            hideSoftInput(view);
        }
    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        stakePickerListener.onValueChanged(getValue(text));
    }

    private Double getValue(CharSequence text) {
        try {
            return Double.valueOf(text.toString());
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public void hideSoftInput(View view) {
        InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
