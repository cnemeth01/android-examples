package com.sportingbet.sportsbook.betslip.commands;

import com.sportingbet.sportsbook.betslip.controller.BetslipController;
import com.sportingbet.sportsbook.model.book.selection.Selection;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class RemoveCommand extends BetslipCommand {

    private final Selection selection;

    public RemoveCommand(BetslipController betslipController, Selection selection) {
        super(betslipController);
        this.selection = selection;
    }

    @Override
    public void execute() {
        betslipController.removeBet(selection, this, this);
    }
}
