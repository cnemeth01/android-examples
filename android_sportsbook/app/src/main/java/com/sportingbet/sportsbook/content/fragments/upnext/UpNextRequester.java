package com.sportingbet.sportsbook.content.fragments.upnext;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.content.general.requesters.PublishingRequester;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.model.event.DetailsWrapper;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class UpNextRequester extends PublishingRequester<DetailsWrapper> {

    public UpNextRequester(PublishingService publishingService) {
        super(publishingService);
    }

    public void requestUpNextForSport(long sportId) {
        publishingService.getUpNext(sportId, BuildConfig.UPNEXT_NUMBER_OF_EVENTS, getCallback());
    }
}
