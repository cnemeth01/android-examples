package com.sportingbet.sportsbook.betslip.receipt.adapter.model;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipReceiptHeader {

    @Getter
    private int message;

    @Getter
    private boolean hasFailedBets;

    public BetslipReceiptHeader(int message, boolean hasFailedBets) {
        this.message = message;
        this.hasFailedBets = hasFailedBets;
    }
}
