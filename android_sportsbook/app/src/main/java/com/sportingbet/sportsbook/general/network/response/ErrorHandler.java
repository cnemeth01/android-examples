package com.sportingbet.sportsbook.general.network.response;

import retrofit.RetrofitError;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public interface ErrorHandler {

    boolean handleError(RetrofitError networkError);
}
