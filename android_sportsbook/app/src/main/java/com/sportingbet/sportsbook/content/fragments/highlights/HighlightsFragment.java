package com.sportingbet.sportsbook.content.fragments.highlights;

import com.sportingbet.sportsbook.content.general.fragments.events.EventsContentFragment;
import com.sportingbet.sportsbook.model.event.DetailsWrapper;

import javax.inject.Inject;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HighlightsFragment extends EventsContentFragment<DetailsWrapper> {

    @Inject
    @Getter
    HighlightsRequester networkRequester;

    public void onRefresh() {
        super.onRefresh();
        networkRequester.requestHighlightsForSport(getSport().getId());
    }
}
