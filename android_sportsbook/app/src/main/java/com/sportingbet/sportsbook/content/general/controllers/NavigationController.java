package com.sportingbet.sportsbook.content.general.controllers;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface NavigationController {

    void onFragmentSelected();

    void onFragmentDisabled();
}
