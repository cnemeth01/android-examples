package com.sportingbet.sportsbook.model.event;

import java.util.List;

public interface EventsWrapper {

    List<Event> getEvents();
}
