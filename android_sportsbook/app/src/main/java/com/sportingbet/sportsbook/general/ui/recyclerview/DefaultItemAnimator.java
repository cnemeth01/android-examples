package com.sportingbet.sportsbook.general.ui.recyclerview;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class DefaultItemAnimator extends android.support.v7.widget.DefaultItemAnimator{

    @Override
    public boolean animateAdd(RecyclerView.ViewHolder holder) {
        endAnimation(holder);
        return true;
    }
}
