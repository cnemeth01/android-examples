package com.sportingbet.sportsbook.mybets;

import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.mybets.model.MyBetType;
import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;

import retrofit.RetrofitError;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsFirstRequestController implements NetworkResponseListener<MyBetsResponse> {

    private boolean isSettledRequest = false;

    private final MyBetsRequester myBetsRequester;

    private final ProgressController progressController;

    private MyBetsFirstRequestControllerDelegate myBetsFirstRequestControllerDelegate;

    public MyBetsFirstRequestController(MyBetsRequester myBetsRequester, ProgressController progressController, MyBetsFirstRequestControllerDelegate myBetsFirstRequestControllerDelegate) {
        this.myBetsRequester = myBetsRequester;
        this.myBetsRequester.setupResponseListener(this);
        this.progressController = progressController;
        this.myBetsFirstRequestControllerDelegate = myBetsFirstRequestControllerDelegate;
        setupRequest(MyBetType.open);
    }

    private void setupRequest(MyBetType myBetType) {
        myBetsRequester.prepareRequest(progressController);
        myBetsRequester.requestMyBets(myBetType);
    }

    @Override
    public void success(MyBetsResponse object) {
        if (object.getBets().getBet().isEmpty()) {
            if (isSettledRequest) {
                myBetsFirstRequestControllerDelegate.controllerDidFinishWithResponse(MyBetType.open, object);
            } else {
                setupRequest(MyBetType.settled);
                isSettledRequest = true;
            }
        } else {
            if (isSettledRequest) {
                myBetsFirstRequestControllerDelegate.controllerDidFinishWithResponse(MyBetType.settled, object);
            } else {
                myBetsFirstRequestControllerDelegate.controllerDidFinishWithResponse(MyBetType.open, object);
            }
        }
    }

    @Override
    public void failure(RetrofitError retrofitError) {
        myBetsFirstRequestControllerDelegate.controllerDidFinishWithError(retrofitError);
    }

    public interface MyBetsFirstRequestControllerDelegate {

        void controllerDidFinishWithResponse(MyBetType myBetType, MyBetsResponse myBetsResponse);

        void controllerDidFinishWithError(RetrofitError retrofitError);
    }
}
