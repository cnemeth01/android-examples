package com.sportingbet.sportsbook.splash.commands;

import com.sportingbet.sportsbook.general.network.CallbackWrapper;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.services.ConfigService;
import com.sportingbet.sportsbook.sports.configuration.SportConfig;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportConfigRequester {

    protected final ConfigService configService;

    public SportConfigRequester(ConfigService configService) {
        this.configService = configService;
    }

    public void requestSportConfigs(NetworkResponseListener<List<SportConfig>> responseListener) {
        configService.getSportConfigs(new CallbackWrapper<>(responseListener));
    }
}
