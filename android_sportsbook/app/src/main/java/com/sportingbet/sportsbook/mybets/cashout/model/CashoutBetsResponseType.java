package com.sportingbet.sportsbook.mybets.cashout.model;

import lombok.Getter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CashoutBetsResponseType {

    @Getter
    public CashoutBetsResponse cashoutBetsResponse;
}
