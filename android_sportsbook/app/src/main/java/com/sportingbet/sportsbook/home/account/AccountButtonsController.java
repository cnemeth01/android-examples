package com.sportingbet.sportsbook.home.account;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.helpers.BalanceLayoutHelper;
import com.sportingbet.sportsbook.home.account.login.LoginActivity;
import com.sportingbet.sportsbook.home.account.model.LoginResponse;
import com.sportingbet.sportsbook.home.account.webview.activities.DepositActivity;
import com.sportingbet.sportsbook.home.account.webview.activities.MyAccountActivity;
import com.sportingbet.sportsbook.home.account.webview.activities.RegisterActivity;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class AccountButtonsController implements View.OnClickListener {

    private final Activity activity;

    public AccountButtonsController(Activity activity) {
        this.activity = activity;
    }

    public void updateButtons(LoginResponse loginResponse) {
        if (loginResponse == null) {
            setupButton(activity, R.id.login_button, R.id.open_account_button);
            setLayoutVisibility(R.id.not_logged_buttons, View.VISIBLE);
            setLayoutVisibility(R.id.logged_buttons, View.GONE);
            setLayoutVisibility(R.id.balance_layout, View.GONE);
        } else {
            setupButton(activity, R.id.deposit_button);
            setupAccountLayout(loginResponse);
            setupBalanceLayout(loginResponse);
            setLayoutVisibility(R.id.not_logged_buttons, View.GONE);
            setLayoutVisibility(R.id.logged_buttons, View.VISIBLE);
        }
    }

    private void setupAccountLayout(LoginResponse loginResponse) {
        View accountLayout = activity.findViewById(R.id.account_layout);
        accountLayout.setOnClickListener(this);
        BalanceLayoutHelper.setupCustomerDetails(accountLayout, loginResponse);
    }

    private void setupBalanceLayout(LoginResponse loginResponse) {
        BalanceLayoutHelper.setupBalanceLayout(activity, loginResponse);
    }

    private void setLayoutVisibility(int layoutId, int visibility) {
        activity.findViewById(layoutId).setVisibility(visibility);
    }

    private void setupButton(Activity activity, int... buttonsResourceId) {
        for (Integer buttonResourceId : buttonsResourceId) {
            activity.findViewById(buttonResourceId).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                activity.startActivityForResult(new Intent(activity, LoginActivity.class), 0);
                break;
            case R.id.open_account_button:
                activity.startActivity(new Intent(activity, RegisterActivity.class));
                break;
            case R.id.account_layout:
                activity.startActivityForResult(new Intent(activity, MyAccountActivity.class), 0);
                break;
            case R.id.deposit_button:
                activity.startActivity(new Intent(activity, DepositActivity.class));
                break;
        }
    }
}
