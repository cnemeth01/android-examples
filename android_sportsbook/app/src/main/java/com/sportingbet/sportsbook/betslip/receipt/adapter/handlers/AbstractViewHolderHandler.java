package com.sportingbet.sportsbook.betslip.receipt.adapter.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.general.adapter.handlers.ViewHolderHandler;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class AbstractViewHolderHandler<T, R extends RecyclerView.ViewHolder> implements ViewHolderHandler<T, R> {

    protected final Context context;

    public AbstractViewHolderHandler(Context context) {
        this.context = context;
    }

    protected View createView(ViewGroup parent, int resourceId) {
        return LayoutInflater.from(context).inflate(resourceId, parent, false);
    }
}
