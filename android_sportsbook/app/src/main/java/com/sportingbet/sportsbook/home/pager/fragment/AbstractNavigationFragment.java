package com.sportingbet.sportsbook.home.pager.fragment;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.FragmentManager;

import com.sportingbet.sportsbook.content.fragments.byevent.ByEventFragment;
import com.sportingbet.sportsbook.content.fragments.home.HomeFragment;
import com.sportingbet.sportsbook.content.general.controllers.NavigationController;
import com.sportingbet.sportsbook.content.general.fragments.ContentFragment;
import com.sportingbet.sportsbook.content.general.helpers.BundleHelper;
import com.sportingbet.sportsbook.general.dagger.components.SportsbookFragment;
import com.sportingbet.sportsbook.home.HomeActivity;
import com.sportingbet.sportsbook.home.navigation.FragmentContentController;
import com.sportingbet.sportsbook.home.navigation.NavigationItem;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class AbstractNavigationFragment<T extends NavigationItem> extends SportsbookFragment implements PageNavigation, NavigationDelegate, FragmentManager.OnBackStackChangedListener {

    private BundleHelper<T> bundleHelper;

    private FragmentContentController fragmentContentController;

    public final String getTitle() {
        return getNavigationItem().getName();
    }

    public final long getIdentifier() {
        return getNavigationItem().getId();
    }

    protected T getNavigationItem() {
        return bundleHelper.get();
    }

    public AbstractNavigationFragment() {
        bundleHelper = new BundleHelper<>(NavigationItem.class);
    }

    public static <T extends NavigationItem> AbstractNavigationFragment<T> prepareInstance(AbstractNavigationFragment<T> navigationFragment, T navigationItem) {
        navigationFragment.setArguments(navigationFragment.bundleHelper.createBundle(navigationItem));
        return navigationFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bundleHelper.onRestoreInstanceState(getArguments());
        fragmentContentController = new FragmentContentController(getChildFragmentManager());
        getChildFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getChildFragmentManager().removeOnBackStackChangedListener(this);
    }

    protected NavigationController getCurrentNavigationController() {
        return (NavigationController) fragmentContentController.getCurrentFragment();
    }

    @Override
    public void onPageSelected() {
        getCurrentNavigationController().onFragmentSelected();
    }

    @Override
    public void onPageDisabled() {
        getCurrentNavigationController().onFragmentDisabled();
    }

    @Override
    public void switchFragment(ContentFragment fragment) {
        fragmentContentController.switchFragment(fragment);
    }

    @Override
    public void addFragment(ContentFragment fragment) {
        getCurrentNavigationController().onFragmentDisabled();
        fragmentContentController.addFragment(fragment);
    }

    @Override
    public void replaceFragment(ContentFragment fragment, ContentFragment newFragment) {
        fragmentContentController.replaceFragment(fragment, newFragment);
    }

    @Override
    public void notifyChildFragmentOnResume() {
        if (isCurrentFragment()) {
            notifyFragmentSelected();
        }
    }

    @Override
    public void notifyChildFragmentOnPause() {
        if (isCurrentFragment()) {
            onPageDisabled();
        }
    }

    private boolean isCurrentFragment() {
        return ((HomeActivity) getActivity()).isCurrentFragment(this);
    }

    private void notifyFragmentSelected() {
        notifyFragmentSelected(getResources().getInteger(android.R.integer.config_shortAnimTime));
    }

    private void notifyFragmentSelected(int delay) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                onPageSelected();
            }
        }, delay);
    }

    @Override
    public void onBackStackChanged() {
        if (!isCurrentFragmentInstanceOf(ByEventFragment.class)) {
            notifyFragmentSelected(getResources().getInteger(android.R.integer.config_mediumAnimTime));
        }
    }

    private boolean isCurrentFragmentInstanceOf(Class... types) {
        for (Class type : types) {
            if (type.isInstance(fragmentContentController.getCurrentFragment())) {
                return true;
            }
        }
        return false;
    }
}
