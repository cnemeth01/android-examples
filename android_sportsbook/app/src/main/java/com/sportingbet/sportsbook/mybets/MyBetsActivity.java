package com.sportingbet.sportsbook.mybets;


import android.content.Intent;
import android.os.Bundle;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.activity.ToolbarActivity;
import com.sportingbet.sportsbook.general.network.error.ActivitySnackbarErrorHandler;
import com.sportingbet.sportsbook.general.network.progress.ProgressBarController;
import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.mybets.model.MyBetType;
import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;
import com.sportingbet.sportsbook.mybets.pager.MyBetsPagerAdapter;
import com.sportingbet.sportsbook.mybets.pager.MyBetsPagerController;

import javax.inject.Inject;

import retrofit.RetrofitError;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */

public class MyBetsActivity extends ToolbarActivity implements MyBetsFirstRequestController.MyBetsFirstRequestControllerDelegate {

    private final ErrorHandler errorHandler = new ActivitySnackbarErrorHandler(this);

    private MyBetsPagerController pagerController;

    @Inject
    MyBetsRequester myBetsRequester;

    @Override
    protected int getToolbarTitle() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_bottom, android.R.anim.fade_out);
        setContentView(R.layout.my_bets);

        setupPagerController();
        setupFirstRequestController();
    }

    private void setupPagerController() {
        pagerController = new MyBetsPagerController(new MyBetsPagerAdapter(this, getSupportFragmentManager()));
        pagerController.registerActivity(this);
    }

    private void setupFirstRequestController() {
        new MyBetsFirstRequestController(myBetsRequester, new ProgressBarController(findViewById(R.id.progress_bar)), this);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_out_bottom);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            finish();
        }
    }

    @Override
    public void controllerDidFinishWithResponse(MyBetType myBetType, MyBetsResponse myBetsResponse) {
        pagerController.selectMyBetType(myBetType, myBetsResponse);
    }

    @Override
    public void controllerDidFinishWithError(RetrofitError retrofitError) {
        errorHandler.handleError(retrofitError);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myBetsRequester.cancelRequest();
    }
}
