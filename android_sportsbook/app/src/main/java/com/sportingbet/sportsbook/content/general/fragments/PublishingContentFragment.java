package com.sportingbet.sportsbook.content.general.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sportingbet.sportsbook.content.general.requesters.PublishingRequester;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;

import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class PublishingContentFragment<T, E> extends ListContentFragment<E> implements NetworkResponseListener<T> {

    protected abstract PublishingRequester<T> getNetworkRequester();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getNetworkRequester().setupResponseListener(this);
    }

    @Override
    public void onFragmentDisabled() {
        super.onFragmentDisabled();
        getNetworkRequester().cancelRequest();
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        getNetworkRequester().prepareRequest(getProgressControllers());
    }

    protected ProgressController[] getProgressControllers() {
        return new ProgressController[]{refreshProgressController, progressBarController};
    }

    @Override
    public void failure(RetrofitError error) {
        errorHandler.handleError(error);
    }
}
