package com.sportingbet.sportsbook.model.event;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class EventsWrapperImpl implements EventsWrapper {

    @Getter
    private List<Event> events;
}
