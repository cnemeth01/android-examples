package com.sportingbet.sportsbook.betslip.model;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.betslip.model.types.BetType;
import com.sportingbet.sportsbook.betslip.model.types.BetTypes;

import java.util.List;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class BetslipInformation {

    @Getter
    private StakeInformation stakeInformation;

    @Getter
    private List<BetSelection> betSelections;

    @Getter
    private BetTypes betTypes;

    public boolean isEmpty() {
        return betSelections == null || betSelections.isEmpty();
    }

    public BetType getBetTypeForSelectionId(long selectionId) {
        for (BetType betType : betTypes.getBetGroups().getMultiplesBetGroup().getBetType()) {
            if (betType.getSelectionId() == selectionId) {
                return betType;
            }
        }
        return null;
    }

    public BetType getBetTypeForId(String betId) {
        for (BetType betType : betTypes.getBetGroups().getMultiplesBetGroup().getBetType()) {
            if (betId.equals(betType.getBetId())) {
                return betType;
            }
        }
        return null;
    }

    public BetSelection getBetSelectionForId(long selectionId) {
        for (BetSelection betSelection : getBetSelections()) {
            if (betSelection.getSelectionId() == selectionId) {
                return betSelection;
            }
        }
        return null;
    }

    public boolean hasMultiples() {
        for (BetType betType : betTypes.getBetGroups().getMultiplesBetGroup().getBetType()) {
            if (!betType.isSingle()) {
                return true;
            }
        }
        return false;
    }

    public List<BetType> getMultiples() {
        List<BetType> multiples = Lists.newArrayList();
        for (BetType betType : betTypes.getBetGroups().getMultiplesBetGroup().getBetType()) {
            if (!betType.isSingle()) {
                multiples.add(betType);
            }
        }
        return multiples;
    }

    public String getPaymentMark() {
        for (BetSelection betSelection : getBetSelections()) {
            if (betSelection.isSPSelection()) {
                return betSelection.getAcceptedState().getDisplayOdds();
            }
        }
        return null;
    }
}
