package com.sportingbet.sportsbook.scoreboard;

import android.view.View;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface ScoreboardProvider {

    View createScoreboard(ViewGroup parent, Event event);

    void prepareScoreboard(View view, Event event);
}
