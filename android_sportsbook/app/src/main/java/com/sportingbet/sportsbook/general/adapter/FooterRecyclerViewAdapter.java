package com.sportingbet.sportsbook.general.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import lombok.Setter;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public abstract class FooterRecyclerViewAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SECTIONS = 1;
    private static final int TYPE_FOOTER = 2;

    protected final Context context;

    protected final LayoutInflater layoutInflater;

    private final List<T> items = new ArrayList<>();

    protected ViewHolderHandler sectionsViewHolderHandler;

    protected ViewHolderHandler footerViewHolderHandler;

    protected FooterRecyclerViewAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    final protected T getItem(int position) {
        return items.get(position);
    }

    final public List<T> getItems() {
        return items;
    }

    public void setList(List<T> list) {
        items.clear();
        items.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.isEmpty() ? 0 : items.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position - items.size()) {
            case 0:
                return TYPE_SECTIONS;
            case 1:
                return TYPE_FOOTER;
            default:
                return TYPE_ITEM;
        }
    }

    public void notifyFooterChanged() {
        notifyItemChanged(items.size() + 1);
    }

    protected abstract RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent);

    protected abstract void onBindViewHolder(RecyclerView.ViewHolder holder, T object);

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_SECTIONS:
                return sectionsViewHolderHandler.onCreateViewHolder(parent);
            case TYPE_FOOTER:
                return footerViewHolderHandler.onCreateViewHolder(parent);
            default:
                return onCreateViewHolder(parent);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_SECTIONS:
                sectionsViewHolderHandler.onBindViewHolder(holder);
                break;
            case TYPE_FOOTER:
                footerViewHolderHandler.onBindViewHolder(holder);
                break;
            default:
                onBindViewHolder(holder, getItem(position));
                break;
        }
    }

    public interface ViewHolderHandler<T extends RecyclerView.ViewHolder> {

        T onCreateViewHolder(ViewGroup parent);

        void onBindViewHolder(T holder);
    }
}