package com.sportingbet.sportsbook.content.fragments.byevent.links;

import com.sportingbet.sportsbook.model.Sport;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class SportLink {


    @Getter
    private Sport sport;

    @Getter
    private int iconResource;

    public SportLink(Sport sport, int iconResource) {
        this.sport = sport;
        this.iconResource = iconResource;
    }
}
