package com.sportingbet.sportsbook.general.network.response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ContainerProgressController implements ProgressController {

    private final ProgressController[] progressControllers;

    public ContainerProgressController(ProgressController... progressControllers) {
        this.progressControllers = progressControllers;
    }

    @Override
    public void preExecute() {
        for (ProgressController progressController : progressControllers) {
            progressController.preExecute();
        }
    }

    @Override
    public void postExecuteWithSuccess(boolean success) {
        for (ProgressController progressController : progressControllers) {
            progressController.postExecuteWithSuccess(success);
        }
    }
}
