package com.sportingbet.sportsbook.betslip.placebets;

import android.app.Activity;
import android.content.Intent;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.BetslipActivity;
import com.sportingbet.sportsbook.betslip.model.BetSelection;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.betslip.model.types.BetType;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBet;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetError;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetRequest;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetResponse;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetType;
import com.sportingbet.sportsbook.general.LoginResponseStore;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.home.account.login.LoginActivity;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class PlaceBetController {

    private final Activity activity;

    private final LoginResponseStore loginResponseStore;

    private final PlaceBetRequester placeBetRequester;

    private ProgressController[] progressControllers;

    private PlaceBetRequest placeBetRequest;

    public PlaceBetController(Activity activity, LoginResponseStore loginResponseStore, PlaceBetRequester placeBetRequester) {
        this.activity = activity;
        this.loginResponseStore = loginResponseStore;
        this.placeBetRequester = placeBetRequester;

    }

    public void preparePlaceBet(NetworkResponseListener<PlaceBetResponse> responseListener, ProgressController... progressControllers) {
        this.placeBetRequester.setupResponseListener(responseListener);
        this.progressControllers = progressControllers;
    }

    public void placeBet(PlaceBetRequest placeBetRequest) {
        this.placeBetRequest = placeBetRequest;
        if (loginResponseStore.isLoggedIn()) {
            requestPlaceBet();
        } else {
            activity.startActivityForResult(createLoginIntent(), BetslipActivity.LOGIN_ACTIVITY_REQUEST_CODE);
        }
    }

    public void retryPlaceBet() {
        requestPlaceBet();
    }

    private void requestPlaceBet() {
        placeBetRequester.prepareRequest(progressControllers);
        placeBetRequester.requestPlaceBet(placeBetRequest);
    }

    private Intent createLoginIntent() {
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.putExtra(Intent.EXTRA_TITLE, R.string.login_place_bet_title);
        return intent;
    }

    public void updateBetslipInformation(BetslipInformation betslipInformation, PlaceBetResponse placeBetResponse) {
        updateSelections(betslipInformation, placeBetResponse);
        updateErrors(betslipInformation, placeBetResponse);
    }

    private void updateSelections(BetslipInformation betslipInformation, PlaceBetResponse placeBetResponse) {
        for (BetSelection placeBetSelection : placeBetResponse.getSelections()) {
            updateBetSelection(betslipInformation.getBetSelectionForId(placeBetSelection.getSelectionId()), placeBetSelection);
            clearBetTypeErrorInformation(betslipInformation.getBetTypeForSelectionId(placeBetSelection.getSelectionId()));
        }
    }

    private void clearBetTypeErrorInformation(BetType betType) {
        betType.setErrorCode(null);
        betType.setLocalisedErrorMessage(null);
    }

    private void updateBetSelection(BetSelection betSelection, BetSelection placeBetSelection) {
        betSelection.setAcceptedState(placeBetSelection.getAcceptedState());
        betSelection.setNewState(placeBetSelection.getNewState());
    }

    private void updateErrors(BetslipInformation betslipInformation, PlaceBetResponse placeBetResponse) {
        for (PlaceBet placeBet : placeBetResponse.getFailedBets()) {
            BetType placeBetType = placeBet.getBetType();
            BetType betslipBetType = betslipInformation.getBetTypeForId(placeBetType.getBetId());
            updateBetType(betslipBetType, placeBet.getBetType());
            updateBetTypeErrorInformation(betslipBetType, placeBet.getError());
        }
    }

    private void updateBetType(BetType betslipBetType, PlaceBetType updatedBetType) {
        betslipBetType.setStakeMultiplier(updatedBetType.getStakeMultiplier());
        betslipBetType.setNumberOfBets(updatedBetType.getNumberOfBets());
    }

    private void updateBetTypeErrorInformation(BetType betslipBetType, PlaceBetError placeBetError) {
        betslipBetType.setErrorCode(placeBetError.getErrorCode());
        betslipBetType.setLocalisedErrorMessage(placeBetError.getLocalisedErrorMessage());
    }
}
