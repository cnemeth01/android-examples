package com.sportingbet.sportsbook.model.event;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.WebCouponTemplate;

import java.util.List;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class Event implements android.os.Parcelable {

    @Getter
    private long id;

    @Getter
    private String name;

    @Getter
    private long sportId;

    @Getter
    private int isVisible;

    @Getter
    private String periodAndTime;

    @Getter
    private InRunningData inRunningData;

    @Getter
    private List<Book> books = Lists.newArrayList();

    public Book getFirstBook() {
        if (books != null && books.size() > 0) {
            return books.get(0);
        }
        return null;
    }

    public WebCouponTemplate getWebCouponTemplate() {
        // TODO refactor (make sure that 'if statement' is necessary)
        Book firstBook = getFirstBook();
        if (firstBook != null) {
            return firstBook.getWebCouponTemplate();
        }
        return WebCouponTemplate.UNKNOWN;
    }

    public String getScoreA() {
        return inRunningData.getScoreA();
    }

    public String getScoreB() {
        return inRunningData.getScoreB();
    }

    public boolean isRunning() {
        return inRunningData != null && inRunningData.isRunning() && inRunningData.getScores() != null;
    }

    public boolean isVisible() {
        return isVisible == 1;
    }

    public int getSelectionCount() {
        Book firstBook = getFirstBook();
        if (firstBook != null) {
            return firstBook.getFirstOffer().getSelectionCount();
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        return getClass() == o.getClass() && Objects.equal(id, ((Event) o).getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
