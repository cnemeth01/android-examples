package com.sportingbet.sportsbook.content.fragments.byevent.events;

import com.sportingbet.sportsbook.content.general.requesters.PublishingRequester;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.model.event.EventsWrapperImpl;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class EventsRequester extends PublishingRequester<EventsWrapperImpl> {

    public EventsRequester(PublishingService publishingService) {
        super(publishingService);
    }

    public void requestEventsFromEventClass(long eventClass) {
        publishingService.getEvents(eventClass, getCallback());
    }
}
