package com.sportingbet.sportsbook.betslip.model;

import com.sportingbet.sportsbook.coupons.betslip.model.BetslipSelection;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class BetSelectionState extends BetslipSelection {

    @Getter
    private String displayOdds;

    public boolean hasBiggerOddsThan(BetSelectionState betSelectionState) {
        return getOdds() > betSelectionState.getOdds();
    }

    public double getOdds() {
        if (getDecimalOdds() != null) {
            return Double.valueOf(getDecimalOdds());
        }
        return 0.0;
    }
}
