package com.sportingbet.sportsbook.coupons.model;

import com.google.common.base.Objects;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.event.Event;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@NoArgsConstructor
public abstract class BaseCoupon implements Coupon {

    @Getter
    private Event event;

    @Getter
    private Book book;

    public BaseCoupon(Event event, Book book) {
        this.event = event;
        this.book = book;
    }

    @Override
    public boolean isVisible() {
        return event.isVisible() && book.isVisible();
    }

    @Override
    public boolean equals(Object o) {
        return getClass() == o.getClass() && Objects.equal(getSelections(), ((BaseCoupon) o).getSelections());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getSelections().toArray());
    }
}
