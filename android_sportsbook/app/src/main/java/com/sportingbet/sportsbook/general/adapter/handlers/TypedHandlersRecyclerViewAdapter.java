package com.sportingbet.sportsbook.general.adapter.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.general.adapter.AbstractRecyclerViewAdapter;

import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class TypedHandlersRecyclerViewAdapter<T> extends AbstractRecyclerViewAdapter<Object, RecyclerView.ViewHolder> {

    private Map<Integer, ViewHolderHandler> typesViewHolderHandlers = Maps.newHashMap();

    public TypedHandlersRecyclerViewAdapter(Context context) {
        super(context);
    }

    public void addViewHolderHandler(Class type, ViewHolderHandler viewHolderHandler) {
        typesViewHolderHandlers.put(type.hashCode(), viewHolderHandler);
    }

    @Override
    public final int getItemViewType(int position) {
        Object item = getItem(position);
        if (isSpecialItem(item)) {
            return getSpecialItemType(item);
        }
        return getTypeItemViewType((T) item);
    }

    public int getSpecialItemType(Object item) {
        return item.getClass().hashCode();
    }

    private boolean isSpecialItem(Object item) {
        return typesViewHolderHandlers.containsKey(item.getClass().hashCode());
    }

    @Override
    public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (typesViewHolderHandlers.containsKey(viewType)) {
            return typesViewHolderHandlers.get(viewType).onCreateViewHolder(parent);
        }
        return onCreateTypeViewHolder(parent, viewType);
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object item = getItem(position);
        if (isSpecialItem(item)) {
            typesViewHolderHandlers.get(getSpecialItemType(item)).onBindViewHolder(holder, item);
        } else {
            onBindTypeViewHolder(holder, (T) item, position);
        }
    }

    protected abstract int getTypeItemViewType(T item);

    protected abstract RecyclerView.ViewHolder onCreateTypeViewHolder(ViewGroup parent, int viewType);

    protected abstract void onBindTypeViewHolder(RecyclerView.ViewHolder holder, T item, int position);
}
