package com.sportingbet.sportsbook.sports.configuration;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sportingbet.sportsbook.model.book.WebCouponTemplate;

import java.io.IOException;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@JsonDeserialize(using = CouponTypeDeserializer.class)
@JsonSerialize(using = CouponTypeSerializer.class)
public enum CouponType {
    CouponTypeEmpty(0),
    CouponTypeSingle(1),
    CouponTypeNoTie(2),
    CouponTypeTie(3),
    CouponTypeOverUnder(2),
    CouponTypeTieAlternative(1),
    CouponTypeNoTieAlternative(1),
    CouponTypeThreeBall(3);

    private final int selectionCount;

    CouponType(int selectionCount) {
        this.selectionCount = selectionCount;
    }

    public int value() {
        return ordinal();
    }

    public int getSelectionCount() {
        return selectionCount;
    }

    public static int getItemTypeForTemplate(WebCouponTemplate webCouponTemplate) {
        return getCouponTypeForTemplate(webCouponTemplate).value();
    }

    public static CouponType getCouponTypeForTemplate(WebCouponTemplate webCouponTemplate) {
        switch (webCouponTemplate) {
            case CPNTEAMTIE:
            case CPNPLAYTIE:
            case CPN2BALLS:
                return CouponType.CouponTypeTie;
            case CPNTEAMNOTIE:
            case CPNPLAYNOTIE:
                return CouponType.CouponTypeNoTie;
            case CPNTOTALS:
            case CPNOVERUNDER:
                return CouponType.CouponTypeOverUnder;
            case CPN3BALLS:
                return CouponType.CouponTypeThreeBall;
            default:
                return CouponType.CouponTypeEmpty;
        }
    }
}

class CouponTypeDeserializer extends JsonDeserializer<CouponType> {

    @Override
    public CouponType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        switch (p.getIntValue()) {
            case 1:
                return CouponType.CouponTypeSingle;
            case 2:
                return CouponType.CouponTypeNoTie;
            case 3:
                return CouponType.CouponTypeTie;
            default:
                return CouponType.CouponTypeEmpty;
        }
    }
}

class CouponTypeSerializer extends JsonSerializer<CouponType> {

    @Override
    public void serialize(CouponType value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(value.value());
    }
}


