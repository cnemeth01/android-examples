package com.sportingbet.sportsbook.content.general.controllers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.sports.configuration.CouponType;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class AbstractHeaderController {

    private Context context;

    public AbstractHeaderController(Context context) {
        this.context = context;
    }

    public void setup(ViewGroup headerLayout, CouponType defaultCouponType) {
        headerLayout.removeAllViews();
    }

    protected void inflateResourceInHeader(ViewGroup headerLayout, int resource) {
        LayoutInflater.from(context).inflate(resource, headerLayout);
    }
}
