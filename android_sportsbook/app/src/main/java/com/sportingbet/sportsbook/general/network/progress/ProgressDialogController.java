package com.sportingbet.sportsbook.general.network.progress;

import android.app.Activity;

import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.ui.dialog.ScreenProgressDialog;

/**
 * Copyright 2014 Tomasz Morcinek. All rights reserved.
 */
public class ProgressDialogController implements ProgressController {

    private Activity activity;

    private ScreenProgressDialog progressDialog;

    public ProgressDialogController(Activity activity) {
        this.activity = activity;
        progressDialog = new ScreenProgressDialog(activity);
    }

    @Override
    public void preExecute() {
        progressDialog.show();
    }

    @Override
    public void postExecuteWithSuccess(boolean success) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.cancel();
            }
        });
    }
}