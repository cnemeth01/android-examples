package com.sportingbet.sportsbook.mybets.model;

import java.util.List;

import lombok.Getter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetSelections {

    @Getter
    private List<MyBetSelection> selection;

}
