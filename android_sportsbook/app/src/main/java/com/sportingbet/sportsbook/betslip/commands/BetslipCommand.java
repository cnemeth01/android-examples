package com.sportingbet.sportsbook.betslip.commands;

import com.sportingbet.sportsbook.betslip.controller.BetslipController;
import com.sportingbet.sportsbook.general.commands.Command;
import com.sportingbet.sportsbook.general.commands.CommandProgressDelegate;
import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.general.network.response.ProgressController;

import lombok.Setter;
import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class BetslipCommand implements Command<RetrofitError>, ErrorHandler, ProgressController {

    protected final BetslipController betslipController;

    @Setter
    private CommandProgressDelegate<RetrofitError> delegate;

    public BetslipCommand(BetslipController betslipController) {
        this.betslipController = betslipController;
    }

    @Override
    public void cancel() {
        delegate = null;
    }

    @Override
    public boolean handleError(RetrofitError networkError) {
        delegate.commandDidFinishWithError(this, networkError);
        return true;
    }

    @Override
    public void preExecute() {
    }

    @Override
    public void postExecuteWithSuccess(boolean success) {
        if (success) {
            delegate.commandDidFinish(this);
        }
    }
}
