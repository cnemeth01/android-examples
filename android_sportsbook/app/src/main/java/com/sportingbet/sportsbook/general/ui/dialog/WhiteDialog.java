package com.sportingbet.sportsbook.general.ui.dialog;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class WhiteDialog extends DialogFragment implements View.OnClickListener {

    private View.OnClickListener onClickListener;

    @SuppressLint("ValidFragment")
    private WhiteDialog() {
    }

    public static WhiteDialog newInstance(String title, String description, String firstButtonText, String secondButtonText, View.OnClickListener onClickListener) {
        WhiteDialog whiteDialog = new WhiteDialog();

        Bundle bundle = new Bundle();
        putResourceValueToBundle(R.id.title, title, bundle);
        putResourceValueToBundle(R.id.subtitle, description, bundle);
        putResourceValueToBundle(android.R.id.button1, firstButtonText, bundle);
        putResourceValueToBundle(android.R.id.button2, secondButtonText, bundle);
        whiteDialog.setArguments(bundle);

        whiteDialog.setCancelable(false);
        whiteDialog.onClickListener = onClickListener;
        return whiteDialog;
    }

    private static void putResourceValueToBundle(int resourceId, String value, Bundle args) {
        args.putString(Integer.toString(resourceId), value);
    }

    private static String getResourceValueFromBundle(int resourceId, Bundle args) {
        return args.getString(Integer.toString(resourceId));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.white_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle arguments = getArguments();
        updateResourceValueWithBundle(view, R.id.title, arguments);
        updateResourceValueWithBundle(view, R.id.subtitle, arguments);
        updateResourceValueWithBundle(view, android.R.id.button1, arguments).setOnClickListener(this);
        updateResourceValueWithBundle(view, android.R.id.button2, arguments).setOnClickListener(this);
    }

    private TextView updateResourceValueWithBundle(View view, int resourceId, Bundle arguments) {
        TextView textView = (TextView) view.findViewById(resourceId);
        textView.setText(getResourceValueFromBundle(resourceId, arguments));
        return textView;
    }

    @Override
    public void onClick(View v) {
        dismiss();
        onClickListener.onClick(v);
    }
}
