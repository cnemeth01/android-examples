package com.sportingbet.sportsbook.general.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Copyright 2014 Tomasz Morcinek. All rights reserved.
 */
public abstract class AbstractListAdapter<T> extends BaseAdapter {

    protected final Context context;

    protected final LayoutInflater layoutInflater;

    private List<T> list;

    protected AbstractListAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    /**
     * This method needs to be invoked in any overriding method.
     * This method already contains invocation of <code>notifyDataSetChanged()</code>,
     * so it shouldn't be explicitly invoked from UI.
     *
     * @param list of elements to visualise
     */
    public void setList(List<T> list) {
        this.list = list;
        this.notifyDataSetChanged();
    }

    public List<T> getList() {
        return list;
    }

    @Override
    public final int getCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override
    public final T getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.indexOf(list.get(position));
    }
}
