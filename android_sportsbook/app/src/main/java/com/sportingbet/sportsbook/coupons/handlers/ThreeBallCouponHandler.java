package com.sportingbet.sportsbook.coupons.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.coupons.AbstractCouponHandler;
import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.coupons.model.BookCoupon;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandler;
import com.sportingbet.sportsbook.coupons.ui.CouponLayout;
import com.sportingbet.sportsbook.coupons.util.CouponHelper;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ThreeBallCouponHandler extends AbstractCouponHandler<ThreeBallCouponHandler.ViewHolder> implements EventCouponHandler<ThreeBallCouponHandler.ViewHolder> {

    public ThreeBallCouponHandler(BetslipCouponDelegate betslipCouponDelegate) {
        super(betslipCouponDelegate);
    }

    @Override
    public ViewHolder onCreateViewHolder(Context context, ViewGroup parent) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.coupon_three_ball, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Coupon coupon, Coupon previousCoupon) {
        setupViews(viewHolder, coupon, previousCoupon, coupon.isVisible());
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Event event, Event previousEvent) {
        onBindViewHolder(viewHolder, new BookCoupon(event, event.getFirstBook()), CouponHelper.createPreviousBookCoupon(previousEvent));
    }

    private void setupViews(ViewHolder viewHolder, Coupon coupon, Coupon previousCoupon, boolean visible) {
        setupViewsWithSelectionUnderTemplateNumber(viewHolder.firstPlayer, viewHolder.firstButton, coupon, previousCoupon, visible, 1);
        setupViewsWithSelectionUnderTemplateNumber(viewHolder.secondPlayer, viewHolder.secondButton, coupon, previousCoupon, visible, 2);
        setupViewsWithSelectionUnderTemplateNumber(viewHolder.thirdPlayer, viewHolder.thirdButton, coupon, previousCoupon, visible, 3);
    }

    private void setupViewsWithSelectionUnderTemplateNumber(TextView textView, CouponLayout button, Coupon coupon, Coupon previousCoupon, boolean visible, int templateNumber) {
        Selection selection = CouponHelper.getSelectionWithTemplateNumber(coupon, templateNumber);
        textView.setText(selection.getName());
        Selection previousSelection = CouponHelper.getPreviousSelectionWithTemplateNumber(previousCoupon, templateNumber);
        CouponHelper.setupButton(button, coupon.getEvent(), coupon.getBook(), selection, previousSelection, visible, betslipCouponDelegate);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView firstPlayer;
        private final TextView secondPlayer;
        private final TextView thirdPlayer;

        private final CouponLayout firstButton;
        private final CouponLayout secondButton;
        private final CouponLayout thirdButton;

        public ViewHolder(View view) {
            super(view);
            firstPlayer = (TextView) view.findViewById(R.id.first_player);
            secondPlayer = (TextView) view.findViewById(R.id.second_player);
            thirdPlayer = (TextView) view.findViewById(R.id.third_player);

            firstButton = (CouponLayout) view.findViewById(R.id.first_button);
            secondButton = (CouponLayout) view.findViewById(R.id.second_button);
            thirdButton = (CouponLayout) view.findViewById(R.id.third_button);
        }
    }
}
