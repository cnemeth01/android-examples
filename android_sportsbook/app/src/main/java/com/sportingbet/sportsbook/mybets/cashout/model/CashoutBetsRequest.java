package com.sportingbet.sportsbook.mybets.cashout.model;

import com.sportingbet.sportsbook.mybets.model.MyBet;

import lombok.Getter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CashoutBetsRequest {

    @Getter
    private String betReferenceId;

    @Getter
    private Double cashoutAmount;

    public static CashoutBetsRequest create(MyBet myBet) {
        CashoutBetsRequest cashoutBetsRequest = new CashoutBetsRequest();
        cashoutBetsRequest.betReferenceId = String.valueOf(myBet.getBetReference());
        cashoutBetsRequest.cashoutAmount = myBet.getCashoutAmount();
        return cashoutBetsRequest;
    }
}
