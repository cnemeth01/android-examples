package com.sportingbet.sportsbook.general.network.services;

import com.sportingbet.sportsbook.sports.configuration.SportConfig;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface ConfigService {

    @GET("/sportsbook/v1/defaultDisplayBySport.json")
    void getSportConfigs(Callback<List<SportConfig>> callback);
}
