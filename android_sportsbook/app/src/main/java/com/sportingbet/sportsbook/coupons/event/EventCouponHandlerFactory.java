package com.sportingbet.sportsbook.coupons.event;

import com.sportingbet.sportsbook.coupons.betslip.BetslipCouponDelegate;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.sports.configuration.CouponType;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface EventCouponHandlerFactory {

    BetslipCouponDelegate getBetslipCouponDelegate();

    int getItemTypeForEvent(Event event, CouponType defaultCouponType);

    EventCouponHandler getHandlerForEvent(Event event, CouponType defaultCouponType);

    EventCouponHandler getHandlerForItemType(int itemType);
}
