package com.sportingbet.sportsbook.betslip.receipt.adapter.handlers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.helpers.StakeHelper;
import com.sportingbet.sportsbook.betslip.receipt.adapter.model.BetslipReceiptFooter;

public class FooterViewHolderHandler extends AbstractViewHolderHandler<BetslipReceiptFooter, FooterViewHolderHandler.ViewHolder> {

    private final View.OnClickListener onClickListener;

    public FooterViewHolderHandler(Context context, View.OnClickListener onClickListener) {
        super(context);
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(createView(parent, R.layout.betreceipt_footer));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, BetslipReceiptFooter object) {
        StakeHelper.setupPotentialReturnView(context, holder.potentialReturn, object.getTotalPotentialReturns(), object.getUserCurrency());
        holder.continueButton.setOnClickListener(onClickListener);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView potentialReturn;
        private final View continueButton;

        private ViewHolder(View view) {
            super(view);
            potentialReturn = (TextView) view.findViewById(R.id.potential_return);
            continueButton = view.findViewById(R.id.continue_button);
        }
    }
}
