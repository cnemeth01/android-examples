package com.sportingbet.sportsbook.general;

import com.sportingbet.sportsbook.home.account.model.LoginResponse;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class LoginResponseStore {

    private LoginResponse loginResponse;

    public void setLoginResponse(LoginResponse loginResponse) {
        this.loginResponse = loginResponse;
    }

    public LoginResponse getLoginResponse() {
        return loginResponse;
    }

    public boolean isLoggedIn() {
        return loginResponse != null;
    }
}
