package com.sportingbet.sportsbook.home.drawer;

import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.sportingbet.sportsbook.R;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class DrawerToggle extends ActionBarDrawerToggle {

    private Runnable onClosedRunnable;

    public DrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar) {
        super(activity, drawerLayout, toolbar, R.string.empty_text, R.string.empty_text);
    }

    public void postOnDrawerClosed(Runnable runnable) {
        onClosedRunnable = runnable;
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        super.onDrawerClosed(drawerView);
        if (onClosedRunnable != null) {
            onClosedRunnable.run();
        }
    }
}
