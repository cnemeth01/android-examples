package com.sportingbet.sportsbook.betslip.picker.adapter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface PickerAdapter {

    Object getPayout();

    double getStake();

    double getTotalStake();

    boolean isEachWaySelected();

    void updateValue(Double value);

    Double getValue();

    interface Delegate {

        void pickerAdapterUpdated(PickerAdapter pickerAdapter);
    }
}
