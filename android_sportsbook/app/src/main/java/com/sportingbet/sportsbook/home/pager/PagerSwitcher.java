package com.sportingbet.sportsbook.home.pager;

import com.sportingbet.sportsbook.model.Sport;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class PagerSwitcher {

    private final PagerController pagerController;

    public PagerSwitcher(PagerController pagerController) {
        this.pagerController = pagerController;
    }

    public void selectSportPage(Sport sport) {
        pagerController.selectPageWithId(sport.getId());
    }
}
