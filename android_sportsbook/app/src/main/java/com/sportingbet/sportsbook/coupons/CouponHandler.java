package com.sportingbet.sportsbook.coupons;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.coupons.model.Coupon;


/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface CouponHandler<T extends RecyclerView.ViewHolder> {

    T onCreateViewHolder(Context context, ViewGroup parent);

    void onBindViewHolder(T viewHolder, Coupon coupon, Coupon previousCoupon);
}
