package com.sportingbet.sportsbook.betslip.picker.adapter;

import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.picker.value.PickerValueProvider;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class AbstractPickerAdapter implements PickerAdapter, View.OnClickListener, StakePicker.StakePickerListener {

    protected final Delegate delegate;

    protected PickerValueProvider pickerValueProvider;

    protected StakePicker stakePicker;

    private Double selectedValue;

    public AbstractPickerAdapter(Delegate delegate) {
        this.delegate = delegate;
    }

    public void setup(StakePicker stakePicker, PickerValueProvider pickerValueProvider) {
        this.stakePicker = stakePicker;
        this.stakePicker.setStakePickerListener(this);
        this.pickerValueProvider = pickerValueProvider;
        stakePicker.getButtonMinus().setOnClickListener(this);
        stakePicker.getButtonPlus().setOnClickListener(this);
        updateValue(selectedValue);
    }

    @Override
    public void updateValue(Double value) {
        updateEditTextView(value);
        notifyValueUpdated(value);
    }

    private void updateEditTextView(Double value) {
        if (stakePicker != null) {
            stakePicker.updateValue(value);
        }
    }

    private void notifyValueUpdated(Double value) {
        selectedValue = value;
        if (stakePicker != null) {
            stakePicker.getButtonMinus().setEnabled(pickerValueProvider.hasPreviousValue(value));
            stakePicker.getButtonPlus().setEnabled(pickerValueProvider.hasNextValue(value));
        }
    }

    @Override
    public Double getValue() {
        return selectedValue;
    }

    @Override
    public void onClick(View view) {
        stakePicker.getStakeEditText().requestFocus();
        Double value = getValue();
        switch (view.getId()) {
            case R.id.stake_button_minus:
                value = pickerValueProvider.getPreviousValue(value);
                break;
            case R.id.stake_button_plus:
                value = pickerValueProvider.getNextValue(value);
                break;
        }
        updateValue(value);
        delegate.pickerAdapterUpdated(this);
    }

    @Override
    public double getStake() {
        return selectedValue != null ? selectedValue : 0;
    }

    @Override
    public void onValueChanged(Double value) {
        notifyValueUpdated(value);
        delegate.pickerAdapterUpdated(this);
    }
}
