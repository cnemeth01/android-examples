package com.sportingbet.sportsbook.model;

import android.support.annotation.NonNull;

import com.sportingbet.sportsbook.home.navigation.NavigationItem;

import hrisey.Parcelable;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class Sport extends NavigationItem implements Comparable<Sport> {

    public static final int TENNIS = 8;
    public static final int FOOTBALL = 102;
    public static final int BASKETBALL = 190;
    public static final int VOLLEYBALL = 177;
    public static final int BEACH_VOLLEYBALL = 370;

    public static final long INTERNATIONAL_HORSE_RACING = 2;
    public static final long UK_HORSE_RACING = 171;

    public Sport(long id) {
        this.id = id;
    }

    public Sport(long id, String name) {
        super(id, name);
    }

    @Override
    public int compareTo(@NonNull Sport another) {
        return name.compareTo(another.name);
    }
}
