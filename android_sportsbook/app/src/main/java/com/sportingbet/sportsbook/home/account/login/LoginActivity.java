package com.sportingbet.sportsbook.home.account.login;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.SwitchCompat;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.sportingbet.sportsbook.BuildConfig;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.LoginResponseStore;
import com.sportingbet.sportsbook.general.activity.ToolbarActivity;
import com.sportingbet.sportsbook.general.network.progress.ProgressDialogController;
import com.sportingbet.sportsbook.general.network.response.ErrorHandler;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.home.account.model.LoginResponse;

import javax.inject.Inject;

import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class LoginActivity extends ToolbarActivity implements View.OnClickListener, NetworkResponseListener<LoginResponse> {

    private ErrorHandler errorHandler = new LoginErrorHandler(this);

    @Inject
    LoginController loginController;

    @Inject
    LoginResponseStore loginResponseStore;

    @Inject
    LoginRequester loginRequester;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_outside_right);
        setContentView(R.layout.login);

        setupViews();
        setupLinks();
    }

    @Override
    protected int getToolbarTitle() {
        return getIntent().getIntExtra(Intent.EXTRA_TITLE, R.string.login_title);
    }

    private void setupViews() {
        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);
        loginButton.setText(getString(getToolbarTitle()));
        loginController.registerViews((EditText) findViewById(R.id.user_name), (EditText) findViewById(R.id.user_password), (SwitchCompat) findViewById(R.id.user_switch), loginButton);
    }

    private void setupLinks() {
        findViewById(R.id.general_rules).setOnClickListener(this);
        findViewById(R.id.gambling_commission).setOnClickListener(this);
        findViewById(R.id.login_footer_text).setOnClickListener(this);
        findViewById(R.id.underage_gambling).setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                hideSoftInput(getCurrentFocus());
                invokeOnBackPressedWithDelay();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void hideSoftInput(View view) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void invokeOnBackPressedWithDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onBackPressed();
            }
        }, getResources().getInteger(android.R.integer.config_shortAnimTime));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                loginRequester.requestLogin(loginController.getLoginRequestBody(), this, new ProgressDialogController(this));
                break;
            case R.id.gambling_commission:
            case R.id.login_footer_text:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.LINK_GAMBLING_COMMISSION)));
                break;
            case R.id.general_rules:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.LINK_GENERAL_RULES)));
                break;
            case R.id.underage_gambling:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.LINK_UNDERAGE_GAMBLING)));
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_inside_left, R.anim.slide_out_right);
    }

    @Override
    public void success(LoginResponse loginResponse) {
        loginResponseStore.setLoginResponse(loginResponse);
        loginController.update();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void failure(RetrofitError error) {
        errorHandler.handleError(error);
    }
}
