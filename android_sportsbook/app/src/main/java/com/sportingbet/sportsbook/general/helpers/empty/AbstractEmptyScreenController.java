package com.sportingbet.sportsbook.general.helpers.empty;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.sportingbet.sportsbook.R;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class AbstractEmptyScreenController implements EmptyScreenController{

    private final View emptyView;
    private final View contentLayout;

    public AbstractEmptyScreenController(View emptyView, View contentLayout) {
        this.emptyView = emptyView;
        this.contentLayout = contentLayout;
    }

    @Override
    public void showEmptyView() {
        if (getEmptyViewVisibility() != View.VISIBLE) {
            changeViewVisibilityWithAnimation(emptyView, View.VISIBLE, R.anim.abc_fade_in);
            changeViewVisibilityWithAnimation(contentLayout, View.GONE, R.anim.abc_fade_out);
        }
    }

    @Override
    public void hideEmptyView() {
        if (getEmptyViewVisibility() == View.VISIBLE) {
            changeViewVisibilityWithAnimation(emptyView, View.GONE, R.anim.abc_fade_out);
            changeViewVisibilityWithAnimation(contentLayout, View.VISIBLE, R.anim.abc_fade_in);
        }
    }

    private int getEmptyViewVisibility() {
        return emptyView.getVisibility();
    }

    private void changeViewVisibilityWithAnimation(View view, int visibility, int animationId) {
        view.setAnimation(AnimationUtils.loadAnimation(view.getContext(), animationId));
        view.setVisibility(visibility);
    }

}
