package com.sportingbet.sportsbook.content.general.controllers;

import android.content.Context;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.sports.configuration.CouponType;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class EventsHeaderController extends AbstractHeaderController {

    public EventsHeaderController(Context context) {
        super(context);
    }

    @Override
    public void setup(ViewGroup headerLayout, CouponType defaultCouponType) {
        super.setup(headerLayout, defaultCouponType);
        switch (defaultCouponType) {
            case CouponTypeTie:
                inflateResourceInHeader(headerLayout, R.layout.header_coupon_tie);
                break;
            case CouponTypeNoTie:
                inflateResourceInHeader(headerLayout, R.layout.header_coupon_no_tie);
                break;
        }
    }
}
