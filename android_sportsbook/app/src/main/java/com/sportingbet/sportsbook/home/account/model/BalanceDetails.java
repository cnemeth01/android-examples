package com.sportingbet.sportsbook.home.account.model;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class BalanceDetails implements android.os.Parcelable {

    @Getter
    private String currencyId;

    @Getter
    private Double tradingBalance;

    public String toString() {
        return String.format("%.2f %s", tradingBalance, currencyId);
    }
}
