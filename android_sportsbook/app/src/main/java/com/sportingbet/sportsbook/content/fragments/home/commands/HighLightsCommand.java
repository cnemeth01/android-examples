package com.sportingbet.sportsbook.content.fragments.home.commands;

import com.sportingbet.sportsbook.content.fragments.highlights.HighlightsRequester;
import com.sportingbet.sportsbook.general.commands.NetworkCommand;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.event.DetailsWrapper;
import com.sportingbet.sportsbook.model.event.Event;

import java.util.List;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HighLightsCommand extends NetworkCommand<DetailsWrapper> {

    private final HighlightsRequester highlightsRequester;
    private final Sport sport;

    private final HighlightsDelegate highlightsDelegate;

    public HighLightsCommand(HighlightsRequester highlightsRequester, Sport sport, HighlightsDelegate highlightsDelegate) {
        this.highlightsRequester = highlightsRequester;
        this.highlightsRequester.setupResponseListener(this);
        this.sport = sport;
        this.highlightsDelegate = highlightsDelegate;
    }

    @Override
    public void execute() {
        highlightsRequester.prepareRequest();
        highlightsRequester.requestHighlightsForSport(sport.getId());
    }

    @Override
    public void success(DetailsWrapper object) {
        highlightsDelegate.highlightsForSport(sport, object.getEvents());
        super.success(object);
    }

    public interface HighlightsDelegate {

        void highlightsForSport(Sport sport, List<Event> events);
    }
}
