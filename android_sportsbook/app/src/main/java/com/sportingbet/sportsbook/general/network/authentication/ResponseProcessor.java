package com.sportingbet.sportsbook.general.network.authentication;

import retrofit.client.Response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface ResponseProcessor {

    void processResponse(Response response);
}
