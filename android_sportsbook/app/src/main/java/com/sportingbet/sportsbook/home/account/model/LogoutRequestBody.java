package com.sportingbet.sportsbook.home.account.model;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class LogoutRequestBody {

    public static final LogoutRequestBody NORMAL = new LogoutRequestBody("NORMAL");

    @Getter
    private String logOffReason;

    private LogoutRequestBody(String logOffReason) {
        this.logOffReason = logOffReason;
    }
}
