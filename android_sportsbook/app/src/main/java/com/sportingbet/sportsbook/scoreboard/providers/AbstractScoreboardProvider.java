package com.sportingbet.sportsbook.scoreboard.providers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.scoreboard.ScoreboardProvider;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class AbstractScoreboardProvider implements ScoreboardProvider {

    protected final Context context;

    public AbstractScoreboardProvider(Context context) {
        this.context = context;
    }

    protected abstract int getScoreboardLayout();

    protected TextView getTextView(View view, int resourceId) {
        return (TextView) view.findViewById(resourceId);
    }

    @Override
    public final View createScoreboard(ViewGroup parent, Event event) {
        View view = LayoutInflater.from(context).inflate(getScoreboardLayout(), parent);
        view.setBackgroundResource(getBackgroundResource(event));
        return view;
    }

    private int getBackgroundResource(Event event) {
        switch ((int) event.getSportId()) {
            case Sport.FOOTBALL:
                return R.drawable.scoreboard_football;
            case Sport.BASKETBALL:
                return R.drawable.scoreboard_basketball;
            case Sport.TENNIS:
                return R.drawable.scoreboard_tennis;
            case Sport.VOLLEYBALL:
                return R.drawable.scoreboard_volleyball;
            default:
                return R.drawable.scoreboard;
        }
    }
}
