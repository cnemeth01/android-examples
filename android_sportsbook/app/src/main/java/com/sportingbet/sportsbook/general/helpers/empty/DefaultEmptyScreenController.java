package com.sportingbet.sportsbook.general.helpers.empty;

import android.view.View;

import com.sportingbet.sportsbook.R;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class DefaultEmptyScreenController extends AbstractEmptyScreenController {

    public DefaultEmptyScreenController(View view) {
        super(view.findViewById(android.R.id.empty), view.findViewById(R.id.recycler_view));
    }
}
