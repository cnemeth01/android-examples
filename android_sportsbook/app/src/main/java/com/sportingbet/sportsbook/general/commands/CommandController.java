package com.sportingbet.sportsbook.general.commands;

import com.sportingbet.sportsbook.general.network.response.ProgressController;

import java.util.ArrayList;
import java.util.List;

import lombok.Setter;
import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CommandController implements CommandProgressDelegate<RetrofitError> {

    final List<Command> commands = new ArrayList<>();

    @Setter
    private Delegate delegate;

    @Setter
    private ProgressController progressController;

    public CommandController(Command... commandArray) {
        for (Command command : commandArray) {
            commands.add(command);
            command.setDelegate(this);
        }
    }

    public void start() {
        progressController.preExecute();
        commands.get(0).execute();
    }

    public void cancel() {
        for (Command command : commands) {
            command.cancel();
        }
        progressController.postExecuteWithSuccess(false);
    }

    @Override
    public void commandDidFinish(Command command) {
        int index = commands.indexOf(command) + 1;
        if (index == commands.size()) {
            finish();
        } else {
            commands.get(index).execute();
        }
    }

    private void finish() {
        progressController.postExecuteWithSuccess(true);
        delegate.commandControllerDidFinish();
    }

    @Override
    public void commandDidFinishWithError(Command command, RetrofitError error) {
        progressController.postExecuteWithSuccess(false);
        delegate.commandControllerDidFinishWithError(error);
    }

    public interface Delegate {

        void commandControllerDidFinish();

        void commandControllerDidFinishWithError(RetrofitError networkError);
    }
}
