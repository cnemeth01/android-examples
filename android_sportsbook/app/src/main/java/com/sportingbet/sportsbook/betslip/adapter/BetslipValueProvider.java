package com.sportingbet.sportsbook.betslip.adapter;

import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetRequest;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface BetslipValueProvider {

    String getTotalPayout();

    boolean hasTotalPayout();

    double getTotalStake();

    PlaceBetRequest getPlaceBetRequest();

    int getPlaceBetDelay();
}
