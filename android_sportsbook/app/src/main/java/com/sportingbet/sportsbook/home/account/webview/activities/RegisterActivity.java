package com.sportingbet.sportsbook.home.account.webview.activities;

import android.os.Bundle;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.home.account.webview.WebURLFactory;
import com.sportingbet.sportsbook.home.account.webview.WebViewActivity;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class RegisterActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        webView.loadUrl(WebURLFactory.getRegistrationURL());
    }

    protected int getToolbarTitle() {
        return R.string.register_title;
    }
}
