package com.sportingbet.sportsbook.general.ui.dialog;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;

import lombok.Setter;

/**
 * Copyright 2014 Tomasz Morcinek. All rights reserved.
 */
public class RingProgressBarDialog extends Dialog {

    @Setter
    private long progressTimeMillis;

    @Setter
    private int message;

    public RingProgressBarDialog(Context context) {
        super(context, R.style.DimmedDialogTheme);
        getWindow().setContentView(R.layout.ring_progress_bar);
        setCancelable(false);
    }

    @Override
    public void show() {
        super.show();
        showMessage();
        showProgressAnimation();
        showTextAnimation();
    }

    private void showMessage() {
        ((TextView) findViewById(R.id.message)).setText(message);
    }

    private void showProgressAnimation() {
        ObjectAnimator animation = ObjectAnimator.ofInt(findViewById(R.id.progress_bar), "progress", 0, getMaxValue());
        animation.setDuration(progressTimeMillis);
        animation.setInterpolator(new LinearInterpolator());
        animation.start();
    }

    private int getMaxValue() {
        return getContext().getResources().getInteger(R.integer.config_progressBarMax);
    }

    private void showTextAnimation() {
        ValueAnimator animator = new ValueAnimator();
        animator.setObjectValues((int) progressTimeMillis, 0);
        animator.setEvaluator(new TypeEvaluator<Integer>() {
            public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
                return Math.round(startValue * (1 - fraction));
            }
        });
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            final TextView view = (TextView) findViewById(R.id.text);

            public void onAnimationUpdate(ValueAnimator animation) {
                Integer animatedValue = (Integer) animation.getAnimatedValue();
                view.setText(String.format("%.0f", (float) animatedValue / 1000));
            }
        });
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(progressTimeMillis);
        animator.start();
    }
}
