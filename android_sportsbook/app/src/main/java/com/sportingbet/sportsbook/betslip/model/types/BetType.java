package com.sportingbet.sportsbook.betslip.model.types;

import com.google.common.base.Objects;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class BetType {

    @Getter
    private String betId;

    @Getter
    private String betType;

    @Getter
    private String betTypeName;

    @Getter
    private long selectionId;

    @Setter
    private Double stakeMultiplier;

    @Getter
    @Setter
    private Integer numberOfBets;

    @Setter
    private Double eachwayStakeMultiplier;

    @Getter
    private Integer eachwayNumberOfBets;

    @Getter
    private Integer inrunningDelay;

    @Getter
    @Setter
    private ErrorCode errorCode;

    @Getter
    @Setter
    private String localisedErrorMessage;

    public boolean isSingle() {
        return "SINGLE".equals(betType);
    }

    public boolean isEachWay(){
        return eachwayNumberOfBets != null;
    }

    public double getStakeMultiplier() {
        if (stakeMultiplier == null) {
            return 0.0;
        }
        return stakeMultiplier;
    }

    public double getEachwayStakeMultiplier() {
        if (eachwayStakeMultiplier == null) {
            return 0.0;
        }
        return eachwayStakeMultiplier;
    }

    @Override
    public boolean equals(Object o) {
        return Objects.equal(betId, ((BetType) o).getBetId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(betId);
    }
}
