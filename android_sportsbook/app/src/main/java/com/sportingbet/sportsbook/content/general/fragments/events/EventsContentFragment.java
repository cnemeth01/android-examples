package com.sportingbet.sportsbook.content.general.fragments.events;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.fragments.event.EventDetailsFragment;
import com.sportingbet.sportsbook.content.general.controllers.EventsHeaderController;
import com.sportingbet.sportsbook.content.general.controllers.refresh.RefreshController;
import com.sportingbet.sportsbook.content.general.fragments.PublishingContentFragment;
import com.sportingbet.sportsbook.content.general.helpers.FragmentStateHelper;
import com.sportingbet.sportsbook.coupons.event.EventCouponHandlerFactory;
import com.sportingbet.sportsbook.general.adapter.handlers.TypedHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.model.event.EventsWrapper;
import com.sportingbet.sportsbook.sports.configuration.CouponType;
import com.sportingbet.sportsbook.sports.configuration.SportConfigProvider;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class EventsContentFragment<T extends EventsWrapper> extends PublishingContentFragment<T, Event> implements RefreshController.RefreshControllerListener {


    private FragmentStateHelper fragmentStateHelper;

    @Inject
    RefreshController refreshController;

    @Inject
    protected EventCouponHandlerFactory couponHandlerFactory;

    @Inject
    protected SportConfigProvider sportConfigProvider;

    @Inject
    protected EventsHeaderController eventsHeaderController;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.events_list_fragment;
    }

    protected CouponType getDefaultCouponType() {
        return sportConfigProvider.getDefaultCouponTypeForSportId(getSport().getId());
    }

    @Override
    protected final TypedHandlersRecyclerViewAdapter<Event> createAdapter() {
        EventsAdapter adapter = new EventsAdapter(getActivity(), couponHandlerFactory, getDefaultCouponType());
        couponHandlerFactory.getBetslipCouponDelegate().register(getView(), adapter);
        return adapter;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        eventsHeaderController.setup((ViewGroup) view.findViewById(R.id.header_container), getDefaultCouponType());

        fragmentStateHelper = new FragmentStateHelper(getAdapter());
        fragmentStateHelper.onRestoreInstanceState(savedInstanceState);

        refreshController.registerRefreshController(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fragmentStateHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onFragmentSelected() {
        refreshController.restart();
        super.onFragmentSelected();
    }

    @Override
    public void onFragmentDisabled() {
        super.onFragmentDisabled();
        refreshController.cancel();
    }

    @Override
    protected ProgressController[] getProgressControllers() {
        return new ProgressController[]{refreshProgressController, progressBarController, refreshController};
    }

    @Override
    public void success(T object) {
        updateEvents(object.getEvents());
        if (object.getEvents().isEmpty()) {
            emptyScreenController.showEmptyView();
        } else {
            emptyScreenController.hideEmptyView();
        }
    }

    private void updateEvents(List events) {
        getAdapter().setList(events);
    }

    @Override
    public void onItemClicked(View view, Object item) {
        getNavigationDelegate().addFragment(EventDetailsFragment.newInstance((Event) item));
    }
}
