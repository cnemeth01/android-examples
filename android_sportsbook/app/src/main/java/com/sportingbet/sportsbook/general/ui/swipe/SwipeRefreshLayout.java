package com.sportingbet.sportsbook.general.ui.swipe;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.sportingbet.sportsbook.R;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SwipeRefreshLayout extends android.support.v4.widget.SwipeRefreshLayout {

    private RecyclerView recyclerView;

    public SwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private RecyclerView getRecyclerView() {
        if (recyclerView == null) {
            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        }
        return recyclerView;
    }

    @Override
    public boolean canChildScrollUp() {
        return getRecyclerView().canScrollVertically(-1);
    }
}
