package com.sportingbet.sportsbook.general.helpers.empty;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface EmptyScreenController {

    void showEmptyView();

    void hideEmptyView();
}
