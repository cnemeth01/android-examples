package com.sportingbet.sportsbook.general.network.progress;

import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.network.response.ProgressController;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public class ItemProgressBarController implements ProgressController {

    private final View view;
    private final View progressBar;
    private final View indicator;

    public ItemProgressBarController(View view) {
        this.view = view;
        this.progressBar = view.findViewById(R.id.progress_bar);
        this.indicator = view.findViewById(R.id.indicator);
    }

    @Override
    public void preExecute() {
        view.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        indicator.setVisibility(View.INVISIBLE);
    }

    @Override
    public void postExecuteWithSuccess(boolean success) {
        view.setEnabled(true);
        progressBar.setVisibility(View.GONE);
        indicator.setVisibility(View.VISIBLE);
    }
}
