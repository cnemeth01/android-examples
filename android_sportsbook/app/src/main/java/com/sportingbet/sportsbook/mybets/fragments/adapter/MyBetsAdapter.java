package com.sportingbet.sportsbook.mybets.fragments.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.helpers.StakeHelper;
import com.sportingbet.sportsbook.general.LoginResponseStore;
import com.sportingbet.sportsbook.general.adapter.handlers.TypedHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.ui.ViewHelper;
import com.sportingbet.sportsbook.mybets.model.MyBetSelection;
import com.sportingbet.sportsbook.mybets.model.MyBetSelections;
import com.sportingbet.sportsbook.mybets.model.MyBetStatus;
import com.sportingbet.sportsbook.mybets.model.MyBetWrapper;

import java.util.List;

import lombok.Setter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsAdapter extends TypedHandlersRecyclerViewAdapter<MyBetWrapper> {

    private final String currencyId;

    @Setter
    private CashOutButtonClickListener cashOutButtonClickListener;

    public MyBetsAdapter(Context context, View.OnClickListener onClickListener, LoginResponseStore loginResponseStore) {
        super(context);
        this.currencyId = getCurrency(loginResponseStore);
        addViewHolderHandler(String.class, new MyBetsFooterViewHolderHandler(context, onClickListener));
    }

    public void setMyBets(List<MyBetWrapper> myBets) {
        List<Object> items = Lists.newArrayList();
        items.addAll(myBets);
        items.add(context.getString(R.string.my_bets_view_account));
        setList(items);
    }

    private String getCurrency(LoginResponseStore loginResponseStore) {
        return loginResponseStore.getLoginResponse().getBalanceDetails().getCurrencyId();
    }

    @Override
    protected int getTypeItemViewType(MyBetWrapper item) {
        return 0;
    }

    @Override
    protected ViewHolder onCreateTypeViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.my_bets_card, parent, false));
    }

    @Override
    protected void onBindTypeViewHolder(RecyclerView.ViewHolder viewHolder, MyBetWrapper object, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        setupViews(holder, object);
        setupSelectionsLayout(holder, object.getBet().getSelections());
    }

    private void setupViews(ViewHolder holder, MyBetWrapper object) {
        setupTitleRow(holder, object);
        setupStakeView(holder, object);
        setupAmountViewContent(holder, object);
        setupTotalPayoutView(holder, object);
        setupCashOutButton(holder, object);
        hideReferenceNumberLayout(holder);
        setupWarning(holder, object);
    }

    private void setupTitleRow(ViewHolder holder, MyBetWrapper object) {
        holder.title.setText(object.getBet().getBetType());
        holder.betStatus.setText(getBetStatusText(object.getBet().getBetStatus()));
        holder.betStatus.setAllCaps(true);
        holder.betStatus.setVisibility(View.VISIBLE);
    }

    @Nullable
    private String getBetStatusText(MyBetStatus betStatus) {
        switch (betStatus) {
            case CASHOUT:
                return context.getString(R.string.my_bets_cashed_out);
            case LOST:
            case WON:
                return betStatus.name();
            default:
                return null;
        }
    }

    private void setupStakeView(ViewHolder holder, MyBetWrapper object) {
        StakeHelper.setupStakeView(context, holder.totalStake, object.getBet().getTotalStake(), currencyId);
    }

    private void setupAmountViewContent(ViewHolder holder, MyBetWrapper object) {
        setupAmountViewVisibility(holder, isStatusLost(object) ? View.GONE : View.VISIBLE);
        switch (object.getBet().getBetStatus()) {
            case LOST:
                break;
            case CASHOUT:
            case CASHEDOUT:
                holder.totalPayoutTitle.setText(R.string.my_bets_cashed_out_for);
                break;
            case WON:
                holder.totalPayoutTitle.setText(R.string.my_bets_return);
                break;
            default:
                holder.totalPayoutTitle.setText(R.string.betslip_total_payout);
                break;
        }
    }

    private boolean isStatusLost(MyBetWrapper object) {
        return object.getBet().getBetStatus() == MyBetStatus.LOST;
    }

    private void setupAmountViewVisibility(ViewHolder holder, int visibility) {
        holder.totalPayoutTitle.setVisibility(visibility);
        holder.totalPayout.setVisibility(visibility);
    }

    private void setupTotalPayoutView(ViewHolder holder, MyBetWrapper object) {
        switch (object.getBet().getBetStatus()) {
            case CASHOUT:
                StakeHelper.setupPotentialReturnView(context, holder.totalPayout, Double.toString(object.getBet().getReturnAmount()), currencyId);
                break;
            default:
                StakeHelper.setupPotentialReturnView(context, holder.totalPayout, Double.toString(object.getBet().getPotentialReturnAmount()), currencyId);
                break;
        }
    }

    private void setupCashOutButton(ViewHolder holder, final MyBetWrapper object) {
        setupCashOutTickLayout(holder, object);
        if (object.getBet().getCashoutAmount() != null && object.getBet().getCashoutAmount() > 0) {
            holder.cashOutButtonView.setEnabled(true);
            holder.cashOutButtonView.setVisibility(View.VISIBLE);
            setupCashOutButtonListener(holder, object);
            StakeHelper.setupCashOutView(context, holder.cashOutButtonTextView, object.getBet().getCashoutAmount(), currencyId);
        } else {
            holder.cashOutButtonView.setVisibility(View.GONE);
            holder.cashOutButtonView.setOnClickListener(null);
        }
    }

    private void setupCashOutButtonListener(final ViewHolder holder, final MyBetWrapper object) {
        holder.cashOutButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cashOutButtonClickListener.cashOutButtonClicked(object, holder.cashOutButtonView);
            }
        });
    }

    private void setupCashOutTickLayout(final ViewHolder holder, final MyBetWrapper object) {
        switch (object.getBet().getBetStatus()) {
            case CASHEDOUT:
                holder.cashOutTickLayout.setVisibility(View.VISIBLE);
                break;
            default:
                holder.cashOutTickLayout.setVisibility(View.GONE);
                break;
        }
    }

    private void hideReferenceNumberLayout(ViewHolder holder) {
        holder.referenceNumberLayout.setVisibility(View.GONE);
    }

    private void setupSelectionsLayout(ViewHolder holder, MyBetSelections selections) {
        ViewGroup selectionsLayout = holder.selectionsLayout;
        selectionsLayout.removeAllViews();
        for (MyBetSelection selection : selections.getSelection()) {
            setupSelection(selectionsLayout, selection);
        }
    }

    private void setupSelection(ViewGroup selectionsLayout, MyBetSelection myBetSelection) {
        SelectionViewHolder selectionViewHolder = onCreateSelectionViewHolder(selectionsLayout);
        selectionViewHolder.event.setText(myBetSelection.getEvent());
        selectionViewHolder.odds.setText(myBetSelection.getPrice());
        selectionViewHolder.book.setText(myBetSelection.getMarket());
        setupSelectionText(myBetSelection, selectionViewHolder);
        setupEachWay(myBetSelection, selectionViewHolder);
    }

    private SelectionViewHolder onCreateSelectionViewHolder(ViewGroup selectionsLayout) {
        View view = layoutInflater.inflate(R.layout.betreceipt_item_selection, selectionsLayout, false);
        selectionsLayout.addView(view);
        return new SelectionViewHolder(view);
    }

    private void setupSelectionText(MyBetSelection myBetSelection, SelectionViewHolder selectionViewHolder) {
        if (myBetSelection.getHandicap() != null) {
            selectionViewHolder.selection.setText(context.getString(R.string.my_bets_handicap_format, myBetSelection.getSelection(), myBetSelection.getHandicap()));
        } else {
            selectionViewHolder.selection.setText(myBetSelection.getSelection());
        }
    }

    private void setupEachWay(MyBetSelection myBetSelection, SelectionViewHolder selectionViewHolder) {
        if (myBetSelection.getEachWayTerms() != null) {
            selectionViewHolder.eachWay.setVisibility(View.VISIBLE);
            selectionViewHolder.eachWay.setText(context.getString(R.string.my_bets_each_way_format, myBetSelection.getEachWayTerms()));
        }
    }

    private void setupWarning(ViewHolder holder, MyBetWrapper object) {
        holder.warningText.setVisibility(object.isHasError() ? View.VISIBLE : View.GONE);
        holder.warningText.setText(object.getErrorMessage());
        if (object.isPriceChange()) {
            holder.cashOutButtonTextView.startAnimation(ViewHelper.blinkAnimation(context));
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView title;
        private final View cashOutTickLayout;
        private final TextView betStatus;
        private final ViewGroup selectionsLayout;
        private final TextView totalStake;
        private final TextView totalPayout;
        private final View cashOutButtonView;
        private final TextView cashOutButtonTextView;
        private final TextView totalPayoutTitle;
        private final View referenceNumberLayout;
        private final TextView warningText;

        private ViewHolder(View view) {
            super(view);
            view.findViewById(R.id.item_layout).setBackgroundResource(android.R.color.transparent);
            title = (TextView) view.findViewById(R.id.title);
            cashOutTickLayout = view.findViewById(R.id.cashout_tick_layout);
            betStatus = (TextView) view.findViewById(R.id.bet_status);
            selectionsLayout = (ViewGroup) view.findViewById(R.id.selections_layout);
            totalStake = (TextView) view.findViewById(R.id.total_stake);
            totalPayout = (TextView) view.findViewById(R.id.total_payout);
            totalPayoutTitle = (TextView) view.findViewById(R.id.total_payout_title);
            cashOutButtonView = view.findViewById(R.id.cash_out_button);
            cashOutButtonTextView = (TextView) view.findViewById(R.id.cash_out_button_text);
            referenceNumberLayout = view.findViewById(R.id.reference_number_layout);
            warningText = (TextView) view.findViewById(R.id.warning_text);
        }
    }

    private class SelectionViewHolder {

        private final TextView selection;
        private final TextView odds;
        private final TextView book;
        private final TextView event;
        private final TextView eachWay;

        private SelectionViewHolder(View view) {
            selection = (TextView) view.findViewById(R.id.selection);
            odds = (TextView) view.findViewById(R.id.odds);
            book = (TextView) view.findViewById(R.id.book);
            event = (TextView) view.findViewById(R.id.event);
            eachWay = (TextView) view.findViewById(R.id.each_way);
        }
    }

    public interface CashOutButtonClickListener {

        void cashOutButtonClicked(MyBetWrapper myBetWrapper, View view);
    }
}
