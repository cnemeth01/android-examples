package com.sportingbet.sportsbook.home.drawer.menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.adapter.AbstractRecyclerViewAdapter;
import com.sportingbet.sportsbook.home.drawer.list.DrawerItem;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportMenuAdapter extends AbstractRecyclerViewAdapter<DrawerItem, SportMenuAdapter.ViewHolder> {

    private long checkedItemId;

    public SportMenuAdapter(Context context) {
        super(context);
    }

    public void setCheckedItemId(long selectedSportId) {
        notifyDataSetChanged();
        this.checkedItemId = selectedSportId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.drawer_menu_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DrawerItem drawerItem = getItem(position);
        initializeOnClickListener(holder, drawerItem);
        holder.titleView.setText(drawerItem.getTitle());
        holder.imageView.setImageResource(drawerItem.getIcon());
        holder.itemView.setActivated(itemShouldBeActivated(drawerItem));
    }

    private boolean itemShouldBeActivated(DrawerItem drawerItem) {
        return checkedItemId == drawerItem.getId();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView titleView;
        private final ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            titleView = (TextView) view.findViewById(R.id.title);
            imageView = (ImageView) view.findViewById(R.id.icon);
        }
    }
}
