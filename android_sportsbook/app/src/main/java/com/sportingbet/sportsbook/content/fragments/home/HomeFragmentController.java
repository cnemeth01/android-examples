package com.sportingbet.sportsbook.content.fragments.home;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.content.fragments.highlights.HighlightsRequester;
import com.sportingbet.sportsbook.content.fragments.home.commands.HighLightsCommand;
import com.sportingbet.sportsbook.general.commands.Command;
import com.sportingbet.sportsbook.general.commands.CommandController;
import com.sportingbet.sportsbook.general.network.response.ContainerProgressController;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.home.navigation.configuration.NavigationConfiguration;
import com.sportingbet.sportsbook.model.Sport;
import com.sportingbet.sportsbook.model.event.Event;

import java.util.List;
import java.util.Map;

import retrofit.RetrofitError;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class HomeFragmentController implements CommandController.Delegate, HighLightsCommand.HighlightsDelegate {

    private final PublishingService publishingService;
    private final NavigationConfiguration navigationConfiguration;

    private HomeControllerDelegate homeControllerDelegate;

    private Map<Sport, List<Event>> eventMap;

    private CommandController commandController;

    public HomeFragmentController(PublishingService publishingService, NavigationConfiguration navigationConfiguration) {
        this.publishingService = publishingService;
        this.navigationConfiguration = navigationConfiguration;
    }

    public void registerDelegate(HomeControllerDelegate homeControllerDelegate) {
        this.homeControllerDelegate = homeControllerDelegate;
    }

    public void setupCommandController(ProgressController... progressControllers) {
        clearEvents();
        commandController = new CommandController(getCommandArray());
        commandController.setProgressController(new ContainerProgressController(progressControllers));
        commandController.setDelegate(this);
        commandController.start();
    }

    private Command[] getCommandArray() {
        List<Command> commands = Lists.newArrayList();
        for (Sport sport : getHighLightSportList()) {
            commands.add(new HighLightsCommand(new HighlightsRequester(publishingService), sport, this));
        }
        return commands.toArray(new Command[commands.size()]);
    }

    public void cancelRequest() {
        if (commandController != null) {
            commandController.cancel();
        }
    }

    private List<Sport> getHighLightSportList() {
        return navigationConfiguration.getHomeSports();
    }

    @Override
    public void commandControllerDidFinish() {
        homeControllerDelegate.controllerDidFinishWithEvents(eventMap);
    }

    @Override
    public void commandControllerDidFinishWithError(RetrofitError networkError) {
        homeControllerDelegate.controllerDidFinishWithError(networkError);
    }

    @Override
    public void highlightsForSport(Sport sport, List<Event> events) {
        updateEvents(sport, events);
    }

    private void updateEvents(Sport sport, List<Event> events) {
        eventMap.put(sport, events);
    }

    private void clearEvents() {
        eventMap = Maps.newLinkedHashMap();
    }

    public interface HomeControllerDelegate {

        void controllerDidFinishWithEvents(Map<Sport, List<Event>> events);

        void controllerDidFinishWithError(RetrofitError retrofitError);
    }
}
