package com.sportingbet.sportsbook.mybets.cashout.model;

import com.google.common.collect.Lists;

import java.util.List;

import lombok.Getter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CashoutBetRequestType {

    @Getter
    private List<CashoutBetsRequest> cashoutBetRequests;

    public static CashoutBetRequestType create(CashoutBetsRequest... cashoutBetsRequest) {
        CashoutBetRequestType cashoutBetRequestType = new CashoutBetRequestType();
        cashoutBetRequestType.cashoutBetRequests = Lists.newArrayList(cashoutBetsRequest);
        return cashoutBetRequestType;
    }
}
