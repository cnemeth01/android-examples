package com.sportingbet.sportsbook.content.general.helpers;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.general.adapter.AbstractRecyclerViewAdapter;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class FragmentStateHelper {

    private final AbstractRecyclerViewAdapter abstractRecyclerViewAdapter;

    public FragmentStateHelper(AbstractRecyclerViewAdapter abstractRecyclerViewAdapter) {
        this.abstractRecyclerViewAdapter = abstractRecyclerViewAdapter;
    }

    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            abstractRecyclerViewAdapter.setList(savedInstanceState.getParcelableArrayList(List.class.getName()));
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(List.class.getName(), Lists.newArrayList(abstractRecyclerViewAdapter.getItems()));
    }
}
