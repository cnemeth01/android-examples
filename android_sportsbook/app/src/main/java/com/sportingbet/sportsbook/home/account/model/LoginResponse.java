package com.sportingbet.sportsbook.home.account.model;

import hrisey.Parcelable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@Parcelable
@NoArgsConstructor
public class LoginResponse implements android.os.Parcelable {

    @Getter
    private CustomerDetails customerDetails;

    @Getter
    @Setter
    private BalanceDetails balanceDetails;
}
