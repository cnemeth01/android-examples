package com.sportingbet.sportsbook.general.ui;

import android.content.Context;
import android.os.Handler;
import android.util.TypedValue;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.sportingbet.sportsbook.R;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ViewHelper {

    public static float pxFromDp(Context context, int size) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, context.getResources().getDisplayMetrics());
    }

    // FIXME
    public static void runAfterTransition(Runnable runnable) {
        new Handler().postDelayed(runnable, 650);
    }

    public static Animation blinkAnimation(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.blinking);
    }
}
