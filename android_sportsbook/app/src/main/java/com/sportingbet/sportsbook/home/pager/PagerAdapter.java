package com.sportingbet.sportsbook.home.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.sportingbet.sportsbook.general.pager.FragmentStatePagerAdapter;
import com.sportingbet.sportsbook.home.navigation.configuration.NavigationConfiguration;
import com.sportingbet.sportsbook.home.pager.fragment.AbstractNavigationFragment;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {

    private final FragmentManager fragmentManager;
    private final NavigationConfiguration navigationConfiguration;

    private final List<AbstractNavigationFragment> fragments = new ArrayList<>();

    private int initialPagerSize;

    @Getter
    private Long dynamicSportIdentifier;

    public PagerAdapter(FragmentManager fragmentManager, NavigationConfiguration navigationConfiguration) {
        super(fragmentManager);
        this.fragmentManager = fragmentManager;
        this.navigationConfiguration = navigationConfiguration;
    }

    public void initialize() {
        fragments.addAll(navigationConfiguration.createNavigationFragments());
        if (fragmentManager.getFragments() != null) {
            for (Fragment fragment : fragmentManager.getFragments()) {
                if (fragment != null) {
                    replaceFragment((AbstractNavigationFragment) fragment);
                }
            }
        }

        initialPagerSize = navigationConfiguration.getNavigationFragmentsCount();
    }

    public void setDynamicSportIdentifier(long dynamicSportIdentifier) {
        this.dynamicSportIdentifier = dynamicSportIdentifier;
        if (fragments.size() <= initialPagerSize) {
            prepareSportNavigationFragment(dynamicSportIdentifier);
            notifyDataSetChanged();
        }
    }

    private void replaceFragment(AbstractNavigationFragment abstractNavigationFragment) {
        int index = getFragmentIndexWithName((abstractNavigationFragment).getTitle());
        if (index < fragments.size()) {
            fragments.set(index, abstractNavigationFragment);
        } else {
            fragments.add(abstractNavigationFragment);
        }
    }

    private int getFragmentIndexWithName(String title) {
        for (int index = 0; index < fragments.size(); index++) {
            if (title.equals(fragments.get(index).getTitle())) {
                return index;
            }
        }
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragments.get(position).getTitle();
    }

    @Override
    public AbstractNavigationFragment getItem(int arg0) {
        return fragments.get(arg0);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public int getPositionFromId(long identifier) {
        for (int position = 0; position < getCount(); position++) {
            if (getItemIdentifier(position) == identifier) {
                return position;
            }
        }
        prepareSportNavigationFragment(identifier);
        notifyDataSetChanged();
        return initialPagerSize;
    }

    private void prepareSportNavigationFragment(long identifier) {
        if (getCount() > initialPagerSize) {
            fragments.remove(initialPagerSize);
        }
        if (dynamicSportIdentifier != null && dynamicSportIdentifier != identifier) {
            removeFragmentSavedState();
        }
        dynamicSportIdentifier = identifier;
        fragments.add(navigationConfiguration.createSportNavigationFragment(identifier));
    }

    public long getItemIdentifier(int position) {
        return fragments.get(position).getIdentifier();
    }

    @Override
    public int getItemPosition(Object object) {
        return fragmentExist((AbstractNavigationFragment) object) ? POSITION_UNCHANGED : POSITION_NONE;
    }

    private boolean fragmentExist(AbstractNavigationFragment navigationFragment) {
        return fragments.contains(navigationFragment);
    }
}
