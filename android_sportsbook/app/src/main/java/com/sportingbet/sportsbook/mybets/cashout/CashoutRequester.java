package com.sportingbet.sportsbook.mybets.cashout;

import com.sportingbet.sportsbook.general.network.CallbackWrapper;
import com.sportingbet.sportsbook.general.network.requesters.Requester;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.network.services.BetService;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetRequestType;
import com.sportingbet.sportsbook.mybets.cashout.model.CashoutBetsResponseType;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class CashoutRequester extends Requester<CashoutBetsResponseType>{

    protected final BetService betService;

    public CashoutRequester(BetService betService) {
        this.betService = betService;
    }

    public void requestCashout(CashoutBetRequestType cashoutBetRequestType) {
        betService.cashOut(cashoutBetRequestType, getCallback());
    }
}
