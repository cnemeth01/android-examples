package com.sportingbet.sportsbook.general.network.error;

import android.content.Context;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.general.network.error.model.ServiceErrorGroup;
import com.sportingbet.sportsbook.general.ui.ViewHelper;

import retrofit.RetrofitError;

/**
 * Copyright 2015 Tomasz Morcinek. All rights reserved.
 */
public abstract class SnackbarErrorHandler extends BaseErrorHandler {

    public SnackbarErrorHandler(Context context) {
        super(context);
    }

    @Override
    public boolean handleError(RetrofitError networkError) {
        if (!super.handleError(networkError)) {
            Snackbar snackbar = Snackbar.with(context).text(provideErrorMessage(networkError));
            showSnackbar(snackbar);
            updateSnackbar(snackbar);
        }
        return true;
    }

    protected abstract void showSnackbar(Snackbar snackbar);

    protected String provideErrorMessage(RetrofitError networkError) {
        if (networkError.getResponse() != null) {
            ServiceErrorGroup serviceErrors = (ServiceErrorGroup) networkError.getBodyAs(ServiceErrorGroup.class);
            if (serviceErrors != null && serviceErrors.getFirstServiceError() != null) {
                return serviceErrors.getFirstServiceError().getLocalisedErrorMessage();
            }
        }
        return networkError.getLocalizedMessage();
    }

    protected void updateSnackbar(Snackbar snackbar) {
        TextView textView = (TextView) snackbar.findViewById(R.id.sb__text);
        textView.setMaxLines(4);
        snackbar.setMaxHeight((int) ViewHelper.pxFromDp(context, 144));
        snackbar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT);
    }
}
