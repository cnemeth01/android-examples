package com.sportingbet.sportsbook.content.fragments.event;

import android.text.TextUtils;

import com.sportingbet.sportsbook.content.general.requesters.PublishingRequester;
import com.sportingbet.sportsbook.general.network.services.PublishingService;
import com.sportingbet.sportsbook.model.event.Event;

import java.util.Collection;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class EventDetailsRequester extends PublishingRequester<Event> {

    public EventDetailsRequester(PublishingService publishingService) {
        super(publishingService);
    }

    public void requestEvent(long eventId) {
        publishingService.getEvent(eventId, getCallback());
    }

    public void requestEventWithSpecificBook(long eventId, Collection<Long> bookIds) {
        publishingService.getEventWithSpecificBook(eventId, TextUtils.join(",", bookIds.toArray()), getCallback());
    }
}
