package com.sportingbet.sportsbook.general.network.services;

import com.sportingbet.sportsbook.home.account.model.BalanceDetailsWrapper;
import com.sportingbet.sportsbook.home.account.model.LoginRequestBody;
import com.sportingbet.sportsbook.home.account.model.LoginResponse;
import com.sportingbet.sportsbook.home.account.model.LogoutRequestBody;
import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;
import com.sportingbet.sportsbook.mybets.model.MyBetType;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface AccountService {

    @POST("/login")
    void login(@Body LoginRequestBody loginRequestBody, Callback<LoginResponse> callback);

    @POST("/logout")
    void logout(@Body LogoutRequestBody logoutRequestBody, Callback<Response> callback);

    @GET("/balance")
    void getBalance(Callback<BalanceDetailsWrapper> callback);

    @GET("/account/mybets/{betType}")
    void getMyBets(@Path("betType") MyBetType betType, Callback<MyBetsResponse> callback);
}
