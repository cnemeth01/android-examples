package com.sportingbet.sportsbook.betslip.receipt.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.betslip.helpers.StakeHelper;
import com.sportingbet.sportsbook.betslip.model.BetSelection;
import com.sportingbet.sportsbook.betslip.model.BetSelectionState;
import com.sportingbet.sportsbook.betslip.model.StakeInformation;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBet;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetResponse;
import com.sportingbet.sportsbook.betslip.placebets.model.PlaceBetType;
import com.sportingbet.sportsbook.betslip.receipt.adapter.handlers.FooterViewHolderHandler;
import com.sportingbet.sportsbook.betslip.receipt.adapter.handlers.HeaderViewHolderHandler;
import com.sportingbet.sportsbook.betslip.receipt.adapter.handlers.WarningViewHolderHandler;
import com.sportingbet.sportsbook.betslip.receipt.adapter.model.BetslipReceiptFooter;
import com.sportingbet.sportsbook.betslip.receipt.adapter.model.BetslipReceiptHeader;
import com.sportingbet.sportsbook.general.adapter.handlers.AbstractHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.adapter.handlers.ViewHolderHandler;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class BetslipReceiptAdapter extends AbstractHandlersRecyclerViewAdapter implements ViewHolderHandler<PlaceBet, BetslipReceiptAdapter.ViewHolder> {

    private final PlaceBetResponse placeBetResponse;

    private final StakeInformation stakeInformation;

    public BetslipReceiptAdapter(Context context, View.OnClickListener onClickListener, PlaceBetResponse placeBetResponse, StakeInformation stakeInformation) {
        super(context);
        this.placeBetResponse = placeBetResponse;
        this.stakeInformation = stakeInformation;
        addViewHolderHandler(PlaceBet.class, this);
        addViewHolderHandler(String.class, new WarningViewHolderHandler(context));
        addViewHolderHandler(BetslipReceiptHeader.class, new HeaderViewHolderHandler(context));
        addViewHolderHandler(BetslipReceiptFooter.class, new FooterViewHolderHandler(context, onClickListener));
        setList(createItems());
    }

    private List<Object> createItems() {
        List<Object> items = Lists.newArrayList();
        items.add(createBetslipReceiptHeader(placeBetResponse));
        items.addAll(placeBetResponse.getSuccessfulBets());
        List<PlaceBet> failedBets = placeBetResponse.getFailedBets();
        if (!failedBets.isEmpty()) {
            items.add(context.getString(R.string.betslip_receipt_not_placed_bets));
            items.addAll(failedBets);
        }
        items.add(createBetslipReceiptFooter());
        return items;
    }

    private BetslipReceiptHeader createBetslipReceiptHeader(PlaceBetResponse placeBetResponse) {
        if (placeBetResponse.hasFailedBets()) {
            return new BetslipReceiptHeader(R.string.betslip_receipt_message_partialy_succesful, true);
        } else {
            return new BetslipReceiptHeader(R.string.betslip_receipt_message_all_successful, false);
        }
    }

    private BetslipReceiptFooter createBetslipReceiptFooter() {
        return new BetslipReceiptFooter(placeBetResponse.getTotalPotentialReturns(), stakeInformation.getUserCurrency());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(layoutInflater.inflate(R.layout.betreceipt_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, PlaceBet object) {
        setupViews(holder, object.getBetType());
        setupSelectionsLayout(holder, object.getBetType());
        setupWarningViews(holder, object.getError() != null);
        if (object.getError() == null) {
            setupPotentialReturnText(holder, object);
            setupReferenceNumber(holder, object);
        } else {
            setupWarningText(holder, object);
        }
    }

    private void setupPotentialReturnText(ViewHolder holder, PlaceBet object) {
        StakeHelper.setupPotentialReturnView(context, holder.totalPayout, object.getBetType().getPotentialReturn(), stakeInformation.getUserCurrency());
    }

    private void setupWarningViews(ViewHolder holder, boolean hasError) {
        holder.warningIndicator.setVisibility(hasError ? View.VISIBLE : View.GONE);
        holder.warningText.setVisibility(hasError ? View.VISIBLE : View.GONE);
        holder.totalPayoutLayout.setVisibility(hasError ? View.GONE : View.VISIBLE);
        holder.referenceNumberLayout.setVisibility(hasError ? View.GONE : View.VISIBLE);
        holder.itemView.setBackgroundResource(hasError ? R.color.warning_background : R.color.backgroundColor);
    }

    private void setupWarningText(ViewHolder holder, PlaceBet object) {
        holder.warningText.setText(object.getError().getLocalisedErrorMessage());
    }

    private void setupViews(ViewHolder holder, PlaceBetType placeBetType) {
        holder.title.setText(placeBetType.getBetTypeName());
        StakeHelper.setupStakeView(context, holder.totalStake, placeBetType.getTotalStake(), stakeInformation.getUserCurrency());
    }

    private void setupReferenceNumber(ViewHolder holder, PlaceBet placeBet) {
        holder.referenceNumber.setText(Long.toString(placeBet.getBetReferenceId()));
    }

    private void setupSelectionsLayout(ViewHolder holder, PlaceBetType placeBetType) {
        ViewGroup selectionsLayout = holder.selectionsLayout;
        selectionsLayout.removeAllViews();
        List<BetSelection> selections = placeBetResponse.getSelectionsForPlaceBetType(placeBetType);
        for (BetSelection betSelection : selections) {
            BetSelectionState acceptedState = betSelection.getAcceptedState();
            setupSelection(selectionsLayout, placeBetType, acceptedState);
        }
    }

    private void setupSelection(ViewGroup selectionsLayout, PlaceBetType placeBetType, BetSelectionState acceptedState) {
        SelectionViewHolder selectionViewHolder = onCreateSelectionViewHolder(selectionsLayout);
        selectionViewHolder.event.setText(acceptedState.getEventName());
        selectionViewHolder.odds.setText(acceptedState.getDisplayOdds());
        selectionViewHolder.book.setText(acceptedState.getBookName());
        selectionViewHolder.selection.setText(acceptedState.getSelectionName());
        setupEachWay(placeBetType, acceptedState, selectionViewHolder);
    }

    private SelectionViewHolder onCreateSelectionViewHolder(ViewGroup selectionsLayout) {
        View view = layoutInflater.inflate(R.layout.betreceipt_item_selection, selectionsLayout, false);
        selectionsLayout.addView(view);
        return new SelectionViewHolder(view);
    }

    private void setupEachWay(PlaceBetType placeBetType, BetSelectionState acceptedState, SelectionViewHolder selectionViewHolder) {
        selectionViewHolder.eachWay.setVisibility(placeBetType.getIsEachwayBet() ? View.VISIBLE : View.GONE);
        if (placeBetType.getIsEachwayBet()) {
            StakeHelper.setupEachWayView(context, selectionViewHolder.eachWay, acceptedState);
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView title;
        private final ViewGroup selectionsLayout;
        private final TextView totalStake;
        private final TextView totalPayout;
        private final View totalPayoutLayout;
        private final TextView referenceNumber;
        private final View referenceNumberLayout;
        private final View warningIndicator;
        private final TextView warningText;

        private ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            selectionsLayout = (ViewGroup) view.findViewById(R.id.selections_layout);
            totalStake = (TextView) view.findViewById(R.id.total_stake);
            totalPayout = (TextView) view.findViewById(R.id.total_payout);
            totalPayoutLayout = view.findViewById(R.id.total_payout_layout);
            referenceNumber = (TextView) view.findViewById(R.id.reference_number);
            referenceNumberLayout = view.findViewById(R.id.reference_number_layout);
            warningIndicator = view.findViewById(R.id.warning_indicator);
            warningText = (TextView) view.findViewById(R.id.warning_text);
        }
    }

    private class SelectionViewHolder {

        private final TextView selection;
        private final TextView odds;
        private final TextView book;
        private final TextView event;
        private final TextView eachWay;

        private SelectionViewHolder(View view) {
            selection = (TextView) view.findViewById(R.id.selection);
            odds = (TextView) view.findViewById(R.id.odds);
            book = (TextView) view.findViewById(R.id.book);
            event = (TextView) view.findViewById(R.id.event);
            eachWay = (TextView) view.findViewById(R.id.each_way);
        }
    }
}
