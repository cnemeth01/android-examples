package com.sportingbet.sportsbook;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import dagger.ObjectGraph;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportsbookApplication extends Application {

    protected ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        setupApplication();
    }

    protected void setupApplication() {
        if (BuildConfig.CRASHLYTICS_ENABLED) {
            Crashlytics.start(this);
        }
        objectGraph = ObjectGraph.create(Modules.create(this));
    }

    public void inject(Object object) {
        objectGraph.inject(object);
    }

    public ObjectGraph getApplicationGraph() {
        return objectGraph;
    }
}

