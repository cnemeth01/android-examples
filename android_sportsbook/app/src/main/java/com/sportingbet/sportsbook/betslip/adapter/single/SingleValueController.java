package com.sportingbet.sportsbook.betslip.adapter.single;

import android.view.View;

import com.google.common.collect.Maps;
import com.sportingbet.sportsbook.betslip.adapter.BetTypeValueController;
import com.sportingbet.sportsbook.betslip.controller.bets.BetsValueContainer;
import com.sportingbet.sportsbook.betslip.model.BetSelection;
import com.sportingbet.sportsbook.betslip.model.BetslipInformation;
import com.sportingbet.sportsbook.betslip.model.types.BetType;
import com.sportingbet.sportsbook.betslip.picker.adapter.BetTypePickerAdapter;
import com.sportingbet.sportsbook.betslip.picker.adapter.PickerAdapter;
import com.sportingbet.sportsbook.betslip.picker.adapter.SinglesPickerAdapter;
import com.sportingbet.sportsbook.general.ui.picker.StakePicker;

import java.util.Map;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SingleValueController extends BetTypeValueController {

    private final SinglesPickerAdapter singlePickerAdapter = new SinglesPickerAdapter(this);

    private BetslipInformation betslipInformation;

    public SingleValueController(BetsValueContainer betsValueContainer) {
        super(betsValueContainer);
    }

    public int getBetsCount() {
        return pickerAdapters.size();
    }

    public void unregisterStakePicker(StakePicker stakePicker, BetType betType) {
        stakePicker.setVisibility(View.GONE);
        pickerAdapters.remove(betType.getSelectionId());
    }

    public void registerSinglesStakePicker(StakePicker stakePicker) {
        singlePickerAdapter.setup(stakePicker, pickerValueProvider);
    }

    @Override
    public void setup(BetslipInformation betslipInformation) {
        super.setup(betslipInformation);
        this.betslipInformation = betslipInformation;
        Map<Object, BetTypePickerAdapter> betTypePickerAdapters = Maps.newHashMap();
        for (BetSelection betSelection : betslipInformation.getBetSelections()) {
            BetTypePickerAdapter pickerAdapter = pickerAdapters.get(betSelection.getSelectionId());
            if (pickerAdapter != null) {
                betTypePickerAdapters.put(betSelection.getSelectionId(), pickerAdapter);
            }
        }
        pickerAdapters.clear();
        pickerAdapters.putAll(betTypePickerAdapters);
    }

    @Override
    protected void preparePickerAdapter(StakePicker stakePicker, View eachWayView, BetType betType) {
        BetTypePickerAdapter pickerAdapter = pickerAdapters.get(betType.getSelectionId());
        if (pickerAdapter == null) {
            pickerAdapter = new BetTypePickerAdapter(this);
            pickerAdapters.put(betType.getSelectionId(), pickerAdapter);
        }
        pickerAdapter.setup(stakePicker, pickerValueProvider, betType, getPaymentMark(betType));
        pickerAdapter.setupEachView(eachWayView);
        updateBetTypePickerAdapter(pickerAdapter);
    }

    private String getPaymentMark(BetType betType) {
        BetSelection betSelection = betslipInformation.getBetSelectionForId(betType.getSelectionId());
        if (betSelection.isSPSelection()) {
            return betSelection.getAcceptedState().getDisplayOdds();
        }
        return null;
    }

    @Override
    public void pickerAdapterUpdated(PickerAdapter pickerAdapter) {
        if (pickerAdapter instanceof SinglesPickerAdapter) {
            for (BetTypePickerAdapter betTypePickerAdapter : pickerAdapters.values()) {
                betTypePickerAdapter.updateValue(pickerAdapter.getValue());
                updateBetsValueContainer(betTypePickerAdapter);
            }
        } else {
            singlePickerAdapter.updateValue(null);
            updateBetsValueContainer((BetTypePickerAdapter) pickerAdapter);
        }
        super.pickerAdapterUpdated(pickerAdapter);
    }
}
