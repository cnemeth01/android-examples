package com.sportingbet.sportsbook.splash.commands;

import com.sportingbet.sportsbook.general.commands.NetworkCommand;
import com.sportingbet.sportsbook.sports.configuration.SportConfig;
import com.sportingbet.sportsbook.sports.configuration.SportConfigPersistentStore;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class SportConfigCommand extends NetworkCommand<List<SportConfig>> {

    private final SportConfigRequester sportConfigRequester;

    private final SportConfigPersistentStore sportConfigPersistentStore;

    public SportConfigCommand(SportConfigPersistentStore sportConfigPersistentStore, SportConfigRequester sportConfigRequester) {
        this.sportConfigPersistentStore = sportConfigPersistentStore;
        this.sportConfigRequester = sportConfigRequester;
    }

    @Override
    public void execute() {
        sportConfigRequester.requestSportConfigs(this);
    }

    @Override
    public void success(List<SportConfig> object) {
        sportConfigPersistentStore.setSportConfigs(object);
        super.success(object);
    }
}
