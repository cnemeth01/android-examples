package com.sportingbet.sportsbook.home.betslip;

public enum BetslipButtonEvent {
    HIDE_BUTTON,
    SHOW_BUTTON
}
