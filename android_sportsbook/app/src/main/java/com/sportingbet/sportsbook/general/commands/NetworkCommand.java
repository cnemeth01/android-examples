package com.sportingbet.sportsbook.general.commands;

import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;

import lombok.Setter;
import retrofit.RetrofitError;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class NetworkCommand<T> implements Command<RetrofitError>, NetworkResponseListener<T> {

    @Setter
    private CommandProgressDelegate<RetrofitError> delegate;

    @Override
    public void success(T object) {
        if (delegate != null) {
            delegate.commandDidFinish(this);
        }
    }

    @Override
    public void failure(RetrofitError error) {
        if (delegate != null) {
            delegate.commandDidFinishWithError(this, error);
        }
    }

    @Override
    public void cancel() {
        delegate = null;
    }
}
