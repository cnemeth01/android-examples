package com.sportingbet.sportsbook.mybets.model;

import com.google.common.base.Objects;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBet {

    @Getter
    private Long betReference;

    @Getter
    private Double totalStake;

    @Setter
    @Getter
    private MyBetStatus betStatus;

    @Getter
    private String betType;

    @Setter
    @Getter
    private Double potentialReturnAmount;

    @Getter
    private Double returnAmount;

    @Setter
    @Getter
    private Double cashoutAmount;

    @Getter
    private MyBetSelections selections;

    @Override
    public boolean equals(Object o) {
        return o instanceof MyBet && Objects.equal(betReference, ((MyBet) o).getBetReference());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(betReference);
    }
}
