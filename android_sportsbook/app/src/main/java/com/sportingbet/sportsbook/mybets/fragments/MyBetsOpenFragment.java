package com.sportingbet.sportsbook.mybets.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.mybets.cashout.CashoutController;
import com.sportingbet.sportsbook.mybets.fragments.controller.MyBetsResponseController;
import com.sportingbet.sportsbook.mybets.fragments.controller.MyBetsTabController;
import com.sportingbet.sportsbook.mybets.model.MyBet;
import com.sportingbet.sportsbook.mybets.model.MyBetsResponse;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class MyBetsOpenFragment extends MyBetsFragment implements MyBetsResponseController.Delegate {

    @Inject
    CashoutController cashoutController;

    private MyBetsResponseController myBetsResponseController;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.mybets_open_layout;
    }

    @Override
    protected String getEmptyScreenTitle() {
        return getString(myBetsResponseController.getEmptyScreenTitle());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupResponseController();
        setupCashOutController();
    }

    private void setupResponseController() {
        myBetsResponseController = new MyBetsResponseController(myBetsResponseFilter, new MyBetsTabController(getView()));
        myBetsResponseController.setDelegate(this);
    }

    private void setupCashOutController() {
        cashoutController.register(getAdapter(), progressBarController);
        getAdapter().setCashOutButtonClickListener(cashoutController);
    }

    @Override
    protected List<MyBet> prepareMyBets(MyBetsResponse object) {
        return myBetsResponseController.prepareMyBets(object);
    }

    @Override
    public void notifyDataSetChanged() {
        onPageSelected();
    }

    @Override
    public void notifyMyBetsChanged(List<MyBet> myBets) {
        setupMyBets(myBets);
    }
}
