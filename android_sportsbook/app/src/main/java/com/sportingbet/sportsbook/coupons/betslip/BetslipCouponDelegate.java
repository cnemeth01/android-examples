package com.sportingbet.sportsbook.coupons.betslip;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface BetslipCouponDelegate {

    void register(View view, RecyclerView.Adapter adapter);

    void onBetClicked(View view, Event event, Book book, Selection selection);

    boolean betslipContainsSelection(Selection selection);

    boolean loadingSelection(Selection selection);
}
