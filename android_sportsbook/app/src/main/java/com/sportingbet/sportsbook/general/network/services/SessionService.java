package com.sportingbet.sportsbook.general.network.services;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Query;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface SessionService {

    @GET("/session/start")
    void getActiveSessionId(@Query("domainId") String domainId, @Header("APPID") String appId, Callback<Response> callback);

    @GET("/session/temp")
    void getTempSessionId(Callback<Response> callback);
}
