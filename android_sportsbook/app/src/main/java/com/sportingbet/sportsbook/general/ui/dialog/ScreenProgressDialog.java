package com.sportingbet.sportsbook.general.ui.dialog;

import android.app.Dialog;
import android.content.Context;

import com.sportingbet.sportsbook.R;

/**
 * Copyright 2014 Tomasz Morcinek. All rights reserved.
 */
public class ScreenProgressDialog extends Dialog {

    public ScreenProgressDialog(Context context) {
        super(context, R.style.DimmedDialogTheme);
        getWindow().setContentView(R.layout.progress_bar);
        setCancelable(false);
    }
}
