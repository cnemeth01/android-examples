package com.sportingbet.sportsbook.coupons.model;

import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.book.WebCouponTemplate;
import com.sportingbet.sportsbook.model.book.selection.Selection;
import com.sportingbet.sportsbook.model.event.Event;

import java.util.List;

import lombok.NoArgsConstructor;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@NoArgsConstructor
public class BookCoupon extends BaseCoupon {

    public BookCoupon(Event event, Book book) {
        super(event, book);
    }

    @Override
    public WebCouponTemplate getWebCouponTemplate() {
        return getBook().getWebCouponTemplate();
    }

    @Override
    public List<Selection> getSelections() {
        return getBook().getFirstOffer().getSelections();
    }
}
