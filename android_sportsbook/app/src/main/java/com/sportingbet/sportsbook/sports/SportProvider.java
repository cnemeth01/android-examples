package com.sportingbet.sportsbook.sports;

import com.sportingbet.sportsbook.model.Sport;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public interface SportProvider {

    Sport getSportWithId(long sportId);
}
