package com.sportingbet.sportsbook.scoreboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.model.event.Scores;

import java.util.List;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ScoreboardHelper {

    private Context context;

    public ScoreboardHelper(Context context) {
        this.context = context;
    }

    public TextView getTextView(View view, int resourceId) {
        return (TextView) view.findViewById(resourceId);
    }

    public String getParticipantA(Event event) {
        String participantA = event.getInRunningData().getParticipantA();
        if (participantA.isEmpty()) {
            participantA = event.getFirstBook().getFirstOffer().getSelections().get(0).getName();
        }
        return participantA;
    }

    public String getParticipantB(Event event) {
        String participantA = event.getInRunningData().getParticipantB();
        if (participantA.isEmpty()) {
            participantA = event.getFirstBook().getFirstOffer().getSelections().get(1).getName();
        }
        return participantA;
    }

    public void setTeamIndicatorVisibility(View view, int teamIndicatorId, String participantPoints, int drawableId) {
        if (participantPoints != null && participantPoints.contains("*")) {
            ImageView indicatorView = (ImageView) view.findViewById(teamIndicatorId);
            indicatorView.setImageResource(drawableId);
        }
    }

    public void setupScores(View view, Scores scores) {
        ViewGroup scoresContainer = (ViewGroup) view.findViewById(R.id.scores_container);
        List<String> participantAScoreHistory = scores.getParticipantAScoreHistory();
        List<String> participantBScoreHistory = scores.getParticipantBScoreHistory();
        for (int i = 0; i < participantAScoreHistory.size(); i++) {
            setupScore(scoresContainer, participantAScoreHistory.get(i), participantBScoreHistory.get(i));
        }
    }

    private void setupScore(ViewGroup scoresContainer, String participantAScore, String participantBScore) {
        View scoreboardTennisScore = LayoutInflater.from(context).inflate(R.layout.scoreboard_score, scoresContainer, false);
        getTextView(scoreboardTennisScore, R.id.first_team_score).setText(participantAScore);
        getTextView(scoreboardTennisScore, R.id.second_team_score).setText(participantBScore);
        scoresContainer.addView(scoreboardTennisScore);
    }

    public void setupTotalSetScore(View view, Scores scores) {
        setupSetScore(view, R.id.first_team_score, scores.getParticipantATotalSetScore());
        setupSetScore(view, R.id.second_team_score, scores.getParticipantBTotalSetScore());
    }

    public void setupTotalSetScore(View view, Scores scores, int title) {
        setupSetScore(view, R.id.score_title, context.getString(title));
        setupTotalSetScore(view, scores);
    }

    private void setupSetScore(View view, int resourceId, String text) {
        TextView textView = getTextView(view, resourceId);
        textView.setText(text);
        textView.setTextColor(context.getResources().getColor(R.color.primaryColor));
    }

    public void setupCurrentSetScore(View view, Scores scores) {
        getTextView(view, R.id.first_team_score).setText(getFormattedPoints(scores.getParticipantACurrentSetScore()));
        getTextView(view, R.id.second_team_score).setText(getFormattedPoints(scores.getParticipantBCurrentSetScore()));
    }

    public void setupCurrentSetScore(View view, Scores scores, int title) {
        getTextView(view, R.id.score_title).setText(title);
        setupCurrentSetScore(view, scores);
    }

    public void setupCurrentGameScore(View view, Scores scores, int title) {
        getTextView(view, R.id.score_title).setText(title);
        getTextView(view, R.id.first_team_score).setText(getFormattedPoints(scores.getParticipantACurrentGameScore()));
        getTextView(view, R.id.second_team_score).setText(getFormattedPoints(scores.getParticipantBCurrentGameScore()));
    }

    private String getFormattedPoints(String participantPoints) {
        if (participantPoints == null) {
            return "";
        }
        return participantPoints.replace("*", "");
    }
}
