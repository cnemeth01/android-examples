package com.sportingbet.sportsbook.content.fragments.event;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.google.common.collect.Sets;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.content.general.controllers.refresh.RefreshController;
import com.sportingbet.sportsbook.content.general.fragments.PublishingContentFragment;
import com.sportingbet.sportsbook.content.general.helpers.BundleHelper;
import com.sportingbet.sportsbook.coupons.CouponHandlerFactory;
import com.sportingbet.sportsbook.coupons.model.Coupon;
import com.sportingbet.sportsbook.general.adapter.handlers.TypedHandlersRecyclerViewAdapter;
import com.sportingbet.sportsbook.general.network.progress.ItemProgressBarController;
import com.sportingbet.sportsbook.general.network.progress.ProgressControllerAdapter;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.ui.recyclerview.DividerItemDecoration;
import com.sportingbet.sportsbook.model.book.Book;
import com.sportingbet.sportsbook.model.event.Event;
import com.sportingbet.sportsbook.scoreboard.ScoreboardFactory;

import java.util.Collection;
import java.util.Set;

import javax.inject.Inject;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class EventDetailsFragment extends PublishingContentFragment<Event, Coupon> implements View.OnClickListener, RefreshController.RefreshControllerListener {

    @Inject
    RefreshController refreshController;

    @Inject
    CouponHandlerFactory couponHandlerFactory;

    @Inject
    ScoreboardFactory scoreboardFactory;

    @Inject
    @Getter
    EventDetailsRequester networkRequester;

    protected BundleHelper<Event> bundleHelper;

    protected final MarginController marginController;

    private ViewGroup scoreboardContainer;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.event_fragment;
    }

    @Override
    protected final TypedHandlersRecyclerViewAdapter<Coupon> createAdapter() {
        BooksAdapter adapter = new BooksAdapter(getActivity(), couponHandlerFactory);
        couponHandlerFactory.getBetslipCouponDelegate().register(getView(), adapter);
        return adapter;
    }

    @Override
    protected BooksAdapter getAdapter() {
        return (BooksAdapter) super.getAdapter();
    }

    public static EventDetailsFragment newInstance(Event event) {
        return newInstance(event, 0);
    }

    public static EventDetailsFragment newInstance(Event event, int step) {
        EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
        eventDetailsFragment.setArguments(eventDetailsFragment.bundleHelper.createBundle(event));
        eventDetailsFragment.marginController.setMarginStep(step);
        return eventDetailsFragment;
    }

    public EventDetailsFragment() {
        bundleHelper = new BundleHelper<>(Event.class);
        marginController = new MarginController(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView(view);

        if (savedInstanceState != null) {
            bundleHelper.onRestoreInstanceState(savedInstanceState);
            updateEvent(bundleHelper.get());
        } else {
            updateEvent(bundleHelper.get());
            getAdapter().clearBooks();
        }

        setupTitle(view);

        refreshController.registerRefreshController(this);
    }

    @Override
    protected DividerItemDecoration getDividerItemDecoration() {
        return new DividerItemDecoration(getActivity(), R.drawable.by_event_divider);
    }

    protected void setupView(View view) {
        marginController.prepareMargin(view);
        view.findViewById(R.id.close_button).setOnClickListener(this);
        scoreboardContainer = (ViewGroup) view.findViewById(R.id.scoreboard_container);
    }

    private void setupTitle(View view) {
        ((TextView) view.findViewById(R.id.title)).setText(bundleHelper.get().getName());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        bundleHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onFragmentSelected() {
        refreshController.restart();
        super.onFragmentSelected();
    }

    @Override
    public void onFragmentDisabled() {
        super.onFragmentDisabled();
        refreshController.cancel();
    }

    @Override
    protected ProgressController[] getProgressControllers() {
        return new ProgressController[]{refreshProgressController, progressBarController, refreshController};
    }

    @Override
    public void success(Event object) {
        bundleHelper.set(object);
        updateEvent(object);
    }

    protected void updateEvent(Event object) {
        scoreboardFactory.prepareScoreboardForEvent(scoreboardContainer, object);
        getAdapter().setEvent(object);
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        Set<Book> openedBooks = getAdapter().getBooks();
        if (openedBooks.isEmpty()) {
            networkRequester.requestEvent(bundleHelper.get().getId());
        } else {
            networkRequester.requestEventWithSpecificBook(bundleHelper.get().getId(), getBookIds(openedBooks));
        }
    }

    @Override
    public void onItemClicked(View view, Object item) {
        Book book = (Book) item;
        BooksAdapter adapter = getAdapter();
        if (adapter.containsBook(book)) {
            adapter.closeBook(book);
        } else {
            adapter.addBook(book);
            openBook(view, book, adapter.getBooks());
        }
    }

    private void openBook(View view, final Book book, Set<Book> openedBooks) {
        networkRequester.prepareRequest(new ItemProgressBarController(view), new ProgressControllerAdapter() {
            @Override
            public void postExecuteWithSuccess(boolean success) {
                if (success) {
                    scrollToItem(book);
                }
            }
        });
        networkRequester.requestEventWithSpecificBook(bundleHelper.get().getId(), getBookIds(openedBooks));
    }

    private Collection<Long> getBookIds(Set<Book> books) {
        Set<Long> bookIds = Sets.newHashSet();
        for (Book book : books) {
            bookIds.add(book.getId());
        }
        return bookIds;
    }

    @Override
    public void onClick(View v) {
        getActivity().onBackPressed();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (!enter) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_bottom);
        }
        return super.onCreateAnimation(transit, true, nextAnim);
    }
}
