package com.sportingbet.sportsbook.model.book;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
@JsonDeserialize(using = WebCouponTemplateDeserializer.class)
public enum WebCouponTemplate {
    CPNTEAMTIE, CPNPLAYTIE, CPN2BALLS,  // (1,x,2)
    CPNTEAMNOTIE, CPNPLAYNOTIE,         // (1,2)
    CPNTOTALS, CPNOVERUNDER,            // (over 0.5 , under 0.5 )
    CPN3BALLS,                          // (1,x,2), 3 sections of text
    UNKNOWN
}

class WebCouponTemplateDeserializer extends JsonDeserializer<WebCouponTemplate> {

    @Override
    public WebCouponTemplate deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String value = p.getText();
        if (value.isEmpty()) {
            return WebCouponTemplate.UNKNOWN;
        }
        return WebCouponTemplate.valueOf(value);
    }
}
