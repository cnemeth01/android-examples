package com.sportingbet.sportsbook.home.account.logout;

import com.sportingbet.sportsbook.home.account.model.LogoutRequestBody;
import com.sportingbet.sportsbook.general.network.CallbackWrapper;
import com.sportingbet.sportsbook.general.network.response.NetworkResponseListener;
import com.sportingbet.sportsbook.general.network.response.ProgressController;
import com.sportingbet.sportsbook.general.network.services.AccountService;

import retrofit.client.Response;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class LogoutRequester {

    protected final AccountService accountService;

    public LogoutRequester(AccountService accountService) {
        this.accountService = accountService;
    }

    public void requestLogout(NetworkResponseListener<Response> responseListener, ProgressController... progressController) {
        accountService.logout(LogoutRequestBody.NORMAL, new CallbackWrapper<>(responseListener, progressController));
    }
}
