package com.sportingbet.sportsbook.general.network.error.model;

import com.google.common.collect.Lists;

import java.util.List;

import lombok.Getter;

/**
 * Created by Tomasz Morcinek.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public class ServiceErrorGroup {

    @Getter
    private List<ServiceError> errors = Lists.newArrayList();

    public ServiceError getFirstServiceError() {
        if (errors.isEmpty()) {
            return null;
        }
        return errors.get(0);
    }
}
