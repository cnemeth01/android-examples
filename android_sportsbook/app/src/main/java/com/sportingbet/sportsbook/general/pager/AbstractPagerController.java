package com.sportingbet.sportsbook.general.pager;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.sportingbet.sportsbook.R;
import com.sportingbet.sportsbook.home.pager.fragment.PageNavigation;

/**
 * Created by Tamas Marton.
 * Copyright (c) 2015 SportingBet. All rights reserved.
 */
public abstract class AbstractPagerController implements ViewPager.OnPageChangeListener {

    protected final FragmentStatePagerAdapter fragmentPagerAdapter;

    protected ViewPager viewPager;

    private Integer selectedItemPosition;

    public AbstractPagerController(FragmentStatePagerAdapter fragmentPagerAdapter) {
        this.fragmentPagerAdapter = fragmentPagerAdapter;
    }

    public void registerActivity(FragmentActivity activity) {
        setupViewPager(activity);
        setupPagerTabs(activity);
    }

    private void setupViewPager(Activity activity) {
        viewPager = (ViewPager) activity.findViewById(R.id.view_pager);
        viewPager.setAdapter(fragmentPagerAdapter);
    }

    private void setupPagerTabs(Activity activity) {
        PagerSlidingTabStrip pageIndicator = (PagerSlidingTabStrip) activity.findViewById(R.id.view_pager_indicator);
        pageIndicator.setViewPager(viewPager);
        pageIndicator.setOnPageChangeListener(this);
    }

    protected void updateStartPosition() {
        selectedItemPosition = viewPager.getCurrentItem();
    }

    protected Integer getSelectedItemPosition() {
        return selectedItemPosition;
    }

    @Override
    public final void onPageScrollStateChanged(int state) {
        int currentItem = viewPager.getCurrentItem();
        if (state == ViewPager.SCROLL_STATE_SETTLING && isSelectedPositionAndChanged(currentItem)) {
            updatePreviousFragment();
            selectedItemPosition = null;
        }
        if (state == ViewPager.SCROLL_STATE_IDLE && isStartingPositionOrChanged(currentItem)) {
            selectedItemPosition = currentItem;
            updateNewFragment();
        }
    }

    private boolean isSelectedPositionAndChanged(int currentItem) {
        return selectedItemPosition != null && selectedItemPosition != currentItem;
    }

    protected boolean isStartingPositionOrChanged(int newItemPosition) {
        return selectedItemPosition == null || selectedItemPosition != newItemPosition;
    }

    private void updatePreviousFragment() {
        PageNavigation pageNavigation = (PageNavigation) fragmentPagerAdapter.getItem(selectedItemPosition);
        pageNavigation.onPageDisabled();
    }

    protected void updateNewFragment() {
        PageNavigation pageNavigation = (PageNavigation) fragmentPagerAdapter.getItem(selectedItemPosition);
        pageNavigation.onPageSelected();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }
}
