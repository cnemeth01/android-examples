// Code generated by dagger-compiler.  Do not edit.
package com.sportingbet.sportsbook.general.commands;

import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

/**
 * A {@code Binding<CommandControllerTest>} implementation which satisfies
 * Dagger's infrastructure requirements including:
 *
 * Owning the dependency links between {@code CommandControllerTest} and its
 * dependencies.
 *
 * Being a {@code Provider<CommandControllerTest>} and handling creation and
 * preparation of object instances.
 *
 * Being a {@code MembersInjector<CommandControllerTest>} and handling injection
 * of annotated fields.
 */
public final class CommandControllerTest$$InjectAdapter extends Binding<CommandControllerTest>
    implements Provider<CommandControllerTest>, MembersInjector<CommandControllerTest> {
  private Binding<CommandController> commandController;
  private Binding<com.sportingbet.sportsbook.splash.commands.SessionRequester> sessionRequester;
  private Binding<com.sportingbet.sportsbook.splash.commands.SportsRequester> sportsRequester;
  private Binding<com.sportingbet.sportsbook.splash.commands.SportConfigRequester> sportConfigRequester;

  public CommandControllerTest$$InjectAdapter() {
    super("com.sportingbet.sportsbook.general.commands.CommandControllerTest", "members/com.sportingbet.sportsbook.general.commands.CommandControllerTest", NOT_SINGLETON, CommandControllerTest.class);
  }

  /**
   * Used internally to link bindings/providers together at run time
   * according to their dependency graph.
   */
  @Override
  @SuppressWarnings("unchecked")
  public void attach(Linker linker) {
    commandController = (Binding<CommandController>) linker.requestBinding("@javax.inject.Named(value=splash)/com.sportingbet.sportsbook.general.commands.CommandController", CommandControllerTest.class, getClass().getClassLoader());
    sessionRequester = (Binding<com.sportingbet.sportsbook.splash.commands.SessionRequester>) linker.requestBinding("com.sportingbet.sportsbook.splash.commands.SessionRequester", CommandControllerTest.class, getClass().getClassLoader());
    sportsRequester = (Binding<com.sportingbet.sportsbook.splash.commands.SportsRequester>) linker.requestBinding("com.sportingbet.sportsbook.splash.commands.SportsRequester", CommandControllerTest.class, getClass().getClassLoader());
    sportConfigRequester = (Binding<com.sportingbet.sportsbook.splash.commands.SportConfigRequester>) linker.requestBinding("com.sportingbet.sportsbook.splash.commands.SportConfigRequester", CommandControllerTest.class, getClass().getClassLoader());
  }

  /**
   * Used internally obtain dependency information, such as for cyclical
   * graph detection.
   */
  @Override
  public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> injectMembersBindings) {
    injectMembersBindings.add(commandController);
    injectMembersBindings.add(sessionRequester);
    injectMembersBindings.add(sportsRequester);
    injectMembersBindings.add(sportConfigRequester);
  }

  /**
   * Returns the fully provisioned instance satisfying the contract for
   * {@code Provider<CommandControllerTest>}.
   */
  @Override
  public CommandControllerTest get() {
    CommandControllerTest result = new CommandControllerTest();
    injectMembers(result);
    return result;
  }

  /**
   * Injects any {@code @Inject} annotated fields in the given instance,
   * satisfying the contract for {@code Provider<CommandControllerTest>}.
   */
  @Override
  public void injectMembers(CommandControllerTest object) {
    object.commandController = commandController.get();
    object.sessionRequester = sessionRequester.get();
    object.sportsRequester = sportsRequester.get();
    object.sportConfigRequester = sportConfigRequester.get();
  }

}
