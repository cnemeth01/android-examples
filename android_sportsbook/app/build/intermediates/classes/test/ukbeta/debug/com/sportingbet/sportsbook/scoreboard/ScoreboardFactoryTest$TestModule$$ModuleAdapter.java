// Code generated by dagger-compiler.  Do not edit.
package com.sportingbet.sportsbook.scoreboard;

import dagger.internal.ModuleAdapter;

/**
 * A manager of modules and provides adapters allowing for proper linking and
 * instance provision of types served by {@code @Provides} methods.
 */
public final class ScoreboardFactoryTest$TestModule$$ModuleAdapter extends ModuleAdapter<ScoreboardFactoryTest.TestModule> {
  private static final String[] INJECTS = { "members/com.sportingbet.sportsbook.scoreboard.ScoreboardFactoryTest", };
  private static final Class<?>[] STATIC_INJECTIONS = { };
  private static final Class<?>[] INCLUDES = { com.sportingbet.sportsbook.general.dagger.configuration.GeneralTestModule.class, };

  public ScoreboardFactoryTest$TestModule$$ModuleAdapter() {
    super(com.sportingbet.sportsbook.scoreboard.ScoreboardFactoryTest.TestModule.class, INJECTS, STATIC_INJECTIONS, true /*overrides*/, INCLUDES, true /*complete*/, false /*library*/);
  }
}
