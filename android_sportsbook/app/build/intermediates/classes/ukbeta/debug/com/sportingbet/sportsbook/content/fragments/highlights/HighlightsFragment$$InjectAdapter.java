// Code generated by dagger-compiler.  Do not edit.
package com.sportingbet.sportsbook.content.fragments.highlights;

import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

/**
 * A {@code Binding<HighlightsFragment>} implementation which satisfies
 * Dagger's infrastructure requirements including:
 *
 * Owning the dependency links between {@code HighlightsFragment} and its
 * dependencies.
 *
 * Being a {@code Provider<HighlightsFragment>} and handling creation and
 * preparation of object instances.
 *
 * Being a {@code MembersInjector<HighlightsFragment>} and handling injection
 * of annotated fields.
 */
public final class HighlightsFragment$$InjectAdapter extends Binding<HighlightsFragment>
    implements Provider<HighlightsFragment>, MembersInjector<HighlightsFragment> {
  private Binding<HighlightsRequester> networkRequester;
  private Binding<com.sportingbet.sportsbook.content.general.fragments.events.EventsContentFragment> supertype;

  public HighlightsFragment$$InjectAdapter() {
    super("com.sportingbet.sportsbook.content.fragments.highlights.HighlightsFragment", "members/com.sportingbet.sportsbook.content.fragments.highlights.HighlightsFragment", NOT_SINGLETON, HighlightsFragment.class);
  }

  /**
   * Used internally to link bindings/providers together at run time
   * according to their dependency graph.
   */
  @Override
  @SuppressWarnings("unchecked")
  public void attach(Linker linker) {
    networkRequester = (Binding<HighlightsRequester>) linker.requestBinding("com.sportingbet.sportsbook.content.fragments.highlights.HighlightsRequester", HighlightsFragment.class, getClass().getClassLoader());
    supertype = (Binding<com.sportingbet.sportsbook.content.general.fragments.events.EventsContentFragment>) linker.requestBinding("members/com.sportingbet.sportsbook.content.general.fragments.events.EventsContentFragment", HighlightsFragment.class, getClass().getClassLoader(), false, true);
  }

  /**
   * Used internally obtain dependency information, such as for cyclical
   * graph detection.
   */
  @Override
  public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> injectMembersBindings) {
    injectMembersBindings.add(networkRequester);
    injectMembersBindings.add(supertype);
  }

  /**
   * Returns the fully provisioned instance satisfying the contract for
   * {@code Provider<HighlightsFragment>}.
   */
  @Override
  public HighlightsFragment get() {
    HighlightsFragment result = new HighlightsFragment();
    injectMembers(result);
    return result;
  }

  /**
   * Injects any {@code @Inject} annotated fields in the given instance,
   * satisfying the contract for {@code Provider<HighlightsFragment>}.
   */
  @Override
  public void injectMembers(HighlightsFragment object) {
    object.networkRequester = networkRequester.get();
    supertype.injectMembers(object);
  }

}
