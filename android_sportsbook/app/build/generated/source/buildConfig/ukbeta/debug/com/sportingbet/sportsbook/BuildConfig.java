/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.sportingbet.sportsbook;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.sportingbet.sportsbook.beta";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "ukBeta";
  public static final int VERSION_CODE = 28;
  public static final String VERSION_NAME = "0.0.28";
  public static final String FLAVOR_domain = "uk";
  public static final String FLAVOR_backend = "beta";
  // Fields from the variant
  public static final int CASHOUT_DELETE_PERIOD_MILLIS = 5000;
  public static final int CASHOUT_REQUEST_PERIOD_SECONDS = 5;
  public static final int FILTER_FRACTION_DIGITS_NUMBER = 2;
  public static final int FILTER_INTEGER_DIGITS_NUMBER = 7;
  public static final int HELP_ITEM_ID = -1002;
  public static final int HIGHLIGHTS_NUMBER_OF_EVENTS = 10;
  public static final int HISTORY_PERIOD_MILLIS = 3500;
  public static final int HOME_ITEM_ID = -1001;
  public static final int INPLAY_ITEM_ID = -1000;
  public static final String INTERNATIONAL_HORSE_RACING_NAME = "International Horse Racing";
  public static final String LINK_GAMBLING_COMMISSION = "https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=1772";
  public static final String LINK_GENERAL_RULES = "https://www.sportingbet.com/t/info/rules/rules_hsc.html?forceDesktop=true";
  public static final String LINK_UNDERAGE_GAMBLING = "http://www.sportingbet.com/t/info/social-responsibility/underage-gambling.aspx?forceDesktop=true";
  public static final long REFRESH_PERIOD_SECONDS_LOGGED_IN = 30;
  public static final long REFRESH_PERIOD_SECONDS_LOGGED_OUT = 60;
  public static final int UPNEXT_NUMBER_OF_EVENTS = 50;
  // Fields from product flavor: uk
  public static final String DOMAIN_ID = "sportingbet-uk";
  public static final String HELP_EMAIL_ADDRESS = "admin@sportingbet.com";
  public static final String HELP_PHONE_NUMBER = "0800 028 0348";
  public static final Integer[] HIGHLIGHTS_SPORT_IDS = {102, 8, 190};
  public static final Integer[] INPLAY_SPORTS_IDS = {102, 8, 190, 4, 33, 11, 5, 177, 10, 18, 13, 175};
  public static final String LINK_PRIVACY_STATEMENT = "http://www.sportingbet.com/t/info/InformationPopup.aspx?info=privacy&forceDesktop=true";
  public static final String LINK_RESPONSIBLE_GAMING = "http://www.sportingbet.com/t/info/social-responsibility/responsible-gaming.aspx";
  public static final String LINK_TERMS_CONDITIONS = "https://www.sportingbet.com/t/info/rules/rules_hsc.html";
  public static final Integer[] POPULAR_SPORTS_IDS = {102, 8, 171, 190, 4, INPLAY_ITEM_ID};
  // Fields from product flavor: beta
  public static final String ACCOUNT_SERVICE_ENDPOINT = "https://apisb-beta.apiv1.com/AccountServices/V1/ServiceV1_4/";
  public static final String BET_SERVICE_ENDPOINT = "https://apisb-beta.apiv1.com/BetServices/V1/ServiceV1_2/";
  public static final String CONFIG_SERVICE_ENDPOINT = "https://content-beta.sportingbet.com/";
  public static final boolean CRASHLYTICS_ENABLED = true;
  public static final String PUBLISHING_SERVICE_ENDPOINT = "https://apisb-beta.apiv1.com/PublishingServices/V1/ServiceV1_1/";
  public static final String SESSION_SERVICE_ENDPOINT = "https://apisb-beta.apiv1.com/SessionServices/V1/ServiceV1_2/";
  public static final String WEB_SERVICE_URL = "https://mobile-beta.sportingbet.com/";
  // Fields from default config.
  public static final String APP_ID = "ANDROID-SPORTS-APP-V1";
}
