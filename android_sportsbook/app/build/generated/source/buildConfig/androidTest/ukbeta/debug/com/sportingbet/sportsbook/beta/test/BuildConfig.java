/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.sportingbet.sportsbook.beta.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.sportingbet.sportsbook.beta.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "ukBeta";
  public static final int VERSION_CODE = 28;
  public static final String VERSION_NAME = "0.0.28";
  public static final String FLAVOR_domain = "uk";
  public static final String FLAVOR_backend = "beta";
  // Fields from product flavor: uk
  public static final String DOMAIN_ID = "sportingbet-uk";
  public static final String HELP_EMAIL_ADDRESS = "admin@sportingbet.com";
  public static final String HELP_PHONE_NUMBER = "0800 028 0348";
  public static final Integer[] HIGHLIGHTS_SPORT_IDS = {102, 8, 190};
  public static final Integer[] INPLAY_SPORTS_IDS = {102, 8, 190, 4, 33, 11, 5, 177, 10, 18, 13, 175};
  public static final String LINK_PRIVACY_STATEMENT = "http://www.sportingbet.com/t/info/InformationPopup.aspx?info=privacy&forceDesktop=true";
  public static final String LINK_RESPONSIBLE_GAMING = "http://www.sportingbet.com/t/info/social-responsibility/responsible-gaming.aspx";
  public static final String LINK_TERMS_CONDITIONS = "https://www.sportingbet.com/t/info/rules/rules_hsc.html";
  public static final Integer[] POPULAR_SPORTS_IDS = {102, 8, 171, 190, 4, INPLAY_ITEM_ID};
  // Fields from product flavor: beta
  public static final String ACCOUNT_SERVICE_ENDPOINT = "https://apisb-beta.apiv1.com/AccountServices/V1/ServiceV1_4/";
  public static final String BET_SERVICE_ENDPOINT = "https://apisb-beta.apiv1.com/BetServices/V1/ServiceV1_2/";
  public static final String CONFIG_SERVICE_ENDPOINT = "https://content-beta.sportingbet.com/";
  public static final boolean CRASHLYTICS_ENABLED = true;
  public static final String PUBLISHING_SERVICE_ENDPOINT = "https://apisb-beta.apiv1.com/PublishingServices/V1/ServiceV1_1/";
  public static final String SESSION_SERVICE_ENDPOINT = "https://apisb-beta.apiv1.com/SessionServices/V1/ServiceV1_2/";
  public static final String WEB_SERVICE_URL = "https://mobile-beta.sportingbet.com/";
  // Fields from default config.
  public static final String APP_ID = "ANDROID-SPORTS-APP-V1";
}
