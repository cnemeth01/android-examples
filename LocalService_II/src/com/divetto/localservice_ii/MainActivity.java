package com.divetto.localservice_ii;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.divetto.localservice_ii.BindForServiceBaby.LocalBinder;

public class MainActivity extends Activity {
    BindForServiceBaby mService;
    private boolean binded;
    private ServiceConnection mConnection = new ServiceConnection() {

	@Override
	public void onServiceDisconnected(ComponentName name) {
	    mService = null;
	    binded = false;
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
	    Log.e("ServiceII", "onServiceConnected");
	    binded = true;
	    LocalBinder mLocalBinder = (LocalBinder) service;
	    mService = mLocalBinder.getServiceInstance();
	    // need some seconds until done
	}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);

	Intent mIntent = new Intent(this, BindForServiceBaby.class);
	bindService(mIntent, mConnection, BIND_AUTO_CREATE);
//	startService(mIntent);
    }

    @Override
    protected void onStop() {
	if (binded)
	    unbindService(mConnection);
	super.onStop();
    }

    public void askService(View v) {
	if (mService != null)
	    Toast.makeText(getApplicationContext(), mService.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.main, menu);
	return true;
    }

}
