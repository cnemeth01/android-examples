package com.sportingbet.casino;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

/**
 * First dummy test class to test robolectirc config.
 * Created by Ferenc_Wieszt on 9/18/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(manifest = "/src/main/AndroidManifest.xml", constants = com.sportingbet.casino.BuildConfig.class, sdk = 21)
public class FirstTest {

    @Test
    public void test() {
    }
}
