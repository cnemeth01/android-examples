package com.example.wishlistapp;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ListView;

public class MainActivity extends Activity {

	
	List<WishItem> items;
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		items=new ArrayList<WishItem>();
		items.add(new WishItem("F�lem�le", "Colosseum"));
		items.add(new WishItem("Fa", "Balaton"));
		items.add(new WishItem("Zacsk� kavics", "K�vez�s"));
		items.add(new WishItem("�lszak�l", "K�vez�s"));
		items.add(new WishItem("F�lem�le", "Colosseum"));
		items.add(new WishItem("Fa", "Balaton"));
		items.add(new WishItem("Zacsk� kavics", "K�vez�s"));
		items.add(new WishItem("�lszak�l", "K�vez�s"));
		items.add(new WishItem("F�lem�le", "Colosseum"));
		items.add(new WishItem("Fa", "Balaton"));
		items.add(new WishItem("Zacsk� kavics", "K�vez�s"));
		items.add(new WishItem("�lszak�l", "K�vez�s"));
		
		ItemAdapter adapter= new ItemAdapter(this, items);
		ListView listView=(ListView) findViewById(R.id.listItemWishView);
		listView.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
	
		getMenuInflater().inflate(R.menu.contextmenu, menu);
		
	}
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if(item.getItemId()==R.id.contects_deleteitme){
			
		}
		return super.onContextItemSelected(item);
	}
}
