package com.example.wishlistapp;

import java.util.List;

import com.example.wishlistapp.R;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class ItemAdapter extends ArrayAdapter<WishItem> {

	public ItemAdapter(Context context, List<WishItem> objects) {
		super(context, R.layout.mainlistitem, objects);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		WishItem wi=getItem(position);
		if (convertView==null) {
			convertView=LayoutInflater.from(getContext()).inflate(R.layout.mainlistitem, null);
		}
		TextView wishItemName=(TextView) convertView.findViewById(R.id.nameTextView1);
		TextView wishItemPlace=(TextView) convertView.findViewById(R.id.placeTextView2 );
		wishItemName.setText(wi.getName());
		wishItemPlace.setText(wi.getPlace());
		return convertView;
		
	}
	



}
