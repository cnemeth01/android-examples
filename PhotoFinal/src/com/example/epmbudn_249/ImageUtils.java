package com.example.epmbudn_249;

import java.io.IOException;
import java.io.InputStream;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;

public final class ImageUtils {
	
	private ImageUtils() {
	}
	
	public static Bitmap decodeBitmapByUri(ContentResolver contentResolver, Uri imageUri, int desiredWidth, int desiredHeight) {
		Bitmap bmp = null;
		InputStream inputStream = null;
		try {
			inputStream = contentResolver.openInputStream(imageUri);

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(inputStream, null, options);
			closeStream(inputStream);
			
			options.inSampleSize = calculateInSampleSize(options, desiredWidth, desiredHeight);
			
			inputStream = contentResolver.openInputStream(imageUri);
			
			// Decode bitmap with inSampleSize set
		    options.inJustDecodeBounds = false;
			bmp = BitmapFactory.decodeStream(inputStream, null, options);
			
//			Matrix matrix = new Matrix();
//			matrix.postRotate(90);
//			
//			finalBitmap = Bitmap.createBitmap(bmp , 0, 0, bmp .getWidth(), bmp .getHeight(), matrix, true);
		} catch (Exception e) {
			//throw new RuntimeException("Can't decode image", e);
		} finally {
			closeStream(inputStream);
		}
		
		return bmp;
	}
	
	private static void closeStream(InputStream in) {
		if (in != null) {
			try {
				in.close();
			} catch (IOException e) {
			}
		}
	}

	private static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

//			final int halfHeight = height / 2;
//			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((height / inSampleSize) > reqHeight
					&& (width / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}
}
