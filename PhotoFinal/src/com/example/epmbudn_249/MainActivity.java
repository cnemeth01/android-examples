package com.example.epmbudn_249;

import java.io.File;

import uk.co.senab.photoview.PhotoViewAttacher;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnClickListener {

	private static final int RESULT_LOAD_IMAGE = 0;
	private static final int PHOTO_TAKEN = 1;
	private static int ACTUAL_POSITION_DEGREE = 0;

	private PhotoViewAttacher photoViewAttacher;
	private ImageView photo;
	private ImageView rotateLeftImageView;
	private ImageView rotateRightImageView;
	private Bitmap actualPhoto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		photo = (ImageView) findViewById(R.id.mainPhoto);
		photo.setOnClickListener(this);
		
		rotateLeftImageView = (ImageView) findViewById(R.id.rotateLeftImageView);
		rotateLeftImageView.setOnClickListener(this);
		
		rotateRightImageView = (ImageView) findViewById(R.id.rotateRightImageView);
		rotateRightImageView.setOnClickListener(this);
		
		photoViewAttacher = new PhotoViewAttacher(photo);
		photoViewAttacher.update();
	}

	public void snapPhoto(View view) {
		Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(i, PHOTO_TAKEN);
	}

	public void loadPhoto(View view) {
		Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, RESULT_LOAD_IMAGE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK&& data != null) {
			loadSelectedImage(data);
		} else if (requestCode == PHOTO_TAKEN && resultCode == RESULT_OK&& data != null) {
			loadSelectedImage(data);
		}
	}

	private void loadSelectedImage(Intent data) {
		Uri imageUri = data.getData();
		new ImageLoaderTask().execute(imageUri);
	}

	class ImageLoaderTask extends AsyncTask<Uri, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(Uri... params) {
			Uri imageUri = params[0];
			int width = photo.getWidth();
			int height = photo.getHeight();
			return ImageUtils.decodeBitmapByUri(getContentResolver(), imageUri, width, height);
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			if (result != null) {		
				MainActivity.this.actualPhoto=result;
				rotateAndShowImage(Rotation.NONE);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rotateLeftImageView:
			rotateAndShowImage(Rotation.LEFT);
			break;
		case R.id.rotateRightImageView:
			rotateAndShowImage(Rotation.RIGHT);
			break;

		default:
			break;
		}	
	}

	private void rotateAndShowImage(Rotation rotation) {
		
		Matrix matrix = new Matrix();
		
		if( rotation == Rotation.LEFT){
			ACTUAL_POSITION_DEGREE-=90;
			matrix.setRotate(ACTUAL_POSITION_DEGREE);
		}else if( rotation == Rotation.RIGHT){
			ACTUAL_POSITION_DEGREE+=90;
			matrix.setRotate(ACTUAL_POSITION_DEGREE);
		}else{
			ACTUAL_POSITION_DEGREE=0;
			matrix.setRotate(ACTUAL_POSITION_DEGREE);
		}
		
		Bitmap rotatedBitmap = Bitmap.createBitmap(actualPhoto , 0, 0, actualPhoto .getWidth(), actualPhoto .getHeight(), matrix, true);
		photo.setImageBitmap(rotatedBitmap);
		
		photoViewAttacher.update();
		
	}
}
