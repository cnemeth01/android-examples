package com.example.gravitybouncingball;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.view.Menu;
import android.view.WindowManager;

import android.view.View;  
  
public class BouncingBallActivity extends Activity {
	private Sensor gravitySensor;
	private SensorManager mgr;
	private BouncingBallView bouncingBallView;
    /** Called when the activity is first created. */  
    @Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        //setContentView(R.layout.main);  
          
        bouncingBallView = new BouncingBallView(this);  
        setContentView(bouncingBallView);
        
        mgr = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        gravitySensor = mgr.getDefaultSensor(Sensor.TYPE_GRAVITY);
          
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

	@Override
	protected void onPause() {
		super.onPause();
		mgr.unregisterListener(bouncingBallView);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mgr.registerListener(bouncingBallView, gravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
	}  
    
    
}  
