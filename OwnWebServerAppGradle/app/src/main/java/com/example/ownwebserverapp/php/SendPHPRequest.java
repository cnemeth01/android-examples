package com.example.ownwebserverapp.php;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

public class SendPHPRequest {

    public static String sendHttpPost(List<NameValuePair> postParams, String url ) throws ClientProtocolException, IOException{
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(postParams);
        entity.setContentEncoding(HTTP.UTF_8);
        post.setEntity(entity);
        HttpResponse res = client.execute(post);
        String responseString = EntityUtils.toString(res.getEntity(), "UTF-8");
        return responseString;
 } 
    
    
}
