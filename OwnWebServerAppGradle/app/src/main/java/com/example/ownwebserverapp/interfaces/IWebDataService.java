package com.example.ownwebserverapp.interfaces;

import android.content.Context;
import android.os.Handler;

import com.example.ownwebserverapp.pojos.MemberModel;


public interface IWebDataService {

    public void getMembers( Handler handler);
    public void getMemberById(Handler handler);
    public void addMember(MemberModel newMember,Handler handler);
    public void deleteMember(MemberModel deletMember,Handler handler);
    public void updatemember(MemberModel updateMember,Handler handler);
    
   
}
