package com.example.ownwebserverapp.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Loader;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ownwebserver.adapters.MainAdapter;
import com.example.ownwebserver.dataservices.WebDataServiceWithThread;
import com.example.ownwebserverapp.R;
import com.example.ownwebserverapp.interfaces.IWebDataService;
import com.example.ownwebserverapp.loaders.DeleteMemberLoader;
import com.example.ownwebserverapp.loaders.GetMembersLoader;
import com.example.ownwebserverapp.loaders.InsertMemberLoader;
import com.example.ownwebserverapp.loaders.UpdateMemberLoader;
import com.example.ownwebserverapp.pojos.MemberModel;
import com.example.ownwebserverapp.pojos.Response;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity implements LoaderCallbacks<Response> {

    private static final int UPDATE_MEMBER = 66;
    private static final int DELETE_MEMBER = 34;
    private static final int INSERT_MEMBER_MESSAGE = 0;
    private static final int INSERT_NEW_MEMBER = 2;
    private static final int GET_MEMBERS_MESSSAGE = 123;
    private static final int GET_MEMEBERS_LOADER = 11;
    private static List<MemberModel> members;
    private EditText etInputName;
    private EditText etInputEmail;
    public ProgressDialog progressDialog;
    private ListView listView;
    private IWebDataService myWebService = new WebDataServiceWithThread();

    private Button btnRefresh;
    private Button btnNew;
    private MainAdapter simpleAdapter;
    private MemberModel newMember;
    private MemberModel selectedMember;
    private AlertDialog editAlert;

    private Handler handler = new MyHandler(this);

    private static class MyHandler extends Handler {

        private final WeakReference<MainActivity> activity;

        private MyHandler(MainActivity activity) {
            this.activity = new WeakReference<MainActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity mainActivity = activity.get();
            if(mainActivity != null) {
                mainActivity.progressDialog.dismiss();
                Response responseFromServer = (Response) msg.obj;

                if (msg.what == GET_MEMBERS_MESSSAGE) {
                    members = (List<MemberModel>) responseFromServer.getObject();
                    Collections.sort(members);
                    mainActivity.simpleAdapter.setAdapter(members);
                } else if (responseFromServer.isSuccess()) {
                    mainActivity.getLoaderManager().restartLoader(GET_MEMEBERS_LOADER, null, mainActivity).forceLoad();
                    Toast.makeText(mainActivity, "Success", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(mainActivity, responseFromServer.getErrorMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

    }

    private void initView() {
        etInputName = (EditText) findViewById(R.id.editText1);
        etInputEmail = (EditText) findViewById(R.id.editTextEmail);
        listView = (ListView) findViewById(R.id.listView1);
        btnRefresh = (Button) findViewById(R.id.button1);
        btnNew = (Button) findViewById(R.id.buttonNewMember);
        // simleAdapter = new ArrayAdapter<String>(this,
        // android.R.layout.simple_list_item_1, listString);
        members = new ArrayList<MemberModel>();
        simpleAdapter = new MainAdapter(this, R.id.listView1, members);

        listView.setAdapter(simpleAdapter);

        progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setInverseBackgroundForced(true);

        btnRefresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                progressDialog.show();

                // myWebService.getMembers(handler);

                getLoaderManager().restartLoader(GET_MEMEBERS_LOADER, null, MainActivity.this).forceLoad();

            }
        });
        btnNew.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String name = etInputName.getText().toString();
                String email = etInputEmail.getText().toString();
                newMember = new MemberModel();
                newMember.setName(name);
                newMember.setEmail(email);

                if (name.length() == 0 && email.length() == 0) {

                    Toast.makeText(MainActivity.this, "Fill both boxes first", Toast.LENGTH_SHORT).show();

                } else {

                    progressDialog.show();

                    getLoaderManager().restartLoader(INSERT_NEW_MEMBER, null, MainActivity.this).forceLoad();
                    etInputEmail.setText("");
                    etInputName.setText("");
                }
            }
        });

        registerForContextMenu(listView);
    }

    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {
        switch (id) {

            case GET_MEMEBERS_LOADER:
                return new GetMembersLoader(this);
            case INSERT_NEW_MEMBER:
                return new InsertMemberLoader(this, newMember);
            case DELETE_MEMBER:
                return new DeleteMemberLoader(this, selectedMember);
            case UPDATE_MEMBER:
                return new UpdateMemberLoader(this, selectedMember);

            default:
                return new Loader<Response>(this);
        }

    }

    private void sendMessageToHandler(Object obj, int what) {
        Message msg = new Message();
        msg.obj = obj;
        msg.what = what;
        handler.sendMessage(msg);
    }

    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
        if (loader.getId() == GET_MEMEBERS_LOADER) {

            sendMessageToHandler(data, GET_MEMBERS_MESSSAGE);
        } else
            sendMessageToHandler(data, INSERT_MEMBER_MESSAGE);

    }

    @Override
    public void onLoaderReset(Loader<Response> loader) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        return super.equals(o);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        selectedMember = members.get((int) info.id);

        menu.setHeaderTitle(selectedMember.getName());
        menu.add(0, v.getId(), 0, "Edit member");
        menu.add(0, v.getId(), 0, "Delete member");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == "Edit member") {

            LayoutInflater factory = LayoutInflater.from(this);
            final View textEntryView = factory.inflate(R.layout.alert_dialog_two_edittext, null);
            final EditText alertEditName = (EditText) textEntryView.findViewById(R.id.editTextName);
            final EditText alertEditEmail = (EditText) textEntryView.findViewById(R.id.editTextEmail);
            alertEditEmail.setText(selectedMember.getEmail());
            alertEditName.setText(selectedMember.getName());

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(textEntryView).setPositiveButton("Done", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    selectedMember.setName(alertEditName.getText().toString());
                    selectedMember.setEmail(alertEditEmail.getText().toString());
                    getLoaderManager().restartLoader(UPDATE_MEMBER, null, MainActivity.this).forceLoad();
                    editAlert.dismiss();
                }
            });


            editAlert = builder.create();
            editAlert.show();

        } else if (item.getTitle() == "Delete member") {
            getLoaderManager().restartLoader(DELETE_MEMBER, null, MainActivity.this).forceLoad();
        }

        return true;
    }



}
