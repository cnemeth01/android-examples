package com.example.ownwebserver.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ownwebserverapp.R;
import com.example.ownwebserverapp.pojos.MemberModel;

public class MainAdapter extends ArrayAdapter<MemberModel> {

	public List<MemberModel> members = new ArrayList<MemberModel>();
	private Context context;
	

	public MainAdapter(Context context, int resource, List<MemberModel> members) {
		super(context, resource, members);
		this.members = members;
		this.context = context;
	}

	@Override
	public int getCount() {
		return this.members.size();
	}
	public void setAdapter(List<MemberModel> items) {
		this.members = items;
		notifyDataSetChanged();
	}

	@Override
	public MemberModel getItem(int position) {
		return this.members.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		final MemberModel member = members.get(position);

		if (convertView == null) {

			LayoutInflater nInflate = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = nInflate.inflate(R.layout.list_row, parent, false);
			holder = new ViewHolder();

			holder.textName = (TextView) convertView.findViewById(R.id.textView1);
			holder.textEmail = (TextView) convertView.findViewById(R.id.textView2);
			
			convertView.setTag(holder);
		} else {

			holder = (ViewHolder) convertView.getTag();
		}

		holder.textName.setText(member.getName());
		holder.textEmail.setText(member.getEmail());
		
				return convertView;

	}

	static class ViewHolder {
		TextView textName;
		TextView textEmail;
		

	}

}
