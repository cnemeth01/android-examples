package com.example.interface_test;

import globals.Globals;
import interfaces.IDataCallback;
import interfaces.IDataSingleObjectCallback;

import java.util.ArrayList;

import pojos.Event;
import pojos.User;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Globals.init();
		
		Globals.dataBaseHelper.getUsers(new IDataCallback<User>()
		{
			@Override
			public void done(ArrayList<User> result)
			{
				for (User user : result)
				{
					Log.d("Main", ""+user.name);
				}
			}
		});
		
		Globals.dataBaseHelper.getUser(new IDataSingleObjectCallback<User>()
		{
			@Override
			public void done(User result)
			{
				
			}
		});
		
		Globals.dataBaseHelper.getEvents(new IDataCallback<Event>()
		{

			@Override
			public void done(ArrayList<Event> result)
			{
				// TODO Auto-generated method stub
				
			}
		});
		
	}
}
