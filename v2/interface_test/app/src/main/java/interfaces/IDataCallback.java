package interfaces;
import java.util.ArrayList;


public interface IDataCallback<T>
{
	void done(ArrayList<T> result);
}
