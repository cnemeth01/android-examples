package interfaces;

public interface IDataSingleObjectCallback<T>
{
	void done(T result);
}
