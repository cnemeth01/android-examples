package interfaces;

import pojos.Event;
import pojos.User;


public interface IDataModell
{
	void getUsers(IDataCallback<User> callBack);
	void getUser(IDataSingleObjectCallback<User> callBack);
	void getEvents(IDataCallback<Event> callBack);
}
