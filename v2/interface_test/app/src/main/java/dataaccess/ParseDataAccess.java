package dataaccess;
import interfaces.IDataCallback;
import interfaces.IDataModell;
import interfaces.IDataSingleObjectCallback;

import java.util.ArrayList;

import pojos.Event;
import pojos.User;


public class ParseDataAccess implements IDataModell
{
	@Override
	public void getUsers(IDataCallback<User> callBack)
	{	
		ArrayList<User> result = new ArrayList<User>();
		result.add(new User("Zoli"));
		result.add(new User("Peti"));
		callBack.done(result);
		// TODO Auto-generated method stub
	}

	@Override
	public void getEvents(IDataCallback<Event> callBack)
	{	
		ArrayList<Event> result = new ArrayList<Event>();
		callBack.done(result);
	}

	@Override
	public void getUser(IDataSingleObjectCallback<User> callBack)
	{
		callBack.done(new User("Zolika"));
		
	}
}
