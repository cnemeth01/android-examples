package globals;
import dataaccess.AnyDataAccess;
import dataaccess.ParseDataAccess;
import interfaces.IDataModell;


public class Globals
{
	public static IDataModell dataBaseHelper;
	private static boolean isParse = true;
	
	private static void setDataBaseHelper(IDataModell modell)
	{
		dataBaseHelper = modell;
	}

	public static void init()
	{
		if (isParse)
		{	
			setDataBaseHelper(new ParseDataAccess());
		}
		else
		{
			setDataBaseHelper(new AnyDataAccess());
		}
	}
}
