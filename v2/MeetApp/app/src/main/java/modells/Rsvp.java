package modells;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
@ParseClassName("Rsvp")
public class Rsvp extends ParseObject {

    private List<Meetup> meetup;
    private String rsvp;
    private List<User> user;

    public List<Meetup> getMeetup() {
        return getList("meetup");
    }

    public void setMeetup(List<Meetup> meetup) {
        put("meetup", meetup);
    }

    public String getRsvp() {
        return getString("rsvp");
    }

    public void setRsvp(String rsvp) {
        put("rsvp", rsvp);
    }

    public List<User> getUser() {
        return getList("user");
    }

    public void setUser(List<User> user) {
        put("user", user);
    }
}
