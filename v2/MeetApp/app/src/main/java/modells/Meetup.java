package modells;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;
import java.util.List;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
@ParseClassName("Meetup")
public class Meetup extends ParseObject {

    private Date date;
    private String description;
    private List<Group> group;
    private List<User> presenter;
    private String title;

    public Date getDate() {
        return getDate("date");
    }

    public void setDate(Date date) {
        put("date", date);
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        put("description", description);
    }

    public List<Group> getGroup() {
        return getList("group");
    }

    public void setGroup(List<Group> group) {
        put("group", group);
    }

    public List<User> getPresenter() {
        return getList("presenter");
    }

    public void setPresenter(List<User> presenter) {
        put("presenter", presenter);
    }

    public String getTitle() {
        return getString("title");
    }

    public void setTitle(String title) {
        put("title", title);
    }
}
