package modells;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
@ParseClassName("Group")
public class Group extends ParseObject {

    private String ibeacon_uuid;
    private String name;

    public String getIbeacon_uuid() {
        return getString("ibeacon_uuid");
    }

    public void setIbeacon_uuid(String ibeacon_uuid) {
        put("ibeacon_uuid",ibeacon_uuid);
    }

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name",name);
    }
}
