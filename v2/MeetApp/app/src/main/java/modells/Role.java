package modells;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
@ParseClassName("Role")
public class Role extends ParseObject {

    private String name;

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name",name);
    }
}
