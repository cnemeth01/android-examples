package modells;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
@ParseClassName("Rate")
public class Rate extends ParseObject {

    private List<Meetup> meetup;
    private int rate;
    private List<User> user;

    public List<Meetup> getMeetup() {
        return getList("meetup");
    }

    public void setMeetup(List<Meetup> meetup) {
        put("meetup", meetup);
    }

    public int getRate() {
        return getInt("rate");
    }

    public void setRate(int rate) {
        put("rate", rate);
    }

    public List<User> getUser() {
        return getList("user");
    }

    public void setUser(List<User> user) {
        put("user", user);
    }
}
