package modells;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
@ParseClassName("User")
public class User extends ParseObject {

    private String about;
    private String email;
    private List<Group> groups;
    private ParseGeoPoint location;
    private String name;
    private String position;
    private ParseFile profile_image;

    public String getAbout() {
        return getString("about");
    }

    public void setAbout(String about) {
        put("about", about);
    }

    public String getEmail() {
        return getString("email");
    }

    public void setEmail(String email) {
        put("email", email);
    }

    public List<Group> getGroups() {
        return getList("groups");
    }

    public void setGroups(List<Group> groups) {
        put("groups", groups);
    }

    public ParseGeoPoint getLocation() {
        return getParseGeoPoint("location");
    }

    public void setLocation(ParseGeoPoint location) {
        put("location", location);
    }

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name", name);
    }

    public String getPosition() {
        return getString("position");
    }

    public void setPosition(String position) {
        put("position", position);
    }

    public ParseFile getProfile_image() {
        return getParseFile("profile_image");
    }

    public void setProfile_image(ParseFile profile_image) {
        put("profile_image", profile_image);
    }
}
