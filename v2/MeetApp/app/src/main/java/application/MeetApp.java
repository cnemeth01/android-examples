package application;

import android.app.Application;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

import dataaccess.DataHelper;
import dataaccess.ParseDataAccess;
import modells.Group;
import modells.Meetup;
import modells.Rate;
import modells.Role;
import modells.Rsvp;
import modells.User;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
public class MeetApp extends Application {


    private boolean isParse=true;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Applicaton", "onCreate");
        initDataBase();
    }

    private void initDataBase() {

       if (isParse){
           initParse();
       }
    }

    private void initParse() {

        DataHelper.setDataModell(new ParseDataAccess());

        String PARSE_APPLICATION_ID = "lAJlh2piMl3cVHlyoprIaHTfnjlWioG49ngbb7yJ";
        String PARSE_CLIENT_KEY = "q6kkvpXWoNaRgmMxUEY4MSbthtBaSaipMeNm3SmO";


        Parse.initialize(this, PARSE_APPLICATION_ID, PARSE_CLIENT_KEY);
        ParseInstallation.getCurrentInstallation().saveInBackground();


        ParseObject.registerSubclass(Meetup.class);
        ParseObject.registerSubclass(Rate.class);
        ParseObject.registerSubclass(Group.class);
        ParseObject.registerSubclass(Role.class);
        ParseObject.registerSubclass(Rsvp.class);
        ParseObject.registerSubclass(User.class);

    }

}
