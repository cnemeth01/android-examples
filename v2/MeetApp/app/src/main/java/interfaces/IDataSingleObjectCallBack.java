package interfaces;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
public interface IDataSingleObjectCallBack<T> {

    public void done(T result);
}
