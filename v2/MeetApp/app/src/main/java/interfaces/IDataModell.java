package interfaces;

import modells.Meetup;
import modells.User;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
public interface IDataModell {

    public void getMeetApps(IDataCallBack<Meetup> meetupIDataCallBack);
    public void searchMeetUpp(IDataSingleObjectCallBack<Meetup> meetupIDataSingleObjectCallBack, String search);
    public void getUserByName(IDataCallBack<User> userIDataCallBack, String name);
}
