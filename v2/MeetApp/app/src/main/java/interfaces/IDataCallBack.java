package interfaces;

import java.util.List;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
public interface IDataCallBack<T> {

    public void done(List<T> result);
}
