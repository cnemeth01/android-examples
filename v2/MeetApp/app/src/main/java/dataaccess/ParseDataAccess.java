package dataaccess;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;

import interfaces.IDataCallBack;
import interfaces.IDataModell;
import interfaces.IDataSingleObjectCallBack;
import modells.Meetup;
import modells.User;

/**
 * Created by Csaba_Bela_Nemeth on 11/5/2014.
 */
public class ParseDataAccess implements IDataModell {


    @Override
    public void getMeetApps(final IDataCallBack<Meetup> meetupIDataCallBack) {
        ParseQuery<Meetup> query = ParseQuery.getQuery(Meetup.class);
        query.orderByAscending("date");

        query.findInBackground(new FindCallback<Meetup>() {
            public void done(List<Meetup> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores")  ;
                    meetupIDataCallBack.done(scoreList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    @Override
    public void searchMeetUpp(IDataSingleObjectCallBack<Meetup> meetupIDataSingleObjectCallBack, String search) {

    }

    @Override
    public void getUserByName(IDataCallBack<User> userIDataCallBack, String name) {

    }
}
