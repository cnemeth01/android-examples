package com.epam.meetapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.epam.meetapp.R;

import java.util.List;

import dataaccess.DataHelper;
import interfaces.IDataCallBack;
import modells.Meetup;

public class WelcomeActivity extends BaseActivity {

    private TextView textView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initView();
    }

    private void initView() {
        textView = (TextView) findViewById(R.id.textView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onClick(View v) {

        progressBar.setVisibility(View.VISIBLE);

        DataHelper.dataAcces.getMeetApps(new IDataCallBack<Meetup>() {
            @Override
            public void done(List<Meetup> result) {
                progressBar.setVisibility(View.GONE);

                String resultString = "";
                for (Meetup meetup : result) {
                    resultString += (meetup.getDescription() + ", " + meetup.getTitle() + ", ");
                }
                textView.setText(resultString);
            }
        });

    }
}
