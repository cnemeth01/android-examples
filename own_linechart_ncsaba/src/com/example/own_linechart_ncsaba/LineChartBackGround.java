package com.example.own_linechart_ncsaba;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class LineChartBackGround extends ViewGroup {

    private List<Integer> randomValues;
    private Paint paint = new Paint();
    private Paint paint2 = new Paint();
    private float y0=0f;
    private float x0 = 0f;
    private float dx = 25f;
    
    public LineChartBackGround(Context context) {
        super(context);
        initValues();
    }
    
    public LineChartBackGround(Context context, AttributeSet attrs) {
        super(context, attrs);
        initValues();
    }
    public LineChartBackGround(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initValues();
    }

   
   
    public void initValues(){
        randomValues=new ArrayList<Integer>();
        
        for (int i = 0; i < 40; i++) {

            randomValues.add(randInt(-400, 400));

        }
       
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(3f);
        paint2.setColor(Color.GRAY);
        paint.setStrokeWidth(5f);
      
    }
        
    
    public static int randInt(int min, int max) {

        Random rand = new Random();

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
        
     @Override
     protected void onDraw(Canvas canvas) {  
       
            float baseLine=this.getHeight()/2;
            y0=baseLine;
         
            canvas.drawLine(0,  baseLine, this.getWidth(), baseLine, paint2);     
            canvas.drawLine(0, baseLine+150, this.getWidth(), baseLine+150, paint2);
            canvas.drawLine(0,baseLine-150,this.getWidth(),baseLine-150, paint2);
            canvas.drawLine(0,baseLine+300,this.getWidth(), baseLine+300, paint2);
            canvas.drawLine(0,baseLine-300,this.getWidth(), baseLine-300, paint2);
            
            for(int i=0;i<randomValues.size();i++){
             
                canvas.drawLine(x0,y0,x0+dx,randomValues.get(i)+baseLine,paint);
                x0+=dx;
                y0=randomValues.get(i)+baseLine;
                
            }
        }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        // TODO Auto-generated method stub
        
    }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }


