package com.example.httpgetdemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class HttpGetActivity extends Activity {
	
	private TextView outputTextView;
	private EditText urlEditText;
	private TextView responseCodeTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_http_get);
		
		outputTextView = (TextView) findViewById(R.id.outputTextView);
		urlEditText = (EditText) findViewById(R.id.urlEditText);
		urlEditText.setText("http://www.index.hu");
		responseCodeTextView = (TextView)findViewById(R.id.responseCodeTextView);
	}
	
	public void letolt(View view) {
		String url = urlEditText.getText().toString();
		AsyncTask<String, Integer, String> task = new AsyncTask<String, Integer, String>() {

			@Override
			protected String doInBackground(String... param) {
				try {
					URL obj = new URL(param[0]);
					HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			 
					// am�gy alap�rtelmez�s is
					con.setRequestMethod("GET");
			 
					// request header hozz�ad�sa
					con.setRequestProperty("User-Agent", "Whatever 1.0");
			 
					int responseCode = con.getResponseCode();
					publishProgress(responseCode);
			 
					BufferedReader in = new BufferedReader(
					        new InputStreamReader(con.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();
			 
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();			
					return response.toString();
				} catch (IOException e) {
					Log.w("DOWNLOADER", e);
					return e.getMessage();
				}
			}
			
			@Override
			protected void onProgressUpdate(Integer... values) {
				responseCodeTextView.setText("Response code: "+values[0]);
			}
			
			@Override
			protected void onPostExecute(String result) {
				outputTextView.setText(result);
			}
		};
		task.execute(url);
		
	}

}
