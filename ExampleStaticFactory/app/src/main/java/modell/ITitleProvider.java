package modell;

/**
 * Created by nemethcsaba1 on 2015. 01. 11..
 */
public interface ITitleProvider {

    public Title newTitle();
}
