package modell;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by nemethcsaba1 on 2015. 01. 11..
 */
public class Title implements ITitle{

    private String titleString;
    private static final Map<String,TitleProvider> providers = new ConcurrentHashMap<>();
    private static final String DEFAULT_TITLE_PROVIDER = "Default Title";

    public String getTitleString() {
        return titleString;
    }

    @Override
    public Title newInstance() {

        return newInstance(DEFAULT_TITLE_PROVIDER);
    }

    public static Title newInstance(String titleProvier) {
        ITitleProvider provider = providers.get(titleProvier);
        if (provider == null) {
            throw new IllegalArgumentException("No pprovider registered: "+ titleProvier);
        }
        return provider.newTitle();
    }

    public static  void registerDefaultProvider(TitleProvider p){
        providers.put(DEFAULT_TITLE_PROVIDER,p);

    }

    public static void registrerPrivider(String name, TitleProvider provider){

        providers.put(name,provider);
    }
}
